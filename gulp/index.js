var os          = require('os'),
    fs          = require('fs'),
    onlyScripts = require('./util/scriptFilter'),
    tasks       = fs.readdirSync('./gulp/tasks/').filter(onlyScripts);
os.tmpDir = os.tmpdir;    

tasks.forEach(function(task) {
    require('./tasks/' + task);
});

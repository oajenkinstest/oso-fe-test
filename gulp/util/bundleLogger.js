// https://github.com/greypants/gulp-starter/

var colors          = require('chalk');
var log             = require('fancy-log');
var prettyHrtime    = require('pretty-hrtime');
var startTime;

module.exports = {
    start: function() {
        startTime = process.hrtime();
        log('Running', colors.green("'bundle'") + '...');
    },

    end: function() {
        var taskTime = process.hrtime(startTime),
            prettyTime = prettyHrtime(taskTime);
            log('Finished', colors.green("'bundle'"), 'in', colors.magenta(prettyTime));
    }
};

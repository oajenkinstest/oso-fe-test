// This file exports an global CONFIG object.
// Any properties added below dealing with
// paths are relative to THIS file location.
// ALL configuration for gulp tasks should live
// in this file.
var path = require('path');

var _CONFIG             = 'config';
var _CSS                = 'css';
var _DIST               = 'dist';
var _FONTS              = 'fonts';
var _IMG                = 'images';
var _JS                 = 'js';
var _MODULES            = 'modules';
var _LESS               = 'less';
var _REPORT             = 'html-report';
var _TEMPLATES          = 'templates';
var _TEST               = 'test';
var _PERFORMANCE_CENTER = 'performance-center'; //PC

var JS_CONFIG_FILE      = 'config.js';
var JS_DEVDATA_FILE     = 'devdata.js';

var LESS_FILE           = 'oso.less';
var LESS_FILE_WIDGET_PC = 'all-pc.less'; //Performance Center
var CSS_FILE            = 'all.css';
var CSS_MIN_FILE        = 'all.min.css';

var APP_START_FILE       = 'app.js';

// Peformance center file
var APP_START_WIDGET_PC_FILE  = 'app.js';

var APP_BUILT_FILE     = 'app.built.js';
var APP_START_MIN_FILE = 'app.min.js';

var DEVDATA_BUILT_FILE = 'devdata.built.js';
var DEVDATA_MIN_FILE   = 'devdata.min.js';

var VENDOR_START_FILE     = 'vendor.js';
var VENDOR_BUILT_FILE     = 'vendor.built.js';
var VENDOR_START_MIN_FILE = 'vendor.min.js';

// these get reused in the export

var APP_ROOT                    = path.join(__dirname, '../../');
var APP_GULP                    = path.join(APP_ROOT, 'gulp');
var APP_SRC                     = path.join(APP_ROOT, 'src');
var APP_DIST                    = path.join(APP_ROOT, _DIST);
var APP_RELEASE                 = path.join(APP_ROOT, 'release');

var APP_SRC_CSS                 = path.join(APP_SRC, _CSS);
var APP_SRC_FONTS               = path.join(APP_SRC, _FONTS);
var APP_SRC_IMG                 = path.join(APP_SRC, _IMG);
var APP_SRC_IMG_WIDGET_PC       = path.join(APP_SRC_IMG, _PERFORMANCE_CENTER);
var APP_SRC_JS                  = path.join(APP_SRC, _JS);

var APP_SRC_JS_TEMPLATES        = path.join(APP_SRC_JS, _TEMPLATES);
var APP_SRC_LESS                = path.join(APP_SRC_CSS, _LESS);
var APP_SRC_LESS_FILE           = path.join(APP_SRC_LESS, LESS_FILE);
var APP_SRC_LESS_FILE_WIDGET_PC = path.join(APP_SRC_LESS, LESS_FILE_WIDGET_PC);
var APP_SRC_CSS_FILE            = path.join(APP_SRC_CSS, CSS_FILE);

var APP_MODULES                 = path.join(APP_SRC_JS, _MODULES);
var APP_MODULES_JS_WIDGET_PC    = path.join(APP_MODULES, _PERFORMANCE_CENTER); //PC Peformance Center widget

var APP_SRC_CONFIG              = path.join(APP_SRC_JS, _CONFIG);
var APP_SRC_CONFIG_FILE         = path.join(APP_SRC_CONFIG, JS_CONFIG_FILE);
var APP_SRC_DEVDATA_FILE        = path.join(APP_SRC_JS, JS_DEVDATA_FILE);

var APP_DIST_CSS                = path.join(APP_DIST, _CSS);
var APP_DIST_FONTS              = path.join(APP_DIST, _FONTS);
var APP_DIST_IMG                = path.join(APP_DIST, _IMG);
var APP_DIST_JS                 = path.join(APP_DIST, _JS);
var APP_DIST_CONFIG             = path.join(APP_DIST_JS, _CONFIG);
var APP_DIST_CONFIG_FILE        = path.join(APP_DIST_CONFIG, JS_CONFIG_FILE);
var APP_DIST_CSS_FILE           = path.join(APP_DIST_CSS, CSS_FILE);
var APP_DIST_BUILT              = path.join(APP_DIST_JS, APP_BUILT_FILE);
var APP_DIST_START              = path.join(APP_DIST_JS, APP_START_FILE);
var APP_DIST_START_MIN          = path.join(APP_DIST_JS, APP_START_MIN_FILE);
var APP_DIST_DEVDATA_BUILT      = path.join(APP_DIST_JS, DEVDATA_BUILT_FILE);
var APP_DIST_VENDOR_BUILT       = path.join(APP_DIST_JS, VENDOR_BUILT_FILE);

var APP_RELEASE_CSS             = path.join(APP_RELEASE, _CSS);
var APP_RELEASE_FONTS           = path.join(APP_RELEASE, _FONTS);
var APP_RELEASE_IMG             = path.join(APP_RELEASE, _IMG);
var APP_RELEASE_JS              = path.join(APP_RELEASE, _JS);

var APP_REPORT                  = path.join(APP_ROOT, _REPORT);

var APP_START                   = path.join(APP_SRC_JS, APP_START_FILE);
var APP_START_WIDGET_PC         = path.join(APP_MODULES_JS_WIDGET_PC, APP_START_WIDGET_PC_FILE);

var APP_TEST                    = path.join(APP_ROOT, _TEST);
var APP_TEST_DIST               = path.join(APP_TEST, _DIST);
var APP_TEST_DIST_EXTERNAL      = path.join(APP_TEST, _DIST + '-external');

var VENDOR_START                = path.join(APP_SRC_JS, VENDOR_START_FILE);

module.exports = {

    APP_DIST                    : APP_DIST,

    APP_DIST_BUILT              : APP_DIST_BUILT,
    APP_DIST_CONFIG_FILE        : APP_DIST_CONFIG_FILE,
    APP_DIST_CSS                : APP_DIST_CSS,
    APP_DIST_CSS_FILE           : APP_DIST_CSS_FILE,
    APP_DIST_FONTS              : APP_DIST_FONTS,
    APP_DIST_IMG                : APP_DIST_IMG,
    APP_SRC_IMG_WIDGET_PC       : APP_SRC_IMG_WIDGET_PC,
    APP_DIST_JS                 : APP_DIST_JS,
    APP_DIST_START              : APP_DIST_START,
    APP_DIST_START_MIN          : APP_DIST_START_MIN,
    APP_DIST_DEVDATA_BUILT      : APP_DIST_DEVDATA_BUILT,
    APP_DIST_VENDOR_BUILT       : APP_DIST_VENDOR_BUILT,

    APP_GULP                    : APP_GULP,

    APP_RELEASE                 : APP_RELEASE,
    APP_RELEASE_CSS             : APP_RELEASE_CSS,
    APP_RELEASE_FONTS           : APP_RELEASE_FONTS,
    APP_RELEASE_IMG             : APP_RELEASE_IMG,
    APP_RELEASE_JS              : APP_RELEASE_JS,

    APP_REPORT                  : APP_REPORT,

    APP_ROOT                    : APP_ROOT,

    APP_SRC                     : APP_SRC,
    APP_SRC_CSS                 : APP_SRC_CSS,
    APP_SRC_CONFIG_FILE         : APP_SRC_CONFIG_FILE,
    APP_SRC_FONTS               : APP_SRC_FONTS,
    APP_SRC_IMG                 : APP_SRC_IMG,
    APP_SRC_JS                  : APP_SRC_JS,
    APP_SRC_LESS                : APP_SRC_LESS,
    APP_SRC_JS_TEMPLATES        : APP_SRC_JS_TEMPLATES,

    APP_SRC_LESS_FILE           : APP_SRC_LESS_FILE,
    APP_SRC_LESS_FILE_WIDGET_PC : APP_SRC_LESS_FILE_WIDGET_PC,
    APP_SRC_CSS_FILE            : APP_SRC_CSS_FILE,

    APP_MODULES_JS_WIDGET_PC    : APP_MODULES_JS_WIDGET_PC,

    APP_START                   : APP_START,
    APP_START_WIDGET_PC         : APP_START_WIDGET_PC,
    APP_START_FILE              : APP_START_FILE,
    APP_BUILT_FILE              : APP_BUILT_FILE,
    APP_START_MIN_FILE          : APP_START_MIN_FILE,

    JS_DEVDATA_FILE             : JS_DEVDATA_FILE,
    DEVDATA_BUILT_FILE          : DEVDATA_BUILT_FILE,
    DEVDATA_MIN_FILE            : DEVDATA_MIN_FILE,
    APP_SRC_DEVDATA_FILE        : APP_SRC_DEVDATA_FILE,

    APP_TEST                    : APP_TEST,
    APP_TEST_DIST               : APP_TEST_DIST,
    APP_TEST_DIST_EXTERNAL      : APP_TEST_DIST_EXTERNAL,

    CSS_FILE                    : CSS_FILE,
    CSS_MIN_FILE                : CSS_MIN_FILE,
    DEV_HOST                    : 'localhost',
    DEV_PORT_START              : 3000,
    DEV_PORT_END                : 3005,

    EXPRESS_PORT                : 3000, //3010,

    VENDOR_BUILT_FILE           : VENDOR_BUILT_FILE,

    VENDOR_START                : VENDOR_START,
    VENDOR_START_FILE           : VENDOR_START_FILE,
    VENDOR_START_MIN_FILE       : VENDOR_START_MIN_FILE
    
};

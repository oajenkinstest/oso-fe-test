// Copies images into the appropriate build folder
// TODO: Minify images for less bytes across the wire.

var config     = require('../config/config');
var gulp       = require('gulp');

var task = function (source, location) {
    return gulp.src(source + '/**/*')
        .pipe(gulp.dest(location));
};

gulp.task('images', function () {
    return task(config.APP_SRC_IMG, config.APP_DIST_IMG);
});

gulp.task('images-release', function () {
    return task(config.APP_SRC_IMG, config.APP_RELEASE_IMG);
});

gulp.task('images-widget-pc', function () {
    return task(config.APP_SRC_IMG_WIDGET_PC, config.APP_DIST_IMG);
});

gulp.task('images-widget-pc-release', function () {
    return task(config.APP_SRC_IMG_WIDGET_PC, config.APP_RELEASE_IMG);
});

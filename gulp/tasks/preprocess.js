// Allows writing out files with environment-specific code
// depending on the target.
// Ex - when running preprocess-dev below, this chunk of HTML
//
// <!-- @ifdef DEV -->
// <link rel="stylesheet" href="css/all.css" media="screen" />
// <!-- @endif -->
// <!-- @ifdef PROD -->
// <link rel="stylesheet" href="css/all.min.css" />
// <!-- @endif -->
//
// would render as
// <link rel="stylesheet" href="css/all.css" media="screen" />

var _            = require('underscore');
var config       = require('../config/config');
var gulp         = require('gulp');
var path         = require('path');
var package_json = require('../../package.json');
var preprocess   = require('gulp-preprocess');
var rename       = require('gulp-rename');

var context = {
    dev: {
        DEV: true,
        VERSION: ''
    },
    prod: {
        PROD: true,
        VERSION: ''
    }
};

var htmlFiles = [
    config.APP_SRC + '/*.html'
];

var preprocessHTML = function (target, location) {
    return gulp.src(htmlFiles)
               .pipe(preprocess({
                   context: context[target]
               }))
               .pipe(gulp.dest(location));
};

/**
 * Build the index-root.html file, adding the version from package.json in the file paths.
 * @param target
 * @param destination
 * @returns {*}
 */
var buildIndexRootHTML = function (target, destination) {
    var version        = package_json.version;
    var versionContext = _.extend(
        context[target], 
        { VERSION: 'v' + version + '/' }
    );
    return gulp.src(config.APP_SRC + '/index.html')
               .pipe(rename(path.join('index-root.html')))
               .pipe(preprocess({
                   context: versionContext
               }))
               .pipe(gulp.dest(destination));
        
};

// the actual tasks
gulp.task('preprocess-dev', function () {
    return preprocessHTML('dev', config.APP_DIST);
});

gulp.task('preprocess-prod', function () {
    return preprocessHTML('prod', config.APP_DIST);
});

gulp.task('preprocess-release', function () {
    return preprocessHTML('prod', config.APP_RELEASE);
});

gulp.task('preprocess-index-root', function() {
    return buildIndexRootHTML('prod', config.APP_RELEASE);
});

/**
 * To analyze all node packages (packages.json) used in the application 
 * and notify the developer of any known vulnerabilities exist.
 * 
 * Require to add `proxy` option to run this task under corporate
 * proxy environment, as it need to communicate NSP server for security check.
 * 
 */
var colors      = require('chalk');
var gulp        = require('gulp');
var spawn       = require('child_process').spawn;

gulp.task('npm-audit', function (cb) {
    var isWindows = /^win/i.test(process.platform);
    var npmCommand = 'npm';
    
    // In windows platform, require to append .cmd to run 'npm' command 
    if (isWindows) {
        npmCommand = 'npm.cmd';
    }

    var auditData = [];
    var auditReport;
    var status = {};
    
    var npmVersion;
    var npmVersionProcess;
    var npmAuditProcess;
    
    // Create child process
    
    npmVersionProcess  = spawn(npmCommand , ['--version']);
    npmVersionProcess.stdout.on('data', function (data) {
        npmVersion = data.toString('utf8');

        if (parseInt(npmVersion.split('.')[0], 10) >= 6) { 
            console.log(colors.green('"npm audit" supported! Start auditing.....'));
            runNPMAudit();

        } else {
            console.log(colors.red('NPM version installed is outdated, '+
                    'please update to version 6 to run "npm audit" command'));
            cb();
        }
    }); 

    /**
     * Run 'npm audit' command and proceed with build if there are no
     * issues with Critical and High status. 
     * Otherwise show the breif output and exist from build process
     */
    function runNPMAudit () {

        npmAuditProcess    = spawn(npmCommand , ['audit', '--json']);

        npmAuditProcess.stdout.on('data', function (data) {
            auditData.push(data.toString());
        });

        npmAuditProcess.stderr.on('data', function (data) {
            
            //throw errors
            console.log('stderr: ' + data);
        });

        npmAuditProcess.on('close', function (code) {
            try {
                auditData = JSON.parse(auditData.join(''));
                status = auditData.metadata.vulnerabilities;
            } catch(error){
                console.log(colors.red('npm audit - JSON data returned is not in valid format'));
            }

            //exit build if there are any vulnerabilities with status critical or high
            if (code !== 0) {
                if (status.critical > 0 || status.high > 0) {
                    auditReport = 'Build failed as NPM audit found '+
                    colors.bold.cyan(status.critical+' Critical') + ', '+
                    colors.bold.red(status.high+' High') + ', and ' +
                    colors.bold.yellow(status.moderate+' Moderate ') +
                    'issues with npm dependencies used in oso-frontend';

                    console.log('--  Audit report -- \n' + auditReport);
                } 
                
                // Since there are issues, we need to fix that before exit
                //process.exit(code);

            } else {
                console.log(colors.green('Yay! no issues found with NPM audit run!'));
            }
            
            cb();
        });
    }

});
var gulp = require('gulp');
// We are using this plugin to allow the entire set of tasks
// to be defined in a single location vs having to trace a
// chain of tasks throughout multiple files/tasks.
var rs   = require('run-sequence');


// This is the normal task to be run when doing local development.
// It starts a local server on the port defined as DEV_PORT_START
// in gulp/config/index.js, running on the host defined there as well
// (normally localhost).
// Changes to any html, images, js files, or less files
// will automatically trigger a build of the files into
// `dist`.
gulp.task('dev', function (callback) {
    rs(
        'build-dist',
        'devserver-dist',
        'mocha-test',
        'npm-audit',
        'watch-dev',
        callback
    );
});

gulp.task('dev-webpack', function (callback) {
    rs(
        'build-dist-webpack',
        'devserver-dist',
        'mocha-test',
        'npm-audit',
        'watch-dev-webpack',
        callback
    );
});

// Just build the site, but don't run tests
gulp.task('dev-fast', function (callback) {
    rs(
        'build-dist',
        'devserver-dist',
        'watch-dev',
        callback
    );
});

gulp.task('dev-fast-webpack', function(callback) {
    rs(
        'build-dist-webpack',
        'devserver-dist',
        'watch-dev-webpack',
        callback
    );
});

// The above dev task is the gulp default task
gulp.task('default', [ 'dev' ]);


// Use this when code-complete, but you want to test
// the minified versions of your JS and CSS.
// It uses built and minified files and updates the
// html files using preprocess and the 'PROD' target
gulp.task('prodcheck', function (callback) {
    rs(
        'build-release',
        'devserver-release',
        'watch-prod',
        callback
    );
});

//task to build Performance center widget.
gulp.task('widget-pc-dev', function (callback) {
    rs(
        'build-widgetpc-dist',
        'devserver-dist',
        'watch-widgetpc-dev',
        callback
    );
});

gulp.task('widget-pc-prodcheck', function (callback) {
    rs(
        'build-widgetpc-release',
        'devserver-release',
        'watch-prod',
        callback
    );
});

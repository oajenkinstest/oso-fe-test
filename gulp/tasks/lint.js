var config = require('../config/config');
var gulp   = require('gulp');
var eslint = require('gulp-eslint');

gulp.task('lint', function () {
    return gulp.src(config.APP_SRC + '/js/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('lint-prod', function () {
    return gulp.src(config.APP_SRC + '/js/**/*.js')
        .pipe(eslint(config.APP_ROOT + '/.eslintrc-prod'))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('lint-release', function () {
    return gulp.src(config.APP_SRC + '/js/**/*.js')
        .pipe(eslint(config.APP_ROOT + '/.eslintrc-prod'))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('lint-gulp', function () {
    return gulp.src(config.APP_GULP + '/**/*.js')
        .pipe(eslint(config.APP_ROOT + '/.eslintrc-node'))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('lint-test', function () {
    // Do not return an error status if there are lint problems in test
    // specs because that breaks the running gulp watch task.
    return gulp.src([
        config.APP_TEST + '/mocha-test/**/*.js'
    ])
        .pipe(eslint(config.APP_ROOT + '/.eslintrc-test'))
        .pipe(eslint.format());
});

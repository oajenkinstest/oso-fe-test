// Removes all files in the specified location.
// Normally runs before starting up.
// You should NOT clean when watching for file changes,
// because we only rebuild what has changed.

var config = require('../config/config');
var del    = require('del');
var gulp   = require('gulp');

var glob = '/**/*';
var keep = '!/**/*.gitkeep';
var artifact  = '!/**/*.zip';

gulp.task('clean-dist', function () {
    return del.sync([
        config.APP_DIST + glob,
        keep
    ]);
});

gulp.task('clean-release', function () {
    return del.sync([
        config.APP_RELEASE + glob,
        keep
    ]);
});

gulp.task('clean-release-keep-artifact', function () {
    return del.sync([
        config.APP_RELEASE + glob,
        artifact
    ]);
});

gulp.task('clean-test', function () {
    return del.sync([
        config.APP_TEST_DIST + glob,
        config.APP_REPORT,
        keep
    ]);
});

gulp.task('clean-all', [
    'clean-dist',
    'clean-release',
    'clean-test'
]);

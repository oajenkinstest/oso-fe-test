// Automatically keeps any browsers that are open to the
// local development environement in sync using socket.io.
// The task of `bs-reload` is triggered as needed in other
// tasks.

/*
      *** Commented out because browser-sync is causing problems with the ***
      *** builds on Jenkins. Browser sync depends on socket.io, which is  ***
      *** a native Node module that is built using npm-gyp during an      ***
      *** `npm install`. Building npm-gyp requires that Python AND Visual ***
      *** Studio C++ 2013 are installed (according to the documentation   ***
      *** at https://github.com/TooTallNate/node-gyp/), which are not     ***
      *** present on the Jenkins server. Usually the build step will      ***
      *** simply fail for that particular dependency and the `npm install`***
      *** will continue. Sometimes, though, the build hangs at this point ***
      *** requiring manual cancellation of the job.                       ***
      ***                                                                 ***
      *** Not having browser-sync will mean that we have to manually      ***
      *** refresh our browsers after saving a change on our local dev     ***
      *** environments. This is a minor annoyance with the benefit of     ***
      *** faster and more reliable Jenkins build jobs.                    ***
      ***                                                                 ***
      *** A better solution would be to separate dependencies in          ***
      *** package.json into "dependencies" that are truly required to     ***
      *** build the application, and "devDependencies" for tools that are ***
      *** only used in our local dev   environments. Separated in this    ***
      *** way, the Jenkins jobs can run `npm install --production` which  ***
      *** ignores the devDependencies. Unfortunately, we can't use this   ***
      *** scheme because this gulp task file requires browser-sync even   ***
      *** if the task is never used. We will need to think of a way to    ***
      *** make different gulp tasks available in dev than are available   ***
      *** in the Jenkins build environment.               --RKC 7/29/15   ***

var browserSync = require('browser-sync');
var config      = require('../config/config');
var gulp        = require('gulp');

gulp.task('browserSync', function() {
    return browserSync({
        open: false,
        port: config.DEV_PORT_START,
        // the proxy handles the static files in the APP_DIST
        // directory - they are bundled by gulp
        // proxying into the various environments for real data
        // could also handle serving locally but this is not
        // set up yet
        proxy: config.DEV_HOST + ':' + config.EXPRESS_PORT
    });
});

// Reload all Browsers
gulp.task('bs-reload', function(){
    browserSync.reload();
});
*/

// Builds the LESS files into CSS files.
// Normally, you should NOT edit the css files directly.
// Edit the appropriate LESS module.

var config          = require('../config/config');
var gulp            = require('gulp');
var less            = require('gulp-less');
var path            = require('path');
var handleErrors    = require('../util/handleErrors');
var rename    		= require('gulp-rename');

gulp.task('less', function () {

    return gulp.src(config.APP_SRC_LESS_FILE)
        .pipe(less({
            javascriptEnabled : true,
            paths: [
                path.join(__dirname, 'less', 'includes'),
                path.join(config.APP_ROOT)
            ]
        }))
        .pipe(rename(config.CSS_FILE))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.APP_DIST_CSS));
});

//less task for Performance center widget
gulp.task('less-pc', function () {

    return gulp.src(config.APP_SRC_LESS_FILE_WIDGET_PC)
        .pipe(less({
            javascriptEnabled : true,
            paths: [
                path.join(__dirname, 'less', 'includes'),
                path.join(config.APP_ROOT)
            ]
        }))
        .pipe(rename(config.CSS_FILE))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.APP_DIST_CSS));
});
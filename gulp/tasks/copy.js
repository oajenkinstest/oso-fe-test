var gulp   = require('gulp');
var rs     = require('run-sequence');

var config = require('../config/config');
var path   = require('path');

var AWESOME_FONTS             = 'node_modules/font-awesome/fonts/*';
var BOOTSTRAP_FONTS           = 'node_modules/bootstrap/fonts/*';
var VENDOR_FONTS              = 'src/vendor/fonts/*';

var SERVICE_WORKER_JS_FILE    = 'src/js/pages/uiTest/views/sw.js';
var IE_CSS                    = 'src/css/ace-ie.css';

gulp.task('copy-dist', function (callback) {
    rs(
        'copy-fonts-dist',
        'copy-ace-ie-css-dist',
        'copy-service-worker-file',
        callback
    );
});

gulp.task('copy-release', function (callback) {
    rs(
        'copy-fonts-release',
        'copy-ace-ie-css-release',
        callback 
    );
});

gulp.task('copy-fonts-dist', function () {
    return gulp.src([AWESOME_FONTS, BOOTSTRAP_FONTS, VENDOR_FONTS])
               .pipe(gulp.dest(config.APP_DIST_FONTS)); 
});

gulp.task('copy-service-worker-file', function () {
    return gulp.src(SERVICE_WORKER_JS_FILE)
               .pipe(gulp.dest(config.APP_DIST_JS)); 
});

gulp.task('copy-fonts-release', function () {
    return gulp.src([AWESOME_FONTS, BOOTSTRAP_FONTS, VENDOR_FONTS])
               .pipe(gulp.dest(config.APP_RELEASE_FONTS)); 
});

gulp.task('copy-ace-ie-css-release', function () {
    return gulp.src(IE_CSS)
                .pipe( gulp.dest(config.APP_RELEASE_CSS));
});

gulp.task('copy-ace-ie-css-dist', function () {
    return gulp.src(IE_CSS)
                .pipe( gulp.dest(config.APP_DIST_CSS));
});


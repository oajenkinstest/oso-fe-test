// Start a local webserver.

/*global require */
/*es5*/
var bodyParser = require('body-parser');
var config     = require('../config/config');
var cors       = require('../util/cors');
var csp        = require('helmet-csp');
var express    = require('express');
var gulp       = require('gulp');
var path       = require('path');
var request    = require('request');
var winston    = require('winston');

// Start a Node.js server that serves static files from the
// specified root directory.
var startDevServer = function (rootDir) {

    var cspURL     = '/api/oso/public/rest/csp-log';
    var dataPort   = 5000;
    var dataHost   = 'http://localhost:' + dataPort;
    var restURL    = '/api/oso/secure/rest';
    var jsonParser;
    var cspLogFile;
    var logger;

    // Create a file to log CSP violations
    cspLogFile = path.join(config.APP_ROOT, 'content-security-policy.log');

    // Create a logger to log CSP violations to the console and to the log file
    logger = new (winston.Logger) ({
        level      : 'debug',
        transports : [
            new (winston.transports.Console) ({
                json     : false,
                colorize : true
            }),
            new (winston.transports.File) ({
                json     : false,
                filename : cspLogFile,
                // cap the log file size at 10MB
                maxsize  : 10*1024*1024,
                // If maxsize is reached and the log rolls over, keep the last old one
                maxFiles : 2
            })
        ]
    });

    /**
     * The reasoning for the policy can be found in the product documentation
     * at docs/ContentSecurityPolicy.md. If changes are made to the directives,
     * the documentation should be updated.
     */
    var cspDirectives = csp({
        directives : {
            defaultSrc              : ["'self'"],
            baseUri                 : ["'none'"],
            // Note: The connect-src directive is set here to allow connections
            // to localhost on port 5000. This directive shouldn't need to be
            // explicitly set in IHS environments.
            connectSrc              : ["'self'", "localhost:5000"],
            imgSrc                  : [
                "'self' data:",
                "statse.webtrendslive.com",
                "www.google-analytics.com",
                "www.googletagmanager.com"
            ],
            scriptSrc               : [
                "'self'",
                "'unsafe-eval'",
                "statse.webtrendslive.com",
                "www.googletagmanager.com",
                "www.google-analytics.com"
            ],
            styleSrc                : [
                "'self'",
                "'unsafe-inline'"
            ],
            reportUri               : cspURL
        },
        reportOnly    : true,
        // Allow IE11 to be tested by setting the X-Content-Security-Policy header
        setAllHeaders : true
    });

    var app = module.exports = express();
    // Serve static files by default
    // in this case, this is our bundle.
    // Apply CSP headers for everything except the devdata.html file
    // which has in-line JavaScript and will trigger a violation. Since
    // the file isn't used outside of the local development environment,
    // we'll just remove the CSP headers.
    app.use(cspDirectives, express.static(rootDir, {
        setHeaders : function(res, path) {
            if (path.substr(-12) === 'devdata.html') {
                res.removeHeader('Content-Security-Policy');
                res.removeHeader('Content-Security-Policy-Report-Only');
            }
        }
    }));
    app.use(cors);

    // jsonParser is used to parse csp-reports using the MIME types used by different browsers
    jsonParser = bodyParser.json({ type : [
        'application/json',
        'application/csp-report'
    ]});

    // Used to log CSP violations
    app.post(cspURL, jsonParser, function(req, res) {
        // Log the violation
        if (req.body['csp-report']) {
            logger.warn('CSP Violation: ', req.body['csp-report']);
        } else {
            logger.warn('CSP Violation: No data received!');
        }
        // Send back the appropriate response code
        res.status(204).end();
    });

    // main page of app
    // on any request, if it's for REST data
    // we proxy against the real env and return data
    app.all(restURL + '/*', function (req, res) {
        var proxyURL = req.url.split(restURL)[1];
        var newReqUrl = dataHost + restURL + proxyURL;
        console.log('URL was ', newReqUrl);
        
        var newReq;
        if (req.method === 'POST') {
            newReq = request.post({
                uri: newReqUrl, 
                json: req.body
            });
        } else if (req.method === 'PUT') {
            newReq = request.put({
                uri: newReqUrl, 
                json: req.body
            });
        } else {
            newReq = request.get({
                uri :newReqUrl,
                json: req.body,
                followAllRedirects: true
            });
        }       
        req.pipe(newReq).pipe(res);
    });


    /**
     * Emulate the ProxyPass setup for WCM
     * Take '/wps/*' URLs and call them on the localdata-dev server,
     * and pipe the response back to the browser.
     */
    app.get('/wps/*', function (req, res) {
        req.pipe(request(dataHost + req.url))
            .on('error', function(e) {
                console.log('Error requesting WCM localdata-dev: ' + JSON.stringify(e));
            })
            .pipe(res)
            .on('error', function(e) {
                console.log('Error piping WCM response from localdata-dev: ' + JSON.stringify(e));
            });
    });


    app.listen(config.EXPRESS_PORT);
    console.log('-------------------------------------------------');
    console.log('Local dev server started at http://localhost:' + config.EXPRESS_PORT);
    console.log('Manually refresh your browser to see changes.');
    console.log('-------------------------------------------------');

};


// Node server runs on the config.EXPRESS_PORT and
// serves the web site files from /dist
gulp.task('devserver-dist', function () {
    return startDevServer(config.APP_DIST);
});


// Node server runs on the config.EXPRESS_PORT and
// serves the web site files from /release
gulp.task('devserver-release', function () {
    return startDevServer(config.APP_RELEASE);
});


// Local server for developing unit tests
gulp.task('devserver-unit-test-develop', function () {

    var app = module.exports = express();
    app.use(express.static(config.APP_ROOT));
    app.listen(config.EXPRESS_PORT);

});

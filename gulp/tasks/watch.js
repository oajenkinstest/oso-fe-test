// Watches for file changes and triggers other gulp tasks
// as needed.

var config = require('../config/config');
var gulp   = require('gulp');
var rs     = require('run-sequence');

// files we always watch
var commonWatch = [
     config.APP_SRC      + '/**/*.html',
     config.APP_SRC_IMG  + '/**/*',
     config.APP_SRC_JS   + '/**/*.js',
     config.APP_SRC      + '/**/*.hbs',
     config.APP_SRC_LESS + '/**/*.less'
];

// Normal development, look for files changes.
// Then places built files in the right places.
gulp.task('watch-dev', [
    //'setWatch',
//    'browserSync'
], function () {
    gulp.watch(commonWatch, [
        'lint',
        [ 'less', 'images', 'preprocess-dev' ],
        'browserify-dev',
        // 'test'
    //    ,'bs-reload'
    ]);
});

// Used for webpack
gulp.task('watch-dev-webpack', [], function () {
    gulp.watch(commonWatch, [
        'lint',
        [ 'less', 'images', 'preprocess-dev' ],
        'webpack-bundle-dev'
    ]);
});

// Same as above, but also minify the files to test as in prod.
gulp.task('watch-prod', [
    //'setWatch',
  //  'browserSync'
], function () {
    gulp.watch(commonWatch, function (callback) {
        rs(
            [ 'less', 'lint-prod', 'images', 'preprocess-prod' ],
            [ 'css-minify-css-prod', 'browserify-release' ],
            'uglify-scripts-prod',
            //'bs-reload',
            callback
        );
    });
});

gulp.task('watch-widgetpc-dev', [
    //'setWatch',
    // 'browserSync'
], function () {
    gulp.watch(commonWatch, [
        'lint',
        [ 'less-pc', 'images-widget-pc', 'preprocess-dev' ],
        'browserify-widgetpc-dev'
        // ,'bs-reload'
    ]);
});

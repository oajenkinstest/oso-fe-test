/* global require:false, process:false */
/**
 * Since all of the gulp tasks get loaded regardless of which task get called, the
 * following will cause the build to fail if the version of node is less than 6.11.0.
 *
 * As a result, there is some logic being incorporated to keep webpack from
 * loading if the node version is less than 6.11.0.
 *
 * TODO: Once Jenkins version of node is updated, this check can be removed and build.gradle
 * can be updated to utilize the build-release-webpack task.
 */
var nodeVersion = require('node-version');
var webpackNodeRequirements = {
    major : 6,
    minor : 11
};

var gulp = require('gulp');

// The following will cause the entire build to fail
// if the version of node is not equal to or greater than v6.11.0.
if (Number(nodeVersion.major) >= webpackNodeRequirements.major &&
    Number(nodeVersion.minor >= webpackNodeRequirements.minor)) {

    var config = require('../config/config');
    var gulpWebpack = require('webpack-stream');
    var webpack = require('webpack');

    gulp.task('webpack-bundle-dev', function () {
        return gulp.src(config.APP_SRC + '**/*.*')
            .pipe(gulpWebpack({
                config: require('../../webpack.dev')
            }, webpack))
            /* Passing in the most recent version of webpack (v4)
             * above since webpack-stream lists version 3 as a dependency.
             * See https://github.com/shama/webpack-stream/pull/188#issuecomment-400773876
             */
            .pipe(gulp.dest(config.APP_DIST_JS));
    });

    gulp.task('webpack-bundle-release', function () {
        return gulp.src(config.APP_SRC + '**/*.*')
            .pipe(gulpWebpack({
                config: require('../../webpack.prod')
            }, webpack))
            /* Passing in the most recent version of webpack (v4)
             * above since webpack-stream lists version 3 as a dependency.
             * See https://github.com/shama/webpack-stream/pull/188#issuecomment-400773876
             */
            .pipe(gulp.dest(config.APP_RELEASE_JS));
    });
} else {

    var displayMessageAndStopTasks = function displayMessageAndStopTasks () {
        // Display a warning message to the user
        console.log('-------------------------------------------');
        console.log('Webpack and webpack-stream are not initialized');
        console.log('since the required version of Node is not installed.\n');
        console.log(' Needed: Node v' + webpackNodeRequirements.major + '.' +
            webpackNodeRequirements.minor);
        console.log(' Found:  Node v' + nodeVersion.short);
        console.log('-------------------------------------------');
        // Don't allow any other tasks that may be ran after this one run
        process.exit();
    };

    gulp.task('webpack-bundle-dev', function () {
        displayMessageAndStopTasks();
    });

    gulp.task('webpack-bundle-release', function () {
        displayMessageAndStopTasks();
    });
}
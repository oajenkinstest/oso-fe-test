var config    = require('../config/config');
var gulp      = require('gulp');
var cleanCSS  = require('gulp-clean-css');
var rename    = require('gulp-rename');

gulp.task('css-minify-css-prod', function () {
    return gulp.src(config.APP_DIST_CSS_FILE)
               .pipe(cleanCSS({
                   // set to 1 to keep all
                   keepSpecialComments: 0,
                   // set to true to preserve for readability
                   keepBreaks: false,
                   // set to true to turn off merging, etc
                   // since this will modify the generated css
                   // it's possible there could be subtle bugs
                   // here
                   // turn off if things get odd
                   advanced: true
               }))
               .pipe(rename(config.CSS_MIN_FILE))
               .pipe(gulp.dest(config.APP_DIST_CSS));
});

gulp.task('css-minify-css-release', function () {
    // TODO: Report an error if the file config.APP_DIST_CSS_FILE is missing
    return gulp.src(config.APP_DIST_CSS_FILE)
               .pipe(cleanCSS({
                   keepSpecialComments : 0,
                   keepBreaks          : false,
                   advanced            : true
               }))
               .pipe(rename(config.CSS_MIN_FILE))
               .pipe(gulp.dest(config.APP_RELEASE_CSS));
});

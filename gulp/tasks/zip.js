var config = require('../config/config');
var gulp   = require('gulp');
var p      = require('../../package.json');
var zip    = require('gulp-zip');

gulp.task('zip-release', function () {
    return gulp.src(config.APP_RELEASE + '/**/*', { base: config.APP_RELEASE })
        .pipe(zip(p.name + '-' + p.version + '.zip'))
        .pipe(gulp.dest(config.APP_RELEASE));
});

var gulp = require('gulp');
var rs   = require('run-sequence');

gulp.task('build-dist', function (callback) {
    rs (
        'clean-dist',
        'lint',
        [ 'less', 'images', 'preprocess-dev', 'copy-dist' ],
        'browserify-dev',
        callback
    );
});

gulp.task('build-dist-webpack', function(callback) {
    rs(
        'clean-dist',
        'lint',
        [ 'less', 'images', 'preprocess-dev', 'copy-dist' ],
        'webpack-bundle-dev',
        callback
    );
});

// Builds files into `release`.
// Add tasks here as needed that will generate the
// needed code.
gulp.task('build-release', function (callback) {
    rs(
        // some of the tasks here build files into dist/, then process
        // them into release/
        [ 'clean-dist', 'clean-release' ],
        [ 'lint-release'],
        [ 'mocha-test' ],
        'less', 'images-release', 'preprocess-release', 'preprocess-index-root', 'copy-release',
        [ 'css-minify-css-release', 'browserify-release' ],
        'uglify-scripts-release',
        callback
    );
});

// Like the task above but this task
// uses the bundles created by webpack.
gulp.task('build-release-webpack', function(callback) {
    rs(
        [ 'clean-dist', 'clean-release' ],
        [ 'lint-release' ],
        [ 'mocha-test' ],
        'less', 'images-release', 'preprocess-release', 'preprocess-index-root', 'copy-release',
        [ 'css-minify-css-release', 'webpack-bundle-release' ],
        callback
    );
});

gulp.task('build-release-archive', function (callback) {
    rs(
        'zip-release',
        'clean-release-keep-artifact',
        callback
    );

});

gulp.task('build-widgetpc-dist', function (callback) {
    rs (
        'clean-dist',
        'lint',
        [ 'less-pc', 'images-widget-pc', 'preprocess-dev', 'copy-dist' ],
        'browserify-widgetpc-dev',
        callback
    );
});

gulp.task('build-widgetpc-release', function (callback) {
    rs(
        [ 'clean-dist', 'clean-release' ],
        [ 'lint-release', 'less-pc', 'images-widget-pc-release', 'preprocess-release', 'copy-release' ],
        [ 'css-minify-css-release', 'browserify-widgetpc-release' ],
        'uglify-scripts-release',
        callback
    );
});

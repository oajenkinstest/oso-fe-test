var gulp     = require('gulp');
var istanbul = require('gulp-istanbul');
var mocha    = require('gulp-mocha');
var rs       = require('run-sequence');

gulp.task('test', ['mocha-test']);
gulp.task('test-fast', ['mocha-test-fast']);

// Browserify files for using in unit tests
gulp.task('test-unit-test-setup', function (callback) {
    rs(
        [ 'clean-test', 'lint-test' ],
        'browserify-for-testing',
        callback
    );
});

// Run Mocha unit tests
gulp.task('mocha-test', ['test-unit-test-setup'], function () {
    return gulp.src([
        'test/mocha-test/setup/helpers.js',
        'test/mocha-test/*spec.js',
    ], {read : false})
        .pipe(mocha({
            // Bail after first test failure?
            bail     : false,
            // Some available reporters:
            //   'progress', 'list', 'spec', 'nyan', 'html', 'markdown', 'json'
            // We're using a third-party reporter that generates the xml test report but also
            // sends output to the console.
            reporter : 'mocha-jenkins-reporter',
            reporterOptions: {
                junit_report_path: 'report.xml'
            },
            require  : ['./test/mocha-test/setup/setup.js'],
            timeout  : 5000,
            slow     : 100
        }))
        .on('error', function(e) {
            console.log('Build failed due to test failure(s).');
            console.dir(e);
            process.exit(1);
        })
        .pipe(istanbul.writeReports({
            // Some of the available reporters:
            //   'html', 'cobertura', 'text', 'json', 'text', 'text-summary', 'lcov'
            reporters: [ 'html', 'cobertura' ],
            reportOpts: {
                html: {
                    dir: './htmlReport',
                    watermarks: {
                        statements : [ 70, 90 ],
                        lines      : [ 70, 90 ],
                        functions  : [ 70, 90 ],
                        branches   : [ 70, 90 ]
                    }
                },
                cobertura   : { filename: 'cobertura-coverage.xml' },
            },
            coverageVariable: '__osoTestCoverage'
        }))
        // These prevent gulp from hanging if something isn't cleaned up properly in mocha
        // or istanbul. See https://github.com/sindresorhus/gulp-mocha#faq. And even though
        // this site suggests also calling process.exit() on 'exit', we can't do it because
        // it interferes with the Gradle build in Jenkins. If no tests are failing but Mocha
        // is hanging, make sure that none of the tests are causing the idle watcher to be
        // started.
        .once('error', function() {
            process.exit(1);
        });
});

// This is a convenience task to use while working on Mocha tests. It runs the tests
// without rebuilding all of the source files in /dist. It will only work if you run
// the normal build first (`gulp test` or `gulp mocha-test` to bundle the dist files.
gulp.task('mocha-test-fast', function () {
    return gulp.src([
        'test/mocha-test/setup/helpers.js',
        'test/mocha-test/*spec.js',
    ], {read : false})
        .pipe(mocha({
            checkLeaks : true,
            // Bail after first test failure?
            bail       : false,
            // Some available reporters:
            //   'progress', 'list', 'spec', 'nyan', 'html', 'markdown', 'json'
            reporter   : 'spec',
            require    : ['./test/mocha-test/setup/setup.js'],
            timeout    : 5000,
            slow       : 50
        }))
        .on('error', function(e) {
            console.log('Build failed due to test failure(s).');
            console.dir(e);
            process.exit(1);
        })
        .pipe(istanbul.writeReports({
            // Some of the available reporters:
            //   'html', 'cobertura', 'json', 'text', 'text-summary', 'lcov'
            reporters: [ 'text-summary' ],
            coverageVariable: '__osoTestCoverage'
        }))
        .once('error', function() {
            process.exit(1);
        });
});

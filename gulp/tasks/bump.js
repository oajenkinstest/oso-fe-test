// Increments package.json and uses tags
// Future use - get the tag that will be applied via gradle
// and update package.json using that.

var bump = require('gulp-bump');
var gulp = require('gulp');

gulp.task('bump-release', function() {

    function validateTag(argv) {
        // semantic versioning regex, prefixed with a v
        // should be what the nebula release plugin we use
        // in gradle builds gives for a tag
        return /^v[0-9]+\.[0-9]+\.[0-9]+$/.test(argv.tag);
    }

    // defined here so every gulp task doesn't fail
    var argv = require('yargs')
                   .usage('Usage: $0 --tag vMAJOR.MINOR.PATCH')
                   .demand(['tag'])
                   .check(validateTag)
                   .argv;

    gulp.src(['./package.json'])
        .pipe(bump({ version: argv.tag.substr(1) }))
        .pipe(gulp.dest('./'));
});

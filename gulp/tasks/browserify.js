/* global err */

// Browserify bundles CommonJS modules into a format
// usable by browsers. A single file (defined in the gulp config)
// is created for us in the browser. Sourcemaps can show the browser
// the individual files for easier debugging.

// To debug, uncomment the line below before running the gulp task
// process.env.BROWSERIFYSHIM_DIAGNOSTICS=1;

var _             = require('underscore');
var browserify    = require('browserify');
var bundleLogger  = require('../util/bundleLogger');
var config        = require('../config/config');
var foreach       = require('gulp-foreach');
var gulp          = require('gulp');
var handleErrors  = require('../util/handleErrors');
var hbsfy         = require('hbsfy');
var istanbul      = require('browserify-istanbul');
var path          = require('path');
var preprocessify = require('preprocessify');
var source        = require('vinyl-source-stream');
var watchify      = require('watchify');

// Libraries will be ignored for code coverage
// Libraries that themselves do NOT use require for modules
// can be skipped when browserifying, so we list them as 2
// arrays and cat them together

// Listing a library here means it will be excluded from any build EXCEPT the vendor file
// That's why gritter isn't here, for example
var libs = [];
var libsWithoutRequire = [
    'ace',
    'brightcove',
    'has',
    'jquery',
    'jquery.cookie',
    'spin',
    'webtrends'
];
var libsWithRequire = [
    'backbone',
    'backbone-fetch-cache',
    'backbone.marionette',
    'radio.shim',
    'underscore',
    'vendor'
];

libs = libs.concat(libsWithoutRequire, libsWithRequire);

// Normally, you may not want sourcemaps when doing local
// development, but they are turned on here.
// Usually this is the only difference between dev and prod
// for this task.
var baseConfig = {
    // excludes will prevent libraries from being bundled
    excludes     : true,
    input        : [ config.APP_START ],
    jsSourcePath : config.APP_SRC_JS,
    outputName   : config.APP_BUILT_FILE,
    outputDir    : config.APP_DIST_JS,
    sourcemaps   : true
};

var devConfig = _.extend({}, baseConfig, {

    // dev: true makes preprocessify use DEV blocks rather than PROD blocks
    dev: true
});

// used to make a bundle for all library/framework pieces
// won't change nearly as much, will reduce bundle time for other files
var vendorConfig = {
    dev        : false,
    exclude    : false,
    input      : [ config.VENDOR_START ],
    jsSourcePath : config.APP_SRC_JS,
    // use this for modules that have NO require statements
    // might be more that can be added here, FYI
    noParse    : libsWithoutRequire,
    outputName : config.VENDOR_BUILT_FILE,
    outputDir  : config.APP_DIST_JS,
    sourcemaps : true,
    standalone : 'vendor'
};


//performance center PC
var widgetPCConfig = _.extend({}, baseConfig, {
    input           : [config.APP_START_WIDGET_PC],
    jsSourcePath    : config.APP_MODULES_JS_WIDGET_PC,
    widgetPC        : true
});

var widgetPCConfigDev = _.extend({}, widgetPCConfig, {
    dev: true
});

// Config for a minimal bundle just for devdata.html that just needs
// jQuery and jquery.cookie
var devdataConfig = _.extend({}, baseConfig, {
    excludes   : false,
    input      : [ config.APP_SRC_DEVDATA_FILE ],
    noParse    : libsWithoutRequire,
    outputDir  : config.APP_DIST_JS,
    outputName : config.DEVDATA_BUILT_FILE
});


// Normalize paths for modules
var generateModulePaths = function (basePath) {

    // All the places to look for modules when we require them
    // this lets us do this for a model: require('some-m')
    // instead of this: require('models/some-m')
    var moduleSuffixes = [
        './',
        '/collections',
        '/config',
        '/models',
        '/templates',
        '/views'
    ];

    return moduleSuffixes.map(function(suffix) {
        return path.join(basePath, suffix);
    });
};

// used below for repeated tasks
// run like
// generateBundle({ input: [ filename ]})
var generateBundle = function(taskConfig) {
    var bundle;
    var bundler;

    bundler = browserify({
        // Specify the entry point of your app
        debug           : taskConfig.sourcemaps,
        entries         : taskConfig.input,
        paths           : generateModulePaths(taskConfig.jsSourcePath),
        standalone      : taskConfig.standalone,
        // Required watchify args
        cache           : {},
        packageCache    : {},
        fullPaths       : false
    });

    // exclude any vendor files
    if (taskConfig.excludes === true ) {
        libs.forEach(function(lib) {
            bundler.exclude(lib);
        });
    }

    //preprocess js file based on environment
    if (taskConfig.dev) {
        bundler.transform(preprocessify({ DEV: true }));
    } else {
        if (taskConfig.widgetPC) {
            bundler.transform(preprocessify({ WIDGETPCPROD: true }));
        } else {
            bundler.transform(preprocessify({ PROD: true }));
        }
    }

    // bundle templates
    bundler.transform(hbsfy);

    bundle = function() {
        bundleLogger.start();

        return bundler.bundle()
                      .on('error', handleErrors)
                      .pipe(source(taskConfig.outputName))
                      .pipe(gulp.dest(taskConfig.outputDir))
                      .on('end', bundleLogger.end);
    };

    return bundle();
};

// Build modules for unit testing
// This task browserifies each individual file
// so that the unit tests can consume them as needed.
//
// Each file will be built as a completely self contained module, with all
// needed dependencies in place.
//
// Based on some ideas found here:
// http://justinjohnson.org/javascript/getting-started-with-gulp-and-browserify
//
gulp.task('browserify-for-testing', function() {

    var singleFileBundler = function(stream, file) {

        var filePath = file.path;
        
        var relativePath = filePath.split(config.APP_SRC_JS + path.sep)[1];
        // this will fail if you dot-namespace a file
        // ex someobj.somefile.js
        var fileName = relativePath.split(path.sep).pop().split('.').shift();
        var configObj = {
            debug      : true,
            paths      : generateModulePaths(config.APP_SRC_JS),
            standalone : fileName
        };

        var bundler = browserify(filePath, configObj);

        //preprocess all js file with TEST flag // to for the fix for testing private methods
        bundler.transform(preprocessify({ TEST: true }));

        bundler.transform(istanbul({
            instrumenterConfig: {
                coverageVariable: '__osoTestCoverage'
            },
            // exclude these paths from instrumentation
            // (make them not show up in code coverage)
            ignore: [
                '**/setup*',
                '**/src/lib/**',
                '**/*.hbs',
                '**/test/**',
                '**/vendor/**',
                '**/pages/uiTest/**',
                '**/pages/hello/**',
                '**/pages/myBusiness/**',
                '**/pages/page*/**',
                '**/utils/jquery.easypiechart.oso.js'
            ]
        }));
        bundler.transform(hbsfy);

        return bundler.bundle()
                      .pipe(source(relativePath))
                      .pipe(gulp.dest(config.APP_TEST_DIST));

    }; // singleFileBuilder

    return gulp.src(config.APP_SRC_JS + '/**/*.js')
               .pipe(foreach(singleFileBundler));

});

gulp.task('browserify-devdata', function() {
    return generateBundle(devdataConfig);
});

gulp.task('browserify-vendor', function() {
    return generateBundle(vendorConfig);
});

gulp.task('browserify-dev', ['browserify-devdata', 'browserify-vendor'], function() {
    return generateBundle(devConfig);
});

gulp.task('browserify-prod', ['browserify-devdata', 'browserify-vendor'], function() {
    return generateBundle(baseConfig);
});

gulp.task('browserify-release', ['browserify-devdata', 'browserify-vendor'], function() {
    return generateBundle(baseConfig);
});

gulp.task('browserify-widgetpc-dev', ['browserify-devdata', 'browserify-vendor'], function() {
    return generateBundle(widgetPCConfigDev);
});

gulp.task('browserify-widgetpc-release', ['browserify-devdata', 'browserify-vendor'], function() {
    return generateBundle(widgetPCConfig);
});

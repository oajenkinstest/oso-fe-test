# OneSource Online Frontend


## Getting Started

1. Install NodeJS v4.4.5 on your development machine.
2. Clone the oso-frontend repo onto your development machine.
3. In git-bash, install gulp globally: `npm install -g gulp`
4. In git-bash, run `sh scripts/checksum.sh` see *Running locally, building, etc.* section below
5. Run `gulp dev` to start the local web server.
6. Open `http://localhost:3000/` in a browser to verify that the server is working.
7. For running all of the mocha tests, see [Installing Canvas](#markdown-header-installing-canvas)

**Note:** The [localdata-dev project](https://bitbucket.org/ebus/localdata-dev) should also be running on your development machine in order to feed data to the front-end

## Gulp Tasks

* `gulp` or `gulp dev` runs the local development server


## OSO Frontend Documentation

### General

* [JavaScript Style Guide](docs/JavaScriptStyleGuide.md)
* [Code Review Checklist](docs/CodeReviewChecklist.md)
* [Content Security Policy](docs/ContentSecurityPolicy.md)

### Development Environment

* [Simulating OSO REST Service for Local Development](docs/SimulatingREST.md)
* [Directory Structure](docs/DirectoryStructure.md)
* [OSO Gulp Tasks](docs/GulpTasks.md)
* [Continuous Integration](docs/ContinuousIntegration.md)
* [Jenkins Job Configuration](docs/JenkinsJobConfiguration.md)

### Code-Specific

* [OSO Application Architecture](docs/Architecture.md)
* [App Startup Sequence](docs/StartupSequence.md)
* [Frontend Architecture for Access Control](docs/FrontendAccessControl.md)
* [Routing and Checkpoints](docs/RoutingAndCheckpoints.md)
* [Error Handling](docs/ErrorHandling.md)
* [Unit Testing Guide](docs/Testing.md)
* [Analytics / Site Metrics](docs/Analytics.md)
* [Ajax Customizations](docs/AjaxCustomizations.md)
* [Ace Template](docs/Ace.md)
* [Data Caching](docs/DataCaching.md)
* Modules
    - [User Module](docs/UserModule.md)
    - [Performance Center](docs/PerformanceCenter.md)
    - [WCM](docs/WCM.md)

### Release Process

* [Release Checklist](docs/ReleaseChecklist.md)
* [Production Deployment](docs/ProductionDeployment.md)

## ESLint

Explanations of ESLint rules are listed on the [ESLint site](http://eslint.org/docs/rules/). Each rule has a numeric value:

Value | Meaning
------|------------------------------------------------------------
   0  | This rule is not enforced.
   1  | Violating this rule results in a warning.
   2  | Violating this rule results in an error, and ESLint returns
      | an error status.

Standard ESLint options for the *development environment* are set in `.eslintrc`.

For production code, the options (found in `.eslintrc-prod`) are slightly different in that no `debugger` statements are allowed.

Test specs (`.eslintrc-test`) can have slightly longer line lengths and only give a warning for unused variables instead of an error.

Gulp tasks use `.eslintrc-node`, which for now is identical to the dev environment settings.

Files listed in `.eslintignore` are not checked by ESLint.



## Dependencies
The core libraries (currently Backbone, Marionette, Underscore, and jQuery) are all available globally. Instead of explicitly requiring them in each module like this:
```
#!javascript

var Backbone = require('backbone');
var Marionette = require('backbone.marionette');
```

We can simply add the JSHint directive to the top of the module so that usage of these libraries will not trip JSHint's flags:

```
#!javascript

/* global Backbone, Marionette */
```


Further, this allows us to create tests without having to add `require('setup');` in every module we want to test. Instead, dependencies are loaded in test/mocha-test/setup/setup.js and test/mocha-test/setup/helpers.js.


----

*This repo is based on the Reference Architecture Frontend repo. The rest of this README came from that repo.*

----

# Reference Architecture Frontend

This repo contains some basic examples of using the front end tech stack defined in [OneAmerica wiki page for Web Solutions Architecture](http://wiki.oneamerica.com/websolutions/index.php/Tech_Stacks).

Except for the code found in `src`, code in this repo should be a verison of the scaffolding that exists at [the Project Scaffolding Repo](http://bitbucket.org/ebus/project-scaffolding/nodejs).

## Shrinkwrapping

If you add a dependency via `npm install`, it is up to you to also run `npm shrinkwrap --dev` in order to build a list of dependencies that is fixed to the actual installed versions. If you do not do this, you will cause a build failure because you will not get the dependency in the build process.

The repo should come with a `npm-shrinkwrap.json` file already that has a working set of dependencies.

This will [soon be fixed in npm by https://github.com/npm/npm/issues/6928](https://github.com/npm/npm/issues/6928).

## Running locally, building, etc.

1. The first time you run the app, run `sh scripts/checksum.sh`
    - This will run `npm install` for the first time and save a checksum of `package.json` and `npm-shrinkwrap.json` to a file called `checksum.txt`.

It is suggested you run this script if you've changed branches and there may be additional modules to install.

## Gulp tasks

- Tasks are named using hyphens.
- Unless found in `default.js`, tasks always start with the name of the file they are located in.
    - Example: `lint-css` should be found in `lint.js`
- Tasks with `dev` in the name are for local development only.
- Tasks with `prod` in the name create production versions of code for use in local testing. Any task with this in the name should **ONLY** place code in `dist`.
- Tasks with `release` in the name are intended to build versions for testing and release. Any task with this in the name should **ONLY** place code in `release`.

## Testing

In a development environment, use gulp to run the unit tests. `gulp test` will bundle all of the source files into the test/dist directory, then run the Mocha unit tests against those bundles. Test results are written to the console as well as to report.xml, which Jenkins uses to show test results. In much the same way, code coverage results are also shown in the console and written to cobertura-coverage.xml for use by Jenkins.

To quickly run the tests without creating new bundles in test/dist, use `gulp test-fast`. This runs much more quickly since it skips the bundling step, but it's only useful when you're just making tweaks to the unit test specs without modifying the application source.

## Installing Canvas
In order for all mocha tests to run against JSDOM, it is required that you install canvas on your system. 

**Note**: This is **not** required. Any tests which require the canvas package will be automatically skipped if canvas is not found on the system.

### Non-Windows OS
* Follow these [installation instructions from the canvas GitHub page](https://github.com/Automattic/node-canvas#installation). 

### Windows 
* Follow the first three steps outlined in the [canvas Wiki](https://github.com/Automattic/node-canvas/wiki/Installation---Windows)
* If Visual Studio 2015 was installed, execute the following command:
`npm config set msvs_version 2015 --global`
* Run `npm install canvas -g`

If you want canvas to be installed whenever `scripts/checksum.sh` is ran, create an environment variable named `OSO_CANVAS_VERSION` and set the value as the version number of canvas you want to install.

For example, if a GitBash shell is used for development, the following command can be added to the `{HOME}/.bashrc` file:
````
export OSO_CANVAS_VERSION=1.6.9
````

This will allow the `checksum.sh` script to install `canvas` version 1.6.9.

## Possible additions to this example and the scaffolding

### Recess task for linting CSS

Recess allows enforcing rules in both LESS and CSS, and may be useful in maintaining consistency.


### Script for automatically getting latest scaffolding

Should be able to do something like:

    mv src/ srcOLD
    cd ../project-scaffolding/nodejs && git archive master | tar -x -C ../reference-archicture-frontend && mv srcOLD src

to automatically pull latest of the project scaffolding work into here.
Then copy the source code automatically over and back and test against the newest version of the code.

### Marionette templateHelper method for pulling in selectors

If we define selectors in the `config/selectors.js` file, then we should be able to automatically populate the [templateHelpers function](http://marionettejs.com/docs/marionette.view.html#viewtemplatehelpers) with the correct object for that template. So a template that currently looks like:

    <a class="ols-expcol-link">link text</a>

could change to be

    <a class="{{ SEL.collapseLink }}">link text</a>

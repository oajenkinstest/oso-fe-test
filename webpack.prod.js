/* global module:false, require:false */
/**
 * This file is merged (using webpack-merge) with the common
 * config file to create a production bundle using webpack.
 */
var commonWebpackConfig = require('./webpack.common');
var merge               = require('webpack-merge');
var UglifyJsPlugin      = require('uglifyjs-webpack-plugin');

module.exports = merge(commonWebpackConfig, {
    mode: 'production',
    output: {
        filename: '[name].min.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                // Preprocess the JS files for PROD
                test: /\.js$/,
                use: ['preprocess-loader?prod=true']
            }
        ]
    },
    plugins: [
        new UglifyJsPlugin({
            sourceMap: true,
            uglifyOptions: {
                output: {
                    comments: false
                }
            }
        })
    ]
});
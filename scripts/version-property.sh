#!/bin/sh

version=`node ./scripts/get-package-version.js`
if [ $? -ne 0 ]; then
    echo $SCRIPT_ERROR
    echo "Could not get the package.json version, exiting"
    exit 1
fi
echo "VERSION=v$version" > version.properties
echo "Build of OSO version $version on" `date` > build_version.txt
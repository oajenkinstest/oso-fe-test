#!/bin/bash

# TODO
# Better detection of success/failure and logging in the console.

# Used to clean node_modules with long path on windows.
# Both git-bash shell scripts and normal windows rmdir fail
# if the path is too long (mostly due to older versions of underscore.js).
# But calling a Windows command prompt from here allows a workaround.
# Seems like you MUST be in the correct directory when calling this, hence
# the cd and then cd back.

echo 'Windows detected, will call a DOS prompt to delete node_modules'
echo 'About to run scripts/clean-node-modules.bat via windows command prompt'
cd ./scripts
$COMSPEC /c clean-node-modules.bat
cd ..

#!/bin/bash

# checksum the package.json file
# if they don't match the checksum.txt file present locally,
# then remove node_modules and rerun npm install
# This is intended to be used on Jenkins, but could also run
# locally on developer machines.

# VARS
pkg='./package.json'
chksum='./checksum.txt'
nmFix='rm -rf node_modules'
temp='./temp.txt'

# actions that change depending on the OS
# assumes mac, linux, or windows
# http://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux
# hash: what binary to get the hash with
# on windows, assuming we have git-bash, which has md5sum

# from an npm standpoint, this is a hammer to kill a fly
# there seems to be a lot of discussion on the npm issues about similar
# topics, but for now this seems to be the safest bet
# watch this one: https://github.com/npm/npm/issues/6928
# Issue is closed, so npm@3 should resolve this
if [ "$(uname)" == "Darwin" ]; then
    # mac, -r reverses the output to match md5sum
    hash='md5 -r'
elif [ "$(uname)" == "Linux" ]; then
    #linux
    hash='md5sum'
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    # windows
    hash=md5sum
    nmFix='sh ./scripts/clean-node-modules.sh'
fi

cleanNodeModules () {
    echo 'Running scripts/checksum.sh, in cleanNodeModules.'
    $nmFix
}

runNpmInstall () {
    echo 'Running scripts/checksum.sh, in runNpmInstall.'
    npm install
}

installCanvas () {
    echo 'Running scripts/checksum.sh, in installCanvas.'
    if [ "$OSO_CANVAS_VERSION" != "" ]; then
        echo "installing canvas v${OSO_CANVAS_VERSION}..."
        npm install canvas@${OSO_CANVAS_VERSION}
    fi
}

# checksum the package.json file
# and echo it back out
createChecksum () {
    $hash $pkg | awk '{ print $1 }' > $temp
    echo `$hash $temp | awk '{ print $1 }'`
}

cleanup () {
    echo 'Running scripts/checksum.sh, in cleanup.'
    if [ -e $temp ]
    then
        rm $temp
    fi
}

# write to a file for saving
# assumes you have already put this file in .gitignore
saveChecksum () {
    echo 'Running scripts/checksum.sh, in saveChecksum.'
    createChecksum > $chksum
    echo 'Running scripts/checksum.sh, checksum created.'
    cleanup
}

# SCRIPT STARTS HERE

# first get clean
cleanup

# does package.json file exist?
if [ ! -e $pkg ]
then
    echo 'Running scripts/checksum.sh, package.json file does not exist.'
    echo 'This should be part of the repository, please run `git status`.'
    echo 'Exiting with error.'
    exit 1;
fi

# if we don't have node_modules, then install and save info
if [ ! -d "./node_modules" ]
then
    echo 'Running scripts/checksum.sh, node modules does not exist.'
    runNpmInstall
    saveChecksum
    exit
fi

# node_modules exists, does checksum file?
if [ ! -e $chksum ]
then
    echo 'Running scripts/checksum.sh, checksum file does not exist.'
    cleanNodeModules
    runNpmInstall
    saveChecksum
    exit
fi

# do the checksums match?
current=`createChecksum`
if [ "$current" != "`cat $chksum`" ]
then
    echo 'Running scripts/checksum.sh, current checksum does not match saved checksum.'
    cleanNodeModules
    runNpmInstall
    saveChecksum
    installCanvas
    exit
fi

# if you get to here, no changes needed
echo 'Running scripts/checksum.sh, nothing needed, exiting.'
echo 'You should be ok to run any gulp tasks now.'
exit


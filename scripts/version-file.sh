#!/bin/bash

# write out the results of git-describe to a version.txt file
# this file will be included in the bundled results

echo `git describe` > release/version.txt

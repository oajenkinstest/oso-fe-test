#!/bin/bash

# move any existing reports to a different name
# example: you have report.xml
# run `sh preserve-reports.sh FOO`
# and you get FOO-report.xml
# run `sh preserve-reports.sh FOO invert`
# and you get report.xml back

if [ -z "$1" ]
then
    echo "No argument supplied when calling scripts/preserve-reports.sh"
    exit 1
fi

# files that we will move
defaultHtmlReport="html-report/"
defaultReportXml="report.xml"
defaultCoberturaXml="cobertura-coverage.xml"

if [ "$2" == "invert" ]
then
    echo 'Running scripts/preserve-reports.sh, running inverted'
    targetHtmlReport=$defaultHtmlReport
    targetReportXml=$defaultReportXml
    targetCoberturaXml=$defaultCoberturaXml

    sourceHtmlReport=$1-$defaultHtmlReport
    sourceReportXml=$1-$defaultReportXml
    sourceCoberturaXml=$1-$defaultCoberturaXml
else

    echo 'Running scripts/preserve-reports.sh, running normally'
    
    sourceHtmlReport=$defaultHtmlReport
    sourceReportXml=$defaultReportXml
    sourceCoberturaXml=$defaultCoberturaXml

    targetHtmlReport=$1-$defaultHtmlReport
    targetReportXml=$1-$defaultReportXml
    targetCoberturaXml=$1-$defaultCoberturaXml
fi

if [ -d $sourceHtmlReport ]
then
    echo 'Running scripts/preserve-reports.sh, moving' $sourceHtmlReport to $targetHtmlReport
    mv -f $sourceHtmlReport $targetHtmlReport
fi

if [ -e $sourceReportXml ]
then
    echo 'Running scripts/preserve-reports.sh, moving' $sourceReportXml to $targetReportXml
    mv -f $sourceReportXml $targetReportXml
fi

if [ -e $sourceCoberturaXml ]
then
    echo 'Running scripts/preserve-reports.sh, moving' $sourceCoberturaXml $targetCoberturaXml
    mv -f $sourceCoberturaXml $targetCoberturaXml
fi


/* global require:false, module:false */
/*
 This is the default configuration file for webpack-stream used in gulp.

 The configuration options for webpack can be found at:

 https://webpack.js.org/configuration/
 */
var path                = require('path');
var gulpConfig          = require('./gulp/config/config');
var MomentLocalesPlugin = require('moment-locales-webpack-plugin');
var ShakePlugin         = require('webpack-common-shake').Plugin;

var webpackCommon = {
    entry: {
        app     : gulpConfig.APP_START,
        devdata : gulpConfig.APP_SRC_DEVDATA_FILE,
        vendor  : gulpConfig.VENDOR_START
    },
    output: {
        // A separate sourcemap will be generated using the filename with the ".map" extension
        sourceMapFilename: '[file].map'
    },
    module: {
        rules: [
            // For the following three items below ("jquery-migrate" and "datatables*"),
            // see https://github.com/jquery/jquery-migrate/issues/273
            {
                test: require.resolve('jquery-migrate'),
                use: 'imports-loader?define=>false'
            }, {
                test: require.resolve('datatables.net-bs'),
                use: 'imports-loader?define=>false'
            }, {
                test: require.resolve('datatables.net'),
                use: 'imports-loader?define=>false'
            }, {
                test: require.resolve('jquery'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    }, {
                        loader: 'expose-loader',
                        options: '$'
                    }
                ]
            }, {
                test: /\.hbs$/,
                use: ['injectify']
            }
        ]
    },
    resolve: {
        alias: {
            // Provide an alias for underscore. If this isn't set, dependencies like
            // Backbone.Radio will import their own underscore sub-dependency.
            underscore : path.resolve(__dirname, 'node_modules/underscore/underscore.js')
        }
    },
    plugins : [
        // By default, this will keep the 'en' locale
        // See https://www.npmjs.com/package/moment-locales-webpack-plugin#usage
        new MomentLocalesPlugin(),
        new ShakePlugin()
    ]
};

module.exports = webpackCommon;
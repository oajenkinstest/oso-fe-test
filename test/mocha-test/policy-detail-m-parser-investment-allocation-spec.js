/* global expect:false, $:false, expect:false */

var PolicyInvestmentAllocationParser = 
    require('../dist/pages/policy/models/parsers/policy-investment-allocation');
var helpers = require('./helpers/helpers');

describe('Policy Detail Model parser - Investment Allocation ' +
    '(pages/policy/models/parsers/policy-investment-allocation.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
    var parser = new PolicyInvestmentAllocationParser();


    describe('_setIndexedAnnuityState method', function () {

        it('exists as a function', function () {
            expect(parser._setIndexedAnnuityState).to.exist.and.be.a('function');
        });

        it('should not add flag when indexed annuity is NOT identified', function () {
            parser._setIndexedAnnuityState(policyDataCopy);

            expect(policyDataCopy.isIndexedAnnuity).to.be.undefined;
        });

        it('should add a flag and set it to TRUE when indexed annuity is identified', function () {
            policyDataCopy.product.productTypeCode = 'INXAN';

            parser._setIndexedAnnuityState(policyDataCopy);

            expect(policyDataCopy.isIndexedAnnuity).to.be.true;
        });

    }); // _setIndexedAnnuityState method

});

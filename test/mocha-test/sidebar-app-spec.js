/* global expect:false */

var Sidebar = require('../dist/apps/sidebar/index');

var menuArray = [
    {
        icon        : 'tachometer',
        displayText : 'Home',
        link        : 'home',
        view        : 'pages/home/views/home-v'
    },
    {
        displayText : 'Products',
        link        : 'products',
        view        : 'pages/products/views/products-v'
    },
    {
        icon        : 'list',
        displayText : 'My Business',
        subItems: [
            {
                displayText           : 'Pending Policy Search',
                link                  : 'pending-search',
                view                  : 'pages/policy/views/pending-search-page-v',
                defaultMyBusinessPage : true,
                activeFor             : [
                    'pending-policies',
                    'pending-policy-manager',
                    'pending-policy-detail'
                ]
            }
        ]
    }
];

var newMenuArray = [
    {
        icon        : 'home',
        displayText : 'Home',
        link        : 'home',
        view        : 'pages/home/views/home-v'
    }, {
        icon        : 'hotel',
        displayText : 'Away',
        link        : 'away',
        view        : 'pages/products/views/products-v'
    }
];

describe('Sidebar Marionette Application (apps/sidebar/index.js)', function () {
    var sidebarApp;

    it('exists', function () {
        expect(Sidebar).to.exist;
    });

    it('can be instantiated', function () {
        sidebarApp = new Sidebar({});
        expect(sidebarApp).not.to.be.undefined;

        sidebarApp = null;
    });

    describe('Running sidebar tests', function() {

        beforeEach(function() {
            sidebarApp = new Sidebar({
                container   : 'body',
                menu        : menuArray
            });
            sidebarApp.start();
        });

        afterEach(function() {
            sidebarApp = null;
        });

        it('inserts the sidebar into the DOM on app start', function () {
            expect(document.querySelector('#__OA_SIDEBAR__')).not.to.be.null;
        });

        it('clicking a sidebar item triggers "nav" event', function () {
            var navCallback = this.sinon.spy();
            sidebarApp.on('nav', navCallback);

            // Simulate a click on a sidebar link
            document.querySelector('#__OA_SIDEBAR__')
                .querySelector('a')
                .click();

            expect(navCallback).to.have.been.calledOnce;

            sidebarApp.off('nav');
        });

        it('setActiveItem method should set active value correctly', function () {
            expect(sidebarApp.setActiveItem).to.exist;

            sidebarApp.setActiveItem('#products');
            expect(sidebarApp.menuRegion.currentView.collection.at(1).get('active')).to.equal(true);

            sidebarApp.setActiveItem('#home');
            expect(sidebarApp.menuRegion.currentView.collection.at(1).get('active'))
                .to.equal(false);

        });

        it('setActiveItem method should use "activeFor" property to set active flag '+
                'if pageId is "#pending-policy-detail" the active flag should set for '+
                '"pending-search" pageId',
                function () {
            sidebarApp.setActiveItem('#pending-policy-detail');
            expect(sidebarApp.menuRegion.currentView.collection
                    .at(2).childItems.models[0].get('active')).to.equal(true);

            expect(sidebarApp.menuRegion.currentView.collection
                    .at(2).childItems.models[0].get('link')).to.equal('pending-search');

        });

        it('setActiveItem method should throw error if pageId not '+
            'exist or not matching', function () {

            var fn;

            fn = function () {
                sidebarApp.setActiveItem();
            };

            expect(fn)
                .to.throw('Sidebar setActiveItem: \'pageId\' parameter must be a string');

            fn = function () {
                sidebarApp.setActiveItem('#pageidnotexist');
            };

            expect(fn)
                .to.throw('Sidebar setActiveItem: Could not find a menu item '+
                'with pageId "pageidnotexist"');

            fn = null;
        });

        it('Can reinitialize the menu', function() {

            // Menu should start with 3 top-level items
            var menuSize = sidebarApp.menuRegion.currentView.collection.size();
            expect(menuSize).to.equal(3);

            // After re-initializing, it should have 2 top-level items.
            sidebarApp.reinitializeMenu(newMenuArray);
            menuSize = sidebarApp.menuRegion.currentView.collection.size();
            expect(menuSize).to.equal(2);
        });
    });
});

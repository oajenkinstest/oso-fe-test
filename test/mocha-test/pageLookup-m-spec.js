/* global describe:false, expect:false, $:false */

var PageLookupModel = require('../dist/models/pageLookup-m');

var testAppStructure = [
    {
        icon        : 'home',
        capability  : {
            forSidebar : [{
                all : true
            }]
        },
        displayText : 'Home',
        link        : 'home',
        view        : 'view',
        activeFor   : [
            'error'
        ]
    }, {
        icon        : 'info-circle',
        displayText : 'FAQs & Resources',
        subItems: [
            {
                capability: {
                    forSidebar : [{
                        'user.products' : true
                    }]
                },
                displayText : 'Products',
                link        : 'products',
                view        : 'view'
            }, {
                displayText: 'Page One',
                subItems: [
                    {
                        capability: {
                            forSidebar : [{
                                'user.pageonea' : true
                            }]
                        },
                        icon        : 'arrow-right',
                        displayText : 'Page 1A',
                        link        : 'pageone-1a',
                        view        : 'view'
                    }, {
                        capability: {
                            forSidebar : [{
                                'user.pageoneb' : true
                            }]
                        },
                        icon        : 'arrow-right',
                        displayText : 'Page 1B',
                        link        : 'pageone-1b',
                        view        : 'view'
                    }
                ]
            }
        ]
    }, {
        icon        : 'tachometer',
        capability  : {
            forSidebar : [{
                'user.hello' : true
            }]
        },
        displayText : 'Hello',
        link        : 'hello',
        view        : 'view'
    }, {
        icon        : 'tachometer',
        capability  : {
            forPageAccess : {
                'all' : true
            }
        },
        displayText : 'Error',
        link        : 'error',
        view        : 'view'
    }
];

describe('Page Lookup Model (models/pageLookup-m.js)', function () {

    it('exists', function () {
        expect(PageLookupModel).to.exist;
    });

    describe('Initialization', function () {
        var modelInstance;
        var invalidAppStructure = {badAppStructure : true};

        it('throws an error when options are not passed', function () {
            var fn = function () {
                modelInstance = new PageLookupModel();
            };

            expect(fn).to.throw(PageLookupModel.prototype.errors.appStructure);
        });

        it('throws an error when passed an invalid appStructure array', function () {
            var fn = function () {
                modelInstance = new PageLookupModel(invalidAppStructure);
            };

            expect(fn).to.throw(PageLookupModel.prototype.errors.appStructure);
        });

        it('initializes successfully when a valid appStructure is passed', function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });

            expect(modelInstance).to.exist;
        });
    }); // Initialization

    describe('isValidPage method', function() {
        var modelInstance;
        var invalidPageId = '#invalidpage';

        beforeEach(function () {
            modelInstance = new PageLookupModel({
                appStructure  : testAppStructure
            });
        });

        afterEach(function () {
            modelInstance = null;
        });

        it('exists', function () {
            expect(modelInstance.isValidPage).to.be.a('function');
        });

        it('returns false if an invalid pageId is passed', function() {
            var result = modelInstance.isValidPage(invalidPageId);
            expect(result).to.be.false;
        });

        it('returns true if a valid pageId which exists in the structure is passed',
            function () {
                var validPageId = 'products';
                var result = modelInstance.isValidPage(validPageId);
                expect(result).to.be.true;
            });
    }); // isValidPage method

    describe('userCanAccessPage method', function () {
        var modelInstance;
        var singleAppStructure = [
            {
                link       : 'testLink',
                capability : {
                    forSidebar :  [{
                        'limited-access' : true
                    }]
                },
                view       : 'test-link-view'
            }
        ];
        var pageId = 'testLink';
        var limitedCapabilities = ['limited-access'];

        beforeEach(function () {
            modelInstance = new PageLookupModel({
                appStructure  : singleAppStructure
            });
        });

        afterEach(function () {
            modelInstance = null;
        });

        it('exists as a function', function () {
            expect(modelInstance.userCanAccessPage).to.be.a('function');
        });

        it('returns false if user lacks the required capability', function () {
            var testCapabilities = [];
            var result = modelInstance.userCanAccessPage(pageId, testCapabilities);

            expect(result).to.be.false;
        });

        it('returns false if `page.capability` does not exist', function () {
            var noCapabilityStructure = [$.extend({}, true, singleAppStructure[0])];
            var result;

            // delete the capability property
            delete noCapabilityStructure[0].capability;

            modelInstance = new PageLookupModel({
                appStructure : noCapabilityStructure
            });

            result = modelInstance.userCanAccessPage(pageId, limitedCapabilities);

            expect(result).to.be.false;
        });

        it('returns true if the user has the required capability', function () {
            var result = modelInstance.userCanAccessPage(pageId, limitedCapabilities);

            expect(result).to.be.true;
        });

        describe('forSidebar property should consider to provide access to a page', function () {
            var menuAccessPropStruct = [
                {
                    link : 'testLinkHome',
                    displayText : 'Test Home Link',
                    capability : {
                        forSidebar : [
                            {
                                capabilityD: true
                            }
                        ]
                    },
                    activeFor : [
                        'testLink'
                    ],
                    view : 'test-link-home-view'
                },
                {
                    link       : 'testLink',
                    capability : {
                        forSidebar : [{
                            capabilityA : true,
                            capabilityB : true,
                            capabilityC : false,
                        }],
                        forPageAccess :  {
                            'limited-access' : true
                        }
                    },
                    view       : 'test-link-view'
                },
                {
                    link       : 'testLinkNoParent',
                    capability : {
                        forPageAccess :  {
                            'limited-access' : true
                        }
                    },
                    view       : 'test-link-view'
                }
            ];
            var hasAccess;

            beforeEach(function () {
                modelInstance = new PageLookupModel({
                    appStructure  : menuAccessPropStruct
                });
            });

            afterEach(function () {
                modelInstance = null;
            });

            it('To be True', function () {
                hasAccess = modelInstance.userCanAccessPage(pageId, [
                    'capabilityA',
                    'capabilityB'
                ]);

                expect(hasAccess).to.be.true;
            });

            it('To be false', function () {
                hasAccess = modelInstance.userCanAccessPage(pageId, [
                    'capabilityC'
                ]);

                expect(hasAccess).to.be.false;
            });

            it('Considering pageAccess property secondly and '+
                'it has mapping in "activeFor" property of parent menu item', function () {
                hasAccess = modelInstance.userCanAccessPage(pageId, [
                    'limited-access',
                    'capabilityD'
                ]);

                expect(hasAccess).to.be.true;
            });

            it('To be false if pageId is not mapped to any of parent menu item', function () {
                hasAccess = modelInstance.userCanAccessPage('testLinkNoParent', [
                    'limited-access',
                    'capabilityD'
                ]);

                expect(hasAccess).to.be.false;
            });
        });
    }); // userCanAccessPage method

    describe('_removeHashPrefix method', function () {
        var modelInstance;
        var leadingHash = '#leading-hash';
        var containsHash = 'contains#';
        var noHash = 'no-hash';

        beforeEach(function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });
        });

        afterEach(function () {
            modelInstance = null;
        });

        describe('when passed a string containing a hash character', function () {

            it('will return a string with a hash as the first character', function () {
                var result = modelInstance._removeHashPrefix(leadingHash);
                expect(result).to.equal(leadingHash.substr(1));
            });

            it('returns the string passed to it when the hash is NOT the first character ' +
                'in the string', function () {
                var result = modelInstance._removeHashPrefix(containsHash);
                expect(result).to.equal(containsHash);
            });

            it('returns the string passed to it when no hash character is present ' +
                'in the string', function () {
                var result = modelInstance._removeHashPrefix(noHash);
                expect(result).to.equal(noHash);
            });
        });
    }); // _removeHashPrefix method

    describe('_buildPageLookup method', function () {
        var modelInstance;
        var expectedPageLookup = {
            home : {
                icon        : 'home',
                capability  : {
                    forSidebar : [{
                        all : true
                    }]
                },
                displayText : 'Home',
                view        : 'view',
                activeFor   : [
                    'error'
                ]
            },
            products : {
                capability: {
                    forSidebar :  [{
                        'user.products' : true
                    }]
                },
                displayText : 'Products',
                view        : 'view'
            },
            'pageone-1a' : {
                capability: {
                    forSidebar : [{
                        'user.pageonea' : true
                    }]
                },
                icon        : 'arrow-right',
                displayText : 'Page 1A',
                view        : 'view'
            },
            'pageone-1b' : {
                capability: {
                    forSidebar : [{
                        'user.pageoneb' : true
                    }]
                },
                icon        : 'arrow-right',
                displayText : 'Page 1B',
                view        : 'view'
            },
            hello : {
                icon        : 'tachometer',
                capability: {
                    forSidebar : [{
                        'user.hello' : true
                    }]
                },
                displayText : 'Hello',
                view        : 'view'
            },
            error : {
                icon        : 'tachometer',
                capability  : {
                    forPageAccess : {
                        'all' : true
                    }
                },
                displayText : 'Error',
                view        : 'view' 
            }
        };

        it('creates a flattened object based on the appStructure passed on initialization',
            function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });

            var pageLookup = modelInstance.get('structure');

            expect(pageLookup).to.deep.equal(expectedPageLookup);
        });

        it('contains "capability" data for each pageId', function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });

            var pageLookup = modelInstance.get('structure');

            // iterate through the object
            for (var key in pageLookup) {
                if (key.hasOwnProperty('capability')) {
                    expect(key).to.have.property('capability');
                }
            }
        });

        it('contains "view" data for each pageId', function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });

            var pageLookup = modelInstance.get('structure');

            // iterate through the object
            for (var key in pageLookup) {
                if (key.hasOwnProperty('view')) {
                    expect(key).to.have.property('view');
                }
            }
        });

        it('contains "display" data for each pageId', function () {
            modelInstance = new PageLookupModel({
                appStructure : testAppStructure
            });

            var pageLookup = modelInstance.get('structure');

            // iterate through the object
            for (var key in pageLookup) {
                if (key.hasOwnProperty('display')) {
                    expect(key).to.have.property('display');
                }
            }
        });
    }); // _buildPageLookup method

    describe('_hasCapabilitiesForPageAccess method', function () {
        var model;
        var capabilities        = ['Nunchuck', 'Sword', 'Other_Skills'];
        var forSidebarAccessObj = {
            displayText : 'Home',
            capability  : {
                forSidebar :[
                    {
                        'all' : true
                    }
                ]
            },
            icon : 'fa fa-home'
        };

        var forPageAccessObj = {
            displayText : 'Home',
            capability  : {
                forPageAccess :{
                    'Vote_For_Pedro'  : true,
                    'Nunchuck'        : true
                }
            },
            icon : 'fa fa-home'
        };


        before(function () {
            model = new PageLookupModel({
                appStructure : testAppStructure
            });
        });

        after(function () {
            model = null;
        });

        it('exists as a function', function () {
            expect(model._hasCapabilitiesForPageAccess).to.be.a('function');
        });

        it('will return false if `capability.forPageAccess` ' +
            'does not exist for the pageObj param', function () {
            var result = model._hasCapabilitiesForPageAccess(forSidebarAccessObj, capabilities);

            expect(result).to.be.false;
        });

        it('will return true if the `forPageAccess` object has "all" property', function () {
            var copyOfForPageAccess = $.extend({}, true, forPageAccessObj);
            var result;

            copyOfForPageAccess.capability.forPageAccess = {
                'all' : true
            };

            result = model._hasCapabilitiesForPageAccess(copyOfForPageAccess, capabilities);

            expect(result).to.be.true;
        });

        it('will return true if one of the properties in `forPageAccess` ' +
            'matches a value in the capabilities parameter', function () {
            var result = model._hasCapabilitiesForPageAccess(forPageAccessObj, capabilities);

            expect(result).to.be.true;
        });

        it('will return false if none of the properties in `forPageAccess` ' +
            'matches any values in the capabilities parameter', function () {
            var copyOfForPageAccess = $.extend({}, true, forPageAccessObj);
            var result;

            copyOfForPageAccess.capability.forPageAccess = {
                'Access_Top_Secret'       : true,
                'Access_Above_Top_Secret' : true
            };

            result = model._hasCapabilitiesForPageAccess(copyOfForPageAccess, capabilities);

            expect(result).to.be.false;
        });

    }); // _hasCapabilitiesForPageAccess method

    describe('_hasCapabilitiesForSidebar method', function () {
        var model;
        var capabilities        = ['Nunchuck', 'Sword', 'Other_Skills'];
        var forSidebarAccessObj = {
            displayText : 'Home',
            capability  : {
                forSidebar : [
                    {
                        'Nunchuck'       : true,
                        'Sword'          : true,
                        'Something_Else' : false
                    }, {
                        'Foo1' : true,
                        'Foo2' : true
                    }
                ]
            },
            icon        : 'fa fa-home'
        };

        var forPageAccessObj = {
            displayText : 'Home',
            capability  : {
                forPageAccess : {
                    'Vote_For_Pedro' : true,
                    'Nunchuck'       : true
                }
            },
            icon        : 'fa fa-home'
        };

        before(function () {
            model = new PageLookupModel({
                appStructure : testAppStructure
            });
        });

        after(function () {
            model = null;
        });

        it('should exist as a function', function () {
            expect(model._hasCapabilitiesForSidebar).to.be.a('function');
        });

        it('should return false if capability.forSidebar does not exist ' +
            'on the pageObj parameter passed in', function () {
            var result = model._hasCapabilitiesForSidebar(forPageAccessObj, capabilities);

            expect(result).to.be.false;
        });

        it('should return true if the "all" property is set on one ' +
            'of the "forSidebar" elements', function () {
            var copyOfForSidebar = {
                displayText : 'Home',
                capability  : {
                    forSidebar : [
                        {
                            all : true
                        }
                    ]
                },
                icon        : 'fa fa-home'
            };
            var result = model._hasCapabilitiesForSidebar(copyOfForSidebar, capabilities);

            expect(result).to.be.true;
        });

        it('should return true if one of the objects in the "forSidebar" ' +
            'array matches all of the conditions', function () {
            var result = model._hasCapabilitiesForSidebar(forSidebarAccessObj, capabilities);

            expect(result).to.be.true;
        });

        it('should return false if one of the objects in the "forSidebar" ' +
            'array does not match exactly', function () {
            var result;
            // Create a new array and add the "Something_Else" capability which is
            // explicitly set to FALSE on the forSidebar array's first element.
            var newCapabilities = capabilities.slice();
            newCapabilities.push('Something_Else');

            result = model._hasCapabilitiesForSidebar(forSidebarAccessObj, newCapabilities);

            expect(result).to.be.false;
        });

    }); // _hasCapabilitiesForSidebar method
});

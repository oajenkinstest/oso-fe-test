/* global expect:false, Backbone:false, isCanvasLoaded:false, Marionette:false, _:false */

var DialChartView = require('../dist/modules/dialChart/views/dial-chart-v');

var dialSingleOptions = {
    chartType: 'single',
    outerValue:5,
    classLabelOne: 'chart-text-big',
    classLabelTwo: 'chart-text-label',
    textLabelOne: 60,
    textLabelTwo: 'Life Lives',
    checkMark: false,
    chartOptions: {
        maxValue:200
    }
};

var dialDoubleOptions = {
    chartType: 'double',
    outerValue:150,
    innerValue:100,
    classLabelOne: 'chart-double-text-one',
    classLabelTwo: 'chart-double-text-two',
    textLabelOne: '$23,998',
    textLabelTwo: '$24,544',
    checkMark: false,
    outerChartOptions: {
        maxValue:200,
        scaleColor: '#ffffff'
    },
    innerChartOptions: {
        maxValue:200,
        size:135,
        scaleColor: '#ffffff'
    }
};

describe('Dial Chart View (modules/dialChart/views/dial-chart-v.js)', function() {

    var dialChartView;
    
    before(function(){
        dialChartView = new DialChartView(dialSingleOptions);
    });

    after(function(){
        dialChartView.destroy();
    });

    it('exists', function () {
        expect(DialChartView).to.exist;
    });

    it('is a Backbone view', function () {   
        expect(dialChartView).to.be.an.instanceof(Backbone.View);
    });

    it('has dial ui elements', function () {
        expect(dialChartView.ui.chartOuter).to.exist;
        expect(dialChartView.ui.chartInner).to.exist;
    });

    describe('initialize method', function () {

        it('exists as a function', function () {
            expect(dialChartView.initialize).to.exist.and.be.a('function');
        });

        it('throws error if \'options\' is invalid', function () {
            var fn = function () {
                dialChartView = new DialChartView();
            };
            expect(fn).to.throw('dialChartModule view.initialize: options is empty or undefined');
        });
    }); // initialize method

    describe('onRender method', function () {

        describe('single dial charts', function() { 

            var chartElement;
            var root;

            beforeEach( function () {
                // if canvas does not exist in the global object, skip this suite
                if (!isCanvasLoaded) {
                    this.skip();
                }

                root = new Marionette.LayoutView({
                    template: _.template('<div id=\'dial-component-region\' ></div>'),
                    regions: {
                        dialRegion: '#dial-component-region'
                    }
                });
                root.render();
                dialChartView = new DialChartView(dialSingleOptions);
                root.dialRegion.show(dialChartView);
                chartElement = root.getChildView('dialRegion').$el.find('.chart-outer');
            });

            afterEach( function () {
                root.destroy();
                dialChartView.destroy();
            });

            it('contains one canvas element', function () {
                expect(chartElement.find('canvas')).to.have.length(1);
            });

            it('has correct percent value', function () {
                expect(chartElement.data('percent')).to.equal(dialSingleOptions.outerValue);
            });

            it('has correct label values', function () {
                expect(Number(chartElement.find('.chart-text-big').text()))
                    .to.equal(dialSingleOptions.textLabelOne);

                expect(chartElement.find('.chart-text-label').text())
                    .to.equal(dialSingleOptions.textLabelTwo);
            });

            it('if checkMark property is true, shows the checkmark element and hides the labels',
                function () {
                dialSingleOptions.checkMark = true;
                dialChartView = new DialChartView(dialSingleOptions);
                root.dialRegion.show(dialChartView);
                chartElement = root.getChildView('dialRegion').$el.find('.chart-outer');

                expect(chartElement.find('.fa-check')).to.have.length(1);

                expect(chartElement.find('.chart-text-big')).to.have.length(0);
                expect(chartElement.find('.chart-text-label')).to.have.length(0);
            });

        }); // single dial charts

        describe('double dial charts', function() { 

            var chartOuterElement;
            var chartInnerlement;
            var root;

            beforeEach(function () {
                // if canvas does not exist in the global object, skip this suite
                if (!isCanvasLoaded) {
                    this.skip();
                }

                root = new Marionette.LayoutView({
                    template: _.template('<div id=\'dial-component-region\' ></div>'),
                    regions: {
                        dialRegion: '#dial-component-region'
                    }
                });
                root.render();
                dialChartView = new DialChartView(dialDoubleOptions);
                root.dialRegion.show(dialChartView);
                chartOuterElement = root.getChildView('dialRegion').$el.find('.chart-outer');
                chartInnerlement = root.getChildView('dialRegion').$el.find('.chart-inner');
            });

            afterEach(function () {
                root.destroy();
                dialChartView.destroy();
            });

            it('contain two canvas elements', function () {
                expect(root.dialRegion.$el.find('canvas')).to.have.length(2);
            });

            it('have correct percent values (outer and inner)', function () {
                expect(chartOuterElement.data('percent')).to.equal(dialDoubleOptions.outerValue);
                expect(chartInnerlement.data('percent')).to.equal(dialDoubleOptions.innerValue);
            });

            it('have correct label values', function () {
                expect(chartOuterElement.find('.chart-double-text-one').text())
                    .to.equal(dialDoubleOptions.textLabelOne);

                expect(chartOuterElement.find('.chart-double-text-two').text())
                    .to.equal(dialDoubleOptions.textLabelTwo);
            });

            it('if checkMark property is true, shows the checkmark element and hides the labels',
                function () {

                dialDoubleOptions.checkMark = true;
                dialChartView = new DialChartView(dialDoubleOptions);
                root.dialRegion.show(dialChartView);
                chartOuterElement = root.getChildView('dialRegion').$el.find('.chart-outer');

                expect(chartOuterElement.find('.fa-check')).to.have.length(1);

                expect(chartOuterElement.find('.chart-double-text-one')).to.have.length(0);
                expect(chartOuterElement.find('.chart-double-text-two')).to.have.length(0);
            });

        }); // double dial charts
    }); // onRender method
});



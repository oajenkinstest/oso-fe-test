/* global expect:false, Backbone:false */

var DialChartModel = require('../dist/modules/dialChart/models/dial-chart-m');

describe('Dial Chart Model (modules/dialChart/models/dial-chart-m.js)', function() {

    var dialChartModel;

    var chartOptions = {
        animate: {
            enabled: false,
            duration: 2500
        },
        barColor: '#6693bc',
        circlePercentage: 75,
        easing: 'easeOutCirc',
        lineCap: 'butt', 
        lineWidth: 20,
        rotate: 225,
        scaleColor: '#777', 
        scaleLength: 3, 
        size:190,
        trackColor: '#d9d9d9',
        maxValue: 20000
    };

    it('exists', function () {
        expect(DialChartModel).to.exist;
    });

    it('can be instantiated', function () {
        dialChartModel = new DialChartModel();
        expect(dialChartModel).to.be.an.instanceof(Backbone.Model);
    });

    it('throws an error if \'outervalue\' '+
            ' is missing for config \'chartType\' with single', function () {
        var fn = function () {
            dialChartModel = new DialChartModel({
                chartType:'single'
            });
        };
        expect(fn).to.throw('dialChartModel.initialize: expected outerValue '+
            'as chartType is single. outerValue should be a number');
    });


    it('throws an error if \'maxValue\' '+
            ' is missing for config \'chartType\' with single', function () {
        var chartOptionsExtended = Backbone.$.extend(true,{}, chartOptions);
        chartOptionsExtended.maxValue = '';

        var fn = function () {
            dialChartModel = new DialChartModel({
                chartType:'single',
                outerValue:1000,
                chartOptions:chartOptionsExtended
            });
        };
        expect(fn).to.throw('dialChartModel.initialize: expected maxValue '+
            'for chartOptions as chartType is single. maxValue should be a number.');
    });

    it('throws an error if \'outervalue\' and '+
            '\'innerValue\' are missing for config \'chartType\' with double', function () {
        var fn = function () {
            dialChartModel = new DialChartModel({
                chartType:'double',
                outerChartOptions:chartOptions,
                innerChartOptions:chartOptions
            });
        };
        expect(fn).to.throw('dialChartModel.initialize: expected outerValue. '+
            'and innerValue as chartType is double. '+
            'Both should be number.');
    });

    it('throwa an error if \'maxValue\' '+
            'is missing for config \'chartType\' with double', function () {
        var innerChartOptions = Backbone.$.extend(true,{}, chartOptions);
        innerChartOptions.maxValue = '';
        var fn = function () {
            dialChartModel = new DialChartModel({
                chartType:'double',
                outerValue:200,
                innerValue:100,
                outerChartOptions:chartOptions,
                innerChartOptions:innerChartOptions
            });
        };
        expect(fn).to.throw('dialChartModel.initialize: expected maxValue '+
            'for outerChartOptions and innerChartOptions as chartType is double. '+
            'maxValue should be a number.');
    });

    it('throws an error if \'outerChartOptions\' and '+
            '\'innerChartOptions\' are missing for config \'chartType\' with double', function () {
        var fn = function () {
            dialChartModel = new DialChartModel({
                chartType:'double',
                outerValue:200,
                innerValue:100
            });
        };
        expect(fn).to.throw('dialChartModel.initialize: expected outerChartOptions '+
            'and innerChartOptions as chartType is double');
    });

    it('can be instantiated with starting values', function () {
        dialChartModel = new DialChartModel({
            chartType:'single',
            checkMark: true,
            outerValue:1000,
            chartOptions: chartOptions
        });
        expect(dialChartModel.get('chartType')).to.equal('single');
        expect(dialChartModel.get('checkMark')).to.equal(true);
        expect(dialChartModel.get('chartOptions')).to.deep.equal(chartOptions);
    });

    it('returns attribute \'outerChartOptions\'', function () {
        var outerChartOptions = Backbone.$.extend(true,{}, chartOptions);
        outerChartOptions.animate.enabled = true;

        dialChartModel = new DialChartModel({
            chartType:'double',
            checkMark: false,
            outerValue:1000,
            innerValue:500,
            outerChartOptions: outerChartOptions,
            innerChartOptions: chartOptions
        });

        expect(dialChartModel.has('outerChartOptions')).to.equal(true);
        expect(dialChartModel.get('outerChartOptions')).to.deep.equal(outerChartOptions);
    });

    it('returns attribute \'innerChartOptions\'', function () {
        var innerChartOptions = Backbone.$.extend(true,{}, chartOptions);
        innerChartOptions.animate.enabled = true;

        dialChartModel = new DialChartModel({
            chartType:'double',
            checkMark: true,
            outerValue:1000,
            innerValue:500,
            outerChartOptions: chartOptions,
            innerChartOptions: innerChartOptions
        });

        expect(dialChartModel.has('innerChartOptions')).to.equal(true);
        expect(dialChartModel.get('innerChartOptions')).to.deep.equal(innerChartOptions);
    });

    it('barColor is green if checkMark is true', function () {
        dialChartModel = new DialChartModel({
            chartType:'double',
            checkMark: true,
            outerValue:1000,
            innerValue:500,
            outerChartOptions: chartOptions,
            innerChartOptions: chartOptions
        });

        expect(dialChartModel.get('outerChartOptions').barColor).to.equal('#006347');
        expect(dialChartModel.get('innerChartOptions').barColor).to.equal('#006347');
    });

    it('if \'outervalue\' is negative, barColor is red '+
            'and value of \'outerValue\' should be set as absolute', function () {
        var innerChartOptions = Backbone.$.extend(true,{}, chartOptions);

        dialChartModel = new DialChartModel({
            chartType:'double',
            outerValue:-2000,
            innerValue:500,
            outerChartOptions: chartOptions,
            innerChartOptions: innerChartOptions
        });

        expect(dialChartModel.get('outerChartOptions').barColor).to.equal('#af1732');
        expect(dialChartModel.get('outerValue')).to.equal(2000);
    });

    it('inner chart size defaults to 135', function () {
        var innerChartOptions = Backbone.$.extend(true,{}, chartOptions);
        innerChartOptions.size = undefined;

        dialChartModel = new DialChartModel({
            chartType:'double',
            outerValue:2000,
            innerValue:500,
            outerChartOptions: chartOptions,
            innerChartOptions: innerChartOptions
        });

        expect(dialChartModel.get('innerChartOptions').size).to.equal(135);
    });

});

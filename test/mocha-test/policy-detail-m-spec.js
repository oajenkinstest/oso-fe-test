/* global expect:false, Backbone:false, $:false, expect:false */

var PolicyDetailModel = require('../dist/pages/policy/models/policy-detail-m');
var helpers           = require('./helpers/helpers');

describe('Policy Detail Model ' +
    '(pages/policy/models/policy-detail-m.js)', function () {

    var ajaxStub;
    var model;
    var policyDataCopy;

    it('model object exists', function () {
        expect(PolicyDetailModel).to.exist;
    });

    it('url is correct after model is instantiated with only an id', function () {
        model = new PolicyDetailModel({
            id:1000001
        });

        expect(model.url()).to.equal('/api/oso/secure/rest/policies/1000001');
    });

    it('url is correct after model is instantiated with only a caseId', function () {
        model = new PolicyDetailModel({
            caseId: 100002
        });

        expect(model.url()).to.equal('/api/oso/secure/rest/policies?caseId=100002');
    });

    it('throws error if \'id\' or \'caseId\' is missing while fetching data' , function () {
        var fn = function () {
            model = new PolicyDetailModel();
            model.fetch();
        };

        expect(fn).to.throw('PolicyDetailModel.urlRoot: missing \'id\' or \'caseId\'');
    });

    describe('_getActivePolicyPaidStatus method', function () {
        before (function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDataCopy
            );
            model = new PolicyDetailModel({
                id:1222312312
            });
            model.fetch();
        });

        beforeEach(function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
        });

        after ( function () {
            ajaxStub.restore();
        });

        it('exists as a function', function () {
            expect(model._getActivePolicyPaidStatus).to.exist.and.be.a('function');
        });

        it('should return paidStatus flag objects (policy not active and '+
                'paid date is not available)', function () {
            policyDataCopy.policyStatus.acordHoldingStatus = 'Proposed';
            policyDataCopy.application.paid = null;
            var paidStatus = model._getActivePolicyPaidStatus(policyDataCopy);
            expect(paidStatus.withinDisplayPeriod).to.be.false;
            expect(paidStatus.afterDisplayPeriod).to.be.false;
        });

        it('should return paidStatus flag objects (policy active and '+
                'paid date with in after 92 days)', function () {
            policyDataCopy.application.paid = '2016-05-19T21:14:00Z';
            var paidStatus = model._getActivePolicyPaidStatus(policyDataCopy);

            expect(paidStatus.withinDisplayPeriod).to.be.false;
            expect(paidStatus.afterDisplayPeriod).to.be.true;
        });

        it('should return paidStatus flag objects (policy active and '+
                'paid date with in within 92 days)', function () {

            // get date which is 50 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 50);
            var paidStatus = model._getActivePolicyPaidStatus(policyDataCopy);

            expect(paidStatus.withinDisplayPeriod).to.be.true;
            expect(paidStatus.afterDisplayPeriod).to.be.false;
        });

        it('should return paidStatus flag objects (acordHoldingStatus is Inactive and '+
                'paid date with in within 92 days)', function () {

            policyDataCopy.policyStatus.acordHoldingStatus = 'Inactive';
            // get date which is 50 days behind
            var paidStatus = model._getActivePolicyPaidStatus(policyDataCopy);

            expect(paidStatus.withinDisplayPeriod).to.be.false;
            expect(paidStatus.afterDisplayPeriod).to.be.true;
        });
    }); // _getActivePolicyPaidStatus method

    describe('_getLinks method', function () {
        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax');
            model = new PolicyDetailModel({
                id:1222312312
            });
        });
        beforeEach (function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );
        });

        after ( function () {
            ajaxStub.restore();
        });
        it('exists as a function', function () {
            expect(model._getLinks).to.exist.and.be.a('function');
        });

        it('should return model object if _link property exist', function () {
            var links;
            model.fetch();
            links = model._getLinks();
            expect(links).to.be.an.instanceof(Backbone.Model);
            expect(links.get('application-document'))
                .to.deep.equal(policyDataCopy._links['application-document']);
        });

        it('should return undefined if _link property empty', function () {
            policyDataCopy._links = {};
            model.fetch();
            expect(model._getLinks()).to.be.undefined;
        });

        it('should return undefined if _link property null', function () {
            policyDataCopy._links = null;
            model.fetch();
            expect(model._getLinks()).to.be.undefined;
        });
    });

    describe('_getLinkForRel method', function () {
        before (function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDataCopy
            );
            model = new PolicyDetailModel({
                id:1222312312
            });
        });

        after ( function () {
            ajaxStub.restore();
        });
        it('exists as a function', function () {
            expect(model._getLinkForRel).to.exist.and.be.a('function');
        });

        it('should return model object if rel is valid', function () {
            model.fetch();
            var link = model._getLinkForRel('application-document');
            expect(link).to.be.an.instanceof(Backbone.Model);
            expect(link.get('href'))
                .to.be.equal(policyDataCopy._links['application-document'].href);
        });

        it('should return undefined if rel (relation) param is missing', function (){
            model.fetch();
            var fn = function () {
                model._getLinkForRel();
            };
            expect(fn).to.throw(model.errors.missingRelParameter);
        });

        it('should return undefined if rel (relation) param is invalid', function (){
            model.fetch();
            var link = model._getLinkForRel('some-document');
            expect(link).to.be.undefined;
        });
    });

    // setUnderwriterAndFilesAvailabilityFlag
    describe('setUnderWriterAndFilesAvailabilityFlag method', function () {
        before (function () {
            ajaxStub = this.sinon.stub($, 'ajax');
            model = new PolicyDetailModel({
                id:1222312312
            });
        });

        beforeEach ( function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );
        });

        after ( function () {
            ajaxStub.restore();
        });
        it('exists as a function', function () {
            expect(model.setUnderwriterAndFilesAvailabilityFlag).to.exist.and.be.a('function');
        });

        it('should hasUnderwriterAndFiles to be true if all require data exist', function () {
            model.fetch();
            model.setUnderwriterAndFilesAvailabilityFlag();
            expect(model.get('application').hasUnderwriterAndFiles).to.be.true;
        });

        it('should call "_addTargetuser" twice while there is require data', function () {
            var addTargetuserSpy = this.sinon.spy(model, '_addTargetuser');
            model.fetch();
            model.setUnderwriterAndFilesAvailabilityFlag();
            expect(addTargetuserSpy).to.have.been.calledTwice;
            addTargetuserSpy.restore();
        });

        it('should hasUnderwriterAndFiles to be true if underwriter data exist, '+
                'but missing "_links" param ', function () {
            policyDataCopy._links = {};
            model.fetch();
            model.setUnderwriterAndFilesAvailabilityFlag();
            expect(model.get('application').hasUnderwriterAndFiles).to.be.true;
        });

        it('should hasUnderwriterAndFiles not exist if there is no "underwriter"'+
                ' data or "_links"', function () {
            policyDataCopy._links = {};
            policyDataCopy.application.underWriter = null;
            model.fetch();
            model.setUnderwriterAndFilesAvailabilityFlag();
           expect(model.get('application').hasUnderwriterAndFiles).to.be.undefined;
        });
    }); // setUnderwriterAndFilesAvailabilityFlag

    describe('_addTargetuser method', function () {
        it('exists as function', function (){
            expect(model._addTargetuser).to.exist.and.be.a('function');
        });

        it('url should have targetuser querystring if user in impersonate state', function (){
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function() {
                return 'baxter';
            });
            var url      ='https://oa.com/22213/policy/doc';
            var expected = url+'?targetuser=baxter';
            expect(model._addTargetuser(url)).to.be.equal(expected);

            userChannel.stopReplying();
            userChannel = null;
        });

        it('url should have targetuser querystring if user not in impersonation', function (){
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function() {
                return '';
            });
            var url ='https://oa.com/22213/policy/doc';
            expect(model._addTargetuser(url)).to.be.equal(url);

            userChannel.stopReplying();
            userChannel = null;
        });

        it('should throw exception is url parameter is missing', function (){
            var fn = function () {
               model._addTargetuser(); 
            };
            expect(fn).to.throw(model.errors.missingURLParam);
        });

    });
});

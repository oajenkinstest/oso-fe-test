/* eslint no-console: 0 */
/* global expect: false, Backbone:false, jsdomConsole:false */

var config      = require('../dist/config/config');
var debugModule = require('../dist/modules/debug/debugModule');
var dbg         = debugModule.init(config.debugEnabled);

//storing as its overriding, after the run of config-spec
var debugEnabled = config.debugEnabled;

describe('Debug Module (modules/debug/debugModule.js)', function() {

    it('exists', function () {
        expect(debugModule).to.exist;
    });

    it('init method should exist', function () {
        expect(debugModule.init).to.be.exist;
    });

    it('init method should return an object', function () {
        expect(dbg).to.be.an('object', 'Expected is object');
    });

    it('Value of `this.enabled` should be same as the value on initializing',
        function () {
            expect(dbg.enabled).to.equal(debugEnabled);
        });

    it('Value of `this.enabled` should change while calling enable method',
        function () {
            dbg.enable();
            var enabled = dbg.isEnabled();
            expect(enabled).to.equal(true);
        });

    it('Value of `this.enabled` should change while calling disable method',
        function () {
            dbg.disable();
            var enabled = dbg.isEnabled();
            expect(enabled).to.equal(false);
        });

    it('Current value of `this.enabled` should return while calling isEnabled method',
        function () {
            dbg.enable();
            var enabled = dbg.isEnabled();
            expect(enabled).to.equal(true);
        });

    describe('log, info, warn, trace, error, dir methods', function () {

        it('exist in debugModule', function () {
            expect(dbg.log).to.be.exist;
            expect(dbg.info).to.be.exist;
            expect(dbg.warn).to.be.exist;
            expect(dbg.trace).to.be.exist;
            expect(dbg.error).to.be.exist;
            expect(dbg.dir).to.be.exist;
        });

        it('`log` method calls console.log', function () {
            var logSpy = this.sinon.spy(jsdomConsole, 'log');
            dbg.log('data');
            expect(logSpy).to.have.been.calledWith(['data']);
            logSpy.restore();
        });

        it('`info` method calls console.info', function () {
            var infoSpy = this.sinon.spy(jsdomConsole, 'info');
            dbg.info('data');
            expect(infoSpy).to.have.been.calledWith(['data']);
        });

        it('`warn` method calls console.warn', function () {
            var warnSpy = this.sinon.spy(jsdomConsole, 'warn');
            dbg.warn('data');
            expect(warnSpy).to.have.been.calledWith(['data']);
        });

        it('`trace` method calls console.trace', function () {
            var traceSpy = this.sinon.spy(jsdomConsole, 'trace');
            dbg.trace('data');
            expect(traceSpy).to.have.been.calledWith(['data']);
        });

        it('`dir` method calls console.dir', function () {
            var dirSpy = this.sinon.spy(jsdomConsole, 'dir');
            dbg.dir('data');
            expect(dirSpy).to.have.been.calledWith(['data']);
        });

        it('log, info, warn, trace, error, dir methods should log data in console ' +
            'while enabled is TRUE', function () {
            dbg.enable();
            expect(dbg.log()).to.equal(true);
            expect(dbg.info()).to.equal(true);
            expect(dbg.warn()).to.equal(true);
            expect(dbg.trace()).to.equal(true);
            expect(dbg.error()).to.equal(true);
            expect(dbg.dir()).to.equal(true);
        });

        it('log, info, warn, trace, error, dir methods should log data in console ' +
            'while enabled is FALSE', function () {
            dbg.disable();
            expect(dbg.log()).to.equal(false);
            expect(dbg.info()).to.equal(false);
            expect(dbg.warn()).to.equal(false);
            expect(dbg.trace()).to.equal(false);
            expect(dbg.error()).to.equal(false);
            expect(dbg.dir()).to.equal(false);
        });

        it('log, info, warn, trace, error, dir methods should log data in console ' +
            'while query string ?debug set to TRUE', function () {
            dbg.changeDebugStatusByQueryString('http://localhost:3000/?debug=true', 'debug');
            expect(dbg.log()).to.equal(true);
            expect(dbg.info()).to.equal(true);
            expect(dbg.warn()).to.equal(true);
            expect(dbg.trace()).to.equal(true);
            expect(dbg.error()).to.equal(true);
            expect(dbg.dir()).to.equal(true);
        });

        it('log, info, warn, trace, error, dir methods should not log data in console ' +
            'while query string ?debug set to FALSE', function () {
            dbg.changeDebugStatusByQueryString('http://localhost:3000/?debug=false', 'debug');
            expect(dbg.log()).to.equal(false);
            expect(dbg.info()).to.equal(false);
            expect(dbg.warn()).to.equal(false);
            expect(dbg.trace()).to.equal(false);
            expect(dbg.error()).to.equal(false);
            expect(dbg.dir()).to.equal(false);
        });
    }); // log, info, warn, trace, error, dir methods



    it('window.onerror methods log error data in console while enabled is TRUE',
        function () {

            // Create an onerror stub, because the debug module's function will call the REAL
            // window.onerror when enabled, and calling the actual window.onerror crashes the
            // tests!
            var onerrorStub = this.sinon.stub(window, 'onerror').callsFake(function () {
                return true;
            });

            dbg.enable();
            dbg.overrideWindowOnError();
            expect(window.onerror()).to.equal(true);

            // Make sure the onerrorStub got called once.
            expect(onerrorStub).to.have.been.calledOnce;
            onerrorStub.restore();
        }
    );

    describe('in Disable state ( GA tracking and server logging )', function () {
        var ajaxStub;
        var analyiticsChannel;
        var analyiticsRadioRequestSpy;

        var logResponse = {
            status: 1,
            message: 'message logged to server successfully'
        };

        beforeEach(function () {
            dbg.disable();
            ajaxStub = this.sinon.stub(Backbone.$, 'ajax').yieldsTo(
                'success',
                logResponse
            );

            analyiticsChannel = Backbone.Radio.channel('analytics');
            analyiticsChannel.reset();
            analyiticsRadioRequestSpy = this.sinon.spy();
            analyiticsChannel.on('trackException', analyiticsRadioRequestSpy);
        });

        afterEach(function () {
            dbg.enable();
            ajaxStub.restore();

            analyiticsChannel.off('trackException', analyiticsRadioRequestSpy);

            analyiticsRadioRequestSpy = null; // no .restore() since it doesn't wrap anything
            analyiticsChannel.reset();
        });

        it ('avoids cascading failures if something within dbg.error() throws ' +
            'an exception', function () {
            // When the dbg.error() function runs, it sends a 'trackException' message
            // on the analyticsChannel. For this test, we want a handler function of
            // that message to throw an exception to show that it will not cause cascading
            // errors.
            var badFunction = this.sinon.stub().throws();
            analyiticsChannel.on('trackException', badFunction);

            dbg.error('analyticsChannel trackException handler test');

            expect(badFunction).to.have.been.calledOnce; // make sure the exception was thrown
            expect(analyiticsRadioRequestSpy).to.have.been.calledOnce; // not called repeatedly

            analyiticsChannel.off('trackException', badFunction);
        });

        describe ('Google analytics exception tracking', function () {
            it('console.error calls Radio event "trackException"', function () {
                dbg.error('some error');
                expect(analyiticsRadioRequestSpy).to.have.been.calledWith({
                    fatal: false,
                    message: 'some error'
                });
            });

            it('window.onerror methods call Radio event "trackException" while '+
                'enabled is FALSE', function () {
                dbg.overrideWindowOnError();
                window.onerror();
                expect(analyiticsRadioRequestSpy).to.have.been.called;
            });
        });

        describe('logToServer method', function() {
            var logToServerSpy;

            beforeEach(function () {
                logToServerSpy = this.sinon.spy(dbg, 'logToServer');
            });
            afterEach(function () {
                logToServerSpy.restore();
            });

            it('console.error calls logToServer and ajax method', function() {
                dbg.error('some error');
                expect(logToServerSpy).to.have.been.calledWith({
                    level   : 'error',
                    message : 'some error'
                });
                expect(ajaxStub).to.have.been.called;
            });

            it('window.onerror calls logToServer and ajax method '+
                'while debug is disabled', function () {
                dbg.overrideWindowOnError();
                window.onerror();
                expect(logToServerSpy).to.have.been.called;
                expect(ajaxStub).to.have.been.called;
            });

            it('returns false if parameters are empty', function () {
                logToServerSpy.restore();
                var output = dbg.logToServer();
                expect(output).to.be.false;

                output = dbg.logToServer({});
                expect(output).to.be.false;
            });

            it('executes callback if parameters are valid', function () {
                logToServerSpy.restore();
                var callbackSpy = this.sinon.spy();
                dbg.logToServer({
                    level   : 'error',
                    message : 'some message'
                }, callbackSpy);
                expect(callbackSpy).to.have.been.called;
            });
        }); // logToServer method
    });

});



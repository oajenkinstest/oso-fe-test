/* global expect:false */

/*
 * IMPORTANT: Make sure that the ._startActivityListener and ._startIdleTimer methods are
 * never actually executed in any unit test. Doing so will leave a setTimeout running when
 * the tests exit, which causes Mocha to hang, thus breaking the build.
 *
 * The maintainer of the gulp-mocha module suggests explicitly exiting the gulp process
 * (see https://github.com/sindresorhus/gulp-mocha#faq), but to do so interferes with the
 * Gradle build that Jenkins uses. So instead of relying on that workaround, we just have to
 * avoid leaving a setTimeout running after the tests are complete.
 *
 * In Oct 2017 Randy tried adding a `stopIdleTimer` method that unit tests could call to
 * clear the timeout and turn off the activity listener, but it didn't prevent Mocha from
 * hanging. It would be worth giving that another try at some point. Randy probably just did
 * it wrong. :)
 */
var idleModule = require('../dist/modules/idle/idleModule');

describe('Idle Module (modules/idle/idleModule.js)', function () {

    it('idleModule object exists', function () {
        expect(idleModule).to.exist;
    });

    describe('start method', function() {

        it('exists as a function', function () {
            expect(idleModule.start).to.exist.and.be.a('function');
        });

        it('starts activity listener and idle timer', function(){
            var stubStartActivityListener = this.sinon.stub(idleModule, '_startActivityListener');
            var stubStartIdleTimer = this.sinon.stub(idleModule, '_startIdleTimer');

            idleModule.start({
                callback: function () {}
            });

            expect(stubStartActivityListener).to.have.been.called;
            expect(stubStartIdleTimer).to.have.been.called;

            stubStartActivityListener.restore();
            stubStartIdleTimer.restore();
        });
    }); // start method

    describe('idle watcher module timeout', function() {

        var idleTimeoutCallbackStub;

        beforeEach(function () {
            // Immediately force the timer to timeout.
            idleTimeoutCallbackStub = this.sinon.stub(idleModule, 'start')
                .yieldsTo('callback', true);
        });

        afterEach(function () {
            idleTimeoutCallbackStub.restore();
        });

        it('logout function is called after timeout', function () {
            // stub a callback method for idleTimeout
            var logout = this.sinon.stub();

            idleModule.start({
                callback: logout
            });

            expect(logout).to.have.been.calledOnce;
        });
    });
});

/* global expect:false, Backbone:false, $:false */

var OrgsGeneralInfoView = require('../dist/pages/policy/views/orgs-general-info-v');

var helpers   = require('./helpers/helpers');

describe('Organization General Info View (pages/policy/views/orgs-general-info-v.js)', function() {

    var data = [
        {
            producers : [
                {
                    producer : {
                        id          : 456,
                        fullName    : 'Darth Vader',
                        lexicalName : 'Vader, Darth'
                    }
                }, {
                    producer : {
                        id          : 789,
                        fullName    : 'Luke Skywalker',
                        lexicalName : 'Skywalker, Luke'
                    }
                }
            ]
        }, {
            producers : [
                {
                    producer : {
                        id          : 101,
                        fullName    : 'James T Kirk',
                        lexicalName : 'Kirk, James T'
                    }
                }, {
                    producer : {
                        id          : 909,
                        fullName    : 'Spock',
                        lexicalName : 'Spock'
                    }
                }
            ]
        }
    ];

    var ajaxStub;
    var rootView;
    var managingProducerData;
    var userChannel;
    var view;

    beforeEach(function () {
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo('success', data);

        userChannel = Backbone.Radio.channel('user');
        userChannel.reply('hasCapability', function() {
            return true;
        });

        managingProducerData = {
            id            : 144586,
            fullName      : 'JEFFREY R SEMLER',
            lexicalName   : 'SEMLER, JEFFREY'
        };
        rootView = helpers.viewHelpers.createRootView();
        view = new OrgsGeneralInfoView({
            model : new Backbone.Model(managingProducerData)
        });
        rootView.render();
        rootView.showChildView('contentRegion', view);
        view.showName(managingProducerData.fullName);
        view.showData(managingProducerData);
    });

    afterEach(function () {
        ajaxStub.restore();
        userChannel.stopReplying();
        userChannel = null;
        view = null;
    });

    it('exists', function () {
        expect(OrgsGeneralInfoView).to.exist;
    });
       
    it('displays full name', function () {
        expect(view.$el.find('h3').text()).to.contain(managingProducerData.fullName);
    });

    it('Link "Policies in Organization" should displayed', function () {
        expect(view.$el.find('.policy-list-link').text())
            .to.contain('Policies in Sub-Organization');
    });

    it('Link "Policies in Organization" should not displayed when Id is missing', function () {
        view = new OrgsGeneralInfoView({
            model : new Backbone.Model(managingProducerData)
        });
        rootView.showChildView('contentRegion', view);
        view.showData({});

        expect(view.$el.find('.policy-list-link')).to.have.a.lengthOf(0);
    });

    it('hierarchy is displayed', function() {
        expect(view.$el.find('ul.list-inline')).to.have.a.lengthOf(2);
    });


    describe('_hierarchyChange method', function () {

        var listener;
        var hierarchyChangeStub;

        beforeEach(function() {
            listener = new Backbone.Marionette.Object();
            hierarchyChangeStub = this.sinon.stub();
            listener.listenTo(view, 'hierarchyChange', hierarchyChangeStub);
        });

        afterEach(function () {
            listener.stopListening();
            listener.destroy();
        });

        it('exists as a function', function () {
            expect(view._hierarchyChange).to.exist.and.be.a('function');
        });

        it('triggers a "hierarchyChange" event', function () {
            var state = { prop : 'value' };
            view._hierarchyChange(state);

            expect(hierarchyChangeStub).to.have.been.calledWith(state);
        });
    }); // _hierarchyChange method

});

/* global describe:false, Backbone:false, $:false, it:false, expect:false */

var PolicyListView = require('../dist/pages/policy/views/policy-list-v');

// load behaviors
require('../dist/modules/behaviors/index');

var helpers = require('./helpers/helpers');
var modelResponseBody = helpers.policyListData.policyList;

describe('Policy List View (pages/policy/views/policy-list-v.js)', function () {
    var rootView;
    var view;

    it('exists', function() {
        expect(PolicyListView).to.exist;
    });
    
    it('should render view without any error', function () {
        rootView = helpers.viewHelpers.createRootView();
        view = new PolicyListView({
            producerId : '100001'
        });
        rootView.render();
        rootView.showChildView('contentRegion', view);

        expect(view.isRendered).to.be.true;

        view.destroy();
    });

    it('should not throw error even producerId is missing', function () {
        var fn = function () {
            view = new PolicyListView();
            view.destroy();
        };
        expect(fn).to.not.throw();
    });

    describe('initialize method', function () {

        beforeEach (function () {
            view = new PolicyListView({
                producerOrgId : '100001'
            });
        });

        afterEach (function () {
            view.destroy();
        });

        it('default tab should "pending"', function () {
            expect(view.currentTab).to.equal('pending');
        });

        it('"hasInvalidTab" flag should be false if a valid tab option', function () {
            view = new PolicyListView({
                currentTab : 'paid',
                producerOrgId : '100001'
            });
            expect(view.hasInvalidTab).to.be.false;
        });

        it('"hasInvalidTab" flag should be true if wrong "currentTab" provided', 
                function () {
            view = new PolicyListView({
                currentTab : 'some tab',
                producerOrgId : '100001'
            });
            expect(view.hasInvalidTab).to.be.true;
        });

        it('each tabState should updated with producerOrgId', function () {
            expect(view.tabState.allPolicies.producerOrgId).to.equal('100001');
            expect(view.tabState.pending.producerOrgId).to.equal('100001');
            expect(view.tabState.paid.producerOrgId).to.equal('100001');
            expect(view.tabState.inactive.producerOrgId).to.equal('100001');
        });

        it('each tabState should updated with producerId', function () {
            view = new PolicyListView({
                currentTab : 'paid',
                producerId : '100001'
            });
            expect(view.tabState.allPolicies.producerId).to.equal('100001');
            expect(view.tabState.pending.producerId).to.equal('100001');
            expect(view.tabState.paid.producerId).to.equal('100001');
            expect(view.tabState.inactive.producerId).to.equal('100001');
        });

        it('"searchEnabled" property in model should be false', function () {
            expect(view.model.get('searchEnabled')).to.be.false;
        });

        // Stopped working after the upgrade to jQuery 3
        describe.skip('Define regions and UI elements', function () {
            it('should prepend "#org-" if options has "hierarchy" with value "org"', function () {
                view = new PolicyListView({
                    currentTab : 'paid',
                    hierarchy : 'org'
                });
                view.render();
                expect(view.regionManager._regions.allPolicyListDatatableRegion.$el.selector)
                    .to.equal('#org-all-policy-list-datatable-region');
                expect(view.regionManager._regions.countAllPolicies.$el.selector)
                    .to.equal('#org-count-all-policies');    

                expect(view.ui.allPoliciesTab.selector)
                    .to.equal('#org-all-policies-tab');
            });

            it('should not prepend', function () {
                view = new PolicyListView({
                    currentTab : 'paid',
                    producerId : '1000001'
                });
                view.render();
                expect(view.regionManager._regions.allPolicyListDatatableRegion.$el.selector)
                    .to.equal('#all-policy-list-datatable-region');
                expect(view.regionManager._regions.countAllPolicies.$el.selector)
                    .to.equal('#count-all-policies');    

                expect(view.ui.allPoliciesTab.selector)
                    .to.equal('#all-policies-tab');
            });
        });
    }); // initialize method

    describe('_setTabActive and _showPolicyList methods', function () {
        var _setTabActiveSpy;
        var _showPolicyListSpy;
        var listener;
        var options = {
            currentTab : 'inactive',
            producerOrgId : '100001'
        };
        var stateChangeStub;

        beforeEach (function () {
            listener = new Backbone.Marionette.Object();
            view = new PolicyListView(options);

            _setTabActiveSpy = this.sinon.spy( view, '_setTabActive');
            _showPolicyListSpy = this.sinon.spy( view, '_showPolicyList');
            stateChangeStub = this.sinon.stub();

            listener.listenTo(view, 'stateChange', stateChangeStub);

            view.render();
        });

        afterEach (function () {
            view.destroy();
            _setTabActiveSpy.restore();
            _showPolicyListSpy.restore();
            listener = null;
        });

        it('should called while rendering', function () {
            expect(_setTabActiveSpy).to.be.called;
        });

        it('default tab should have active class', function () {
            expect(view.$el.find('ul li.active').attr('id'))
                .to.equal(options.currentTab+'-tab');
        });

        it('"_showPolicyList" method should fired', function () {
            expect(_showPolicyListSpy).to.be.called;
        });

        it('"stateChange" should triggered', function () {
            expect(stateChangeStub).to.be.calledWith({
                producerOrgId : options.producerOrgId
            });
        });

        it('orgPendingPolicyListDataTableInfoView should added to region', function () {
            expect(view.getChildView(options.currentTab+'PolicyListDatatableRegion')
                .isRendered).to.be.true;
        });

        describe('orgPendingPolicyListDataTableInfoView should listen to', function () {

            it('tableStateChange', function () {
                expect(view.pendingPolicyListDataTableInfoView._events.tableStateChange)
                    .to.exist;
            });

            it('error', function () {
                expect(view.pendingPolicyListDataTableInfoView._events.error)
                    .to.exist;
            });

            it('noResults', function () {
                expect(view.pendingPolicyListDataTableInfoView._events.noResults)
                    .to.exist;
            });

            it('retrieveOrgProducerInfo', function () {
                expect(view.pendingPolicyListDataTableInfoView
                    ._events.setProducerInfo).to.exist;
            });
        });

        describe('Should change Tab focus and render table data', function () {
            var ajaxStub;

            beforeEach(function() {
                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    modelResponseBody
                ); 
            });

            afterEach(function() {
                ajaxStub.restore();
            });

            it('All', function () {
                options = {
                    currentTab : 'allPolicies',
                    producerId : '100001'
                };

                view = new PolicyListView(options);
                view.render();

                var liElement = view.ui[view.currentTab+'Tab'];
                var dataTableView = view.pendingPolicyListDataTableInfoView;
                liElement.find('a').click();
                
                expect(liElement.hasClass('active')).to.be.true;
                expect(_showPolicyListSpy).to.have.called;
                expect(dataTableView.model.get('status')).to.equal('all');
                expect(dataTableView.$el.find('table#policy-list-datatable')).to.have.lengthOf(1);
                expect(liElement.find('a').data('dataFetchStatus')).to.equal(1);
            });

            it('Pending - default (even currentTab is not specified)', function () {
                options = {
                    currentTab : '',
                    producerId : '100001'
                };

                view = new PolicyListView(options);
                view.render();

                var liElement = view.ui[view.currentTab+'Tab'];
                var dataTableView = view.pendingPolicyListDataTableInfoView;
                liElement.find('a').click();
                
                expect(liElement.hasClass('active')).to.be.true;
                expect(_showPolicyListSpy).to.have.called;
                expect(dataTableView.model.get('status')).to.equal('pending');
                expect(dataTableView.$el.find('table#policy-list-datatable')).to.have.lengthOf(1);
                expect(liElement.find('a').data('dataFetchStatus')).to.equal(1);
            });

            it('Paid', function () {
                options = {
                    currentTab : 'paid',
                    producerId : '100001'
                };

                view = new PolicyListView(options);
                view.render();

                var liElement = view.ui[view.currentTab+'Tab'];
                var dataTableView = view.pendingPolicyListDataTableInfoView;
                liElement.find('a').click();
                
                expect(liElement.hasClass('active')).to.be.true;
                expect(_showPolicyListSpy).to.have.called;
                expect(dataTableView.model.get('status')).to.equal('paid');
                expect(dataTableView.$el.find('table#policy-list-datatable')).to.have.lengthOf(1);
            });
        });

    }); // _setTabActive and _showPolicyList methods

    describe('_notifyOfStateChange method', function () {

        var options = {
            currentTab : 'paid',
            producerOrgId : '100001'
        };
        var listener;
        var stateChangeStub;

        beforeEach(function () {
            view = new PolicyListView(options);
            stateChangeStub = this.sinon.stub();
            listener        = new Backbone.Marionette.Object();
            listener.listenTo(view, 'stateChange', stateChangeStub);
            view.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should exist', function () {
            expect(view._notifyOfStateChange).to.exist;
        });

        it('it should trigger "stateChange" event', function () {
            view._notifyOfStateChange({
                start: 0,
                length: 50
            });

            expect(stateChangeStub).to.be.calledWith({
                producerOrgId : options.producerOrgId,
                length        : 50,
                start         : 0
            });
        });
    }); // _notifyOfStateChange method

    describe('_setProducerInfo method', function () {
        var options = {
            currentTab : 'paid',
            producerId : '100001'
        };
        var listener;
        var setProducerInfoStub;

        beforeEach(function () {
            view = new PolicyListView(options);
            setProducerInfoStub = this.sinon.stub();
            listener        = new Backbone.Marionette.Object();
            listener.listenTo(
                view, 'setProducerInfo', setProducerInfoStub
            );
            view.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should exist', function () {
            expect(view._setProducerInfo).to.exist;
        });

        it('should trigger "setProducerInfo" event', function () {
            view._setProducerInfo({
                producerId : 100001
            });
            expect(setProducerInfoStub).to.be.calledWith({
                producerId : 100001
            });  
        });
    }); // _setProducerInfo method

    describe('_showPolicyCounts method', function () {
        var reportingGroupsCounts =  [ {
            group : 'PENDING',
            count : 29
        }, {
            group : 'PAID',
            count : 39
        }, {
            group : 'INACTIVE',
            count : 33
        }, {
            group : 'ALL',
            count : 114
        } ];

        modelResponseBody.reportingGroups = reportingGroupsCounts;
        var ajaxStub;

        beforeEach (function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                modelResponseBody
            );

            view = new PolicyListView({
                producerId : '100001'
            });

            view.render();
        });

        afterEach(function () {
            ajaxStub.restore();
            view.destroy();
        });

        it('"ALL counts" should rendered correctly', function () {
            expect(view.$el.find('#count-all-policies').text()).to.be.contain(114);
        });

        it('"PENDING counts" should rendered correctly', function () {
            expect(view.$el.find('#count-pending-policies').text()).to.be.contain(29);
        });

        it('"PAID counts" should rendered correctly', function () {
            expect(view.$el.find('#count-paid-policies').text()).to.be.contain(39);
        });

        it('"INACTIVE counts" should rendered correctly', function () {
            expect(view.$el.find('#count-inactive-policies').text()).to.be.contain(33);
        });
    }); // _showPolicyCounts method

    describe('_handleCountsFetchError method', function () {

        describe('When call to "counts" endpoint responds with an error', function () {
            var response = {
                status       : 504,
                responseText : 'Oops!'
            };
            var model = {};

            beforeEach(function () {
                view = new PolicyListView({
                    producerId : '100001'
                });

                view.render();
                view._handleCountsFetchError(model, response);
            });

            afterEach(function () {
                view.destroy();
            });

            it('"ALL counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-all-policies').text()).to.equal('()');
            });

            it('"PENDING counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-pending-policies').text()).to.equal('()');
            });

            it('"PAID counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-paid-policies').text()).to.equal('()');
            });

            it('"INACTIVE counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-inactive-policies').text()).to.equal('()');
            });

        });

        describe('When call to "counts" endpoint does not return a response', function () {
            var model = {};

            beforeEach(function () {
                view = new PolicyListView({
                    producerId : '100001'
                });

                view.render();
                view._handleCountsFetchError(model);
            });

            afterEach(function () {
                view.destroy();
            });

            it('"ALL counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-all-policies').text()).to.equal('()');
            });

            it('"PENDING counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-pending-policies').text()).to.equal('()');
            });

            it('"PAID counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-paid-policies').text()).to.equal('()');
            });

            it('"INACTIVE counts" should rendered as empty parentheses', function () {
                expect(view.$el.find('#count-inactive-policies').text()).to.equal('()');
            });

        });

    }); // _handleCountsFetchError method

    describe('_showServerErrorMessage method', function () {
        var options = {
            currentTab : 'paid',
            producerId : '100001'
        };

        beforeEach(function () {
            view = new PolicyListView(options);
            view.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should exist', function () {
            expect(view._showServerErrorMessage).to.exist;
        });

        it('should render orgPendingPolicyListDataTableInfoView '+
                'with error message', function () {
            view._showServerErrorMessage();
            var policyListDTElement = view.pendingPolicyListDataTableInfoView.$el;
            expect(policyListDTElement.find('p:not(".hidden")').text())
                .to.equal(view.errors.serverError);

            expect(policyListDTElement.find('.alert-warning'))
                .to.have.lengthOf(1);
        });

        it('should render orgPendingPolicyListDataTableInfoView '+
                'with error message based on inputs', function () {
            view._showServerErrorMessage('Some error message', 'info');
            var policyListDTElement = view.pendingPolicyListDataTableInfoView.$el;
            expect(policyListDTElement.find('p:not(".hidden")').text())
                .to.equal('Some error message');

            expect(policyListDTElement.find('.alert-info'))
                .to.have.lengthOf(1);
        });
    }); // _showServerErrorMessage method

    describe ('_showPolicyListView method', function () {
        var options = {
            currentTab : 'search',
            searchEnabled : true
        };
        var _showPolicyListViewSpy;

        before(function () {
            view = new PolicyListView(options);
            _showPolicyListViewSpy = this.sinon.spy(view, '_showPolicyListView');
            view.render();
        });

        after(function () {
            view.destroy();
            _showPolicyListViewSpy.restore();
        });

        it('should exist and be a function', function (){
            expect(view._showPolicyListView).to.exist.and.be.a('function');
        });

        it('should call with pending details, as default tab is search ', function () {
            expect(_showPolicyListViewSpy).to.be.calledWith(
                'pendingPolicyListDatatableRegion', 'pending'
            );
        });

        it('should throw error if region or status is missing ', function () {
            var fn = function () {
                view._showPolicyListView();
            };
            expect(fn).to.throw(view.errors.missingRegionName);

            fn = function () {
                view._showPolicyListView('pendingPolicyListDatatableRegion');
            };
            expect(fn).to.throw(view.errors.missingStatus);
        });

        it('should call if default tab is not search', function () {
            options = {
                currentTab : 'paid',
                searchEnabled : true
            };
            view = new PolicyListView(options);
            _showPolicyListViewSpy.restore();
            _showPolicyListViewSpy = this.sinon.spy(view, '_showPolicyListView');
            view.render();
            expect(_showPolicyListViewSpy).to.be.calledWith(
                'paidPolicyListDatatableRegion', 'paid', {}
            );
        });
    });
});

/* global expect:false*/

var Model = require('../dist/pages/policy/models/hierarchy-paths-m');

describe('Hierarchy Paths Model (pages/policy/models/hierarchy-paths-m.js)', function() {

    it('exists', function() {
        expect(Model).to.exist;
    });

    it('throws an error if instantiated without an id', function() {
        var fn = function() {
            var model = new Model(); // eslint-disable-line no-unused-vars
        };

        expect(fn).to.throw(Model.prototype.errors.idRequired);
    });

    it('url adds the id properly to the URL', function() {
        var id          = 999;
        var expectedUrl = '/api/oso/secure/rest/producers/' + id + '/paths';
        var model       = new Model({ id : id });
        var url         = model.url();

        expect(url).to.equal(expectedUrl);
    });

    it('throws an error if calling URL after the id has been removed', function() {
        var model = new Model({ id : 999 });
        model.unset('id');
        var fn = function() {
            model.url();
        };

        expect(fn).to.throw(Model.prototype.errors.idRequired);
    });

    it('parse sets the response on the "paths" attribute', function() {
        var response = [ {id: 123}, {id:456} ];
        var model = new Model({ id : 999 });

        // there's no 'paths' property yet...
        expect(model.has('paths')).to.equal(false);

        model.parse(response);

        // Now there is!
        expect(model.has('paths')).to.equal(true);
        expect(model.get('paths')).to.equal(response);
    });

    describe('producerIsRoot method', function () {

        it('exists as function', function() {
            var model = new Model({ id : 999 });

            expect(model.producerIsRoot).to.exist.and.be.a('function');
        });

        it('throws error if there is no id in model', function() {
            var model = new Model({ id : 999 });
            model.unset('id');
            var fn = function() {
                model.producerIsRoot();
            };

            expect(fn).to.throw(Model.prototype.errors.idRequired);
        });

        it('returns true if all hierarchy paths start with the ' +
            'model\'s id', function() {

            var model = new Model({
                id    : 999,
                paths : [
                    {
                        producers : [
                            {
                                producer : {
                                    id : 999
                                },
                            }, {
                                producer : {
                                    id : 123
                                }
                            }
                        ]
                    }, {
                        producers : [
                            {
                                producer : {
                                    id : 999
                                },
                            }, {
                                producer : {
                                    id : 456
                                }
                            }
                        ]
                    }
                ]
            });

            expect(model.producerIsRoot()).to.equal(true);
        });

        it('returns false if not all hierarchy paths start with the ' +
            'model\'s id', function() {

            var model = new Model({
                id    : 999,
                paths : [
                    {
                        producers : [
                            {
                                producer : {
                                    id : 999
                                },
                            }, {
                                producer : {
                                    id : 123
                                }
                            }
                        ]
                    }, {
                        producers : [
                            {
                                producer : {
                                    id : 111
                                },
                            }, {
                                producer : {
                                    id : 456
                                }
                            }
                        ]
                    }
                ]
            });

            expect(model.producerIsRoot()).to.equal(false);
        });
    }); // producerIsRoot method
    
});

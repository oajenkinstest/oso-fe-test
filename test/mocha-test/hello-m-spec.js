/* global  $:false, expect:false */

var HelloModel = require('../dist/pages/hello/models/hello-m');


describe('Hello Model (pages/hello/models/hello-m.js)', function() {

    it('exists', function () {
        expect(HelloModel).to.exist;
    });

    it('can be instantiated', function () {
        var model = new HelloModel();

        expect(model).to.be.defined;
    });

    it('has a url', function () {
        var model = new HelloModel();

        expect(model.url).to.be.defined;
    });

    describe('Service integration', function() {
        var ajaxStub;

        beforeEach(function() {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                {'id': 123, 'content': 'Hello, World!'}
            );
        });

        afterEach(function() {
            ajaxStub.restore();
        });

        it('correctly loads data from the service', function () {
            var model = new HelloModel();
            model.fetch();
            expect(model.get('id')).to.equal(123);
        });
    });
});

/* global expect:false, $:false, _:false */

var ApplicationStatusParser =
    require('../dist/pages/policy/models/parsers/policy-application-status-tracker');
var helpers = require('./helpers/helpers');

describe('Application Status Tracker Model Parser ' +
    '(pages/policy/models/parsers/policy-application-status-tracker.js)', function () {
    var parser;
    var policyDataCopy;
    var result;

    var getApplicationStatusParser = function (policyData, description, status) {
        if (status) {
            policyData.policyStatus.status = status;
        }

        if (description) {
            policyData.policyStatus.description = description;
        }

        return new ApplicationStatusParser(
            policyDataCopy.policyStatus,
            policyDataCopy.application,
            policyDataCopy.policyNumber
        );
    };

    beforeEach(function () {
        policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        parser = getApplicationStatusParser();
    });

    afterEach(function () {
        parser.destroy();
        parser = null;
        policyDataCopy = null;
        result = null;
    });

    it('exists', function () {
        expect(ApplicationStatusParser).to.exist;
    });

    describe('initialization', function () {
        var errors = ApplicationStatusParser.prototype.errors;
        var fn;
        var validPolicyStatus = helpers.policyData.pendingPolicyDetail.policyStatus;

        it('throws an error if first argument is null', function () {
            fn = function () {
                parser = new ApplicationStatusParser();
            };

            expect(fn).to.throw(errors.invalidPolicyStatusObject);
        });

        it('throws an error if first argument is not an object', function () {
            fn = function () {
                parser = new ApplicationStatusParser('foo');
            };

            expect(fn).to.throw(errors.invalidPolicyStatusObject);
        });

        it('throws an error if the second argument is null', function () {
            fn = function () {
                parser = new ApplicationStatusParser(validPolicyStatus);
            };

            expect(fn).to.throw(errors.invalidApplicationObject);
        });

        it('throws an error if the second argument is not an object', function () {
            fn = function () {
                parser = new ApplicationStatusParser(validPolicyStatus, 'foo');
            };

            expect(fn).to.throw(errors.invalidApplicationObject);
        });

    });

    describe('_setDefaultStatusAndDescriptionValues method', function () {

        it('sets "statusLowerCase" variable to empty string if ' +
            'status is not present in data', function () {
            parser.destroy();
            delete policyDataCopy.policyStatus.status;
            parser = new ApplicationStatusParser(
                policyDataCopy.policyStatus, policyDataCopy.application
            );

            expect(parser.statusLowerCase).to.equal('');
        });

        it('sets "descriptionLowerCase" variable to empty string if ' +
            'description is not present in data', function () {
            parser.destroy();
            delete policyDataCopy.policyStatus.description;
            parser = new ApplicationStatusParser(
                policyDataCopy.policyStatus, policyDataCopy.application
            );

            expect(parser.descriptionLowerCase).to.equal('');
        });

        it('"statusLowerCase" variable should be empty string if ' +
            'status is null', function () {
            parser.destroy();
            policyDataCopy.policyStatus.status = null;
            parser = new ApplicationStatusParser(
                policyDataCopy.policyStatus, policyDataCopy.application
            );

            expect(parser.statusLowerCase).to.equal('');
        });

        it('"descriptionLowerCase" variable should be empty string if ' +
            'description is null', function () {
            parser.destroy();
            policyDataCopy.policyStatus.description = null;
            parser = new ApplicationStatusParser(
                policyDataCopy.policyStatus, policyDataCopy.application
            );

            expect(parser.descriptionLowerCase).to.equal('');
        });

    }); // _setDefaultStatusAndDescriptionValues method

    describe('getIconPropertiesObject method', function () {

        beforeEach(function () {
            result = parser.getIconPropertiesObject();
        });

        afterEach(function () {
            result = null;
        });

        it('exists as a function', function () {
            expect(parser.getIconPropertiesObject).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            var keys = [
                'application',
                'paid',
                'policySent',
                'policyStatus',
                'readyToIssue',
                'uwDecision'
            ];

            _.each(keys, function(key) {
                expect(result).to.have.property(key);
            });

        });

    }); // getIconPropertiesObject method

    describe('_getPolicyStatusDisplayProperties method', function () {

        beforeEach(function () {
            result = parser._getPolicyStatusDisplayProperties();
        });

        afterEach(function () {
            result = null;
        });

        it('exists as a function', function () {
            expect(parser._getPolicyStatusDisplayProperties).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('text');
            expect(result).to.have.deep.property('displayProps.labelSuffix');
            expect(result).to.have.deep.property('displayProps.type');
        });

        describe('"text" property in returned object should populate with', function () {
            var scenarioText;
            var policyStatusInputsMatrix = [

                //expected      //status        //description   //holdingStatus     //workflowStatus
                ['Paid',        'Issued',       'Paid'],
                ['Paid',        'Terminated',   'Paid'],
                ['Paid',        'Issued',       'Active',       'Active',           'Paid'],
                ['Paid',        'Terminated',   'Terminated',   'Inactive',         'Paid'],
                ['Terminated',  'Terminated',   'Terminated',   'Inactive']
            ];
            
            it('populate with policyStatus.status', function (){
                expect(result.text).to.equal('Pending');
            });

            it('should be blank when policy status is null', function (){
                parser.policyStatus.status = null;
                result = parser._getPolicyStatusDisplayProperties();
                expect(result.text).to.be.empty;
            });

            policyStatusInputsMatrix.forEach(function (inputs) {
                var expected        = inputs[0];
                var status          = inputs[1];
                var description     = inputs[2];
                var holdingStatus   = inputs[3];
                var workflowStatus  = inputs[4];
                if (holdingStatus) {
                    scenarioText = ', policyStatus.acordHoldingStatus = "'+holdingStatus+'", ';
                } 
                
                if (workflowStatus) {
                    scenarioText = scenarioText + 'policyStatus.workflowStatus = "'+workflowStatus+'"';
                }
                
                it('"'+expected+'" if policyStatus.status = "'+status+'", '+
                    'policyStatus.description = "'+description+'"'+scenarioText, function (){
                    parser.policyStatus.status = status;
                    parser.policyStatus.description = description;
                    parser.policyStatus.acordHoldingStatus = holdingStatus;
                    parser.policyStatus.workflowStatus = workflowStatus;
                    result = parser._getPolicyStatusDisplayProperties();
                    expect(result.text).to.equal(expected);
                });
            });
        });

        it('"policyStatusDate" should exist if status is "withdrawn", "incomplete" '+
            'OR description is "In Process" ', function () {
            parser = getApplicationStatusParser(policyDataCopy, 'In Process');
            result = parser._getPolicyStatusDisplayProperties();
            expect(result.policyStatusDate).to.equal('2016-02-21T15:50:00Z');

            parser = getApplicationStatusParser(policyDataCopy, '', 'Withdrawn');
            result = parser._getPolicyStatusDisplayProperties();
            expect(result.policyStatusDate).to.equal('2016-02-21T15:50:00Z');

            parser = getApplicationStatusParser(policyDataCopy, '', 'Incomplete');
            result = parser._getPolicyStatusDisplayProperties();
            expect(result.policyStatusDate).to.equal('2016-02-21T15:50:00Z');
        });

        it('"policyStatusDate" should not exist unless status is "withdrawn", '+
            '"incomplete" OR description is "In Process" ', function () {
            parser = getApplicationStatusParser(policyDataCopy, 'Paid');
            result = parser._getPolicyStatusDisplayProperties();
            expect(result.policyStatusDate).to.not.exist;
        });

        describe('"displayProps" should set correctly ', function () {
            it('policyStatus.description=In Underwriting', function () {
                parser.policyStatus.description = 'In Underwriting';
                result = parser._getPolicyStatusDisplayProperties();
                var expectedDisplayProps = {
                    labelSuffix : 'yellow',
                    type        : 'pencil'
                };
                expect(result.displayProps).to.deep.equal(expectedDisplayProps);
            });

            describe('If policyStatus.description=Paid', function () {

                it('and policyStatus.status=Pending', function () {
                    parser.policyStatus.description = 'Paid';
                    parser.policyStatus.status      = 'Pending';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'yellow',
                        type        : 'pencil'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('and policyStatus.status=Issued', function () {
                    parser.policyStatus.description = 'Paid';
                    parser.policyStatus.status      = 'Issued';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'success',
                        type        : 'check'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('and policyStatus.status=Terminated', function () {
                    parser.policyStatus.description = 'Paid';
                    parser.policyStatus.status      = 'Terminated';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'grey',
                        type        : ''
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

            });     // If policyStatus.description=Paid

            it('policyStatus.description=Terminated', function () {
                parser.policyStatus.description = 'Terminated';
                result = parser._getPolicyStatusDisplayProperties();
                var expectedDisplayProps = {
                    labelSuffix : 'grey',
                    type        : 'warning'
                };
                expect(result.displayProps).to.deep.equal(expectedDisplayProps);
            });

            it('policyStatus.status=Terminated and description = Paid', function(){
                parser.policyStatus.description = 'Paid';
                parser.policyStatus.status = 'Terminated';
                result = parser._getPolicyStatusDisplayProperties();
                var expectedDisplayProps = {
                    labelSuffix : 'grey',
                    type        : ''
                };
                expect(result.displayProps).to.deep.equal(expectedDisplayProps);
            });

            describe ('if "acordHoldingStatus" ', function () {
                it('is "Active" and policyStatus.description=Active and '+
                    'policyStatus.workflowStatus=Paid', function () {
                    parser.policyStatus.description         = 'Active';
                    parser.policyStatus.status              = 'Issued';
                    parser.policyStatus.workflowStatus      = 'Paid';
                    parser.policyStatus.acordHoldingStatus  = 'Active';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'success',
                        type        : 'check'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('is "Proposed" and policyStatus.description=Paid and '+
                    'policyStatus.workflowStatus=Paid', function () {
                    parser.policyStatus.description         = 'Paid';
                    parser.policyStatus.status              = 'Issued';
                    parser.policyStatus.workflowStatus      = 'Paid';
                    parser.policyStatus.acordHoldingStatus  = 'Proposed';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'success',
                        type        : 'check'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('is "Inactive" and policyStatus.description=Terminated', function () {
                    parser.policyStatus.description         = 'Terminated';
                    parser.policyStatus.status              = 'Terminated';
                    parser.policyStatus.acordHoldingStatus  = 'Inactive';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'grey',
                        type        : ''
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('is "Dormant" and policyStatus.description=Active and '+
                    'policyStatus.workflowStatus=Paid', function () {
                    parser.policyStatus.description         = 'Active';
                    parser.policyStatus.status              = 'Issued';
                    parser.policyStatus.workflowStatus      = 'Paid';
                    parser.policyStatus.acordHoldingStatus  = 'Dormant';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'success',
                        type        : 'check'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });
            });

            describe('should set default DisplayProps', function (){
                it('If Status description/workflow is missing', function () {
                    parser.policyStatus.description = '';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : '',
                        type        : 'pencil'
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });

                it('Data issue with Status description:- case sensitive or not a part'+
                        'of any reporting group (Pending, Paid, Inactive) ', function () {
                    parser.policyStatus.status = 'Terminated';
                    parser.policyStatus.description = 'Not taken';
                    result = parser._getPolicyStatusDisplayProperties();
                    var expectedDisplayProps = {
                        labelSuffix : 'grey',
                        type        : ''
                    };
                    expect(result.displayProps).to.deep.equal(expectedDisplayProps);
                });
            });
        });
    }); // _getPolicyStatusDisplayProperties method

    describe('_getApplicationIconProperties method', function () {

        var expectedIconProps;

        beforeEach(function () {
            result = parser.getIconPropertiesObject().application;
        });

        afterEach(function () {
            result = null;
        });

        it('exists as a function', function () {
            expect(parser._getApplicationIconProperties).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('displayAppReceivedDate');
            expect(result).to.have.property('iconProps');
        });

        describe('"iconProps" should match', function () {
            it('application obj has properties - signed, received, entered '+
                'and status is Pending', function (){
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                expect(result.iconProps).to.deep.equal(expectedIconProps);
            });

            it('application obj has properties - signed, received, entered '+
                'and status is not Pending', function (){
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };

                parser.policyStatus.description = 'Paid';
                parser.policyStatus.status      = 'Issued';
                result = parser.getIconPropertiesObject().application;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
            });

            it('displayAppReceivedDate should be TRUE if description is '+
                '"application received" and policyNumber is not ready', function () {
                policyDataCopy.policyNumber = '';
                parser = getApplicationStatusParser(
                            policyDataCopy, 'Application Received');
                result = parser.getIconPropertiesObject().application;
                expect(result.displayAppReceivedDate).to.be.true;
            });
        });
    }); // _getApplicationIconProperties method

    describe('_getUwDecisionIconProperties method', function () {

        beforeEach(function () {
            result = parser._getUwDecisionIconProperties();
        });

        afterEach(function () {
            result = null;
        });

        it('exists as a function', function () {
            expect(parser._getUwDecisionIconProperties).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('iconProps');
        });

        describe('iconProps property should match', function () {

            beforeEach (function (){
                policyDataCopy.application.uwDecision = '2016-02-05T12:50:00';
            });

            afterEach (function () {

                //reset to default one
                policyDataCopy.policyStatus.description = 'In Underwriting';
                policyDataCopy.application.uwDecision   = null;
            });

            it('uwDecision date is exist and policyStatus is Pending', function () {
                var expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                parser = getApplicationStatusParser(policyDataCopy, 'Ready for Issue' );
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result.iconProps).to.deep.equal(expectedIconProps); 
            });

            it('uwDecision date is  exist and policyStatus is not Pending', function () {
                var expectedIconProps = {
                    liClass : null,
                    icon    : 'check'
                };
                policyDataCopy.policyStatus.status = 'Terminated';
                parser = getApplicationStatusParser(policyDataCopy, 'Paid' );
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
            });

            it('status is "Declined" or "Postponed"', function () {
                var expectedIconProps = {
                    liClass : null,
                    icon    : 'warning'
                };
                parser = getApplicationStatusParser(policyDataCopy, '', 'Declined' );
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result.iconProps).to.deep.equal(expectedIconProps);

                parser = getApplicationStatusParser(policyDataCopy, '', 'Postponed' );
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
            });

            it('describe is "In Underwriting"', function () {
                var expectedIconProps = {
                    liClass : 'active',
                    icon    : 'hourglass-half'
                };
                parser = getApplicationStatusParser(policyDataCopy, 'In Underwriting' );
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
            });

            it('"noUWDetail" property should be true if there is no data', function () {
                policyDataCopy.policyStatus.acordHoldingStatus = 'Inactive';
                policyDataCopy.application.uwDecision = null;
                policyDataCopy.application.acceleratedUnderwritingMessage = null;
                var expectedIconProps = {
                    displayText : null,
                    iconProps   : null,
                    noUWDetail  : true
                };
                parser = getApplicationStatusParser(policyDataCopy, 'Terminated', 'Terminated');
                result = parser.getIconPropertiesObject().uwDecision;
                expect(result).to.deep.equal(expectedIconProps);
            });

        });

    }); // _getUwDecisionIconProperties method

    describe('_getReadyToIssueIconProperties method', function () {

        beforeEach(function () {
            policyDataCopy.application.readyToIssue = '2016-02-05T12:50:00';
            result = parser.getIconPropertiesObject().readyToIssue;
        });

        afterEach(function () {
            result = null;
            policyDataCopy.application.readyToIssue = null;
        });

        it('exists as a function', function () {
            expect(parser._getReadyToIssueIconProperties).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('displayText');
            expect(result).to.have.property('iconProps');
        });

        describe('iconProps and displayText should match', function () {

            var expectedIconProps;
            var expectedDisplayText = null;

            it('if status is pending', function () {
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if status is not "Declined" and not in "Pending"', function () {
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                policyDataCopy.policyStatus.status = 'Issued';
                parser = getApplicationStatusParser(policyDataCopy, 'Paid', 'Withdrawn' );
                result = parser.getIconPropertiesObject().readyToIssue;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if description has awaiting values such as '+
                '"requirements outstanding, underwriting complete" or '+
                '"requirements outstanding, not issued" ', function () {
                expectedIconProps = {
                    liClass : 'active',
                    icon    : 'hourglass-half'
                };
                
                expectedDisplayText= 'Requirements Outstanding';

                parser = getApplicationStatusParser(policyDataCopy, 
                                'requirements outstanding, underwriting complete');
                result = parser.getIconPropertiesObject().readyToIssue;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });
        });
    }); // _getReadyToIssueIconProperties method

    describe('_getPolicySentIconProperties method', function () {

        beforeEach(function () {
            policyDataCopy.application.policySent = '2016-02-05T12:50:00';
            result = parser.getIconPropertiesObject().policySent;
        });

        afterEach(function () {
            policyDataCopy.application.policySent = null;
            result = null;
        });

        it('exists as a function', function () {
            expect(parser._getPolicySentIconProperties).to.exist.and.be.a('function');
        });

        it('returns an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('displayText');
            expect(result).to.have.property('iconProps');
        });

        describe ('iconProps and displayText should match', function () {
            var expectedIconProps;
            var expectedDisplayText = null;

            it('if status is pending', function () {
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if status is not pending', function () {
                expectedIconProps = {
                    liClass : null,
                    icon    : 'check'
                };

                policyDataCopy.policyStatus.status = 'Terminated';
                parser = getApplicationStatusParser(policyDataCopy, 'Paid');
                result = parser.getIconPropertiesObject().policySent;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if status description is "Ready for Issue"', function () {
                expectedIconProps = {
                    liClass : 'active',
                    icon    : 'hourglass-half'
                };
                policyDataCopy.application.policySent = null;
                parser = getApplicationStatusParser(
                                policyDataCopy, 'Ready for Issue');
                expectedDisplayText = 'ready for issue';
                result = parser.getIconPropertiesObject().policySent;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

        });
    }); // _getPolicySentIconProperties method

    describe('_getPaidIconProperties method', function () {

        beforeEach(function () {
            policyDataCopy.application.paid = '2016-02-05T12:50:00';
            result = parser.getIconPropertiesObject().paid;
        });

        afterEach(function () {
            policyDataCopy.application.paid = null;
            result = null;
        });

        it('should exist as a function', function () {
            expect(parser._getPaidIconProperties).to.exist.and.be.a('function');
        });

        it('should return an object', function () {
            expect(result).to.be.an('object');
        });

        it('returned object should contain appropriate keys', function () {
            expect(result).to.have.property('displayText');
            expect(result).to.have.property('iconProps');
        });

        describe ('iconProps and displayText should match', function () {
            var expectedIconProps;
            var expectedDisplayText = null;

            it('if status is pending', function () {
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if status is not pending', function () {
                expectedIconProps = {
                    liClass : 'success',
                    icon    : 'check'
                };
                policyDataCopy.policyStatus.status = 'Issued';
                parser = getApplicationStatusParser(policyDataCopy, 'Paid');
                result = parser.getIconPropertiesObject().paid;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });

            it('if status description has "issued not paid" or "ready for payment" ', function () {
                expectedIconProps = {
                    liClass : 'active',
                    icon    : 'hourglass-half'
                };
                parser = getApplicationStatusParser(policyDataCopy, 'Issued Not Paid');
                expectedDisplayText = 'issued not paid';
                result = parser.getIconPropertiesObject().paid;
                expect(result.iconProps).to.deep.equal(expectedIconProps);
                expect(result.displayText).to.deep.equal(expectedDisplayText);
            });
        });
    }); // _getPaidIconProperties method

    describe('_setAcceleratedUnderwritingMessage method', function () {
        it('exists as a function', function () {
            expect(parser._setAcceleratedUnderwritingMessage).to.exist.and.be.a('function');
        });

        describe('When input parameter is a string', function () {

            it('should return the input string unmodified if it does not ' +
                'contain the word "Underwriting"', function () {
                var inputString = 'Four score and seven years ago.';
                result          = parser._setAcceleratedUnderwritingMessage(inputString);

                expect(result).to.equal(inputString);
            });

            it('should abbreviate "Underwriting" to "U/W" in the input parameter', function (){
                var inputString = 'I contain the word "Underwriting"!';
                var expected    = 'I contain the word "U/W"!';
                result          = parser._setAcceleratedUnderwritingMessage(inputString);

                expect(result).to.equal(expected);
            });

            it('should abbreviate "Underwriting" to "U/W" in the input parameter', function (){
                var inputString = 'Underwriting once and underwriting twice.';
                var expected    = 'U/W once and underwriting twice.';
                result          = parser._setAcceleratedUnderwritingMessage(inputString);

                expect(result).to.equal(expected);
            });

        });

        it('when input parameter is NOT a string, result is null', function () {
            result = parser._setAcceleratedUnderwritingMessage({});

            expect(result).to.be.null;
        });
    }); // _setAcceleratedUnderwritingMessage method

    describe ('_setApplicationStatusDisplayFlags method', function () {
        it('exists as a function', function () {
            expect(parser._setApplicationStatusDisplayFlags).to.exist.and.be.a('function');
        });

        it('Should add display flags to application object', function () {
            parser._setApplicationStatusDisplayFlags();
            expect(parser.application.showApplicationStatus).to.be.true;
            expect(parser.application.showIcons).to.be.true;
        });

        it('Should update application object if method has paidStatus param'+
                ' (withinDisplayPeriod: true)', function (){
            var paidStatus = {
                withinDisplayPeriod : true,
                afterDisplayPeriod  : false
            };
            parser._setApplicationStatusDisplayFlags(paidStatus);
            expect(parser.application.showApplicationStatus).to.be.true;
            expect(parser.application.showIcons).to.be.false;
        });

        it('Should update application object if method has paidStatus param'+
                ' (afterDisplayPeriod: true)', function (){
            var paidStatus = {
                withinDisplayPeriod : false,
                afterDisplayPeriod  : true
            };
            parser._setApplicationStatusDisplayFlags(paidStatus);
            expect(parser.application.showApplicationStatus).to.be.false;
            expect(parser.application.showIcons).to.be.true;
        });
    }); // _setApplicationStatusDisplayFlags method

});

/* global describe, $, _, expect */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

/* Another spec (most likely policy-detail-m-spec.js) uses the policyDetail
 * data directly and the coverageAge object is then appended to it. Removing the coverageAge
 * object before creating the copy.
 */
delete helpers.policyData.pendingPolicyDetail.coverageAge;

var cloneObj = function (obj) {
    return $.extend(true, {}, obj);
};

describe('Policy Detail Page - Coverage Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var dataCopy = cloneObj(helpers.policyData.pendingPolicyDetail);
    var policyDetailTestData;
    var rootView;

    var setView = function () {
        var newView = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });

        rootView.showChildView('contentRegion', newView);
        
        return newView;
    };

    rootView = helpers.viewHelpers.createRootView();
    rootView.render();

    describe('Main section', function () {

        describe('tests for typical pending policies', function () {

            before(function () {
                policyDetailTestData = cloneObj(dataCopy);
                ajaxStub = this.sinon.stub($, 'ajax');
            });

            beforeEach(function () {
                ajaxStub
                    .yieldsTo(
                        'success',
                        policyDetailTestData
                    );
            });

            after(function () {
                ajaxStub.restore();
            });

            describe('default pending policy view', function () {

                var view;

                before(function () {
                    view = setView();
                });

                after(function () {
                    view.destroy();
                });

                it('should display if data exists', function () {
                    expect(view.$el.find('#policy-coverage-info').length).to.equal(1);
                });

                it('heading should be correct', function () {
                    var h3Text = view.$el.find('#policy-coverage-info h3').text().trim();

                    expect(helpers.viewHelpers.removeDuplicateWhitespace(h3Text))
                        .to.equal('annual Premium Total: $51,953.34');
                });

                it('table header for fourth column should be \'Monthly Benefit\' if ' +
                    'coverage.base contains monthlyBenefit key', function () {
                    expect(view.$el.find('#policy-coverage-table th:eq(3)').text().trim())
                        .to.equal('Monthly Benefit');
                });
            });

            it('\'Issue Age\' does not display when issueAge value is not present', function () {
                var view = setView();

                // Pre-test to make sure that the checks at the end of this test are valid.
                expect(view.$el.find('#issue-age').length).above(0);
                expect(view.$el.find('#policy-detail-coverage-issue-age').length).above(0);

                // Modify the test data
                var noIssueAgeData = cloneObj(dataCopy);
                noIssueAgeData.issueAge = null;

                ajaxStub.yieldsTo(
                        'success',
                        noIssueAgeData
                    );

                view.destroy();
                view = setView();

                expect(view.$el.find('#issue-age').length).to.equal(0);
                expect(view.$el.find('#policy-detail-coverage-issue-age').length).to.equal(0);

                view.destroy();
            });

            it('\'Issue Age\' label should match for Joint Insured', function () {
                // Modify the test data
                var ia = policyDetailTestData.issueAge;
                var jea = policyDetailTestData.jointEqualAge;
                delete policyDetailTestData.issueAge; // This line makes no difference in this test
                policyDetailTestData.jointEqualAge = 'P47Y';

                var view = setView();

                expect(view.$el.find('#issue-age').text().trim()).to.equal('Joint Equal Age:');

                // Undo the modifications to the test data
                policyDetailTestData.issueAge = ia;
                policyDetailTestData.jointEqualAge = jea;

                view.destroy();
            });

            it('\'Billing Mode\' label should be hidden if there is no value ', function () {
                var view;

                // TODO: THIS TEST MIGHT NOT BE VALID. It checks that the label is missing,
                //       but there is no test that shows that the label is actually *present*. I
                //       tried adding the following lines as a "pre-test", but I could not get the
                //       pre-test to pass.   --RKC 28 Aug 2017
                // First, make sure that 'Billing Mode' is present if there *is* a value
                // var view = setView();
                // expect(view.$el.find('#policy-coverage-info #billing-mode').length).above(0);

                // Modify the test data
                var oldPaymentMode = policyDetailTestData.billingDetail.currentBilling.paymentMode;
                policyDetailTestData.billingDetail.currentBilling.paymentMode = null;

                view = setView();

                expect(view.$el.find('#policy-coverage-info #billing-mode').length).to.equal(0);

                // Undo the modifications to the test data
                policyDetailTestData.billingDetail.currentBilling.paymentMode = oldPaymentMode;

                view.destroy();
            });

            it('table header for fourth column should be \'Face Amount\' if ' +
                'coverage.base does NOT contain monthlyBenefit key', function () {
                var newCoverageBase  = [];

                // Modify the test data
                var oldCoverageBase = policyDetailTestData.coverage.base;
                var oldIsMonthlyBenefit = policyDetailTestData.coverage.isMonthlyBenefit;
                // remove any "monthlyBenefit" keys from base
                _.each(policyDetailTestData.coverage.base, function (base) {
                    newCoverageBase.push(_.omit(base, 'monthlyBenefit'));
                });
                policyDetailTestData.coverage.base = newCoverageBase;
                delete policyDetailTestData.coverage.isMonthlyBenefit;

                var view = setView();

                expect(view.$el.find('#policy-coverage-table th:eq(3)').text().trim())
                    .to.equal('Face Amount');

                // Undo the modifications to the test data
                policyDetailTestData.coverage.base = oldCoverageBase;
                policyDetailTestData.coverage.isMonthlyBenefit = oldIsMonthlyBenefit;

                view.destroy();
            });
        });

        describe('for Annuity Care', function () {

            var view;

            before(function () {
                var annuityCareCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAnnuityCare);

                ajaxStub = this.sinon.stub($, 'ajax')
                    .yieldsTo(
                        'success',
                        annuityCareCopy
                    );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });
            
            it('table header first column value should be \'Annuitant\'', function () {
                expect(view.$el.find('#policy-coverage-table th:eq(0)').text().trim())
                    .to.equal('Annuitant');
            });

            it('table column count should be three. The columns \'Face Amount\'' +
                ' and \'Annual Premium\' will not be visible', function () {
                expect(view.$el.find('#policy-coverage-table thead:eq(0) th').length)
                    .to.equal(3);
            });
        });

        describe ('display of PUA amounts column on coverage table', function () {
            var view;

            before(function () {
                dataCopy.coverage =  {
                    'base': [
                        {
                            'sequence': 1,
                            'customerId': '123',
                            'name': 'WL Legacy 121',
                            'planName': 'WL Legacy 121',
                            'faceAmount': 'USD 1.00',
                            'monthlyBenefit': 'USD 0.00',
                            'annualPremium': 'USD 1212.23'
                        }
                    ],
                    'riders': [
                        {
                            'sequence': 3,
                            'customerId': '123',
                            'name': 'Accelerator Paid Up Additions Rirder',
                            'planName': 'Accelerator Paid Up Additions Rirder',
                            'faceAmount': 'USD 1.00',
                            'monthlyBenefit': 'USD 21212.56',
                            'annualPremium': 'USD 1212.23',
                            'divPUA': 'USD 5844.25'
                        }
                    ]
                };
                ajaxStub = this.sinon.stub($, 'ajax')
                    .yieldsTo(
                        'success',
                        dataCopy
                    );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });
            
            it('There should be 5th column for PUA Amount', function () {
                expect(view.$el.find('#policy-coverage-table thead th:eq(4)').text().trim())
                    .to.equal('PUA from Dividends');
            });

            it('total column should be 6', function () {
                expect(view.$el.find('#policy-coverage-table thead th').length)
                    .to.equal(6);
            });

            it('Amount should be formatted as currency(5th col of 2nd row of table)', function () {
                expect(view.$el.find('#policy-coverage-table tbody tr:eq(1) td:eq(4)').text())
                    .to.contain('$5,844.25');
            });

            describe ('for Annuitant', function () {
                it('column count shoud be 4', function () {
                    dataCopy.product.productTypeCategory = 'Life';
                    dataCopy.product.productTypeCode ='SPFA';
                    view = setView();
                    expect(view.$el.find('#policy-coverage-table thead th').length)
                    .to.equal(4);
                });
            });
        });

        it('disclaimer note and asterisk with \'Annual Premium\' column header ' +
            'should be visible if product, workflow status, status and ' +
            'underwritingRequired as per requirement', function () {
            var assetCareCopy = cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

            ajaxStub = this.sinon.stub($, 'ajax')
                .yieldsTo(
                    'success',
                    assetCareCopy
                );

            var view = setView();

            expect(view.$el.find('#policy-coverage-table thead:eq(0) th:eq(4)').text())
                .to.equal('Annual Premium*');
            // use the "next sibling" selector to find the
            expect(view.$el.find('#note-container').length).to.equal(1);

            ajaxStub.restore();
            view.destroy();
        });

        it('should not display if data does not exist', function () {
            var awdRipCopy = cloneObj(helpers.policyData.pendingPolicyDetailAWDRip);

            ajaxStub = this.sinon.stub($, 'ajax')
                .yieldsTo(
                    'success',
                    awdRipCopy
                );

            var view = setView();

            expect(view.$el.find('.policy-coverage-info').length).to.equal(0);

            ajaxStub.restore();
            view.destroy();
        });
    }); // Main section

    describe('Related Polices details under coverage section', function () {

        var $relatedPoliciesList;
        var view;

        before(function () {
            policyDetailTestData = cloneObj(helpers.policyData.pendingPolicyDetail);
            ajaxStub = this.sinon.stub($, 'ajax')
                .yieldsTo(
                    'success',
                    policyDetailTestData
                );

            view = setView();

            $relatedPoliciesList = view.$el.find('#policy-coverage-info ul');
        });

        after(function () {
            ajaxStub.restore();
            view.destroy();
        });

        it('Element should exist', function () {
            expect($relatedPoliciesList.length).to.be.at.least(1);
        });

        it('Heading should match based on policy quantity (1 and 2)', function () {
            expect($relatedPoliciesList.prev('h4').text().trim())
                .to.equal('Related Policies');
        });

        it('Element should contain two policy links', function () {
            expect($relatedPoliciesList.find('a').length).to.equal(2);
        });

        it('Policy links value should match', function () {
            expect(view.$el.find('#policy-coverage-info ul a:eq(0)').text().trim())
                .to.equal('9558845545 – Asset-Care IV');
            expect(view.$el.find('#policy-coverage-info ul a:eq(1)').text().trim())
                .to.equal('9558845784 – Continuation of Benefit Rider');
        });

        it('"Annual Premium" should be displayed for policy 1', function () {
            var text = view.$el.find('#policy-coverage-info ul:eq(0) span:eq(0)').text().trim();
            text = helpers.viewHelpers.removeDuplicateWhitespace(text);

            expect(text).to.contain('Annual Premium:');
        });

    }); // Related Polices details under coverage section

    describe('Rating Information under "Coverage" section', function () {

        describe('will be displayed', function () {

            var view;

            before(function () {
                var policyDetailCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    policyDetailCopy
                );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });

            it('when productCategory is "Life", uwDecision is a date, and ' +
                'ratingInformation is present in the response from the service', function () {

                expect(view.$el.find('#ratings').length).to.equal(1);
            });

            it('when productCategory is "Care Solutions", uwDecision is a date, ' +
                'and ratingInformation is present in the response from the service', function () {

                expect(view.$el.find('#rating-information').length).to.equal(1);
            });
        });

        describe('will NOT be displayed', function () {

            var policyDetailCopy;

            before(function () {
                policyDetailCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

                ajaxStub = this.sinon.stub($, 'ajax');
            });

            after(function () {
                ajaxStub.restore();
            });

            it('when underwritingRequired is false', function () {
                policyDetailCopy.product.underwritingRequired = false;
                ajaxStub.yieldsTo('success', policyDetailCopy);
                var view = setView();

                expect(view.$el.find('#rating-information').length).to.equal(0);

                view.destroy();
            });

            it('when ratingInformation is not returned from the service', function () {
                delete policyDetailCopy.ratingInformation;
                ajaxStub.yieldsTo('success', policyDetailCopy);
                var view = setView();

                expect(view.$el.find('#rating-information').length).to.equal(0);

                view.destroy();
            });
        });

        describe('will indicate the insured\'s tobacco use', function () {

            var view;

            before(function () {
                var assetCareCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    assetCareCopy
                );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });

            it('will display "Non-Tobacco" when insured does not use tobacco', function () {
                var rateClassCellContent = view.$('#ratingsTable tbody tr')
                    .eq(0).children('td').eq(1).text().trim();

                expect(rateClassCellContent).to.include('Non-Tobacco');
            });

            it('will display "Tobacco" when insured does use tobacco', function () {
                var rateClassCellContent = view.$('#ratingsTable tbody tr')
                    .eq(1).children('td').eq(1).text().trim();

                expect(rateClassCellContent).to.include('Tobacco');
            });

            it('will not display "Non-Tobacco" when the "tobaccoUse" property is not ' +
                'present', function () {
                var rateClassCellContent = view.$('#ratingsTable tbody tr')
                    .eq(2).children('td').eq(1).text().trim();

                // this will match "Non-Tobacco" as well.
                expect(rateClassCellContent).to.not.include('Tobacco');
            });
        });

        describe('"Asset Care"', function () {

            var view;

            before(function () {
                var assetCareCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    assetCareCopy
                );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });

            it('will display "Coverage PlanName" if present in the data', function () {
                var expectedResult = helpers.policyData.pendingPolicyDetailAssetCare
                    .ratingInformation[1].coverageRatings[0].coveragePlanName;
                var jointInsuredCoverageDescriptionCell = view.$('#ratingsTable tbody tr')
                    .eq(1).children('td').eq(2).text().trim();

                expect(jointInsuredCoverageDescriptionCell).to.equal(expectedResult);
            });

            it('will display "Mortality Table" if present in the data', function () {
                var expectedResult = helpers.policyData.pendingPolicyDetailAssetCare
                    .ratingInformation[1].coverageRatings[0].mortalityTable;
                var jointInsuredMortalityTableCell = view.$('#ratingsTable tbody tr')
                    .eq(1).children('td').eq(3).text().trim();

                expect(jointInsuredMortalityTableCell).to.equal(expectedResult);
            });

            it('will display "Morbidity Table" if present in the data', function () {
                var expectedResult = helpers.policyData.pendingPolicyDetailAssetCare
                    .ratingInformation[1].coverageRatings[0].morbidityTable;
                var jointInsuredMorbidityTableCell = view.$('#ratingsTable tbody tr')
                    .eq(1).children('td').eq(4).text().trim();

                expect(jointInsuredMorbidityTableCell).to.equal(expectedResult);
            });

            it('will display "Rated Age" if present in the data', function () {
                var ratedAgeCell = view.$('#ratingsTable tbody tr')
                    .eq(1).children('td').eq(5).text().trim();

                expect(ratedAgeCell).to.equal('99');
            });
        });

        describe('"Non Asset Care" Risk data table layout', function () {
            var ratingInformation = [
                {
                    customerId  : 123,
                    underwritingClass: 'Sub-Standard',
                    tobaccoUse: false,
                    ratedAge  : 99,
                    coverageRatings : [
                        {
                            coveragePlanName   : '20 Year Level Term',
                            coverageSequence   : 1,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'
                        },{
                            coveragePlanName   : '20 Year Level Term',
                            coverageSequence   : 2,
                            ratingType         : 'Flat Extra',
                            flatExtraAmount    : 'USD 2.50',
                            duration           : 'P3Y',
                            tableName          : null
                        },{
                            coveragePlanName   : 'Rider 1',
                            coverageSequence   : 3,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'
                        },{
                            coveragePlanName   : 'Rider 2',
                            coverageSequence   : 4,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'
                        }
                    ]
                },
                {
                    customerId  : 456,
                    underwritingClass : 'Sub-Standard',
                    tobaccoUse: true,
                    ratedAge  : 99,
                    coverageRatings : []
                }
            ];
            var policyDetailCopy;
            var view;

            before(function () {
                policyDetailCopy =
                    cloneObj(helpers.policyData.pendingPolicyDetailAssetCare);

                policyDetailCopy.carrierCode = 'AUL';
                policyDetailCopy.product.productCategory = 'Life';
                policyDetailCopy.ratingInformation = ratingInformation;

                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    policyDetailCopy
                );

                view = setView();
            });

            after(function () {
                ajaxStub.restore();
                view.destroy();
            });

            it('should exist columns : "Coverage Description", "Rating Type", '+
                '"Flat Extra / $1000" "Duration (yrs)", "Table Name" ', function () {
                var tableHeadingColumns = view.$el.find('#ratingsTable thead th');

                expect($(tableHeadingColumns[2]).text()).to.equal('Coverage Description');
                expect($(tableHeadingColumns[3]).text()).to.equal('Rating Type');
                expect($(tableHeadingColumns[4]).text()).to.equal('Flat Extra / $1,000');
                expect($(tableHeadingColumns[5]).text()).to.equal('Duration (yrs)');
                expect($(tableHeadingColumns[6]).text()).to.equal('Table Name');
            });

            it('will display "Coverage PlanName" if data is present', function () {
                var expectedResult = policyDetailCopy.ratingInformation[0].coverageRatings[0]
                    .coveragePlanName;
                var descriptionTableCellContent = view.$el.find('#ratingsTable tbody tr')
                    .eq(0).children('td').eq(2).text().trim();

                expect(descriptionTableCellContent).to.equal(expectedResult);
            });

            it('will display "Rating Type" if data is present', function () {
                var expectedResult = policyDetailCopy.ratingInformation[0].coverageRatings[0]
                    .ratingType;
                var ratingTypeCellContent = view.$el.find('#ratingsTable tbody tr')
                    .eq(0).children('td').eq(3).text().trim();

                expect(ratingTypeCellContent).to.equal(expectedResult);
            });

            describe('will display "Flat Extra"', function () {
                it('as an empty cell if property is null', function () {
                    var ratingTypeCellContent = view.$el.find('#ratingsTable tbody tr')
                        .eq(0).children('td').eq(4).text().trim();

                    expect(ratingTypeCellContent).to.equal('');
                });

                it('if data is present', function () {
                    var ratingTypeCellContent = view.$el.find('#ratingsTable tbody tr')
                        .eq(1).children('td').eq(4).text().trim();

                    expect(ratingTypeCellContent).to.equal('$2.50');
                });
            });

            it('will display "Duration" if data is present', function () {
                var durationCellContent = view.$el.find('#ratingsTable tbody tr')
                    .eq(0).children('td').eq(5).text().trim();

                expect(durationCellContent).to.equal('20');
            });

            describe('will display "Table Name"', function () {
                it('as an empty cell if property is null', function () {
                    var durationCellContent = view.$el.find('#ratingsTable table tbody tr')
                        .eq(1).children('td').eq(6).text().trim();

                    expect(durationCellContent).to.equal('');
                });

                it('if data is present', function () {
                    var expectedResult = policyDetailCopy.ratingInformation[0].coverageRatings[0]
                        .tableName;
                    var durationCellContent = view.$el.find('#ratingsTable tbody tr')
                        .eq(0).children('td').eq(6).text().trim();

                    expect(durationCellContent).to.equal(expectedResult);
                });
            });
        });
    }); // Rating Information under "Coverage" section

    describe('"Section unavailable" note', function () {
        var policyDetailCopy;
        var view;

        before(function () {
            policyDetailCopy = cloneObj(helpers.policyData.pendingPolicyDetail);
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDetailCopy
            );
            view = setView();
        });

        after(function () {
            ajaxStub.restore();
            view.destroy();
        });

        it ('should not display if coverage doesn\'t have '+
                'dataAvailability property', function () {
            expect(view.$el.find('#coverage-section-unavailable')).to.be.lengthOf(0);
        });

        it ('should display if coverage has dataAvailability '+
                'property with value notAvailable', function () {
            policyDetailCopy.coverage = {
                dataAvailability: 'notAvailable'
            };

            ajaxStub.yieldsTo(
                'success',
                policyDetailCopy
            );
            
            view = setView();
            expect(view.$el.find('#coverage-section-unavailable')).to.be.lengthOf(1);
        });
    }); // "Section unavailable" note
});

/* global describe:false, Backbone:false, expect:false */

var PolicySearchFormView      = require('../dist/pages/policy/views/policy-search-form-v');
var PolicySearchFormViewModel = require('../dist/pages/policy/viewModels/policy-search-form-vm');

describe('Policy Search Form View (pages/policy/views/policy-search-form-v.js)', function () {

    it('policy-search-form-v exists', function () {
        expect(PolicySearchFormView).to.exist;
    });

    /*
     * Tests without the search type radio selection
     */
    describe('policy-search-form-v tests with bottom-feeder user', function () {

        var userChannel;
        beforeEach (function () {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function(capability) {
                if (capability === 'Policy_Search_by_Producer' 
                    || capability === 'HO_ViewAsProducer') {
                    return true;
                } else {
                    return false;
                }
            });
        });

        afterEach (function () {
            userChannel.stopReplying();
            userChannel = null;
        });

        it('policy-search-form-v initialization creates a default', function () {
            var view = new PolicySearchFormView();

            expect(view.model).to.exist;
            expect(view.model.get('searchTerm')).to.equal('');

            // Default search is policy number
            expect(view.model.get('searchType')).to.equal('policyNumber');
        });

        it('_updateModel function correctly updates the model properties', function () {
            var input = 'MONKEY!';
            var view  = new PolicySearchFormView({
                el          : 'body'
            });

            // initially empty string
            expect(view.model.get('searchTerm')).to.equal('');

            view.render();

            // set val in the UI, and call _updateModel()
            view.ui.searchTerm.val(input);
            view._updateModel();

            expect(view.model.get('searchTerm')).to.equal(input);
        });

        it('Does not display errors when none exist in model', function () {
            var view = new PolicySearchFormView({
                el       : 'body',
                model    : new PolicySearchFormViewModel({
                    searchTerm  : 'Fred'
                })
            });

            view.render();
            var formGroup = view.$el.find('#pending-search-term-group');
            var termError = view.$el.find('#pending-search-term-error');

            expect(formGroup.hasClass('has-error')).to.equal(false);
            expect(termError.hasClass('hide')).to.equal(true);
        });

        it('Displays search term error if present', function () {
            var invalidMessage = 'Fix your input, punk!';
            var view           = new PolicySearchFormView({
                el       : 'body',
                model    : new PolicySearchFormViewModel({
                    searchTerm          : 'Classy Freddie Blassie',
                    searchTermErrorText : invalidMessage
                })
            });

            view.render();
            var formGroup = view.$el.find('#pending-search-term-group');
            var termError = view.$el.find('#pending-search-term-error');

            expect(formGroup.hasClass('has-error')).to.equal(true);
            expect(termError.hasClass('hide')).to.equal(false);
            expect(termError.text().trim()).to.equal(invalidMessage);
        });

        it('_search function "executes" search for valid input', function () {
            var view = new PolicySearchFormView({
                el        : 'body',
                model     : new PolicySearchFormViewModel({
                    searchTerm  : 'Fred'
                })
            });

            var updateModelSpy = this.sinon.spy(view, '_updateModel');
            var validatorSpy   = this.sinon.spy(view.model, 'isValid');
            var renderSpy      = this.sinon.spy(view, 'render');
            var triggerSpy     = this.sinon.spy(view, 'trigger');

            view.render();

            // reset the render spy so we don't count the above render in the below check
            renderSpy.resetHistory();

            view._search();

            expect(updateModelSpy).to.have.been.calledOnce;
            expect(validatorSpy).to.have.been.calledOnce;
            expect(renderSpy).to.have.been.calledOnce;
            expect(triggerSpy).to.have.been.calledWith('clientNameSearch', 'Fred');

            updateModelSpy.restore();
            renderSpy.restore();
            validatorSpy.restore();
            triggerSpy.restore();
        });
        
        it('_search function "executes" search for valid polnum input', function () {
            var view = new PolicySearchFormView({
                el        : 'body',
                model     : new PolicySearchFormViewModel({
                    searchTerm  : '1234567890'
                })
            });

            var updateModelSpy = this.sinon.spy(view, '_updateModel');
            var validatorSpy   = this.sinon.spy(view.model, 'isValid');
            var renderSpy      = this.sinon.spy(view, 'render');
            var triggerSpy     = this.sinon.spy(view, 'trigger');

            view.render();

            // reset the render spy so we don't count the above render in the below check
            renderSpy.resetHistory();

            view._search();

            expect(updateModelSpy).to.have.been.calledOnce;
            expect(validatorSpy).to.have.been.calledOnce;
            expect(renderSpy).to.have.been.calledOnce;
            expect(triggerSpy).to.have.been.calledWith('policyNumberSearch', '1234567890');

            updateModelSpy.restore();
            renderSpy.restore();
            validatorSpy.restore();
        });

        it('_search function "executes" search for valid producer number input', function () {
            var view = new PolicySearchFormView({
                el        : 'body',
                model     : new PolicySearchFormViewModel({
                    searchTerm  : '123456789'
                })
            });

            var updateModelSpy = this.sinon.spy(view, '_updateModel');
            var validatorSpy   = this.sinon.spy(view.model, 'isValid');
            var renderSpy      = this.sinon.spy(view, 'render');
            var triggerSpy     = this.sinon.spy(view, 'trigger');

            view.render();

            // set radio for producer number
            var producerNumber = 'producerNumber';
            view.ui.searchOption.filter('[value='+producerNumber+']').attr('checked', true);

            // reset the render spy so we don't count the above render in the below check
            renderSpy.resetHistory();

            view._search();

            expect(updateModelSpy).to.have.been.calledOnce;
            expect(validatorSpy).to.have.been.calledOnce;
            expect(renderSpy).to.have.been.calledOnce;
            expect(triggerSpy).to.have.been.calledWith('producerNumberSearch', '123456789');

            updateModelSpy.restore();
            renderSpy.restore();
            validatorSpy.restore();
            triggerSpy.restore();
        });

        it('_search function does not search when input invalid', function () {
            var view = new PolicySearchFormView({
                el        : 'body',
                model     : new PolicySearchFormViewModel({
                    searchTerm  : 'Classy Freddie Blassie'
                })
            });

            var fakeEvent = this.sinon.stub({
                preventDefault: function () {
                    return true;
                }
            });

            var updateModelSpy = this.sinon.spy(view, '_updateModel');
            var validatorSpy   = this.sinon.spy(view.model, 'isValid');
            var renderSpy      = this.sinon.spy(view, 'render');

            view.render();

            // reset the render spy so we don't count the above render in the below check
            renderSpy.resetHistory();

            view._search(fakeEvent);

            expect(updateModelSpy).to.have.been.calledOnce;
            expect(validatorSpy).to.have.been.calledOnce;
            expect(renderSpy).to.have.been.calledOnce;
            expect(fakeEvent.preventDefault).to.have.been.calledOnce;

            updateModelSpy.restore();
            renderSpy.restore();
            validatorSpy.restore();
        });

        it('_search function should call analytics if it\'s a form submission', function () {
            var analyticsChannel = Backbone.Radio.channel('analytics');
            var trackActionStub = this.sinon.stub();
            analyticsChannel.on('trackAction', trackActionStub);
            var view = new PolicySearchFormView({
                el        : 'body',
                model     : new PolicySearchFormViewModel({
                    searchTerm  : '1234567890'
                })
            });
            view.render();
            view.ui.searchForm.submit();
            expect(trackActionStub).to.have.been.called;
            
            analyticsChannel.off('trackAction', trackActionStub);
            trackActionStub = null;
        });
    }); // bottom-feeder user

    /*
     *  Tests with the search type radio selection
     */
    describe('policy-search-form-v tests with office view user', function () {

        var userChannel;
        var userRadioRequestStub;

        beforeEach (function () {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function(capability) {
                if (capability === 'Policy_Search_by_Producer' 
                    || capability === 'HO_ViewAsProducer') {
                    return true;
                } else {
                    return false;
                }
                
            });
            userRadioRequestStub = this.sinon.stub(userChannel._requests.hasCapability, 'callback');
            userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(true);
            userRadioRequestStub.withArgs('HO_ViewAsProducer').returns(true);
        });

        afterEach (function () {
            userChannel.stopReplying();
            userChannel = null;
            userRadioRequestStub.restore();
        });

        it('policy-search-form-v initialization creates a default model', function () {

            var view = new PolicySearchFormView();

            expect(view.model).to.exist;
            expect(view.model.get('searchTerm')).to.equal('');
            expect(view.model.get('searchType')).to.equal('policyNumber');
        });

        it('initialization should set flag for '+
                'hasHOViewAsProducer and hasPolicySearchByProducer', function () {

            var view = new PolicySearchFormView();

            expect(view.model.get('hasHOViewAsProducer')).to.be.true;
            expect(view.model.get('hasPolicySearchByProducer')).to.be.true;
        });

        it('initialization sets model\'s \'searchType\' to \'policyNumber\' if falsy', function () {
            var searchTerm = 'Meaning of Life';
            var view       = new PolicySearchFormView({
                model: new PolicySearchFormViewModel({
                    searchTerm : searchTerm
                })
            });

            expect(view.model).to.exist;
            expect(view.model.get('searchTerm')).to.equal(searchTerm);
            expect(view.model.get('searchType')).to.equal('policyNumber');
        });

        it('initialization does not override model\'s \'searchType\' if set', function () {
            var searchTerm = 'Meaning of Life';
            var searchType = 'clientName';
            var view       = new PolicySearchFormView({
                model: new PolicySearchFormViewModel({
                    searchTerm : searchTerm,
                    searchType : searchType
                })
            });

            expect(view.model).to.exist;
            expect(view.model.get('searchTerm')).to.equal(searchTerm);
            expect(view.model.get('searchType')).to.equal(searchType);
        });

        it('_updateModel function correctly updates the model properties', function () {
            var input         = 'GIRAFFE!';
            var typeSelection = 'producerName';

            var view = new PolicySearchFormView({
                el : 'body'
            });

            // initially empty string
            expect(view.model.get('searchTerm')).to.equal('');
            expect(view.model.get('searchType')).to.equal('policyNumber');

            view.render();

            expect(view.ui.searchOption.filter(':checked').val())
                .to.equal('policySearch');

            // set val in the UI, and call _updateModel()
            view.ui.searchTerm.val(input);
            view.ui.searchOption.filter('[value="producerName"]').prop('checked', true);
            view._updateModel();

            expect(view.model.get('searchTerm')).to.equal(input);

            expect(view.model.get('searchType')).to.equal(typeSelection);
        });
    }); // office-view user

});

/* global expect:false, sinon:false, Backbone:false */

var GoogleTagManagerModule =
        require('../dist/modules/analytics/providers/googleTagManagerProvider');

var helpers   = require('./helpers/helpers');

describe('Google Tag Manager data layer array', function () {

    it('is available on the window object', function () {
        expect(window.gtmDataLayer).to.exist;
    });

    it('is an array', function () {
        expect(window.gtmDataLayer).to.be.an('array');
    });
});

describe('Google Tag Manager Provider ' +
    '(modules/analytics/providers/googleTagManagerProvider.js)', function () {
    var module;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        module = new GoogleTagManagerModule();
    });

    after(function () {
        module = null;
    });

    describe('initialize method', function () {

        it('should attach window.gtmDataLayer to the module', function () {
            expect(module.gtmDataLayer).to.deep.equal(window.gtmDataLayer);
        });

    }); // initialize method

    describe('setDataLayerValue method', function () {

        describe('Will throw an error', function () {

            it('if not passed an options parameter', function () {
                var fn = function () {
                    module.setDataLayerValue();
                };
                expect(fn).to.throw(module.errors.setDataLayerValueObject);
            });

            it('if the parameter passed is not an object', function () {
                var fn = function () {
                    module.setDataLayerValue('The quick brown fox');
                };
                expect(fn).to.throw(module.errors.setDataLayerValueObject);
            });
        });

        it('will add the object passed to the method to the gtmDataLayer array', function () {
            var optionsObject = {
                'User ID' : 'Darth Vader',
                'Role'    : 'Sith Lord',
                'Channel' : 'Dark Side'
            };

            // Ensure the object is not already in gtmDataLayer array
            expect(optionsObject).not.to.be.oneOf(window.gtmDataLayer);

            module.setDataLayerValue(optionsObject);

            expect(optionsObject).to.be.oneOf(window.gtmDataLayer);
        });

    }); // setDataLayerValue method

    
    describe ('`unsetDataLayerValue()` method', function () {
        
        it('should exist', function () {
            expect(module.unsetDataLayerValue).to.exist.and.be.a('function');
        });
        
        describe('Will throw an error', function () {

            it('if not passed an options parameter', function () {
                var fn = function () {
                    module.unsetDataLayerValue();
                };
                expect(fn).to.throw(module.errors.unsetDataLayerValueObject);
            });

            it('if the parameter passed is not an object', function () {
                var fn = function () {
                    module.unsetDataLayerValue('The quick brown fox');
                };
                expect(fn).to.throw(module.errors.unsetDataLayerValueObject);
            });
        });

        it('All dataLayer fields added should set undefined', function () {
            var dataLayerValues = {
                'Policy Product Type' : 'SL Asset Care',
                'policy Status' : 'In Underwriting',
                'Case ID' : '2018-02-26-12.05.22.666555'
            };

            var expectedObject = {
                'Policy Product Type' : undefined,
                'policy Status' : undefined,
                'Case ID' : undefined
            };
            var gtmDataLayer = window.gtmDataLayer;
            expect(expectedObject).not.to.be.oneOf(gtmDataLayer);
            module.setDataLayerValue(dataLayerValues);
            module.unsetDataLayerValue(dataLayerValues);
            expect(expectedObject).to.be.deep.equal(gtmDataLayer[gtmDataLayer.length-1]);
        });
    });

    describe('trackException method', function () {

        describe('will throw an error if parameter passed', function () {

            it('is not an object', function () {
                var fn = function () {
                    module.trackException('I am not an object');
                };
                expect(fn).to.throw(module.errors.trackExceptionObject);
            });

            it('does not have a "message" attribute', function () {
                var fn  = function () {
                    module.trackException({ foo : 'bar' });
                };
                expect(fn).to.throw(module.errors.trackExceptionObject);
            });

            it('the "message" attribute is not a string value', function () {
                var fn = function () {
                    module.trackException({ message : 42 });
                };
                expect(fn).to.throw(module.errors.trackExceptionObject);
            });
        }); // Will throw an error if parameter passed

        describe('Will call setDataLayerValue method', function () {
            var optionsObj = { message : 'It\'s a cookbook!' };
            var setDataLayerValueStub;

            beforeEach(function () {
                setDataLayerValueStub = this.sinon.stub(module, 'setDataLayerValue');
            });

            afterEach(function () {
                setDataLayerValueStub.restore();
            });

            it('options.event will be set to "exception"', function () {
                var eventValue;
                module.trackException(optionsObj);

                eventValue = setDataLayerValueStub.getCalls()[0].args[0].event;
                expect(eventValue).to.equal('exception');
            });

            it('options.exceptionDescription will be set to the value of "message"', function () {
                var exDescValue;
                module.trackException(optionsObj);

                exDescValue = setDataLayerValueStub.getCalls()[0].args[0].exceptionDescription;

                expect(exDescValue).to.equal(optionsObj.message);
            });

            describe('options.exceptionFatal value', function () {

                it('will be false if not explicitly passed in ' +
                    'options param to trackException', function () {
                    var exFatalValue;
                    module.trackException(optionsObj);

                    exFatalValue = setDataLayerValueStub.getCalls()[0].args[0].exceptionFatal;

                    expect(exFatalValue).to.be.false;
                });

                it('will be false if options.fatal passed to trackException is false', function () {
                    var exFatalValue;
                    optionsObj.fatal = false;
                    module.trackException(optionsObj);

                    exFatalValue = setDataLayerValueStub.getCalls()[0].args[0].exceptionFatal;

                    expect(exFatalValue).to.be.false;
                });

                it('will be false if options.fatal passed to trackException is not a boolean value',
                    function () {
                    var exFatalValue;
                    optionsObj.fatal = 'not a boolean value';
                    module.trackException(optionsObj);

                    exFatalValue = setDataLayerValueStub.getCalls()[0].args[0].exceptionFatal;

                    expect(exFatalValue).to.be.false;
                });

                it('will be true if options.fatal passed to trackException is true', function () {
                    var exFatalValue;
                    optionsObj.fatal = true;
                    module.trackException(optionsObj);

                    exFatalValue = setDataLayerValueStub.getCalls()[0].args[0].exceptionFatal;

                    expect(exFatalValue).to.be.true;
                });
            });

        }); // Will call setDataLayerValue method

    }); // trackException method

    describe('trackView method', function () {

        it('should exist as a function', function () {
            expect(module.trackView).to.exist.and.be.a('function');
        });

        it('should call setDataLayer method with `newViewEvent`', function () {
            var setDataLayerValueStub = this.sinon.stub(module, 'setDataLayerValue');
            var expected              = { event : 'newViewEvent' };

            module.trackView();

            expect(setDataLayerValueStub.getCalls()[0].args[0]).to.deep.equal(expected);
        });

        it('should listen to dataFetched event when there is a '+
            'view parameter and uri is "pending-policy-detail"', function () {
            var rootView = helpers.viewHelpers.createRootView();
            var View = Backbone.Marionette.ItemView.extend({
                template : '<div></div>',
                initialize : function () {
                    var Model = Backbone.Model.extend({
                        url:'/some/data'
                    });

                    this.model = new Model();
                },
                onBeforeShow : function () {
                    var _this = this;
                    this.model.fetch({

                        //to hide wait indicator
                        success : function () {
                            _this.dataLayerValues = {
                                myData : 'foo'
                            };
                            _this.trigger('dataFetched');
                        }
                    });
                }
            });

            var ajaxStub = this.sinon.stub(Backbone.$, 'ajax').yieldsTo(
                'success',
                {some:'data'}
            );
            var setDataLayerValueSpy = this.sinon.spy(module, 'setDataLayerValue');
            var view=  new View();
            var options = {
                uri  : 'pending-policy-detail',
                view : view
            };
            
            module.trackView(options);
            rootView.render();
            rootView.showChildView('contentRegion', view);
            
            expect(setDataLayerValueSpy).to.have.been.calledOnce;

            rootView.destroy();
            view.destroy();
            ajaxStub.restore();
        });

    }); // trackView method
});
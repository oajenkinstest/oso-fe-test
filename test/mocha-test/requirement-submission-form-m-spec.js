/* global expect:false, Backbone:false, $:false, _:false */



var RequirementSubmissionModel =
    require('../dist/pages/requirementSubmission/models/requirement-submission-form-m');

describe('Requirement Submission Form Model ' +
    '(pages/requirementSubmission/models/requirement-submission-form-m.js)', function () {
    var model;

    var files = [
        {
            name    : 'File1',
            type    : 'application/pdf',
            size    : 123456,
            inputId : 'file1'
        },{
            name    : 'File2',
            type    : 'application/pdf',
            size    : 123456,
            inputId : 'file2'
        }

    ];

    var file = {
        name    : 'File3',
        type    : '',
        size    : 654321,
        inputId : 'file3'
    };

    var formObj;

    var invalidChars      = [ '<', '>' ];

    var validFileTypes = [ 'application/pdf' ];

    beforeEach(function () {
        model = new RequirementSubmissionModel({
            policyId                  : 1234567890,
            maxCommentsLength         : 10,
            maxFileSizeInMegabytes    : 5,
            invalidCommentsCharacters : invalidChars,
            validFileTypes            : validFileTypes
        });

        formObj = {
            comments                 : '',
            creditCard               : false,
            files                    : []
        };
    });

    it('model object should exists', function () {
        expect(RequirementSubmissionModel).to.exist;
    });

    describe('preValidateFile method', function () {
        var isDuplicateFileStub;
        var isValidTotalFileSizeStub;
        var isValidTypeStub;
        var sendFormErrorsToAnalyticsStub;

        beforeEach(function () {
            isDuplicateFileStub      = this.sinon.stub(model, '_isDuplicateFile').returns(false);
            isValidTotalFileSizeStub = this.sinon.stub(model, '_isValidTotalFileSize').returns(true);
            isValidTypeStub          = this.sinon.stub(model, '_isValidFileType').returns(true);

            sendFormErrorsToAnalyticsStub
                = this.sinon.stub(model.utils, 'sendFormErrorsToAnalytics');
        });

        afterEach(function () {
            isDuplicateFileStub.restore();
            isValidTotalFileSizeStub.restore();
            isValidTypeStub.restore();
            sendFormErrorsToAnalyticsStub.restore();
        });

        it('should be a function', function () {
            expect(model.preValidateFile).to.be.a('function');
        });

        it('should call _isValidFileType()', function () {
            model.preValidateFile(files, file);
            expect(isValidTypeStub).to.have.been.calledOnce;
        });

        it('should call _isDuplicateFile()', function () {
            model.preValidateFile(files, file);
            expect(isDuplicateFileStub).to.have.been.calledOnce;
        });

        it('should call _isValidTotalFileSize()', function () {
            model.preValidateFile(files, file);
            expect(isValidTotalFileSizeStub).to.have.been.calledOnce;
        });

        it('should call utils.sendFormErrorsToAnalytics() when an error is found', function () {
            isDuplicateFileStub.returns(true);
            model.preValidateFile(files, file);
            expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
        });

        it('should not call utils.sendFormErrorsToAnalytics() when an error is ' +
            'not found', function () {
            model.preValidateFile(files, file);
            expect(sendFormErrorsToAnalyticsStub).not.to.have.been.called;
        });

        describe('should set "fileErrorText" property', function () {

            it('when an invalid file type is passed as the second parameter', function () {

                var expectedMessage = model.errors.invalidFileType;
                isValidTypeStub.returns(false);

                model.preValidateFile(files, file);

                expect(model.get('fileErrorText')).to.equal(expectedMessage);
            });

            it('when the fileToBeAdded has a size of 0', function () {
                var expectedMessage = model.errors.emptyFile;
                isValidTypeStub.returns(true);
                isDuplicateFileStub.returns(false);
                isValidTotalFileSizeStub.returns(true);
                file.size = 0;
                model.preValidateFile(files, file);

                expect(model.get('fileErrorText')).to.equal(expectedMessage);
            });

        });

        it('should return FALSE if _isValidFileType() fails', function () {
            var outputValue;

            isValidTypeStub.returns(false);
            isDuplicateFileStub.returns(false);

            outputValue = model.preValidateFile(files, file);

            expect(outputValue).to.be.false;
        });

        it('should set the "fileErrorText" property if the second argument ' +
            'has a file name and size equal to that of one of the files in ' +
            'the first argument', function () {
            var expectedMessage = model.errors.duplicateFile;

            isValidTypeStub.returns(true);
            isDuplicateFileStub.returns(true);

            model.preValidateFile(files, file);

            expect(model.get('fileErrorText')).to.equal(expectedMessage);
        });

        it('should return FALSE if _isDuplicateFile() returns TRUE', function () {
            var outputValue;
            isDuplicateFileStub.returns(true);

            outputValue = model.preValidateFile(files, file);

            expect(outputValue).to.be.false;
        });

        it('should set "fileErrorText" property if _isValidTotalFileSize() returns FALSE',
            function () {
            var expectedMessage = model.errors.totalFileSize;

            isValidTotalFileSizeStub.returns(false);

            model.preValidateFile(files, file);

            expect(model.get('fileErrorText')).to.equal(expectedMessage);
        });

        it('should return FALSE if _isValidTotalFileSize() returns FALSE', function () {
            var outputValue;
            isValidTotalFileSizeStub.returns(false);

            outputValue = model.preValidateFile(files, file);

            expect(outputValue).to.be.false;
        });

        describe('When _isValidFileType() is TRUE AND _isDuplicateFile() returns FALSE ' +
            'AND isValidTotalFileSize() returns TRUE AND fileToBeAdded has a ' +
            'size GREATER THAN 0', function () {

            var output;

            beforeEach(function () {
                isValidTypeStub.returns(true);
                isDuplicateFileStub.returns(false);
                isValidTotalFileSizeStub.returns(true);

                file.size = 1;

                output = model.preValidateFile(files, file);
            });

            it('should unset "formErrorText"', function () {
                expect(model.has('formErrorText')).to.be.false;
            });

            it('should unset "creditCardErrorText" property', function () {
                expect(model.has('creditCardErrorText')).to.be.false;
            });

            it('should return TRUE', function () {
                expect(output).to.be.true;
            });

        });

    }); // preValidateFile method

    describe('sync method', function () {

        it('should call _getFormData method', function () {
            var ajaxStub        = this.sinon.stub($, 'ajax');
            var getFormDataStub = this.sinon.stub(model, '_getFormData');

            model.sync('create', model);

            expect(getFormDataStub).to.have.been.calledOnce;

            getFormDataStub.restore();
            ajaxStub.restore();
        });

    });     // sync method

    describe('urlRoot method', function () {

        it('should return correct url', function () {

            expect(model.url()).to.equal(
                '/api/oso/secure/rest/policies/1234567890/requirements'
            );
        });

        it('should throw an error if "policyId" is not set', function () {
            var expectedError = model.errors.missingPolicyId;
            var fn = function () {
                model.unset('policyId');
                model.url();
            };

            expect(fn).to.throw(expectedError);
        });

    });     // urlRoot method

    describe('validate method', function () {
        var errorObj;
        var expectedKey;
        var expectedValue;
        var sendFormErrorsToAnalyticsStub;

        beforeEach(function () {
            sendFormErrorsToAnalyticsStub =
                this.sinon.stub(model.utils, 'sendFormErrorsToAnalytics');
        });

        afterEach(function () {
            sendFormErrorsToAnalyticsStub.restore();
        });

        describe('if required elements on the form are not present', function () {

            beforeEach(function () {
                expectedKey = 'formErrorText';
                expectedValue =  model.errors.emptyForm;

                errorObj = model.validate(formObj);
            });

            it('should return an error', function () {
                expect(errorObj).to.have.property(expectedKey, expectedValue);
            });

            it('should set the "formErrorText" key of model', function () {
                expect(model.has(expectedKey)).to.be.true;
            });

            it('utils.sendFormErrorsToAnalytics() should be called', function () {
                expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
            });

        });

        describe('if creditCard is TRUE and the files array is empty', function () {

            beforeEach(function () {
                formObj.creditCard = true;

                errorObj = model.validate(formObj);

                expectedKey   = 'creditCardErrorText';
                expectedValue = model.errors.creditCardNoFiles;
            });

            it('utils.sendFormErrorsToAnalytics() should be called', function () {
                expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
            });

            describe('"creditCardErrorText" key set with error message as value',
                function () {

                it('should exist in the error object returned', function () {
                    expect(errorObj).to.have.property(expectedKey, expectedValue);
                });

                it('should be set on the model', function () {
                    expect(model.get(expectedKey)).to.equal(expectedValue);
                });

            });

            describe('"formErrorText" key set with error message as value', function () {

                beforeEach(function () {
                    expectedKey   = 'formErrorText';
                    expectedValue  = model.errors.attachmentRequired;
                });

                it('should exist in the error object returned', function () {
                    expect(errorObj).to.have.property(expectedKey, expectedValue);
                });

                it('should be set on the model', function () {
                    expect(model.get(expectedKey)).to.equal(expectedValue);
                });

            });

        });     // if creditCard is TRUE and the files array is empty

        describe('if comments length is greater than "maxCommentsLength" property',
            function () {

            beforeEach(function () {
                expectedKey   = 'commentsErrorText';
                expectedValue = model.errors.commentsLength;

                formObj.comments   = '12345678910';

                errorObj = model.validate(formObj);
            });

            it('"commentsErrorText" key/value should indicate error', function () {
                expect(errorObj).to.have.property(expectedKey, expectedValue);
            });

            it('"commentsErrorText" key/value should be set on model', function () {
                expect(model.get(expectedKey)).to.equal(expectedValue);
            });

            it('utils.sendFormErrorsToAnalytics() should be called', function () {
                expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
            });

        });

        describe('if file type in files is not valid', function () {

            beforeEach(function () {
                expectedKey   = 'fileErrorText';
                expectedValue = model.errors.invalidFileType;

                // add an invalid file to the form
                formObj.files = [ file ];

                errorObj = model.validate(formObj);
            });

            it('"fileErrorText" key/value should indicate error', function () {
                expect(errorObj).to.have.property(expectedKey, expectedValue);
            });

            it('"fileErrorText" key/value should be set on model', function () {
                expect(model.get(expectedKey)).to.equal(expectedValue);
            });

            it('utils.sendFormErrorsToAnalytics() should be called', function () {
                expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
            });

        });     // if file type in files is not valid

        describe('if files have a size which is over the allowable limit', function () {
            var bigFile;

            beforeEach(function () {
                // Create a big file so that we end up over the limit of 10mb
                // which was set in maxFileSizeInMegabytes at the beginning suite of tests.
                bigFile = {
                    name : 'File3',
                    type : validFileTypes[0],
                    size : 11534336     // 11mb
                };

                formObj.files = files.concat(bigFile);

                expectedKey   = 'fileErrorText';
                expectedValue = model.errors.totalFileSize;

                errorObj = model.validate(formObj);
            });

            it('"fileErrorText" key/value should indicate error', function () {
                expect(errorObj).to.have.property(expectedKey, expectedValue);
            });

            it('"fileErrorText" key/value should be set on model', function () {
                expect(model.get(expectedKey)).to.equal(expectedValue);
            });

            it('utils.sendFormErrorsToAnalytics() should be called', function () {
                expect(sendFormErrorsToAnalyticsStub).to.have.been.calledOnce;
            });

        });

        describe('if form is valid', function () {
            var allErrorKeys = [
                'creditCardErrorText',
                'commentsErrorText',
                'fileErrorText',
                'formErrorText'
            ];

            beforeEach(function () {
                // it's valid to just send comments
                formObj.comments = 'Hello!';

                errorObj = model.validate(formObj);
            });

            it('should return undefined value', function () {
                expect(errorObj).to.be.undefined;
            });

            it('all error-related keys in the model are undefined', function () {

                allErrorKeys.forEach(function(key) {
                    expect(model.get(key)).to.be.undefined;
                });
            });
        });

    });     // validate method

    describe('validateComments method', function () {
        var viewModel;

        beforeEach(function () {
            viewModel = new Backbone.Model({
                comments : 'hello'
            });
        });

        describe('"commentsErrorText" value should be set to appropriate error', function () {

            var expectedKey;
            var invalidCommentsLengthError;

            beforeEach(function () {
                expectedKey                = 'commentsErrorText';
                invalidCommentsLengthError = model.errors.commentsLength;
            });

            it('should indicate length error if text length EQUALS "maxCommentsLength"',
                function () {
                viewModel.set('comments', '1234567890');
                model.validateComments(viewModel);

                expect(model.get(expectedKey)).to.equal(invalidCommentsLengthError);
            });

            it('should indicate length error if text length IS GREATER ' +
                'THAN "maxCommentsLength"', function () {
                viewModel.set('comments', '12345678901');
                model.validateComments(viewModel);

                expect(model.get(expectedKey)).to.equal(invalidCommentsLengthError);
            });

            it('should be undefined if both tests pass', function () {
                model.validateComments(viewModel);

                expect(model.has(expectedKey)).to.be.false;
            });

        });     // "commentsErrorText" value should be set to appropriate error

    });     // validateComments method

    describe('_getTotalFileSizeInMegabytes method', function () {

        it('should return "0.0" if passed an empty array', function () {
            var result = model._getTotalFileSizeInMegabytes([]);

            expect(result).to.equal('0.0');
        });

        it('should add the size of each file in the array and return the ' +
            'total in megabytes', function () {
            var expectedResult = '0.2';
            var result         = model._getTotalFileSizeInMegabytes(files);

            expect(result).to.equal(expectedResult);
        });

    });     // _getTotalFileSizeInMegabytes method

    /* Note: These tests will work when the unit-browser-tests-develop.sh
     * script is ran but not when intern is ran from the command line since
     * FormData is not present in node.
     * I am leaving these tests here for now.
     */
    //describe('_getFormData method', function () {
    //    var formData;
    //
    //    beforeEach(function () {
    //        model.set({
    //            comments   : 'test',
    //            creditCard : false,
    //            files      : []
    //        });
    //
    //        formData = model._getFormData();
    //    });
    //
    //    it('should return a FormData object', function () {
    //        expect(formData).to.be.an.instanceOf(FormData);
    //    });
    //
    //    describe('"metadata" key', function () {
    //
    //        it('should exist', function () {
    //           var result = formData.has('metadata');
    //
    //           expect(result).to.be.true;
    //        });
    //
    //        it('should have a mime type of "application/json"', function () {
    //            var metadataValue = formData.get('metadata');
    //
    //            expect(metadataValue.type).to.equal('application/json');
    //        });
    //
    //        it('should contain "answer" key with a value which corresponds ' +
    //            'to the "comments" value in the model', function () {
    //            var dfd      = this.async(500);
    //            var fr       = new FileReader();
    //            var metadata = formData.get('metadata');
    //
    //            fr.onloadend = function(event) {
    //                var text = JSON.parse(event.target.result);
    //
    //                if (event.target.error) {
    //                    dfd.reject();
    //                }
    //
    //                expect(text).to.have.property('answer');
    //                expect(text.answer).to.equal(model.get('comments'));
    //                dfd.resolve();
    //            };
    //
    //            fr.readAsText(metadata, 'application/json');
    //
    //        });
    //
    //        it('should contain "payment" key with a value which corresponds ' +
    //            'to the "creditCard" value in the model', function () {
    //            var dfd      = this.async(500);
    //            var fr       = new FileReader();
    //            var metadata = formData.get('metadata');
    //
    //            fr.onloadend = function(event) {
    //                var text = JSON.parse(event.target.result);
    //
    //                if (event.target.error) {
    //                    dfd.reject();
    //                }
    //
    //                expect(text).to.have.property('payment');
    //                expect(text.payment).to.equal(model.get('creditCard'));
    //                dfd.resolve();
    //            };
    //
    //            fr.readAsText(metadata, 'application/json');
    //        });
    //
    //    });     // "metadata" key
    //
    //    describe('"file" key', function () {
    //
    //        it('should not be set when no files were submitted', function () {
    //            var result = formData.has('file');
    //
    //            expect(result).to.be.false;
    //        });
    //
    //        it('should be set when a file has been submitted', function () {
    //            var result;
    //
    //            model.set({
    //                comments   : 'test',
    //                creditCard : false,
    //                files      : [
    //                    {
    //                        name : 'file.pdf',
    //                        size : 12345,
    //                        type : 'application/pdf'
    //                    }
    //                ]
    //            });
    //
    //            formData = model._getFormData();
    //
    //            result = formData.has('file');
    //
    //            expect(result).to.be.true;
    //        });
    //
    //    });     // "file" key
    //
    //});     // _getFormData method


    describe('_isDuplicateFile method', function () {
        var result;

        it('should return TRUE if name and size properties on a file are identical ' +
            'to another file passed in first param', function () {
            var duplicateFile = files[0];

            result = model._isDuplicateFile(files, duplicateFile);

            expect(result).to.be.true;
        });

        it('should return FALSE if name and size properties are NOT identical ' +
            'to another file passed in the first param', function () {
            var uniqueFile = $.extend({}, true, files[0]);
            uniqueFile.name = 'Unique Name.pdf';

            result = model._isDuplicateFile(files, uniqueFile);

            expect(result).to.be.false;

        });
    });     // _isDuplicateFile method

    describe('_isValidFileType method', function () {
        var result;

        it('should return FALSE if the "type" property on the file doesn\'t ' +
            'match one of the types in "validFileTypes"', function () {

            result = model._isValidFileType(file);

            expect(result).to.be.false;

        });

        it('should return TRUE if the "type" property on the file matches one of ' +
            'the types in "validFileTypes"', function () {

            result = model._isValidFileType(files[0]);

            expect(result).to.be.true;

        });

    });     // _isValidFileType method


    describe('_isValidTotalFileSize method', function () {
        var result;

        it('should return FALSE if the number passed in to the method is ' +
            'GREATER THAN "maxFileSizeInMegabytes"', function () {

            model.set('maxFileSizeInMegabytes', 5);

            result = model._isValidTotalFileSize(6.2);

            expect(result).to.be.false;

        });

        it('should return FALSE if the number passed in to the method is ' +
            'EQUAL TO "maxFileSizeInMegabytes"', function () {

            model.set('maxFileSizeInMegabytes', 5);

            result = model._isValidTotalFileSize(5);

            expect(result).to.be.false;

        });

        it('should return FALSE if the number passed in to the method is ' +
            'LESS THAN "maxFileSizeInMegabytes"', function () {

            model.set('maxFileSizeInMegabytes', 5);

            result = model._isValidTotalFileSize(6.2);

            expect(result).to.be.false;

        });

    });     // _isValidTotalFileSize method

    describe('_parseServerErrorMessages method', function () {
        var messageObj;
        var result;

        beforeEach(function () {
            messageObj = {
                items : null,
                errors : [
                    {
                        className : 'java.lang.IllegalArgumentException',
                        message   : 'Both text answer and files are empty.  ' +
                            'At least one must be populated.'
                    },
                    {
                        className : 'java.lang.IllegalArgumentException',
                        message   : 'Payments require at least one file to be submitted.'
                    }, {
                        className : 'java.lang.IllegalArgumentException',
                        message   : 'File submitted is null or empty'
                    }
                ],
                'count' : 0,
                'errorCount' : 1
            };

            // pass the message in as a string
            result = model._parseServerErrorMessages(JSON.stringify(messageObj));
        });

        it('the array returned should contain the same number of error messages ' +
            'as the object returned from the service', function () {
            var expectedCount = messageObj.errors.length;

            expect(result.length).to.equal(expectedCount);
        });

        it('each element of the returned array should contain the ' +
            'errors.message from the service', function () {
            _.each(messageObj.errors, function(error, index) {
                expect(result[index]).to.equal(error.message);
            });
        });

        describe('When passed a string which cannot be parsed', function () {
            var unParseable = '{JSON.parse does not like me';
            var expectedMsg = RequirementSubmissionModel.prototype.errors.unknownError;

            it('should return a single message', function () {
                result = model._parseServerErrorMessages(unParseable);
                expect(result.length).to.equal(1);
                expect(result[0]).to.equal(expectedMsg);
            });

            it('should not throw an error message', function () {
                var fn = function () {
                    model._parseServerErrorMessages(unParseable);
                };

                expect(fn).not.to.throw(Error);
            });

        });

    });     // _parseServerErrorMessages method

    describe('_setErrorParameters method', function () {
        var expectedMsg;
        var messages;
        var parseServerErrorMsgsStub;
        var response;
        var setMessages;

        beforeEach(function () {
            messages = [];

            response = {
                status       : 400,
                responseText : 'Watchu talking about Willis?!',
                url          : '/api/oso/secure/rest/policies/1234567890/requirements'
            };

            parseServerErrorMsgsStub = this.sinon.stub(model, '_parseServerErrorMessages');

            setMessages = function(messagesToReturn) {
                parseServerErrorMsgsStub.returns(messagesToReturn);
                model._setErrorParameters(model, response);
            };
        });

        afterEach(function () {
            response = null;
            if (parseServerErrorMsgsStub) {
                parseServerErrorMsgsStub.restore();
            }
        });

        it('should not update the model if response.status is NOT 400', function () {
            var modelSetSpy = this.sinon.spy(model, 'set');
            response.status = 200;
            model._setErrorParameters(model, response);

            expect(modelSetSpy).not.to.have.been.called;
        });

        it('should call _parseServerErrorMessages() with the responseText', function () {
            model._setErrorParameters(model, response);

            expect(parseServerErrorMsgsStub).to.have.been.calledOnce;
        });

        describe('"formErrorText" should be set with the appropriate error message ' +
            'in the model', function () {

            it('when service returns "Both text answer and files are empty." message', function () {
                expectedMsg = model.errors.emptyForm;
                messages.push('Both text answer and files are empty.');
                setMessages(messages);

                expect(model.has('formErrorText')).to.be.true;
                expect(model.get('formErrorText')).to.equal(expectedMsg);
            });

            it('when service returns "Payments require at least one file to be ' +
                'submitted." message', function () {
                messages.push('Payments require at least one file to be submitted.');
                setMessages(messages);

                expect(model.get('creditCardErrorText')).to
                    .equal(model.errors.creditCardNoFiles);
                expect(model.get('formErrorText')).to.equal(model.errors.attachmentRequired);

            });

            it('when service returns "Uploaded file extension is invalid...." ' +
                'message', function () {
                var expectedMessage = model.errors.serviceValidation.invalidFileType;
                messages.push('Uploaded file extension is invalid. Uploaded extension = "txt"');
                setMessages(messages);

                expect(model.get('serviceFileErrorText')).to.equal(expectedMessage);
            });

            it('when service returns an error indicating the maximum upload size was ' +
                'exceeded', function () {
                var expectedMessage = model.errors.serviceValidation.totalFileSize;
                messages.push('Maximum upload size of 27262976 bytes exceeded; ' +
                    'nested exception is org.apache.commons.fileupload.FileUploadBase' +
                    '$SizeLimitExceededException');

                setMessages(messages);

                expect(model.get('serviceFileErrorText')).to.equal(expectedMessage);
            });

            it('when service returns "File submitted is null or empty" message', function () {
                messages.push('File submitted is null or empty');
                setMessages(messages);

                expect(model.get('serviceFileErrorText')).to.equal(model.errors.emptyFile);
            });

        });

        it('if two errors exist for the same model property, the errors will' +
            ' be concatenated', function () {
            var errorText;

            messages.push('Uploaded file extension is invalid. Uploaded extension = "txt');
            messages.push('File submitted is null or empty');
            setMessages(messages);

            errorText = model.get('serviceFileErrorText');

            expect(errorText).to.contain(model.errors.serviceValidation.invalidFileType);
            expect(errorText).to.contain(model.errors.emptyFile);
        });

    });     // _setErrorParameters method

});

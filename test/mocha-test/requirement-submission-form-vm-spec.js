/* global expect:false, _:false */

var RequirementSubmissionViewModel =
    require('../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm');

describe('Requirement Submission Form ViewModel ' +
    '(pages/requirementSubmission/viewModels/requirement-submission-form-vm.js)', function () {
    var viewModel;

    beforeEach(function () {
        viewModel = new RequirementSubmissionViewModel({
            policyId : 1234567890
        });
    });

    it('should exist', function () {
        expect(RequirementSubmissionViewModel).to.exist;
    });

    describe('initialize method', function () {

        it('should throw an error if initialized without a "policyId" option OR ' +
            '"policyDetailModel"', function () {
            var fn = function () {
                new RequirementSubmissionViewModel();    // eslint-disable-line no-new
            };

            expect(fn).to
                .throw(RequirementSubmissionViewModel.prototype.errors.missingPolicyIdOrModel);
        });

    });     // initialize method

    describe('reset method', function () {
        var clearStub;
        var setStub;
        var setPolicyIdValuesStub;

        beforeEach(function () {
            clearStub             = this.sinon.stub(viewModel, 'clear');
            setStub               = this.sinon.stub(viewModel, 'set');
            setPolicyIdValuesStub = this.sinon.stub(viewModel, '_setPolicyIdentificationValues');

            viewModel.reset();
        });

        afterEach(function () {
            clearStub.restore();
            setPolicyIdValuesStub.restore();
            setStub.restore();
        });

        it('should exist as a function', function () {
            expect(viewModel.reset).to.exist.and.be.a('function');
        });

        it('should call clear()', function () {
            expect(clearStub).to.have.been.calledOnce;
        });

        it('clear() should be passed the "silent" option', function () {
            expect(clearStub).to.have.been.calledWith({ silent : true });
        });

        it('set() method should be called', function () {
            expect(setStub).to.have.been.calledOnce;
        });

        it('_setPolicyIdentificationValues() should be called', function () {
            expect(setPolicyIdValuesStub).to.have.been.calledOnce;
        });

    });     // reset method


    describe('_getErrorMessagesFromDomainModel method', function () {
        var sharedKeys;

        beforeEach(function () {
            sharedKeys = viewModel.sharedProperties.domainModel;
            // set each of the sharedKeys in the domain model
            sharedKeys.forEach(function(key) {
                viewModel.domainModel.set(key, key);
            });
        });

        it('copies "formErrorText" form the domainModel', function () {
            expect(viewModel.has('formErrorText')).to.be.true;
        });

        it('copies "commentsErrorText" form the domainModel', function () {
            expect(viewModel.has('commentsErrorText')).to.be.true;
        });

        it('copies "creditCardErrorText" form the domainModel', function () {
            expect(viewModel.has('creditCardErrorText')).to.be.true;
        });

        it('copies "fileErrorText" form the domainModel', function () {
            expect(viewModel.has('fileErrorText')).to.be.true;
        });

    });     // _getErrorMessagesFromDomainModel method

    describe('_setPolicyIdentificationValues method', function () {
        var keys;

        beforeEach(function () {
            keys = viewModel.sharedProperties.policyDataModel;

            // clear out the properties which were set when
            // creating an instance of this view model above.
            keys.forEach(function(key) {
                viewModel.unset(key);
                viewModel.policyDetailModel.set(key, key + ' value');
            });

            viewModel._setPolicyIdentificationValues();

        });

        it('gets all related keys from the domain model and copies them to ' +
            'the view model', function () {

            keys.forEach(function(key) {
                expect(viewModel.get(key)).to.equal(viewModel.policyDetailModel.get(key));
            });
        });

    });     // _setPolicyIdentificationValues method

    describe('_updateTotalFileSize method', function () {
        var files = [
            {
                name : 'file1',
                size : 5242880
            },
            {
                name : 'file2',
                size : 2621440
            }
        ];

        afterEach(function () {
            viewModel.unset('files');
        });

        it('sets "totalFileSizeInBytes" in model', function () {
            var afterCall;
            var beforeCall     = viewModel.get('totalFileSizeInBytes');
            var expectedTotal  = _.reduce(files, function(memo, file) {
                return Number(memo + file.size);
            },0);

            viewModel.set('files', files);

            viewModel._updateTotalFileSize(files);

            afterCall = viewModel.get('totalFileSizeInBytes');

            expect(beforeCall).to.equal(0);
            expect(afterCall).to.equal(expectedTotal);
        });

    });     // _updateTotalFileSize method

});     // Requirement Submission View Model tests

/* global describe: false, expect:false */

/**
 * Unit tests covering the tout model class
 */

var ToutModel = require('../dist/modules/tout/models/tout-m');

describe('Tout Model (modules/tout/models/tout-m.js)', function () {

    it('exists', function () {
        expect(ToutModel).to.exist;
    });

    describe('Constructor', function () {
        var options;

        beforeEach(function () {
            options =  {
                iconCss : 'ace-icon fa fa-pencil brown',
                title   : 'My Tout',
                text    : 'Hello',
                href    : '#hiya'
            };
        });

        afterEach(function () {
            options = null;
        });

        it('Throws an error if \'iconCss\' property is not provided', function () {
            delete options.iconCss;

            var fn = function () {
                new ToutModel(options); // eslint-disable-line no-new
            };

            expect(fn).to.throw(ToutModel.prototype.errors.missingKeys);
        });

        it('Throws an error if \'title\' property is not provided', function () {
            delete options.title;

            var fn = function () {
                new ToutModel(options); // eslint-disable-line no-new
            };

            expect(fn).to.throw(ToutModel.prototype.errors.missingKeys);
        });

        it('Throws an error if \'href\' property is not provided', function () {
            delete options.href;

            var fn = function () {
                new ToutModel(options); // eslint-disable-line no-new
            };

            expect(fn).to.throw(ToutModel.prototype.errors.missingKeys);
        });

    });

});

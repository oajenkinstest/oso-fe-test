/* global Backbone:false, $:false, expect:false, sinon:false */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView  = require('../dist/pages/policy/views/policy-detail-v');
var PolicyDetailModel = require('../dist/pages/policy/models/policy-detail-m');
var helpers           = require('./helpers/helpers');


describe('Policy Detail View (pages/policy/views/policy-detail-v.js)', function() {

    var ajaxStub;
    var rootView;
    var view;
    var policyDataCopy;

    var setView = function () {
        var newView = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newView);
        
        return newView;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        policyDataCopy = $.extend(true, {}, helpers.policyData.activePolicyDetail);
    });

    after(function () {
        rootView.destroy();
    });

    it('PolicyDetailView exists', function () {
        expect(PolicyDetailView).to.exist;
    });

    describe('rendered content with typical data', function () {
        
        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
            ajaxStub.restore();
        });

        it('should render view without any error', function () {
            expect(view.isRendered).to.be.true;
        });

        it('should display "Last Refresh Date" field', function () {
            var expectedValue = '02/11/2016, 09:30 AM EST';
            expect(view.$el.find('#last-refresh-date').text().trim()).to.contain(expectedValue);
        });

        it('Successfully looks up the names for coverage section', function () {
            var base0Id    = helpers.policyData.pendingPolicyDetail.coverage.base[0].customerId;
            var base1Id    = helpers.policyData.pendingPolicyDetail.coverage.base[1].customerId;
            var r0Id       = helpers.policyData.pendingPolicyDetail.coverage.riders[0].customerId;
            var r1Id       = helpers.policyData.pendingPolicyDetail.coverage.riders[1].customerId;
            var base0Name  = helpers.policyData.pendingPolicyDetail.customers[base0Id].fullName;
            var base1Name  = helpers.policyData.pendingPolicyDetail.customers[base1Id].fullName;
            var rider0Name = helpers.policyData.pendingPolicyDetail.customers[r0Id].fullName;
            var rider1Name = helpers.policyData.pendingPolicyDetail.customers[r1Id].fullName;

            expect(view.$el.find('#base-0-coverage-person-name').html().trim())
                .to.equal(base0Name);

            expect(view.$el.find('#base-1-coverage-person-name').html().trim())
                .to.equal(base1Name);

            expect(view.$el.find('#rider-0-coverage-person-name').html().trim())
                .to.equal(rider0Name);

            expect(view.$el.find('#rider-1-coverage-person-name').html().trim())
                .to.equal(rider1Name);
        });

        describe('`_setAnalyticsData()` method', function () {
            var setAnalyticsDataSpy;
            var dataFetchedEventSpy;
            var listener;

            before(function () {
                setAnalyticsDataSpy  = this.sinon.spy(view, '_setAnalyticsData');
                dataFetchedEventSpy  = this.sinon.spy();
                listener             = new Backbone.Marionette.Object();

                listener.listenTo(view, 'dataFetched', dataFetchedEventSpy);
                view.render();
            });

            after(function () {
                setAnalyticsDataSpy.restore();
                listener.stopListening();
                listener.destroy();
            });

            it('should get called onRender', function () {
                expect(setAnalyticsDataSpy).to.have.been.called;
            });

            it('should set `dataLayerValues` object', function () {
                delete view.dataLayerValues;
                
                var policyStatus = view.model.get('policyStatus');
                policyStatus.statusView = 'Active';
                view.model.set('policyStatus', policyStatus);
                view.render();

                var expectedDataLayerObject = {
                    'Policy Product Type'   : 'Annuity Care',
                    'Policy Status'         : 'Active',
                    'Status View'           : 'Active',
                    'Policy ID'             : '123457890'
                };

                expect(view.dataLayerValues).to.be.instanceof(Object);
                expect(view.dataLayerValues).to.be.deep.equal(expectedDataLayerObject);
            });


            it('should trigger `dataFetched` event', function () {
                expect(dataFetchedEventSpy).to.have.been.called;
            });

            describe('When policyId, product.productName, and policyStatus.description ' +
                'do not exist', function () {

                before(function () {
                    // set up test with missing data
                    var policyStatus = view.model.get('policyStatus');
                    var product      = view.model.get('product');

                    delete policyStatus.description;
                    delete product.productName;

                    view.model.set('policyStatus', policyStatus);
                    view.model.set('product', product);
                    view.model.unset('policyId');

                    setAnalyticsDataSpy.resetHistory();

                    view.render();
                });

                it('`setAnalyticsData()` method should be called', function () {
                    expect(setAnalyticsDataSpy).to.have.been.called;
                });

            });
        });

        describe('Related Policies links below "Policy Identification" section',function () {
            var $relatedPoliciesList;
            var relatedPoliciesTitle;

            before(function () {
                $relatedPoliciesList = view.$el.find('#related-policies-links ul');
                relatedPoliciesTitle = view.$el.find('#related-policies-links h4').text().trim();
            });

            after(function () {
                $relatedPoliciesList = null;
                relatedPoliciesTitle = null;
            });

            it('element should exist', function () {
                expect($relatedPoliciesList.length).to.be.at.least(1);
            });

            it('heading should be plural ("Policies") when more than one related ' +
                'policy is returned', function () {

                expect(relatedPoliciesTitle).to.equal('Related Policies');
            });

            it('element should contain two policy links', function () {
                expect($relatedPoliciesList.find('a').length).to.equal(2);
            });

            it('policy links value should match', function () {
                expect($relatedPoliciesList.find('a').eq(0).text().trim())
                    .to.equal('9558845545 – Asset-Care IV');
                expect($relatedPoliciesList.find('a').eq(1).text().trim())
                    .to.equal('9558845784 – Continuation of Benefit Rider');
            });

            it('no premium amounts should accompany the related policy link', function () {
                $relatedPoliciesList.each(function () {
                    var numberOfListItems = $(this).find('li').length;

                    expect(numberOfListItems).to.equal(1);
                });
            });
        }); // Related Policies links below "Policy Identification" section
        
    }); // rendered content with typical data

    describe('rendered content with only one policy', function () {
        
        before(function () {
            var singleRelatedPolicyData = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            singleRelatedPolicyData.relatedPolicies.pop();

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                singleRelatedPolicyData
            );

            view = setView();
        });

        after(function () {
            view.destroy();
            ajaxStub.restore();
        });

        describe('Related Policies links below "Policy Identification" section',function () {

            it('heading should be singular ("Policy") when only one related policy is returned',
                function () {

                var relatedPoliciesTitle = view.$el.find('#related-policies-links h4').text().trim();

                expect(relatedPoliciesTitle).to.equal('Related Policy');
            });

        }); // Related Policies links below "Policy Identification" section
        
    }); // rendered content with only one policy

       
    describe('Policy and workflow status help text display', function () {

        var copyOfAWDRip;
        var showHelpTextModalSpy;
        var wcmHelpPath = '/wps/wcm/connect/indcontent/OSO/misc/policy-status-help';

        before(function () {
            copyOfAWDRip = $.extend(true,{},helpers.policyData.pendingPolicyDetailAWDRip);
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfAWDRip
            );

            showHelpTextModalSpy = this.sinon.spy(PolicyDetailView.prototype,
                'showHelpTextModal');

            view = setView();
        });

        after(function () {
            ajaxStub.restore();
            showHelpTextModalSpy.restore();
            view.destroy();
        });

        it('help icon should exist', function () {
            expect(view.$el.find('#help-text-icon')).to.exist;
        });

        it('showHelpTextModal should called when clicking on help text icon', function () {

            // The ajaxStub doesn't play well with the fake server
            ajaxStub.restore();

            // Fake WCM response
            var server = this.setupFakeServer(this.sinon, global, global.window);
            view.ui.policyStatusHelpTextIcon.click();

            expect(showHelpTextModalSpy).to.have.been.called;

            server.restore();
        });

        it('showHelpTextModal should load using the correct WCM path', function () {
            var wcmHelpRequestUrl;

            // The ajaxStub doesn't play well with the fake server
            ajaxStub.restore();

            // Set up the fake server so we can look at the request; we don't actually need
            // it to respond with any data for this test.
            var server = this.setupFakeServer(this.sinon, global, global.window);

            view.ui.policyStatusHelpTextIcon.click();

            // Look at the URL of the ajax request
            wcmHelpRequestUrl = server.requests[0].url;

            // Using "contain" here as some tests have run first and
            // appended a "targetuser" parameter in the query string.
            expect(wcmHelpRequestUrl).to.contain(wcmHelpPath);

            server.restore();
        });

    }); // Policy and workflow status help text display

    describe('Error handling when server request failed', function () {

        var showAlertMessageSpy;

        beforeEach(function () {
            showAlertMessageSpy = this.sinon.spy(
                PolicyDetailView.prototype, '_showAlertMessage'
            );
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'error',
                {
                    status:404,
                    responseText: {},
                    statusText: 'policy not found'
                }
            );

            view = setView();
        });

        afterEach(function () {
            ajaxStub.restore();
            showAlertMessageSpy.restore();
        });

        it('should call showAlertMessage', function () {
            expect(showAlertMessageSpy).to.have.been.called;
        });

        it('model should update alertMessage Attibute', function () {
            expect(view.model.has('alertMessage')).to.be.true;
        });

        it('should show error message', function () {
            // find an alert that contains the expected error message
            var expectedMessage = PolicyDetailView.prototype.errors.policyNotFoundMessage;
            var errorAlert = view.$el.find('.alert:contains("' + expectedMessage + '")');
            expect(errorAlert.length).to.equal(1);
        });

        it('should call showErrorPage if policy number or case Id is missing', function () {

            var errorHandlerSpy = this.sinon.spy();
            var errorChannel = Backbone.Radio.channel('error');
            errorChannel.on('showErrorPage', errorHandlerSpy);

            view = new PolicyDetailView();
            expect(errorHandlerSpy).to.have.been.called;

            errorChannel.reset();
        });

        it('should call waitIndicator module\'s "show" and "hide" ', function () {

            var waitIndicatorShowSpy = this.sinon.spy();
            var waitIndicatorHideSpy = this.sinon.spy();
            var waitIndicatorChannel = Backbone.Radio.channel('spinner');

            waitIndicatorChannel.on('show', waitIndicatorShowSpy);
            waitIndicatorChannel.on('hide', waitIndicatorHideSpy);

            view = setView();

            expect(waitIndicatorShowSpy).to.have.been.called;
            expect(waitIndicatorHideSpy).to.have.been.called;

            waitIndicatorChannel.reset();
        });

        it('should call showAlertMessage and show error message '+
            'if status code is 500', function () {

            ajaxStub.restore();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'error',
                {
                    status:500,
                    responseText: {},
                    statusText: 'server error'
                }
            );

            view = setView();

            expect(showAlertMessageSpy).to.have.been.called;

            var expectedMessage = PolicyDetailView.prototype.errors.serverError;
            var errorAlert = view.$el.find('.alert:contains("' + expectedMessage + '")');
            expect(errorAlert.length).to.equal(1);
        });

    }); // Error handling when server request failed

    describe('_prepPolicyHighlightsConfig method', function () {

        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
            ajaxStub.restore();
        });

        it('shows the Rates column if it contains any data', function () {
            var expected = {
                columnClass: 'four-col',
                show: {
                    policyValues    : true,
                    ltcValues       : true,
                    cashValues      : true,
                    rates           : true
                }
            };
            var model = new PolicyDetailModel(policyDataCopy);
            view._prepPolicyHighlightsConfig(model);
            var actual = view.model.get('policyHighlightsConfig');

            expect(actual).to.deep.equal(expected);
        });

    }); // _prepPolicyHighlightsConfig method

    describe('view destroy', function () {
        it('should call unsetDataLayerValue', function () {
            
            var unsetDataLayerValueSpy = this.sinon.spy();
            var analyticsChannel     = Backbone.Radio.channel('analytics');
            analyticsChannel.on('unsetDataLayerValue', unsetDataLayerValueSpy);

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                policyDataCopy
            );
            view = setView();
            view.destroy();
            expect(unsetDataLayerValueSpy).to.have.been.calledOnce;
            ajaxStub.restore();
            
            analyticsChannel.stopListening();
            analyticsChannel = null;
        });
    });
});



/* global describe:false, expect:false */

var SidebarMenuItemModel = require('../dist/apps/sidebar/models/sidebar-menu-item-m');

describe('Sidebar Menu Item Model (apps/sidebar/models/sidebar-menu-item-m.js)', function (){

    it('exists', function () {
        expect(SidebarMenuItemModel).to.exist;
    });

    it('has a method containsAnActiveItem', function () {
        var item = new SidebarMenuItemModel();
        expect(item.containsAnActiveItem).to.be.a.function;
        item = null;
    });

    describe('validation', function () {
        var item;

        beforeEach(function () {
            item = new SidebarMenuItemModel();
        });

        afterEach(function () {
            item = null;
        });

        it('default model passes validation', function () {
            expect(item.isValid()).to.be.true;
        });

        it('passes if icon is set to a string', function () {
            item.set('icon', 'this is a string');
            expect(item.isValid()).to.be.true;
        });

        it('fails if icon is set to a number', function () {
            item.set('icon', 123);
            expect(item.isValid()).to.be.false;
        });

        it('passes if displayText is set to a string', function () {
            item.set('displayText', 'this is a string');
            expect(item.isValid()).to.be.true;
        });

        it('fails if displayText is set to a boolean', function () {
            item.set('displayText', true);
            expect(item.isValid()).to.be.false;
        });

        it('passes if link is set to a string', function () {
            item.set('link', 'this is a string');
            expect(item.isValid()).to.be.true;
        });

        it('fails if link is set to an array', function () {
            item.set('link', [1, false, 'C']);
            expect(item.isValid()).to.be.false;
        });

        it('passes if subItems is set to an array', function () {
            item.set('subItems', []);
            expect(item.isValid()).to.be.true;
        });

        it('fails if subItems is set to a string', function () {
            item.set('subItems', 'this is not an array');
            expect(item.isValid()).to.be.false;
        });
    });

    describe('simple active model', function () {

        var simpleModelActive = {
            icon        : 'home',
            displayText : 'Home',
            link        : 'home',
            active      : true
        };

        it('is active', function () {
            var item = new SidebarMenuItemModel(simpleModelActive);
            expect(item.get('active')).to.be.true;
        });

        it('is not a parent of an active item', function () {
            var item = new SidebarMenuItemModel(simpleModelActive);
            expect(item.containsAnActiveItem()).to.be.false;
        });
    });


    describe('simple inactive model', function () {

        var simpleModelActive = {
            icon        : 'home',
            displayText : 'Home',
            link        : 'home',
            active      : false
        };

        it('is not active', function () {
            var item = new SidebarMenuItemModel(simpleModelActive);
            expect(item.get('active')).to.be.false;
        });

        it('is not a parent of an active item', function () {
            var item = new SidebarMenuItemModel(simpleModelActive);
            expect(item.containsAnActiveItem()).to.be.false;
        });
    });


    describe('nested inactive model', function () {

        var nestedModelInactive = {
            icon        : 'info-circle',
            displayText : 'FAQs & Resources',
            subItems: [
                {
                    displayText: 'Products',
                    link: 'products'
                }, {
                    displayText: 'My Business',
                    link: 'mybiz'
                }
            ]
        };

        var item;

        beforeEach(function () {
            item = new SidebarMenuItemModel(nestedModelInactive);
        });

        afterEach(function () {
            item = null;
        });

        it('is not active', function () {
            expect(item.get('active')).to.be.false;
        });

        it('is not a parent of an active item', function () {
            expect(item.containsAnActiveItem()).to.be.false;
        });

    });


    describe('nested inactive model with active child', function () {

        var nestedModelInactiveWithActiveChild = {
            icon        : 'info-circle',
            displayText : 'FAQs & Resources',
            subItems: [
                {
                    displayText: 'Products',
                    link: 'products'
                }, {
                    displayText: 'My Business',
                    link: 'mybiz',
                    active: true
                }
            ]
        };

        var item;

        beforeEach(function () {
            item = new SidebarMenuItemModel(nestedModelInactiveWithActiveChild);
        });

        afterEach(function () {
            item = null;
        });

        it('is not active', function () {
            expect(item.get('active')).to.be.false;
        });

        it('is a parent of an active item', function () {
            expect(item.containsAnActiveItem()).to.be.true;
        });

    });


    describe('deeply nested inactive model', function () {

        var deeplyNestedModelInactive = {
            icon        : 'info-circle',
            displayText : 'FAQs & Resources',
            subItems: [
                {
                    displayText: 'Products',
                    link: 'products'
                }, {
                    displayText: 'My Business',
                    link: 'mybiz'
                }, {
                    displayText: 'Page One',
                    subItems: [
                        {
                            icon: 'arrow-right',
                            displayText: 'Page 1A',
                            link: 'pageone-1a'
                        }, {
                            icon: 'arrow-right',
                            displayText: 'Page 1B',
                            link: 'pageone-1b'
                        }
                    ]
                }
            ]
        };

        var item;

        beforeEach(function () {
            item = new SidebarMenuItemModel(deeplyNestedModelInactive);
        });

        afterEach(function () {
            item = null;
        });

        it('is not active', function () {
            expect(item.get('active')).to.be.false;
        });

        it('is not a parent of an active item', function () {
            expect(item.containsAnActiveItem()).to.be.false;
        });

    });

    describe('deeply nested inactive model with active grandchild', function () {

        var deeplyNestedModelInactiveWithActiveGrandchild = {
            icon        : 'info-circle',
            displayText : 'FAQs & Resources',
            subItems: [
                {
                    displayText: 'Products',
                    link: 'products'
                }, {
                    displayText: 'My Business',
                    link: 'mybiz'
                }, {
                    displayText: 'Page One',
                    subItems: [
                        {
                            icon: 'arrow-right',
                            displayText: 'Page 1A',
                            link: 'pageone-1a'
                        }, {
                            icon: 'arrow-right',
                            displayText: 'Page 1B',
                            link: 'pageone-1b',
                            active: true
                        }
                    ]
                }
            ]
        };

        var item;

        beforeEach(function () {
            item = new SidebarMenuItemModel(deeplyNestedModelInactiveWithActiveGrandchild);
        });

        afterEach(function () {
            item = null;
        });

        it('is not active', function () {
            expect(item.get('active')).to.be.false;
        });

        it('contains an active item', function () {
            expect(item.containsAnActiveItem()).to.be.true;
        });

    });
});

/* global expect:false, $:false */


var SalesConnectionLinksView = require('../dist/pages/home/views/sales-connection-links-v');

describe('Sales Connection Links View (pages/home/views/sales-connection-links-v.js)', function () {
    var view;
    var submitStub;

    beforeEach(function () {
        view = new SalesConnectionLinksView();
        view.render();

        submitStub = this.sinon.stub(view.ui.ipipelineIgoForm, 'submit');
    });

    afterEach(function () {
        view.destroy();
        submitStub.restore();
    });

    describe('initialize method', function () {

        it('creates the model used by the view', function () {
            expect(view.model).to.exist;
        });

        it('Should set "showDisabled" property in model if it is added in options', function () {
            view = new SalesConnectionLinksView({
                showDisabled : true
            });
            expect(view.model.has('showDisabled')).to.be.true;
        });

        it('Should set "showCareSolutionsProductsLink" property '+
                'in model if it is added in options', function () {
            view = new SalesConnectionLinksView({
                showCareSolutionsProductsLink : true
            });
            expect(view.model.has('showCareSolutionsProductsLink')).to.be.true;
        });

    });     // initialize method

    describe('_fetchApplicationData method', function () {
        var modelFetchSpy;
        var event;

        beforeEach (function () {
            event = $.Event('click');
            modelFetchSpy = this.sinon.spy(view.model, 'fetch');
        });

        afterEach (function () {
            event= null;
            modelFetchSpy.restore();
        });
        
        it('Method should invoke model "fetch"', function () {
            view._fetchApplicationData(event);
            expect(modelFetchSpy).to.have.been.called;
        });

        it('Method should not invoke model "fetch" if links are disabled', function () {
            view = new SalesConnectionLinksView({
                showDisabled : true
            });
            view._fetchApplicationData(event);
            expect(modelFetchSpy).to.not.have.been.called;
        });

        it('Should call this method while clicking Sales Connection links', function () {
            var _fetchApplicationDataSpy = this.sinon.spy(SalesConnectionLinksView.prototype, 
                '_fetchApplicationData');
            view = new SalesConnectionLinksView();
            view.render();

            view.ui.eAppIllustrationLink.click();
            expect(_fetchApplicationDataSpy).to.have.been.called;
            _fetchApplicationDataSpy.restore();
        });

    });     // _fetchToken method


    describe('Method _setupFormAndSubmit', function () {
        var ajaxStub;
        var data = {
            targetURL        : 'https://sso-uat.ipipeline.com/sso/ACS',
            RelayState       : 'https://pipepasstoigo-td2.ipipeline.com/'+
                'default.aspx?gaid=5484',
            SAMLResponse     : 'some saml content'
        };

        beforeEach (function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                data
            );
        });

        afterEach (function () {
            ajaxStub.restore();
        });

        it('should call this method once fetch is success', function () {
            var _setupFormAndSubmitSpy = this.sinon.spy(SalesConnectionLinksView.prototype,
                '_setupFormAndSubmit');
            view = new SalesConnectionLinksView();
            view.render();

            view.ui.eAppIllustrationLink.click();
            expect(_setupFormAndSubmitSpy).to.have.been.called;
            _setupFormAndSubmitSpy.restore();
        });

        it('Form should fix exact service response', function () {
            view.ui.eAppIllustrationLink.click();

            expect(view.ui.ipipelineIgoForm.attr('action')).to.equal(data.targetURL);
            expect(view.ui.relayStateURLInput.val()).to.equal(data.RelayState);
            expect(view.ui.samlResponseInput.val()).to.equal(data.SAMLResponse);
        });

        it('Should call Form.submit', function () {
            view.ui.eAppIllustrationLink.click();
            expect(submitStub).to.have.been.called;
        });

        it('Should not call Form.submit if links are disabled', function () {
            submitStub.resetHistory();
            view = new SalesConnectionLinksView({
                showDisabled : true
            });
            view.render();
            view.ui.eAppIllustrationLink.click();
            expect(submitStub).to.not.have.been.called;
        });
    });

    describe('_handleiGoSsoError method', function () {
        var alertMessage;
        var expectedMessage;
        var response = {};

        describe('When response status is 404', function () {

            beforeEach(function () {
                expectedMessage = view.errors.missingProducerRecord;
                response.status = 404;

                view._handleiGoSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "iGoErrorMessage" in model', function () {
                expect(view.model.get('iGoErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });     // when response status is 404

        describe('When response status is 403', function () {

            beforeEach(function () {
                expectedMessage = view.errors.missingProducerRecord;
                response.status = 403;

                view._handleiGoSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "iGoErrorMessage" in model', function () {
                expect(view.model.get('iGoErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });     // when response status is 403

        describe('When response status is 500', function () {

            beforeEach(function () {
                expectedMessage = view.errors.xmlGenerationError;
                response.status = 500;

                view._handleiGoSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "iGoErrorMessage" in model', function () {
                expect(view.model.get('iGoErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });

        describe('When response is not defined or response.status is not set', function () {

            it('"iGoErrorMessage" is not set', function () {
                view._handleiGoSsoError(view.model, null);
                expect(view.model.has('iGoErrorMessage')).to.be.false;
            });
        });

    });     // _handleiGoSsoError method

});

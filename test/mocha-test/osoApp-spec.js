/* global expect:false, Backbone:false, $:false, sinon:false, jsdomConsole:false */

var OSOApp    = require('../dist/osoApp');
var UserModel = require('../dist/models/user-m');

describe('OSO App (osoApp.js)', function () {

    var osoApp;
    var userResponseBody  = {
        'webId'      : '123456',
        'displayName': 'Joe',
        'capabilities' : [
            'user.get',
            'user.getCapabilities',
            'user.password.update',
            'user.securityinfo.update'
        ]
    };
    var targetProducers = [{
        producer : {
            webId : 'oscar',
            fullName : 'Oscar T. Grouch',
            lexicalName :'Grouch, Oscar T.',
            roles : [
                {
                    roleCode : 'ABC12345',
                    statusCode : 'A'
                }
            ]
        }
    },{
        producer : {
            webId : 'johns',
            fullName : 'John T. Booker',
            lexicalName :'Booker, John T.',
            roles : [
                {
                    roleCode : 'ABC2323',
                    statusCode : 'A'
                }
            ]
        }
    }];

    var server;
    var idleWatcherStub;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // Set up a fake server to avoid tests making real ajax requests. The fake server doesn't
        // actually have to return data for any of the tests in this spec to work; it just allows
        // the tests to run without generating "connect ECONNREFUSED 127.0.0.1:3000" errors in Node.
        server = this.setupFakeServer(this.sinon, global, global.window);

        $('body').append('<div id="topbar"></div><div id="content"></div><div id="footer"></div>');
        osoApp = new OSOApp();
        idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');   
        osoApp.start();
    });

    after(function () {
        idleWatcherStub.restore();
        osoApp.destroy();
        $('#topbar, #footer, #content').remove();
        server.restore();
    });

    it('osoApp module should exist and can be instantiated', function () {
        expect(osoApp).to.exist.and.be.an.instanceof(Backbone.Marionette.Application);
    });

    it('osoApp has an initialize function', function () {
        expect(osoApp.initialize).to.exist;
        expect(osoApp.initialize).to.be.a('function');
    });

    it('osoApp has an requestUserData function', function () {
        expect(osoApp.requestUserData).to.exist;
        expect(osoApp.requestUserData).to.be.a('function');
    });

    /* TODO: Figure out why this test hangs if localdata-dev server is running.

    it('requestUserData should ready with user data and should call '+
            '`userReadySetup` and other util modules', function () {

        var analyticsInitStub = this.sinon.stub(app.analytics, 'init');
        var idleWatcherStartStub = this.sinon.stub(app.idleWatcher, 'start');

        server.respondWith('GET', '',
                [200, { 'Content-Type': 'application/json' },JSON.stringify(userResponseBody)]);

        document.cookie = 'persona=123abc';

        var requestUserData = function () {
            app.requestUserData();
            server.respond();
        };

        expect(requestUserData).to.not.throw(Error);
        //expect(idleWatcherStartStub).to.have.been.calledOnce;
        // expect(analyticsInitStub).to.have.been.calledOnce;

        idleWatcherStartStub.restore();
        analyticsInitStub.restore();
    });
    */

    it('osoApp has an addUIComponents function', function () {
        expect(osoApp.addUIComponents).to.exist;
        expect(osoApp.addUIComponents).to.be.a('function');
    });

    it('addUIComponents adds the components to the page', function () {
        var getUserStub = this.sinon.stub(osoApp.userModule, 'getDisplayName')
            .callsFake(function () {
                return 'Fred Flintstone';
            }
        );

        var addUIComponents = function () {
            osoApp.addUIComponents();
        };

        expect(addUIComponents).to.not.throw(Error);
        expect(osoApp.rootView).to.exist;
        expect(osoApp.navbar).to.exist;
        expect(osoApp.footer).to.exist;

        getUserStub.restore();
    });

    it('osoApp can be started', function () {
        var addUIComponentsSpy = this.sinon.spy(osoApp, 'addUIComponents');
        var requestUserDataSpy = this.sinon.spy(osoApp, 'requestUserData');

        var startApp = function () {
            osoApp.start();
        };

        expect(startApp).to.not.throw(Error);
        expect(requestUserDataSpy).to.have.been.calledOnce;
        expect(addUIComponentsSpy).to.have.been.calledOnce;

        requestUserDataSpy.restore();
        addUIComponentsSpy.restore();
    });

    it('osoApp.start() Should call rewrite method defined in rewriteDeprecatedURL', function (){
        var urlRewriteStub = this.sinon.stub(osoApp.rewriteURL, 'rewrite');
        osoApp.start();
        expect(urlRewriteStub).to.called.once;
        urlRewriteStub.restore();
    });

    describe('_getAnalyticsProviders method', function () {

        it('calls `_getGoogleTagManagerProvider()`', function () {
            var getGtmProviderStub = this.sinon.stub(osoApp, '_getGoogleTagManagerProvider');
            osoApp._getAnalyticsProviders();

            expect(getGtmProviderStub).to.have.been.calledOnce;
        });

        it('returns a single provider when `config.webTrends.active` ' +
            'is FALSE', function () {
            var providers;

            // ensure config is not set to enable WebTrends
            osoApp.config.webTrends.active = false;
            providers = osoApp._getAnalyticsProviders();

            expect(providers.length).to.equal(1);
        });

        it('returns two providers when `config.webTrends.active` is TRUE', function () {
            var providers;

            // ensure config is set to enable WebTrends
            osoApp.config.webTrends.active = true;
            providers = osoApp._getAnalyticsProviders();

            expect(providers.length).to.equal(2);
        });

    }); // _getAnalyticsProviders method

    describe('._handleUserChange method', function () {
        
        it('exists', function () {
            expect(osoApp._handleUserChange).to.exist;
            expect(osoApp._handleUserChange).to.be.a('function');
        });

        it('is called if the osoApp\'s userModule triggers a change event', function () {
            var methodSpy = this.sinon.spy(osoApp, '_handleUserChange');
            var dontStartTheRouter = this.sinon.stub(osoApp, '_configureRouter');

            osoApp.userReadySetup(); // pretend that the user data is loaded
            osoApp.userModule.triggerMethod('change');

            expect(methodSpy).to.have.been.calledOnce;
            dontStartTheRouter.restore();
            methodSpy.restore();
        });

        it('notifies the navbar if the displayName has changed', function () {
            var uunStub = this.sinon.stub(osoApp.navbar, 'updateUserName');

            osoApp._handleUserChange({displayName: 'Chauncy'});

            expect(uunStub).to.have.been.calledOnce;
            uunStub.restore();
        });
    }); // describe _handleUserChange method

    describe('._showPage method', function () {
        var ajaxStub;
        var analyticsInitStub;
        var fetchUserDataStub;
        var getUserStub;

        beforeEach(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                userResponseBody
            );

            analyticsInitStub = this.sinon.stub(osoApp.analytics, 'init');
            fetchUserDataStub = this.sinon.stub(osoApp.userModule, 'fetchUserData');
            getUserStub = this.sinon.stub(osoApp.userModule, 'getDisplayName')
                .callsFake(function () {
                    return 'Barney Rubble';
                }
            );
            
            osoApp.contentLayout = { showPage: function () {} };
            osoApp.sidebarApp = { setActiveItem: function () {} };
        });

        afterEach(function () {
            ajaxStub.restore();
            fetchUserDataStub.restore();
            analyticsInitStub.restore();
            getUserStub.restore();
        });

        it('exists', function () {
            expect(osoApp._showPage).to.be.a('function');
        });

        it('calls contentLayout.showPage', function () {
            var stubCl = this.sinon.stub(osoApp.contentLayout, 'showPage');

            osoApp._showPage('example-page');
            expect(stubCl).to.have.been.called;

            stubCl.restore();
        });

        it('calls sidebarApp.setActiveItem', function () {
            var stubSb = this.sinon.stub(osoApp.sidebarApp, 'setActiveItem');

            osoApp._showPage('somePage');
            expect(stubSb).to.have.been.called;

            stubSb.restore();
        });
    }); // describe ._showPage method

    describe('._handleNavEvent method', function () {
        var isValidPageIdStub;
        var showPageStub;
        var userHasPagePermissionsStub;
        var getHomePageForDelegateAccessSpy;

        beforeEach(function () {
            isValidPageIdStub = this.sinon.stub(OSOApp.prototype, '_isValidPageId');
            showPageStub = this.sinon.stub(OSOApp.prototype, '_showPage');
            userHasPagePermissionsStub = this.sinon.stub(OSOApp.prototype,
                '_userHasPagePermissions');

            getHomePageForDelegateAccessSpy = this.sinon.spy(OSOApp.prototype,
                'getHomePageForDelegateAccess');
            window.location.hash = '#home';
        });

        afterEach(function () {
            isValidPageIdStub.restore();
            showPageStub.restore();
            userHasPagePermissionsStub.restore();
            getHomePageForDelegateAccessSpy.restore();
        });

        it('exists', function () {
            expect(osoApp._handleNavEvent).to.be.a('function');
        });

        it('prevents an invalid page id from being displayed', function () {
            isValidPageIdStub.returns(false);

            osoApp._handleNavEvent('#invalid-page-id');

            expect(showPageStub).not.to.have.been.called;
        });

        it('prevents access to a page without proper user capabilities', function () {
            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(false);

            osoApp._handleNavEvent('#valid-page-id');

            expect(showPageStub).not.to.have.been.called;
        });

        it('allows user to access a valid page id with proper user capabilities', function () {
            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._handleNavEvent('#valid-page-id');

            expect(showPageStub).to.have.been.called;
        });

        it('handles subpages in the hash', function () {
            var pageId   = '#valid-page-id';
            var hash     = pageId + '/valid-subpage';

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._handleNavEvent(hash);

            expect(showPageStub).to.have.been
                .calledWith(pageId, {subpages:['valid-subpage']});
        });

        it('handles wcm "c" links in the hash', function () {
            var hash     = '#c/content/stuff';

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._handleNavEvent(hash);

            expect(showPageStub).to.have.been
                .calledWith(hash, {subpages:['content', 'stuff']});
        });

        it('Should call "getHomePageForDelegateAccess" ', function () {
            osoApp._handleNavEvent('#home');
            expect(getHomePageForDelegateAccessSpy).to.have.been.called;
        });

    }); // ._handleNavEvent method

    describe('_routerOnNavigate method', function () {
        var isValidPageIdStub;
        var showPageStub;
        var userHasPagePermissionsStub;
        var routerOnNavSpy;

        beforeEach(function () {
            isValidPageIdStub = this.sinon.stub(OSOApp.prototype, '_isValidPageId');
            showPageStub = this.sinon.stub(OSOApp.prototype, '_showPage');
            userHasPagePermissionsStub = this.sinon.stub(OSOApp.prototype,
                '_userHasPagePermissions');
            routerOnNavSpy = this.sinon.spy(OSOApp.prototype, '_routerOnNavigate');
        });

        afterEach(function () {
            routerOnNavSpy.restore();
            isValidPageIdStub.restore();
            userHasPagePermissionsStub.restore();
            showPageStub.restore();
        });

        it('exists', function () {
            expect(osoApp._routerOnNavigate).to.be.a('function');
        });

        /* TODO: This test runs osoApp._configureRouter(), which somehow causes a checkpointModule
        test to fail in an interesting way (if the checkpoint test is run after this test):

        × phantomjs on any platform - unit tests - Checkpoint Module (modules/checkpointModule) -
        writeCheckpoint - stateObj parameter - must be a simple object (not a primitive or array,
        etc) (0.002s)
        AssertionError: expected [Function] to throw error including 'writeCheckpoint(stateObj)
        parameter must be a plain JS object' but got 'undefined is not an object (evaluating
        \'this.contentLayout.showPage\')'
        No stack or location

        I've tried stopping Backbone.history after running this test. I've tried attaching the
        showPageStub and routerOnNavSpy to OSOApp.prototype rather than to the instantiated osoApp
        object. I've wasted enough time on it for now. --RKC 22MAR2016

        it('is called to handle "navigate" events', function () {

            osoApp._configureRouter();
            Backbone.history.navigate('tetris', {trigger: true});

            expect(routerOnNavSpy).to.have.been.called;
            // _routerOnNavigate gets the pageId out of the routeData and sends it as
            // a param to _showPage. Checking the param to _showPage is the easiest
            // way I can think of to ensure that the routeData sent into _routerOnNavigate
            // contains the correct pageId. (There must be an easier way!)
            // -- RKC 21MAR2106
            expect(showPageStub).to.have.been.calledWith('tetris');
        });
        */

        it('with no params, shows "home"', function () {
            var expectedPageId = 'home';
            var expectedStateObj = {};

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._routerOnNavigate();

            expect(showPageStub).to.have.been.calledWith(expectedPageId, expectedStateObj);
        });

        it('with malformed routeData param, shows "home"', function () {
            var expectedPageId = 'home';
            var expectedStateObj = {};

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._routerOnNavigate({invalid:'Not a valid routeData object'});

            expect(showPageStub).to.have.been.calledWith(expectedPageId, expectedStateObj);
        });

        it('prevents a page with an invalid id from being displayed', function () {
            var invalidPageRouteData = {params:{pageId:'#invalid-page-id'}, query: {}};
            isValidPageIdStub.returns(false);

            osoApp._routerOnNavigate(invalidPageRouteData);

            expect(showPageStub).not.to.have.been.called;
        });

        it('attempts to show pageId from routeData when a valid page id and proper user' +
            ' capabilities are present', function () {
            var routeData = {params:{pageId:'tetris'}, query: {}};
            var expectedPageId = 'tetris';
            var expectedStateObj = {};

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._routerOnNavigate(routeData);

            expect(showPageStub).to.have.been.calledWith(expectedPageId, expectedStateObj);
        });

        it('attaches the subpage URL part to the "stateObj" object', function () {
            var routeData = {
                params : {
                    pageId  : 'tetris',
                    subpages : 'welltris/new'
                }
            };

            var expectedPageId = 'tetris';
            var expectedStateObj = {
                subpages : ['welltris', 'new']
            };

            isValidPageIdStub.returns(true);
            userHasPagePermissionsStub.returns(true);

            osoApp._routerOnNavigate(routeData);

            expect(showPageStub).to.have.been.calledWith(expectedPageId, expectedStateObj);
        });

        it('router pattern for wcm should call _showPage method, if '+
                'pageId is valid and has permission to view', function () {
            
            //add temporary nav element to structure
            osoApp.pageLookupModel.get('structure')['c/interest-rates/Content-Care-Solutions'] = {
                capability:{
                    forSidebar : [{
                        all : true
                    }]
                },
                displayText:'Care Solutions',
                icon:'line-chart'
            };

            var routeData = {
                linked : 'wcm',
                params : {
                    pageId  : 'interest-rates/Content-Care-Solutions'
                }
            };

            var expectedPageId = 'c/interest-rates/Content-Care-Solutions';
            var expectedStateObj = {};

            //Restoring below methods to validate pageId  and it permission
            //to call _showPage method
            isValidPageIdStub.restore();
            userHasPagePermissionsStub.restore();

            osoApp._routerOnNavigate(routeData);

            expect(showPageStub).to.have.been.calledWith(expectedPageId, expectedStateObj);
        });

        it('router pattern for wcm should not call _showPage method, if '+
                'there is no capability to view content', function () {

            //add temporary nav element to structure
            osoApp.pageLookupModel.get('structure')['c/interest-rates/Content-Care-Solutions'] = {
                capability:{
                    forSidebar : [{
                        'no-permission' : true
                    }]
                },
                displayText:'Care Solutions',
                icon:'line-chart'
            };

            var routeData = {
                linked : 'wcm',
                params : {
                    pageId  : 'interest-rates/Content-Care-Solutions'
                }
            };

            //Restoring below methods to validate pageId  and it permission
            //to call _showPage method
            isValidPageIdStub.restore();
            userHasPagePermissionsStub.restore();

            osoApp._routerOnNavigate(routeData);

            expect(showPageStub).to.have.been.calledWith(
                'error',
                { message: 'The page you requested does not exist.' }
            );
        });

// Running this test causes later tests in userModule-spec.js to fail.
        it.skip('Should call "beginImpersonation" if query has "targetuser" parameter ', 
            function () {
            var routeData = {
                linked : 'page',
                params : {
                    pageId  : 'pending-policy-manager'
                },
                query : {
                    targetuser : 'johns'
                }
            };

            var beginImpersonationSpy = this.sinon.spy(osoApp.userModule, 'beginImpersonation');

            osoApp.userModule.userModel.set('capabilities', [
                'OLS:PRODUCER',
                'OLS:DELEGATE'
            ]);
            osoApp.userModule.userModel.set('delegation', targetProducers);

            location.hash = '#pending-policy-list?targetuser=johns';
            osoApp._routerOnNavigate(routeData);

            expect(beginImpersonationSpy).to.have.been.called;
            beginImpersonationSpy.restore();
        });

// Running this test causes later tests in userModule-spec.js to fail.
        it.skip('Should call "endImpersonation" is absense of "targetuser" param and user '+
                'is impersonated mode', function () {

            var routeData = {
                linked : 'page',
                params : {
                    pageId  : 'pending-policy-manager'
                },
                query : {}
            };
            var endImpersonationStub = this.sinon.stub(osoApp.userModule, 'endImpersonation');

            osoApp.userModule.userModel.set('capabilities', [
                'OLS:DELEGATE'
            ]);
            osoApp.userModule.userModel.set('delegation', targetProducers);

            osoApp.userModule.impersonatedWebId = 'johns';

            osoApp._routerOnNavigate(routeData);

            expect(endImpersonationStub).to.have.been.called;

            endImpersonationStub.restore();
        });

    }); // describe _routerOnNavigate method

    describe('_addSubpagesToStateObj method', function () {
        var stateObj;

        it('should exist', function () {
            expect(osoApp._addSubpagesToStateObj).to.be.exist;
        });

        it('Should update statObject with subpages if '+
                'parameter has subpages', function () {
            stateObj = {
                targetuser : 'someThing'
            };
            var expectedStateObj = {
                targetuser : 'someThing',
                subpages : [
                    'sub1',
                    'sub2'
                ]
            };
            stateObj = osoApp._addSubpagesToStateObj(stateObj, '/sub1/sub2/');
            expect(stateObj).to.be.deep.equal(expectedStateObj);
        });

        it('Should sent back stateObj as it is if there is no subpages', function () {
            stateObj = {
                targetuser : 'someThing'
            };
            var expectedStateObj = {
                targetuser : 'someThing'
            };
            stateObj = osoApp._addSubpagesToStateObj(stateObj, '');
            expect(stateObj).to.be.deep.equal(expectedStateObj);
        });

        it('Should sent back blank object if there is no parameter sent', function () {
            stateObj = osoApp._addSubpagesToStateObj();
            expect(stateObj).to.be.deep.equal({});
        });

    }); // end describe _addSubpagesToStateObj method

    describe('._isValidPageId method', function () {
        var pageLookupIsValidPageStub;
        var showPageSpy;
        var validPageId = '#valid-page-id';

        beforeEach(function () {
            pageLookupIsValidPageStub = this.sinon.stub(osoApp.pageLookupModel,
                'isValidPage').callsFake(function (id) {
                    return id === validPageId;
                }
            );
            showPageSpy = this.sinon.spy(osoApp.contentLayout, 'showPage');
        });

        afterEach(function () {
            pageLookupIsValidPageStub.restore();
            showPageSpy.restore();
        });

        it('exists', function () {
            expect(osoApp._isValidPageId).to.be.a('function');
        });

        describe('When an invalid page ID is requested', function () {
            var invalidPageId;
            var trackExceptionStub;
            var result;

            beforeEach(function () {
                invalidPageId      = 'i-am-not-a-valid-page-id';
                trackExceptionStub = this.sinon.stub(osoApp.analytics, 'trackException');
                result             = osoApp._isValidPageId(invalidPageId);
            });

            afterEach(function () {
                trackExceptionStub.restore();
            });

            it('returns false', function () {
                expect(result).to.be.false;
            });

        });

        it('shows the error page when an invalid page id is requested', function () {
            var invalidPageId = 'invalid-page-id';
            osoApp._isValidPageId(invalidPageId);

            expect(showPageSpy).to.have.been.calledWith('error');
        });

        describe('when a valid page id is requested', function () {

            it('returns true', function () {
                var result = osoApp._isValidPageId(validPageId);

                expect(result).to.be.true;
            });
        });

    }); // ._isValidPageId method

    describe('._userHasPagePermissions method', function () {
        var pageLookupUserCanAccessPageStub;
        var showPageSpy;

        beforeEach(function () {
            pageLookupUserCanAccessPageStub = this.sinon.stub(osoApp.pageLookupModel,
                'userCanAccessPage');
            showPageSpy = this.sinon.spy(osoApp.contentLayout, 'showPage');
        });

        afterEach(function () {
            pageLookupUserCanAccessPageStub.restore();
            showPageSpy.restore();
        });

        it('exists', function () {
            expect(osoApp._userHasPagePermissions).to.be.a('function');
        });

        describe('When user does NOT have permission', function () {
            var analyticsMessage;
            var trackExceptionStub;
            var result;
            var validPageId;

            beforeEach(function () {
                pageLookupUserCanAccessPageStub.returns(false);

                validPageId        = 'valid-page-id';
                trackExceptionStub = this.sinon.stub(osoApp.analytics, 'trackException');
                result             = osoApp._userHasPagePermissions(validPageId);
                analyticsMessage   = trackExceptionStub.getCalls()[0].args[0].message;
            });

            afterEach(function () {
                analyticsMessage = null;
                trackExceptionStub.restore();
            });

            it('returns false', function () {
                expect(result).to.be.false;
            });

            it('passes the pageId to the analytics module', function () {
                expect(analyticsMessage).to.contain(validPageId);
            });

            it('displays error page', function () {
                expect(showPageSpy).to.have.been.calledWith('error');
            });
        });

        it('displays error page when the user does not have permission', function () {
            pageLookupUserCanAccessPageStub.returns(false);
            var validPageId = 'valid-page-id';
            osoApp._userHasPagePermissions(validPageId);

            expect(showPageSpy).to.have.been.calledWith('error');
        });


        describe('when a user has valid permissions to view a valid page id', function () {

            beforeEach(function () {
                pageLookupUserCanAccessPageStub.returns(true);
            });

            it('returns true', function () {
                var validPageId = 'valid-page-id';
                var result = osoApp._userHasPagePermissions(validPageId);

                expect(result).to.be.true;
            });
        });

    }); // ._userHasPagePermissions method


    /* TODO: Figure out how to unit test the handling of a sidebar nav event
     now that the sidebar isn't added until after the user's
     "state:ready" event. --RKC 02/02/2016

    describe.skip('showPage at the app level', function () {

        var analyticsInitStub;
        var idleWatcherStartStub;
        var userInitStub;
        var getUserStub;

        it('handles a "nav" event from sidebarApp', function () {
            // We need to attach to the prototype before instantiating!
            var showPageStub = this.sinon.stub(OSOApp.prototype, '_showPage');
            app = new OSOApp({});
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');

            analyticsInitStub = this.sinon.stub(app.analytics, 'init');
            idleWatcherStartStub = this.sinon.stub(app.idleWatcher, 'start');
            userInitStub = this.sinon.stub(app.userModule, 'init');
            getUserStub = this.sinon.stub(app.userModule, 'getUser').callsFake(function () {
                return new Backbone.Model();
            });

            app.start();

            app.sidebarApp.trigger('nav', 'example-page');

            expect(showPageStub).to.have.been.calledOnce;

            userInitStub.restore();
            idleWatcherStartStub.restore();
            analyticsInitStub.restore();
            getUserStub.restore();

            OSOApp.prototype._showPage.restore();
        });
    }); */

    // Running this test causes Mocha to crash many specs later. --RKC Jan 29 2018
    describe.skip('delegation flow - userReadySetup', function () {
        var beginImpersonationSpy;
        var handleNavSpy;
        var startDelegateAccessSpy;
        var showProducerDelegateAccessMenuOptionsSpy;

        var dataDelegateUser = {
            displayName : 'Delegate Multiple',
            webId : 'D12334',
            capabilities : [
                'OLS:DELEGATE'
            ],
            delegation : [
                {
                    producer : {
                        webId : 'oscar',
                        fullName : 'Oscar T. Grouch',
                        lexicalName :'Grouch, Oscar T.',
                        roles : [
                            {
                                roleCode : 'ABC12345',
                                statusCode : 'A'
                            }
                        ]
                    }
                },
                {
                    producer : {
                        webId : 'bookert',
                        fullName : 'Booker T. John',
                        lexicalName :'John, Booker T.',
                        roles : [
                            {
                                roleCode : 'BCSS12332',
                                statusCode : 'A'
                            }
                        ]
                    }
                }
            ]
        };

        before(function () {
            idleWatcherStub.restore();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
        });

        beforeEach( function () {
            handleNavSpy  = this.sinon.spy(osoApp, '_handleNavEvent');
            startDelegateAccessSpy = this.sinon.spy(osoApp, '_startDelegateAccess');
            osoApp.userModule.userModel.set('capabilities', dataDelegateUser.capabilities);
            osoApp.userModule.userModel.set('delegation', dataDelegateUser.delegation);

            beginImpersonationSpy = this.sinon.spy(osoApp.userModule, 'beginImpersonation');
            osoApp.userReadySetup();
           
        });

        afterEach( function () {
            beginImpersonationSpy.restore();
            handleNavSpy.restore();
            startDelegateAccessSpy.restore();
        });

        describe('Capability OSO:DELEGATION with '+
                'Multiple producers delegation only', function () {
            it('should called startDelegateAccess and handleNav methods and '+
                    'should navigate to delegate home page,', function () {
            
                expect(startDelegateAccessSpy).to.be.called;
                expect(handleNavSpy).to.be.calledWith('#producer-delegate-list');
            });
        });

        describe('delegation ONLY single producer, with '+
                'capability "OLS:DELEGATE"', function () {
            
            it('should call beginImpersonation and startDelegateAccess', function () {
                osoApp.userModule.userModel.set('capabilities', ['OLS:DELEGATE']);
                osoApp.userModule.userModel.set('delegation', [{
                    producer : {
                        webId : 'oscar',
                        fullName : 'Oscar T. Grouch',
                        lexicalName :'Grouch, Oscar T.',
                        roles : [
                            {
                                roleCode : 'ABC12345',
                                statusCode : 'A'
                            }
                        ]
                    }
                }]);

                startDelegateAccessSpy.resetHistory();
                handleNavSpy.resetHistory();

                //currently it won't reset if that already exist in userModule
                osoApp.userModule.validTargets = null;
                osoApp.userReadySetup();

                expect(startDelegateAccessSpy).to.be.called;
                expect(handleNavSpy).to.not.be.called;
                expect(beginImpersonationSpy).to.be.called;
            });
        });

        describe('Capability OLS:DELEGATION and OLS:PRODUCERS with '+
                'Multiple/Single producers delegation ', function () {

            it('Should called showProducerDelegateAccessMenuOptions '+
                    'to display Delegate Access menu in navBar '+
                    'and but should not startDelegateAccess '+
                    'and handleNavSpy methods ', function () {

                // end impersonation if it is already exist
                osoApp.userModule.endImpersonation();

                showProducerDelegateAccessMenuOptionsSpy = 
                this.sinon.spy(osoApp.navbar, 'showProducerDelegateAccessMenuOptions');
                
                osoApp.userModule.userModel.set('capabilities', ['OLS:DELEGATE', 
                    'OLS:PRODUCER'
                ]);

                startDelegateAccessSpy.resetHistory();
                handleNavSpy.resetHistory();

                osoApp.userReadySetup();

                expect(startDelegateAccessSpy).to.not.be.called;
                expect(handleNavSpy).to.not.be.called;
                expect(showProducerDelegateAccessMenuOptionsSpy).to.be.called;

                showProducerDelegateAccessMenuOptionsSpy.restore();
            });
        });
    });

    // Running this test causes Mocha to crash many specs later. --RKC Jan 29 2018
    describe.skip('ViewAsProducer tests', function () {

        before(function () {
            idleWatcherStub.restore();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
        });

        beforeEach(function () {
            osoApp.userReadySetup();
        });

        describe('_viewAsProducer function', function () {

            beforeEach(function () {
                osoApp.userModule.impersonatedUserModel = new UserModel({
                    webId        : 'rsavage',
                    displayName  : 'Macho Man Randy Savage',
                    capabilities : ['user.test']
                });
            });

            it('exists', function () {
                expect(osoApp._viewAsProducer).to.exist.and.be.a('function');
            });

            it('Updates the UI with the impersonated user info', function () {
                var handleNavSpy  = this.sinon.spy(osoApp, '_handleNavEvent');
                var menuSpy       = this.sinon.spy(osoApp.appStructureModel, 'filterForSidebar');
                var sidebarSpy    = this.sinon.spy(osoApp.sidebarApp, 'reinitializeMenu');

                osoApp._viewAsProducer();

                expect(menuSpy).to.have.been.calledWith(['user.test']);
                expect(sidebarSpy).to.have.been.calledOnce;
                expect(handleNavSpy).to.have.been.calledOnce;

                var producerName = $('.alert-view-producer div.row div.col-sm-6 h3 strong')
                    .html();
                expect(producerName).to.equal('Macho Man Randy Savage');

                handleNavSpy.restore();
                menuSpy.restore();
                sidebarSpy.restore();
            });

            it('VaP - contentLayout showRibbon call and label', function () {
                var contentLayoutSpy  = this.sinon.spy(osoApp.contentLayout, 'showRibbon');
                var currentViewOptions;

                osoApp._viewAsProducer();
                currentViewOptions = osoApp.contentLayout.regionManager
                    ._regions.ribbon.currentView.options;

                expect(contentLayoutSpy).to.have.been.calledOnce;
                expect(currentViewOptions).to.have.property('label','Viewing as');
                contentLayoutSpy.restore();
            });

            it('contentLayout.showBanner() method should be called', function () {
                var showBannerStub;

                showBannerStub = this.sinon.stub(osoApp.contentLayout, 'showBanner');
                osoApp._viewAsProducer();

                expect(showBannerStub).to.be.calledOnce;
                showBannerStub.restore();
            });

            describe('Delegation flow', function () {
                it('Only Delegate - contentLayout showRibbon call and label', function () {
                    var currentViewOptions;
                    
                    // isDelegate
                    osoApp._viewAsProducer(true);
                    currentViewOptions = osoApp.contentLayout.regionManager
                        ._regions.ribbon.currentView.options;

                    expect(currentViewOptions).to.have.property('label','Delegate for');
                });

                it('should redirect to #home if user ONLY delegate and has '+
                        'multiple producers', function () {     
                    var handleNavSpy = this.sinon.spy(osoApp, '_handleNavEvent');
                    osoApp._viewAsProducer(true, true); 
                    expect(handleNavSpy).to.have.been.calledWith('#home');
                    handleNavSpy.restore();
                });

                it('should redirect to #home if user is producer delegate and has '+
                        'multiple producers', function () {
                    var handleNavSpy = this.sinon.spy(osoApp, '_handleNavEvent');
                    osoApp._viewAsProducer(true, true, true); 
                    expect(handleNavSpy).to.have.been.calledWith('#home');
                    handleNavSpy.restore();
                });

                it('should redirect to #home if user is ONLY delegate, '+
                        'no multiple producers', function () {
                    var handleNavSpy = this.sinon.spy(osoApp, '_handleNavEvent');
                    osoApp._viewAsProducer(true, false, false); 
                    expect(handleNavSpy).to.have.been.calledWith('#home');
                    handleNavSpy.restore();
                });

                it('should redirect to specific page if parameter has '+
                        'redirectHash', function () {
                    var handleNavSpy = this.sinon.spy(osoApp, '_handleNavEvent');
                    var redirectHash = '#pending-policy-manager/org';
                    osoApp._viewAsProducer(true, true, true, redirectHash); 
                    expect(handleNavSpy).to.have.been.calledWith(redirectHash);
                    handleNavSpy.restore();
                });

            });
        });

        describe('_endViewAsProducer function', function () {

            it('exists', function () {
                expect(osoApp._endViewAsProducer).to.exist.and.be.a('function');
            });

            it('Updates the UI with the logged-in user info', function () {
                var menuSpy      = this.sinon.spy(osoApp.appStructureModel, 'filterForSidebar');
                osoApp.userModule.impersonatedUserModel = new UserModel({
                    webId        : 'rsavage',
                    displayName  : 'Macho Man Randy Savage',
                    capabilities : ['user.test']
                });
                osoApp._viewAsProducer();
                var ribbon = $('.alert-view-producer');
                expect(ribbon).to.have.length(1);

                osoApp._endViewAsProducer();
                ribbon = $('.alert-view-producer');
                expect(menuSpy).to.have.been.calledTwice;
                expect(ribbon).to.have.length(0);

                menuSpy.restore();
            });

            it('contentLayout.showBanner() method should be called with no ' +
                'parameters', function () {
                var showBannerStub;

                showBannerStub = this.sinon.stub(osoApp.contentLayout, 'showBanner');
                osoApp._endViewAsProducer();

                expect(showBannerStub).to.be.calledOnce;

                showBannerStub.restore();
            });

            describe('Delegation flow - navigation', function () {
                var handleNavSpy;
                beforeEach(function () {
                    handleNavSpy = this.sinon.spy(osoApp, '_handleNavEvent');
                });

                afterEach(function () {
                    handleNavSpy.restore();
                });

                it('ONLY delegation and multiple producers', function () {
                    osoApp._endViewAsProducer(true, true);
                    expect(handleNavSpy).to.have.been.calledWith('#producer-delegate-list');
                });

                it('Producer delegation and single producer', function () {
                    osoApp._endViewAsProducer(true, true, true);
                    expect(handleNavSpy).to.have.been.calledWith('#home');
                });

                it('Producer delegation and multiple producer', function () {
                    osoApp._endViewAsProducer(true, false, true);
                    expect(handleNavSpy).to.have.been.calledWith('#home');
                });

                it('Should not call _handleNavEvent when there is '+
                        'redirectFlag with "false"', function () {
                    handleNavSpy.resetHistory();
                    osoApp._endViewAsProducer(true, false, true, false);
                    expect(handleNavSpy).not.to.have.been.called;
                });
            });
        });
    });

    describe('getHomePageForDelegateAccessSpy method', function () {
        
        it('Should call getHomePageForDelegateAccess', function () {
            var getHomePageForDelegateAccessSpy = this.sinon.spy(osoApp, 
                'getHomePageForDelegateAccess');
            osoApp._handleNavEvent('#home');
            expect(getHomePageForDelegateAccessSpy).to.have.been.calledWith('#home');
            getHomePageForDelegateAccessSpy.restore();
        });

        it('ONLY Delegate with multiple producer', function () {
            idleWatcherStub.restore();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
            osoApp.userModule.userModel.set('capabilities', ['OLS:DELEGATE']);
            osoApp.userModule.userModel.set('delegation', targetProducers);
            var homePageId = osoApp.getHomePageForDelegateAccess('home');
            expect(homePageId).to.be.equal('producer-delegate-list');
        });

        it('should return null incase pageId is missing '+
            'or not a delegate user, even doesn\'t have impersonatedWebId', function (){
            osoApp.userModule.userModel.set('capabilities', ['some_capability']);
            osoApp.userModule.userModel.set('delegation', targetProducers);
            osoApp.userModule.impersonatedWebId = null;
            var homePageId = osoApp.getHomePageForDelegateAccess();     
            expect(homePageId).to.be.null;

            homePageId = osoApp.getHomePageForDelegateAccess('home');     
            expect(homePageId).to.be.null;
        });
    });

    // Running this test causes Mocha to crash many specs later. --RKC Jan 29 2018
    describe.skip('_startDelegateAccess method', function () {
       
        it('should exist', function () {
            expect(osoApp._startDelegateAccess).to.exist.and.to.be.a.function;
        });

        it('should call userModule.endImpersonation when user is a producer and in '+
            'delegate impersonated state and has multiple targer producers ', function () {
            idleWatcherStub.restore();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
            osoApp.userModule.impersonatedWebId = 'impersonatedUser';
            osoApp.userModule.userModel.set('capabilities', [
                'OLS:PRODUCER',
                'OLS:DELEGATE'
            ]);
            osoApp.userModule.userModel.set('delegation', targetProducers);
            var endImpersonationSpy = this.sinon.spy(osoApp.userModule, 'endImpersonation');
            osoApp._startDelegateAccess();
            expect(endImpersonationSpy).to.be.calledOnce;
            endImpersonationSpy.restore();
        });
    });

    describe('getImpersonatedDataFromQueryString method', function () {

        before(function () {
            idleWatcherStub.restore();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
        });
        
        it('should exist', function () {
            expect(osoApp.getImpersonatedDataFromQueryString).to.exist.and.be.a('function');
        });

        it('should return producer info if URL Query Param has "targetuser" (VaP) ', function () {
            //update hash
            location.hash = '#pending-policy-list?targetuser=jbell';
            var expectedProducerInfo = {
                webId : 'jbell',
                redirectHash : location.hash
            };
            var producerInfo = osoApp.getImpersonatedDataFromQueryString();
            expect(producerInfo).to.deep.equal(expectedProducerInfo);
        });

        it('should return producer info if URL Query Param has "as" ' +
            'but not "targetuser" (VaP) ', function () {
            //update hash
            location.hash = '#pending-policy-list?as=jbell';
            var expectedProducerInfo = {
                webId : 'jbell',
                redirectHash : location.hash
            };
            var producerInfo = osoApp.getImpersonatedDataFromQueryString();
            expect(producerInfo).to.deep.equal(expectedProducerInfo);
        });

        it('producer info should match (Delegation)', function () {
            osoApp.userModule.userModel.set('capabilities', [
                'OLS:PRODUCER',
                'OLS:DELEGATE'
            ]);
            osoApp.userModule.userModel.set('delegation', targetProducers);

            //update hash
            location.hash = '#pending-policy-list?targetuser=johns';
            var producerInfo         = osoApp.getImpersonatedDataFromQueryString();
            var expectedProducerInfo = targetProducers[1].producer;
            expectedProducerInfo.redirectHash = location.hash;
            expect(producerInfo).to.deep.equal(expectedProducerInfo);
        });

        it('Producer info Should be undefined if there is no '+
                '"targetuser" Query Param ', function () {
            
            location.hash = '#pending-policy-list';
            var producerInfo         = osoApp.getImpersonatedDataFromQueryString();
            expect(producerInfo).to.be.undefined;
        });
    }); // Method "getImpersonatedDataFromQueryString"

    // Running this test causes Mocha to crash many specs later. --RKC Jan 29 2018
    describe.skip('_getTargetURL method' , function () {
        var errorLogSpy;
        var getTargetURLSpy;

        before(function () {
            idleWatcherStub.restore();
            osoApp.destroy();
            osoApp = new OSOApp();
            idleWatcherStub = this.sinon.stub(osoApp.idleWatcher, 'start');
            osoApp.start();
        });

        beforeEach(function () {
            getTargetURLSpy  = this.sinon.spy(osoApp, '_getTargetURL');
            errorLogSpy      = this.sinon.spy(jsdomConsole, 'error');
        });

        afterEach(function () {
            getTargetURLSpy.restore();
            errorLogSpy.restore();
        });

        it('should exist', function () {
            expect(osoApp._getTargetURL).to.exist.and.be.a('function');
        });

        it('should invoked while not in impersonated state ', function () {
            osoApp.userReadySetup();
            expect(getTargetURLSpy).to.have.been.called;
        });

        it('should invoked from _viewAsProducer while in impersonated state', function () {
            osoApp.userModule.impersonatedUserModel = new UserModel({
                webId        : 'oscar',
                displayName  : 'Oscar',
                capabilities : ['user.test']
            });
            osoApp._viewAsProducer(true);
            expect(getTargetURLSpy).to.have.been.called;
        });

        it('should throw error if $.deparam plugin is not loaded', function () {
            var saveDeparam = Backbone.$.deparam;
            Backbone.$.deparam = null;
            var fn = function () {
                osoApp._getTargetURL(
                    'http://onesourceonline.oneamerica.com?'+
                    'targeturl=https://api.oneamerica.com/secure/web/sso/pinpoint',
                    true
                );
            };

            expect(fn).to.throw(osoApp.errors.jQueryDeparamMissing);

            Backbone.$.deparam = saveDeparam;
        });

        it('should returns targetURL if domain is "oneamerica.com" ', function () {
            var result = osoApp._getTargetURL(
                'http://onesourceonline.oneamerica.com?'+
                'targeturl=https://api.oneamerica.com/secure/web/sso/pinpoint',
                true
            );

            expect(result).to.be.equal('https://api.oneamerica.com/secure/web/sso/pinpoint');
        });

        it('should call console.error if domain is not "oneamerica.com" ', function () {
            osoApp._getTargetURL(
                'http://onesourceonline.oneamerica.com?'+
                'targeturl=https://api.someurl.com/secure/web/sso/pinpoint',
                true
            );

            expect(errorLogSpy).to.have.been.calledWith(
                ['osoApp._getTargetURL - Invalid domain : someurl.com']
            );
        });

        it('should call console.error if targeturl is not valid URL ', function () {
            osoApp._getTargetURL(
                'http://onesourceonline.oneamerica.com?'+
                'targeturl=api.someurl.com/secure/web/sso/pinpoint',
                true
            );

            expect(errorLogSpy).to.have.been.calledWith(
                ['osoApp._getTargetURL - Invalid targeturl : '+
                'api.someurl.com/secure/web/sso/pinpoint']
            );
        });

        it('should returns undefined if targeturl missing', function () {
            var result = osoApp._getTargetURL(
                'http://onesourceonlie.oneamerica.com', 
                true
            );

            expect(result).to.be.undefined;
        });
    });
});

/* global expect:false */

var helpers = require('./helpers/helpers');

var TotalFileSizeView = require('../dist/pages/requirementSubmission/views/total-file-size-v');

var RequirementSubmissionFormModel = require(
    '../dist/pages/requirementSubmission/models/requirement-submission-form-m'
);

var RequirementSubmissionFormViewModel = require(
    '../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm'
);

describe('Requirement Submission Form - Total File Size View ' +
    '(pages/requirementSubmission/views/total-file-size-v.js)', function () {
        
    var domainModel;
    var view;

    beforeEach(function () {
        domainModel = new RequirementSubmissionFormModel();

        view = new TotalFileSizeView({
            model : new RequirementSubmissionFormViewModel({
                domainModel : domainModel,
                policyId    : 123456789
            })
        });

        view.render();
    });

    afterEach(function () {
        view.destroy();
    });

    it('Total File Size View exists', function () {
        expect(TotalFileSizeView).to.exist;
    });

    it('Total file size displayed', function () {
        var bytes = 5242880;    // 5mb
        var expectedMsg = 'Total file size: 5.0 MB of 25 MB';
        var actualValue;

        view.model.set('totalFileSizeInBytes', bytes);

        actualValue = helpers
            .viewHelpers.removeDuplicateWhitespace(view.$el.find('p').text().trim());

        expect(actualValue).to.equal(expectedMsg);
    });

    describe('Error message and associated CSS classes', function () {

        it('are NOT displayed when "serviceFileErrorText" is NOT set in the model',
            function () {

            expect(view.model.has('serviceFileErrorText')).to.be.false;
            expect(view.$el.find('#service-file-error').hasClass('hidden')).to.be.true;
        });

        it('are displayed when "serviceFileErrorText" is set in the model', function () {
            var errorMsg = 'Uh oh Spaghettios!';

            view.model.set('serviceFileErrorText', errorMsg);

            expect(view.$el.find('#service-file-error').hasClass('hidden')).to.be.false;
            expect(view.$el.find('#service-file-error').text().trim()).to.equal(errorMsg);

        });
    });     // Error message and associated CSS classes

    describe('initialize method', function () {

        it('exists as a function', function () {
            expect(view.initialize).to.exist.and.be.a('function');
        });

        it('should throw an error if not passed a model or policyId', function () {
            var expectedErrorMsg = TotalFileSizeView.prototype.errors.invalidOptions;
            var fn = function () {
                new TotalFileSizeView();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedErrorMsg);
        });

        it('should create model if passed policyId', function () {
            view.destroy();
            view = new TotalFileSizeView({
                policyId : 987656789
            });

            expect(view.model).to.exist;
        });

    });     // initialize method

});

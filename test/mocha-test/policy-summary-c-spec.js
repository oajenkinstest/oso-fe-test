/* global expect:false */

var PolicySummaryCollection = require('../dist/pages/policy/collections/policy-summary-c');

describe('Policy Summary Collection ' +
    '(pages/policy/collections/policy-summary-c.js)', function () {

    it('Constructor throws error if policyNumber prop is not in options', function () {
        var fn = function () {
            new PolicySummaryCollection(); // eslint-disable-line no-new
        };
        expect(fn).to.throw(
            PolicySummaryCollection.prototype.errors.policyNumberRequired
        );
    });

    it('Can be instantiated with policyNumber prop in options', function () {
        var fn = function () {
            new PolicySummaryCollection( // eslint-disable-line no-new
                null, { policyNumber:1234567890 }
            );
        };
        expect(fn).to.not.throw(Error);
    });

    it('Uses the policyNumber for the url', function () {
        var policyNumber = 1234567890;
        var col = new PolicySummaryCollection(null, {policyNumber:policyNumber});
        var url = col.url();
        expect(url.indexOf('policies?policyNumber=' + policyNumber))
            .to.be.greaterThan(1);
    });
});

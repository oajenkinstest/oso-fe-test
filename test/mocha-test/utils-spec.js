/* global describe: false, expect:false */

/**
 * Utility module spec
 */

var utils   = require('../dist/utils/utils');

describe('Utility Test Spec', function() {

    it('utils should exist', function() {
        expect(utils).to.exist;
    });

    describe('global method', function () {
        it('exist and a function', function (){
            expect(utils.global).to.exist.and.be.a('function');
        });

        it('should return global object', function () {
            expect(utils.global()).to.deep.equal(utils);
        });
    });

    describe('should extend with submodules', function () {
        describe('datatables', function () {
            it('"errors.dataTableObject" should exist', function () {
                expect(utils.errors.dataTableObject).to.be.exist;
            });

            it('formatDataTable method should exist', function () {
                expect(utils.formatDataTable).to.be.exist.and.to.be.a('function');
            });
        });

        describe('formatting', function () {
            it('"errors.bytesToMegabytes" should exist', function () {
                expect(utils.errors.bytesToMegabytes).to.be.exist;
            });

            it('bytesToMegabytesForDisplay method should exist', function () {
                expect(utils.bytesToMegabytesForDisplay).to.be.exist.and.to.be.a('function');
            });
        });

        describe('ui', function () {
            it('"errors.scrollToJQuery" should exist', function () {
                expect(utils.errors.scrollToJQuery).to.be.exist;
            });

            it('unwrapView method should exist', function () {
                expect(utils.unwrapView).to.be.exist.and.to.be.a('function');
            });
        });
    });

});

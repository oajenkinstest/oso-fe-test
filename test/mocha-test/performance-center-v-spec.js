/* global expect:false, $:false, isCanvasLoaded:false, sinon:false */

var helpers = require('./helpers/helpers');

var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);

var utils   = require('../dist/utils/utils');

describe('Performance Center View ' +
    '(modules/performance-center/views/performance-center-v.js)', function () {
    var ajaxStub;
    var view;
    before(function() {

        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
        
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
    });

    describe('Basic view tests', function () {

        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                {}
            );
            view = new PerformanceCenterView({
                model: new PerformanceCenterModel()
            });
        });

        after(function (){
            ajaxStub.restore();
            view.destroy();
        });

        it('performance-center-v should exist', function () {
            expect(PerformanceCenterView).to.exist;
        });

        it('performance center view can be instantiated', function () {
            expect(view).not.to.be.undefined;
        });

        it('should not visible any DOM elements if data not exist' , function () {
            expect(view.$el.html()).to.equal('');
        });
    });
 
    describe('refresh date', function () {
        var responseBody = helpers.pcData.responseBody;
        var root;
        var model;
        before(function (){
            root = helpers.viewHelpers.createRootView();
            root.render();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );

            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });
            root.contentRegion.show(view);
        });
        after(function () {
            ajaxStub.restore();
            root.destroy();
        });
        
        it('should displayed', function () {
            var expectedDate = utils.formatDate(view.model.get('refreshDate'),'MM/DD/YYYY');
            expect(view.$el.find('#refresh-date').text()).to.contain(expectedDate);
            expect(view.$el.find('#refresh-date').text()).to.contain('Last Refresh Date:');
        });

        it('should not displayed', function () {
            var copyResponseBody = $.extend(true,{}, responseBody);
            copyResponseBody.calendarYearProduction.asOfDate = null;
            ajaxStub.restore();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyResponseBody
            );
            view = new PerformanceCenterView({
                model: model
            });
            root.contentRegion.show(view);
            expect(view.$el.find('#refresh-date').length).to.equal(0);
        });
    });

    describe('More info link display', function () {

        var responseBody = helpers.pcData.responseBody;
        var root;
        var model;

        before(function () {
            root = helpers.viewHelpers.createRootView();
            root.render();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );

            model = new PerformanceCenterModel();
        });

        after(function () {
            ajaxStub.restore();
            root.destroy();
        });

        it('more info / rule link should displayed if \'showInfoLinks\' flag is true', function () {
            view = new PerformanceCenterView({
                model: model,
                showInfoLinks: true
            });
            root.contentRegion.show(view);

            //First Year Commission, life lives
            var moreInfolinks;
            moreInfolinks = view.fycRegion.$el
                .find('div.well-gray-footer a')
                .filter(function () {
                    return $(this).text() === 'More Info';
                });

            expect(moreInfolinks).to.have.length.above(0);
        });

        it('more info / rule link should not displayed if "showInfoLinks" is not set', function () {
            
            view = new PerformanceCenterView({
                model: model
            });
            root.contentRegion.show(view);

            //NEC, life lives
            var moreInfolinks;
            moreInfolinks = view.fycRegion.$el
                .find('div.well-gray-footer a')
                .filter(function () {
                    return $(this).text() === 'more info';
                });

            expect(moreInfolinks).to.have.length(0);
        });
    });

    describe ('Add separator using <hr> tag', function () {
        var responseBody = helpers.pcData.responseBody;
        var rootView;
        var model;

        before(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
        });

        after(function () {
            ajaxStub.restore();
        });
        
        it('<hr> tag should separate the first two rows', function () {
            var elementAfterFirstRow;
            var expectedElement = 'HR';
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );

            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });

            rootView.contentRegion.show(view);

            elementAfterFirstRow = view.$el.find('.row:first').next()[0].tagName;

            expect(elementAfterFirstRow).to.equal(expectedElement);
        });

        it('<hr> tag should not exist if only one row is present', function () {
            var hrCount;
            var noDialsResponse = {
                calendarYearProduction : {
                    commission : responseBody.calendarYearProduction.commission,
                    fypc       : responseBody.calendarYearProduction.fypc
                }
            };
            ajaxStub.restore();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                noDialsResponse
            );

            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });

            rootView.contentRegion.show(view);

            hrCount = view.$el.find('hr').length;

            expect(hrCount).to.equal(0);
        });
    });

    describe ('collapse Peformance Center panel as default ', function () {
        var responseBody = helpers.pcData.responseBody;

        var root;
        var model;

        var performanceCenterCollapseWrapper;
        var _handlePCCollapseShowSpy;
        var _handlePCCollapseShownSpy;
        var _handlePCCollapseHideSpy;
        var _handlePCCollapseHiddenSpy;

        before(function () {
            _handlePCCollapseShowSpy = 
                this.sinon.spy(PerformanceCenterView.prototype,'_handlePCCollapseShow');
            _handlePCCollapseShownSpy = 
                this.sinon.spy(PerformanceCenterView.prototype,'_handlePCCollapseShown');

            _handlePCCollapseHideSpy = 
                this.sinon.spy(PerformanceCenterView.prototype,'_handlePCCollapseHide');
            _handlePCCollapseHiddenSpy = 
                this.sinon.spy(PerformanceCenterView.prototype,'_handlePCCollapseHidden');

            root = helpers.viewHelpers.createRootView();
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );

            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });

            root.render();
            root.contentRegion.show(view);
            performanceCenterCollapseWrapper = view.ui.performanceCenterCollapseWrapper;
        });

        after(function () {
            ajaxStub.restore();
            root.destroy();
            _handlePCCollapseShowSpy.restore();
            _handlePCCollapseShownSpy.restore();
            _handlePCCollapseHideSpy.restore();
            _handlePCCollapseHiddenSpy.restore();
        });

        it ('Performance Center should be in collapsed state by default', function () {
            expect(performanceCenterCollapseWrapper.hasClass('in')).to.be.false;
        });

        it ('Default panel height should be 55px', function () {
            expect(performanceCenterCollapseWrapper.css('height')).to.equal('55px');
        });

        describe('method _handlePCCollapseShow', function () {
            it('should exist and be a function', function () {
                expect(view._handlePCCollapseShow).to.exist.and.be.a('function');
            });

            it('should called while expand start', function (){
                performanceCenterCollapseWrapper.collapse('show');
                expect(_handlePCCollapseShowSpy).to.have.been.called;
            });

            it('collapse button label should be "Collapse"', function (){
                expect(view.ui.performanceCenterCollapseBtn.text()).to.equal('Collapse');
            });
        });

        describe('method _handlePCCollapseShown', function () {
            it('should exist and be a function', function () {
                expect(view._handlePCCollapseShown).to.exist.and.be.a('function');
            });

            it('should called while expand finish', function (){
                performanceCenterCollapseWrapper.collapse('show');
                expect(_handlePCCollapseShownSpy).to.have.been.called;
            });
        });

        describe('method _handlePCCollapseHide', function () {
            it('should exist and be a function', function () {
                expect(view._handlePCCollapseHide).to.exist.and.be.a('function');
            });

            it('should called while collapse start', function (){
                performanceCenterCollapseWrapper.collapse('hide');
                expect(_handlePCCollapseHideSpy).to.have.been.called;
            });

            it('collapse button label should be "Expand"', function (){
                expect(view.ui.performanceCenterCollapseBtn.text()).to.equal('Expand');
            });
        });

        describe('method _handlePCCollapseHidden', function () {
            it('should exist and be a function', function () {
                expect(view._handlePCCollapseHidden).to.exist.and.be.a('function');
            });

            it('should called while collapse finish', function (){
                performanceCenterCollapseWrapper.collapse('hide');
                expect(_handlePCCollapseHiddenSpy).to.have.been.called;
            });
        });

    });
});

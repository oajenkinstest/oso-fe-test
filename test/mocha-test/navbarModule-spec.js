/* global expect:false, Backbone:false */

var Navbar = require('../dist/modules/navbar/navbarModule');

describe('Navbar Module (modules/navbar/navbarModule.js)', function () {

    it('exists', function () {
        expect(Navbar).to.exist;
    });

    describe('instantiation', function() {

        it('throws an error if options are omitted', function () {
            var fn = function () {
                var navbar = new Navbar();  // eslint-disable-line no-unused-vars
            };
            expect(fn).to.throw(Navbar.prototype.errors.options);
        });

        it('throws an error if no region is given in options', function () {
            var fn = function () {
                var navbar = new Navbar({   // eslint-disable-line no-unused-vars
                    userName: 'Alice'
                });
            };
            expect(fn).to.throw(Navbar.prototype.errors.parentRegion);
        });

        it('can be instantiated with a userName and region', function () {
            var fn = function () {
                var testRegion = new Backbone.Marionette.Region({
                    el : document.createElement('div')
                });
                var navbar = new Navbar({   // eslint-disable-line no-unused-vars
                    region: testRegion,
                    userName: 'Bob'
                });

                testRegion.reset();
                testRegion = null;
            };
            expect(fn).to.not.throw(Error);
        });
    }); // instantiation

    describe('after instantiation', function() {
        var navbar;
        var navbarProps;
        var testRegion;

        beforeEach(function() {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });

            navbarProps = {
                region: testRegion,
                userName: 'Daffy Duck'
            };

            navbar = new Navbar(navbarProps);
        });

        afterEach(function() {
            testRegion.reset();
            navbar = null;
        });

        it('has a navbarView', function () {
            expect(navbar.navbarView).to.exist;
        });

    }); // after instantiation

    describe('rendering', function() {
        var navbar;
        var navbarProps;
        var renderStub;
        var testRegion;

        beforeEach(function () {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });

            navbarProps = {
                region: testRegion,
                userName: 'Daffy Duck'
            };

            navbar = new Navbar(navbarProps);
            renderStub = this.sinon.stub(navbar.navbarView, 'render');
        });

        afterEach(function () {
            testRegion.reset();
            navbar = null;
            renderStub.restore();
        });


        it('inserts an #__OA_LINK__ link into document', function () {
            expect(navbar.navbarView.$('#__OA_LINK__')).not.to.be.null;
        });

        it('clicking the navbar-header link triggers "nav" event', function () {
            var navCallback = this.sinon.stub();
            navbar.on('nav', navCallback);

            // Simulate a click on a navbar header link
            navbar.navbarView.$('#__OA_LINK__').click();

            expect(navCallback).to.have.been.calledOnce;

            navbar.off('nav');
        });

        it('inserts an #logout_link into document', function () {
            expect(navbar.navbarView.$('#logout_link')).not.to.be.null;
        });

        it('clicking the logout link triggers "logout" event', function () {
            var logoutCallback = this.sinon.stub();
            navbar.on('logout', logoutCallback);

            // Simulate a click on a navbar logout link
            navbar.navbarView.$('#logout_link').click();

            expect(logoutCallback).to.have.been.calledOnce;

            navbar.off('logout');
        });
    }); // rendering

    describe('updateUserName method', function () {
        var navbar;
        var navbarProps;
        var renderStub;
        var testRegion;

        beforeEach(function () {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });

            navbarProps = {
                region: testRegion,
                userName: 'Daffy Duck'
            };

            navbar = new Navbar(navbarProps);
            renderStub = this.sinon.stub(navbar.navbarView, 'render');
        });

        afterEach(function () {
            testRegion.reset();
            navbar = null;
            renderStub.restore();
        });

        it('exists as a function', function () {
            expect(navbar.updateUserName).to.exist.and.be.a('function');
        });

        it('re-renders the view', function () {
            navbar.updateUserName('TestGuy');
            expect(renderStub).to.have.been.calledOnce;
        });
    }); // updateUserName method

    describe('showProducerDelegateAccessMenuOptions method', function () {
        var navbar;
        var navbarProps;
        var renderStub;
        var testRegion;

        beforeEach(function () {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });

            navbarProps = {
                region: testRegion,
                userName: 'Daffy Duck'
            };

            navbar = new Navbar(navbarProps);
            renderStub = this.sinon.stub(navbar.navbarView, 'render');
        });

        afterEach(function () {
            testRegion.reset();
            navbar = null;
            renderStub.restore();
        });

        it('exists as a function', function () {
            expect(navbar.showProducerDelegateAccessMenuOptions).to.exist.and.be.a('function');
        });

        it('re-renders the view', function () {
            navbar.showProducerDelegateAccessMenuOptions(true);
            expect(renderStub).to.be.calledOnce;
        });

        it('renders menu element if input is true', function () {
            renderStub.restore();
            navbar.showProducerDelegateAccessMenuOptions(true);
            expect(navbar.navbarView.ui.producerDelegateAccessLink).to.be.length(1);
        });

        it('does not render menu element if input is false', function () {
            renderStub.restore();
            navbar.showProducerDelegateAccessMenuOptions(false);
            expect(navbar.navbarView.ui.producerDelegateAccessLink).to.be.length(0);
        });

        it('calls "startProducerDelegateAccess" event while clicking '+
            'on "producerDelegateAccessLink"', function () {
            var startProducerDelegateAccessCallback = this.sinon.stub();
            navbar.on('startProducerDelegateAccess', startProducerDelegateAccessCallback);
            
            renderStub.restore();
            navbar.showProducerDelegateAccessMenuOptions(true);
            navbar.navbarView.ui.producerDelegateAccessLink.click();

            expect(startProducerDelegateAccessCallback).to.be.calledOnce;
            navbar.off('startProducerDelegateAccess');
        });

        describe ('reRender method', function () {
            it('should exist', function () {
                expect(navbar.reRender).to.exist.and.be.a('function');
            });

            it('should render NavView', function (){
                navbar.reRender();
                expect(renderStub).to.be.calledOnce;
            }) ;
        }); // reRender method
        
    }); // showProducerDelegateAccessMenuOptions method

});


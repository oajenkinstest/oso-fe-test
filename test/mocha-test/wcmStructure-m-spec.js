/* global describe:false, expect:false */

var WcmStructureModel = require('../dist/models/wcmStructure-m');

describe('WCM Structure Model (models/wcmStructure-m.js)', function () {

    it('exists', function () {
        expect(WcmStructureModel).to.exist;
    });


    describe('default "structure" property', function () {

        var wcmStructure;

        beforeEach(function () {
            var instance = new WcmStructureModel();
            wcmStructure = instance.get('structure');
        });

        afterEach(function () {
            wcmStructure = null;
        });

        it('is a non-empty array', function () {
            expect(wcmStructure).to.be.an('array')
                .that.is.not.empty;
        });

        it('all nodes only include valid keys', function () {
            var validKeys = 'icon capability displayText link subItems forSidebar ' +
                'forPageAccess activeFor isExternal';

            // Recursive function to iterate through an object and make sure
            // that it contains no invalid property names or empty arrays
            var hasValidKeys = function (objToValidate) {
                var i, key;

                // If objToValidate is an array, it mustn't be empty and all of its
                // elements must also be valid.
                if (Array.isArray(objToValidate)) {

                    expect(objToValidate).is.not.empty;

                    for (i=0; i<objToValidate.length; i++) {
                        hasValidKeys(objToValidate[i]);
                    }
                
                } else {

                    // Other properties must be limited to list of valid keys
                    for (key in objToValidate) {
                        if (objToValidate.hasOwnProperty(key)) {
                            
                            //key with number index can be acceptable
                            //Validating only key with format String
                            if (isNaN(key)) {
                                expect(validKeys).to.have.string(key);
                            }

                            if (Array.isArray(objToValidate[key])) {
                                hasValidKeys(objToValidate[key]);
                            }
                        }
                    }
                }
            };

            hasValidKeys(wcmStructure);
        });

        it('all nodes include a "displayText" property', function () {

            // Recursive function to iterate through an object and make sure
            // that each node has a "displayText" function
            var allNodesHaveDisplayText = function (objToValidate) {
                var i, key;

                if (Array.isArray(objToValidate)) {
                    for (i=0; i<objToValidate.length; i++) {
                        allNodesHaveDisplayText(objToValidate[i]);
                    }
                
                } else {

                    // Must have displayText
                    expect(objToValidate).to.have.property('displayText');

                    // Iterate into any array properties
                    for (key in objToValidate) {
                        if (objToValidate.hasOwnProperty(key)) {

                            if (key === 'subItems' && Array.isArray(objToValidate[key])) {
                                allNodesHaveDisplayText(objToValidate[key]);
                            }
                        }
                    }
                }
            };

            allNodesHaveDisplayText(wcmStructure);
        });

    });

});

/* global describe, it, $, expect */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

// TODO: SLOW TESTS - Why do these tests run so much slower than the Intern tests? (5s vs 70s)
describe('Policy Detail Page - Application Status Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var getViewWithData;
    var rootView;
    var view;
    var policyDataCopy;

    // returns a view using the data passed in as a parameter
    getViewWithData = function(policyData) {
        var newView;

        ajaxStub.yieldsTo(
            'success',
            policyData
        );

        newView = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newView);

        return newView;
    };

    // This is run once before any of the tests within this test suite
    before(function () {
        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
    });

    beforeEach(function () {
        policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

        ajaxStub = this.sinon.stub($, 'ajax');
    });

    afterEach(function () {
        ajaxStub.restore();
        view.destroy();
    });

    it('"Application" data should match', function() {
        view = getViewWithData(policyDataCopy);
        var appStatusDateText = view.$el.find('.application-status ul li:eq(0) .status-date')
            .text().trim();

        expect(appStatusDateText).to.contain('Signed: 02/10/2016');
        expect(appStatusDateText).to.contain('Received: 02/02/2016');
        expect(appStatusDateText).to.contain('Entered: 02/05/2016');
    });

    describe('Application Status Tracker icons', function () {
        var statusIconContainer;
        var getStatusIconContainer;
        var getListItem;
        var statusDateText;

        // return the jQuery object for the status-icon-container class in the view
        getStatusIconContainer = function(viewInstance) {
            return viewInstance.$el.find('.application-status .status-icon-container');
        };

        // return the jQuery object for <li> tag in div.application-status
        // specified by the liElement argument
        getListItem = function(viewInstance, liElement) {
            return viewInstance.$el.find('.application-status ul li:eq(' + liElement + ')');
        };

        describe('Policy Status Icon', function () {
        
            describe('should display as a "Yellow Pencil" icon', function () {
        
                it('with label "Pending" when status is "Pending" and description is ' +
                    'NOT "Issued Not Paid"', function () {

                    policyDataCopy.policyStatus.status = 'Pending';
                    policyDataCopy.policyStatus.description = 'Application Received';

                    view = getViewWithData(policyDataCopy);

                    statusIconContainer = getStatusIconContainer(view);

                    expect(statusIconContainer.hasClass('label-yellow')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-pencil')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Pending');
                });
        
            }); // Yellow Pencil "Pending"

            describe('policyStatusDate', function (){

                var formatDate = function (date) {
                    if (date) {
                        date = new Date(date);
                        return (date.getMonth() + 1) + '/'+ 
                            date.getDate() + '/' +  date.getFullYear();
                    }
                };

                it('display when status is "Withdrawn" ', function (){
                    policyDataCopy.policyStatus.status = 'Withdrawn';

                    view = getViewWithData(policyDataCopy);       
                    statusIconContainer = getStatusIconContainer(view);         
                    expect(statusIconContainer.find('h3 span').text())
                        .to.be.contain(formatDate(policyDataCopy.policyStatus.date));
                });

                it('display when status is "Incomplete" ', function (){
                    policyDataCopy.policyStatus.status = 'Incomplete';
                    view = getViewWithData(policyDataCopy);       
                    statusIconContainer = getStatusIconContainer(view);         
                    expect(statusIconContainer.find('h3 span').text())
                        .to.be.contain(formatDate(policyDataCopy.policyStatus.date));
                });

                it('display when workflow status is "In Process" ', function (){
                    policyDataCopy.policyStatus.description = 'In Process';
                    view = getViewWithData(policyDataCopy);       
                    statusIconContainer = getStatusIconContainer(view);         
                    expect(statusIconContainer.find('h3 span').text())
                        .to.be.contain(formatDate(policyDataCopy.policyStatus.date));
                });

                it('should display when workflow unless status is "withdrawn" '+
                    ' or "Incomplete"  or "In Process" ', function (){
                    policyDataCopy.policyStatus.status = 'Declined';
                    view = getViewWithData(policyDataCopy);       
                    statusIconContainer = getStatusIconContainer(view);         
                    expect(statusIconContainer.find('h3 span').text())
                    .to.equal('');
                });
            });
        
            describe('should display as a "Green Checkmark" icon with "Paid" ' +
                'label', function () {
        
                it('when status and description are both "Paid"', function () {
                    policyDataCopy.policyStatus.description = 'Paid';
                    policyDataCopy.policyStatus.status      = 'Paid';
         
                    view = getViewWithData(policyDataCopy);
         
                    statusIconContainer = getStatusIconContainer(view);
         
                    expect(statusIconContainer.hasClass('label-success')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Paid');
                });
 
                it('description is "Paid" and status is "Issued". Text should '+
                    'return as "Paid"', function () {
                    policyDataCopy.policyStatus.status = 'Issued';
                    policyDataCopy.policyStatus.description = 'Paid';
    
                    view = getViewWithData(policyDataCopy);
    
                    statusIconContainer = getStatusIconContainer(view);
    
                    expect(statusIconContainer.hasClass('label-success')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Paid');
                });

                it('description is "Active", status is "Issued" and '+
                    'acordHoldingStatus is "Active". Text should '+
                    'return as "Paid" (workflowStatus)', function () {
                    policyDataCopy.policyStatus.status = 'Issued';
                    policyDataCopy.policyStatus.description = 'Active';
                    policyDataCopy.policyStatus.acordHoldingStatus = 'Active';
                    policyDataCopy.policyStatus.workflowStatus = 'Paid';
                    policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 50);
    
                    view = getViewWithData(policyDataCopy);
    
                    statusIconContainer = getStatusIconContainer(view);
                    expect(statusIconContainer
                        .parents('.application-status')
                        .hasClass('application-status-active')).to.be.true;
                    expect(statusIconContainer.hasClass('label-success')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Paid');
                });

                it('description is "Paid", status is "Issued" and '+
                    'acordHoldingStatus is "Proposed". Text should '+
                    'return as "Paid"', function () {
                    policyDataCopy.policyStatus.status = 'Issued';
                    policyDataCopy.policyStatus.description = 'Paid';
                    policyDataCopy.policyStatus.acordHoldingStatus = 'Proposed';
    
                    view = getViewWithData(policyDataCopy);
    
                    statusIconContainer = getStatusIconContainer(view);
    
                    expect(statusIconContainer.hasClass('label-success')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Paid');
                });
        
            }); // Green Checkmark "Paid"
        
            describe('should display as a "Grey Warning" icon', function () {

                it('description is "Declined"', function () {
                        
                    policyDataCopy.policyStatus.status = 'Declined';
                    policyDataCopy.policyStatus.description = 'Declined';
    
                    view = getViewWithData(policyDataCopy);
    
                    statusIconContainer = getStatusIconContainer(view);
    
                    expect(statusIconContainer.hasClass('label-grey')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-warning')).to.be.true;
                    expect(statusIconContainer.find('h3').text())
                            .to.be.contain('Declined');
                });
        
                it('with label "Postponed" when description is "Postponed"', function () {
                    policyDataCopy.policyStatus.status = 'Postponed';
                    policyDataCopy.policyStatus.description = 'Postponed';
        
                    view = getViewWithData(policyDataCopy);
        
                    statusIconContainer = getStatusIconContainer(view);
        
                    expect(statusIconContainer.hasClass('label-grey')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-warning')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Postponed');
                });
        
                it('with label "Incomplete" when status is "Incomplete"', function () {
                    policyDataCopy.policyStatus.status = 'Incomplete';
                    policyDataCopy.policyStatus.description = 'Incomplete';
                    view = getViewWithData(policyDataCopy);
        
                    statusIconContainer = getStatusIconContainer(view);
        
                    expect(statusIconContainer.hasClass('label-grey')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-warning')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Incomplete');
                });
        
                it('with label "Withdrawn" when status is "Withdrawn"', function () {
                    policyDataCopy.policyStatus.status = 'Withdrawn';
                    policyDataCopy.policyStatus.description = 'Withdrawn';
        
                    view = getViewWithData(policyDataCopy);
        
                    statusIconContainer = getStatusIconContainer(view);
        
                    expect(statusIconContainer.hasClass('label-grey')).to.be.true;
                    expect(statusIconContainer.find('i').hasClass('fa-warning')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Withdrawn');
                });
        
                it('with label "Terminated" when status is "Terminated" and ' +
                    'description is any value OTHER THAN "Death Claim", "Maturity", or ' +
                    '"Converted"', function () {
        
                    policyDataCopy.policyStatus.description = 'Terminated';
                    policyDataCopy.policyStatus.status      = 'Terminated';
        
                    view = getViewWithData(policyDataCopy);
        
                    statusIconContainer = getStatusIconContainer(view);
        
                    expect(statusIconContainer.hasClass('label-grey')).to.be.true;
                    expect(statusIconContainer.find('h3').text()).to.be.contain('Terminated');
                });

                
        
            }); // Grey Warning icon
        
        }); // Policy Status icon
        
        describe('"Application" icon', function () {
            var appIcon;
            var regexStatusDate;
        
            beforeEach(function () {
                // used to verify the expected result in div.status-date
                regexStatusDate = /\s*(Signed|Received|Entered):\s?\d{2}\/\d{2}\/\d{4}\s*/g;
            });
        
            // clean up after the tests
            afterEach(function () {
                appIcon         = null;
                regexStatusDate = null;
                statusDateText  = null;
            });
        
            describe('should display as a "Green Checkmark" icon with "Signed", "Received", and '
                + '"Entered" dates', function () {
        
                it('when status is "Pending"', function () {
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    expect(appIcon.hasClass('success')).to.be.true;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatusDate to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
            }); // "Green Checkmark"
        
            describe('should display as a "Grey Checkmark" icon with "Signed", ' +
               '"Received", and "Entered" dates', function () {
        
                it('when status is "Terminated" and description is "Paid"', function () {
        
                    policyDataCopy.policyStatus.description = 'Paid';
                    policyDataCopy.policyStatus.status      = 'Terminated';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Declined"', function () {
        
                    policyDataCopy.policyStatus.description = 'Declined';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Incomplete"', function () {
        
                    policyDataCopy.policyStatus.description = 'Incomplete';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Issued"', function () {
        
                    policyDataCopy.policyStatus.description = 'Issued';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Postponed"', function () {
        
                    policyDataCopy.policyStatus.description = 'Postponed';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Terminated"', function () {
        
                    policyDataCopy.policyStatus.description = 'Terminated';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
                it('when status is "Withdrawn"', function () {
        
                    policyDataCopy.policyStatus.description = 'Withdrawn';
        
                    view = getViewWithData(policyDataCopy);
        
                    appIcon        = getListItem(view, 0);
                    statusDateText = appIcon.find('.status-date').text();
        
                    // there should be no class specified on the <li> tag
                    expect(appIcon.attr('class')).to.be.undefined;
                    expect(appIcon.find('i').hasClass('fa-check')).to.be.true;
        
                    // use regexStatus Date to verify text
                    expect(statusDateText).to.match(regexStatusDate);
                });
        
            }); // "Grey Checkmark"

        }); // "Application" icon
        
        describe('"U/W Decision" icon', function () {
            var uwIcon;
            var uwDecisionElement;
            beforeEach(function () {
                // add a uwDecision date to the data
                policyDataCopy.application.uwDecision = '2016-08-31T15:00:00Z';
            });
        
            describe('when underwrtingRequired is FALSE', function () {
                var iconsUlContainer;
        
                beforeEach(function () {
                    policyDataCopy.product.underwritingRequired = false;
        
                    view = getViewWithData(policyDataCopy);
        
                    iconsUlContainer = view.$el.find('.application-status ul');
                });
       
                it('should NOT be displayed', function () {
                    expect(iconsUlContainer.find('li').length).to.equal(4);
                });
       
                it('<UL> tag should have appropriate CSS class applied', function () {
                    expect(iconsUlContainer.hasClass('four-col')).to.be.true;
                });
        
            }); // underwritingRequired false
        
            describe('should display a "Green Checkmark" icon with the uwDecision ' +
                'date displayed', function () {
        
                it('when status is "Pending"', function () {

                    policyDataCopy.policyStatus.description = 'Ready for Issue';
        
                    view = getViewWithData(policyDataCopy);
        
                    uwIcon         = getListItem(view, 1);
                    statusDateText = uwIcon.find('.status-date').text();
        
                    expect(uwIcon.hasClass('success')).to.be.true;
                    expect(uwIcon.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusDateText).to.have.string('08/31/2016');
                });
        
            });
        
            describe('should display a "Grey Checkmark" icon with the uwDecision ' +
                'date displayed', function () {
        
                it('when status is NOT "Pending" or "Declined"', function () {
                    policyDataCopy.policyStatus.description = 'Issued';
        
                    view = getViewWithData(policyDataCopy);
        
                    uwIcon         = getListItem(view, 1);
                    statusDateText = uwIcon.find('.status-date').text();
        
                    expect(uwIcon.attr('class')).to.be.undefined;
                    expect(uwIcon.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusDateText).to.have.string('08/31/2016');
                });
            });
        
        
            describe('should display a "Grey Warning" icon with the uwDecision ' +
                'date displayed', function () {
        
                it('when status is "Declined" and uwDecision data is null', function () {
                    policyDataCopy.policyStatus.status = 'Declined';
        
                    view = getViewWithData(policyDataCopy);
        
                    uwIcon         = getListItem(view, 1);
                    statusDateText = uwIcon.find('.status-date').text();

                    expect(uwIcon.find('i').hasClass('fa-warning')).to.be.true;
                    expect(statusDateText).to.be.contain('02/21/2016');
                });
            });

            describe('Accelerated u/w status message', function (){
                it('should displayed even there is no uwDecision date', function () {
                    policyDataCopy.policyStatus.status      = 'Withdrawn';
                    policyDataCopy.application.uwDecision   = null;
                    policyDataCopy.application.acceleratedUnderwriting = 'Moved To Full Underwriting';

                    view = getViewWithData(policyDataCopy);
                    uwDecisionElement = view.$el.find('#uw-decision');
                    expect(uwDecisionElement.find('#auw-status').length).to.equal(1);
                    expect(uwDecisionElement.find('#auw-status').text()).to.contain('Accelerated U/W');
                });

                it('should not displayed if there is no auw status message', function () {
                    policyDataCopy.application.acceleratedUnderwriting = null;
                    view = getViewWithData(policyDataCopy);
                    uwDecisionElement = view.$el.find('#uw-decision');
                    expect(uwDecisionElement.find('#auw-status').length).to.equal(0);
                });
            });

            it('should have "border-top-0" class for UW detail placer '+
                    'if there is no data to display', function () {
                policyDataCopy.policyStatus.status      = 'Terminated';
                policyDataCopy.policyStatus.description = 'Terminated';
                policyDataCopy.policyStatus.acordHoldingStatus  = 'Inactive';
                policyDataCopy.application.uwDecision   = null;
                policyDataCopy.application.acceleratedUnderwriting = null;
                view = getViewWithData(policyDataCopy);
                uwDecisionElement = view.$el.find('#uw-decision');
                expect(uwDecisionElement.find('#jump-links').hasClass('border-top-0')).to.be.true;
            });
        
        }); // "U/W Decision" icon
        
        describe('"Ready for Issue" Icon', function () {
            var readyToIssueIcon;
        
            beforeEach(function () {
                // add a readyToIssue date to the data
                policyDataCopy.application.readyToIssue = '2016-08-31T15:00:00Z';
            });
        
            describe('should only display "Ready for Issue" header', function () {
        
                it('when readyToIssue date is falsey', function () {
        
                    delete policyDataCopy.application.readyToIssue;
                    view = getViewWithData(policyDataCopy);
         
                    readyToIssueIcon = getListItem(view, 2);
         
                    expect(readyToIssueIcon.text().trim()).to.equal('Ready for Issue');
                });
        
                it('when status is "Declined"', function () {
                    policyDataCopy.policyStatus.status = 'Declined';
                    view = getViewWithData(policyDataCopy);
         
                    readyToIssueIcon = getListItem(view, 2);
         
                    expect(readyToIssueIcon.text().trim()).to.be.contain('Ready for Issue');
                });
            });
        
            describe('should display a "Green Checkmark" icon with readyToIssue date',
                function () {
        
                it('when status is "Pending"', function () {
                    view = getViewWithData(policyDataCopy);
        
                    readyToIssueIcon = getListItem(view, 2);
                    statusDateText   = readyToIssueIcon.find('.status-date').text().trim();
         
                    expect(readyToIssueIcon.hasClass('success')).to.be.true;
                    expect(readyToIssueIcon.find('i').hasClass('fa-check')).to.be.true;
                    expect(statusDateText).to.equal('08/31/2016');
                });
        
            }); // Green Checkmark
        
            describe('should display a "Grey Checkmark" icon', function () {
        
                it('when status is NOT "Declined" or "Pending"', function () {
                    policyDataCopy.policyStatus.status = 'Terminated';
                    view = getViewWithData(policyDataCopy);
         
                    readyToIssueIcon = getListItem(view, 2);
                    statusDateText   = readyToIssueIcon.find('.status-date').text().trim();
                });
        
            });
        
        }); // "Ready for Issue" icon

    }); // Application Status Tracker icons

    describe('Underwriter name', function () {

        it('should appear if product.underwritingRequired is TRUE ' +
            'and application.underWriter exists', function () {
            var expectedUw = 'JOE UNDERWRITER';
            var uwField;

            policyDataCopy.product.underwritingRequired = true;
            policyDataCopy.application.underWriter      = expectedUw;
            view = getViewWithData(policyDataCopy);

            uwField = view.$el.find('h2:contains("Application Status")').next('div')
                .find('.list-horizontal li:eq(0)').text();

            expect(uwField.split(':')[1].trim()).to.equal(expectedUw);
        });
    }); // Underwriter name

    describe('Jump links from U/W Decision section', function () {
        var link;

        it('label and title should match if policy status is not "Declined" or "Postponed"',
            function (){
            // using Asset Care data as test to allow "See Decision" link to display
            var assetCareCopy = $.extend(true,{},
                helpers.policyData.pendingPolicyDetailAssetCare);

            assetCareCopy.policyStatus.status      = 'Pending';
            assetCareCopy.policyStatus.description = 'Pending';

            view = getViewWithData(assetCareCopy);
            link = view.$el.find('#jump-links.status-date a');

            expect(link.text()).to.equal('See Decision');
            expect(link.attr('title')).to.equal('See policy\'s rating information');
        });

        it('label and title should match if policy status is "Declined"', function () {
            policyDataCopy.policyStatus.status = 'Declined';

            view = getViewWithData(policyDataCopy);

            link = view.$el.find('#jump-links.status-date a');

            expect(link.text().toUpperCase()).to.equal('DECLINED');
            expect(link.attr('title')).to.equal('See Requirements for more information');
        });

        it('label and title should match if policy status is "Postponed"', function () {
            policyDataCopy.policyStatus.status    = 'Postponed';

            view = getViewWithData(policyDataCopy);

            link = view.$el.find('#jump-links.status-date a');

            expect(link.text().toUpperCase()).to.equal('POSTPONED');
            expect(link.attr('title')).to.equal('See Requirements for more information');
        });
    }); // Jump links from U/W Decision section

    describe('Active Policy - Application status and icons visibility', function () {
        it('Not active and paid date not exists', function () {
            view = getViewWithData(policyDataCopy);
            expect(view.$el.find('h2:contains(Application Status)')).to.be.lengthOf(1);
        });

        it('Active and paid date is after Active Period', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            policyDataCopy.application.paid = '2016-05-19T21:14:00Z';
            view = getViewWithData(policyDataCopy);
            expect(view.$el.find('h2:contains(Application Status)')).to.be.lengthOf(0);
        });

        it('Active and paid date is within Active Period', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);

            // get date which is 60 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 60);
            view = getViewWithData(policyDataCopy);
            expect(view.$el.find('h2:contains(Application Status)')).to.be.lengthOf(1);
            expect(view.$el.find('div.application-status-active')).to.be.lengthOf(1);
            
        });

    }); // Active Policy - Application status and icons visibility

    describe('"Available links" section', function () {
        it('should display eApp and illustration links', function () {
            view = getViewWithData(policyDataCopy);
            expect(view.$el.find('#eapp-document-link')).to.be.lengthOf(1);
            // Link needs to start with the api root url, and remove the '../../../' at the
            // start of the _links href.
            expect(view.$el.find('#eapp-document-link').attr('href'))
                .to.equal('http://localhost:3000/api/oso/secure' +
                    policyDataCopy._links['application-document'].href.slice(8));

            expect(view.$el.find('#illustration-document-link')).to.be.lengthOf(1);
            expect(view.$el.find('#illustration-document-link').attr('href'))
                .to.equal('http://localhost:3000/api/oso/secure' +
                    policyDataCopy._links['illustration-document'].href.slice(8));
        });
        it('section should not be displayed if both links are not exists', function () {
            policyDataCopy._links = {};
            view = getViewWithData(policyDataCopy);
            expect(view.$el.find('#files-available')).to.be.lengthOf(0);
        });
    });

});

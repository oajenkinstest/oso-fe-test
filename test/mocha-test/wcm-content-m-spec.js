/* global describe, $, expect:false, sinon:false, Backbone:false */

var config   = require('../dist/config/config');
var WcmModel = require('../dist/modules/wcm-content/models/wcm-content-m');

describe('WCM Content Model (modules/wcm-content/models/wcm-content-m.js)', function () {

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
    });

    it('WcmModel exists', function() {
        expect(WcmModel).to.exist;
    });

    it('A WcmModel can be instantiated', function() {
        var fn = function() {
            var model = new WcmModel(); // eslint-disable-line no-unused-vars
        };

        expect(fn).to.not.throw(Error);
    });

    it('Instantiating a WcmModel creates default attributes', function() {
        var model = new WcmModel();

        // these attributes are listed in 'defaults', so should exist
        expect(model.attributes.hasOwnProperty('body')).to.be.true;
        expect(model.attributes.hasOwnProperty('wcmPath')).to.be.true;

        // Let's throw in an obviously fake one to keep us honest.
        expect(model.attributes.hasOwnProperty('monkey')).to.be.false;

        model = null;
    });

    it('Default attributes have the correct values', function() {
        var model = new WcmModel();

        // values should all be null.
        expect(model.get('body')).to.equal(null);
        expect(model.get('wcmPath')).to.equal(null);

        model = null;
    });

    it('Model builds the URL correctly', function() {
        var testWcmPath = 'OnlineServices/Marketing/ELEVATE';
        var expectedUrl = '/wps/wcm/connect/indcontent/OSO/' + testWcmPath;

        var model = new WcmModel({
            wcmPath: testWcmPath
        });

        expect(model.url()).to.equal(expectedUrl);

        model = null;
    });


    /*
     Test the parse function! It's complex...
     */
    describe('parse method', function() {

        it('Parse returns standard attributes on null input', function() {
            var model = new WcmModel();
            var attributes = model.parse(null);

            expect(attributes.hasOwnProperty('body')).to.be.true;
            expect(attributes.hasOwnProperty('wcmPath')).to.be.true;

            expect(attributes.body).to.equal('');
            expect(attributes.wcmPath).to.equal(null);

            model = null;
        });

        it('Parse\'s return value will contain the model\'s existing wcmPath on null input',
            function() {
                var testWcmPath = 'OnlineServices/Marketing/ELEVATE';
                var model  = new WcmModel({
                    wcmPath: testWcmPath
                });

                var attributes = model.parse(null);

                expect(attributes.wcmPath).to.equal(testWcmPath);

                model = null;
            }
        );

        it('Parse handles very simple input', function() {
            var testWcmPath = 'OnlineServices/Marketing/ELEVATE';
            var testBody =
                '<h2>Digital Marketing Suite</h2>' +
                '<img src="/wps/wcm/connect/764e7e804c8062758d71ed910725b403/ELV_216w.png?MOD=AJPERES"/>' +
                '<p>Content</p>';

            var ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                testBody
            );

            var model    = new WcmModel({
                wcmPath: testWcmPath
            });

            model.fetch();

            expect(model.get('body')).to.equal(testBody);
            expect(model.get('wcmPath')).to.equal(testWcmPath);

            model = null;
            ajaxStub.restore();
        });
       
    });

    describe('WCM URL used in request to WCM server', function () {

        it('should contain the wcmPath model attribute', function () {
            var wcmPath  = 'this/is/a/path';
            var model    = new WcmModel({
                wcmPath : wcmPath
            });

            expect(model.url()).to.contain(wcmPath);
        });

        it('should contain the "wcmRole" query param if one is returned from _getWcmRole', function () {
            var wcmRole = 'IB_GUY';
            var getWcmRoleStub = this.sinon.stub(WcmModel.prototype, '_getWcmRole')
                .returns(wcmRole);
            var wcmPath = 'another/path';
            var model   = new WcmModel({
                wcmPath : wcmPath
            });

            expect(model.url()).to.contain('wcmRole=' + wcmRole);

            getWcmRoleStub.restore();
        });
    }); // WCM URL used in request to WCM server

    describe('_getWcmRole method', function () {
        var userChannel;
        var model;

        before(function () {
            model = new WcmModel();
            userChannel = Backbone.Radio.channel('user');
        });

        after(function () {
            model = null;
        });

        it('should return "Home_Office" if user has "Home_Office" capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'Home_Office';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Home_Office';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "IB_Manager" if user has "Pathway_IB_Manager_View" ' +
            'capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'IB_Manager';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pathway_IB_Manager_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "IB_Producer" if user has "Pathway_IB_Producer_View" ' +
            'capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'IB_Producer';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pathway_IB_Producer_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "Retail_Manager" if user has "Pathway_Retail_Manager_View" ' +
            'capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'Retail_Manager';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pathway_Retail_Manager_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "Retail_Producer" if user has "Pathway_Retail_Producer_View" ' +
            'capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'Retail_Producer';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pathway_Retail_Producer_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "CS_Producer" if user has ALL of the following capabilities: ' +
            '"Contact_List_CS_View", "Producer_Role_View", "WCM_All_But_Bank_View"', function () {
            var result;
            var expected = config.wcmRolePrefix + 'CS_Producer';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'WCM_All_But_Bank_View' ||
                    capability === 'Contact_List_CS_View' ||
                    capability === 'Producer_Role_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "CS_Manager" if user has ALL of the following capabilities: ' +
            '"Contact_List_CS_View", "Producer_Manager", "WCM_All_But_Bank_View"', function () {
            var result;
            var expected = config.wcmRolePrefix + 'CS_Manager';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Contact_List_CS_View' ||
                    capability === 'Producer_Manager' ||
                    capability === 'WCM_All_But_Bank_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "CS_Bank_Manager" if user has "Contact_List_CS_View" and ' +
            '"Producer_Manager" capabilities, but not the "WCM_All_But_Bank_View" capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'CS_Bank_Manager';

            userChannel.reply('hasCapability', function(capability) {
                return (capability === 'Contact_List_CS_View' ||
                    capability === 'Producer_Manager') &&
                    capability !== 'WCM_All_But_Bank_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "CS_Bank_Producer" if user has "Contact_List_CS_View" and ' +
            '"Producer_Role_View" capabilities, but not the "WCM_All_But_Bank_View" ' +
            'capability', function () {
            var result;
            var expected = config.wcmRolePrefix + 'CS_Bank_Producer';

            userChannel.reply('hasCapability', function(capability) {
                return (capability === 'Contact_List_CS_View' ||
                    capability === 'Producer_Role_View') &&
                    capability !== 'WCM_All_But_Bank_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return "IB_Producer" if user has both "Contact_List_IB_View" and ' +
            '"Producer_Role_View" capabilities.', function () {
            var result;
            var expected = config.wcmRolePrefix + 'IB_Producer';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Contact_List_IB_View' ||
                    capability === 'Producer_Role_View';
            });

            result = model._getWcmRole();

            expect(result).to.equal(expected);
        });

        it('should return NULL if none of the above capabilities exist for the user', function () {
            var result;

            userChannel.reply('hasCapability', function(capability) {
                return capability !== 'Home_Office' &&
                    capability !== 'Pathway_IB_Manager_View' &&
                    capability !== 'Pathway_IB_Producer_View' &&
                    capability !== 'Pathway_Retail_Manager_View' &&
                    capability !== 'Pathway_Retail_Producer_View' &&
                    capability !== 'Contact_List_CS_View' &&
                    capability !== 'Contact_List_IB_View';
            });

            result = model._getWcmRole();

            expect(result).to.be.null;
        });

    });

    describe('_addParams method', function () {
        var ajaxStub;
        var getWcmRoleStub;
        var model;
        var testBody;
        var testWcmPath;

        before(function () {
            if (!this.sinon) {
                this.sinon = sinon;
            }
            testWcmPath = 'OnlineServices/Marketing/ELEVATE?bugs_bunny=17&elmer_fudd=0';
            testBody =
                '<h2>Digital Marketing Suite</h2>' +
                '<img src="/wps/wcm/connect/764e7e804c8062758d71ed910725b403/ELV_216w' +
                '.png?MOD=AJPERES"/>' +
                '<p>Content</p>';

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                testBody
            );

            getWcmRoleStub = this.sinon.stub(WcmModel.prototype, '_getWcmRole');

            model = new WcmModel({
                wcmPath : testWcmPath
            });
        });

        after(function () {
            ajaxStub.restore();
            model = null;
        });

        it('should throw an error if a parameter is not passed', function () {
            var fn = function () {
                model._addParams();
            };

            expect(fn).to.throw(model.errors.addParamsNoString);
        });

        it('should throw an error if a string is not passed', function () {
            var fn = function () {
                model._addParams(3.14);
            };

            expect(fn).to.throw(model.errors.addParamsNoString);
        });

        it('should return parameter passed in if _getWcmRole() returns null', function () {
            var result;

            getWcmRoleStub.returns(null);
            result = model._addParams(model.get('wcmPath'));

            expect(result).to.equal(model.get('wcmPath'));
        });

        it('should add wcmRole param to existing query string', function () {
            var result;

            getWcmRoleStub.returns('Home_Fry');
            result = model._addParams(model.get('wcmPath'));

            expect(result).to.contain(model.get('wcmPath') + '&wcmRole=');
        });
    });
});

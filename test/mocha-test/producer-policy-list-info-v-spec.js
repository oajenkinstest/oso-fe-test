/* global expect:false, Backbone:false, $:false */


var helpers = require('./helpers/helpers');
var View = require('../dist/pages/policy/views/producer-policy-list-info-v');

describe('Producer Policy List Info View ' +
    '(pages/policy/views/producer-policy-list-info-v.js)', function () {

    var ajaxStub;
    var rootView;
    var view;

    it('should exist', function() {
        expect(View).to.exist;
    });

    describe('view other org tests', function() {
        var data = [
            {
                producers : [
                    {
                        producer : {
                            id          : 456,
                            fullName    : 'Darth Vader',
                            lexicalName : 'Vader, Darth'
                        }
                    }, {
                        producer : {
                            id          : 789,
                            fullName    : 'Luke Skywalker',
                            lexicalName : 'Skywalker, Luke'
                        }
                    }
                ]
            }, {
                producers : [
                    {
                        producer : {
                            id          : 101,
                            fullName    : 'James T Kirk',
                            lexicalName : 'Kirk, James T'
                        }
                    }, {
                        producer : {
                            id          : 909,
                            fullName    : 'Spock',
                            lexicalName : 'Spock'
                        }
                    }
                ]
            }
        ];

        var model = new Backbone.Model({
            id       : 123,
            fullName : 'Jimbo Jones'
        });
        var hierarchyChangeStub;
        var listener;
        var userChannel;

        beforeEach(function() {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function(capability) {
                return capability !== 'Home_Office';
            });
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo('success', data);
            view = new View({
                model : model
            });
            listener = new Backbone.Marionette.Object();
            hierarchyChangeStub = this.sinon.stub();
            listener.listenTo(view, 'hierarchyChange', hierarchyChangeStub);
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);
        });

        afterEach(function() {
            ajaxStub.restore();
            view.destroy();
            listener.destroy();
            userChannel.stopReplying();
            userChannel = null;
        });

        it('Renders the hierarchy paths', function() {
            var paths = view.$el.find('ul.list-inline');
            expect(paths).to.have.length(2);
        });

    });

    describe('if user has "Home_Office" capability', function () {
        var data = [
            {
                producers : [
                    {
                        producer : {
                            id          : 456,
                            fullName    : 'Darth Vader',
                            lexicalName : 'Vader, Darth'
                        }
                    }, {
                        producer : {
                            id          : 789,
                            fullName    : 'Luke Skywalker',
                            lexicalName : 'Skywalker, Luke'
                        }
                    }
                ]
            }, {
                producers : [
                    {
                        producer : {
                            id          : 101,
                            fullName    : 'James T Kirk',
                            lexicalName : 'Kirk, James T'
                        }
                    }, {
                        producer : {
                            id          : 909,
                            fullName    : 'Spock',
                            lexicalName : 'Spock'
                        }
                    }
                ]
            }
        ];

        var model = new Backbone.Model({
            id       : 123,
            fullName : 'Jimbo Jones'
        });
        var hierarchyChangeStub;
        var listener;
        var userChannel;

        beforeEach(function() {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function(capability) {
                return true;
            });
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo('success', data);
            view = new View({
                model : model
            });
            listener = new Backbone.Marionette.Object();
            hierarchyChangeStub = this.sinon.stub();
            listener.listenTo(view, 'hierarchyChange', hierarchyChangeStub);
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);
        });

        afterEach(function() {
            ajaxStub.restore();
            view.destroy();
            listener.destroy();
            userChannel.stopReplying();
            userChannel = null;
        });

        it('no call is made to service to retrieve "paths"', function () {
            expect(ajaxStub).not.to.have.been.called;
        });
    });

    describe('view self org tests', function() {
        var data = [
            {
                producers : [
                    {
                        producer : {
                            id          : 123,
                            fullName    : 'Jimbo Jones',
                            lexicalName : 'Jones, Jimbo'
                        }
                    }
                ]
            }
        ];

        var model = new Backbone.Model({
            id       : 123,
            fullName : 'Jimbo Jones'
        });
        var userChannel;

        beforeEach(function() {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function() {
                return true;
            });
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo('success', data);
            view = new View({
                model : model
            });
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

        });

        afterEach(function() {
            ajaxStub.restore();
            view.destroy();
            userChannel.stopReplying();
            userChannel = null;
        });

        it('Does not render hierarchy', function() {
            var paths = view.$el.find('ul.list-inline');
            expect(paths).to.have.length(0);
        });
    });
});

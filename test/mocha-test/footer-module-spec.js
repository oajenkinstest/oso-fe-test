/* global expect:false, Backbone:false */

var FooterModule = require('../dist/modules/footer/index');
var FooterView   = require('../dist/modules/footer/views/footer-v');

describe('Footer Module (modules/footer/index.js)', function() {

    it('exists', function() {
        expect(FooterModule).to.exist;
    });

    describe('initialize method', function () {

        it('throws an error if instantiated without options', function() {
            var fn = function() {
                var footer = new FooterModule();
                footer.destroy();
            };
            expect(fn).to.throw(FooterModule.prototype.errors.parentRegion);
        });

        it('throws an error if instantiated without a region', function() {
            var fn = function() {
                var footer = new FooterModule({});
                footer.destroy();
            };
            expect(fn).to.throw(FooterModule.prototype.errors.parentRegion);
        });

        it('is instantiated correctly when given a region', function() {
            var fn = function() {
                var testRegion = new Backbone.Marionette.Region({
                    el : document.createElement('div')
                });
                var footer = new FooterModule({
                    region : testRegion
                });
                testRegion.reset();
                footer.destroy();
            };
            expect(fn).to.not.throw(Error);
        });
    }); // initialize method

    describe('Rendered footer', function() {

        var footer;
        var testRegion;

        beforeEach(function() {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });

            footer = new FooterModule({
                region : testRegion
            });
        });

        afterEach(function() {
            testRegion.reset();
            testRegion = null;
            footer.destroy();
            footer = null;
        });

        it('shows current year in copyright', function() {
            var currentYear  = String((new Date()).getFullYear());
            var renderedYear = footer.footerView.$('#footer-copyright-year').html();

            expect(renderedYear).to.equal(currentYear);
        });

        // Make sure links target "_blank", which indicates a new window that won't be reused.
        // Initially had "blank" which is a child window with the ID "blank" and gets reused.
        it('links have target="_blank"', function() {
            var legalLinkTarget   = footer.footerView.$('#footer-legal-link').attr('target');
            var privacyLinkTarget = footer.footerView.$('#footer-privacy-link').attr('target');

            expect(legalLinkTarget).to.equal('_blank');
            expect(privacyLinkTarget).to.equal('_blank');
        });
    });
});

describe('Footer View (modules/footer/views/footer-v.js)', function() {

    // footer-v is dumb, it renders the year property it's given.
    it('renders the year it is given', function() {
        var element = Backbone.$('body');

        var footerModel = new Backbone.Model({
            year: 1066
        });

        var footerView = new FooterView({
            el: element,
            model: footerModel
        });

        footerView.render();

        var renderedYear = element.find('#footer-copyright-year').html();

        // $.html() always returns a string, so quoting the number used above
        expect(renderedYear).to.equal('1066');
    });
});


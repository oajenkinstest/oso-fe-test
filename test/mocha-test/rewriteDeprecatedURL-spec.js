/* global expect */

var rewriteDeprecatedURL = require('../dist/utils/rewriteDeprecatedURL');

describe ('rewrite Deprecated URLs (rewriteDeprecatedURL.js)', function() {
    it ('deprecatedURLMap object should exist', function () {
        expect(rewriteDeprecatedURL.deprecatedURLMap).to.exist.and.be.a('object');
    });

    it ('rewrite method should exist', function () {
        expect(rewriteDeprecatedURL.rewrite).to.exist.and.be.a('function');
    });

    it ('should return new hash URL if old URL match with deprecatedURLMap ', function () {
        location.hash = '#pending-policy-detail?policyId=1000101';
        rewriteDeprecatedURL.rewrite();
        expect(location.hash).to.be.equal('#policy-detail?policyId=1000101');
    });

    it ('should not make any changes to hash if no match with deprecatedURLMap ', function () {
        location.hash = '#producer-policy-list?producerId=1000101';
        rewriteDeprecatedURL.rewrite();
        expect(location.hash).to.be.equal('#producer-policy-list?producerId=1000101');
    });

});

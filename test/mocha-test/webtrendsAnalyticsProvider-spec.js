/* global describe:false, WebTrends, expect, jsdomConsole:false */

var analytics         = require('../dist/modules/analytics/analyticsModule');
var WebTrendsProvider = require('../dist/modules/analytics/providers/webtrendsAnalyticsProvider');

describe('WebTrends Analytics Provider ' +
    '(modules/analytics/providers/webtrendsAnalyticsProvider.js)', function () {

    describe('Basic behavior', function () {

        analytics.init({
            providers: [
                new WebTrendsProvider({
                    dcsid: '123456',
                    onsitedoms: 'oneamerica.com',
                    fpcdom: '.oneamerica.com'
                })
            ],
            webId: '123456'
        });

        it('webTrend multitrack object should exist', function () {
            expect(WebTrends.multiTrack).to.exist;
        });

        it('multitrack callback method should be invoked after logging data to webTrends',
                function () {

            var webTrendsMultiTrackStub = this.sinon.stub(WebTrends, 'multiTrack')
                .yieldsTo('callback', true);
            analytics.analyticsChannel.trigger('trackView', {
                uri: 'products'
            });

            WebTrends.multiTrack({
                args: {
                    'DCS.dcsuri': 'products',
                    'WT.dl': 0,
                    'WT.ti': 'OneSourceOnline'
                },
                callback: function (response) {
                    expect(response).to.equal(true);
                }
            });

            webTrendsMultiTrackStub.restore();
        });

        it('callback method should be invoked after logging action', function () {

            var webTrendsMultiTrackStub = this.sinon.stub(WebTrends, 'multiTrack')
                .yieldsTo('callback', true);

            WebTrends.multiTrack({
                args: {
                    'DCS.dcsuri': 'products',
                    'WT.dl': 0,
                    'WT.ti': 'OneSourceOnline'
                },
                callback: function (response) {
                    expect(response).to.equal(true);
                }
            });

            webTrendsMultiTrackStub.restore();
        });
    });

    describe('webtrends provider class ', function () {

        var errorSpy;
        var logSpy;
        var setUserWebTrendsSpy;
        var webTrendsProvider;
      
        beforeEach(function () {
           
            analytics.init({
                providers: [
                    new WebTrendsProvider({
                        dcsid: '123456',
                        onsitedoms: 'oneamerica.com',
                        fpcdom: '.oneamerica.com'
                    })
                ],
                webId: '123456'
            });

            logSpy              = this.sinon.spy(jsdomConsole, 'log');
            errorSpy            = this.sinon.spy(jsdomConsole, 'error');
            webTrendsProvider   = analytics.providers[0];
        });

        afterEach(function () {
            logSpy.restore();
            errorSpy.restore();
        });


        //webtrends provider methods test
        it('webTrends options value should match', function () {
            expect(webTrendsProvider.options.dcsid).to.equal('123456');
            expect(webTrendsProvider.options.onsitedoms).to.equal('oneamerica.com');
            expect(webTrendsProvider.options.fpcdom).to.equal('.oneamerica.com');
        });

        it('setUser method in provider class can be called', function () {
            setUserWebTrendsSpy = this.sinon.spy(webTrendsProvider, 'setUser');
            webTrendsProvider.setUser('123456');
            expect(setUserWebTrendsSpy).to.have.been.calledWith('123456');
            setUserWebTrendsSpy.restore();
        });

        it('WebTrends provider should have a getWebTrendsEventId function', function () {
            expect(webTrendsProvider._getWebTrendsEventId).to.exist;
            expect(webTrendsProvider._getWebTrendsEventId).to.be.a('function');
        });

        it('WebTrends IDs should translate correctly', function () {
            expect(webTrendsProvider._getWebTrendsEventId('pageView')).to.equal(0);
            expect(webTrendsProvider._getWebTrendsEventId('download')).to.equal(20);
            expect(webTrendsProvider._getWebTrendsEventId('anchor')).to.equal(21);
            expect(webTrendsProvider._getWebTrendsEventId('javascript')).to.equal(22);
            expect(webTrendsProvider._getWebTrendsEventId('mailto')).to.equal(23);
            expect(webTrendsProvider._getWebTrendsEventId('external')).to.equal(24);
            expect(webTrendsProvider._getWebTrendsEventId('rightClick')).to.equal(25);
            expect(webTrendsProvider._getWebTrendsEventId('formGet')).to.equal(26);
            expect(webTrendsProvider._getWebTrendsEventId('formPost')).to.equal(27);
            expect(webTrendsProvider._getWebTrendsEventId('formInput')).to.equal(28);
            expect(webTrendsProvider._getWebTrendsEventId('formButton')).to.equal(29);
            expect(webTrendsProvider._getWebTrendsEventId('imageMap')).to.equal(30);
            expect(webTrendsProvider._getWebTrendsEventId('youTubeImpression')).to.equal(40);
            expect(webTrendsProvider._getWebTrendsEventId('videoEvent')).to.equal(41);
            expect(webTrendsProvider._getWebTrendsEventId('onSiteAd')).to.equal(50);
            expect(webTrendsProvider._getWebTrendsEventId('mobileEvent')).to.equal(60);
            expect(webTrendsProvider._getWebTrendsEventId('mobileStateEvent')).to.equal(61);
            expect(webTrendsProvider._getWebTrendsEventId('linkClick')).to.equal(99);
            expect(webTrendsProvider._getWebTrendsEventId('facebook')).to.equal(111);
            expect(webTrendsProvider._getWebTrendsEventId('heatmap')).to.equal(125);
            expect(webTrendsProvider._getWebTrendsEventId('not in the list')).to.equal(99);
        });

        it('webTrends provider should have a responseLogger function', function () {
            expect(webTrendsProvider._logResponse).to.exist;
            expect(webTrendsProvider._logResponse).to.be.a('function');
        });

        it('responseLogger recognizes successful "view" posts to WebTrends', function () {
            webTrendsProvider._logResponse({uri: 'test'}, true);
            expect(logSpy).to.have.been.calledWith(['(WT) view of uri "test" logged successfully']);
        });

        it('responseLogger recognizes successful "action" posts to WebTrends', function () {
            webTrendsProvider._logResponse({
                uri: 'test',
                eventType: 'submit',
                actionName: 'Sell Soul Form'
            }, true);
            expect(logSpy).to.have.been.calledWith([
                '(WT) submit:Sell Soul Form on uri "test" logged successfully'
            ]);
        });
        
        it('responseLogger properly logs events that do not have a uri', function () {
            webTrendsProvider._logResponse({
                uri: '',
                eventType: 'anchor',
                actionName: 'navigation link',
                title: 'My Business'
            }, true);
            expect(logSpy).to.have.been.calledWith([
                '(WT) anchor:navigation link on title "My Business" logged successfully'
            ]);
        });

        it('responseLogger recognizes unsuccessful "view" posts to WebTrends', function () {
            var resp = {errors: ['an error']};
            webTrendsProvider._logResponse({uri: 'test'}, resp);
            expect(logSpy).to.have.been.calledWith([
                '(WT) Errors logging analytics data', resp.errors]
            );
        });

        describe ('Custom identifier "DCSext.acting_as" for impersonated user ', function () {
            var webTrendsMultiTrackStub;

            beforeEach(function () {
                webTrendsMultiTrackStub = this.sinon.stub(WebTrends, 'multiTrack');
            });

            afterEach(function () {
                webTrendsMultiTrackStub.restore();
            });

            it('"setViewingAs" method should exist', function () {
                expect(webTrendsProvider.setViewingAs).to.be.exist;
            });

            it('"setViewingAs" should set impersonated webId', function () {
                webTrendsProvider.setViewingAs('someImpersonateWebId');
                expect(webTrendsProvider.impersonatedWebId).to.be.equal('someImpersonateWebId');
            });

            it('"setViewingAs" should set impersonatedWebId as null', function () {
                webTrendsProvider.setViewingAs(null);
                expect(webTrendsProvider.impersonatedWebId).to.be.equal(null);
            });
            
            it('should set if impersonatedWebId exist', function () {
                analytics.analyticsChannel.trigger('setViewingAs','someImpersonateWebId');

                analytics.analyticsChannel.trigger('trackView', {
                    uri: 'products'
                });

                expect(webTrendsMultiTrackStub).to.have.been.calledWithMatch({args: {
                    'DCSext.acting_as': 'someImpersonateWebId'
                }});

            });

            it('should set "null" if impersonatedWebId exist', function () {
                analytics.analyticsChannel.trigger('setViewingAs',null);

                analytics.analyticsChannel.trigger('trackView', {
                    uri: 'products'
                });

                expect(webTrendsMultiTrackStub).to.have.not.been.calledWithMatch({args: {
                    'DCSext.acting_as': null
                }});
            });
        });
    });

    describe('Webtrend analytics errors handling', function () {
        var wtProvider;
        var errorLogSpy;

        beforeEach(function () {
            wtProvider = new WebTrendsProvider({});
            errorLogSpy = this.sinon.spy(jsdomConsole, 'error');
        });

        afterEach(function () {
            wtProvider = null;
            errorLogSpy.restore();
        });

        it('should log error if webtrends options is missing while instantiating', function () { 
            wtProvider = new WebTrendsProvider({});
            expect(errorLogSpy)
                .to.have.been.calledWith(['WebTrends configurations are missing', {}]);
        });

        it('should log error if webtrends options are missing while calling trackView',
                function () {
            wtProvider.trackView();

            expect(errorLogSpy)
                .to.have.been.calledWith(['WebTrends configurations are missing']);
        });

        it('should log error if webtrends options are missing while calling trackAction',
                function () {
            wtProvider.trackAction();

            expect(errorLogSpy)
                .to.have.been.calledWith(['WebTrends configurations are missing']);
        });
    });
});

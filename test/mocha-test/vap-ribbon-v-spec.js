/* global expect:false, Backbone:false */

var helpers = require('./helpers/helpers');
var ViewAsProducerRibbonView = require('../dist/modules/viewAsProducer/vap-ribbon-v');

describe('View As Producer Ribbon View (modules/viewAsProducer/vap-ribbon-v.js)', function () {

    describe('Basic tests', function() {

        it('ViewAsProducerRibbonView exists', function() {
            expect(ViewAsProducerRibbonView).to.exist;
        });

        it('Throws an error if instantiated without a producer', function() {
            var fn = function() {
                var view = new ViewAsProducerRibbonView(); // eslint-disable-line no-unused-vars
            };

            expect(fn).to.throw(ViewAsProducerRibbonView.prototype.errors.producerRequired);
        });

        it('Instantiates normally with a producer property', function() {
            var fn = function() {
                var view = new ViewAsProducerRibbonView({ // eslint-disable-line no-unused-vars
                    producer : 'Boba Fett'
                });
            };

            expect(fn).to.not.throw(Error);
        });
    });

    describe('Functional tests', function () {
        var userChannel = Backbone.Radio.channel('user');
        var endStub;
        var rootView;
        var view;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new ViewAsProducerRibbonView({
                label           : 'Viewing as',
                endLinkLabel    : 'End View as Producer',
                producer        : 'Han Solo',
                showEnd         : true
            });
            rootView.showChildView('contentRegion', view);
            endStub = this.sinon.stub();
            userChannel.on('endViewAsProducer', endStub, view);
        });

        afterEach(function () {
            userChannel.stopListening();
            view.destroy();
            rootView.destroy();
            userChannel.off('endViewAsProducer');
            userChannel.reset();
        });

        it('Renders as expected', function () {
            var div = view.$el.find('.alert-view-producer');
            var end = view.$el.find('#end-vap-session-link');

            expect(div).to.have.length(1);
            expect(div.text()).to.be.contain('Viewing as');
            expect(end).to.have.length(1);
            expect(end.text()).to.be.contain('End View as Producer');
        });

        it('Renders as expected if showEnd flag is false', function () {
            view = new ViewAsProducerRibbonView({
                label    : 'Delegate for',
                producer : 'Han Solo',
                showEnd  : false
            });

            var end = view.$el.find('#end-vap-session-link');
            expect(end).to.have.length(0);
        });

        it('Has an "_endSession" function', function () {
            expect(view._endSession).to.exist.and.be.a.function;
        });

        it('the "_endSession" function triggers an event', function () {
            var fakeEvent = {
                preventDefault : function() {}
            };
            view._endSession(fakeEvent);
            expect(endStub).to.have.been.called.once;
        });
    });
});

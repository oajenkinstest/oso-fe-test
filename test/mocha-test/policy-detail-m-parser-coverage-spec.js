/* global expect:false, $:false, expect:false */

var coverageParser  = 
    require('../dist/pages/policy/models/parsers/policy-coverage');
var helpers = require('./helpers/helpers');

describe('Policy Detail Model parser - Coverage ' +
    '(pages/policy/models/parsers/policy-coverage.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
    describe('_setDisclaimerNoteState method', function () {

        it('exists as a function', function () {
            expect(coverageParser._setDisclaimerNoteState).to.exist.and.be.a('function');
        });

        it('defines `.showDisclaimerNote` property when productTypeCategory is ' +
            '"Life" and underwritingRequired is true', function () {
            policyDataCopy.product.productTypeCategory = 'Life';
            policyDataCopy.product.underwritingRequired = true;

            expect(policyDataCopy.showDisclaimerNote).to.be.undefined;

            coverageParser._setDisclaimerNoteState(policyDataCopy);

            expect(policyDataCopy.showDisclaimerNote).to.not.be.undefined;
        });

        describe('sets `.showDisclaimerNote` property to true', function () {
            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and workflowStatus is "Application Received"', function () {

                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.workflowStatus = 'Application Received';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });

            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and workflowStatus is "In Underwriting"', function () {
                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.workflowStatus = 'In Underwriting';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });

            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and status is "Declined"', function () {
                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.status = 'Declined';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });

            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and status is "Postponed"', function () {
                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.status = 'Postponed';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });

            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and status is "Withdrawn"', function () {
                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.status = 'Withdrawn';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });

            it('when productTypeCategory is "Life", underwritingRequired is true' +
                ' and status is "Incomplete"', function () {
                policyDataCopy.product.productTypeCategory = 'Life';
                policyDataCopy.product.underwritingRequired = true;
                policyDataCopy.policyStatus.status = 'Incomplete';

                coverageParser._setDisclaimerNoteState(policyDataCopy);

                expect(policyDataCopy.showDisclaimerNote).to.be.true;
            });
        });

        it('leaves `.showDisclaimerNote` property undefined when productTypeCategory is ' +
            '"Annuity" and underwritingRequired is false', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            policyDataCopy.product.productTypeCategory = 'Annuity';
            policyDataCopy.product.underwritingRequired = false;

            expect(policyDataCopy.showDisclaimerNote).to.be.undefined;

            coverageParser._setDisclaimerNoteState(policyDataCopy);

            expect(policyDataCopy.showDisclaimerNote).to.be.undefined;
        });

    }); // _setDisclaimerNoteState method

    describe('_setAnnuitantState method', function () {

        it('exists as a function', function () {
            expect(coverageParser._setAnnuitantState).to.exist.and.be.a('function');
        });

        it('adds `.isAnnuitant` flag correctly', function () {
            policyDataCopy.product.productTypeCategory = 'Annuity';
            policyDataCopy.product.productName = 'Fixed Annuity';

            expect(policyDataCopy.isAnnuitant).to.be.undefined;

            coverageParser._setAnnuitantState(policyDataCopy);

            expect(policyDataCopy.isAnnuitant).to.not.be.undefined;
        });

        it('adds flag when productTypeCategory is "Annuity" ' +
            'and productTypeCode is NOT "WL"', function () {

            policyDataCopy.product.productTypeCategory = 'Annuity';
            policyDataCopy.product.productTypeCode = 'Omaha';

            coverageParser._setAnnuitantState(policyDataCopy);

            expect(policyDataCopy.isAnnuitant).to.be.true;
        });

        it('adds flag when productTypeCategory is "Life" ' +
            'and productTypeCode is "SPFA"', function () {

            policyDataCopy.product.productTypeCategory = 'Life';
            policyDataCopy.product.productTypeCode = 'SPFA';

            coverageParser._setAnnuitantState(policyDataCopy);

            expect(policyDataCopy.isAnnuitant).to.be.true;
        });

        it('does NOT add flag when productTypeCategory is ' +
            '"Annuity" and productTypeCode is "WL"', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            policyDataCopy.product.productTypeCategory = 'Annuity';
            policyDataCopy.product.productTypeCode = 'WL';

            coverageParser._setAnnuitantState(policyDataCopy);

            expect(policyDataCopy.isAnnuitant).to.be.undefined;
        });

        it('does NOT add flag when productTypeCategory is ' +
            '"Care Solutions" and productTypeCode is "Asset Care"', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            policyDataCopy.product.productCategory = 'Care Solutions';
            policyDataCopy.product.productName = 'Asset Care';

            expect(policyDataCopy.isAnnuitant).to.be.undefined;

            coverageParser._setAnnuitantState(policyDataCopy);

            expect(policyDataCopy.isAnnuitant).to.be.undefined;
        });
    }); // _setAnnuitantState method


    describe('_setCoverageAge method', function () {

        it('exists as a function', function () {
            expect(coverageParser._setCoverageAge).to.exist.and.be.a('function');
        });

        it('should append "coverageAge" property if "issueAge" is present in the ' +
            'response', function () {

            var expectedValue = {
                label : 'Issue Age',
                value : policyDataCopy.issueAge
            };

            coverageParser._setCoverageAge(policyDataCopy);

            expect(policyDataCopy.coverageAge).to.deep.equal(expectedValue);
            delete policyDataCopy.coverageAge; // clean up 
        });

        it('should append "coverageAge" if "jointEqualAge" is present in the response',
            function () {
                var expectedValue;

                // remove issueAge and add jointEqualAge
                delete policyDataCopy.issueAge;
                policyDataCopy.jointEqualAge = 'P47Y';

                expectedValue = {
                    label : 'Joint Equal Age',
                    value : policyDataCopy.jointEqualAge
                };

                coverageParser._setCoverageAge(policyDataCopy);

                expect(policyDataCopy.coverageAge).to.deep.equal(expectedValue);
                delete policyDataCopy.coverageAge; // clean up 
            });

        it('should not append "coverageAge" if neither "issueAge" or "jointEqualAge" are ' +
            'present in the response', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

            delete policyDataCopy.issueAge;
            delete policyDataCopy.jointEqualAge;

            coverageParser._setCoverageAge(policyDataCopy);

            expect(policyDataCopy.coverageAge).to.be.undefined;
            delete policyDataCopy.coverageAge; // clean up 
        });

        it('should ignore "issueAge" and "jointEqualAge" if they are not implemented', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

            policyDataCopy.issueAge = { dataAvailability: 'notImplemented' };
            policyDataCopy.jointEqualAge = { dataAvailability: 'notImplemented' };

            coverageParser._setCoverageAge(policyDataCopy);

            expect(policyDataCopy.coverageAge).to.be.undefined;
        });
    }); // _setCoverageAge method

    describe('_setCoverageHeaderLeadingText method', function () {

        it('exists as a function', function () {
            expect(coverageParser._setCoverageHeaderLeadingText).to.exist.and.be.a('function');
        });

        describe('coverageHeaderLeadingText value created by the function', function () {

            it('should be appended to the response', function () {

                coverageParser._setCoverageHeaderLeadingText(policyDataCopy);

                expect(policyDataCopy.coverageHeaderLeadingText).to.exist;
            });

            it('should be "Single" if billingDetail.currentBilling.paymentMethod ' +
                'is "Single Premium"', function () {

                policyDataCopy.billingDetail.currentBilling.paymentMethod = 'Single Premium';

                coverageParser._setCoverageHeaderLeadingText(policyDataCopy);

                expect(policyDataCopy.coverageHeaderLeadingText).to.equal('Single');
            });

            it('should be <paymentMode> if billingDetail.currentBilling.paymentMethod is NOT ' +
                '"Single Premium" and <paymentMode> exists', function () {

                var mode = 'Depeche';

                policyDataCopy.billingDetail.currentBilling.paymentMethod = 'Quadruple Premium';
                policyDataCopy.billingDetail.currentBilling.paymentMode   = mode;

                coverageParser._setCoverageHeaderLeadingText(policyDataCopy);

                expect(policyDataCopy.coverageHeaderLeadingText).to.equal(mode);
            });

            it('should be "Modal" if billingDetail.paymentMethod is NOT ' +
                '"Single Premium" and <paymentMode> does NOT exist', function () {

                policyDataCopy.billingDetail.currentBilling.paymentMethod = 'Tredecuple Premium';

                delete policyDataCopy.billingDetail.currentBilling.paymentMode;

                coverageParser._setCoverageHeaderLeadingText(policyDataCopy);

                expect(policyDataCopy.coverageHeaderLeadingText).to.equal('Modal');
            });

        });

    }); // _setCoverageHeaderLeadingText method

    describe( '_setRelatedPoliciesLabels method',function () {

        it('exists as a function',function () {
            expect(coverageParser._setRelatedPoliciesLabels).to.exist.and.be.a('function');
        });

        it('should set label to "Single Premium" if billingMethod is'+ 
                '"Single Premium"',function () {
            var spText = 'Single Premium';

            policyDataCopy.relatedPolicies[0].billingMethod = spText;

            coverageParser._setRelatedPoliciesLabels(policyDataCopy);

            expect(policyDataCopy.relatedPolicies[0].label).to.equal(spText);
        });

        it('should set label to "Modal Premium" if billingMethod is falsey', function () {
            delete policyDataCopy.relatedPolicies[0].billingMethod;

            coverageParser._setRelatedPoliciesLabels(policyDataCopy);

            expect(policyDataCopy.relatedPolicies[0].label).to.equal('Modal Premium');
        });

        it('should set label to "<billingMode> Premium" if billingMethod is not ' +
            '"Single Premium"', function () {
            policyDataCopy.relatedPolicies[0].billingMethod = 'Foo';
            policyDataCopy.relatedPolicies[0].billingMode   = 'USDA';

            coverageParser._setRelatedPoliciesLabels(policyDataCopy);

            expect(policyDataCopy.relatedPolicies[0].label).to.equal('USDA Premium');
        });

    }); // _setRelatedPoliciesLabels method

    describe ('_setIsUlVulFlag method', function () {

        it('should exist and be a function', function () {
            expect(coverageParser._setIsUlVulFlag).to.exist.and.be.a('function');
        });

        it('should set "isUlVul" flag to false for non UL/VUL product', function () {
            var response = coverageParser._setIsUlVulFlag({
                product: {
                    productTypeCode: 'Giraffe'
                }
            });

            expect(response.isUlVul).to.exist.and.equal(false);
        });

        it('should set "isUlVul" flag to true for UL product', function () {
            var response = coverageParser._setIsUlVulFlag({
                product: {
                    productTypeCode: 'UL'
                }
            });

            expect(response.isUlVul).to.exist.and.equal(true);
        });

        it('should set "isUlVul" flag to true for VUL product', function () {
            var response = coverageParser._setIsUlVulFlag({
                product: {
                    productTypeCode: 'VUL'
                }
            });

            expect(response.isUlVul).to.exist.and.equal(true);
        });

        it('should set "isUlVul" flag to false if there is no product', function () {
            var response = coverageParser._setIsUlVulFlag({});
            expect(response.isUlVul).to.exist.and.equal(false);
        });

        it('should set "isUlVul" flag to false if there is no productTypeCode', function () {
            var response = coverageParser._setIsUlVulFlag({
                product: {}
            });
            expect(response.isUlVul).to.exist.and.equal(false);
        });
    }); // _setIsUlVulFlag method

    describe ('_setHasPUAAmountFlag method', function () {
        it('should exist and be a function', function () {
            expect(coverageParser._setHasPUAAmountFlag).to.exist.and.be.a('function');
        });

        describe('should update coverage object with hasPUAAmount flag', function () {
            it('flag exist if base/riders data has PUA amount', function () {
                
                //riders
                var response = coverageParser._setHasPUAAmountFlag({
                    coverage: {
                        base: [
                            {
                                'sequence' : 3,
                                'customerId': 25498373,
                                'name': 'Chronic Illness Rider'
                            }
                        ],
                        riders :[
                            {
                                'sequence' : 4,
                                'customerId': 25498374,
                                'divPUA': 'USD 9876.00'
                            }
                        ]
                    }
                });

                expect(response.coverage.hasPUAAmount).to.exist.and.equal(true);

                delete response.coverage.hasPUAAmount;

                // base
                response = coverageParser._setHasPUAAmountFlag({
                    coverage: {
                        base: [
                            {
                                'sequence' : 3,
                                'customerId': 25498373,
                                'name': 'Chronic Illness Rider',
                                'divPUA': 'USD 9876.00'
                            }
                        ],
                        riders :[
                            {
                                'sequence' : 4,
                                'customerId': 25498374
                            }
                        ]
                    }
                });
                expect(response.coverage.hasPUAAmount).to.exist.and.equal(true);
            });

            it('flag should be false PUA data missing', function () {
                var response = coverageParser._setHasPUAAmountFlag({
                    coverage: {
                        base: [
                            {
                                'sequence' : 3,
                                'customerId': 25498373,
                                'name': 'Chronic Illness Rider'
                            }
                        ],
                        riders :[
                            {
                                'sequence' : 4,
                                'customerId': 25498374
                            }
                        ]
                    }
                });

                expect(response.coverage.hasPUAAmount).to.be.false;
            });

            it('should not throw an exception if "coverage" is not present', function () {
                policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
                delete policyDataCopy.coverage;
    
                var fn = function () {
                    coverageParser._setHasPUAAmountFlag(policyDataCopy);
                };
    
                expect(fn).not.to.throw();
            });
        });

    });
});

/* global expect:false, Backbone:false */

var localStorageMemory = require('../dist/modules/localStorageMemory/localStorageMemory');

describe('localStorageMemory module ' +
    '(modules/localStorageMemory/localStorageMemory.js)', function() {

    it('is an instance of Marionette.Object', function () {
        expect(localStorageMemory).to.be.an.instanceof(Backbone.Marionette.Object);
    });
    
    describe('setItem method', function () {
        it('exists as a function', function () {
            expect(localStorageMemory.setItem).to.be.exist.and.be.a('function');
        });

        it('adds data to local cache', function () {
            localStorageMemory.setItem('newKey', 'some data');
            expect(localStorageMemory.getItem('newKey')).to.be.equal('some data');
        });
        
    }); // setItem method

    describe('getItem method', function () {

        it('exists as a function', function () {
            expect(localStorageMemory.getItem).to.be.exist.and.be.a('function');
        });
    }); // getItem method

    describe('removeItem method', function () {
        it('exists as a function', function () {
            expect(localStorageMemory.removeItem).to.be.exist.and.be.a('function');
        });

        it('removes data from local cache', function () {
            localStorageMemory.removeItem('newKey');
            expect(localStorageMemory.getItem('newKey')).to.be.null;
        });
    }); // removeItem method
    
    describe('key method', function () {
        it('exists as a function', function () {
            expect(localStorageMemory.key).to.be.exist.and.be.a('function');
        });

        it('returns the name of the key at a given index in the local cache', function () {
            localStorageMemory.setItem('newKey', 'some data');
            expect(localStorageMemory.key(0)).to.be.equal('newKey');
        });
    }); // key method

    describe('clear method', function () {
        it('exists as a function', function () {
            expect(localStorageMemory.clear).to.be.exist.and.be.a('function');
        });

        it('clears all data from the cache', function () {

            expect(localStorageMemory.length).to.equal(1);

            localStorageMemory.clear();

            expect(localStorageMemory.length).to.equal(0);

        });
    }); // clear method

});

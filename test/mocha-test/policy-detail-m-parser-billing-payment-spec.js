/* global expect:false, $:false, expect:false */

var PolicyBillingPayment = 
    require('../dist/pages/policy/models/parsers/policy-billing-payment');

describe('Policy Billing payment Model parse ' +
    '(pages/policy/models/parsers/policy-billing-payment.js)', function () {

    var policyBillingPayment;
    var data ={
        billingDetail : {
            currentBilling : {
                paymentMethod : 'direct',
                paymentMode   : 'annual',
                paymentAmount : 'USD 51953.34'
            },
            cashWithApplication : 'USD 51953.34'
        }, 
    };

    before(function () {
        policyBillingPayment = new PolicyBillingPayment();
    });

    afterEach(function () {
        policyBillingPayment.destroy();
    });

    it('method _setBillingDetailDisplayFlags to exists', function () {
        expect(policyBillingPayment._setBillingDetailDisplayFlags).to.exist
            .and.be.a('function');
    });

    it('set showBillingModeAndMethod flag if paymentMode / paymentMethod exist', function (){
        var dataCopy = $.extend(true,{},data);
        var response = policyBillingPayment._setBillingDetailDisplayFlags(data);
        expect(response.billingDetail.showBillingModeAndMethod).to.equal(
            data.billingDetail.currentBilling.paymentMode
        );

        dataCopy.billingDetail = {};
        response = policyBillingPayment._setBillingDetailDisplayFlags(dataCopy);
        expect(response.billingDetail.showBillingModeAndMethod).to.be.undefined;
    }); 

    it('set showCashIncomePlan flag if logic is match', function (){
        var dataCopy = $.extend(true,{},data);
        var response = policyBillingPayment._setBillingDetailDisplayFlags(data);
        expect(response.billingDetail.showCashIncomePlan).to.equal(
            data.billingDetail.currentBilling.paymentAmount
        );

        dataCopy.billingDetail = {};
        response = policyBillingPayment._setBillingDetailDisplayFlags(dataCopy);
        expect(response.billingDetail.showCashIncomePlan).to.be.undefined;
    });

    // Pending - Might need to add other conditions too. 
});
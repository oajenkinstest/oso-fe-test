/* global describe:false, expect:false, Backbone:false */

var AppStructureModel = require('../dist/models/appStructure-m');

var testStructure = [
    {
        icon        : 'home',
        capability  : {
            forSidebar : [{
                all : true
            }]
        },
        displayText : 'Home',
        link        : 'home'
    }, {
        icon        : 'list',
        displayText : 'My Business',
        subItems: [
            {
                capability  : {
                    forSidebar : [{
                        'Home_Office' : true,
                        'Policy_Search_by_Producer' : true
                    }]
                },
                defaultMyBusinessPage : true,
                displayText : 'Search',
                link        : 'search'
            }, {
                capability        : {
                    forSidebar : [{
                        'Policy_Search_by_Producer' : true,
                        'Home_Office' : false
                    }],
                    forPageAccess : {
                        'Policy_Search_by_Producer' : true
                    }
                },
                displayText             : 'Pending Policies',
                link                    : 'pending-policy-manager',
                defaultMyBusinessPage   : true,
                activeFor               : [
                    'pending-policies',
                    'pending-policy-detail'
                ]
            }, {
                capability        : {
                    forSidebar : [{
                        'Policy_View' : true,
                        'Policy_Search_by_Producer' : false,
                        'Home_Office' : false
                    }],
                    forPageAccess : {
                        'Policy_View' : true
                    }
                },
                displayText           : 'Pending Policies',
                link                  : 'pending-policies',
                defaultMyBusinessPage : true,
                activeFor             : [
                    'pending-policy-manager',
                    'pending-policy-detail'
                ]
            }, {
                capability  : {
                    forPageAccess : {
                        'Policy_View' : true
                    }
                },
                displayText : 'Pending Policy Detail',
                link        : 'pending-policy-detail'
            }
        ]
    }, {
        icon        : 'info-circle',
        displayText : 'FAQs & Resources',
        subItems: [
            {
                capability: {
                    forSidebar : [{
                        'user.products' : true
                    }]
                },
                displayText: 'Products',
                link: 'products'
            }, {
                displayText: 'Page One',
                subItems: [
                    {
                        capability: {
                            forSidebar : [{
                                'user.pageonea' : true
                            }]
                        },
                        icon: 'arrow-right',
                        displayText: 'Page 1A',
                        link: 'pageone-1a'
                    }, {
                        capability: {
                            forSidebar : [{
                                'user.pageoneb' : true
                            }]
                        },
                        icon: 'arrow-right',
                        displayText: 'Page 1B',
                        link: 'pageone-1b'
                    }
                ]
            }
        ]
    }, {
        icon        : 'tachometer',
        capability  : {
            forSidebar : [{
                'user.hello' : true
            }]
        },
        displayText : 'Hello',
        link        : 'hello'
    }
];


describe('App Structure Model (models/appStructure-m)', function () {

    it('exists', function () {
        expect(AppStructureModel).to.exist;
    });


    describe('default "structure" property', function () {

        var appStructure;

        beforeEach(function () {
            var instance = new AppStructureModel();
            appStructure = instance.get('structure');
        });

        afterEach(function () {
            appStructure = null;
        });

        it('is a non-empty array', function () {
            expect(appStructure).to.be.an('array').that.is.not.empty;
        });

        it('all nodes only include valid keys', function () {
            var validKeys = 'icon capability displayText link view subItems active ' +
                'activeFor forSidebar forPageAccess defaultMyBusinessPage';

            // Recursive function to iterate through an object and make sure
            // that it contains no invalid property names or empty arrays
            var hasValidKeys = function (objToValidate) {
                var i, key;

                // If objToValidate is an array, it mustn't be empty and all of its
                // elements must also be valid.
                if (Array.isArray(objToValidate)) {

                    expect(objToValidate).is.not.empty;

                    for (i=0; i<objToValidate.length; i++) {
                        hasValidKeys(objToValidate[i]);
                    }

                // If the obj is a string (probably a capability name in an "activeFor" array),
                // then it's valid.
                } else if (typeof objToValidate === 'string') {
                    return true;

                } else {

                    // Other properties must be limited to list of valid keys
                    for (key in objToValidate) {
                        if (objToValidate.hasOwnProperty(key)) {
                            expect(validKeys).to.have.string(key);

                            if (Array.isArray(objToValidate[key])) {
                                hasValidKeys(objToValidate[key]);
                            }
                        }
                    }
                }
            };

            hasValidKeys(appStructure);
        });

        it('all nodes include a "displayText" property', function () {
            // Recursive function to iterate through an object and make sure
            // that each node has a "displayText" function
            var allNodesHaveDisplayText = function (objToValidate) {
                var i, key;

                if (Array.isArray(objToValidate)) {
                    for (i=0; i<objToValidate.length; i++) {
                        allNodesHaveDisplayText(objToValidate[i]);
                    }

                } else {

                    // Must have displayText
                    expect(objToValidate).to.have.property('displayText');

                    // Iterate into any array properties
                    for (key in objToValidate) {
                        if (objToValidate.hasOwnProperty(key)) {

                            if (Array.isArray(objToValidate[key])) {
                                if (key === 'subItems' ) {
                                    allNodesHaveDisplayText(objToValidate[key]);
                                }
                            }
                        }
                    }
                }
            };

            allNodesHaveDisplayText(appStructure);
        });

        it('all nodes without subItems have a "view" property', function () {

            var allLeafNodesHaveAView = function (objToValidate) {
                var i, key;

                if (Array.isArray(objToValidate)) {
                    for (i=0; i<objToValidate.length; i++) {
                        allLeafNodesHaveAView(objToValidate[i]);
                    }

                } else {

                    if (!objToValidate.subItems) {
                        expect(objToValidate).to.have.property('view');
                    }

                    // Iterate into any array properties
                    for (key in objToValidate) {
                        if (objToValidate.hasOwnProperty(key)) {

                            if (Array.isArray(objToValidate[key])) {
                                //Ignore activeFor property as it is part of one
                                //navigation item and its plain array of string
                                if (key === 'subItems') {
                                    allLeafNodesHaveAView(objToValidate[key]);
                                }
                            }
                        }
                    }
                }
            };

            allLeafNodesHaveAView(appStructure);
        });

        it('starts with a "Home" node', function () {
            expect(appStructure[0]).to.have.property('displayText')
                .that.equals('Home');
        });

    });


    it('can override the default "structure" property', function () {
        var instance = new AppStructureModel({
            structure: testStructure
        });

        var actualStructure = instance.get('structure');

        expect(actualStructure).to.deep.equal(testStructure);
    });

    describe('filterForSidebar method', function () {

        var modelInstance;

        beforeEach(function () {
            modelInstance = new AppStructureModel({
                structure: testStructure
            });
        });

        afterEach(function () {
            modelInstance = null;
        });

        it('removes all items except "forSidebar" items that match the user\'s ' +
                'capabilities', function () {

            var testCapabilities = [
                'user.products',
                'user.pageonea'
            ];

            var expectedStructure = [
                {
                    icon        : 'home',
                    capability  : {
                        forSidebar : [{
                            all : true
                        }]
                    },
                    displayText : 'Home',
                    link        : 'home'
                }, {
                    icon        : 'info-circle',
                    displayText : 'FAQs & Resources',
                    subItems: [
                        {
                            capability:{
                                forSidebar : [{
                                    'user.products' : true
                                }]
                            },
                            displayText: 'Products',
                            link: 'products'
                        }, {
                            displayText: 'Page One',
                            subItems: [
                                {
                                    capability: {
                                        forSidebar : [{
                                            'user.pageonea' : true
                                        }]
                                    },
                                    icon: 'arrow-right',
                                    displayText: 'Page 1A',
                                    link: 'pageone-1a'
                                }
                            ]
                        }
                    ]
                }
            ];

            var filteredAppStructure = modelInstance.filterForSidebar(testCapabilities);

            expect(filteredAppStructure).to.deep.equal(expectedStructure);
        });

        it('removes the parent node if all child nodes are filtered out', function () {

            // Remove both "pageone" subitems
            var testCapabilities = [
                'user.products',
                'user.hello'
            ];

            var expectedStructure = [
                {
                    icon        : 'home',
                    capability  : {
                        forSidebar : [{
                            all : true
                        }]
                    },
                    displayText : 'Home',
                    link        : 'home'
                }, {
                    icon        : 'info-circle',
                    displayText : 'FAQs & Resources',
                    subItems: [
                        {
                            capability: {
                                forSidebar : [{
                                    'user.products' : true
                                }]
                            },
                            displayText: 'Products',
                            link: 'products'
                        }
                    ]
                }, {
                    icon        : 'tachometer',
                    capability  : {
                        forSidebar : [{
                            'user.hello' : true
                        }]
                    },
                    displayText : 'Hello',
                    link        : 'hello'
                }
            ];

            var filteredAppStructure = modelInstance.filterForSidebar(testCapabilities);

            expect(filteredAppStructure).to.deep.equal(expectedStructure);
        });

        it('removes menu items that don\'t have "forSidebar" property and ' +
            'returns default MyBusiness Page', function () {

            // Remove both "pageone" subitems
            var policyCapabilities = [
                'Policy_Search',
                'Policy_View',
                'Home_Office',
                'Policy_Search_by_Producer'
            ];

            var expectedStructure = [
                {
                    icon        : 'home',
                    capability  : {
                        forSidebar : [{
                            all : true
                        }]
                    },
                    displayText : 'Home',
                    link        : 'home'
                },
                {
                    icon        : 'list',
                    displayText : 'My Business',
                    subItems: [
                        {
                            capability  : {
                                forSidebar : [{
                                    'Home_Office' : true,
                                    'Policy_Search_by_Producer' : true
                                }]
                            },
                            defaultMyBusinessPage : true,
                            displayText : 'Search',
                            link        : 'search'
                        }
                    ]
                }
            ];

            var filteredAppStructure = modelInstance.filterForSidebar(policyCapabilities);

            expect(filteredAppStructure).to.deep.equal(expectedStructure);
        });

        it('returns a structure with "all" capability items ' +
                'when parameter \'capabilities\' is an empty array', function () {

            var expectedStructure = [
                {
                    icon        : 'home',
                    capability  :{
                        forSidebar : [{
                            all : true
                        }]
                    },
                    displayText : 'Home',
                    link        : 'home'
                }
            ];

            var filteredAppStructure = modelInstance.filterForSidebar([]);

            expect(filteredAppStructure).to.deep.equal(expectedStructure);
        });

        it('returns a structure with "all" capability items ' +
                'even when parameter \'capabilities\' is undefined', function () {

            var expectedStructure = [
                {
                    icon        : 'home',
                    capability  : {
                        forSidebar : [{
                            all : true
                        }]
                    },
                    displayText : 'Home',
                    link        : 'home'
                }
            ];

            var filteredAppStructure = modelInstance.filterForSidebar();

            expect(filteredAppStructure).to.deep.equal(expectedStructure);
        });

        it('throws error if parameter \'capabilities\' is not an array', function () {

            var fn = function () {
                modelInstance.filterForSidebar({});
            };

            expect(fn).to.throw(modelInstance.errors.capabilitiesShouldbeArray);

        });

    });

    describe('appendStructure Method', function () {

        var appStructureModel = new AppStructureModel({
            structure: testStructure
        });
        var wcmStructureToAppend = [
            {
                icon        : 'pencil-square-o',
                displayText : 'Products',
                subItems    : [
                    {
                        icon        : '',
                        displayText : 'Term',
                        subItems    : [
                            {
                                icon        : 'home',
                                displayText : 'Overview',
                                capability  : 'all',
                                link        : 'c/Marketing/ELEVATE'
                            }, {
                                icon        : 'arrow-right',
                                displayText : 'American Protector Plus',
                                capability  : 'all',
                                link        : 'c/Sales+Support/Selling+Concepts+to+Grow'+
                                                '+Your+Practice'
                            }
                        ]
                    }
                ]
            }
        ];

        it('exists', function () {
            expect(appStructureModel.appendStructure).to.exist;
        });

        it('correctly appends an additional structure item to the appStructure', function () {
            var startingStructureLength = appStructureModel.get('structure').length;
            var additionalStructureLength = wcmStructureToAppend.length;

            var WcmView = require('../dist/modules/wcm-content/views/wcm-content-v');
            appStructureModel.appendStructure(wcmStructureToAppend, WcmView);
            var currentStructureLength = appStructureModel.get('structure').length;

            expect(currentStructureLength)
                .to.equal(startingStructureLength + additionalStructureLength);

            var lastStructureItem = appStructureModel.get('structure')[currentStructureLength - 1];
            expect(lastStructureItem).to.deep.equal(wcmStructureToAppend[0]);
        });

        it('throws an error if the parameter is not an Array', function () {
            var fn = function () {
                appStructureModel.appendStructure({});
            };
            expect(fn).to.throw(appStructureModel.errors.structureShouldbeArray);
        });

        it('throws an error if view parameter is not an instance of Backbone.View', function () {
            var wcmView  = 'some string';
            var fn = function () {
                appStructureModel.appendStructure(wcmStructureToAppend, wcmView);
            };
            expect(fn).to.throw(appStructureModel.errors.viewIsNotInstanceOfBackboneView);
        });
    });

    describe('_getPageDataByKeyValue method', function () {

        var appStructureModel;

        beforeEach( function () {
            appStructureModel = new AppStructureModel({
                structure: testStructure
            });

            appStructureModel.filterForSidebar([
                'Policy_Search',
                'Home_Office',
                'Policy_Search_by_Producer'
            ]);
        });
        afterEach( function () {
            appStructureModel = null;
        });

        it('exists', function () {
            expect(appStructureModel._getPageDataByKeyValue).to.exist;
        });

        it('returns the right page data for a valid key', function () {
            var expectedStructure = {
                capability : {
                    forSidebar : [{
                        'Home_Office' : true,
                        'Policy_Search_by_Producer' : true
                    }]
                },
                defaultMyBusinessPage :true,
                displayText : 'Search',
                link : 'search'
            };

            expect(appStructureModel._getPageDataByKeyValue('defaultMyBusinessPage', true))
                .to.deep.equal(expectedStructure);
        });

        it('returns undefined if the key doesn\'t exist', function () {
            expect(appStructureModel._getPageDataByKeyValue('defaultMyBusinessPage', false))
                .to.be.undefined;
        });
    });

    describe('Backbone.Radio - appStructureChannel', function () {

        it('exists', function () {
            expect(Backbone.Radio._channels.appStructure).to.exist;
        });

        it('has a getPageDataByKeyValue callback', function () {
            expect(Backbone.Radio._channels.appStructure._requests.getPageDataByKeyValue)
                .to.exist;
        });
    });

});

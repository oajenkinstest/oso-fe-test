/* global describe:false, jsdomConsole:false, expect:false, WebTrends:false, $:false */

var analytics = require('../dist/modules/analytics/analyticsModule');
var WebTrendsProvider
            = require('../dist/modules/analytics/providers/webtrendsAnalyticsProvider');

describe('Analytics Module (modules/analytics/analyticsModule.js)', function () {

    var trackViewSpy;
    var trackActionSpy;
    var errorLogSpy;
    var trackExceptionSpy;
    var setDataLayerSpy;
    var unsetDataLayerSpy;
    var setViewingAsSpy;


    beforeEach(function () {
        errorLogSpy             = this.sinon.spy(jsdomConsole, 'error');
        setDataLayerSpy         = this.sinon.spy(analytics, 'setDataLayerValue');
        unsetDataLayerSpy       = this.sinon.spy(analytics, 'unsetDataLayerValue');
        setViewingAsSpy         = this.sinon.spy(analytics, 'setViewingAs');
        trackViewSpy            = this.sinon.spy(analytics, 'trackView');
        trackActionSpy          = this.sinon.spy(analytics, 'trackAction');
        trackExceptionSpy       = this.sinon.spy(analytics, 'trackException');

        analytics.init({
            providers: [
            ],
            webId: '123456'
        });
    });

    afterEach(function () {
        errorLogSpy.resetHistory();
        errorLogSpy.restore();
        setDataLayerSpy.restore();
        unsetDataLayerSpy.restore();
        setViewingAsSpy.restore();
        trackViewSpy.restore();
        trackActionSpy.restore();
        trackExceptionSpy.restore();
    });

    it('AnalyticsModule can be instantiated', function () {
        expect(analytics).to.exist;
    });

    it('Init method should exist', function () {
        expect(analytics.init).to.exist;
    });

    it('Channel name \'analytics\' should exist', function () {
        expect(analytics.analyticsChannel.channelName).to.equal('analytics');
    });

    it('At least one provider should exits, otherwise error should throw', function () {
        expect(errorLogSpy)
            .to.have.been.calledWith(['Expected atleast one providers to capture analytics data']);
    });

    describe('`setDataLayerValue` method', function () {

        it('should exist', function () {
            expect(analytics.setDataLayerValue).to.be.a('function');
        });

        it('should be called when "setDataLayerValue" is called on analytics channel', function () {
            analytics.analyticsChannel.trigger('setDataLayerValue', {
                foo : 'bar'
            });

            expect(setDataLayerSpy).to.have.been.calledOnce;
        });
    });

    describe('`unsetDataLayerValue` method', function () {

        it('should exist', function () {
            expect(analytics.unsetDataLayerValue).to.exist.and.be.a('function');
        });

        it('should be called when "unsetDataLayerValue" is called on analytics channel', function () {
            analytics.analyticsChannel.trigger('unsetDataLayerValue', {
                foo : 'bar'
            });

            expect(unsetDataLayerSpy).to.have.been.calledOnce;
        });
    });

    describe('Method trackView', function () {

        it('Should exist', function (){
            expect(analytics.trackView).to.exist;
        });

        it('Should be called while logging data', function () {
            analytics.analyticsChannel.trigger('trackView', {
                uri: 'products'
            });

            expect(trackViewSpy).to.have.been.called;
        });

        it('Should be called', function () {
            analytics.analyticsChannel.trigger('trackView', {
                uri: '#products'
            });

            expect(trackViewSpy).to.have.been.called;
        });

        it('Should not be called after invoking stopComplying', function () {
            analytics.stopListening(analytics.analyticsChannel,'trackView');
            analytics.analyticsChannel.trigger('trackView', {
                uri: 'products'
            });

            expect(trackViewSpy).to.have.not.been.called;
        });
    });


    describe('Method trackAction', function () {

        it('should exist', function () {
            expect(analytics.trackAction).to.exist;
        });

        it('should be called', function () {
            analytics.analyticsChannel.trigger('trackAction', {
                uri: 'products',
                actionName: 'filter',
                eventType: 'formGet'
            });
            expect(trackActionSpy).to.have.been.called;
        });

        it('should be called with event object', function () {

            analytics.analyticsChannel.trigger('trackAction', {
                event: {
                    which: 3,
                    currentTarget: {
                        tagName: 'A',
                        text: 'Policy Details'
                    }
                }
            });
            expect(trackActionSpy).to.have.been.called;
        });

        it('should not be called after invoking stopComplying', function () {

            analytics.stopListening(analytics.analyticsChannel,'trackAction');
            analytics.analyticsChannel.trigger('trackAction', {
                uri: 'products',
                actionName: 'filter',
                eventType: 'formGet'
            });
            expect(trackActionSpy).to.have.not.been.called;
        });

    });

    describe('Method trackException', function () {
            
        it('Should exist', function (){
            expect(analytics.trackException).to.exist;
        });

        it('Should be called while logging data', function () {
            analytics.analyticsChannel.trigger('trackException', {
                level: 'error',
                message: 'some error occured'
            });

            expect(trackExceptionSpy).to.have.been.calledWith({
                level: 'error',
                message: 'some error occured'
            });
        });

        it('Should not be called after invoking stopComplying', function () {
            analytics.stopListening(analytics.analyticsChannel,'trackException');
            analytics.analyticsChannel.trigger('trackException', {
                level: 'error',
                message: 'some error occured'
            });

            expect(trackExceptionSpy).to.have.not.been.called;
        });
    });

    describe('Method setViewingAs', function () {

        it('should exist', function () {
            expect(analytics.setViewingAs).to.exist;
        });
        it('should called while logging data', function () {
            analytics.analyticsChannel.trigger('setViewingAs', 'someImpersonatedWebId');

            expect(setViewingAsSpy).to.have.been.calledWith('someImpersonatedWebId');
        });

    });

    describe('Method _getEventDetails', function () {

        it('should exist', function () {
            expect(analytics._getEventDetails).to.exist;
        });

        it('should return title if data title specified', function () {

            var eventDetails;
            var $dataTitleLink;

            $('body').append('<a data-title="Business Title">Business Link</a>');
            $dataTitleLink = $('a[data-title="Business Title"]');
            $dataTitleLink.click(function (event) {
                eventDetails = analytics._getEventDetails(event);
                expect(eventDetails.title).to.equal('Business Title');
            });

            $dataTitleLink.click();
            eventDetails = null;
            $dataTitleLink.remove();

        });

        it('should return expected details from event object', function () {
            var eventDetails = analytics._getEventDetails({
                which: 3,
                currentTarget: {
                    hash: '#/policy-details',
                    tagName: 'A',
                    innerText: 'Policy Details'
                }
            });
            expect(eventDetails.eventType).to.equal('rightClick');
            expect(eventDetails.uri).to.equal('#/policy-details');
            expect(eventDetails.title).to.equal('Policy Details');
        });

        it('should return title if textContent property exist', function () {
            var eventDetails = analytics._getEventDetails({
                which: 3,
                currentTarget: {
                    hash: '#/policy-details',
                    tagName: 'A',
                    textContent: 'Policy Details'
                }
            });
            expect(eventDetails.title).to.equal('Policy Details');
        });

    });

    describe('method _getElementTrackingInfo', function () {
        var currentTarget;
        var elementTrackingInfo;

        afterEach(function () {
            currentTarget = null;
            elementTrackingInfo = null;
            $('a.test-link').remove();
        });

        it('should exist', function () {
            expect(analytics._getElementTrackingInfo).to.exist;
        });

        describe('A element - Event Data capture', function () {

            it('should return eventType and title : Javascript link', function () {

                $('body')
                    .append('<a class="test-link" href="javascript:showSomething()">js link</a>');

                $('a.test-link').click(function (event) {
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);

                    expect(elementTrackingInfo.eventType).to.equal('javascript');
                    expect(elementTrackingInfo.title).to.equal('javascript link:showSomething()');
                });

                $('a.test-link').click();

            });

            it('should return eventType and title  : Mailto link', function () {

                $('body')
                    .append('<a class="test-link" href="mailto:oso@oa.com">mail to</a>');

                $('a.test-link').click(function (event) {
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);

                    expect(elementTrackingInfo.eventType).to.equal('mailto');
                    expect(elementTrackingInfo.title).to.equal('mailto link:oso@oa.com');
                });

                $('a.test-link').click();

            });

            it('should return eventType and title  : Download link', function () {

                $('body').append('<a class="test-link"'+
                    'href="http://www1.oneamerica.com/oso/files/Deck.pdf">download pdf</a>');

                $('a.test-link').click(function (event) {
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);

                    expect(elementTrackingInfo.eventType).to.equal('download');
                    expect(elementTrackingInfo.title)
                        .to.equal('Download link:www1.oneamerica.com/oso/files/Deck.pdf');
                });

                $('a.test-link').click();

            });

            it('should return eventType and title : External link', function () {

                $('body').append('<a class="test-link"'+
                    'href="http://www.yahoo.com/">yahoo</a>');

                $('a.test-link').click(function (event) {
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);

                    expect(elementTrackingInfo.eventType).to.equal('external');
                    expect(elementTrackingInfo.title)
                        .to.equal('Offsite link:www.yahoo.com/');
                });

                $('a.test-link').click();

            });
        });

        describe('Button element - Event Data capture', function () {
            var actionSpy;
            afterEach (function () {
                $('.test-form').remove();
                if (actionSpy) {
                    actionSpy.restore();
                }
            });
            it('INPUT (type=submit) element under FORM tag ', function () {
                $('body').append('<form class="test-form" id="policies-search-form">'+
                    '<input type="submit" value="Search"></form>');

                this.clickFunction  = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formGet');
                    expect(elementTrackingInfo.title)
                        .to.equal('Form submit (element - input): Search - '+
                        'policies-search-form');
                };
                actionSpy = this.sinon.spy(this, 'clickFunction');
                $('#policies-search-form input[type=submit]').click(this.clickFunction);
                $('#policies-search-form input[type=submit]').click();

                expect(actionSpy).to.have.been.called;
            });


            it('INPUT (type=submit) element under FORM tag - No form ID ', function () {
                $('body').append('<form class="test-form">'+
                    '<input type="submit" value="Search"></form>');
                this.clickFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formGet');
                    expect(elementTrackingInfo.title)
                        .to.equal('Form submit (element - input): Search');
                };
                actionSpy = this.sinon.spy(this, 'clickFunction');
                $('.test-form input[type=submit]').click(this.clickFunction);
                $('.test-form input[type=submit]').click();

                expect(actionSpy).to.have.been.called;
            });

            it('BUTTON element under FORM tag ', function () {
                $('body').append('<form method="post" class="test-form" '+
                    'id="policies-search-form"><button>Search Policies</button></form>');
                this.clickFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formPost');
                    expect(elementTrackingInfo.title)
                        .to.equal('Form submit (element - button): '+
                            'Search Policies - policies-search-form');
                };
                actionSpy = this.sinon.spy(this, 'clickFunction');
                $('#policies-search-form button').click(this.clickFunction);
                $('#policies-search-form button').click();

                expect(actionSpy).to.have.been.called;
            });

            it('INPUT (type=button) element under FORM tag', function () {
                $('body').append('<form class="test-form">'+
                    '<input type="button" id="cancel-button" value="Cancel"></form>');
                this.clickFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formInput');
                    expect(elementTrackingInfo.title)
                        .to.equal('Button click (element - input): Cancel');
                };
                actionSpy = this.sinon.spy(this, 'clickFunction');
                $('.test-form input[type=button]').click(this.clickFunction);
                $('.test-form input[type=button]').click();

                expect(actionSpy).to.have.been.called;
            });

            it('Button (type=button) element under FORM tag', function () {
                $('body').append('<form class="test-form">'+
                    '<button type="button" id="cancel-button">Cancel</button></form>');
                this.clickFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formButton');
                    expect(elementTrackingInfo.title)
                        .to.equal('Button click (element - button): Cancel');
                };
                actionSpy = this.sinon.spy(this, 'clickFunction');
                $('.test-form button[type=button]').click(this.clickFunction);
                $('.test-form button[type=button]').click();

                expect(actionSpy).to.have.been.called;
            });

        });     // Button element - Event Data capture

        describe('Form element', function () {
            var actionSpy;
            afterEach (function () {
                $('.test-form').remove();
                if (actionSpy) {
                    actionSpy.restore();
                }
            });
            it('FORM element - Post', function () {
                $('body').append('<form method="POST" class="test-form" '+
                    'id="policies-search-form"><button>Search Policies</button></form>');
                this.submitFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formPost');
                    expect(elementTrackingInfo.title)
                        .to.equal('Form submit (element - form): policies-search-form');
                };
                actionSpy = this.sinon.spy(this, 'submitFunction');
                $('#policies-search-form').submit(this.submitFunction);
                $('#policies-search-form').submit();

                expect(actionSpy).to.have.been.called;
            });

            it('FORM element - Get', function () {
                $('body').append('<form id="policies-search-form"  class="test-form" >'+
                    '<button>Search Policies</button></form>');
                this.submitFunction = function (event) {
                    event.preventDefault();
                    currentTarget = event.currentTarget;
                    elementTrackingInfo = analytics._getElementTrackingInfo(currentTarget);
                    
                    expect(elementTrackingInfo.eventType).to.equal('formGet');
                    expect(elementTrackingInfo.title)
                        .to.equal('Form submit (element - form): policies-search-form');
                };
                actionSpy = this.sinon.spy(this, 'submitFunction');
                $('#policies-search-form').submit(this.submitFunction);
                $('#policies-search-form').submit();

                expect(actionSpy).to.have.been.called;
            });
        });     // Form element

    });

});

describe('WebTrends framework', function () {

    it('webTrend multitrack object should exist', function () {
        expect(WebTrends.multiTrack).to.exist;
    });

    it('multitrack callback method should be invoked after logging data to webTrends', function () {

        var webTrendsMultiTrackStub = this.sinon.stub(WebTrends, 'multiTrack')
            .yieldsTo('callback', true);
        analytics.analyticsChannel.trigger('trackView', {
            uri: 'products'
        });

        WebTrends.multiTrack({
            args: {
                'DCS.dcsuri': 'products',
                'WT.dl': 0,
                'WT.ti': 'OneSourceOnline'
            },
            callback: function (response) {
                expect(response).to.equal(true);
            }
        });

        webTrendsMultiTrackStub.restore();
    });

    it('callback method should be invoked after logging action', function () {

        var webTrendsMultiTrackStub =this.sinon.stub(WebTrends, 'multiTrack')
            .yieldsTo('callback', true);

        WebTrends.multiTrack({
            args: {
                'DCS.dcsuri': 'products',
                'WT.dl': 0,
                'WT.ti': 'OneSourceOnline'
            },
            callback: function (response) {
                expect(response).to.equal(true);
            }
        });

        webTrendsMultiTrackStub.restore();
    });
});

describe('WebTrends provider', function () {

    var errorSpy;
    var logSpy;
    var setUserWebTrendsSpy;
    var webTrendsProvider;

    beforeEach(function () {
        analytics.init({
            providers: [
                new WebTrendsProvider({
                    dcsid: '123456',
                    onsitedoms: 'oneamerica.com',
                    fpcdom: '.oneamerica.com'
                })
            ],
            webId: '123456'
        });

        logSpy              = this.sinon.spy(jsdomConsole, 'log');
        errorSpy            = this.sinon.spy(jsdomConsole, 'error');
        webTrendsProvider   = analytics.providers[0];
    });

    afterEach(function () {
        errorSpy.restore();
        logSpy.restore();
    });

    //webtrends provider methods test
    it('webTrends options value should match', function () {
        expect(webTrendsProvider.options.dcsid).to.equal('123456');
        expect(webTrendsProvider.options.onsitedoms).to.equal('oneamerica.com');
        expect(webTrendsProvider.options.fpcdom).to.equal('.oneamerica.com');
    });

    it('setUser method in provider class can be called', function () {
        setUserWebTrendsSpy = this.sinon.spy(webTrendsProvider, 'setUser');
        webTrendsProvider.setUser('123456');

        expect(setUserWebTrendsSpy).to.have.been.calledWith('123456');
        setUserWebTrendsSpy.restore();
    });

    it('WebTrends provider should have a getWebTrendsEventId function', function () {
        expect(webTrendsProvider._getWebTrendsEventId).to.exist;
        expect(webTrendsProvider._getWebTrendsEventId).to.be.a('function');
    });

    it('WebTrends IDs should translate correctly', function () {
        expect(webTrendsProvider._getWebTrendsEventId('pageView')).to.equal(0);
        expect(webTrendsProvider._getWebTrendsEventId('download')).to.equal(20);
        expect(webTrendsProvider._getWebTrendsEventId('anchor')).to.equal(21);
        expect(webTrendsProvider._getWebTrendsEventId('javascript')).to.equal(22);
        expect(webTrendsProvider._getWebTrendsEventId('mailto')).to.equal(23);
        expect(webTrendsProvider._getWebTrendsEventId('external')).to.equal(24);
        expect(webTrendsProvider._getWebTrendsEventId('rightClick')).to.equal(25);
        expect(webTrendsProvider._getWebTrendsEventId('formGet')).to.equal(26);
        expect(webTrendsProvider._getWebTrendsEventId('formPost')).to.equal(27);
        expect(webTrendsProvider._getWebTrendsEventId('formInput')).to.equal(28);
        expect(webTrendsProvider._getWebTrendsEventId('formButton')).to.equal(29);
        expect(webTrendsProvider._getWebTrendsEventId('imageMap')).to.equal(30);
        expect(webTrendsProvider._getWebTrendsEventId('youTubeImpression')).to.equal(40);
        expect(webTrendsProvider._getWebTrendsEventId('videoEvent')).to.equal(41);
        expect(webTrendsProvider._getWebTrendsEventId('onSiteAd')).to.equal(50);
        expect(webTrendsProvider._getWebTrendsEventId('mobileEvent')).to.equal(60);
        expect(webTrendsProvider._getWebTrendsEventId('mobileStateEvent')).to.equal(61);
        expect(webTrendsProvider._getWebTrendsEventId('linkClick')).to.equal(99);
        expect(webTrendsProvider._getWebTrendsEventId('facebook')).to.equal(111);
        expect(webTrendsProvider._getWebTrendsEventId('heatmap')).to.equal(125);
        expect(webTrendsProvider._getWebTrendsEventId('not in the list')).to.equal(99);
    });

    it('webTrends provider should have a responseLogger function', function () {
        expect(webTrendsProvider._logResponse).to.exist;
        expect(webTrendsProvider._logResponse).to.be.a('function');
    });

    it('responseLogger recognizes successful "view" posts to WebTrends', function () {
        webTrendsProvider._logResponse({uri: 'test'}, true);

        expect(logSpy).to.have.been.calledWith(['(WT) view of uri "test" logged successfully']);
    });

    it('responseLogger recognizes successful "action" posts to WebTrends', function () {
        webTrendsProvider._logResponse({
            uri: 'test',
            eventType: 'submit',
            actionName: 'Sell Soul Form'
        }, true);

        expect(logSpy).to.have.been.calledWith([
            '(WT) submit:Sell Soul Form on uri "test" logged successfully'
        ]);
    });

    it('responseLogger properly logs events that do not have a uri', function() {
        webTrendsProvider._logResponse({
            uri: '',
            eventType: 'anchor',
            actionName: 'navigation link',
            title: 'My Business'
        }, true);

        expect(logSpy).to.have.been.calledWith([
            '(WT) anchor:navigation link on title "My Business" logged successfully'
        ]);
    });

    it('responseLogger recognizes unsuccessful "view" posts to WebTrends', function () {
        var resp = {errors: ['an error']};
        webTrendsProvider._logResponse({uri: 'test'}, resp);

        expect(logSpy).to.have.been.calledWith([
            '(WT) Errors logging analytics data', resp.errors]
        );
    });
});
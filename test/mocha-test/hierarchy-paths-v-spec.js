/* global expect:false, Backbone:false */

var HierarchyPathsModel = require('../dist/pages/policy/models/hierarchy-paths-m');
var HierarchyPathsView  = require('../dist/pages/policy/views/hierarchy-paths-v');

describe('Hierarchy Paths View (pages/policy/views/hierarchy-paths-v.js)', function() {

    var model = new HierarchyPathsModel({
        id    : 123,
        paths : [
            {
                producers : [
                    {
                        producer : {
                            id          : 456,
                            fullName    : 'Darth Vader',
                            lexicalName : 'Vader, Darth'
                        }
                    }, {
                        producer : {
                            id          : 789,
                            fullName    : 'Luke Skywalker',
                            lexicalName : 'Skywalker, Luke'
                        }
                    }, {
                        producer : {
                            id          : 855,
                            fullName    : 'Phil Johnson',
                            lexicalName : 'Johnson, Phil'
                        }
                    }
                ]
            }, {
                producers : [
                    {
                        producer : {
                            id          : 101,
                            fullName    : 'James T Kirk',
                            lexicalName : 'Kirk, James T'
                        }
                    }
                ]
            }
        ]
    });
    var view;

    beforeEach(function() {
        view = new HierarchyPathsView({
            model : model
        });

        view.render();
    });

    afterEach(function() {
        view.destroy();
    });

    it('exists', function() {
        expect(HierarchyPathsView).to.exist;
    });

    describe('Render Tests', function() {

        it('renders the hierarchy path(s) in the model', function() {

            // the UL elements (expecting 2)
            var unorderedLists = view.$el.find('ul');

            // LI elements (expecting 3)
            var firstList      = view.$el.find('ul:eq(0) li');

            // LI elements (expecting 1)
            var secondList     = view.$el.find('ul:eq(1) li');

            // ID on first A (expecting 456)
            var firstId        = Backbone.$(
                view.$el.find('ul:eq(0) li:eq(0) a')
            ).data('producerid');

            // text in last LI (no A tag in there, expecting only name)
            var lastItemText   = view.$el.find('ul:eq(1) li:eq(0)')[0].innerHTML.trim();

            expect(unorderedLists).to.have.length(2);
            expect(firstList).to.have.length(3);
            expect(secondList).to.have.length(1);
            expect(firstId).to.equal(456);
            expect(lastItemText).to.equal('James T Kirk');
        });

        describe('HREF attribute value', function () {
           
            it('Each item should include "producerOrgId"', function () {
                var href = view.$el.find('ul:eq(0) a:eq(0)').attr('href');
                expect(href).to.contain('?producerOrgId=456');

                href = view.$el.find('ul:eq(0) a:eq(1)').attr('href');
                expect(href).to.contain('?producerOrgId=789');
            });

            it('Last item should not be a link', function () {
                var anchorElement = view.$el.find('ul:eq(0) li').last().find('a');
                expect(anchorElement).to.be.lengthOf(0);
            });
        });
    });

    describe('_hierarchyChange method', function() {

        it('exists as a function', function() {
            expect(view._hierarchyChange).to.exist.and.be.a('function');
        });

        it('missing producerId gets logged', function() {
            var debugModuleSpy = this.sinon.spy(view.debugModule, 'error');
            var link           = view.$el.find('ul:eq(0) li:eq(0) a.oa-js-nav');

            // clear out the value for data attribute "producerId"
            link.data('producerid', '');
            link.click();

            expect(debugModuleSpy).to.have.been.calledOnce;
        });
    });
});

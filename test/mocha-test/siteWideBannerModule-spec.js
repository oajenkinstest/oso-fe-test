/* global expect:false, Backbone:false */
/* eslint-disable no-new */

// load behaviors
require('../dist/modules/behaviors/index');

var SiteWideBannerModule = require('../dist/modules/siteWideBanner/siteWideBannerModule');

describe('Site-Wide Banner Module (modules/siteWideBanner/siteWideBannerModule.js)', function () {

    describe('initialize method', function () {
        var module;
        var testRegion;

        beforeEach(function () {
            testRegion = new Backbone.Marionette.Region({
                el : document.createElement('div')
            });
        });

        afterEach(function () {
            testRegion.destroy();
            module = null;
        });

        describe('Should throw an error', function () {
            var expectedError;
            var fn;
            var options;

            it('when options parameter is not passed', function () {
                fn = function () {
                    new SiteWideBannerModule();
                };

                expectedError = SiteWideBannerModule.prototype.errors.options;
                expect(fn).to.throw(expectedError);
            });

            it('when options parameter is not an object', function () {
                fn = function () {
                    options = 'I am not an object';
                    new SiteWideBannerModule(options);
                };
                expectedError = SiteWideBannerModule.prototype.errors.options;

                expect(fn).to.throw(expectedError);
            });

            it('when options object passed does not have a "region" key', function () {
                fn = function () {
                    options = { notRegion : 'Not a Region' };
                    new SiteWideBannerModule(options);
                };

                expectedError = SiteWideBannerModule.prototype.errors.parentRegion;

                expect(fn).to.throw(expectedError);
            });
        });     // Should throw an error

        describe('Should set the correct wcmPath', function () {
            var expectedPath;
            var userChannel;

            beforeEach(function () {
                userChannel = Backbone.Radio.channel('user');
                userChannel.reply('hasCapability', function(capability){
                    return capability === 'Home_Office';
                });

                module = new SiteWideBannerModule({ region : testRegion });
            });

            afterEach(function () {
                userChannel.stopListening();
                userChannel = null;
            });

            it('"Home_Office" capability should get correct content', function () {
                expectedPath = module.wcmPathByCapability.homeOffice;
                userChannel  = Backbone.Radio.channel('user');
                userChannel.reply('hasCapability', function (){
                    return true;
                });

                module = new SiteWideBannerModule({ region : testRegion });

                expect(module.wcmPath).to.equal(expectedPath);
            });

            it('if the user does not have "Home_Office" capability', function () {
                expectedPath = module.wcmPathByCapability.producer;
                userChannel  = Backbone.Radio.channel('user');
                userChannel.reply('hasCapability', function (){
                    return false;
                });

                module = new SiteWideBannerModule({ region : testRegion });

                expect(module.wcmPath).to.equal(expectedPath);
            });

        });

        it('should call "show" method on the region', function () {
            var showSpy = this.sinon.spy(testRegion, 'show');

            module = new SiteWideBannerModule({ region : testRegion });

            expect(showSpy).to.have.been.calledOnce;
            showSpy.restore();
        });

        describe ('event listener "collapseBanner" ', function () {
            var collapseBannerSpy;

            beforeEach (function () {
                collapseBannerSpy  = this.sinon.spy(
                    SiteWideBannerModule.prototype, '_collapseBanner'
                );
                module = new SiteWideBannerModule({ region : testRegion });
            });

            afterEach (function () {
                collapseBannerSpy.restore();
                module.destroy();
            });

            it('should exist', function () {
                expect(module._events.collapseBanner).to.exist;
            });

            it('should invoke "_collapseBanner" method', function () {
                module.trigger('collapseBanner', true);
                expect(collapseBannerSpy).to.have.been.calledWith(true);
            }); // event listener "collapseBanner"
        });

    });     // initialize method
});

/* global expect:false */


var Policy1035ExchangeHistory =
    require('../dist/pages/policy/models/parsers/policy-1035-exchange-history');

var exchangeHistoryData = [
    {
        otherCarrierName         : 'A USAA',
        otherCarrierPolicyNumber : '30900405850',
        amountReceived           : 'USD 0.00',
        comments                 : [
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 8774357099'
            },
            {
                date    : '2016-06-06',
                comment : 'Spoke with Melissa. A check was mailed.'
            },
            {
                date    : '2016-06-14',
                comment : 'Direct transfer. No cost basis required.'
            }
        ]
    },
    {
        otherCarrierName         : 'USAA Prudential',
        amountReceived           : 'USD 20000.00',
        otherCarrierPolicyNumber : '40902595727',
        comments                 : [
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 4544775454'
            }
        ]
    },
    {
        otherCarrierName         : 'AB USAA',
        amountReceived           : 'USD 5000.00',
        otherCarrierPolicyNumber : '30900405860',
        comments                 : [
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 8774357099'
            }
        ]
    }
];

var expectedData = [
    {
        otherCarrierName         : 'A USAA',
        amountReceived           : 'USD 0.00',
        otherCarrierPolicyNumber : '30900405850',
        comments                 : [
            {
                date    : '2016-06-14',
                comment : 'Direct transfer. No cost basis required.'
            },
            {
                date    : '2016-06-06',
                comment : 'Spoke with Melissa. A check was mailed.'
            },
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 8774357099'
            }
        ]
    },
    {
        otherCarrierName         : 'AB USAA',
        amountReceived           : 'USD 5000.00',
        otherCarrierPolicyNumber : '30900405860',
        comments                 : [
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 8774357099'
            }
        ]
    },
    {
        otherCarrierName         : 'USAA Prudential',
        amountReceived           : 'USD 20000.00',
        otherCarrierPolicyNumber : '40902595727',
        comments                 : [
            {
                date    : '2016-06-02',
                comment : 'FAXED PACKET TO 4544775454'
            }
        ]
    }
];

var expectedCounts = {
    pending  : 1,
    received : 2
};

describe('1035 Exchange History Parser ' +
    '(pages/policy/models/parsers/policy-1035-exchange-history.js)', function () {

    it('exists', function () {
        expect(Policy1035ExchangeHistory).to.exist;
    });

    describe('sort method', function () {

        it('exists', function () {
            expect(Policy1035ExchangeHistory.prototype.sort).to.exist;
        });

        it('returns sorted transfer and comments data', function () {
            var parser     = new Policy1035ExchangeHistory(exchangeHistoryData);
            var sortedData = parser.sort();

            expect(sortedData[0].comments).to.deep.equal(expectedData[0].comments);
            expect(sortedData).to.deep.equal(expectedData);
        });

    }); // sort method

    describe('getStatusCounts method', function () {

        it('exists as a function', function () {
            expect(Policy1035ExchangeHistory.prototype.getStatusCounts).to.exist
                .and.be.a('function');
        });

        it('returns counts of pending and received histories', function () {
            var parser = new Policy1035ExchangeHistory(exchangeHistoryData);
            var result = parser.getStatusCounts();

            expect(result).to.deep.equal(expectedCounts);
        });

    }); // getStatusCounts method

});

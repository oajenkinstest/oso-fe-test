/* global expect:false, $:false, Backbone:false, describe:false, sinon:false  */

var WcmToutView = require('../dist/pages/home/views/wcm-tout-v');

describe('WCM Tout View (pages/home/views/wcm-tout-v.js)', function () {
    var ajaxStub;
    var view;
    var wcmToutContent = '<div class="center">Lame Tout<a href="http://www.foo.com">foo</a></div>';

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        ajaxStub =this.sinon.stub($, 'ajax').yieldsTo('success', wcmToutContent);
        view = new WcmToutView({
            wcmPath : 'touts/homepage/education-and-practice-development/lame'
        });
    });

    after(function () {
        ajaxStub.restore();
        view.destroy();
    });

    describe('Initialization', function () {
        var expectedError = WcmToutView.prototype.errors.invalidOptions;

        it('should throw an error if options passed are not an object', function () {
            var fn = function () {
                new WcmToutView();  // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedError);
        });

        it('should throw an error if options passed do not contain "wcmPath"', function () {
            var fn = function () {
                new WcmToutView({   // eslint-disable-line no-new
                    foo : 'bar'
                });
            };

            expect(fn).to.throw(expectedError);
        });

        it('should throw an error if "wcmPath" is not a string', function () {
            var fn = function () {
                new WcmToutView({   // eslint-disable-line no-new
                    wcmPath : 42
                });
            };

            expect(fn).to.throw(expectedError);
        });

    }); // Initialization

    describe('onRender method', function () {

        it('should hide the spinner', function () {
            var spinnerChannel    = Backbone.Radio.channel('spinner');
            var spinnerChannelSpy = this.sinon.spy();

            spinnerChannel.on('hide', spinnerChannelSpy);
            view.render();

            expect(spinnerChannelSpy).to.have.been.called;

            spinnerChannel.stopListening();
        });

        describe('When "showDisabledIcon" is set on view', function () {

            after(function () {
                delete view.showDisabledIcon;
            });

            it('should add the "disabled" CSS class to the root DIV tag', function () {
                expect(view.ui.root.hasClass('disabled')).to.be.false;

                view.showDisabledIcon = true;
                view.render();

                expect(view.ui.root.hasClass('disabled')).to.be.true;

                delete view.showDisabledIcon;
            });

            it('should remove the "href" attribute from links', function () {
                view.render();
                expect(view.ui.links.attr('href')).to.exist;

                view.showDisabledIcon = true;
                view.render();

                expect(view.ui.links.attr('href')).to.be.undefined;
            });

        });



    }); // onRender method

    describe('_updateLinks method', function () {
        var buildHrefUrlSpy;
        beforeEach(function () {
            buildHrefUrlSpy            = this.sinon.spy(view.utils, 'buildHrefURL');
        });

        afterEach(function () {
            buildHrefUrlSpy.restore();
        });

        it('should call `utils.convertWcmLinks`', function () {
            var convertWcmLinksSpy = this.sinon.spy(view.utils, 'convertWcmLinks');
            view.render();

            expect(convertWcmLinksSpy).to.have.been.called;
        });

        it('should call utils.buildHrefURL() method if "buildHrefUrl" is set on view', function () {
            view.buildHrefUrl = true;
            view.render();

            expect(buildHrefUrlSpy).to.have.been.called;
        });

        it('should not call utils.buildHrefURL() method if "buildHrefUrl" is not ' +
            'set on view', function () {
            delete view.buildHrefUrl;
            view.render();

            expect(buildHrefUrlSpy).not.to.have.been.called;
        });

    }); // _updateLinks method

    describe('_handleWcmError method', function () {
        var analyticsChannel;
        var analyticsChannelSpy;
        var spinnerChannel;
        var spinnerChannelSpy;
        var statusCode = 404;

        before(function () {
            analyticsChannelSpy = this.sinon.spy();
            spinnerChannelSpy   = this.sinon.spy();
            analyticsChannel    = Backbone.Radio.channel('analytics');
            spinnerChannel      = Backbone.Radio.channel('spinner');

            analyticsChannel.on('trackException', analyticsChannelSpy);
            spinnerChannel.on('hide', spinnerChannelSpy);

            view._handleWcmError(view.model, { status : statusCode });
        });

        after(function () {
            analyticsChannel.stopListening();
            spinnerChannel.stopListening();
            view.destroy();
        });

        it('should hide the spinner', function () {
            expect(spinnerChannelSpy).to.have.been.calledOnce;
        });

        it('should send the error to analytics', function () {
            expect(analyticsChannelSpy).to.have.been.calledOnce;
        });

        it('error message sent to analytics should contain the response code', function () {
            var messageToAnalytics = analyticsChannelSpy.getCall(0).args[0].message;

            expect(messageToAnalytics).to.contain(statusCode);
        });

    }); // _handleWcmError method
});
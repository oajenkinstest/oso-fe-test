/* global expect:false, $:false, _:false, expect:false */

var parser = 
        require('../dist/pages/policy/models/parsers/policy-requirements');
var helpers = require('./helpers/helpers');

describe('Policy Detail Model parser - Policy Requirements ' +
    '(pages/policy/models/parsers/policy-requirements.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

    describe('shouldShowRequirements method', function () {
        it('exists as a function', function () {
            expect(parser.shouldShowRequirements).to.exist.and.be.a('function');
        });

        it('returns true for a pending policy', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            expect(parser.shouldShowRequirements(policyDataCopy)).to.equal(true);
        });

        it('returns false for an active policy with no pending requirements', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    { 'status'          : 'Complete' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };
            expect(parser.shouldShowRequirements(policyDataCopy)).to.equal(false);
        });

        it('returns true for an active policy with pending requirements', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'Pending' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };
            expect(parser.shouldShowRequirements(policyDataCopy)).to.equal(true);
        });

        it('does not throw an error if there is no requirements data', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            delete policyDataCopy.requirements;

            var fn = function () {
                parser.shouldShowRequirements(policyDataCopy);
            };

            expect(fn).not.to.throw();
        });
    }); // shouldShowRequirements method

    describe('shouldShowReceivedRequirements method', function () {
        it('exists as a function', function () {
            expect(parser.shouldShowReceivedRequirements).to.exist.and.be.a('function');
        });

        it('returns true if acord status is "Proposed", even if more than 92 days beyond paid date' +
            'and no pending requirements', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            // get date which is 93 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 93);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'NotPending' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };

            expect(parser.shouldShowReceivedRequirements(policyDataCopy)).to.equal(true);
        });

        it('returns false if more than 92 days beyond paid date even with ' +
            'pending requirements', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            // get date which is 93 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 93);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'Pending' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };

            expect(parser.shouldShowReceivedRequirements(policyDataCopy)).to.equal(false);
        });

        it('returns false if there are no pending requirements even within the 92 days ' +
            'beyond paid date', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            // get date which is 92 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 92);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    { 'status'          : 'Complete' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };
            
            expect(parser.shouldShowReceivedRequirements(policyDataCopy)).to.equal(false);
        });

        it('returns true if within 92 days of paid date and has ' +
            'pending requirements', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            // get date which is 92 days behind
            policyDataCopy.application.paid = helpers.viewHelpers.getISODate('-', 92);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'Pending' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };

            expect(parser.shouldShowReceivedRequirements(policyDataCopy)).to.equal(true);
        });

        it('returns true if the policy does not have a paid date', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            delete policyDataCopy.application.paid;

            expect(parser.shouldShowReceivedRequirements(policyDataCopy)).to.equal(true);
        });
    }); // shouldShowReceivedRequirements method

    describe('deleteReceivedRequirements method', function () {
        it('exists as a function', function () {
            expect(parser.deleteReceivedRequirements).to.exist.and.be.a('function');
        });

        it('deletes received requirements from the input data', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            policyDataCopy.requirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'Pending' }
                ],
                'received'              : [
                    { 'status'          : 'Waived' },
                    { 'status'          : 'Complete' }
                ]
            };
            var expectedRequirements = {
                'neededForUnderwriting': [
                    { 'status': 'something' },
                    { 'status': 'something' }
                ],
                'obtainAtDelivery'      : [
                    {
                        'new': true,
                        'status'          : 'Pending' }
                ]
            };
            var actualRequirements = parser.deleteReceivedRequirements(policyDataCopy).requirements;
            expect(actualRequirements).to.deep.equal(expectedRequirements);
        });

        it('does not throw an error if there is no requirements data', function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
            delete policyDataCopy.requirements;

            var fn = function () {
                parser.deleteReceivedRequirements(policyDataCopy);
            };

            expect(fn).not.to.throw();
        });
    }); // deleteReceivedRequirements method

    describe( 'setCustomerFirstNamePolicyRequirements method',function () {

        it('exists as a function',function () {
            expect(parser.setCustomerFirstNamePolicyRequirements).to.exist.and.be.a('function');
        });

        it('should return requirements object with \'Insured Name\' '+
            'if person ID exist in customers data',function () {
            var policyRequirementsData  = parser.setCustomerFirstNamePolicyRequirements(
                helpers.policyData.pendingPolicyDetail
            ).requirements;

            expect(policyRequirementsData.neededForUnderwriting[0].insuredName)
                .to.be.defined;
            expect(policyRequirementsData.neededForUnderwriting[0].insuredName)
                .to.equal('Joe The Policy holder');
        });

        it('\'Insured Name\' should be empty '+
                'if person ID does not exist in customers data', function () {
            var policyRequirementsData  = parser.setCustomerFirstNamePolicyRequirements(
                {
                    customers:{
                        452: {
                            firstName: 'Juan',
                            fullName: 'Juan Dsouza'
                        }
                    },
                    requirements: {
                        neededForUnderwriting:[
                            {
                                customerId: 456
                            }
                        ]
                    }
                }
            ).requirements;

            expect(policyRequirementsData.neededForUnderwriting[0].insuredName)
                .to.be.empty;
        });

    }); // setCustomerFirstNamePolicyRequirements method

    describe('setRequirementsCounts method', function () {
        var testRequirements;

        before(function () {
            testRequirements = {
                obtainAtDelivery : [
                    {
                        customerId      : 25074156,
                        requirementName : 'Proxy-Delivery Receipt',
                        status          : 'Pending',
                        dateCreated     : '2016-03-15T12:50:00Z',
                        dateReceived    : null,
                        neededToPay     : true,
                        comments        : 'Collect on delivery.',
                        new             : false
                    },
                    {
                        customerId      : 25074155,
                        requirementName : 'Amendment',
                        status          : 'Complete',
                        dateCreated     : '2016-03-16T12:50:00Z',
                        dateReceived    : null,
                        neededToPay     : false,
                        comments        : 'Collect on delivery.',
                        new             : false
                    }
                ],
                received         : [
                    {
                        customerId      : 25498372,
                        requirementName : 'Temporary Insurance Agreement',
                        status          : 'Waived',
                        dateCreated     : '2016-02-10T12:50:00Z',
                        dateReceived    : '2016-03-05T12:50:00Z',
                        neededToPay     : null,
                        comments        : 'New form required to be completed and signed',
                        new             : false
                    },
                    {
                        customerId      : 25498373,
                        requirementName : 'Balance of Premium',
                        status          : 'Pending',
                        dateCreated     : '2016-02-10T12:50:00Z',
                        dateReceived    : '2016-03-05T12:50:00Z',
                        neededToPay     : null,
                        comments        : 'I-20833 (ICC)',
                        new             : false
                    },
                    {
                        customerId      : 25074155,
                        requirementName : 'Bank Draft Authorization Form',
                        status          : 'In House',
                        dateCreated     : '2016-02-10T12:50:00Z',
                        dateReceived    : '2016-03-25T12:50:00Z',
                        neededToPay     : null,
                        comments        : '$31.16 initial monthly premium due.',
                        new             : false
                    }
                ]
            };
        });

        after(function () {
            testRequirements = null;
        });

        beforeEach(function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        });

        afterEach(function () {
            policyDataCopy = null;
        });

        it('exists as a function', function () {
            expect(parser.setRequirementsCounts).to.exist.and.be.a('function');
        });

        describe('when requirements are NOT passed in', function () {

            it('should not modify response', function () {
                var dataBeforeMethod = policyDataCopy;
                var dataAfterMethod  = parser.setRequirementsCounts(policyDataCopy);

                expect(dataBeforeMethod).to.deep.equal(dataAfterMethod);
            });
        });

        describe('when requirements are passed in', function () {

            it('should add a "pendingCount" field to requirements', function () {
                parser.setRequirementsCounts(policyDataCopy);
                expect(policyDataCopy.requirements.pendingCount).to.not.be.undefined;
            });

            it('should add a "nonPendingCount" field to requirements', function () {
                parser.setRequirementsCounts(policyDataCopy);
                expect(policyDataCopy.requirements.nonPendingCount).to.not.be.undefined;
            });

            it('"pendingCount" value should be total of all requirements with status ' +
                'of "Pending"', function () {
                policyDataCopy.requirements = testRequirements;
                var result = parser.setRequirementsCounts(policyDataCopy);

                expect(result.requirements.pendingCount).to.equal(2);
            });

            it('"nonPendingCount" should be a total of all requirements which do NOT ' +
                'have a status of "Pending"', function () {
                policyDataCopy.requirements = testRequirements;
                var result = parser.setRequirementsCounts(policyDataCopy);

                expect(result.requirements.nonPendingCount).to.equal(3);
            });

        });
    }); // setRequirementsCounts method

    describe ('sortPolicyRequirements method', function () {

        it('exists as a function', function () {
            expect(parser.sortPolicyRequirements).to.exist.and.be.a('function');
        });

        it('should sort Received Requirements data', function () {
            var receivedRequirementsData  = parser.sortPolicyRequirements(
                {
                    requirements: {
                        received:[
                            {
                                customerId      : '123',
                                requirementName : 'Additional Form Required to Mail Policy',
                                status          : 'Waived',
                                dateCreated     : '2016-02-10T12:50:00Z',
                                dateReceived    : '2016-04-18T12:50:00Z',
                            },
                            {
                                customerId      : '154',
                                requirementName : 'A for 04-10-2016',
                                status          : 'In House',
                                dateCreated     : '2016-04-09T12:50:00Z',
                                dateReceived    : '2016-04-10T12:50:00Z',
                            },
                            {
                                customerId      : '451',
                                requirementName : 'B for 04-25-2016',
                                status          : 'In House',
                                dateCreated     : '2016-04-26T12:50:00Z',
                                dateReceived    : '2016-04-25T12:50:00Z',
                            },
                            {
                                customerId      : '487',
                                requirementName : 'A for 04-25-2016',
                                status          : 'In House',
                                dateCreated     : '2016-04-26T12:50:00Z',
                                dateReceived    : '2016-04-25T12:50:00Z',
                            },
                            {
                                customerId      : '355',
                                requirementName : 'Additional Form Required to Mail Policy',
                                status          : 'Complete',
                                dateCreated     : '2016-02-10T12:50:00Z',
                                dateReceived    : '2016-02-15T12:50:00Z',
                            }
                        ]
                    }
                }
            ).requirements.received;

            //row 1
            expect(receivedRequirementsData[0].status).to.equal('In House');
            expect(receivedRequirementsData[0].dateReceived).to.equal('2016-04-25T12:50:00Z');
            expect(receivedRequirementsData[0].requirementName).to.equal('A for 04-25-2016');

            //row 2
            expect(receivedRequirementsData[1].status).to.equal('In House');
            expect(receivedRequirementsData[1].dateReceived).to.equal('2016-04-25T12:50:00Z');
            expect(receivedRequirementsData[1].requirementName).to.equal('B for 04-25-2016');

            //row 3
            expect(receivedRequirementsData[2].status).to.equal('In House');
            expect(receivedRequirementsData[2].dateReceived).to.equal('2016-04-10T12:50:00Z');
            expect(receivedRequirementsData[2].requirementName).to.equal('A for 04-10-2016');

            //row 4
            expect(receivedRequirementsData[3].status).to.equal('Complete');

            //row 5
            expect(receivedRequirementsData[4].status).to.equal('Waived');
        });

        it('should sort Requirements list using key', function () {
            var requirementsList  = parser.sortPolicyRequirements(
                {
                    requirements: {
                        received:[
                            {
                                customerId      : '123',
                                requirementName : 'Additional Form Required to Mail Policy',
                                status          : 'Waived',
                                dateCreated     : '2016-02-10T12:50:00Z',
                                dateReceived    : '2016-04-18T12:50:00Z',
                            }
                        ],
                        neededForUnderwriting: [],
                        obtainAtDelivery: []
                    }
                }
            ).requirements;

            //sort order will be 
            var keySortOrder = [
                'neededForUnderwriting',
                'obtainAtDelivery',
                'received'
            ];

            var order = 0;
            _.each(requirementsList ,function (value, key){
                expect(key).to.equal(keySortOrder[order]);
                order++;
            });
        });

    }); // sortPolicyRequirements method
});

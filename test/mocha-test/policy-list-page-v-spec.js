/* global describe:false, Backbone:false, _:false, expect:false */


// set up framework
var helpers = require('./helpers/helpers');

// require('../dist/vendor');
require('../dist/modules/behaviors/index');

var PolicyListPageView = require('../dist/pages/policy/views/policy-list-page-v');


describe('Policy Page View Tests (policy-list-page-v.js)', function () {

    before(function() {
        helpers.viewHelpers.startBBhistory();
    });

    after(function() {
        helpers.viewHelpers.stopBBhistory();
    });

    describe('Basic Tests', function() {
        it('The view should exist', function() {
            expect(PolicyListPageView).to.exist;
        });
    });

    // These tests started failing after the jQuery 3 upgrade. We need to find a way to stub the
    // checkpoint.writeCheckpoint() function that is called by PolicyListPageView.initialize() to
    // avoid errors in these tests. --RKC Jan 29, 2018
    describe.skip('Initialization Tests', function() {
        var view;
        var root;

        beforeEach(function() {
            root = helpers.viewHelpers.createRootView();
            root.render();
        });

        afterEach(function() {
            view.destroy();
            root.destroy();
        });

        it('default rendering', function() {
            view = new PolicyListPageView();
            root.showChildView('contentRegion', view);
            expect(view.pendingPolicyListView.currentTab).to.equal('search');
        });

        it('understands the subpages', function() {
            view = new PolicyListPageView({
                stateObj : {
                    subpages : ['paid']
                }
            });
            root.showChildView('contentRegion', view);
            expect(view.pendingPolicyListView.currentTab).to.equal('paid');
        });

        it('default tab should be "Pending" if producerId exist in stateObj', function() {
            view = new PolicyListPageView({
                stateObj : {
                    producerId : 100001
                }
            });
            root.showChildView('contentRegion', view);
            expect(view.pendingPolicyListView.currentTab).to.equal('pending');
        });

        it('default tab should be "Pending" if producerOrgId exist in stateObj', function() {
            view = new PolicyListPageView({
                stateObj : {
                    producerOrgId : 200001
                }
            });
            root.showChildView('contentRegion', view);
            expect(view.pendingPolicyListView.currentTab).to.equal('pending');
        });

        it('subpages in stateObj should be default, even producerOrgId exist', function() {
            view = new PolicyListPageView({
                stateObj : {
                    subpages : ['paid'],
                    producerOrgId : 200001
                }
            });
            root.showChildView('contentRegion', view);
            expect(view.pendingPolicyListView.currentTab).to.equal('paid');
        });

    });

    describe('_setCheckpoint tests', function() {

        var view;
        var checkpointStub;

        beforeEach(function () {
            view = new PolicyListPageView();
            checkpointStub = this.sinon.stub(view.checkpoint, 'writeCheckpoint');
        });

        afterEach(function () {
            checkpointStub.restore();
            view.destroy();
        });

        it('Simply calls the writeCheckpoint', function () {
            var state = {
                monkey: 'George',
                owner: 'Man in the Yellow Hat',
                subpages : ['pending']
            };

            view._setCheckpoint(state);
            expect(checkpointStub).to.have.been.calledWith(state, true);
        });

        it('handles a subpage in the state', function () {
            var state = {
                monkey  : 'George',
                owner   : 'Man in the Yellow Hat'
            };

            var expectedState = {
                monkey   : 'George',
                owner    : 'Man in the Yellow Hat',
                subpages : ['search']
            };

            view._setCheckpoint(state);
            expect(checkpointStub).to.have.been.calledWith(expectedState, true);
        });
    });

    describe('_setCheckpoint tests with producerId', function() {

        var view;
        var checkpointStub;

        beforeEach(function () {
            view = new PolicyListPageView({
                stateObj : {
                    producerId : 12345
                }
            });
            checkpointStub = this.sinon.stub(view.checkpoint, 'writeCheckpoint');
        });

        afterEach(function () {
            checkpointStub.restore();
            view.destroy();
        });

        it('Adds the producerId to the checkpoint, if the page was initialized with it',
            function() {
                var state = {
                    monkey : 'George',
                    owner  : 'Man in the Yellow Hat',
                    subpages    : ['pending']
                };
                var expectedState = _.extend({producerId : 12345}, state);

                view._setCheckpoint(state);
                expect(checkpointStub).to.have.been.calledWith(expectedState, true);
            }
        );
    });

    describe('_showPolicy tests', function() {
        var listener;
        var navStub;
        var view;

        beforeEach(function() {
            navStub = this.sinon.stub();
            view = new PolicyListPageView();
            listener = new Backbone.Marionette.Object();
            listener.listenTo(view, 'nav', navStub);
        });

        afterEach(function() {
            listener.stopListening();
            view.destroy();
        });

        it('triggers a "nav" event', function() {
            var policyId = '918273645';
            view._showPolicy(policyId);
            expect(navStub).to.have.been.calledWith(
                '#policy-detail?policyId=' + policyId
            );
        });
    });

    // These tests started failing after the jQuery 3 upgrade. We need to find a way to stub the
    // checkpoint.writeCheckpoint() function that is called by PolicyListPageView.initialize() to
    // avoid errors in these tests. --RKC Jan 29, 2018
    describe.skip('_setProducerInfo tests', function() {
        var view;
        var root;

        beforeEach(function() {
            root = helpers.viewHelpers.createRootView();
            root.render();
        });

        afterEach(function() {
            view.destroy();
            root.destroy();
        });

        it('Renders the info in the producerInfoRegion', function() {
            view = new PolicyListPageView({
                stateObj : {
                    producerId : 12345
                }
            });
            root.showChildView('contentRegion', view);
            var region = view.$el.find('#producer-info-region');

            expect(region).to.exist.and.have.length.above(0);
            expect(Backbone.$(region[0]).hasClass('hidden')).to.equal(true);

            view._setProducerInfo({ producerName: 'Jonas Grumby'});
            expect(Backbone.$(region[0]).hasClass('hidden')).to.equal(false);
        });
    });

    // These tests started failing after the jQuery 3 upgrade. We need to find a way to stub the
    // checkpoint.writeCheckpoint() function that is called by PolicyListPageView.initialize() to
    // avoid errors in these tests. --RKC Jan 29, 2018
    describe.skip('_setOrgProducerInfo tests', function() {
        var view;
        var root;

        beforeEach(function() {
            root = helpers.viewHelpers.createRootView();
            root.render();
        });

        afterEach(function() {
            view.destroy();
            root.destroy();
        });

        it('Renders the info in the orgs producer info region', function() {
            view = new PolicyListPageView({
                stateObj : {
                    producerOrgId : 12345
                }
            });
            root.showChildView('contentRegion', view);

            var region = view.$el.find('#orgs-producer-info-region');

            expect(region).to.exist.and.have.length.above(0);
            expect(Backbone.$(region[0]).hasClass('hidden')).to.equal(true);

            view._setOrgProducerInfo({ producerName: 'Jonas Grumby'});
            expect(Backbone.$(region[0]).hasClass('hidden')).to.equal(false);
        });
    });
});

/* global expect:false */

var errorHelper = require('../dist/utils/error-helper');

describe('Error Helper Utils (utils/error-helper.js)', function() {

    it('module exists', function() {
        expect(errorHelper).to.exist;
    });

    describe('shouldDisplayGlobalError method', function() {
        
        it('function exists', function() {
            expect(errorHelper.shouldDisplayGlobalError).to.exist.and.be.a('function');
        });

        it('returns true for an unhandled endpoint/status', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(
                'http://api.oneamerica.com/oso/secure/rest/monkeys', 501
            );
            expect(shouldDisplay).to.equal(true);
        });

        it('returns true for a handled endpoint, but unhandled status', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(
                'http://api.oneamerica.com/oso/secure/rest/policies', 501
            );
            expect(shouldDisplay).to.equal(true);
        });

        it('returns false for a handled endpoint/status', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(
                'http://api.oneamerica.com/oso/secure/rest/policies', 500
            );
            expect(shouldDisplay).to.equal(false);
        });

        it('returns false for an endpoint with an empty array', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(
                'http://api.oneamerica.com/oso/secure/rest/ui/log', 500
            );
            expect(shouldDisplay).to.equal(false);
        });
        
        it('returns true if url not specified', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(null, 123);
            expect(shouldDisplay).to.equal(true);
        });
        
        it('returns true if status not specified', function() {
            var shouldDisplay = errorHelper.shouldDisplayGlobalError(
                'http://api.oneamerica.com'
            );
            expect(shouldDisplay).to.equal(true);
        });
    });


    describe('createAlert method', function() {
        
        var message = 'A simple message';
        var messageWithMarkup = '<p>' + message + '</p>';

        it('function exists', function() {
            expect(errorHelper.createAlert).to.exist.and.be.a('function');
        });

        describe('checks for beginning <p> tag', function () {

            it('adds surrounds <p> tags if missing from message text', function () {
                var alert = errorHelper.createAlert(message, 'success');
                expect(alert.message).to.equal(messageWithMarkup);
            });

            it('passes the message directly if already using leading <p> tag', function () {
                var alert = errorHelper.createAlert(messageWithMarkup, 'success');
                expect(alert.message).to.equal(messageWithMarkup);
            });
        });

        it('creates a success alert', function() {
            var alert = errorHelper.createAlert(message, 'success');
            expect(alert).to.not.be.null;
            expect(alert.message).to.equal(messageWithMarkup);
            expect(alert.divClass).to.equal('alert-success');
            expect(alert.icon).to.equal('fa-check');
        });

        it('creates an info alert', function() {
            var alert = errorHelper.createAlert(message, 'info');
            expect(alert).to.not.be.null;
            expect(alert.message).to.equal(messageWithMarkup);
            expect(alert.divClass).to.equal('alert-info');
            expect(alert.icon).to.equal('fa-exclamation');
        });

        it('creates a warning alert', function() {
            var alert = errorHelper.createAlert(message, 'warning');
            expect(alert).to.not.be.null;
            expect(alert.message).to.equal(messageWithMarkup);
            expect(alert.divClass).to.equal('alert-warning');
            expect(alert.icon).to.equal('fa-bullhorn');
        });

        it('creates an error alert', function() {
            var alert = errorHelper.createAlert(message, 'error');
            expect(alert).to.not.be.null;
            expect(alert.message).to.equal(messageWithMarkup);
            expect(alert.divClass).to.equal('alert-danger');
            expect(alert.icon).to.equal('fa-times');
        });

        it('throws an error if the messageType is invalid', function() {
            var fn = function() {
                errorHelper.createAlert(message, 'Monkey');
            };
            expect(fn).to.throw(errorHelper.errors.invalidType);
        });

        it('throws an error if the messageText parameter is not defined', function() {
            var fn = function() {
                errorHelper.createAlert(null, 'Monkey');
            };
            expect(fn).to.throw(errorHelper.errors.typeAndTextRequired);
        });

        it('throws an error if the messageType parameter is not defined', function() {
            var fn = function() {
                errorHelper.createAlert(message);
            };
            expect(fn).to.throw(errorHelper.errors.typeAndTextRequired);
        });
    });
});

/* global expect:false, Backbone:false */

var helpers = require('./helpers/helpers');

var PolicyManagerView = require('../dist/pages/policy/views/policy-manager-page-v');

describe('Policy Manager Page View ' +
    '(pages/policy/views/policy-manager-page-v.js)', function () {

    before(function () {
        helpers.viewHelpers.startBBhistory();
    });

    after(function () {
        helpers.viewHelpers.stopBBhistory();
    });

    describe('basic tests', function () {

        var view;

        before(function () {
            document.location.hash='#pending-policy-manager';
            view = new PolicyManagerView();
        });

        after(function () {
            view.destroy();
        });

        it('PolicyManagerView object should exist', function () {
            expect(PolicyManagerView).to.exist;
        });

        it('"organizationRegion" region should exist', function () {
            expect(view.getRegion('organizationRegion')).not.to.be.undefined;
        });

        it('"orgPolicyListRegion" region should exist', function () {
            expect(view.getRegion('orgPolicyListRegion')).not.to.be.undefined;
        });

        it('"orgProducerInfoRegion" region should exist', function () {
            expect(view.getRegion('orgProducerInfoRegion')).not.to.be.undefined;
        });
        it('"producerInfoRegion" region should exist', function () {
            expect(view.getRegion('producerInfoRegion')).not.to.be.undefined;
        });

        it('"policyListRegion" region should exist', function () {
            expect(view.getRegion('policyListRegion')).not.to.be.undefined;
        });
    }); // basic tests

    // Trying to track down text crash about '_location' of null
    // Caused by document.location.hash?
    describe.skip('initialization tests', function () {
        var view;
        var root;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            root = helpers.viewHelpers.createRootView();
            root.render();
            view = new PolicyManagerView();
        });

        afterEach(function () {
            view.destroy();
        });

        it('Defaults to the searchTab by default', function () {
            root.showChildView('contentRegion', view);
            expect(view.ui.searchTab.hasClass('active')).to.equal(true);
        });

        it('Org Pending Policies view should have property "hierarchy"', function () {
            root.showChildView('contentRegion', view);
            expect(view.orgPendingPolicyListView.hierarchy).to.equal('org');
        });

        describe('Default tab highlight based on stateObj', function () {
            it('Defaults to orgTab when subpage is invalid', function () {
                view = new PolicyManagerView({
                    stateObj : {
                        subpages : ['tottenhamTab']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(view.ui.searchTab.hasClass('active')).to.equal(true);
            });

            it('Will show the Org Policies tab and default to paid when subpage is ' +
                '"orgPolicies/paid"', function () {
                view = new PolicyManagerView({
                    stateObj : {
                        subpages : ['orgPolicies', 'paid']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(view.ui.orgPoliciesTab.hasClass('active')).to.equal(true);
                expect(view.orgPendingPolicyListView.ui.paidTab.hasClass('active'))
                    .to.equal(true);
            });

            it('Will show the policies tab and default to pending when subpage is ' +
                '"policies"', function () {
                view = new PolicyManagerView({
                    stateObj : {
                        subpages : ['policies']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(view.ui.policiesTab.hasClass('active')).to.equal(true);
                expect(view.pendingPolicyListView.ui.pendingTab.hasClass('active'))
                    .to.equal(true);
            });

            it('Will show the search tab when subpage is "search"', function () {
                view = new PolicyManagerView({
                    stateObj : {
                        subpages : ['search']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(view.ui.searchTab.hasClass('active')).to.equal(true);
            });
        });

        describe('Checkpoints', function () {
            it('Hash should reset to /org/ when subpages are exist', function () {
                var expectedHash = '#pending-policy-manager/org/';
                view = new PolicyManagerView({
                    stateObj : {
                        subpages  : ['org', 'sometab', 'sometab2']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(document.location.hash).to.equal(expectedHash);
            });

            it('Hash URL should reset with valid hash', function () {
                var expectedHash = '#pending-policy-manager/policies/paid/';
                view = new PolicyManagerView({
                    stateObj : {
                        subpages  : ['policies', 'paid']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(document.location.hash).to.equal(expectedHash);
            });

            it('Sub hash should reset to pending/ if second one is invalid', function () {
                var expectedHash = '#pending-policy-manager/policies/pending/';
                view = new PolicyManagerView({
                    stateObj : {
                        subpages  : ['policies', 'some']
                    }
                });
                root.showChildView('contentRegion', view);

                expect(document.location.hash).to.equal(expectedHash);
            });
        });
    }); // initialization tests

    // Trying to track down text crash about '_location' of null
    // Caused by document.location.hash?
    describe.skip('Tab management tests', function () {

        var checkpointStub;
        var root;
        var view;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            root           = helpers.viewHelpers.createRootView();
            view           = new PolicyManagerView();
            checkpointStub = this.sinon.stub(view, '_setCheckpoint');
        });

        afterEach(function () {
            checkpointStub.restore();
            view.destroy();
        });

        it('switching tabs will call the view._setCheckpoint', function () {
            var e = {
                currentTarget  : 'orgTab',
                preventDefault : function () {}
            };

            view._switchTab(e);
            expect(checkpointStub).to.have.been.called;
        });

        it('switching to the policiesTab adds the subtab state to the checkpoint', function () {
            var subTabState = {
                length : 50,
                start  : 50
            };

            view.pendingPolicyListView.currentTab    = 'paid';
            view.pendingPolicyListView.tabState.paid = subTabState;
            root.render();
            root.showChildView('contentRegion', view);

            view.$el.find('[data-tabname="policies"]').click();
            expect(checkpointStub).to.have.been.calledWith(subTabState);
        });

        it('switching to the orgPoliciesTab adds the subtab state to the checkpoint', function () {
            var subTabState = {
                length : 25,
                start  : 25
            };

            view.orgPendingPolicyListView.currentTab    = 'inactive';
            view.orgPendingPolicyListView.tabState.inactive = subTabState;
            root.render();
            root.showChildView('contentRegion', view);

            view.$el.find('[data-tabname="orgPolicies"]').click();
            expect(checkpointStub).to.have.been.calledWith(subTabState);
        });

        it('switching to the searchTab will create a checkpoint with the searchState', function () {
            view.searchState = {
                searchTerm : 'Gojira',
                searchType : 'clientName'
            };
            root.render();
            root.showChildView('contentRegion', view);

            view.$el.find('[data-tabname="search"]').click();

            expect(checkpointStub).to.have.been.calledWith(view.searchState);
        });
    }); // Tab management tests

    describe.skip('_showPolicy method', function () {
        var listener;
        var navStub;
        var view;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            navStub = this.sinon.stub();
            view = new PolicyManagerView();
            listener = new Backbone.Marionette.Object();
            listener.listenTo(view, 'nav', navStub);
        });

        afterEach(function () {
            view.destroy();
        });

        it('_showPolicy triggers a "nav" event', function () {
            var policyId = '918273645';
            view._showPolicy(policyId);
            expect(navStub).to.have.been.calledWith(
                '#pending-policy-detail?policyId=' + policyId
            );
        });
    }); // _showPolicy method

    describe.skip('_setProducerInfo method', function () {
        var view;
        var root;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            root = helpers.viewHelpers.createRootView();
            root.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('Renders the info in the producerInfoRegion', function () {
            view = new PolicyManagerView();
            root.showChildView('contentRegion', view);

            var region = view.$el.find('#producer-info-region');

            expect(region).to.exist.and.have.length.above(0);
            expect(region[0].innerHTML).to.equal('');

            view._setProducerInfo({ producerName: 'Jonas Grumby'});
            expect(region[0].innerHTML).to.not.equal('');
        });
    }); // _setProducerInfo method

    describe.skip('_setOrgProducerInfo method', function () {
        var view;
        var root;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            root = helpers.viewHelpers.createRootView();
            root.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('Renders the info in the producerInfoRegion', function () {
            view = new PolicyManagerView();
            root.showChildView('contentRegion', view);

            var region = view.$el.find('#org-producer-info-region');

            expect(region).to.exist.and.have.lengthOf(1);
            expect(region[0].innerHTML).to.equal('');

            view._setOrgProducerInfo({ fullName: 'Jonas Grumby'});
            expect(region[0].innerHTML).to.be.contain('Jonas Grumby');
            expect(Backbone.$(region[0]).find('i.fa-users.grey')).to.have.lengthOf(1);
        });
    }); // _setOrgProducerInfo method

    describe.skip('_showProducerPolicyList method', function () {
        var listener;
        var navStub;
        var root;
        var switchTabStub;
        var view;

        beforeEach(function () {
            listener = new Backbone.Marionette.Object();
            navStub  = this.sinon.stub();
            root     = helpers.viewHelpers.createRootView();
            root.render();

            view          = new PolicyManagerView();
            switchTabStub = this.sinon.stub(view, '_switchTab');
            listener.listenTo(view, 'nav', navStub);
            root.showChildView('contentRegion', view);

            // set the producerId being displayed in the "My Pending Policies" tab
            view.myPoliciesProducerId = '12345';
        });

        afterEach(function () {
            view.destroy();
            switchTabStub.restore();
        });

        it('should switch tabs to "My Pending Policies" if data-id attribute ' +
            'is the ID of the current producer', function () {

            // simulate clicking on the link with a data-id attribute of '12345'
            view._showProducerPolicyList({ producerId : '12345' });

            expect(switchTabStub).to.have.been.called;
        });

        it('should navigate to the #producer-policy-list page', function () {
            view._showProducerPolicyList({ producerId : '54321' });
            expect(navStub).to.have.been.calledWith('#producer-policy-list?producerId=54321');
        });

    }); // _showProducerPolicyList method

    describe('_setMyPoliciesProducerId method', function () {

        it('should set myPoliciesProducerId value on the view to the id passed to it', function () {

            var expectedId = '12345';
            var view       = new PolicyManagerView();
            view._setMyPoliciesProducerId({ producerId : expectedId });

            expect(view.myPoliciesProducerId).to.equal(expectedId);
        });

    }); // _setMyPoliciesProducerId method

    describe('_setCheckpoint method', function () {
        var view;
        var writeCheckpointStub;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            view = new PolicyManagerView();
            writeCheckpointStub = this.sinon.stub(view.checkpoint, 'writeCheckpoint');
        });

        afterEach(function () {
            writeCheckpointStub.restore();
            view.destroy();
        });

        it('calls the checkpointModule.writeCheckpoint function', function () {
            var expectedState = {
                subpages : ['search']
            };
            view._setCheckpoint();
            expect(writeCheckpointStub).to.have.been.calledWith(expectedState, true);
        });

        it('handles the subtabs of the policies', function () {
            var changedState = {
                col     : 3,
                dir     : 'desc',
                length  : 50,
                start   : 100
            };
            var expectedState = {
                col      : 3,
                dir      : 'desc',
                length   : 50,
                start    : 100,
                subpages  : ['policies', 'paid']
            };

            view.currentTab = 'policies';
            view.pendingPolicyListView.currentTab = 'paid';
            view._setCheckpoint(changedState);
            expect(writeCheckpointStub).to.have.been.calledWith(expectedState, true);
        });

        it('handles the state of the search tab', function () {
            var changedState = {
                col     : 3,
                dir     : 'desc',
                length  : 50,
                start   : 100
            };
            var expectedState = {
                col      : 3,
                dir      : 'desc',
                length   : 50,
                start    : 100,
                subpages : ['search']
            };

            view.currentTab = 'search';
            view._setCheckpoint(changedState);
            expect(writeCheckpointStub).to.have.been.calledWith(expectedState, true);
            expect(view.searchState).to.deep.equal(changedState);
        });
    }); // _setCheckpoint method

    describe('_addCheckpoint method', function () {
        var view;
        var writeCheckpointStub;

        beforeEach(function () {
            document.location.hash='#pending-policy-manager';
            view = new PolicyManagerView();
            writeCheckpointStub = this.sinon.stub(view.checkpoint, 'writeCheckpoint');
        });

        afterEach(function () {
            writeCheckpointStub.restore();
            view.destroy();
        });
        it('_addCheckpoint exists', function () {
            expect(view._addCheckpoint).to.exist.and.be.a('function');
        });

        it('_addCheckpoint calls `writeCheckpoint` with the 2nd param false', function () {
            var expectedState = {
                subpages : ['search']
            };
            view._addCheckpoint();
            expect(writeCheckpointStub).to.have.been.calledWith(expectedState, false);
        });
    }); // _addCheckpoint method
});


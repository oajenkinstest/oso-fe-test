/* global describe:false, Backbone:false, expect:false */

var WaitIndicator = require('../dist/modules/waitIndicator/waitIndicatorModule');

describe('Wait Indicator Module (modules/waitIndicatorModule.js)', function () {
    var showSpinnerSpy;
    var hideSpinnerSpy;
    var waitIndicator;

    beforeEach(function () {
        showSpinnerSpy = this.sinon.spy(WaitIndicator.prototype, 'showSpinner');
        hideSpinnerSpy = this.sinon.spy(WaitIndicator.prototype, 'hideSpinner');
        waitIndicator = new WaitIndicator();
    });

    afterEach(function () {
        showSpinnerSpy.restore();
        hideSpinnerSpy.restore();
        waitIndicator = null;
    });

    it('waitIndicatorModule can be instantiated', function () {
        expect(waitIndicator).to.be.an.instanceOf(Backbone.Marionette.Object);
    });

    it('should have wait Indicator Channel', function () {
        expect(waitIndicator.waitIndicatorChannel.channelName).to.equal('wait');
    });

    it('should have wait Indicator Channel event "show" and "hide"', function () {
        expect(waitIndicator.waitIndicatorChannel._events).to.have.property('show');
        expect(waitIndicator.waitIndicatorChannel._events).to.have.property('hide');
    });

    it('spinner Object should exist"', function () {
        expect(waitIndicator.spinner).to.exist;
    });

    it('should call respective method while trigger channel events"', function () {
        waitIndicator.waitIndicatorChannel.trigger('show');
        expect(showSpinnerSpy).to.have.been.called;

        waitIndicator.waitIndicatorChannel.trigger('hide');
        expect(hideSpinnerSpy).to.have.been.called;
    });

    describe('"activeSpinners" counts', function () {
        it('count should be 2 if "show" event called two times', function (){
            waitIndicator.activeSpinners = 0;
            waitIndicator.waitIndicatorChannel.trigger('show');
            waitIndicator.waitIndicatorChannel.trigger('show');

            expect(waitIndicator.activeSpinners).to.equal(2);
        });

        it('count should be 0 and "stop" should called once if "hide" '+
                'called event two times', function (){

            var spinnerStopSpy = this.sinon.spy(waitIndicator.spinner, 'stop');
            waitIndicator.activeSpinners = 2;
            
            waitIndicator.waitIndicatorChannel.trigger('hide');
            waitIndicator.waitIndicatorChannel.trigger('hide');

            expect(waitIndicator.activeSpinners).to.equal(0);

            expect(spinnerStopSpy).to.be.calledOnce;

            spinnerStopSpy.restore();
        });

        it('"stop" should not called once if "hide" '+
                'called event once times', function (){

            var spinnerStopSpy = this.sinon.spy(waitIndicator.spinner, 'stop');
            waitIndicator.activeSpinners = 2;
            
            waitIndicator.waitIndicatorChannel.trigger('hide');

            expect(spinnerStopSpy).to.not.be.calledOnce;

            spinnerStopSpy.restore();
        });

        it('resetActiveSpinnersCount method should exist', function () {
            expect(waitIndicator.resetActiveSpinnersCount).to.exist;
        });

        it('resetActiveSpinnersCount should set count 0', function () {
            waitIndicator.waitIndicatorChannel.trigger('show');
            waitIndicator.waitIndicatorChannel.trigger('show');
            waitIndicator.resetActiveSpinnersCount();
            expect(waitIndicator.activeSpinners).to.equal(0);
        });
    });


});

/* global describe: false, expect:false, Backbone: false, _:false, $:false */

/**
 * Datatables Utility module spec
 */

var utils   = require('../dist/utils/utils-datatables');
var helpers = require('./helpers/helpers');

describe('utility - Datatable (utils/utils-datatables) Test Spec', function() {

    it('utilsDatatable should exist', function() {
        expect(utils).to.exist;
    });

    describe('setDatatablesDefaultContent', function () {
        var columns;

        beforeEach(function () {
            columns = [
                { data : 'foo', name : 'foo' },
                { data : 'bar', name : 'bar' },
                { data : 'snafu', name : 'snafu' }
            ];
        });

        afterEach(function () {
            columns = null;
        });

        it('exists as a function', function () {
            expect(utils.setDatatablesDefaultContent).to.be.a('function');
        });

        it('returns previous array with \'defaultContent\' property in each object', function () {
            var results = utils.setDatatablesDefaultContent(columns);

            _.each(results, function (result) {
                expect(result).to.have.property('defaultContent');
            });
        });

        it('adds value passed as the defaultContent', function () {
            var value   = 'I see you!';
            var results = utils.setDatatablesDefaultContent(columns, value);

            _.each(results, function (result) {
                expect(result.defaultContent).to.equal(value);
            });
        });

        it('adds an empty string as defaultContent if no value param is provided',
            function () {
                var results = utils.setDatatablesDefaultContent(columns);

                _.each(results, function (result) {
                    expect(result.defaultContent).to.equal('');
                });
            });

        it('preserves any existing defaultContent values', function () {
            var results;
            var previouslySetValue = 'Can\'t touch this!';
            var newValue           = 'Hammer don\'t hurt \'em!';
            var previouslySetIndex = 1;

            columns[previouslySetIndex].defaultContent = previouslySetValue;
            results = utils.setDatatablesDefaultContent(columns, newValue);

            _.each(results, function (result, i) {
                if (i === previouslySetIndex) {
                    expect(result.defaultContent).to.equal(previouslySetValue);
                } else {
                    expect(result.defaultContent).to.equal(newValue);
                }
            });
        });

        it('throws an error if first param is not an array', function () {
            var fn = function () {
                utils.setDatatablesDefaultContent('boom!');
            };

            expect(fn).to.throw(Error);
        });

    }); // setDatatablesDefaultContent

    describe('getCssClassForStatus method', function () {
        var expected;
        var result;

        var testParametersMatrix = [
            // description                  //holding stat  //status          //expected
            ['Application Received',        'Proposed',     'Pending',        'label-yellow-0'],
            ['In Process',                  'Proposed',     'Pending',        'label-yellow-0'],
            ['In Underwriting',             'Proposed',     'Pending',        'label-yellow-25'],
            ['Requirements Outstanding, Underwriting Complete', 
                                            'Proposed',     'Pending',        'label-yellow-50'],
            ['Requirements Outstanding, Not Issued',          
                                            'Proposed',     'Pending',        'label-yellow-75'],
            ['Ready for Issue',             'Proposed',     'Pending',        'label-yellow'],
            ['Paid',                        'Proposed',     'Pending',        'label-yellow'],
            ['Issued Not Paid',             'Proposed',     'Pending',        'label-success-50'],
            ['Ready for Payment',           'Proposed',     'Pending',        'label-success-50'],
            ['Expired',                     'Proposed',     'Issued',         'label-grey'],

            ['Paid',                        'Proposed',     'Terminated',     'label-grey'],

            ['Terminated',                  'Inactive',     'Lapse',          'label-transparent'],
            ['Expired',                     'Inactive',     'Terminated',     'label-transparent'],
            
            ['Paid',                        'Proposed',     'Pending',        'label-yellow'],

            ['Active',                      'Active',       'Issued',         'label-transparent']
        ];
        
        testParametersMatrix.forEach(function (param) {
            expected =  param[3]; 
            result   =  utils.getCssClassForStatus(
                param[0], //description
                param[2], //acord holding status
                param[1]  //status
            );
            it ('Should return "'+expected+'" if params are "description" as "'+param[0]+'"'+
                ', "reportingGroup" as "'+param[1]+'" and "status" as "'+param[2]+'"', function (){
                expect(result).to.be.equal(expected);
            });
        });
    });  // getCssClassForStatus method

    describe ('nullifyPolicyStatusNotAvailableProps method', function () {
        var policyDataCopy;
        
        it('exists as a function', function () {
            expect(utils.nullifyPolicyStatusNotAvailableProps).to.be.a('function');
        });

        it('should return updated policy status if acordHolding status is Dormant/Unknown'+
            'and properties has dataAvailability property with value notAvailable', function () {
            
            policyDataCopy = $.extend(true, {}, helpers.policyData.activePolicyDetail);
            policyDataCopy.policyStatus = {
                'acordHoldingStatus': 'Unknown',
                'status': { 
                    'dataAvailability': 'notAvailable' 
                },
                'statusDetail': { 
                    'dataAvailability': 'notAvailable' 
                },
                'workflowStatus': { 
                    'dataAvailability': 'notAvailable' 
                },
                'description': { 
                    'dataAvailability': 'notAvailable' 
                },
                'date': '2016-02-21T15:50:00Z'
            };

            var expectedOutput = {
                'acordHoldingStatus': 'Unknown',
                'status': null,
                'statusDetail': null,
                'workflowStatus': null,
                'description': null,
                'date': '2016-02-21T15:50:00Z'
            };
            policyDataCopy = 
                utils.nullifyPolicyStatusNotAvailableProps(policyDataCopy);
            expect(policyDataCopy.policyStatus).to.be.deep.equal(expectedOutput);
        });
    });

    describe('buildPolicyNumberLink method', function () {

        // Its suppose to not required, but some how it failed
        // because some file not cleared up userChannel 
        var userChannel = Backbone.Radio.channel('user');
        userChannel.reset();

        it('buildPolicyNumberLink returns the policyNumber when present', function() {
            var policyNumber = '1234567890';
            var policyId     = '9876543210';
            var expectedLink = '<a href="#policy-detail?policyId=9876543210" ' +
                'class="oa-js-nav">1234567890</a>';

            var rowData = {
                policyNumber: policyNumber,
                policyId: policyId,
                caseId: ''
            };

            var actualLink = utils.buildPolicyNumberLink(policyNumber, rowData);
            expect(actualLink).to.equal(expectedLink);
        });

        it('buildPolicyNumberLink returns "Recently Received" without policyNumber',
            function() {
                var policyNumber = null;
                var expectedLink = '<a href="#policy-detail?caseId=12345" ' +
                    'class="oa-js-nav">Recently Received</a>';

                var rowData = {
                    policyNumber: policyNumber,
                    caseId: '12345'
                };

                var actualLink = utils.buildPolicyNumberLink(policyNumber, rowData);
                expect(actualLink).to.equal(expectedLink);
            }
        );
    }); // buildPolicyNumberLink method

    describe('buildPendingCountLink tests', function() {

        // Its suppose to not required, but some how it failed
        // because some file not cleared up userChannel 
        var userChannel = Backbone.Radio.channel('user');
        userChannel.reset();

        it('returns an empty string if the pendingRequirementCount is empty', function() {
            var data = {};
            var link = utils.buildPendingCountLink(data);
            expect(link).to.equal('');
        });

        it('returns an empty string if the pendingRequirementCount is 0', function() {
            var data = {
                pendingRequirementCount : 0,
                policyId                : '1234567890'
            };
            var link = utils.buildPendingCountLink(data);
            expect(link).to.equal('');
        });

        it('returns a proper link if pendingRequirementCount is > 0', function() {
            var data = {
                pendingRequirementCount : 4,
                policyId                : '1234567890'
            };
            var link = utils.buildPendingCountLink(data);
            var expectedLink =
                '<a href="#policy-detail/requirements/?policyId=1234567890">' +
                '<span class="badge badge-info" title="4 Requirements Outstanding">4</span></a>';

            expect(link).to.equal(expectedLink);
        });

        it('Uses proper English for a single outstanding requirement', function() {
            var data = {
                pendingRequirementCount : 1,
                policyId                : '1234567890'
            };
            var link = utils.buildPendingCountLink(data);
            var expectedLink =
                '<a href="#policy-detail/requirements/?policyId=1234567890">' +
                '<span class="badge badge-info" title="1 Requirement Outstanding">1</span></a>';

            expect(link).to.equal(expectedLink);
        });

        it('returns a proper link if for AWD only policies with outstanding reqs', function() {
            var data = {
                pendingRequirementCount : 7,
                caseId                  : 'timestamp'
            };
            var link = utils.buildPendingCountLink(data);
            var expectedLink =
                '<a href="#policy-detail/requirements/?caseId=timestamp">' +
                '<span class="badge badge-info" title="7 Requirements Outstanding">7</span></a>';

            expect(link).to.equal(expectedLink);
        });

        describe('CSS class applied to the badge changes based on pending requirements count',
            function () {

            it('when count is greater than 9, "badge-important" class is used', function () {
                var data = {
                    pendingRequirementCount : 10,
                    policyId                : '1234567890'
                };
                // Create a jQuery link from the link text and then find the span tag
                var span = $($.parseHTML(utils.buildPendingCountLink(data))).find('span');

                expect(span.hasClass('badge-important')).to.be.true;

            });

                it('when count is less than 9, "badge-info" class is used', function () {
                    var data = {
                        pendingRequirementCount : 9,
                        policyId                : '1234567890'
                    };
                    // Create a jQuery link from the link text and then find the span tag
                    var span = $($.parseHTML(utils.buildPendingCountLink(data))).find('span');

                    expect(span.hasClass('badge-info')).to.be.true;

                });
        });
    }); // buildPendingCountLink tests

    describe('Method "getActiveProducerRoleCodesAsString"', function () {
        it('should exist', function (){
            expect(utils.getActiveProducerRoleCodesAsString).to.exist.and.be.a('function');
        });
        it('should returns role codes sorted alphanumerically', function (){
            var roleCodes = utils.getActiveProducerRoleCodesAsString( [
                {
                    roleCode : 'BDBC123',
                    statusCode : 'A'
                },
                {
                    roleCode : 'ABC123',
                    statusCode : 'A'
                }
            ]);
            expect(roleCodes).to.be.equal('ABC123, BDBC123');
        });

        it('should returns blank array if there is no role codes', function (){
            var roleCodes = utils.getActiveProducerRoleCodesAsString([]);
            expect(roleCodes).to.be.equal('');
        });
    }); // Method "getActiveProducerRoleCodesAsString"

    describe('Ordering tests', function() {

        it('getStatusOrder returns an array to be used for ordering', function() {
            var ordering = utils.getPolicyStatusOrder();
            var expected = [
                'Application Received',
                'In Process',
                'In Underwriting',
                'Requirements Outstanding, Underwriting Complete',
                'Requirements Outstanding, Not Issued',
                'Ready for Issue',
                'Issued Not Paid',
                'Ready for Payment',
                'Paid',
                'Declined',
                'Postponed',
                'Withdrawn',
                'Incomplete',
                'Terminated',
                'Expired',
                'Surrendered',
                'Lapse',
                'Not Taken'
            ];
            expect(ordering).to.deep.equal(expected);
        });
    }); // Ordering tests

    describe('_oSortPolicyStatusAsc method', function() {
        it('exist and be a function', function () {
            expect(utils._oSortPolicyStatusDesc).to.exist.and.be.a('function');
        });

        it('should return 1', function () {
            expect(utils._oSortPolicyStatusAsc(
                'Lapse',
                'Surrendered'
            )).to.equal(1);
        });

        it('should return -1', function () {
            expect(utils._oSortPolicyStatusAsc(
                'Surrendered',
                'Lapse'
            )).to.equal(-1);
        });

        it('should return 0', function () {
            expect(utils._oSortPolicyStatusAsc(
                'Surrendered',
                'Surrendered'
            )).to.equal(0);
        });
    });

    describe('_oSortPolicyStatusDesc', function() {
        it('exist and be a function', function () {
            expect(utils._oSortPolicyStatusDesc).to.exist.and.be.a('function');
        });

        it('should return -1', function () {
            expect(utils._oSortPolicyStatusDesc(
                'Lapse',
                'Surrendered'
            )).to.equal(-1);
        });

        it('should return 1', function () {
            expect(utils._oSortPolicyStatusDesc(
                'Surrendered',
                'Lapse'
            )).to.equal(1);
        });

        it('should return 0', function () {
            expect(utils._oSortPolicyStatusDesc(
                'Surrendered',
                'Surrendered'
            )).to.equal(0);
        });
    });

    describe('receivedDateAndPolicyNumberSort tests', function() {

        it('date-x before date-y', function() {
            var expected = -1;
            var x        = {receivedDate: '2016-01-01'};
            var y        = {receivedDate: '2016-12-15'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('date-x after date-y', function() {
            var expected = 1;
            var x        = {receivedDate: '2016-12-15'};
            var y        = {receivedDate: '2016-01-01'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('dates equal, policy-x greater than policy-y', function() {
            var expected = -1;
            var x        = {receivedDate: '2016-01-01', policyNumber: '12345'};
            var y        = {receivedDate: '2016-01-01', policyNumber: '11111'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('dates equal, policy-x less than policy-y', function() {
            var expected = 1;
            var x        = {receivedDate: '2016-01-01', policyNumber: '11111'};
            var y        = {receivedDate: '2016-01-01', policyNumber: '12345'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('dates and policy equal', function() {
            var expected = 0;
            var x        = {receivedDate: '2016-01-01', policyNumber: '11111'};
            var y        = {receivedDate: '2016-01-01', policyNumber: '11111'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('date-x empty, date-y populated', function() {
            var expected = -1;
            var x        = {receivedDate: ''};
            var y        = {receivedDate: '2016-12-15'};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('date-y empty, date-x populated', function() {
            var expected = 1;
            var x        = {receivedDate: '2016-01-01'};
            var y        = {receivedDate: ''};
            var result   = utils.receivedDateAndPolicyNumberSort(x, y);

            expect(result).to.equal(expected);
        });

        it('receivedDateAndPolicyNumberSortDesc returns oppposite of sort', function() {
            var expected = -1;
            var x        = {receivedDate: '2016-01-01', policyNumber: '11111'};
            var y        = {receivedDate: '2016-01-01', policyNumber: '12345'};
            var result   = utils.receivedDateAndPolicyNumberSortDesc(x, y);

            expect(result).to.equal(expected);
        });
    }); // receivedDateAndPolicyNumberSort tests

    describe('reverseSort tests', function() {

        it('returns 1 for negative', function () {
            expect(utils.reverseSort(-1)).to.equal(1);
        });

        it('returns -1 for positive', function () {
            expect(utils.reverseSort(1)).to.equal(-1);
        });

        it('returns 0 for 0', function () {
            expect(utils.reverseSort(0)).to.equal(0);
        });
    }); // reverseSort tests

    describe('Method bindDataTableEvents', function () {
        var rootView;
        var View;
        var view;
        before(function () {
            rootView = helpers.viewHelpers.createRootView();
            View = Backbone.Marionette.ItemView.extend({
                ui: {
                    dtTable : '#dtTable',
                    responsiveTableInstruction : '.table-responsive-instruction'
                },
                template  : _.template(
                    '<table id="dtTable">'+
                        '<thead><tr><th>Col1</th></tr></thead>'+
                        '<tbody><tr><td>content</td></tr></tbody>'+
                    '</table>'+
                    '<div class="table-responsive-instruction"></div>'
                ),
                onRender : function () {
                    this.ui.dtTable.DataTable({
                        dom : 'ilptilp'
                    });
                }
            });
            rootView.render();
            view = new View();
            rootView.showChildView('contentRegion', view);
        });

        after(function () {
            view.destroy();
            rootView.destroy();
            view = null;
        });

        it('should exist', function () {
            expect(utils.bindDataTableEvents).to.exist;
        });

        it('should throw error if parameter is missing', function () {
            var fn = function () {
                utils.bindDataTableEvents();
            };

            expect(fn).to.throw(utils.errors.dataTableObject);
        });

        it('should throw error if wrong dataTable Object', function () {
            var fn = function () {
                utils.bindDataTableEvents({dtObject:{}});
            };

            expect(fn).to.throw(utils.errors.dataTableObject);
        });

        describe ('_dataTableEventInitCallback method', function (){
            it('exist and be a function', function () {
                expect(utils._dataTableEventInitCallback).to.exist.and.be.a('function');
            });

            it('Add "table-responsive-instruction" div before "dataTables_info"', function(){
                var param = {
                    viewScope : view
                };

                var responsiveInstrElement = view.$el.find('.dataTables_info')
                    .eq(1).prev('.table-responsive-instruction');
                expect(responsiveInstrElement).to.be.lengthOf(0);

                utils._dataTableEventInitCallback.call(
                    param, 
                    {},
                    $.fn.dataTableSettings[$.fn.dataTableSettings.length-1]
                );
                responsiveInstrElement = view.$el.find('.dataTables_info')
                    .eq(1).prev('.table-responsive-instruction');
                expect(responsiveInstrElement).to.be.lengthOf(1);
            });
        });
        
    }); // Method bindDataTableEvents

    describe('Method getDatatableOptions', function () {
        it('Should exist', function () {
            expect(utils.getDatatableOptions).to.exist.and.be.a('function');
        });

        it('Should return expected options', function () {
            var expected = {
                lengthMenu:[25,50,100],
                orderMulti:false,
                processing:false,
                searching:false,
                serverSide:true,
                ajax:{
                    url:'/api/oso/secure/rest/',
                    cache:true,
                    cacheTTL:0.17,
                    localCache:false,
                    dataSrc: null
                }
            };

            var dataTableOptions =  utils.getDatatableOptions(
                this, 
                '/api/oso/secure/rest/',
                true
            );
            dataTableOptions.ajax.localCache = false;
            dataTableOptions.ajax.dataSrc = null;

            expect(dataTableOptions).to.deep.equal(expected);
        });

        it('Cache options should not be exist if parameter not specified', function () {
            var expected = {
                lengthMenu:[25,50,100],
                orderMulti:false,
                processing:false,
                searching:false,
                serverSide:true,
                ajax:{
                    url:'/api/oso/secure/rest/'
                }
            };
            var dataTableOptions =  utils.getDatatableOptions(
                this, 
                '/api/oso/secure/rest/'
            );

            expect(dataTableOptions).to.deep.equal(expected);
        });

        it('Should throw Error if url provided is not string/blank', function () {
            var fn = function () {
                utils.getDatatableOptions(this,'');
            };
            expect(fn).to.throw(utils.errors.dataTableOptionInvalidURL);
        });

        it('callback method "_dataSourceCallback" for dataSrc option ', function () {
            var dataSourceCallbackSpy = this.sinon.spy(utils,'_dataSourceCallback');
            var dataTableOptions =  utils.getDatatableOptions(
                this, 
                '/api/oso/secure/rest/',
                true
            );

            dataTableOptions.ajax.dataSrc({});
            expect(dataSourceCallbackSpy).to.have.been.called;

        });
    }); // getDatatableOptions

});

/* global expect:false, $:false, Backbone: false, Marionette:false */

var OrgsTableView = require('../dist/pages/policy/views/orgs-table-v');
var helpers   = require('./helpers/helpers');

describe('Organization Table View (pages/policy/views/orgs-table-v.js)', function() {

    var ajaxStub;
    var orgsData = {
        'draw': null,
        'recordsTotal': 6,
        'recordsFiltered': 6,
        'data': [
            {
                'id': 144527,
                'fullName': 'WILLIAM R BEGLEY',
                'lexicalName': 'BEGLEY, WILLIAM',
                'hasDownlineReports': true,
                'hasPolicies' : true
            },
            {
                'id': 144569,
                'fullName': 'JAMES R COMSTOCK',
                'lexicalName': 'COMSTOCK, JAMES',
                'hasDownlineReports': true,
                'hasPolicies' : false
            },
            {
                'id': 144798,
                'fullName': 'THEODORE W CONNER',
                'lexicalName': 'CONNER, THEODORE',
                'hasDownlineReports': true,
                'hasPolicies' : true
            },
            {
                'id': 155780,
                'fullName': 'CRAIG W DEMAREE',
                'lexicalName': 'DEMAREE, CRAIG',
                'hasDownlineReports': true
            },
            {
                'id': 150279,
                'fullName': 'JOHN H DITSLEAR',
                'lexicalName': 'DITSLEAR, JOHN',
                'hasDownlineReports': true
            },
            {
                'id': 164283,
                'fullName': 'FEIGHNER INSURANCE INC',
                'lexicalName': 'FEIGHNER INSURANCE INC',
                'hasDownlineReports': false
            },
            {
                'id': 144586,
                'fullName': 'JEFFREY R SEMLER',
                'lexicalName': 'SEMLER, JEFFREY',
                'hasDownlinReports': false
            }
        ],
        'error': null,
        'column': null,
        'dir': null,
        'managingProducer': {
            'id': 144586,
            'fullName': 'JEFFREY R SEMLER',
            'lexicalName': 'SEMLER, JEFFREY'
        },
        'lastRefreshDate' : '2016-12-14T14:23:00.000',
        'dataSrc'         : 'data'
    };
    var rootView;
    var view;

    beforeEach(function () {
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            orgsData
        );

        rootView = helpers.viewHelpers.createRootView();
        view = new OrgsTableView();
        rootView.render();
        rootView.showChildView('contentRegion', view);
    });

    afterEach(function () {
        view.destroy();
        view = null;
        ajaxStub.restore();
    });

    it('exists', function () {
        expect(OrgsTableView).to.exist;
    });

    describe('initialization', function () {

        it('ui elements should exist', function () {
            expect(view.ui.orgsTableWrapper).to.be.defined;
            expect(view.ui.orgsTable).to.be.defined;
            expect(view.ui.responsiveTableInstruction).to.be.defined;
        });
           
        it('"producerId" should set if option contain it', function () {
            view = new OrgsTableView({
                producerId : 122323232
            });
            expect(view.options.producerId).to.equal(122323232);
        });
    }); // initialization

    describe('rendering', function () {

        it('"Last Refresh Date" should display the date/time sent from the service', function () {
            var expectedValue = '12/14/2016, 02:23 PM EST';

            expect(view.$el.find('#orgs-table-last-refresh-date').text().trim())
                .to.equal(expectedValue);
        });

        it('Table should contain 7 column', function () {
            expect(view.$el.find('tr:eq(0) th')).to.have.lengthOf(7);
        });

        it('Should contain Gray icon if producer has downline reports', function () {
            expect(view.$el.find('tr:eq(1) td:eq(0) i').hasClass('fa-users grey')).to.be.true;
        });

        it('Should contain a link to view an org if a producer has downline reports', function () {
            expect(view.$el.find('tr:eq(1) td:eq(1) a.organization-link')).to.have.lengthOf(1);
        });

        it('Should not contain Gray icon if producer has no downline reports', function () {
            expect(view.$el.find('tr:eq(7) td:eq(0) i')).to.have.lengthOf(0);
        });

        it('Should not contain a link to view an org if a producer has downline reports',
            function() {
            expect(view.$el.find('tr:eq(7) td:eq(1) a.organization-link')).to.have.lengthOf(0);
        });

        it('"Policies in Sub-Organization" link will appear if producer has downline reports',
            function () {
            expect(view.$el.find('tr:eq(1) td:eq(1) a.policy-list-link')).to.have.lengthOf(1);
        });

        it('"Policies" link will be displayed for an individual producer', function () {
           expect(view.$el.find('tr:eq(6) td:eq(1) a.producer-link').length).to.equal(1);
        });

        it('DataTable language info label should update with producer name', function (){
            expect(view.$el.find('.dataTables_info span').text())
                .to.be.contain(orgsData.managingProducer.fullName);
        });

        it('Should trigger updateGeneralInfo while completing table draw', function (){
            var updateGeneralInfoStub = this.sinon.stub();
            var listener = new Backbone.Marionette.Object();
            view = new OrgsTableView();
            listener.listenTo(view, 'updateGeneralInfo', updateGeneralInfoStub);

            rootView.showChildView('contentRegion', view);

            expect(updateGeneralInfoStub).to.have.been.calledWith(orgsData.managingProducer);
        });
    }); // rendering

    describe('table sorting', function () {

        it('enable default sorting order ASC for "Producer Name" column', function () {
            expect(view.$el.find('th:eq(1)').hasClass('sorting_asc')).to.be.true;
        });

        it('should change sorting order for "Producer Name" column', function () {
            view.$el.find('th:eq(1)').click();
            expect(view.$el.find('th:eq(1)').hasClass('sorting_desc')).to.be.true;
        });
    }); // table sorting

    describe('_setProducerOrgsContent method', function () {

        it('_setProducerOrgsContent should exist', function () {
            expect(view._setProducerOrgsContent).to.exist;
        });

        it('_setProducerOrgsContent should called while drawing table', function () {
            var setProducerOrgsContentSpy =
                    this.sinon.spy(OrgsTableView.prototype, '_setProducerOrgsContent');
            view = new OrgsTableView();
            rootView.showChildView('contentRegion', view);
            expect(setProducerOrgsContentSpy).to.have.been.called;
            setProducerOrgsContentSpy.restore();
        });
    }); // _setProducerOrgsContent method

    describe('_retrieveOrganization method', function () {

        it('_retrieveOrganization Test', function() {
        // TODO: Break this into several different tests

            var listener      = new Marionette.Object();
            var newProducerId = 144527;
            var renderSpy     = this.sinon.spy(view, 'render');
            var stub          = this.sinon.stub();

            // when an organization link is clicked, the service will append the
            // new managingProducer data. Mock this for the second ajax call.
            var copyOfOrgsData = $.extend(true, {}, orgsData);

            copyOfOrgsData.managingProducer = orgsData.data[0];
            ajaxStub.onCall(1).returns(copyOfOrgsData);

            listener.listenTo(view, 'hierarchyChange', stub);

            expect(view.producerId).to.be.undefined;

            view.$el.find('tr:eq(1) td:eq(1) a.organization-link').click();
            expect(view.model.get('producerId')).to.equal(newProducerId);
            expect(renderSpy).to.have.been.called;
            expect(stub).to.have.been.calledWith({ producerOrgId : newProducerId });

            listener.stopListening();
            listener.destroy();
            renderSpy.restore();
        });
    });

    describe('_retrieveProducer method', function () {

        it('triggers a "showProducerPolicyList" event', function () {
            var listener      = new Marionette.Object();
            var stub          = this.sinon.stub();

            listener.listenTo(view, 'showProducerPolicyList', stub);
            view.$el.find('tr:eq(7) td:eq(1) a.producer-link').click();

            expect(stub).to.have.been.called;

            listener.stopListening();
            listener.destroy();
        });
    }); // _retrieveProducer method
});

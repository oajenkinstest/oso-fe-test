/* global expect:false, $:false, Marionette:false, _:false, Backbone:false,
isCanvasLoaded:false, sinon:false */

var helpers      = require('./helpers/helpers');
var HomeView     = require('../dist/pages/home/views/home-v');


describe('Home View (pages/home/views/home-v.js)', function () {
    
    var rootView;
    var homeView;
    var userChannel;
    var server;

    // Unable to user helper method
    // Need to investigate why
    var getRootView = function getRootView () {
        return new Marionette.LayoutView({
            template: _.template('<div id="content"></div>'),
            regions: {
                contentRegion: '#content'
            }
        });
    };

    var getHomeView = function () {
        rootView = getRootView();

        homeView = new HomeView();
        rootView.render();
        rootView.showChildView('contentRegion', homeView);

        return homeView;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // Set up a fake server to avoid tests making real ajax requests. The fake server doesn't
        // actually have to return data for any of the tests in this spec to work; it just allows
        // the tests to run without generating "connect ECONNREFUSED 127.0.0.1:3000" errors in Node.
        server = this.setupFakeServer(this.sinon, global, global.window);
    });

    after(function () {
        server.restore();
    });

    beforeEach(function () {
        userChannel = Backbone.Radio.channel('user');
        homeView = getHomeView();
    });

    afterEach(function () {
        userChannel.stopReplying();
        userChannel = null;
        homeView.destroy();
        // rootView.destroy();
    });

    it('exists', function () {
        expect(HomeView).to.exist;
    });

    describe ('Performance center section in Home', function () {
        var ajaxStub;
        var userRadioRequestStub;
        
        beforeEach(function () {
            // if canvas does not exist in the global object, skip this suite
            if (!isCanvasLoaded) {
                this.skip();
            }

            userChannel.reply('hasCapability', function() {
                return true;
            });
            userRadioRequestStub = this.sinon.stub(userChannel._requests.hasCapability, 'callback');
            userRadioRequestStub.withArgs('Producer_Performance_View').returns(true);

            //performance center data stub
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                helpers.pcData.responseBody
            );
        });

        afterEach(function () {
            if (ajaxStub) {
                ajaxStub.restore();
            }
            
            userRadioRequestStub.restore();
        });

        it('Radio Request \'hasCapability\' is called while rendering homeView', function () {
            homeView = getHomeView();

            expect(userRadioRequestStub).to.have.been.calledWith('Producer_Performance_View');
        });

        it('Performance Center view renders correctly', function () {
            homeView = getHomeView();

            var currentView =
                    rootView.contentRegion.currentView.performanceCenterRegion.currentView;

            expect(rootView.$el.find('#performance-center-region')).to.have.length.above(0);
            
            expect(currentView.fycRegion.$el.find('.year-to-date'))
                .to.have.length(1);
            expect(currentView.qualifyingFycRegion.$el.find('.year-to-date'))
                .to.have.length(1);

            expect(currentView.chairmansTripRegion.$el.find('#chairmansTrip-qualified'))
                .to.have.length(1);
            expect(currentView.leadersConferenceRegion.$el.find('#leadersConference-qualified'))
                .to.have.length(1);
        });

        describe('Home View does not load PC if the user does not have access', function() {
          
            beforeEach(function () {
                userChannel.reply('hasCapability', function() {
                    return true;
                });
                userRadioRequestStub =
                        this.sinon.stub(userChannel._requests.hasCapability, 'callback');
                userRadioRequestStub.withArgs('Producer_Performance_View').returns(false);
            });

            it('Performance Center not shown if user does not have capability', function() {
                homeView = getHomeView();

                var pcContent = rootView.$el.find('#performance-center-region');

                // #performance-center exists as part of parent template...
                expect(pcContent).to.have.length(1);

                var pcHtml    = pcContent.html().trim();
                // but will be empty.
                expect(pcHtml).to.equal('');
            });
        });
    }); // Performance center section in Home

    describe('Pending Policies tout', function () {

        describe('when user has valid page data', function () {

            var appStructureChannel;

            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) { 
                    if (capability === 'Home_Office') {
                        return true;
                    }
                });

                appStructureChannel = Backbone.Radio.channel('appStructure');
                appStructureChannel.reply('getPageDataByKeyValue', function () {
                    return {
                        displayText   : 'Search',
                        link          : 'pending-search',
                    };
                });
                homeView = getHomeView();
            });

            afterEach(function () {
                appStructureChannel.stopReplying();
            });

            it('exists under "My Business" H2 tag', function () {

                var myBusinessH2 = homeView.$el.find('h2:contains("My Business")');
                var toutWell = myBusinessH2.next('.well-callout');

                expect(toutWell.find('a').text()).to.equal('Search');
                expect(toutWell.find('p').text()).to.equal('Find policies and View as Producer');
                expect(toutWell.hasClass('hidden')).to.be.false;
                expect(myBusinessH2.hasClass('hidden')).to.be.false;
            });

            it('directs users to "Pending Search" page', function () {
                var toutLink = homeView.$el.find('a:contains("Search")');
                var expectedValue = '#pending-search';

                expect(toutLink.attr('href')).to.equal(expectedValue);
            });

        });

        describe('when user has valid page data but not have Home_Office capability', function () {

            var appStructureChannel;

            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) { 
                    if (capability === 'Home_Office') {
                        return false;
                    }
                });

                appStructureChannel = Backbone.Radio.channel('appStructure');
                appStructureChannel.reply('getPageDataByKeyValue', function () {
                    return {
                        displayText   : 'Policies',
                        link          : 'pending-policies',
                    };
                });

                homeView = getHomeView();
            });

            afterEach(function () {
                appStructureChannel.reset();
            });

            it('exists under "My Business" H2 tag', function () {
                var myBusinessH2 = homeView.$el.find('h2:contains("My Business")');
                var toutWell = myBusinessH2.next('.well-callout');

                expect(toutWell.find('a').text()).to.equal('View Policies');
                expect(toutWell.find('p').text()).to.equal('Find policies and pending lists');
                expect(toutWell.hasClass('hidden')).to.be.false;
                expect(myBusinessH2.hasClass('hidden')).to.be.false;
            });

        });

    }); // Pending Policies Tout

    describe('iPipeline Forms Tout', function () {

        describe('When user has "IPipeline_FormsPipe" capability', function () {

            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) {
                    return capability === 'IPipeline_FormsPipe';
                });

                homeView = getHomeView();
            });

            it('model is set with "showFormsRegion" ', function () {
                expect(homeView.model.get('showFormsRegion')).to.be.true;
            });

            it('forms region is displayed', function () {
                var formsRegion = homeView.$el.find('#forms-region');
                expect(formsRegion.length).to.equal(1);
            });

            it('forms icon is displayed', function () {
                var icon = homeView.$el.find('#forms-region .fa-file-o');
                expect(icon.length).to.equal(1);
            });

            it('forms link is displayed', function () {
                var formsLink = homeView.$el.find('#forms-region .center a');
                expect(formsLink.length).to.equal(1);
            });

        });     // When user has "IPipeline_FormsPipe" capability

        describe('When user has "IPipeline_FormsPipe(disabled)" capability', function () {

            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) {
                    return capability === 'IPipeline_FormsPipe(disabled)';
                });
                homeView = getHomeView();
            });

            it('model is set with "showFormsRegionDisabled" ', function () {
                expect(homeView.model.has('showFormsRegionDisabled')).to.be.true;
            });

            it('forms icon is displayed in <div> with "disabled" class', function () {
                var icon = homeView.$el.find('#forms-region .fa-file-o');
                var divHasDisabledClass = icon.closest('.center').hasClass('disabled');
                expect(divHasDisabledClass).to.be.true;
            });

        });     // When user has "IPipeline_FormsPipe" capability

        describe('When user DOES NOT have "IPipeline_FormsPipe" capability', function () {

            beforeEach(function () {
                userChannel.reply('hasCapability', function () {
                    return false;
                });
                homeView = getHomeView();
            });

            it('model is not set with "showFormsRegion" ', function () {
                expect(homeView.model.get('showFormsRegion')).to.be.false;
            });

            it('forms region is not displayed', function () {
                var formsRegion = homeView.$el.find('#forms-region');
                expect(formsRegion.length).to.equal(0);
            });

        });     // When user DOES NOT have "IPipeline_FormsPipe" capability

    }); // iPipeline Forms Tout

    describe('Sales Connection Links (eApp/Illustration and Practice) tout ', function () {

        describe('Has IPipeline_iGO capability', function () {
            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) {
                    return capability === 'IPipeline_iGO';
                });

                homeView = getHomeView();
            });

            it('model is set with "showSalesConnectionLinksRegion" ', function () {
                expect(homeView.model.get('showSalesConnectionLinksRegion')).to.be.true;
            });

            it('Sales Connection Links region is displayed', function () {
                var region = homeView.$el.find('#sales-connection-links-region');
                expect(region).to.have.length(1);
            });

            it('Laptop icon is displayed', function () {
                var icon = homeView.$el.find('#sales-connection-links-region .laptop');
                expect(icon.length).to.equal(1);
            });

            it('Two links are displayed', function () {
                var links = homeView.$el.find('#sales-connection-links-region .center a');

                expect(links.length).to.equal(2);
                expect($(links[0]).text()).to.contain('Start/View an eApp or Illustration');
                expect($(links[1]).text()).to.contain('Sales Connection Practice Center');
            });
        });

        describe('No IPipeline_iGO capability', function () {
            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) {
                    return false;
                });

                homeView = getHomeView();
            });

            it('Sales Connection Links region is displayed', function () {
                var region = homeView.$el.find('#sales-connection-links-region');
                expect(region).to.have.length(0);
            });

            it('Laptop icon is not displayed', function () {
                var icon = homeView.$el.find('#sales-connection-links-region .laptop');
                expect(icon.length).to.equal(0);
            });

            it('Tout is not displayed', function () {
                var toutWrapper = homeView.$el.find('#sales-connection-links-region .center');
                expect($(toutWrapper).length).to.equal(0);
            });
        });

        describe('Has IPipeline_iGO(disabled) capability', function () {
            beforeEach(function () {
                userChannel.reply('hasCapability', function (capability) {
                    return capability === 'IPipeline_iGO(disabled)';
                });

                homeView = getHomeView();
            });

            it('model is set with "showSalesConnectionLinksRegionDisabled" ', function () {
                expect(homeView.model.get('showSalesConnectionLinksRegionDisabled')).to.be.true;
            });

            it('Laptop icon is displayed', function () {
                var icon  = homeView.$el.find('#sales-connection-links-region .laptop');
                expect(icon.length).to.equal(1);
            });

            it('Has "disabled" style', function () {
                var toutWrapper = homeView.$el.find('#sales-connection-links-region .center');
                expect($(toutWrapper).hasClass('disabled')).to.be.true;
            });
        });

        describe ('"only Care Solutions Products" link', function () {

            describe('for advanced home user', function () {

                it ('should displayed', function () {
                    userChannel.reply('hasCapability', function (capability) {
                        var hasCapability;
                        if (capability === 'IPipeline_iGO' 
                            || capability === 'Home_Office'
                            || capability === 'OLS:HO:OVERRIDE:IGO.EAPP' ) {
                            hasCapability = true;
                        } 
                        return hasCapability;
                    });

                    homeView = getHomeView();
                    var link = homeView.$el.find('#care-solutions-products-link');
                    expect(link).to.be.lengthOf(1);
                    expect(link.text()).to.contain('Only Care Solutions Products');
                });
            });

            describe('not an advanced home user', function () {
                it ('should not displayed', function () {
                    userChannel.reply('hasCapability', function (capability) {
                        var hasCapability;
                        if (capability === 'IPipeline_iGO' 
                            || capability === 'Home_Office' ) {
                            hasCapability = true;
                        } 
                        return hasCapability;
                    });

                    homeView = getHomeView();
                    var link = homeView.$el.find('#care-solutions-products-link');
                    expect(link).to.be.lengthOf(0);
                });
            });

            describe('an advanced home user, but no IPipeline_iGO capability', function () {
                it ('should not displayed', function () {
                    userChannel.reply('hasCapability', function (capability) {
                        var hasCapability;
                        if ( capability === 'Home_Office'
                            || capability === 'OLS:HO:OVERRIDE:IGO.EAPP'
                            || capability === 'OLS:HO:OVERRIDE:IGO.ILLUSTRATIONS'
                            || capability === 'OLS:HO:OVERRIDE:IGO.ADMIN' ) {
                            hasCapability = true;
                        } 
                        return hasCapability;
                    });

                    homeView = getHomeView();
                    var link = homeView.$el.find('#care-solutions-products-link');
                    expect(link).to.be.lengthOf(0);
                });
            });

        });
        
    }); // Sales connection Links (eApp/Illustration and Practice) tout

    describe('Education section', function () {
        var header;
        var well;

        it('should be displayed if user has "Pinpoint_LMS" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pinpoint_LMS';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "Pinpoint_LMS(disabled)" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Pinpoint_LMS(disabled)';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "Home_Office" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'Home_Office';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "WCM_CS_Not_Bank_HO_View" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'WCM_CS_Not_Bank_HO_View';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "WCM_IB_HO_View" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'WCM_IB_HO_View';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "WCM_Retail_HO_View" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'WCM_Retail_HO_View';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "OAMS_View" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'OAMS_View';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should be displayed if user has "OAMS_View(disabled)" capability', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability === 'OAMS_View(disabled)';
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(1);
            expect(well.length).to.equal(1);
        });

        it('should display in a three-column view if all three touts are displayed', function () {
            var capabilities = [
                'Pinpoint_LMS',
                'WCM_Retail_HO_View',
                'OAMS_View'
            ];
            var expectedCssClass = 'col-sm-4';
            userChannel.reply('hasCapability', function(capability) {
                return _.contains(capabilities, capability);
            });

            homeView = getHomeView();

            expect(homeView.model.get('educationColumnWidthClass')).to.equal(expectedCssClass);
        });

        it('should display in a three-column view if less than four touts are displayed',
            function () {
            var expectedCssClass = 'col-sm-4';

            userChannel.reply('hasCapability', function(capability) {
                return capability === 'OAMS_View';
            });

            homeView = getHomeView();

            expect(homeView.model.get('educationColumnWidthClass')).to.equal(expectedCssClass);
        });

        it('should NOT be displayed if user has none of the above capabilities', function () {
            userChannel.reply('hasCapability', function(capability) {
                return capability !== 'Pinpoint_LMS' &&
                    capability !== 'Pinpoint_LMS(disabled)' &&
                    capability !== 'Home_Office' &&
                    capability !== 'WCM_CS_Not_Bank_HO_View' &&
                    capability !== 'WCM_IB_HO_View' &&
                    capability !== 'WCM_Retail_HO_View' &&
                    capability !== 'OAMS_View' &&
                    capability !== 'OAMS_View(disabled)';
                    
            });

            homeView = getHomeView();

            header = homeView.$el.find('#education-development-header');
            well   = homeView.$el.find('#education-development-well');

            expect(header.length).to.equal(0);
            expect(well.length).to.equal(0);
        });

    }); // Education section

    describe('Center of Excellence Link tout model attributes', function () {

        it('model is set with "showCOELinkRegion" when user has "Pinpoint_LMS" ' +
            'capability', function () {

            userChannel.reply('hasCapability', function (capability) {
                return capability === 'Pinpoint_LMS';
            });

            homeView = getHomeView();

            expect(homeView.model.get('showCOELinkRegion')).to.be.true;
        });


        it('model is not set with "showCOELinkRegion" when user does not have "Pinpoint_LMS" ' +
            'capability', function () {

            userChannel.reply('hasCapability', function (capability) {
                return false;
            });

            homeView = getHomeView();

            expect(homeView.model.get('showCOELinkRegion')).to.be.false;
        });

        it('model should set with "showCOELinkRegionDisabled" when user has ' +
            '"Pinpoint_LMS(disabled)" capability', function () {
            userChannel.reply('hasCapability', function (capability) {
                return capability === 'Pinpoint_LMS(disabled)';
            });

            homeView = getHomeView();
            expect(homeView.model.get('showCOELinkRegionDisabled')).to.be.true;
        });

    });// Center of Excellence Link tout model attributes

    describe('OneAmerica Marketing Store(OAMS) tout attributes', function () {
        it('model is set with "showOAMSToutRegion" when user has "OAMS_View" ' +
            'capability', function () {

            userChannel.reply('hasCapability', function (capability) {
                return capability === 'OAMS_View';
            });

            homeView = getHomeView();

            expect(homeView.model.get('showOAMSToutRegion')).to.be.true;
        });


        it('model is not set with "showOAMSToutRegion" when user does not have "OAMS_View" ' +
            'capability', function () {

            userChannel.reply('hasCapability', function (capability) {
                return false;
            });

            homeView = getHomeView();

            expect(homeView.model.get('showOAMSToutRegion')).to.be.false;
        });

        it('model should set with "showOAMSToutRegionDisabled" when user has ' +
            '"OAMS_View(disabled)" capability', function () {
            userChannel.reply('hasCapability', function (capability) {
                return capability === 'OAMS_View(disabled)';
            });

            homeView = getHomeView();
            expect(homeView.model.get('showOAMSToutRegionDisabled')).to.be.true;
        });

        it('non delegate - WCM URL should match', function () {
            userChannel.reply('hasCapability', function (capability) {
                return capability === 'OAMS_View';
            });

            userChannel.reply('getImpersonatedWebId', function (capability) {
                return false;
            });
            
            homeView = getHomeView();
            expect(homeView.regionManager._regions.oamsRegion.currentView.options.wcmPath)
                .to.be.equal('touts/homepage/education-and-practice-development/oams');
        });

        it('delegate - WCM URL should match', function () {
            userChannel.reply('hasCapability', function (capability) {
                return capability === 'OAMS_View';
            });

            userChannel.reply('getImpersonatedWebId', function (capability) {
                return 'smith';
            });
            
            homeView = getHomeView();
            expect(homeView.regionManager._regions.oamsRegion.currentView.options.wcmPath)
                .to.be.equal('touts/homepage/education-and-practice-development/oams-delegate');
        });
    });

    describe ('Display delegate user access error message', function () {

        it('if "delegation" property of userModel is an empty array', function () {
            var homeAlertModel;
            userChannel.reply('hasCapability', function (capability) {
                if (capability === 'OLS:DELEGATE') {
                    return true;
                }
            });

            userChannel.reply('getDelegateTargets', function (capability) {
                return [];
            });

            homeView = getHomeView();

            homeAlertModel = homeView.model.get('alertMessage');
            expect(homeAlertModel).to.not.be.undefined;
            expect(homeAlertModel.message).to.be.contain(homeView.errors.noDelegateTargetUsers);
            expect(homeAlertModel.divClass).to.equal('alert-info');
        });
    }); // Display delegate user access error message

});

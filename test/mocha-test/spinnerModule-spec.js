/* global expect:false, Backbone:false */

var SpinnerModule = require('../dist/modules/waitIndicator/spinnerModule');
var helpers = require('./helpers/helpers');

describe('Spinner Module (modules/waitIndicator/spinnerModule.js)', function () {

    var showSpinnerSpy;
    var hideSpinnerSpy;
    var spinner;

    beforeEach(function () {
        showSpinnerSpy = this.sinon.spy(SpinnerModule.prototype, 'showSpinner');
        hideSpinnerSpy = this.sinon.spy(SpinnerModule.prototype, 'hideSpinner');
        spinner = new SpinnerModule();
    });

    afterEach(function () {
        showSpinnerSpy.restore();
        hideSpinnerSpy.restore();
    });

    it('waitIndicatorModule can be instantiated', function () {
        expect(spinner).to.be.an.instanceOf(Backbone.Marionette.Object);
    });

    it('should have wait Indicator Channel', function () {
        expect(spinner.spinnerChannel.channelName).to.equal('spinner');
    });

    it('should have wait Indicator Channel event "show" and "hide"', function () {
        expect(spinner.spinnerChannel._events).to.have.property('show');
        expect(spinner.spinnerChannel._events).to.have.property('hide');
    });

    it('should call respective method while trigger channel events"', function () {
        spinner.spinnerChannel.trigger('show', {
            viewScope : new Backbone.Marionette.ItemView({template: false})
        });
        expect(showSpinnerSpy).to.have.been.called;

        spinner.spinnerChannel.trigger('hide');
        expect(hideSpinnerSpy).to.have.been.called;
    });

    describe ('method showSpinner', function () {
        
        var rootView;

        before(function () {
            rootView = helpers.viewHelpers.createRootView();
        });

        after(function () {
            rootView.destroy();
        });

        it('Spinner in region of LayoutView ', function () {
            rootView.render();
            spinner.spinnerChannel.trigger('show', {
                viewScope  : rootView,
                regionName :'contentRegion'
            });
            var childViewEl = rootView.getChildView('contentRegion').$el;
            expect(childViewEl.hasClass('spinner-wrapper')).to.be.true;
            expect(childViewEl.find('.spinner')).to.have.lengthOf(1);
        });

        it('Spinner in LayoutView itself', function () {
            rootView.render();
            spinner.spinnerChannel.trigger('show', {
                viewScope  : rootView
            });
            var childViewEl = rootView.$el;
            expect(childViewEl.find('.spinner-wrapper')).to.have.lengthOf(1);
        });

        it('Spinner in ItemView', function () {
            rootView = helpers.viewHelpers.createRootView();

            rootView.render();
            var view = new Backbone.Marionette.ItemView({template:false});
            rootView.showChildView('contentRegion', view);
            spinner.spinnerChannel.trigger('show', {
                viewScope  : view
            });
            var childViewEl = view.$el;
            expect(childViewEl.find('.spinner-wrapper')).to.have.lengthOf(1);
        });

        it('Spinner in #contentView directly', function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            var object = new Backbone.Marionette.Object({});
            spinner.spinnerChannel.trigger('show', {
                viewScope  : object
            });
            var childViewEl = rootView.$el;
            expect(childViewEl.find('#contentView .spinner-wrapper')).to.have.lengthOf(1);

            spinner.spinnerChannel.trigger('hide', object);
            expect(childViewEl.find('#contentView .spinner-wrapper')).to.have.lengthOf(0);
        });

    });

    describe('method hideSpinner', function () {

        var rootView;

        before(function () {
            rootView = helpers.viewHelpers.createRootView();
        });

        after(function () {
            rootView.destroy();
        });

        it('Remove spinner from ItemView', function () {
            rootView.render();
            var view = new Backbone.Marionette.ItemView({template:false});
            rootView.showChildView('contentRegion', view);
            spinner.spinnerChannel.trigger('show', {
                viewScope  : view
            });
            var childViewEl = view.$el;

            // Prove that it is showing so we can hide it
            expect(childViewEl.find('.spinner-wrapper')).to.have.lengthOf(1);

            spinner.spinnerChannel.trigger('hide', view);
            expect(childViewEl.find('.spinner-wrapper')).to.have.lengthOf(0);
        });

        it('Remove spinner from ContentView', function () {
            rootView.render();
            var object = new Backbone.Marionette.Object({});
            spinner.spinnerChannel.trigger('show', {
                viewScope  : object
            });
            var childViewEl = rootView.$el;

            // Prove it is showing so we can hide it
            expect(childViewEl.find('#contentView .spinner-wrapper')).to.have.lengthOf(1);

            spinner.spinnerChannel.trigger('hide', object);
            expect(childViewEl.find('#contentView .spinner-wrapper')).to.have.lengthOf(0);
        });
    });

});

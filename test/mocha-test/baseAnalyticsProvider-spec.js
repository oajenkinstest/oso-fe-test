/* global expect:false */

var BaseAnalyticsProvider = require('../dist/modules/analytics/providers/baseAnalyticsProvider');

describe('Base Analytics Provider ' +
    '(modules/analytics/providers/baseAnalyticsProvider.js)', function () {

    var ExtendedProvider;
    var provider;

    beforeEach(function () {
        ExtendedProvider = BaseAnalyticsProvider.extend({});
        provider = new ExtendedProvider();
    });

    afterEach(function () {
        ExtendedProvider = null;
        provider = null;
    });

    it('exists', function () {
        expect(BaseAnalyticsProvider).to.exist;
    });

    it('is extendable', function () {
        provider.setUser('USER10001');
        expect(provider.webId).to.equal('USER10001');
    });

    describe ('setUser method', function () {
        it('exists',function () {
            expect(provider.setUser).to.exist;
        });

        it('sets value for "webId"', function () {
            provider.setUser('USER10001');
            expect(provider.webId).to.equal('USER10001');
        });

        it('returns empty string if input is undefined', function () {
            provider.setUser();
            expect(provider.webId).to.equal('');
        });
    });

    describe('trackAction method', function () {
        it('exists', function () {
            expect(provider.trackAction).to.exist;
        });
    });

    describe('trackException method', function () {
        it('exists', function () {
            expect(provider.trackException).to.exist;
        });
    });

    describe('trackView method', function () {
        it('exists', function () {
            expect(provider.trackView).to.exist;
        });
    });
});

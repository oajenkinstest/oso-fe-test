/* global expect:false */

var CommentsCharacterCountView = require(
    '../dist/pages/requirementSubmission/views/comments-character-count-v'
);

var RequirementSubmissionFormModel = require(
    '../dist/pages/requirementSubmission/models/requirement-submission-form-m'
);

var RequirementSubmissionFormViewModel = require(
    '../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm'
);

describe('Requirement Submission Page - Comments Character Count View ' +
    '(pages/requirementSubmission/views/comments-character-count-v.js)', function () {

    var commentsLength = 50;
    var domainModel;
    var view;

    beforeEach(function () {
        domainModel    = new RequirementSubmissionFormModel();

        view = new CommentsCharacterCountView({
            model : new RequirementSubmissionFormViewModel({
                commentsLength : Number(commentsLength),
                domainModel    : domainModel,
                policyId       : 1234567890
            })
        });

        view.render();
    });

    afterEach(function () {
        view.destroy();
    });

    it('exists', function () {
        expect(CommentsCharacterCountView).to.exist;
    });

    it('Displays the commentsLength value from the model', function () {
        var expected = commentsLength.toString();
        expect(view.$el.text().trim()).to.equal(expected);
    });

    describe('initialize method', function () {

        it('exists as a function', function () {
            expect(view.initialize).to.exist.and.be.a('function');
        });

        it('throws an error if not passed a model or policyId', function () {
            var expectedErrorMsg = CommentsCharacterCountView.prototype.errors.invalidOptions;
            var fn = function () {
                new CommentsCharacterCountView();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedErrorMsg);
        });

        it('creates model if passed policyId', function () {
            view.destroy();
            view = new CommentsCharacterCountView({
                policyId : 987656789
            });

            expect(view.model).to.exist;
        });

    });

});

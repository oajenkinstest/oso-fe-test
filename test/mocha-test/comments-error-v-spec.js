/* global expect:false */


var CommentsErrorView = require('../dist/pages/requirementSubmission/views/comments-error-v');

var RequirementSubmissionFormModel = require(
    '../dist/pages/requirementSubmission/models/requirement-submission-form-m'
);

var RequirementSubmissionFormViewModel = require(
    '../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm'
);

describe('Requirement Submission Page - Comments Error View' +
    '(pages/requirementSubmission/views/comments-error-v.js)', function () {

    var commentsLength = 50;
    var domainModel;
    var view;

    beforeEach(function () {
        domainModel = new RequirementSubmissionFormModel();

        view = new CommentsErrorView({
            model : new RequirementSubmissionFormViewModel({
                policyId       : 1234567890,
                commentsLength : Number(commentsLength),
                domainModel    : domainModel
            })
        });

        view.render();
    });

    afterEach(function () {
        view.destroy();
    });


    it('exists', function () {
        expect(CommentsErrorView).to.exist;
    });

    it('Container div should be hidden when "commentsErrorText" is ' +
        'undefined in the model', function () {

        expect(view.$el.hasClass('hidden'));
    });

    it('Container div should have "text-error" class applied when ' +
        '"commentsErrorText" is defined in the model', function () {
        view.model.set('commentsErrorText', 'Too much typing you did, hmmm?');

        expect(view.$el.find('div').hasClass('text-error')).to.be.true;
    });

    it('Container div should NOT be hidden when "commentsErrorText" is ' +
        'present in the model', function() {

        view.model.set('commentsErrorText', 'Hiya!');

        expect(view.$el.hasClass('hidden')).to.be.false;
    });

    describe('initialize method', function () {

        it('throws an error if not passed a model or policyId', function () {
            var expectedErrorMsg = CommentsErrorView.prototype.errors.invalidOptions;
            var fn = function () {
                new CommentsErrorView();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedErrorMsg);
        });

        it('creates model if passed policyId', function () {
            view.destroy();
            view = new CommentsErrorView({
                policyId : 987656789
            });

            expect(view.model).to.exist;
        });

    });     // initialize method

});

/* global describe: false, expect:false */

/**
 * Data utility module spec
 */

var dataUtils = require('../dist/utils/utils-data');

describe('Data Utilities (utils/utils-data)', function () {

    describe('isImplemented method', function () {
        
        it('is a function', function () {
            expect(dataUtils.isImplemented).to.be.a('function');
        });

        it('does not throw an error if the object is undefined', function () {
            var fn = function () {
                dataUtils.isImplemented(undefined);
            };
            expect(fn).not.to.throw(Error);
        });

        it('returns true if the object does not have a "dataAvailability" property', function () {
            expect(dataUtils.isImplemented({'foo':123})).to.equal(true);
        });

        it('returns true if the object has a "dataAvailability" property that is set to any ' +
            'value other than "notImplemented"', function () {
            expect(dataUtils.isImplemented({ 'dataAvailability': 'notAvailable' })).to.equal(true);
        });

        it('returns false if the object has a "dataAvailability" property of "notImplemented"', function () {
            expect(dataUtils.isImplemented({ 'dataAvailability': 'notImplemented' })).to.equal(false);
        });
    });

});
/* global expect:false, Marionette:false, $:false, _:false, Backbone:false */

var helpers = require('./helpers/helpers');
var behaviors = require('../dist/modules/behaviors/index');

describe('Behaviors Module (modules/behaviors/index.js)', function () {

    it('exists', function () {
        expect(behaviors).to.exist;
    });

    it('should use default behaviors when no initialization parameters are passed', function () {
        _.each(_.keys(behaviors.behaviors), function(behavior) {
            expect(behaviors.behaviors[behavior]).to.be.a('function');
        });
    });

    it('contains a reference to the default implementation of ' +
        '#Marionette.Behaviors.behaviorsLookup', function () {
        expect(behaviors.originalBehaviorsLookup).to.be.a('function');
    });

    describe('surveyMonkeyButton.openSurveyInNewWindow method', function () {

        it('is a function', function () {
            expect(behaviors.behaviors.surveyMonkeyButton.prototype.openSurveyInNewWindow)
                .to.be.a('function');
        });

        it('should call window.open with "https://www.surveymonkey.com/r/Q5BNNH5"', function () {
            var windowSpy   = this.sinon.spy(window, 'open');
            var expectedUrl = 'https://www.surveymonkey.com/r/Q5BNNH5';

            behaviors.behaviors.surveyMonkeyButton.prototype.openSurveyInNewWindow();

            expect(windowSpy).to.have.been.calledWith(expectedUrl);

            windowSpy.restore();
        });
    });

    describe('wcmContentLinks Behavior', function () {

        describe('wcmContentLinks.isValidFileExtension method', function () {
            var setInvalidLink;
            var validLink;

            beforeEach(function () {
                setInvalidLink = function(ext) {
                    return '/wps/wcm/connect/some/really/long/path/with/invalid/extension.' + ext;
                };

                validLink   = '/wps/wcm/connect/not/so/long/path/with/no/extension';
            });

            afterEach(function () {
                setInvalidLink = null;
                validLink      = null;
            });

            it('is a function', function () {
                expect(behaviors.behaviors.wcmContentLinks.prototype.isViewableFileExtension)
                    .to.be.a('function');
            });

            it('should return FALSE when a link with the ".PDF" extension is passed', function () {
                var pdfLink = setInvalidLink('pdf');

                expect(behaviors.behaviors.wcmContentLinks.prototype.isViewableFileExtension(pdfLink))
                    .to.be.false;
            });

            it('should return FALSE when a link with the ".EXE" extension is passed', function () {
                var exeLink = setInvalidLink('exe');

                expect(behaviors.behaviors.wcmContentLinks.prototype.isViewableFileExtension(exeLink))
                    .to.be.false;
            });

            it('should return FALSE when a link with the ".ZIP" extension is passed', function () {
                var zipLink = setInvalidLink('zip');

                expect(behaviors.behaviors.wcmContentLinks.prototype.isViewableFileExtension(zipLink))
                    .to.be.false;
            });

            it('should return TRUE when a link without a file extension is passed', function () {
                expect(behaviors.behaviors.wcmContentLinks.prototype.isViewableFileExtension(validLink))
                   .to.be.true;
            });
        }); // wcmContentLinks.isValidFileExtension method

        describe('wcmContentLinks.hasValidFileIcon method', function () {
            var genericMarkup;
            var pdfMarkup;
            var zipMarkup;

            before(function () {
                genericMarkup = '<div><a href="/wps/wcm/connect/not/so/long/path/with/no/extension">' +
                        'Test</a> <i class="fa gray-80 fa-file-foo-o"></i></div>';

                pdfMarkup = '<div><a href="/wps/wcm/connect/not/so/long/path/with/no/extension">' +
                    'Test</a>  <i class="fa gray-80 fa-file-pdf-o"></i></div>';

                zipMarkup = '<div><a href="/wps/wcm/connect/not/so/long/path/with/no/extension">' +
                    'Test</a>  <i class="fa gray-80 fa-file-archive-o"></i></div>';
            });

            after(function () {
                pdfMarkup     = null;
                genericMarkup = null;
            });

            it('is a function', function () {
                expect(behaviors.behaviors.wcmContentLinks.prototype.iconIndicatesViewableContent)
                   .to.be.a('function');
            });

            it('should return TRUE when passed a link without an <i> element next to it', function () {
                var noIconMarkup = '<div><a href="/wps/wcm/connect/file">Test</a><p>Hello</p>' +
                    '</div>';
                var $link = $('a', noIconMarkup);

                expect(behaviors.behaviors.wcmContentLinks.prototype.iconIndicatesViewableContent($link))
                    .to.be.true;
            });

            it('should return TRUE when passed a link with an icon which does NOT identify ' +
                'the target file with a ZIP or PDF file', function () {
                var $link = $('a', genericMarkup);

                expect(behaviors.behaviors.wcmContentLinks.prototype.iconIndicatesViewableContent($link))
                    .to.be.true;
            });

            it('should return FALSE when passed a link with an icon which identifies ' +
                ' the target file as a PDF', function () {
                var $link = $('a', pdfMarkup);

                expect(behaviors.behaviors.wcmContentLinks.prototype.iconIndicatesViewableContent($link))
                    .to.be.false;
            });

            it('should return FALSE when passed a link with an icon which identifies ' +
                ' the target file as a ZIP', function () {
                var $link = $('a', zipMarkup);

                expect(behaviors.behaviors.wcmContentLinks.prototype.iconIndicatesViewableContent($link))
                    .to.be.false;
            });

        }); // wcmContentLinks.hasValidFileIcon method
    }); // wcmContentLinks Behavior

    describe('wcmContentLinks.handleBackLinkClick method', function () {

        beforeEach(function () {
            helpers.viewHelpers.startBBhistory();
        });

        afterEach(function () {
            helpers.viewHelpers.stopBBhistory();
        });

        it('should call `Backbone.history.back()` method', function () {
            var backStub  = this.sinon.stub(Backbone.history.history, 'back');
            var fakeEvent = {
                preventDefault : _.noop
            };

            behaviors.behaviors.wcmContentLinks.prototype.handleBackLinkClick(fakeEvent);

            expect(backStub).to.have.been.calledOnce;
            backStub.restore();
        });
    });

    describe('wcmContentGrid Behavior', function () {
        var rowCountBeforeRender;
        var view;
        var View = Marionette.ItemView.extend({
            behaviors : {
                wcmContentGrid : {}
            },
            template  : _.template(
                '<div class="wcm-grid-wrapper-top"><div class="wcm-grid-section">' +
                'Intro to WCM Grid</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 1</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 2</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 3</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 4</div>' +
                '</div>'
            )
        });

        beforeEach(function () {
            view = new View();
            rowCountBeforeRender = view.$el.find('.row').length;
            view.render();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should wrap two "wcm-grid-item" classes with a "row" class', function () {
            var rowCountAfterRender = view.$el.find('.row').length;

            expect(rowCountBeforeRender).to.equal(0);
            expect(rowCountAfterRender).to.equal(2);
        });

        it('should wrap "wcm-grid-item" content correctly', function () {
            var actualResult   = view.$el.html();
            var expectedResult = '<div class="wcm-grid-wrapper-top">' +
                '<div class="wcm-grid-section">Intro to WCM Grid' +
                '<div class="row">' +
                '<div class="col-md-6 wcm-grid-item">Content 1</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 2</div></div>' +
                '<div class="row">' +
                '<div class="col-md-6 wcm-grid-item">Content 3</div>' +
                '<div class="col-md-6 wcm-grid-item">Content 4</div>' +
                '</div></div></div>';

            expect(actualResult).to.equal(expectedResult);
        });

    }); // wcmContentGrid Behavior

    describe('wcmModalWindow Behavior', function () {
        var modal;
        var view;
        var View = Marionette.ItemView.extend({
            behaviors : {
                wcmModalWindow : {}
            },
            template : _.template(
                '<div class="modal help-modal fade" id="wcmModal">' +
                '<div class="modal-dialog" role="document"><div class="modal-content">' +
                '<div class="modal-header">I am header content.</div><div class="modal-body">' +
                'I am body content.</div><div class="modal-footer">' +
                '<button type="button" class="btn btn-primary margin-btm-10" id="btnClose"' +
                'data-dismiss="modal" autofocus="yes">Close</button></div></div></div></div>'
            )
        });

        beforeEach(function () {
            view = new View();
            view.render();

            modal = view.$el.find('#wcmModal');
        });

        afterEach(function () {
            view.destroy();
        });

        it('Empties the "modal-header" of modal window with "#wcmModal" id', function () {
            var headerContentLength = modal.find('.modal-header').html().length;

            modal.trigger('hidden.bs.modal');

            expect(headerContentLength).to.be.greaterThan(0);
            expect(modal.find('.modal-header').html().length).to.equal(0);
        });

        it('Empties the "modal-body" of modal window with "#wcmModal" id', function () {
            var bodyContentLength = modal.find('.modal-body').html().length;

            modal.trigger('hidden.bs.modal');

            expect(bodyContentLength).to.be.greaterThan(0);
            expect(modal.find('.modal-body').html().length).to.equal(0);
        });

    });     // wcmModalWindow Behavior

    describe('JumpLinks Behavior', function() {

        var View = Marionette.ItemView.extend({
            behaviors: {
                jumpLinks: {}
            },
            template: _.template(
                '<div><div id="jump-links"><a href="#test"></a><div><div id="test"></div></div>'
            )
        });
        var view;

        beforeEach(function() {
            view = new View();

            // give it a fake checkpoint module
            view._behaviors[0].checkpoint = {
                readCheckpoint: function () {
                    return {};
                },
                writeCheckpoint: this.sinon.stub()
            };

            view._behaviors[0].utils = {
                scrollTo: this.sinon.stub()
            };
        });

        afterEach(function() {
            view.destroy();
        });

        it('jumpLinkClick adds a subpage', function() {
            view.render();
            view.$el.find('#jump-links a').click();

            expect(view._behaviors[0].checkpoint.writeCheckpoint).to.have.been.calledWith(
                { subpages: ['test'] }
            );

            // Would like to test 'calledWith', but the jQuery object doesn't match.
            // var $test = view.$el.find('#test');
            expect(view._behaviors[0].utils.scrollTo).to.have.been.called;
        });
    }); // JumpLinks Behavior

    describe('smartScrollTables._onPanelShown Behavior', function () {
        var smartScrollTablesPrototype = behaviors.behaviors.smartScrollTables.prototype;
        var fakeEvent    = {
            currentTarget : 'foo',
            meaningOfLife : 42
        };

        it('should call _tablesSmartScroll with event object', function () {
            var tablesSmartScrollStub = this.sinon.stub(smartScrollTablesPrototype,
                '_tablesSmartScroll');

            // pass the fake event to _onPanelShown
            smartScrollTablesPrototype._onPanelShown(fakeEvent);

            expect(tablesSmartScrollStub).to.have.been.calledWith(fakeEvent);
        });
    });

    describe('floatTableHeaders Behavior', function () {

        describe('_getTranslateYValue function', function () {

            var _getTranslateYValue = behaviors.behaviors.floatTableHeaders.prototype
                ._getTranslateYValue;

            it('returns NULL when "transform" value is "none"', function () {
                var obj       = $('<div></div>').css('transform', 'none');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.be.null;
            });

            it('returns NULL value from invalid "transform" property', function () {
                var obj           = $('<div></div>').css('transform', 'matrix(Blue pill/red pill)');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.be.null;
            });

            // After upgrading to jQuery 3, I could not figure out how to make these tests work
            // after 2 days of trying. No matter what I tried, the `obj.css('transform')` calls
            // (including the ones with browser prefixes) in the _getTranslateYValue function
            // always returned undefined. It works fine even in jQuery v2.2.4, but fails starting
            // at v3.0.0. I have verified that the floating table header feature still works in
            // browsers (tested Safari 11.0.2, FF 57, and Chrome 63), so I'm just going to
            // disable these tests. -- RKC, Jan 16, 2018
            it.skip('returns the correct value from "-moz-transform" property', function () {
                var expectedValue = -15835;
                var obj           = $('<div></div>')
                    .css('-moz-transform', 'matrix(1, 1, 1, 1, 1, -15835)');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.equal(expectedValue);
            });

            it.skip('returns the correct value from "-ms-transform" property', function () {
                var expectedValue = -15835;
                var obj           = $('<div></div>')
                    .css('-ms-transform', 'matrix(1, 21, 51, 1, 1, -15835)');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.equal(expectedValue);
            });

            it.skip('returns the correct value from "-o-transform" property', function () {
                var expectedValue = -15835;
                var obj           = $('<div></div>')
                    .css('-o-transform', 'matrix(1, 21, 51, 1, 1, -15835)');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.equal(expectedValue);
            });

            it.skip('returns the correct value from "-o-transform" property', function () {
                var expectedValue = -15835;
                var obj           = $('<div></div>')
                    .css('-o-transform', 'matrix(1, 21, 51, 1, 1, -15835)');
                var returnVal = _getTranslateYValue(obj);

                expect(returnVal).to.equal(expectedValue);
            });
        });
    });
});

/* global describe, $, expect, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

describe('Policy Detail View - Policy Identification Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var rootView;
    var view;

    var setView = function() {
        var newview = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        ajaxStub = this.sinon.stub($, 'ajax');
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    describe('Happy path tests with typical data', function () {
        var copyOfPolicyDetail;
        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('PolicyDetailView exists', function () {
            expect(PolicyDetailView).to.exist;
        });

        it('data should match', function() {
            var piElement = view.$el.find('.well-account-info');

            expect(piElement.find('#product .profile-info-value').text().trim())
                .to.equal('Liberty Select');

            expect(piElement.find('#company .profile-info-value').text().trim())
                .to.equal('AUL');

            expect(piElement.find('#writing-agent .profile-info-value h5:eq(0)').text().trim())
                .to.be.contain('Christopher E Ekstrom (dg33442)');
        });

        describe('contact mailto link', function () {
            describe ('case Manager', function () {
                it('data should match', function() {
                    var piElement = view.$el.find('.well-account-info');
                    expect(piElement.find('#case-manager .profile-info-value a:eq(0)')
                    .text().trim())
                    .to.equal('Barry Allen');

                    expect(piElement.find('#case-manager .profile-info-value a:eq(0)')
                    .attr('href').trim()).to
                    .equal('mailto:barry.allen@oa.com?subject=Policy%20%23720459750%20MAXWELL');
                });
            });

            describe ('policy Service Contact', function () {
                it('data should match', function () {
                    delete copyOfPolicyDetail.caseManager;
                    copyOfPolicyDetail.policyServiceContact = {
                        displayName: 'Barry Allen',
                        emailAddress: 'barry.allen@oa.com'
                    };

                    ajaxStub.yieldsTo(
                        'success',
                        copyOfPolicyDetail
                    );

                    view = setView();

                    var piElement = view.$el.find('.well-account-info');
                    expect(piElement.find('#policy-service-contact .profile-info-value a:eq(0)')
                    .text().trim())
                    .to.equal('Barry Allen');

                    expect(piElement.find('#policy-service-contact .profile-info-value a:eq(0)')
                    .attr('href').trim()).to
                    .equal('mailto:barry.allen@oa.com?subject=Policy%20%23720459750%20MAXWELL');

                });
            });
        });
    }); // Happy path tests with typical data

    describe('AWD rip tests', function () {

        before(function () {
            ajaxStub.yieldsTo(
                'success',
                helpers.policyData.pendingPolicyDetailAWDRip
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('data to exist for AWD rip', function() {
            var policyNumberText;

            policyNumberText = view.$el.find('#policy-number').parent('h3').text().trim();
            expect(helpers.viewHelpers.removeDuplicateWhitespace(policyNumberText))
                .to.equal('Policy #: Recently Received');

            expect(view.$el.find('.well-account-info .policy-name')).to.exist;
        });

        it('data should not exist for AWD rip', function() {
            expect(view.$el.find('#product').length).to.equal(0);
            expect(view.$el.find('#company').length).to.equal(0);
            expect(view.$el.find('#case-manager').length).to.equal(0);
        });
    });

    describe('Successfully displays the correct customer name and label based ' +
        'on the policy', function() {
        var copyOfAwd;
        var expectedName;
        var expectedLabel;
        var label;
        var name;

        it('AWD will used the "Name" label and a blank value if no customer '+
            'roles exist', function () {

            expectedLabel = 'Name:';
            expectedName  = '';

            copyOfAwd = $.extend(true,{},helpers.policyData.pendingPolicyDetailAWDRip);

            ajaxStub.yieldsTo('success', copyOfAwd);
            view = setView();

            label = view.$el.find('#policy-primary-name').parent().text().trim();
            name  = view.$el.find('#policy-primary-name').text();

            expect(label).to.contain(expectedLabel);
            expect(name).to.equal(expectedName);

            view.destroy();
        });

        it('AWD will use customers["0"] returned by the service', function () {

            expectedLabel = 'Name:';
            expectedName  = 'Joe The Insured';
            
            // add customers to AWD data
            copyOfAwd = $.extend(true,{},helpers.policyData.pendingPolicyDetailAWDRip);
            copyOfAwd.customers = {
                '0' : {
                    fullName  : expectedName,
                    firstName : 'Joe'
                }
            };

            ajaxStub.yieldsTo('success', copyOfAwd);
            view = setView();

            label = view.$el.find('#policy-primary-name').parent().text().trim();
            name  = view.$el.find('#policy-primary-name').text();

            expect(label).to.contain(expectedLabel);
            expect(name).to.equal(expectedName);
            view.destroy();
        });

        it('Label will be "Annuitant" and name will be first "Annuitant" role', function () {

            var copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            expectedLabel = 'Annuitant:';
            expectedName  = 'Johnny Socko and his Giant Robot';

            copyOfPolicyDetail.product.productTypeCategory = 'Annuity';

            ajaxStub.yieldsTo('success', copyOfPolicyDetail);

            view = setView();

            label = view.$el.find('#policy-primary-name').parent().text().trim();
            name  = view.$el.find('#policy-primary-name').text();

            expect(label).to.contain(expectedLabel);
            expect(name).to.equal(expectedName);
            view.destroy();
        });

        it('Label will be "Insured" and name will be first "Primary Insured" role', function () {

            var copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            expectedLabel = 'Insured:';
            expectedName  = 'Johnny Socko and his Giant Robot';

            // delete the 'Annuitant' role
            delete copyOfPolicyDetail.customerRoles.Annuitant;

            ajaxStub.yieldsTo('success', copyOfPolicyDetail);
            view = setView();

            label = view.$el.find('#policy-primary-name').parent().text().trim();
            name  = view.$el.find('#policy-primary-name').text();

            expect(label).to.contain(expectedLabel);
            expect(name).to.equal(expectedName);
            view.destroy();
        });

        it('If "Primary Insured" role does not exist for policy, ' +
            '"Insured" role will be used', function () {

            var copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            expectedLabel = 'Insured:';
            expectedName  = 'Johnny Socko and his Giant Robot';

            // Copy the "Primary Insured" to "Insured" and delete "Primary Insured"
            copyOfPolicyDetail.customerRoles.Insured =
                copyOfPolicyDetail.customerRoles['Primary Insured'];

            delete copyOfPolicyDetail.customerRoles['Primary Insured'];
            delete copyOfPolicyDetail.customerRoles.Annuitant;

            ajaxStub.yieldsTo('success', copyOfPolicyDetail);
            view = setView();

            label = view.$el.find('#policy-primary-name').parent().text().trim();
            name  = view.$el.find('#policy-primary-name').text();

            expect(label).to.contain(expectedLabel);
            expect(name).to.equal(expectedName);
            view.destroy();
        });
    });

    describe('Issue Date field', function () {
        var oldIssuedDate;
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
            oldIssuedDate = copyOfPolicyDetail.application.issued;
            copyOfPolicyDetail.application.issued = '2017-03-06T10:14:00Z';

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('should be displayed when application.issued is returned', function () {
            var issueDateField = view.$el.find('#issue-date');

            expect(issueDateField.length).to.equal(1);
        });

        it('should be displayed in MM/DD/YYYY format', function () {
            var issueDateField = view.$el.find('#issue-date');
            var issueDateText = issueDateField.find('.profile-info-value').text().trim();

            expect(issueDateText).to.equal('03/06/2017');
        });

        it('should NOT be displayed if application.issued is NOT returned', function () {
            copyOfPolicyDetail.application.issued = oldIssuedDate;
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            expect(view.$el.find('#issue-date').length).to.equal(0);
        });

    });     // Issue Date field


    describe('Termination Date field', function () {
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
            copyOfPolicyDetail.policyStatus.terminationDate = '2018-03-06';

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('should be displayed when policyStatus.terminationDate is exist', function () {
            var terminationDateField = view.$el.find('#termination-date');
            expect(terminationDateField.length).to.equal(1);
        });

        it('should be displayed in MM/DD/YYYY format', function () {
            var terminationDate = view.$el.find('#termination-date');
            var terminationDateText = terminationDate.find('.profile-info-value').text().trim();
            expect(terminationDateText).to.equal('03/06/2018');
        });

        it('should NOT be displayed if policyStatus.terminationDate is NOT exist', function () {
            delete copyOfPolicyDetail.policyStatus.terminationDate;
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();
            expect(view.$el.find('#termination-date').length).to.equal(0);
        });

    });     // Termination Date field

    describe('Data - Issue state and Resident State', function () {
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('Issue State should displayed', function () {
            var expectedValue = copyOfPolicyDetail.issueState;
            expect(view.$el.find('#issue-state')).to.be.lengthOf(1);
            expect(view.$el.find('#issue-state').text()).to.contain(expectedValue);
        });

        it('Resident State should displayed', function (){
            var expectedValue = copyOfPolicyDetail.residentState;
            expect(view.$el.find('#resident-state')).to.be.lengthOf(1);
            expect(view.$el.find('#resident-state').text()).to.contain(expectedValue);
        });

    });

    describe('Tax Qualification data', function () {
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('Tax Qualification Type should displayed', function () {
            var expectedValue = copyOfPolicyDetail.taxQualification.type;
            expect(view.$el.find('#tax-qualification-type')).to.be.lengthOf(1);
            expect(view.$el.find('#tax-qualification-type').text()).to.contain(expectedValue);
        });

        it('Tax Qualification description should displayed', function (){
            var expectedValue = copyOfPolicyDetail.taxQualification.description;
            expect(view.$el.find('#tax-qualification-descr')).to.be.lengthOf(1);
            expect(view.$el.find('#tax-qualification-descr').text()).to.contain(expectedValue);
            view.destroy();
        });

        it('Tax Qualification description should not displayed '+
                'if description not exist', function () {
            // Make sure that an unrelated part of the page is the same before and
            // after the data change. This shows that the change didn't just cause the
            // whole page to disappear.
            expect(view.$el.find('#resident-state')).to.be.lengthOf(1);

            delete copyOfPolicyDetail.taxQualification.description;
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            expect(view.$el.find('#tax-qualification-descr')).to.be.lengthOf(0);
            expect(view.$el.find('#resident-state')).to.be.lengthOf(1);
        });

    });

    describe('Servicing Agent vs Writing Agent display', function () {
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
        });

        it('Servicing Agent should display and hide Writing agent if '+
                '"application.paid" date crossed 30 days', function () {
            
            copyOfPolicyDetail.application.paid = '2017-02-05T12:50:00Z';

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            var servicingAgentElement = view.$el.find('#servicing-agent');
            var writingAgentElement = view.$el.find('#writing-agent');
            expect(servicingAgentElement).to.be.lengthOf(1);
            expect(writingAgentElement).to.be.lengthOf(0);  

            view.destroy();
        });

        it('Writing Agent should display and hide Servicing agent if '+
                '"application.paid" date with in 30 days', function () {
            copyOfPolicyDetail.activePolicy = true;
            copyOfPolicyDetail.application.paid = helpers.viewHelpers.getISODate('-', 2);
            
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            var servicingAgentElement = view.$el.find('#servicing-agent');
            var writingAgentElement = view.$el.find('#writing-agent');

            expect(servicingAgentElement).to.be.lengthOf(0);
            expect(writingAgentElement).to.be.lengthOf(1);

            view.destroy();
        }); 
    }); // Servicing Agent vs Writing Agent display


    describe('writing producers list sorting and displaying splitPercent', function () {
        var copyOfPolicyDetail;

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
        });

        it('writing producers list should contain split percent ', function () {
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            var writingAgentElement = view.$el.find('#writing-agent');
            expect(writingAgentElement.find('.profile-info-value h5:eq(0)').text())
                .to.be.contain('70%');
            expect(writingAgentElement.find('.profile-info-value h5:eq(1)').text())
                .to.be.contain('30%');

            view.destroy();
        });

        it('split percent should not displayed if there is only one producer', function () {
            copyOfPolicyDetail.writingProducers.splice(1,1);
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            var writingAgentElement = view.$el.find('#writing-agent');
            expect(writingAgentElement.find('.profile-info-value h5:eq(0)').text())
                .to.be.not.contain('70%');

            view.destroy();
        });
    });

    describe ('Show under construction message', function () {
        var copyOfActivePolicyDetail;

        before(function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
        });
        

        it('Should display if policyStatus.statusView is Active ', function () {
            var alertAttentionMessage;
            copyOfActivePolicyDetail.policyStatus.statusView = 'Active';
            ajaxStub.yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );
            view = setView();
            alertAttentionMessage = view.model.get('alertAttentionMessage');

            var element = view.$el.find('#'+alertAttentionMessage.id);
            var titleElement = element.find('strong');
            expect(element).to.be.lengthOf(1);
            expect(titleElement.text()).to.be.contain(alertAttentionMessage.title);
            expect(element.text()).to.be.contain(alertAttentionMessage.message);

            view.destroy();
        });

        it('Should not display if policyStatus.statusView is not exist ', function () {
            var alertAttentionMessage;
            delete copyOfActivePolicyDetail.policyStatus.statusView;
            ajaxStub.yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );
            view = setView();
            alertAttentionMessage = view.model.get('alertAttentionMessage');
            var element = view.$el.find('.alert-attention');
            expect(element).to.be.lengthOf(0);
            expect(alertAttentionMessage).to.be.undefined;
            view.destroy();
        });
        
    });

});

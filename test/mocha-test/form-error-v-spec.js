/* global expect:false */

var FormErrorView = require('../dist/pages/requirementSubmission/views/form-error-v');

var RequirementSubmissionFormModel = require(
    '../dist/pages/requirementSubmission/models/requirement-submission-form-m'
);
var RequirementSubmissionFormViewModel = require(
    '../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm'
);

describe('Requirement Submission Page - Form Error View ' +
    '(pages/requirementSubmission/views/form-error-v.js)', function () {
    var domainModel;
    var view;

    beforeEach(function () {
        domainModel = new RequirementSubmissionFormModel();

        view = new FormErrorView({
            model : new RequirementSubmissionFormViewModel({
                domainModel : domainModel,
                policyId    : 1234567890
            })
        });

        view.render();
    });

    afterEach(function () {
        view.destroy();
    });

    it('exists', function () {
        expect(FormErrorView).to.exist;
    });

    describe('modifies markup based on "formErrorText" property in model', function () {

        it('when "formErrorText" is NOT set, no error is displayed', function () {
            expect(view.model.get('formErrorText')).not.to.exist;
            expect(view.$el.find('.form-group').hasClass('has-error')).to.be.false;
            expect(view.$el.find('.help-block').hasClass('hidden')).to.be.true;
        });

        it('when "formErrorText" is set, the error message is displayed', function () {
            var expectedMessage = 'Danger Will Robinson!';
            var helpBlock;
            var formGroup;

            // update the formErrorText in the model
            view.model.set('formErrorText', expectedMessage);

            formGroup = view.$el.find('.form-group');
            helpBlock = view.$el.find('.help-block');

            expect(formGroup.hasClass('has-error')).to.be.true;
            expect(helpBlock.hasClass('hidden')).to.be.false;
            expect(helpBlock.text().trim()).to.equal(expectedMessage);
        });
    });  // modifies markup based on "formErrorText" property in model

    describe('initialize method', function () {

        it('exists as a function', function () {
            expect(view.initialize).to.exist.and.be.a('function');
        });

        it('throws an error if not passed a model or policyId', function () {
            var expectedErrorMsg = FormErrorView.prototype.errors.invalidOptions;
            var fn = function () {
                new FormErrorView();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedErrorMsg);
        });

        it('creates a model if passed policyId', function () {
            view.destroy();
            view = new FormErrorView({
                policyId : 987656789
            });

            expect(view.model).to.exist;
        });

    }); // initialize method

});

/* global expect, $, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');
var utils = require('../dist/utils/utils');

describe('Policy Detail - Billing/Payment Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var rootView;
    var view;
    // use a copy of the data so that objects appended to it aren't re-used in each test
    var copyOfPolicyDetail;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        copyOfPolicyDetail = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            copyOfPolicyDetail
        );

        view = new PolicyDetailView({
            stateObj: {
                policyId: 1234567890
            }
        });

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
        rootView.showChildView('contentRegion', view);
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    it('data should match', function() {
        var biElement = view.$el;

        expect(biElement.find('#billing-method .profile-info-value').text().trim())
            .to.equal('direct');
        
        expect(biElement.find('#cash-with-application .profile-info-value').text().trim())
            .to.equal('$51,953.34');
        
        expect(biElement.find('#paid-to-date .profile-info-value').text().trim())
            .to.equal('10/15/2016');

        expect(biElement.find('#income-settlement-option .profile-info-value').text().trim())
            .to.equal('');

        expect(biElement.find('#income-date .profile-info-value').text().trim())
            .to.equal('03/26/2010');

        expect(biElement.find('#consolidated-plan .profile-info-value').text().trim())
            .to.equal('Yes');

        expect(biElement.find('#next-index-date .profile-info-value').text().trim())
            .to.equal('12/15/2017');

        expect(biElement.find('#payment-amount .profile-info-value').text().trim())
            .to.equal('$51,953.34');

        expect(biElement.find('#billed-to-date .profile-info-value').text().trim())
            .to.equal('10/15/2017');

        expect(biElement.find('#total-deposits-to-date .profile-info-value').text().trim())
            .to.equal('$1,200.00');

        expect(biElement.find('#last-premium-date .profile-info-value').text().trim())
            .to.equal('10/15/2016');
    });

    describe('"cashWithApplication" value from the service', function () {

        var cashWithAppRow;
        var labelText;
        var value;

        beforeEach(function () {
            cashWithAppRow = view.$el.find('#cash-with-application');
            labelText      = cashWithAppRow.find('.profile-info-name').text().trim();
            value          = cashWithAppRow.find('.profile-info-value').text().trim();
        });

        it('text label should be "Premium Received"', function () {
            expect(labelText).to.equal('Premium Received');
        });

        it('value should be the dollar amount returned by the service', function () {
            var expected = utils.formatAsCurrency(
                copyOfPolicyDetail.billingDetail.cashWithApplication);

            expect(value).to.equal(expected);
        });
    });

    describe('Pay Period (premiumSchedule) value from service', function () {
        var premiumPayPeriod;
        var labelText;
        var value;

        before(function () {
            premiumPayPeriod    = view.$el.find('#premium-pay-period');
            labelText           = premiumPayPeriod.find('.profile-info-name').text().trim();
            value               = premiumPayPeriod.find('.profile-info-value').text().trim();
        });

        it('text label should be "Pay Period"', function () {
            expect(labelText).to.equal('Pay Period');
        });

        it('value should be the data returned by the service', function () {
            expect(value).to.equal(copyOfPolicyDetail.billingDetail.premiumSchedule);
        });

        it('field should not displayed if data missing', function () {
            copyOfPolicyDetail.billingDetail.premiumSchedule = null;
            view = new PolicyDetailView({
                stateObj: {
                    policyId: 1234567890
                }
            });
            rootView.showChildView('contentRegion', view);
            expect(view.$el.find('#premium-pay-period').length).to.be.equal(0);
            copyOfPolicyDetail.billingDetail.premiumSchedule = value;
        });
    });

    describe('AWD rip', function () {

        it('section should not exist for AWD rip', function() {
            // Make sure the difference in data really changes the view
            expect(view.$el.find('#policy-billing-info').length).above(0);

            ajaxStub.yieldsTo(
                'success',
                helpers.policyData.pendingPolicyDetailAWDRip
            );

            view = new PolicyDetailView({
                stateObj: {
                    policyId: 1234567890
                }
            });
            rootView.showChildView('contentRegion', view);
            expect(view.$el.find('#policy-billing-info').length).to.equal(0);

            view.destroy();
        });
    });

    describe('"Section unavailable" note', function () {
        after (function () {
            copyOfPolicyDetail = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        });
        it ('should not display if billingDetail doesn\'t have '+
                'dataAvailability property', function () {
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view =  new PolicyDetailView({
                stateObj: {
                    policyId: 1234567890
                }
            });
            rootView.showChildView('contentRegion', view);
            expect(view.$el.find('#policy-billing-info #billing-section-unavailable'))
                .to.be.lengthOf(0);
        });

        it ('should display if billingDetail has dataAvailability '+
                'property with value notAvailable', function () {
           
            copyOfPolicyDetail.billingDetail = {
                dataAvailability : 'notAvailable'
            };

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = new PolicyDetailView({
                stateObj: {
                    policyId: 1234567890
                }
            });
            rootView.showChildView('contentRegion', view);
            expect(view.$el.find('#policy-billing-info #billing-section-unavailable'))
                .to.be.lengthOf(1);
        });
    }); // "Section unavailable" note
});

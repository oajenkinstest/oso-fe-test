/* global describe:false, $:false, expect:false, sinon:false */

var UserModel = require('../dist/models/user-m');

describe('User model (models/user-m.js)', function() {
    var userResponseBody  = {
        'webId'      : '123456',
        'producerId' : 8675309,
        'displayName': 'Jenny',
        'capabilities' : [
            'Producer_Performance_View'
        ]
    };
    var modelValidation = true;
    var user;
    var ajaxStub;

    before(function(){
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            userResponseBody
        );

        user = new UserModel({webId:'123abc'});
        user.fetch();
    });

    after(function(){
        user = null;
        ajaxStub.restore();
    });

    it('userModel object should exist', function() {
        expect(UserModel).to.exist;
    });

    it('default properties should exist', function() {
        expect(UserModel.prototype.defaults.displayName).to.be.a('string');
        expect(UserModel.prototype.defaults.capabilities).to.be.an('array');
        expect(UserModel.prototype.defaults.producerId).to.be.null;
    });

    it('hasCapability method should exist', function() {
        expect(user.hasCapability).to.exist;
    });

    it('Should match all properties received after fetching data from user API',
        function() {
            user.on('invalid', function(model, error) {
                modelValidation = false;
            });

            user.fetch();

            expect(user.get('webId')).to.be.a('string');
            expect(user.get('displayName')).to.be.a('string');
            expect(user.get('capabilities')).to.be.an('array');
            expect(user.get('producerId')).to.be.a('number');
        }
    );

    it('hasCapability method should return a boolean while invoking it', function() {
        expect(user.hasCapability('Producer_Performance_View')).to.equal(true);
    });

    it('Should pass validation before updating model with the data fetched from user API',
        function() {
            expect(modelValidation).to.equal(true);
    });

});

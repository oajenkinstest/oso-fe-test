/* global describe: false, expect:false */

var dataAvailabilityParser =
    require('../dist/pages/policy/models/parsers/policy-data-availability');

describe('Pending Policy Detail Model parser - Data Availability ' +
'(pages/policy/models/parsers/policy-data-availability.js)', function () {

    describe('deleteNotImplemented method', function () {

        it('is a function', function () {
            expect(dataAvailabilityParser.deleteNotImplemented).to.be.a('function');
        });

        it('does not throw an error if the object is undefined', function () {
            var fn = function () {
                dataAvailabilityParser.deleteNotImplemented(undefined);
            };
            expect(fn).not.to.throw(Error);
        });

        it('does not throw an error if the object is empty', function () {
            var fn = function () {
                dataAvailabilityParser.deleteNotImplemented({});
            };
            expect(fn).not.to.throw(Error);
        });

        it('does not throw an error if the object contains null properties', function () {
            var fn = function () {
                var inputObj = {
                    policyId: 2542736,
                    policyStatus: {
                        status: { dataAvailability: 'notAvailable' },
                        statusDetail: { dataAvailability: 'notImplemented' },
                        acordHoldingStatus: 'Unknown'
                    },
                    carrierCode: null,
                    lastRefreshDate: { something: null },
                    residentState: 'GA',
                    issueState: 'GA',
                    issueAge: { dataAvailability: null }
                };
                dataAvailabilityParser.deleteNotImplemented(inputObj);
            };
            expect(fn).not.to.throw(Error);
        });

        it('returns an object with the notImplemented properties deleted', function () {
            var inputObj = {
                policyId: 2542736,
                policyStatus: {
                    status: { dataAvailability: 'notAvailable' },
                    statusDetail: { dataAvailability: 'notImplemented' },
                    acordHoldingStatus: 'Unknown'
                },
                carrierCode: 'AUL',
                lastRefreshDate: { dataAvailability: 'notImplemented' },
                residentState: 'GA',
                issueState: 'GA',
                issueAge: { dataAvailability: 'notImplemented' }
            };
            var expectedOutput = {
                policyId: 2542736,
                policyStatus: {
                    status: { dataAvailability: 'notAvailable' },
                    acordHoldingStatus: 'Unknown'
                },
                carrierCode: 'AUL',
                residentState: 'GA',
                issueState: 'GA'
            };
            var actualOutput = dataAvailabilityParser.deleteNotImplemented(inputObj);
            expect(actualOutput).to.deep.equal(expectedOutput);
        });
    });

    describe('nullifyNotAvailableChildProperties method', function () {
        
        it('is a function', function () {
            expect(dataAvailabilityParser.nullifyNotAvailableChildProperties).to.be.a('function');
        });

        it('does not throw an error if the object is undefined', function () {
            var fn = function () {
                dataAvailabilityParser.nullifyNotAvailableChildProperties(undefined);
            };
            expect(fn).not.to.throw(Error);
        });

        it('returns an object with the notAvailable child properties nullified, ' +
            'but leaving the top-level notAvailable properties alone.', function () {
            var inputObj = {
                policyStatus: {
                    status: { dataAvailability: 'notAvailable' },
                    statusDetail: { dataAvailability: 'notAvailable' },
                    statusSortOrder: 1,
                    acordHoldingStatus: 'Unknown'
                },
                billingPayment: { dataAvailability: 'notAvailable' },
                policyServiceContact: {
                    displayName: 'Email us',
                    emailAddress: 'PolicyService@oneamerica.com'
                },
                investmentAllocation: { dataAvailability: 'notImplemented' }
            };
            var expectedOutput = {
                policyStatus: {
                    status: null,
                    statusDetail: null,
                    statusSortOrder: 1,
                    acordHoldingStatus: 'Unknown'
                },
                billingPayment: { dataAvailability: 'notAvailable' },
                policyServiceContact: {
                    displayName: 'Email us',
                    emailAddress: 'PolicyService@oneamerica.com'
                },
                investmentAllocation: { dataAvailability: 'notImplemented' }
            };
            var actualOutput = dataAvailabilityParser.nullifyNotAvailableChildProperties(inputObj);
            expect(actualOutput).to.deep.equal(expectedOutput);
        });
    });
});
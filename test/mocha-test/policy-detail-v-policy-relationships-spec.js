/* global describe, $, expect, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

/**
 * Return the Policy Relationship container div as a jQuery object
 */
var getPolicyRelationshipsSection = function(view) {
    return view.$el.find('h2:contains("Policy Relationships")').next('div');
};

describe('Policy Detail View - Policy Relationships Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var policyDataCopy;
    var rootView;
    var view;

    var setView = function() {
        var newview = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
        ajaxStub = this.sinon.stub($, 'ajax');

        policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    describe('Happy path tests with typical data', function () {

        before(function () {
            policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('section should be available', function () {
            expect(getPolicyRelationshipsSection(view).length).to.equal(1);
        });

        it('heading for Owner section should be "Insured/Owner" followed by count if ' +
            'productTypeCategory is NOT "Annuity"', function () {

            var headingText = getPolicyRelationshipsSection(view).find('.accordion-toggle:eq(0)')
                .text().trim();

            expect(helpers.viewHelpers.removeDuplicateWhitespace(headingText))
                .to.equal('Insured (2)');
        });

        it('Relationship column should fill with correct value', function () {
            var firstTable = getPolicyRelationshipsSection(view).find('table:eq(0)');

            expect(firstTable.find('tbody tr:eq(0) td:eq(3)').text().trim())
                .to.equal('Annuitant');

            expect(firstTable.find('tbody tr:eq(1) td:eq(3)').text().trim())
                .to.equal('Primary Insured');
        });

        it('"Owner" roles should appear in the "Other" section', function () {
            var ownerId       = policyDataCopy.customerRoles.Owner[0].customerId;
            var expectedName  = policyDataCopy.customers[ownerId].fullName;
            var section       = getPolicyRelationshipsSection(view).find('.panel-default:eq(2)');
            var headingText   = helpers.viewHelpers.removeDuplicateWhitespace(
                section.find('.accordion-toggle:eq(0)').text().trim()
            );
            var ownerRoleCell = section.find('table tbody td:contains("Owner")');

            expect(headingText).to.equal('Other (3)');
            expect(ownerRoleCell.prev().text().trim()).to.equal(expectedName);
        });
    }); // Happy path tests with typical data

    describe('Tests with modified data', function () {

        it('section should not display if data not exist', function () {

            // Verify that section is displayed if the data DOES exist
            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );
            view = setView();
            expect(getPolicyRelationshipsSection(view).length).above(0);

            // Modify the data and rerender the view
            var oldCustomerRoles = policyDataCopy.customerRoles;
            policyDataCopy.customerRoles = {};

            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view.destroy();
            view = setView(); // rerender the view with modified data

            // Show that the section is not present due to the modified data
            expect(getPolicyRelationshipsSection(view).length).to.equal(0);

            // Fix the modified data
            policyDataCopy.customerRoles = oldCustomerRoles;

            view.destroy();
        });


        it('heading for Owner section should be "Annuitant/ Owner" followed by count '
            + 'if productTypeCategory is "Annuity"', function () {
            var headingText;

            // Modify the data
            var oldProductTypeCategory = policyDataCopy.product.productTypeCategory;
            policyDataCopy.product.productTypeCategory = 'Annuity';

            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();

            headingText = getPolicyRelationshipsSection(view).find('.accordion-toggle:eq(0)')
                .text().trim();
            expect(helpers.viewHelpers.removeDuplicateWhitespace(headingText))
                .to.equal('Annuitant (2)');

            // Fix the modified data
            policyDataCopy.product.productTypeCategory = oldProductTypeCategory;

            view.destroy();
        });

        it('SSN column should display blank value if "taxId" is "NONE"', function () {
            
            // Modify the data
            var oldTaxId = policyDataCopy.customers['123'].taxId;
            policyDataCopy.customers['123'].taxId = 'NONE';

            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();

            var firstTable = getPolicyRelationshipsSection(view).find('table:eq(0)');

            expect(firstTable.find('tbody tr:eq(2) td:eq(4)').text().trim())
                .to.equal('');

            // Fix the modified data
            policyDataCopy.customers['123'].taxId = oldTaxId;

            view.destroy();
        });

        describe ('Policies that have repetitive payments (SLRP system)', function () {
            it('Hide section if policyStaus.onlySummaryDataAvailable flag is true', function (){
                policyDataCopy.policyStatus.onlySummaryDataAvailable = true;

                ajaxStub.yieldsTo(
                    'success',
                    policyDataCopy
                );

                view = setView();
                expect(getPolicyRelationshipsSection(view).length).to.equal(0);
                view.destroy();
            });

            it('Show if policyStaus.onlySummaryDataAvailable flag is false/undefined', function (){
                delete policyDataCopy.policyStatus.onlySummaryDataAvailable;

                ajaxStub.yieldsTo(
                    'success',
                    policyDataCopy
                );

                view = setView();
                expect(getPolicyRelationshipsSection(view).length).to.equal(1);
                view.destroy();
            });
        });
    }); // Tests with modified data

});

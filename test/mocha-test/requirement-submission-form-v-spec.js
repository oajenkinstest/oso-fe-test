/* global expect:false, Backbone:false, $:false */


var helpers = require('./helpers/helpers');
var RequirementSubmissionFormView =
    require('../dist/pages/requirementSubmission/views/requirement-submission-form-v');


describe('Requirement Submission Form View ' + 
    '(pages/requirementSubmission/views/requirement-submission-form-v.js)', function () {

    var ajaxStub;
    var confirmFileDeleteSpy;
    var copyOfPolicyDetail;
    var view;
    var viewOnBeforeShowStub;
    var rootView;

    before(function () {
        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
    });

    after(function () {
        rootView.destroy();
    });

    beforeEach(function () {
        confirmFileDeleteSpy =
            this.sinon.spy(RequirementSubmissionFormView.prototype, '_confirmFileDelete');

        copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            copyOfPolicyDetail
        );

        view = new RequirementSubmissionFormView({
            stateObj : {
                policyId : 1234567890
            }
        });
        viewOnBeforeShowStub = this.sinon.stub(view, 'onBeforeShow');
        rootView.showChildView('contentRegion', view);
    });

    afterEach(function () {
        ajaxStub.restore();
        copyOfPolicyDetail = null;
        confirmFileDeleteSpy.restore();
        viewOnBeforeShowStub.restore();
        if (view) {
            view.destroy();
        }
    });

    it('view exists', function () {
        expect(RequirementSubmissionFormView).to.exist;
    });

    it('should render view without any error', function() {
        expect(view.isRendered).to.be.true;
    });

    describe('initialize method', function () {

        describe('When NOT passed either a model or policyId', function () {
            var errorMsg = RequirementSubmissionFormView.prototype.errors.policyNumberMissing;

            beforeEach(function () {
                view.destroy();
            });

            it('should trigger an error', function () {
                var errorHandlerSpy = this.sinon.spy();
                var errorChannel = Backbone.Radio.channel('error');

                errorChannel.on('showErrorPage', errorHandlerSpy);

                view = new RequirementSubmissionFormView();

                expect(errorHandlerSpy).to.have.been.calledOnce;
                expect(errorHandlerSpy).to.have.been.calledWith(errorMsg);
            });

            it('should call destroy() method', function () {
                var destroyMethodSpy =
                    this.sinon.spy(RequirementSubmissionFormView.prototype, 'destroy');

                view = new RequirementSubmissionFormView();

                expect(destroyMethodSpy).to.have.been.calledOnce;
            });

        });     // When NOT passed either a model or policyId

    });     // initialize method

    describe('_addFileField method', function () {
        var countAfterCall;
        var countBeforeCall;

        it('adds a new file input and container well to the form', function () {
            countBeforeCall = view.$el.find('input:file').length;

            view._addFileField();
            countAfterCall = view.$el.find('input:file').length;

            expect(countAfterCall).to.be.above(countBeforeCall);
        });

        it('does not add a new file input and container well to the form if a well ' +
            'container on which to append the new container does not already exist', function () {
            // remove the only well on the view
            view.$el.find('#file-input-group .well').remove();
            countBeforeCall = view.$el.find('input:file').length;

            view._addFileField();
            countAfterCall = view.$el.find('input:file').length;

            expect(countAfterCall).to.equal(countBeforeCall);
        });
    });     // _addFileField method

    describe('_changeFileLabelText method', function () {

        it('should change label text on existing label to "Remove File"', function () {
            var label;
            var labelTextAfterCall;
            var labelTextBeforeCall;

            label               = view.$el.find('input:file').closest('label');
            labelTextBeforeCall = label.text().trim();

            view._changeFileLabelText(label);

            labelTextAfterCall = label.text().trim();

            expect(labelTextBeforeCall).to.equal('Choose File');
            expect(labelTextAfterCall).to.equal('Remove File');
        });

        it('should NOT change label text if label passed in to method ' +
            'does not exist in the DOM', function () {
            var label;
            var labelTextAfterCall;
            var labelTextBeforeCall;

            label               = view.$el.find('input:file').closest('label');
            labelTextBeforeCall = label.text().trim();

            // remove the label
            view.$el.find('input:file').closest('label').remove();
            label = view.$el.find('input:file').closest('label');

            view._changeFileLabelText(label);

            labelTextAfterCall = label.text().trim();

            expect(labelTextBeforeCall).to.equal('Choose File');
            expect(labelTextAfterCall).to.be.empty;

        });

    });     // _changeFileLabelText method

    describe('_clearFileErrors method', function () {

        beforeEach(function () {
            // set an error on all file inputs AND an individual file error
            view._setErrorOnFileGroup('All files error!');
            view._setErrorOnLastFile({}, 'Individual file error');
        });

        it('should remove the "well-form-error" class from all wells', function () {
            var wellFormErrorCountAfter;
            var wellFormErrorCountBefore = view.$el.find('.well-form-error').length;

            view._clearFileErrors();

            wellFormErrorCountAfter = view.$el.find('.well-form-error').length;

            expect(wellFormErrorCountBefore).to.be.greaterThan(wellFormErrorCountAfter);
            expect(wellFormErrorCountAfter).to.equal(0);
        });

        it('should remove the "hidden" class from the last well', function () {
            var isHiddenAfter;
            var isHiddenBefore;

            var lastWell = view.$el.find('#file-input-group .well').last();

            isHiddenBefore = lastWell.hasClass('hidden');

            view._clearFileErrors();

            isHiddenAfter = lastWell.hasClass('hidden');

            expect(isHiddenBefore).to.be.true;
            expect(isHiddenAfter).to.be.false;
        });

        it('should remove "has-error" class from the "form-group"', function () {
            var hasErrorAfter;
            var hasErrorBefore;

            var formGroup = view.$el.find('#attachment-form-group');

            hasErrorBefore = formGroup.hasClass('has-error');

            view._clearFileErrors();

            hasErrorAfter = formGroup.hasClass('has-error');

            expect(hasErrorBefore).to.be.true;
            expect(hasErrorAfter).to.be.false;
        });

        it('should remove "text-error" from the <p> elements', function () {
            var textErrorCountAfter;
            var textErrorCountBefore;

            textErrorCountBefore = view.$el.find('p.text-error').length;

            view._clearFileErrors();

            textErrorCountAfter = view.$el.find('p.text-error').length;

            expect(textErrorCountBefore).to.be.greaterThan(textErrorCountAfter);
            expect(textErrorCountAfter).to.equal(0);
        });

        it('should call _setSubmitButtonState() with a boolean of TRUE', function () {
            var methodSpy = this.sinon.spy(view, '_setSubmitButtonState');

            view._clearFileErrors();

            expect(methodSpy).to.have.been.calledOnce;
            expect(methodSpy.getCalls()[0].args[0]).to.be.true;
        });

    });     // _clearFileErrors method

    describe('_confirmFileDelete method', function () {
        var event;
        var files;

        beforeEach(function () {
            files = [{
                name    : 'myfile.pdf',
                size    : 123456,
                type    : 'application/pdf',
                inputId : 'file1'
            }];

            // set files on the model
            view.model.set('files', files);

            // construct a quick-and-dirty event object
            event = {
                currentTarget  : view.$el.find('input:file').closest('label'),
                preventDefault : function () {  }
            };
        });

        it('should call preventDefault() on jQuery event', function () {
            var preventDefaultSpy = this.sinon.spy(event, 'preventDefault');
            view._confirmFileDelete(event);

            expect(preventDefaultSpy).to.have.been.calledOnce;
        });

        it('should be called when the "Remove File" button is clicked', function () {
            var label;

            // simulate a file having been added to the form
            label = view.$el.find('input:file').closest('label');
            view._changeFileLabelText(label);

            // make sure the view sees the changes
            view.bindUIElements();

            // simulate a click on the label
            label.trigger('click');

            expect(confirmFileDeleteSpy).to.have.been.calledOnce;

        });

        it('should set the file name on #confirm-file-remove-modal', function () {
            var label;
            var modalWindow;

            label       = view.$el.find('input:file').closest('label');
            modalWindow = view.$el.find('#confirm-file-remove-modal');

            view._changeFileLabelText(label);
            view.bindUIElements();

            label.trigger('click');

            expect(modalWindow.find('.modal-body>p>strong').text()).to.equal(files[0].name);
        });

        it('will NOT set the file name on #confirm-file-remove-modal if the inputId ' +
            'does not match a corresponding inputId in "files"', function () {
            var label;
            var modalWindow;

            label       = view.$el.find('input:file').closest('label');
            modalWindow = view.$el.find('#confirm-file-remove-modal');

            view._changeFileLabelText(label);
            view.bindUIElements();

            files[0].inputId = 'foo';
            view.model.set('files', files);

            label.trigger('click');

            expect(modalWindow.find('.modal-body>p>strong').text()).to.be.empty;
        });

    });     // _confirmFileDelete method

    describe('_getCommentsLengthRemaining method', function () {

        it('returns the difference between the number of characters in ' +
            'comments and the max allowed', function () {
            var expectedValue = 499;
            var result;

            view.$el.find('textarea').val('a');

            result = view._getCommentsLengthRemaining();

            expect(result).to.equal(expectedValue);
        });

        it('should be triggered when the text in the Comments section is changed', function () {
            var methodStub = this.sinon.stub(view, '_getCommentsLengthRemaining');

            view.$el.find('textarea').trigger('keyup');

            expect(methodStub).to.have.been.calledOnce;

            methodStub.restore();
        });

    });     // _getCommentsLengthRemaining method

    describe('_getFilesArrayFromInputs method', function () {

        it('if no files have been added to the form, returns an empty array', function () {
            var result = view._getFilesArrayFromInputs();

            expect(result).to.be.empty;
        });

        // TODO: Determine the best way to mock a file on the input

    });     // _getFilesArrayFromInputs method

    // TODO: Determine the best way to mock a file on the input
    //describe('_handelAddFile method', function () {
    //
    //});     // _handleAddFile method

    describe('_removeFadeClassFromModal method', function () {

        it('removes the "fade" css class from the "Cancel Submit Modal" window', function () {
            var modal             = view.ui.cancelSubmitModal;
            var hasFadeBeforeCall = modal.hasClass('fade');

            view._removeFadeClassFromModal();

            expect(hasFadeBeforeCall).to.be.true;
            expect(modal.hasClass('fade')).to.be.false;
        });
    });      // _removeFadeClassFromModal method

    describe('_removeFile method', function () {
        var clearFileErrorsSpy;
        var jQueryFindSpy;
        var fileDeleteBtn;
        var renumberFileInputsSpy;
        var updateModelSpy;

        beforeEach(function () {
            fileDeleteBtn = view.$el.find('#confirm-remove-file-button');

            clearFileErrorsSpy    = this.sinon.spy(view, '_clearFileErrors');
            jQueryFindSpy         = this.sinon.spy(view.$el, 'find');
            renumberFileInputsSpy = this.sinon.spy(view, '_renumberFileInputs');
            updateModelSpy        = this.sinon.spy(view, '_updateModel');

            // add the inputId to the file delete button
            fileDeleteBtn.data('inputId', 'file1');
            fileDeleteBtn.click();
        });

        it('should call _clearFileErrors()', function () {
            expect(clearFileErrorsSpy).to.have.been.calledOnce;
        });

        it('should find the file input based on the data attribute "inputId"', function () {
            expect(jQueryFindSpy).to.have.been.calledOnce;
            expect(jQueryFindSpy).to.have.been.calledWith('#file1');
        });

        it('will NOT find the file input if "inputId" does not exist as a ' +
            'data attribute', function () {
            // reset the spy
            jQueryFindSpy.resetHistory();

            // remove the data attribute and click the button
            fileDeleteBtn.removeData('inputId');
            fileDeleteBtn.click();

            expect(jQueryFindSpy).not.to.have.been.called;
        });

        it('should call _renumberFileInputs()', function () {
            expect(renumberFileInputsSpy).to.have.been.calledOnce;
        });

        it('should call _updateModel()', function () {
            expect(updateModelSpy).to.have.been.calledOnce;
        });

    });     // _removeFile method

    describe('_renumberFileInputs method', function () {

        var expectedValues = ['file1', 'file2', 'file3'];

        beforeEach(function () {
            var outOfOrderIds = ['file16', 'file21', 'file102'];

            // create 2 additional file input wells on the form
            view._addFileField();
            view._addFileField();

            // cause chaos by changing the name/id attributes on the file
            // input and the "for" label attribute on the corresponding label.
            view.$el.find('input:file').each(function (index) {
                var inputField = $(this);
                var newId      = outOfOrderIds[index];

                // change the ID and name on the input
                inputField.attr('id', newId);
                inputField.attr('name', newId);

                // change the label id
                inputField.parent().attr('for', newId);
            });

            // now call the method
            view._renumberFileInputs();
        });

        it('should rename label\'s "for" attribute', function () {

            // loop through the file inputs and check the label ID's
            view.$el.find('input:file').parent().each(function(index) {
                var labelFor       = $(this).attr('for');
                var expectedValue = expectedValues[index];

                expect(labelFor).to.equal(expectedValue);
            });

        });

        it('should rename the file input\'s "id" attribute', function () {

            // loop through the file inputs and check the label ID's
            view.$el.find('input:file').each(function(index) {
                var inputId       = $(this).attr('id');
                var expectedValue = expectedValues[index];

                expect(inputId).to.equal(expectedValue);
            });

        });

        it('should rename the file input\'s "name" attribute', function () {

            // loop through the file inputs and check the label ID's
            view.$el.find('input:file').each(function(index) {
                var inputName     = $(this).attr('name');
                var expectedValue = expectedValues[index];

                expect(inputName).to.equal(expectedValue);
            });

        });

    });     // _renumberFileInputs method

    describe('_setCommentsErrorStyling method', function () {
        var mockModel = {};

        describe('When "commentsErrorStyling" is set to TRUE on the model', function () {

            it('should add the "has-error" class to the comments container', function () {
                var hasErrorBeforeCall = view.ui.commentsContainer.hasClass('has-error');

                view._setCommentsErrorStyling(mockModel, true);

                expect(hasErrorBeforeCall).to.be.false;
                expect(view.ui.commentsContainer.hasClass('has-error')).to.be.true;
            });

            it('should add the "well-form-error" class to comments ".well"', function () {
                var well                    = view.ui.commentsContainer.find('.well');
                var hasErrorClassBeforeCall = well.hasClass('well-form-error');

                view._setCommentsErrorStyling(mockModel, true);

                expect(hasErrorClassBeforeCall).to.be.false;
                expect(well.hasClass('well-form-error')).to.be.true;
            });

        });     // When "commentsErrorStyling" is set to TRUE on the model

        describe('When "commentsErrorStyling" is set to FALSE on the model', function () {

            it('should add the "has-error" class to the comments container', function () {
                var hasErrorBeforeCall = view.ui.commentsContainer.hasClass('has-error');

                view._setCommentsErrorStyling(mockModel, false);

                expect(hasErrorBeforeCall).to.be.false;
                expect(view.ui.commentsContainer.hasClass('has-error')).to.be.false;
            });

            it('should add the "well-form-error" class to comments ".well"', function () {
                var well                    = view.ui.commentsContainer.find('.well');
                var hasErrorClassBeforeCall = well.hasClass('well-form-error');

                view._setCommentsErrorStyling(mockModel, false);

                expect(hasErrorClassBeforeCall).to.be.false;
                expect(well.hasClass('well-form-error')).to.be.false;
            });

        });     // When "commentsErrorStyling" is set to FALSE on the model

    });     // _setCommentsErrorStyling method

    describe('_setErrorOnFileGroup method', function () {
        var errorMsg;

        describe('When passed an error message', function () {

            beforeEach(function () {
                errorMsg = 'I am a fake!';

                // call _addFileField to have more than one file input field on the form
                view._addFileField();
            });

            it('should add "well-form-error" class to all of the file input "wells"', function () {
                var wells = view.$el.find('#file-input-group').children('.well');
                var well;

                wells.each(function () {
                    well = $(this);
                    expect(well.hasClass('well-form-error')).to.be.false;
                });

                view._setErrorOnFileGroup(errorMsg);

                wells.each(function () {
                    well = $(this);
                    expect(well.hasClass('well-form-error')).to.be.true;
                });

            });

            it('should hide the last well on the form', function () {
                var isHiddenBeforeCall;
                var lastWell = view.$el.find('#file-input-group').children('.well').last();

                isHiddenBeforeCall = lastWell.hasClass('hidden');

                view._setErrorOnFileGroup(errorMsg);

                expect(isHiddenBeforeCall).to.be.false;
                expect(lastWell.hasClass('hidden')).to.be.true;
            });

            it('should call _setSubmitButtonState with FALSE arg', function () {
                var methodSpy = this.sinon.spy(view, '_setSubmitButtonState');

                view._setErrorOnFileGroup(errorMsg);

                expect(methodSpy).to.have.been.calledOnce;
                expect(methodSpy.getCalls()[0].args[0]).to.be.false;

                methodSpy.restore();
            });

        });     // When passed an error message

        describe('When passed an empty error message', function () {

            beforeEach(function () {
                errorMsg = null;
            });

            it('should call _clearFileErrors()', function () {
                var clearFileErrorsStub = this.sinon.stub(view, '_clearFileErrors');

                view._setErrorOnFileGroup(errorMsg);

                expect(clearFileErrorsStub).to.have.been.calledOnce;

            });

        });     // When passed an empty error message

    });     // _setErrorOnFileGroup method

    describe('_setErrorOnLastFile method', function () {
        var attachmentFormGroup;
        var mockModel = {};
        var errorMsg;
        var lastWell;
        var pTag;

        beforeEach(function () {
            attachmentFormGroup = view.$el.find('#attachment-form-group');
            errorMsg            = 'Oops!';
            lastWell            = view.$el.find('#file-input-group .well').last();
            pTag                = lastWell.find('p');
        });

        describe('When "fileErrorText" is set on the model', function () {

            it('should add "has-error" CSS class to files container', function () {
                var hasErrorClassBefore = attachmentFormGroup.hasClass('has-error');

                view._setErrorOnLastFile(mockModel, errorMsg);

                expect(hasErrorClassBefore).to.be.false;
                expect(attachmentFormGroup.hasClass('has-error')).to.be.true;
            });

            it('should add "well-form-error" CSS class to the last "well" ' +
                'used by file inputs', function () {
                var hasWellFormErrorBefore = lastWell.hasClass('well-form-error');

                view._setErrorOnLastFile(mockModel, errorMsg);

                expect(hasWellFormErrorBefore).to.be.false;
                expect(lastWell.hasClass('well-form-error')).to.be.true;
            });

            it('should add "text-error" CSS class to the <p> tag in the last well', function () {

                var hasTextErrorBefore = pTag.hasClass('text-error');

                view._setErrorOnLastFile(mockModel, errorMsg);

                expect(hasTextErrorBefore).to.be.false;
                expect(pTag.hasClass('text-error')).to.be.true;
            });

            it('should set the text in the <p> tag to the error passed to the method', function () {
                var textBeforeCall = pTag.text();
                view._setErrorOnLastFile(mockModel, errorMsg);

                expect(textBeforeCall).not.to.equal(pTag.text());
                expect(pTag.text()).to.equal(errorMsg);
            });

        });     // When "fileErrorText" is set on the model

        describe('When "fileErrorText" is unset on the model', function () {

            it('should remove "has-error" CSS class to files container', function () {
                var clearFileErrorsStub = this.sinon.stub(view, '_clearFileErrors');

                view._setErrorOnLastFile(mockModel, null);

                expect(clearFileErrorsStub).to.have.been.calledOnce;
            });

        });     // When "fileErrorText" is unset on the model

    });     // _setErrorOnLastFile method

    describe('_setSubmitButtonState method', function () {
        var submitButton;

        beforeEach(function () {
            submitButton = view.$el.find('button:submit');
        });

        describe('When passed FALSE', function () {

            it('should set "disabled" CSS class on submit button', function () {
                var hasDisabledClassBefore = submitButton.hasClass('disabled');

                view._setSubmitButtonState(false);

                expect(hasDisabledClassBefore).to.be.false;
                expect(submitButton.hasClass('disabled')).to.be.true;
            });

            it('should set "disabled" attribute on submit button', function () {
                var disabledAttributeBefore = submitButton.attr('disabled');

                view._setSubmitButtonState(false);

                expect(disabledAttributeBefore).to.be.undefined;
                expect(submitButton.attr('disabled')).not.to.be.undefined;
            });

        });     // When passed FALSE

        describe('When passed TRUE', function () {

            beforeEach(function () {
                // start tests with a disabled submit button
                view._setSubmitButtonState(false);
            });

            it('should remove "disabled" CSS class on submit button', function () {
                var hasDisabledClassBefore = submitButton.hasClass('disabled');

                view._setSubmitButtonState(true);

                expect(hasDisabledClassBefore).to.be.true;
                expect(submitButton.hasClass('disabled')).to.be.false;
            });

            it('should remove "disabled" attribute on submit button', function () {
                var disabledAttributeBefore = submitButton.attr('disabled');

                view._setSubmitButtonState(true);

                expect(disabledAttributeBefore).not.to.be.undefined;
                expect(submitButton.attr('disabled')).to.be.undefined;
            });

        });     // When passed TRUE

    });     // _setSubmitButtonState method

    describe('_showAlertMessage method', function () {
        var response;

        beforeEach(function () {
            response = { status : 500 };
        });

        it('Spinner element should not exist', function () {
            view._showAlertMessage(view.model, response);
            expect(view.$el.find('.spinner-wrapper')).to.have.lengthOf(0);
        });

        it('should NOT update model if a status of 500 is NOT returned', function () {
            var attributesPrior = view.model.attributes;
            var attributesAfter;

            response.status = 404;

            view._showAlertMessage(view.model, response);

            attributesAfter = view.model.attributes;

            expect(attributesAfter).to.deep.equal(attributesPrior);
        });

        it('should set "alertMessage" on model when 500 status is returned', function () {
            var alertMessagePrior = view.model.get('alertMessage');
            var alertMessageAfter;

            view._showAlertMessage(view.model, response);

            alertMessageAfter = view.model.get('alertMessage');

            expect(alertMessagePrior).to.be.undefined;
            expect(alertMessageAfter).not.to.be.undefined;
        });

    });     // _showAlertMessage method

    describe('_submitClick method', function () {
        var domainModelSetStub;
        var domainModelValidateStub;
        var domainModelSaveStub;
        var setSubmitButtonStateStub;
        var updateModelStub;

        beforeEach(function () {
            domainModelSetStub       = this.sinon.stub(view.model.domainModel, 'set');
            domainModelValidateStub  = this.sinon.stub(view.model.domainModel, 'validate')
                .returns('Error');
            domainModelSaveStub      = this.sinon.stub(view.model.domainModel, 'save');
            setSubmitButtonStateStub = this.sinon.stub(view, '_setSubmitButtonState');
            updateModelStub          = this.sinon.stub(view, '_updateModel');

            view.$el.find('button:submit').click();
        });

        afterEach(function () {
            domainModelSetStub.restore();
            domainModelValidateStub.restore();
            domainModelSaveStub.restore();
            setSubmitButtonStateStub.restore();
            updateModelStub.restore();
        });

        it('should make a call to _updateModel', function () {
            expect(updateModelStub).to.have.been.called;
        });

        it('should make a call to set attributes on the domainModel', function () {
            expect(domainModelSetStub).to.have.been.called;
        });

        it('should make a call to validate() on the domainModel', function () {
            expect(domainModelValidateStub).to.have.been.called;
        });

        describe('if validation is successful', function () {

            beforeEach(function () {
                // simulate validation passing
                domainModelValidateStub.returns(null);
            });

            it('should make a call to save() on the domainModel if validation passes', function () {
                view.$el.find('button:submit').click();
                expect(domainModelSaveStub).to.have.been.called;
            });

            it('should show the spinner', function () {
                var spinnerShowSpy = this.sinon.spy();
                var spinnerChannel = Backbone.Radio.channel('spinner');

                spinnerChannel.on('show', spinnerShowSpy);

                view.$el.find('button:submit').click();
                expect(spinnerShowSpy).to.have.been.calledOnce;
            });

            it('should call _setSubmitButtonState()', function () {
                view.$el.find('button:submit').click();
                expect(setSubmitButtonStateStub).to.have.been.calledOnce;
            });

            it('should pass FALSE to _setSubmitButtonState()', function () {
                view.$el.find('button:submit').click();
                expect(setSubmitButtonStateStub).to.have.been.calledWith(false);
            });

        });     // if validation is successful

        it('should not make a call to save() on the domainModel if validation ' +
            'does NOT pass', function () {
            domainModelValidateStub.returns('Error message');
            view.$el.find('button:submit').click();

            expect(domainModelSaveStub).not.to.have.been.called;
        });

    });     // _submitClick method

    describe('_updateFileNameAndSize method', function () {
        var inputId   = 'file1';
        var fileName  = 'MyFakeFile.pdf';

        var fakeFiles;

        var pTag;
        var bToMbSpy;
        var bToMbReturnVal = '1.2';

        beforeEach(function () {
            fakeFiles = [
                {
                    inputId : inputId,
                    name    : fileName,
                    size    : 1258291
                }
            ];

            // add the fake file to the model
            view.model.set('files', fakeFiles);

            bToMbSpy = this.sinon.stub(view.utils, 'bytesToMegabytesForDisplay')
                .returns(bToMbReturnVal);

            pTag = view.$el.find('#' + inputId).parent().next('p');

            view._updateFileNameAndSize(inputId);
        });

        afterEach(function () {
            bToMbSpy.restore();
        });

        it('should call bytesToMegabytesForDisplay() in the model', function () {
            expect(bToMbSpy).to.have.been.calledOnce;
        });

        it('will NOT call bytesToMegabytesForDisplay() if a corresponding ' +
            '<p> tag does not exist in the DOM', function () {
            // reset the spy
            bToMbSpy.resetHistory();

            // remove the <p> tag from the DOM
            view.$el.find('#' + inputId).parent().next('p').remove();

            view._updateFileNameAndSize(inputId);

            expect(bToMbSpy).not.to.have.been.called;
        });

        it('will NOT call bytesToMegabytesForDisplay() if file.size is not a number', function () {
            // reset the spy
            bToMbSpy.resetHistory();

            // remove the <p> tag from the DOM
            fakeFiles[0].size = 'A string';
            view.model.set('files', fakeFiles);

            view._updateFileNameAndSize(inputId);

            expect(bToMbSpy).not.to.have.been.called;
        });

        it('should pass the file size to bytesToMegabytesForDisplay()', function () {
            expect(bToMbSpy).to.have.been.calledWith(fakeFiles[0].size);
        });

        it('should add the file name to the <p> tag\'s text', function () {
            expect(pTag.text()).to.contain(fileName);
        });

        it('should add the file size in megabytes to the <span> tag\'s text', function () {
            expect(pTag.find('span').text()).to.equal(bToMbReturnVal + ' MB');
        });

    });     // _updateFileNameAndSize method

    describe('_updateModel method', function () {
        var modelSetSpy;
        var expectedObj = {
            comments       : '',
            commentsLength : 500,
            files          : []
        };

        beforeEach(function () {
            modelSetSpy = this.sinon.spy(view.model, 'set');

            view._updateModel();
        });

        afterEach(function () {
            modelSetSpy.restore();
        });

        it('should call set() on the model', function () {
            expect(modelSetSpy).to.have.been.calledOnce;
        });

        it('should pass "comments", "commentsLength", and "files" values to the model',
            function () {
            var argPassedToSet = modelSetSpy.getCalls()[0].args[0];

            expect(argPassedToSet).to.deep.equal(expectedObj);
        });

    });     // _updateModel method

});

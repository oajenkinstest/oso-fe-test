/* global expect:false, Backbone:false */

var hbsHelpers = require('../dist/utils/hb-helpers');

describe('Handlebars helper methods (util/hb-helpers)', function() {

    describe('bytesToMegabytes method', function () {

        it('exists as a function', function () {
            expect(hbsHelpers.helpers.bytesToMegabytes).to.exist.and.be.a('function');
        });

        it('returns "0.0" if value passed in is not a number', function () {
            var result = hbsHelpers.helpers.bytesToMegabytes('Hiya!');

            expect(result).to.equal('0.0');
        });

        it('converts the value of bytes passed in to megabytes', function () {
            var fiveMbInBytes = 5 * 1024 * 1024;
            var result = hbsHelpers.helpers.bytesToMegabytes(fiveMbInBytes);

            expect(result).to.equal('5.0');
        });

    }); // bytesToMegabytes


    describe('currencyFormat method', function () {
    // TODO: Test when "spaceZero" is false and with invalid inputs.

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.currencyFormat).to.exist.and.be.a('function');
        });

        it('correctly rounds the value if round flag is true', function() {
            expect(hbsHelpers.helpers.currencyFormat('USD 328176.44', true)).to.equal('$328,176');
        });

        it('does not round if round flag is false', function() {
            expect(hbsHelpers.helpers.currencyFormat('USD 328176.54')).to.equal('$328,176.54');
        });

        it('currencyFormat returns \'--\' if value is 0 and spaceZero flag is true', 
                function() {
            expect(hbsHelpers.helpers.currencyFormat('USD 0.00', true, true)).to.equal('--');
        });

        it('currencyFormat does not return \'--\' if value is > 0 and spaceZero flag is true', 
            function() {
            expect(hbsHelpers.helpers.currencyFormat('USD -2000.20', true, true))
                .to.equal('-$2,000');
        });
    }); // currencyFormat


    describe('compare method', function () {
    // TODO: Add tests that don't use the default "==" hash and also some for invalid inputs.

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.compare).to.exist.and.be.a('function');
        });

        it('returns true using the default "==" hash operator and equal inputs', function() {
            var controlValue = 'retail';
            var actualValue = 'retail';

            var compare = hbsHelpers.helpers.compare.apply(null,
                [
                    controlValue,
                    actualValue,
                    {
                        fn:function(){
                            return true;
                        },
                        inverse:function(){
                            return false;
                        },
                        hash:{  }
                    }
                ]);

            expect(compare).to.equal(true);
        });

        it('returns false using the default "==" hash operator and different inputs', function() {
            var controlValue = 'retail';
            var actualValue = 'ib';

            var compare = hbsHelpers.helpers.compare.apply(null,
                [
                    controlValue,
                    actualValue,
                    {
                        fn:function(){
                            return true;
                        },
                        inverse:function(){
                            return false;
                        },
                        hash:{ }
                    }
                ]);

            expect(compare).to.equal(false);
        });
    }); // compare helper


    describe('isoCurrencyToNumber method', function () {
    // TODO: Add a test with invlid inputs

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.isoCurrencyToNumber).to.exist.and.be.a('function');
        });

        it('does not throw an error if it is called with valid inputs', function() {
            var fn = function() {
                return hbsHelpers.helpers.isoCurrencyToNumber.apply(null, ['USD 123.45']);
            };

            // isoCurrencyToNumber simply wraps a function in utils.js,
            // so just make sure we can call it as a helper function.
            expect(fn).to.not.throw(Error);
            expect(fn()).to.equal(123.45);
        });
    }); // isoCurrencyToNumber


    describe('isoDurationToTimeUnit method', function () {
    // TODO: Add tests for invalid inputs.

        it('exists as a function', function () {
            expect(hbsHelpers.helpers.isoDurationToTimeUnit).to.exist.and.be.a('function');
        });
        
        it('returns an empty string if first argument (value) is null', function () {
            var result = hbsHelpers.helpers.isoDurationToTimeUnit(null, 'year');
            
            expect(result).to.equal('');
        });

        it('throws an error if second argument (unit) is not provided', function () {
            var testDuration = 'P3Y6M4DT12H30M5S';
            var fn = function () {
                hbsHelpers.helpers.isoDurationToTimeUnit(testDuration);
            };

            expect(fn).to.throw(Error);
        });

        it('correctly parses "hours" from a valid duration string', function () {
            var testDuration = 'P3Y6M7W4DT12H30M5S';
            var expectedResult = 12;
            var result = hbsHelpers.helpers.isoDurationToTimeUnit(testDuration, 'hours');

            expect(result).to.equal(expectedResult);
        });
    }); // isoDurationToTimeUnit


    describe('hasValue method', function () {
    // TODO: Test that it returns false if the value is not present in one of the input objects and
    //       test for invalid inputs.

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.hasValue).to.exist.and.be.a('function');
        });

        it('returns true if one of the input objects has a property with the value', function() {
            var value = 'N';

            var hasValue = hbsHelpers.helpers.hasValue.apply(null,
                [
                    value,
                    false,
                    {
                        fn:function(){
                            return true;
                        },
                        inverse:function(){
                            return false;
                        },
                        hash:{
                            requiredFypcQualFlag   : 'N',
                            requiredLifeFypcFlag   : 'N'
                        }
                    }
                ]);

            expect(hasValue).to.equal(true);
        });
    }); // hasValue


    describe('absolute method', function () {
    // TODO: Tests with floating-point numbers.

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.absolute).to.exist.and.be.a('function');
        });

        it('returns absolute value for negative integer', function() {
            var output;
            output = hbsHelpers.helpers.absolute(-1);
            expect(output).to.equal(1);
        });

        it('returns absolute value for positive integer', function() {
            var output;
            output = hbsHelpers.helpers.absolute(1);
            expect(output).to.equal(1);
        });

        it('returns blank string if input is undefined/null', function() {
            var output;
            output = hbsHelpers.helpers.absolute();
            expect(output).to.equal('');
        });

        it('throws an error for non-numeric input', function() {
            var fn = function () {
                hbsHelpers.helpers.absolute('abcd');
            };

            expect(fn).to.throw('Expected a numeric inputs');
        });
    }); // absolute


    describe('round method', function () {

        it('exists as a function', function() {
            expect(hbsHelpers.helpers.round).to.exist.and.be.a('function');
        });

        it('returns rounded value', function() {
            var output;
            output = hbsHelpers.helpers.round(25.56);
            expect(output).to.equal(26);
        });

        it('returns blank string if input is undefined/null', function() {
            var output;
            output = hbsHelpers.helpers.round();
            expect(output).to.equal('');
        });

        it('throws an error for non-numeric input', function() {
            var fn = function () {
                hbsHelpers.helpers.round('abcd');
            };

            expect(fn).to.throw('Expected a numeric inputs');
        });
    }); // round


    describe('uppercase method', function () {

        it('exists and be a function', function () {
            expect(hbsHelpers.helpers.uppercase).to.exist.and.be.a('function');
        });

        it('returns an empty string when no value is passed', function () {
            var result = hbsHelpers.helpers.uppercase();
            
            expect(result).to.equal('');
        });
        
        it('returns a string to uppercase', function () {
            var testString = 'bazinga';
            var result = hbsHelpers.helpers.uppercase(testString);
            
            expect(result).to.equal('BAZINGA');
        });
    }); // uppercase

    describe('dateFormat method', function () {
    // TODO: Add a positive test that uses a custom date format string, a negative test with
    //       an invalid input date string, and a negative test with an invalid format string.

        it('formats date correctly with default format', function() {
            var date = hbsHelpers.helpers.dateFormat('2016-02-10T00:00:00');
            expect(date).to.equal('02/10/2016');
        });
        
        it('throws an error if a time-related format is used', function () {
            var fn;

            fn = function () {
                hbsHelpers.helpers
                    .dateFormat('2016-02-10T18:15:00Z', 'MM/DD/YYYY - hh:ss z');
            };

            expect(fn).to.throw(Error);
        });
    }); // dateFormat


    describe('isNotEmpty method', function () {
    // TODO: Add tests that show what happens when the object *is* empty and also an invalid input.

        it('returns false if the object is not empty', function() {
            var bool = hbsHelpers.helpers.isNotEmpty.apply(null,
                [
                    {},
                    {
                        fn:function(){
                            return true;
                        },
                        inverse:function(){
                            return false;
                        },
                        hash:{ }
                    }
                ]);
            expect(bool).to.equal(false);
        });
    }); // isNotEmpty

    describe('ifAvailable method', function () {
        it('does not throw an error if the object is undefined', function () {
            var fn = function () {
                hbsHelpers.helpers.ifAvailable.apply(null,
                    [
                        undefined,
                        {
                            fn: function () {
                                return true;
                            },
                            inverse: function () {
                                return false;
                            },
                            hash: {}
                        }
                    ]);
            };
            expect(fn).not.to.throw(Error);
        });

        it('returns true if the object does not have a "dataAvailability" property', function () {
            var bool = hbsHelpers.helpers.ifAvailable.apply(null,
                [
                    { 'foo': 123 },
                    {
                        fn: function () {
                            return true;
                        },
                        inverse: function () {
                            return false;
                        },
                        hash: {}
                    }
                ]);
            expect(bool).to.equal(true);
        });

        it('returns true if the object has a "dataAvailability" property that is set to any ' +
            'value other than "notAvailable"', function () {
            var bool = hbsHelpers.helpers.ifAvailable.apply(null,
                [
                    { 'dataAvailability': 'notImplemented' },
                    {
                        fn: function () {
                            return true;
                        },
                        inverse: function () {
                            return false;
                        },
                        hash: {}
                    }
                ]);
            expect(bool).to.equal(true);
        });

        it('returns false if the object passed has a "dataAvailability" '+
                'property with value "notAvailable"', function () {
            var bool = hbsHelpers.helpers.ifAvailable.apply(null,
                [
                    { 'dataAvailability': 'notAvailable' },
                    {
                        fn: function () {
                            return true;
                        },
                        inverse: function () {
                            return false;
                        },
                        hash: {}
                    }
                ]);
            expect(bool).to.equal(false);
        });

    }); // ifAvailable

    describe('wrapRoleCodeWithParenthesis method', function () {
    // TODO: Add tests with invalid inputs

        it('returns the correct string when name is present and when it is missing', function() {
        // TODO: Split this test into two separate tests
            var roleCode = hbsHelpers.helpers.wrapRoleCodeWithParenthesis(
                'James Philps', 
                'ds454s'
            );
            expect(roleCode).to.equal('(ds454s)');

            roleCode = hbsHelpers.helpers.wrapRoleCodeWithParenthesis(
                 '', 
                'ds454s'
            );
            expect(roleCode).to.equal('ds454s');
        });
    }); // wrapRoleCodeWithParenthesis


    describe('formatPhoneNumber method', function () {
    // TODO: Add tests with invalid inputs

        it('formats a string of 10 digits into phone number', function () {
            expect(hbsHelpers.helpers.formatPhoneNumber('2325558989'))
                .to.equal('(232) 555-8989');
        });
    }); // formatPhoneNumber


    describe ('math method', function (){
    // TODO: Add tests with floating-point numbers

        it('exists as a function', function () {
            expect(hbsHelpers.helpers.math).to.exit;
        });
        
        it('returns correct result for "+"', function () {
            var result = hbsHelpers.helpers.math(1, '+', 7);
            expect(result).to.equal(8);
        });

        it('returns correct result for "-"', function () {
            var result = hbsHelpers.helpers.math(8, '-', 1);
            expect(result).to.equal(7);
        });

        it('returns correct result for "*"', function () {
            var result = hbsHelpers.helpers.math(2, '*', 3);
            expect(result).to.equal(6);
        });
        
        it('returns correct result for "/"', function () {
            var result = hbsHelpers.helpers.math(12, '/', 3);
            expect(result).to.equal(4);
        });

        it('returns correct result for "%"', function () {
            var result = hbsHelpers.helpers.math(3, '%', 10);
            expect(result).to.equal(3);
        });

        it('returns 0 if inputs are not numbers or operator is invalid', function () {
            var result = hbsHelpers.helpers.math(10, '+', 'aaadad');
            expect(result).to.equal(0);

            result = hbsHelpers.helpers.math(10, '+=', 90);
            expect(result).to.equal(0);
        });

    }); // math

    describe ('"buildHrefURL" helper method', function () {
        it('should exist', function () {
            expect(hbsHelpers.helpers.buildHrefURL).to.exist.and.be.a('function');
        });
        it('should throw error if input is not string', function () {
            var fn = function () {
                hbsHelpers.helpers.buildHrefURL({});
            };
            expect(fn).to.throw('buildHrefURL : Expected pageLink as a string format');
        });

        it('should return correct hash URL', function () {
            var result = hbsHelpers.helpers.buildHrefURL.apply(null,
            [
                '#pending-policy-details',
                {
                    hash:{
                        policyId   : 5555445555
                    }
                }
            ]);
            var expectedResult = '#pending-policy-details?policyId=5555445555';
            expect(result).to.equal(expectedResult  );
        });

        it('should return correct hash URL with "targetuser" Query param if user impersonate state',
                function () {

            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function() {
                return 'john';
            });

            var result = hbsHelpers.helpers.buildHrefURL.apply(null,
            [
                '#pending-policy-details',
                {
                    hash:{
                        policyId   : 5555445555
                    }
                }
            ]);
            var expectedResult = '#pending-policy-details?policyId=5555445555&targetuser=john';
            expect(result).to.equal(expectedResult  );

            userChannel.stopReplying();
            userChannel = null;
        });

    }); //"buildHrefURL" helper method

    describe ('"concat" helper method', function () {
        it('should exist', function () {
            expect(hbsHelpers.helpers.concat).to.exist.and.be.a('function');
        });

        it('should return concatenated string', function () {
            var result = hbsHelpers.helpers.concat('#org','/allPolices','?name=john');
            var expectedResult = '#org/allPolices?name=john';
            expect(result).to.equal(expectedResult);
        });

        it('null/undefined/object arguments should ignored', function () {
            var result = hbsHelpers.helpers.concat(
                '#org','/allPolices',undefined, null, {}
            );
            var expectedResult = '#org/allPolices';
            expect(result).to.equal(expectedResult);
        });
    }); //"concat" helper method

});

/* global expect:false, jsdomConsole:false, Backbone:false */

var utils = require('../dist/utils/ajax-utils');

describe('Basic AJAX Utility Tests (utils/ajax-utils.js)', function() {
    
    it('ajax-utils should exist', function () {
        expect(utils).to.exist;
    });

    describe('buildPrefilter method', function() {

        it('ajax-utils should have a prefilter function', function () {
            expect(utils.buildPrefilter).to.exist;
            expect(utils.buildPrefilter).to.be.a('function');
        });

        it('should add values to options', function () {
            var options = {};
            var prefilter = utils.buildPrefilter({
                getImpersonatedWebId: function() {
                    return 'DaffyDuck';
                }
            });

            prefilter(options, {}, {});
            expect(options.xhrFields.withCredentials).to.equal(true);
        });

        describe ('ajax options.method as GET', function () {
            it('should add a targetuser param to the options during impersonation',
                function () {
                    var options = {};
                    var prefilter = utils.buildPrefilter({
                        getImpersonatedWebId: function() {
                            return 'DaffyDuck';
                        }
                    });

                    prefilter.apply(this, [options, {}, {}]);
                    expect(options.data).to.equal('targetuser=DaffyDuck');
                }
            );

            it('should not add a targetuser param to the options when not impersonating',
                function() {
                    var options = {};
                    var prefilter = utils.buildPrefilter({
                        getImpersonatedWebId: function() {
                            return null;
                        }
                    });

                    prefilter.apply(this, [options, {}, {}]);

                    // targetuser param not added at all.
                    expect(options.data).to.be.undefined;
                }
            );
        });

        describe ('ajax options.method as POST', function () {
            it('should add a targetuser param to the options.data during impersonation',
                function () {
                    var options = {
                        method : 'POST'
                    };
                    var originalOptions ={
                        data : '{"level" : 1}'
                    };
                    var prefilter = utils.buildPrefilter({
                        getImpersonatedWebId: function() {
                            return 'DaffyDuck';
                        }
                    });
                    
                    prefilter.apply(this, [options, originalOptions, {}]);
                    expect(options.data).to.equal('{"level":1,"targetuser":"DaffyDuck"}');
                }
            );

            it('should call debugModule.error if originalOptions.data is not valid JSON string ',
                function() {
                    var options = {
                        method : 'POST'
                    };

                    var originalOptions ={
                        data : ''
                    };
                    var errorLogSpy  = this.sinon.spy(jsdomConsole, 'error');
                    var prefilter = utils.buildPrefilter({
                        getImpersonatedWebId: function() {
                            return 'DaffyDuck';
                        }
                    });

                    prefilter.apply(this, [options, originalOptions, {}]);

                    expect(errorLogSpy).to.have.been.calledWith(
                        ['buildPrefilter : originalOptions.data provided is not valid JSON string']
                    );
                }
            );
        });
    });

    describe('beforeSend method', function() {
        it('ajax-utils should have a beforeSend function', function () {
            expect(utils.beforeSend).to.exist;
            expect(utils.beforeSend).to.be.a('function');
        });

        it('beforeSend function should populate the URL of the jqXHR', function () {
            var jqXHR = {
                responseJSON: {}
            };
            var options = {
                url: 'http://www.crunchyroll.com/'
            };
            utils.beforeSend(jqXHR, options);
            expect(jqXHR.url).to.equal(options.url);
        });

        it('ajax-utils should have a getAjaxMessages function', function () {
            expect(utils.getAjaxMessages).to.exist;
            expect(utils.getAjaxMessages).to.be.a('function');
        });

    });


    /* Test the error handling utils */
    describe('AJAX Utility Error Handling Tests', function() {

        var genericUserMessage = 'An error occurred while processing this request.';
        var xhr = {};
        var errorSpy;

        // Set up some spies to check that things are being called
        beforeEach(function () {
            errorSpy = this.sinon.spy(jsdomConsole, 'error');

            xhr = {
                url: 'http://www.google.com/',
                responseJSON: {}
            };
        });

        // Clean up those spies
        afterEach(function () {
            errorSpy.restore();
            xhr = null;
        });

        it('getAjaxMessages should handle 400 responses', function () {
            xhr.status = 400;
            xhr.responseJSON.message = 'Nope!';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 400 Nope!');
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = '';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 400 Bad Request');
            expect(messages.userMessage).to.equal(genericUserMessage);
        });

        it('getAjaxMessages should handle 403 responses', function () {
            xhr.status = 403;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.userMessage).to.equal('The page you requested does not exist.');
        });

        it('getAjaxMessages should handle 405 responses', function () {
            xhr.status = 405;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 405 Method Not Allowed');
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = 'No POST for you!';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 405 No POST for you!');
            expect(messages.userMessage).to.equal(genericUserMessage);
        });

        it('getAjaxMessages should handle 500 responses', function () {
            xhr.status = 500;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 500 Unknown Server Error');
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = 'Something broke. Big time.';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 500 Something broke. Big time.');
            expect(messages.userMessage).to.equal(genericUserMessage);
        });

        it('getAjaxMessages should handle 501 responses', function () {
            xhr.status = 501;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 501 Not Implemented');
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = 'Coming in version 2.12.7!';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 501 Coming in version 2.12.7!');
            expect(messages.userMessage).to.equal(genericUserMessage);
        });

        it('getAjaxMessages should handle 502 responses', function () {
            xhr.status = 502;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 502 Bad Gateway');
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = 'Big bad gateway!';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.equal(xhr.url + ' 502 Big bad gateway!');
            expect(messages.userMessage).to.equal(genericUserMessage);
        });

        it('getAjaxMessages should handle 503 responses', function () {
            xhr.status = 503;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal('The system is currently unavailable.');

            xhr.responseJSON.message = '"The system is down" -- Strongbad';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal('"The system is down" -- Strongbad');
        });

        it('getAjaxMessages should handle 504 responses', function () {
            var expectedMsg = 'The system reached its maximum timeout. Please try again.';
            xhr.status = 504;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);

            xhr.responseJSON.message = 'Timeout!?!? What do you mean timeout?!?!';
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal('Timeout!?!? What do you mean timeout?!?!');
        });

        it('getAjaxMessages should handle 505 responses', function () {
            var expectedMsg = 'HTTP Version Not Supported!';
            xhr.status = 505;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('getAjaxMessages should handle 506 responses', function () {
            var expectedMsg = 'Variant Also Negotiates!';
            xhr.status = 506;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('getAjaxMessages should handle 507 responses', function () {
            var expectedMsg = 'Insufficient Storage!';
            xhr.status = 507;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('getAjaxMessages should handle 508 responses', function () {
            var expectedMsg = 'Loop Detected!';
            xhr.status = 508;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('getAjaxMessages should handle 510 responses', function () {
            var expectedMsg = 'Not Extended!';
            xhr.status = 510;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('getAjaxMessages should handle 511 responses', function () {
            var expectedMsg = 'Network Authentication Required!';
            xhr.status = 511;
            xhr.responseJSON.message = '';

            var messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(genericUserMessage);

            xhr.responseJSON.message = expectedMsg;
            messages = utils.getAjaxMessages(xhr);
            expect(messages).not.to.be.null;
            expect(messages.logMessage).to.be.undefined;
            expect(messages.userMessage).to.equal(expectedMsg);
        });

        it('ajaxUtils should have a buildErrorHandler function', function () {
            expect(utils.buildErrorHandler).to.exist;
            expect(utils.buildErrorHandler).to.be.a('function');
        });

        it('buildErrorHander should return a function', function () {
            var errorHandler = utils.buildErrorHandler();
            expect(errorHandler).to.be.a('function');
        });

        it('errorHandler should called logout callback passed', function () {
            xhr.status = 401;
            xhr.responseJSON.message = 'Get out!';

            var logoutStub = this.sinon.stub();

            var errorHandler = utils.buildErrorHandler(logoutStub);
            errorHandler({}, xhr);

            // Would like to verify that logoutStub got called...
            expect(logoutStub).to.have.been.called;
        });

        it('errorHandler should display errors', function () {
            var errorHandlerSpy = this.sinon.spy();
            var errorChannel = Backbone.Radio.channel('error');
            errorChannel.on('showErrorPage', errorHandlerSpy);

            xhr.status = 403;
            xhr.responseJSON.message = '';
            var errorHandler = utils.buildErrorHandler(function () {});
            errorHandler({}, xhr);
            
            expect(errorHandlerSpy).to.have.been.called;

            errorChannel.reset();
        });

        it('errorHandler should log errors', function () {
            xhr.status = 501;
            xhr.responseJSON.message = 'No jeans for you!';
            var errorHandler = utils.buildErrorHandler();
            errorHandler({}, xhr);
            expect(errorSpy).to.have.been.calledWith(
                ['http://www.google.com/ 501 No jeans for you!']
            );
        });
        
        it('errorHandler parses "siteminder" response', function() {
            var expectedURL = 'https://www.st.oneamerica.com/login';
            xhr.status = 401;
            xhr.responseText = 
                'SiteminderReason=Challenge\n' + 
                'SiteminderRedirectURL=www.st.oneamerica.com\n' + 
                'SiteminderChallengeURL=';

            var logoutStub = this.sinon.stub();

            var errorHandler = utils.buildErrorHandler(logoutStub);
            errorHandler({}, xhr);
            
            expect(logoutStub).to.have.been.calledWith(true, expectedURL);
        });
    });
});

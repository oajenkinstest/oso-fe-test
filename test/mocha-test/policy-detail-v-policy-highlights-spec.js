/* global describe:false, expect:false, $:false, sinon:false */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers          = require('./helpers/helpers');

describe('Policy Detail - Policy Highlights section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var copyOfActivePolicyDetail;
    var policyHighlightsSection;
    var rootView;
    var view;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
    });

    describe('Section should display', function () {

        before(function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
            ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );

            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123478
                }
            });

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

            policyHighlightsSection = view.$el.find('.policy-highlights');
        });

        after(function () {
            ajaxStub.restore();
            rootView.destroy();
        });

        it('Section should display if "isActive" is true and the ' +
            '"policyValue" object is returned from the service', function () {
            expect(copyOfActivePolicyDetail.isActive).to.be.true;
            expect(copyOfActivePolicyDetail.policyValue).to.be.an('object');
            expect(policyHighlightsSection.length).to.equal(1);
        });
    });

    describe('Section should not be displayed', function () {

        beforeEach(function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
            ajaxStub                 = this.sinon.stub($, 'ajax');
            rootView                 = helpers.viewHelpers.createRootView();
        });

        afterEach(function () {
            copyOfActivePolicyDetail = null;
            ajaxStub.restore();
            rootView.destroy();
        });

        it('if "activePolicy" is falsey', function () {
            delete copyOfActivePolicyDetail.activePolicy;
            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123456
                }
            });
            rootView.render();
            rootView.showChildView('contentRegion', view);

            policyHighlightsSection = view.$el.find('.policy-highlights');

            expect(policyHighlightsSection.length).to.equal(0);
        });

        it('if "policyValue" object does not exist', function () {
            delete copyOfActivePolicyDetail.policyValue;
            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123456
                }
            });
            rootView.render();
            rootView.showChildView('contentRegion', view);

            policyHighlightsSection = view.$el.find('.policy-highlights');

            expect(policyHighlightsSection.length).to.equal(0);
        });

        it('if "policyValue" object is empty', function () {
            copyOfActivePolicyDetail.policyValue = {};
            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123456
                }
            });
            rootView.render();
            rootView.showChildView('contentRegion', view);

            policyHighlightsSection = view.$el.find('.policy-highlights');

            expect(policyHighlightsSection.length).to.equal(0);
        });
    });

    describe('Policy Values column', function () {

        before(function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
            ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );

            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123478
                }
            });

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

            policyHighlightsSection = view.$el.find('.policy-highlights');
        });

        after(function () {
            ajaxStub.restore();
            rootView.destroy();
        });

        describe('Happy path with typical data', function () {
            var policyValueCol;

            before(function () {
                policyValueCol = policyHighlightsSection.find('li.js-policy-values-col');
            });

            after(function () {
                policyValueCol = null;
            });

            it('"As of" date should be displayed in column header', function () {
                var h4Text   = policyValueCol.find('h4 .smaller-65').text();
                var expected = 'As of 12/06/2017';

                expect(h4Text).to.contain(expected);
            });

            it('"Account Value" should appear first', function () {
                var firstHeadingText  = policyValueCol.find('p:eq(0)').text();
                var expected          = 'Account Value:';

                expect(firstHeadingText).to.contain(expected);
            });

            it('"Accumulated Value" should appear second', function () {
                var secondHeadingText = policyValueCol.find('p:eq(1)').text();
                var expected          = 'Accumulated Value';

                expect(secondHeadingText).to.contain(expected);
            });

            it('"Cash Surrender Value" should appear third', function () {
                var thirdHeadingText = policyValueCol.find('p:eq(2)').text();
                var expected         = 'Cash Surrender Value';

                expect(thirdHeadingText).to.contain(expected);
            });

            it('"Cost Basis" should appear fourth', function () {
                var fourthHeadingText = policyValueCol.find('p:eq(3)').text();
                var expected          = 'Cost Basis';

                expect(fourthHeadingText).to.contain(expected);
            });

            it('"Loan Balance" should appear fifth', function () {
                var fifthHeadingText = policyValueCol.find('p:eq(4)').text();
                var expected         = 'Loan Balance';

                expect(fifthHeadingText).to.contain(expected);
            });

            it('"Cash Withdrawals" should appear sixth', function () {
                var sixthHeadingText = policyValueCol.find('p:eq(5)').text();
                var expected         = 'Cash Withdrawals';

                expect(sixthHeadingText).to.contain(expected);
            });

            it('"Total Policy Contributions" should appear seventh', function () {
                var seventhHeadingText = policyValueCol.find('p:eq(6)').text();
                var expected           = 'Total Policy Contributions';

                expect(seventhHeadingText).to.contain(expected);
            });

        }); // Policy Values column happy path with typical data

        describe('Edge cases with atypical data', function () {

            it('"As of" date and explanation should not be shown if asOfDate is null', function () {
                var columnHeaderText;
                var policyValueObj = view.model.get('policyValue');

                policyValueObj.asOfDate = null;

                view.model.set('policyValue', policyValueObj);
                view.render();

                columnHeaderText = view.$el.find('.policy-highlights li.js-policy-values-col h4').text();

                expect(columnHeaderText).not.to.contain('As of');
            });

            it('"Loan Balance" should not display with a $0 value', function () {
                var loanElement;
                var loanObj = view.model.get('loan');

                loanObj.loanBalance = 'USD 0.00';
                view.model.set('loan', loanObj);
                view.render();

                loanElement = view.$el
                    .find('.policy-highlights li.js-policy-values-col p:contains("Loan Balance")');

                expect(loanElement.length).to.equal(0);
            });
        }); // Policy Values column edge cases with atypical data
    });

    describe('Long Term Care Values column', function () {

        it('should not display if it contains no items', function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);

            // Null out the data for the items in this column
            var ltcCoverageDetail = copyOfActivePolicyDetail.coverage
                .coverageDetail.ltcCoverageDetail;
            ltcCoverageDetail.ltcCurrentBenefitBalance  = null;
            ltcCoverageDetail.ltcTotalWithdrawnAmount   = null;
            ltcCoverageDetail.monthlyBenefitAmount      = null;

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );

            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123478
                }
            });

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

            var ltcColumn = view.$el.find('.policy-highlights li.js-ltc-col');

            expect(ltcColumn.length).to.equal(0);

            ajaxStub.restore();
            rootView.destroy();
        });

        describe('Happy path with typical data', function () {
            var ltcCol;
            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
                ltcCol = policyHighlightsSection.find('li.js-ltc-col');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
                ltcCol = null;
            });

            it('displays As of Date in column header', function () {
                var colHeaderText   = ltcCol.find('#ltcAsOfDate').text();

                expect(colHeaderText).to.contain('As of 12/06/2017');
                expect(colHeaderText).to.contain('Last system cycle');
            });

            it('"Current LTC Benefit Balance" should appear first', function () {
                expect(ltcCol.find('#ltcCurrentBenefitBalance').length).to.equal(1);
            });

            it('"LTC Total Amount Withdrawn" should appear second', function () {
                expect(ltcCol.find('#ltcTotalWithdrawnAmount').length)
                    .to.equal(1);
            });

            it('"LTC Monthly Maximum" should appear third', function () {
                expect(ltcCol.find('#monthlyBenefitAmount').length).to.equal(1);
            });
        }); // LTC Values column happy path with typical data

        describe('Edge cases with atypical data', function () {

            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
            });

            it('does not display the date header if the As Of Date is null', function () {
                var policyValObj = view.model.get('policyValue');
                var oldAsOfDate = policyValObj.asOfDate;

                policyValObj.asOfDate = null;
                view.model.set('policyValue', policyValObj);
                view.render();

                var ltcCol = view.$el.find('.policy-highlights li.js-ltc-col');

                expect(ltcCol.find('#ltcColDates').length).to.equal(0);

                policyValObj.asOfDate = oldAsOfDate;
                view.model.set('policyValue', policyValObj);
            });

            it('should still display a Current LTC Benefit Balance of $0', function () {
                var coverage = view.model.get('coverage');
                coverage.coverageDetail.ltcCoverageDetail.ltcCurrentBenefitBalance = 'USD 0.00';
                view.model.set('coverage', coverage);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-ltc-col #ltcCurrentBenefitBalance');
                expect(itemEl.text()).to.contain('$0.00');
            });
        
            it('should still display a LTC Total Amount Withdrawn of $0', function () {
                var coverage = view.model.get('coverage');
                coverage.coverageDetail.ltcCoverageDetail.ltcTotalWithdrawnAmount = 'USD 0.00';
                view.model.set('coverage', coverage);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-ltc-col #ltcTotalWithdrawnAmount');
                expect(itemEl.text()).to.contain('$0.00');
            });
        
            it('should still display a LTC Monthly Maximum of $0', function () {
                var coverage = view.model.get('coverage');
                coverage.coverageDetail.ltcCoverageDetail.monthlyBenefitAmount = 'USD 0.00';
                view.model.set('coverage', coverage);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-ltc-col #monthlyBenefitAmount');
                expect(itemEl.text()).to.contain('$0.00');
            });
        }); // LTC Withdrawal Values column edge cases with atypical data
    });

    describe('Cash Withdrawal Values column', function () {

        it('should not display if it contains no items', function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);

            // Null out the data for the items in this column
            copyOfActivePolicyDetail.policyValue.penaltyFreeCashWithdrawalAmount = null;
            copyOfActivePolicyDetail.policyValue.totalCashAmountWithdrawn = null;
            copyOfActivePolicyDetail.policyValue.remainingCashFreeOutAmount = null;

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );

            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123478
                }
            });

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

            var ratesColumn = view.$el.find('.policy-highlights li.js-cash-col');

            expect(ratesColumn.length).to.equal(0);

            ajaxStub.restore();
            rootView.destroy();
        });

        describe('Happy path with typical data', function () {
            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
            });

            var cashCol;

            before(function () {
                cashCol = policyHighlightsSection.find('li.js-cash-col');
            });

            after(function () {
                cashCol = null;
            });

            it('displays appropriate dates in column header', function () {
                var colHeaderText   = cashCol.find('#cashColDates').text();

                expect(colHeaderText).to.contain('From 12/06/2017');
                expect(colHeaderText).to.contain('to 12/06/2018');
                expect(colHeaderText).to.contain('Current policy year');
            });

            it('"Penalty Free Cash Withdrawal Amount" should appear first', function () {
                expect(cashCol.find('#penaltyFreeCashWithdrawalAmount').length).to.equal(1);
            });

            it('"Total Cash Amount Withdrawn" should appear second', function () {
                expect(cashCol.find('#totalCashAmountWithdrawn').length)
                    .to.equal(1);
            });

            it('"Remaining Cash Free Out Amount" should appear third', function () {
                expect(cashCol.find('#remainingCashFreeOutAmount').length).to.equal(1);
            });
        }); // Cash Withdrawal Values column happy path with typical data

        describe('Edge cases with atypical data', function () {

            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
            });

            it('does not display the date header if the From date is null', function () {
                var policyValObj = view.model.get('policyValue');
                var oldCashWithdrawalFromDate = policyValObj.cashWithdrawalFromDate;

                policyValObj.cashWithdrawalFromDate = null;
                view.model.set('policyValue', policyValObj);
                view.render();

                var cashCol = view.$el.find('.policy-highlights li.js-cash-col');

                expect(cashCol.find('#cashColDates').length).to.equal(0);

                policyValObj.cashWithdrawalFromDate = oldCashWithdrawalFromDate;
                view.model.set('policyValue', policyValObj);
            });

            it('does not display the date header if the To date is null', function () {
                var policyValObj = view.model.get('policyValue');
                var oldCashWithdrawalToDate = policyValObj.cashWithdrawalToDate;

                policyValObj.cashWithdrawalToDate = null;
                view.model.set('policyValue', policyValObj);
                view.render();

                var cashCol = view.$el.find('.policy-highlights li.js-cash-col');

                expect(cashCol.find('#cashColDates').length).to.equal(0);

                policyValObj.cashWithdrawalToDate = oldCashWithdrawalToDate;
                view.model.set('policyValue', policyValObj);
            });

            it('should still display a Penalty Free Cash Withdrawal Amount of $0', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.penaltyFreeCashWithdrawalAmount = 'USD 0.00';
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-cash-col #penaltyFreeCashWithdrawalAmount');
                expect(itemEl.text()).to.contain('$0.00');
            });
        
            it('should still display a Total Cash Amount Withdrawn of $0', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.totalCashAmountWithdrawn = 'USD 0.00';
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-cash-col #totalCashAmountWithdrawn');
                expect(itemEl.text()).to.contain('$0.00');
            });
        
            it('should still display a Remaining Cash Free Out Amount of $0', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.remainingCashFreeOutAmount = 'USD 0.00';
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-cash-col #remainingCashFreeOutAmount');
                expect(itemEl.text()).to.contain('$0.00');
            });
        }); // Cash Withdrawal Values column edge cases with atypical data
    });

    describe('Rates column', function () {

        it('should not display if it contains no items', function () {
            copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);

            // Null out the data for the items in this column
            copyOfActivePolicyDetail.policyValue.accumValueInterestRateCurrent = null;
            copyOfActivePolicyDetail.coverage.coverageDetail
                .ltcCoverageDetail.accumulatedValueLtcInterestRateCurrent = null;
            copyOfActivePolicyDetail.policyValue.currentInterestRate = null;
            copyOfActivePolicyDetail.policyValue.guaranteedInterestRate = null;

            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                copyOfActivePolicyDetail
            );

            view = new PolicyDetailView({
                stateObj : {
                    policyId : 1234123478
                }
            });

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            rootView.showChildView('contentRegion', view);

            var ratesColumn = view.$el.find('.policy-highlights li.js-rates-col');

            expect(ratesColumn.length).to.equal(0);

            ajaxStub.restore();
            rootView.destroy();
        });

        describe('Happy path with typical data', function () {
            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
            });

            var ratesCol;

            before(function () {
                ratesCol = policyHighlightsSection.find('li.js-rates-col');
            });

            after(function () {
                ratesCol = null;
            });

            it('"Accumulated Value Current Interest Rate" should appear first', function () {
                expect(ratesCol.find('#accumValueInterestRateCurrent').length).to.equal(1);
            });

            it('"Accumulated Value LTC Current Interest Rate" should appear second', function () {
                expect(ratesCol.find('#accumulatedValueLtcInterestRateCurrent').length)
                    .to.equal(1);
            });

            it('"Current Interest Rate" should appear third', function () {
                expect(ratesCol.find('#currentInterestRate').length).to.equal(1);
            });

            it('"Minimum Guaranteed Interest Rate" should appear fourth', function () {
                expect(ratesCol.find('#guaranteedInterestRate').length).to.equal(1);
            });
        }); // Rates column happy path with typical data

        describe('Edge cases with atypical data', function () {

            before(function () {
                copyOfActivePolicyDetail = $.extend(true, {}, helpers.policyData.activePolicyDetail);
                ajaxStub                 = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    copyOfActivePolicyDetail
                );

                view = new PolicyDetailView({
                    stateObj : {
                        policyId : 1234123478
                    }
                });

                rootView = helpers.viewHelpers.createRootView();
                rootView.render();
                rootView.showChildView('contentRegion', view);

                policyHighlightsSection = view.$el.find('.policy-highlights');
            });

            after(function () {
                ajaxStub.restore();
                rootView.destroy();
            });
        
            it('should not display Accumulated Value Current Interest Rate of zero', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.accumValueInterestRateCurrent = 0.0;
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col #accumValueInterestRateCurrent');
                expect(itemEl.length).to.equal(0);
            });

            it('should not display Accumulated Value Current Interest Rate of null', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.accumValueInterestRateCurrent = null;
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col #accumValueInterestRateCurrent');
                expect(itemEl.length).to.equal(0);
            });

            it('should not display Accumulated Value LTC Current Interest Rate of zero',
                function () {
                var coverageObj = view.model.get('coverage');

                coverageObj.coverageDetail.ltcCoverageDetail
                    .accumulatedValueLtcInterestRateCurrent = 0.0;
                view.model.set('coverage', coverageObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col ' +
                        '#accumulatedValueLtcInterestRateCurrent');
                expect(itemEl.length).to.equal(0);
            });

            it('should not display Accumulated Value LTC Current Interest Rate of null',
                function () {
                var coverageObj = view.model.get('coverage');

                coverageObj.coverageDetail.ltcCoverageDetail
                    .accumulatedValueLtcInterestRateCurrent = null;
                view.model.set('coverage', coverageObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col ' +
                        '#accumulatedValueLtcInterestRateCurrent');
                expect(itemEl.length).to.equal(0);
            });

            it('should not display Current Interest Rate of zero', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.currentInterestRate = 0.0;
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col #currentInterestRate');
                expect(itemEl.length).to.equal(0);
            });

            it('should not display Current Interest Rate of null', function () {
                var policyValObj = view.model.get('policyValue');

                policyValObj.currentInterestRate = null;
                view.model.set('policyValue', policyValObj);
                view.render();

                var itemEl = view.$el
                    .find('.policy-highlights li.js-rates-col #currentInterestRate');
                expect(itemEl.length).to.equal(0);
            });
        }); // Rates column edge cases with atypical data

    });
});

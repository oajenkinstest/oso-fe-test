/* global Backbone:false, $:false, expect:false, sinon:false */

var ProducerSearchResultsView = require('../dist/pages/policy/views/producer-search-results-v');

describe('Producer Search Results View ' +
    '(pages/policy/views/producer-search-results-v.js)', function () {

    var dataNameSearch = {
        recordsTotal: null,
        recordsFiltered: null,
        data : [
            {
                id : 232699,
                lexicalName : 'A A HAMMERSMITH INS INC',
                pendingViewCount : 5,
                roles : [ {
                    id : 2049969,
                    roleCode : 'SLF19',
                    statusCode : 'A'
                }, {
                    id : 2049970,
                    roleCode : 'SLF20',
                    statusCode : 'A'
                }, {
                    id : 2049971,
                    roleCode : 'SLF21',
                    statusCode : 'A'
                }, {
                    id : 2049972,
                    roleCode : 'SLF22',
                    statusCode : 'A'
                }, {
                    id : 2049973,
                    roleCode : 'SLF23',
                    statusCode : 'A'
                } ]
            }, {
                id : 213554,
                lexicalName : 'A J SMITH FEDERAL SAVINGS BANK',
                pendingViewCount : 1,
                roles : [ {
                    id : 1922345,
                    roleCode : 'SSD02',
                    statusCode : 'T'
                } ]
            }, {
                id : 205061,
                lexicalName : 'AMD INC DBA SMITH DAVIS AND ABEL',
                pendingViewCount : 0,
                roles : [ {
                    id : 1910515,
                    roleCode : 'SAG16',
                    statusCode : 'T'
                }, {
                    id : 1910516,
                    roleCode : 'ST076',
                    statusCode : 'T'
                } ]
            }
        ]
    };

    var dataNumberSearch =  [
        {
            id : 232699,
            webId : 'jbell',
            fullName : 'Jason Bell',
            lexicalName : 'Jason Bell',
            roles : [ {
                id : 2049969,
                roleCode : 'SLF19',
                statusCode : 'A'
            } ]
        }, {
            id : 213554,
            webId : 'fred',
            fullName : 'Fred Johns',
            lexicalName : 'Fred Johns',
            roles : [ {
                id : 1922345,
                roleCode : 'SSD02',
                statusCode : 'T'
            } ]
        }, {
            id : 205061,
            webId: 'smith12',
            fullName: 'Smith johns',
            lexicalName : 'fullName',
            roles : [ {
                id : 1910515,
                roleCode : 'SAG16',
                statusCode : 'T'
            }]
        }
    ];
    var view;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
    });

    it('exists', function() {
        expect(ProducerSearchResultsView).to.exist;
    });

    it('should throw error if its missing "searchType" option', function() {
        var func = function () {
            view = new ProducerSearchResultsView({
                searchTerm : 'smith'
            });
        };
        expect(func).to.throw(ProducerSearchResultsView.prototype
            .errors.searchTypeMissing);
    });

    describe('Search results', function () {
        var tableHeaderCols;
        var server;

        beforeEach(function () {
            server = this.setupFakeServer(this.sinon, global, global.window);
            server.respondWith('GET', '', [
                200,
                { 'Content-Type' : 'application/json' },
                JSON.stringify(dataNameSearch)
            ]);

            view = new ProducerSearchResultsView({
                searchTerm : 'smith',
                searchType : 'name',
            });
            view.render();
            server.respond();

            tableHeaderCols= view.$el.find('.dataTables_wrapper thead th');
        });

        afterEach(function () {
            server.restore();
            if (view) {
                view.destroy();
            }
        });

        it('displayed within datatables', function () {
            expect(view.$el.find('.dataTables_wrapper').length).to.equal(1);
        });

        it('table has five columns', function () {
            var numCols = view.$el.find('.dataTables_wrapper table tr:eq(0) th').length;

            expect(numCols).to.equal(5);
        });

        it('table displays a "Name" column', function () {
            expect(tableHeaderCols.filter('th:contains(Name)'))
                .to.be.lengthOf(1);
        });

        it('table displays an "Actions" column', function () {
            expect(tableHeaderCols.filter('th:contains(Actions)'))
                .to.be.lengthOf(1);
        });

        it('table displays "Producer Number" column', function () {
            expect(tableHeaderCols.filter('th:contains(Active Producer Number)'))
                .to.be.lengthOf(1);
        });

        it('table displays "ICO Business Address" column', function () {
            expect(tableHeaderCols.filter('th:contains(ICO Business Address)'))
                .to.be.lengthOf(1);
        });

        it('table displays "Business Email" column', function () {
            expect(tableHeaderCols.filter('th:contains(Business Email)'))
                .to.be.lengthOf(1);
        });


        it('url should match Search Type "number"', function () {
            view = new ProducerSearchResultsView({
                searchTerm : '12121',
                searchType : 'number',
            });
            view.render();
            expect(view.url).to.be.equal('/api/oso/secure/rest/producers?roleCode=12121');
        });

        it('url should match Search Type "number"', function () {
            view = new ProducerSearchResultsView({
                searchTerm : '12121',
                searchType : 'number',
            });
            view.render();
            expect(view.url).to.be.equal('/api/oso/secure/rest/producers?roleCode=12121');
        });

        describe('Action link', function () {

            describe('When searchType is "name"', function () {

                describe('When pendingViewCount is greater than 0', function () {
                    var firstRowActionCell;

                    before(function () {
                        server.respondWith('GET', '', [
                            200,
                            { 'Content-Type' : 'application/json' },
                            JSON.stringify(dataNameSearch)
                        ]);
                        view = new ProducerSearchResultsView({
                            searchTerm : 'smith',
                            searchType : 'name'
                        });
                        view.render();
                        server.respond();
                    });

                    it('should display "View Pending List" link', function () {
                        firstRowActionCell = view.$el.find('table tbody tr:eq(0) td:eq(1)');
                        var link = firstRowActionCell.find('a').text().replace(/\s+/g, ' ');

                        expect(link).to.contain('View Pending List');
                    });

                    it('should display "pendingGroupCount" in parentheses', function () {
                        firstRowActionCell = view.$el.find('table tbody tr:eq(0) td:eq(1)');
                        var actionText = firstRowActionCell.text();

                        expect(actionText).to.contain('(5)');
                    });
                });

                it('should NOT display link when pendingViewCount is 0', function () {
                    var thirdRowActionCell = view.$el.find('table tbody tr:eq(2) td:eq(1)');
                    var actionText = thirdRowActionCell.text();

                    expect(actionText).to.be.empty;
                });
            });

            describe('Table legend', function () {

                it('should display when searchType is "name"', function (done) {
                    server.respondWith('GET', '', [
                        200,
                        { 'Content-Type' : 'application/json' },
                        JSON.stringify(dataNameSearch)
                    ]);
                    view = new ProducerSearchResultsView({
                        searchTerm : 'smith',
                        searchType : 'name'
                    });
                    view.render();
                    server.respond();

                    setTimeout(function () {
                        expect(view.ui.tableLegend.hasClass('hidden')).to.be.false;
                        done();
                    }, 0);

                });

                it('should not display when searchType is "number"', function (done) {
                    server.respondWith('GET', '', [
                        200,
                        { 'Content-Type' : 'application/json' },
                        JSON.stringify(dataNumberSearch)
                    ]);
                    view = new ProducerSearchResultsView({
                        searchTerm : 'smith',
                        searchType : 'number'
                    });
                    view.render();
                    server.respond();

                    setTimeout(function () {
                        expect(view.ui.tableLegend.hasClass('hidden')).to.be.true;
                        done();
                    }, 0);
                });

            }); // Table legend

            describe('When searchType is "number"', function () {

                it('action value should match', function (done) {
                    server.respondWith('GET', '', [
                        200,
                        { 'Content-Type' : 'application/json' },
                        JSON.stringify(dataNumberSearch)
                    ]);
                    view = new ProducerSearchResultsView({
                        searchTerm : 'smith',
                        searchType : 'number'
                    });
                    view.render();
                    server.respond();

                    setTimeout(function () {
                        var thirdColumnDataRow = view.$el
                            .find('.dataTables_wrapper tr:eq(1) td:eq(1) a');

                        expect(thirdColumnDataRow.text()).to.equal('View as Producer');
                        expect(thirdColumnDataRow.data('webid')).to.equal('jbell');
                        expect(thirdColumnDataRow.data('name')).to.equal('Jason Bell');
                        done();
                    }, 0);
                });

            });
        });

    });

    describe('on an invalid response', function () {
        var listener;
        var errorStub;
        var server;

        beforeEach(function () {
            listener  = new Backbone.Marionette.Object();
            errorStub = this.sinon.stub();
            server = this.setupFakeServer(this.sinon, global, global.window);

            server.respondWith('GET', '', [
                200,
                { 'Content-Type' : 'application/json' },
                'invaliddata'
            ]);

            view = new ProducerSearchResultsView({
                searchTerm : 'smith',
                searchType : 'name'
            });
            listener.listenTo(view, 'error', errorStub);
            view.render();
            server.respond();
        });

        afterEach(function () {
            server.restore();
            if (view) {
                view.destroy();
            }

            listener.stopListening();
            listener.destroy();
        });

        it('the table is not displayed', function (done) {
            setTimeout(function () {
                var table = view.$el.find('#producerSearchResults');
                expect(table.hasClass('hidden')).to.be.true;
                done();
            }, 0);
        });

        it('triggers an "error" event', function (done) {
            setTimeout(function () {
                // TODO - figure out why this is firing twice.
                // It's hitting the 'error' handler, but what else?
                expect(errorStub).to.have.been.calledTwice;
                done();
            }, 0);
        });
    });


    describe('Handles a non-200 response', function() {
        var listener;
        var errorStub;
        var server;

        beforeEach(function () {
            listener  = new Backbone.Marionette.Object();
            errorStub = this.sinon.stub();
            server = this.setupFakeServer(this.sinon, global, global.window);

            server.respondWith('GET', '', [
                500,
                { 'Content-Type' : 'application/json' },
                JSON.stringify(dataNameSearch)
            ]);

            view = new ProducerSearchResultsView({
                searchTerm : 'smith',
                searchType : 'name'
            });
            listener.listenTo(view, 'error', errorStub);
            view.render();
            server.respond();
        });

        afterEach(function () {
            server.restore();
            if (view) {
                view.destroy();
            }

            listener.stopListening();
            listener.destroy();
            errorStub.resetHistory();
        });

        it('triggers an "error" event', function (done) {
            setTimeout(function () {
                // TODO - figure out why this is firing twice.
                // It's hitting the 'error' handler, but what else?
                expect(errorStub).to.have.been.calledTwice;
                done();
            }, 0);
        });

        it('the table is not displayed', function () {
            var table = view.$el.find('#producerSearchResults');
            expect(table.hasClass('hidden')).to.be.true;
        });
    });

    describe('When no results are found', function () {
        var dataCopy;
        var server;

        beforeEach(function () {
            dataCopy = $.extend({},true,dataNameSearch);

            // empty the data array within dataCopy
            dataCopy.data = [];

            server = this.setupFakeServer(this.sinon, global, global.window);

            server.respondWith('GET', '', [
                200,
                { 'Content-Type' : 'application/json' },
                JSON.stringify(dataCopy)
            ]);

            view = new ProducerSearchResultsView({
                searchTerm : 'smith',
                searchType : 'name'
            });
            view.render();
            server.respond();
        });

        afterEach(function () {
            server.restore();
            dataCopy = null;
            if (view) {
                view.destroy();
            }
        });

        it('the table is not displayed', function () {
            var table = view.$el.find('#producerSearchResults');
            expect(table.hasClass('hidden')).to.be.true;
        });

    });

});

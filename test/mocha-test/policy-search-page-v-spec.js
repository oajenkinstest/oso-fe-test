/* global describe:false, Backbone:false, it:false, expect */

var helpers = require('./helpers/helpers');

var PolicySearchPageView = require('../dist/pages/policy/views/policy-search-page-v');

describe('Search Page View (pages/policy/views/policy-search-page-v.js)', function () {

    describe('Tests without rendering', function () {

        it('policy-search-page-v exists', function () {
            expect(PolicySearchPageView).to.exist;
        });

        it('Creates a PolicySearchView with searchOptions', function () {
            var state = {
                searchTerm : 'Gojira',
                searchType : 'producerName'
            };
            var policyPageView = new PolicySearchPageView({
                stateObj : state
            });

            expect(policyPageView.policySearchView).to.exist;
            expect(policyPageView.policySearchView.searchFormView).to.exist;
        });
    });

    describe('Tests of rendered page', function () {
        var rootView;
        var navigateSpy;
        var policyPageView;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            policyPageView = new PolicySearchPageView();

            rootView.render();
            rootView.showChildView('contentRegion', policyPageView);

            helpers.viewHelpers.startBBhistory();
            
            navigateSpy  = this.sinon.spy(Backbone.history, 'navigate');
        });
        
        afterEach(function () {
            navigateSpy.restore();
            helpers.viewHelpers.stopBBhistory();
            policyPageView.destroy();
            // destroying rootView causes all kinds of problems...
        });

        it('policy-search-page-v will also render the search form', function () {
            var searchForm = policyPageView.$el.find('#pending-search-form');
            expect(searchForm.hasClass('form-horizontal')).to.equal(true);
        });
    });

    describe('_setResultsCheckPoint method', function () {

        var navigateSpy;
        var policyPageView;

        beforeEach(function () {
            policyPageView = new PolicySearchPageView();

            helpers.viewHelpers.startBBhistory();

            navigateSpy = this.sinon.spy(Backbone.history, 'navigate');
        });

        afterEach(function () {
            navigateSpy.restore();
            helpers.viewHelpers.stopBBhistory();
            policyPageView.destroy();
        });

        it('calling _setResultsCheckpoint updates the hash', function () {
            var currentHash  = '#page?someProp=Elephant';
            var expectedHash = '#page?someProp=Elephant&prop1=Monkey&prop2=Giraffe';

            document.location.hash = currentHash;

            policyPageView._setResultsCheckpoint({
                prop1: 'Monkey',
                prop2: 'Giraffe'
            });

            expect(navigateSpy).to.have.been.calledWith(expectedHash, {replace: true});
        });

        it('calling _setResultsCheckpoint replaces matched params in the hash', function () {
            var currentHash = '#page?someProp=Elephant';
            var expectedHash = '#page?someProp=Monkey&anotherProp=Giraffe';

            document.location.hash = currentHash;

            policyPageView._setResultsCheckpoint({
                someProp: 'Monkey',
                anotherProp: 'Giraffe'
            });

            expect(navigateSpy).to.have.been.calledWith(expectedHash, {replace: true});
        });

        it('Should not reset, but extend matched params in hash if "reset" '+
                'parameter is not specified', function () {
            var currentHash = '#page?someProp=Elephant&start=0&length=25';
            var expectedHash = '#page?someProp=Monkey&start=0&length=25&anotherProp=Giraffe';

            document.location.hash = currentHash;

            policyPageView._setResultsCheckpoint({
                someProp: 'Monkey',
                anotherProp: 'Giraffe'
            });

            expect(navigateSpy).to.have.been.calledWith(expectedHash, {replace: true});
        });

        it('Should reset hash, if called with "reset" parameter as true', function (){
            var currentHash = '#page?searchTerm=122123&searchType=producerNumber&start=0';
            var expectedHash = '#page?searchTerm=john&searchType=producerName';

            document.location.hash = currentHash;

            policyPageView._setResultsCheckpoint({
                searchTerm: 'john',
                searchType: 'producerName'
            }, true);

            expect(navigateSpy).to.have.been.calledWith(expectedHash, {replace: true});
        });
    });
    
    describe('_showPolicy method', function () {
        var listener;
        var navStub;
        var policyPageView;

        beforeEach(function () {
            policyPageView = new PolicySearchPageView();
            navStub         = this.sinon.stub();
            listener        = new Backbone.Marionette.Object();
            listener.listenTo(policyPageView, 'nav', navStub);
        });

        afterEach(function () {
            listener.stopListening();
            listener.destroy();
            policyPageView.destroy();
        });
        
        it('triggers a nav event', function () {
            policyPageView._showPolicy(123);
            expect(navStub).to.have.been.calledWith('#policy-detail?policyId=123');
        });
    });
});

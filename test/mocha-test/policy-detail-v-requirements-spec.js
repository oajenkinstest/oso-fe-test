/* global describe, $, expect, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

describe('Policy Detail View - Requirements Section ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var policyDataCopy;
    var requirementsElement;
    var submitRequirementsLink;
    var rootView;
    var view;

    var setView = function() {
        var newview = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
        ajaxStub = this.sinon.stub($, 'ajax');

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        policyDataCopy = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    describe('Happy path tests with typical data', function () {
        var bindRequirementsTablesWithDataTableSpy;

        before(function () {
            ajaxStub.yieldsTo(
                'success',
                helpers.policyData.pendingPolicyDetail
            );

            bindRequirementsTablesWithDataTableSpy = this.sinon.spy(
                PolicyDetailView.prototype, '_bindRequirementsTablesWithDataTable');

            view = setView();

            submitRequirementsLink = view.$el.find('#requirements+a.oa-js-nav');
            requirementsElement      = submitRequirementsLink.next('div');
        });

        after(function () {
            bindRequirementsTablesWithDataTableSpy.restore();
            view.destroy();
        });

        it('Requirements element should exist', function () {
            expect(requirementsElement.length).to.equal(1);
        });

        it('Three expandable sections should exist in this section and data '+
            '\'table\' value should match', function () {

            expect(requirementsElement.find('table').length).to.equal(3);
            expect(requirementsElement.find('table:eq(0)').attr('id'))
                .to.equal('neededForUnderwriting');

            expect(requirementsElement.find('table:eq(1)').attr('id'))
                .to.equal('obtainAtDelivery');

            expect(requirementsElement.find('table:eq(2)').attr('id'))
                .to.equal('received');
        });

        it('"Needed for Underwriting" section should use "panel-warning" CSS class', function () {
            var neededForUnderwriting = requirementsElement.find('.panel:eq(0)');

            expect(neededForUnderwriting.hasClass('panel-warning')).to.be.true;
        });

        it('accordion panel title and count should match for each section', function () {
            expect(requirementsElement.find('.panel-heading:eq(0)').text().trim())
                .to.equal('Needed to Underwrite / Issue / Mail Policy (1)');

            expect(requirementsElement.find('.panel-heading:eq(1)').text().trim())
                .to.equal('May Obtain at Delivery (1)');

            expect(requirementsElement.find('.panel-heading:eq(2)').text().trim())
                .to.equal('Received Requirements (3)');
        });

        it('"May Obtain at Delivery" section should have "Needed to Pay Comp" column', function () {
            expect(requirementsElement.find('table#obtainAtDelivery th.need-head').length)
                .to.equal(1);
        });

        it('"Received Requirements" section should have "Date Received"'+
            ' column', function () {
            expect(requirementsElement.find('table#received th.received-head').length)
                .to.equal(1);
        });

        it('bindRequirmentsTablesWithDataTable method should called', function () {
            expect(bindRequirementsTablesWithDataTableSpy).to.have.been.called;
        });

        it('"Date Created" column should be sortable '+
            'for "May Obtain at Delivery" and "Needed to Underwrite" table', function (){

            expect(requirementsElement
                .find('table#obtainAtDelivery thead th:eq(3)')
                .attr('class').match(/sorting/ig)).to.not.be.null;

            expect(requirementsElement
                .find('table#neededForUnderwriting thead th:eq(3)')
                .attr('class').match(/sorting/ig)).to.not.be.null;
        });

        it('"Date Created" and "Date Received" columns should be sortable '+
            'for "Received Requirements" table', function (){
            expect(requirementsElement.find('table#received thead th:eq(3)')
                .attr('class').match(/sorting/ig)).to.not.be.null;

            expect(requirementsElement.find('table#received thead th:eq(4)')
                .attr('class').match(/sorting/ig)).to.not.be.null;
        });

        describe('"Submit Requirements" button', function () {

            it('should be displayed', function () {
                expect(submitRequirementsLink.length).to.equal(1);
            });

            it('should provide link to Requirement Submission form', function () {
                var expectedHref = '#requirement-submission?policyId=' + view.model.get('policyId');

                expect(submitRequirementsLink.attr('href')).to.equal(expectedHref);
            });
        });
    }); // Happy path tests with typical data


    describe('"Needed to Pay Comp" column in "May Obtain at Delivery" table', function () {

        before(function () {
            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('text should be bold and brown when value is "Yes"', function () {
            var neededToPayCompValue = requirementsElement
                .find('table#obtainAtDelivery tbody td:eq(4)');

            expect(neededToPayCompValue.text()).to.equal('Yes');
            expect(neededToPayCompValue.find('strong').length).to.equal(1);
            expect(neededToPayCompValue.find('strong').hasClass('pending-brown')).to.be.true;
        });

        it('text should NOT be bold or brown when value is "No"', function () {
            var dataCopy = $.extend(true, {}, policyDataCopy);
            dataCopy.requirements.obtainAtDelivery[0].neededToPay = null;

            ajaxStub.yieldsTo(
                'success',
                dataCopy
            );

            view.destroy();
            view = setView();

            var neededToPayCompValue = view.$el.find('table#obtainAtDelivery tbody td:eq(4)');

            expect(neededToPayCompValue.text()).to.equal('No');
            expect(neededToPayCompValue.find('strong').length).to.equal(0);
            expect(neededToPayCompValue.find('strong').hasClass('pending-brown')).to.be.false;
        });
    });

    describe('"Name" columns should be labeled according to customerRole', function () {
        var label;
        var tables;

        var setupViewAndGetTables = function(data) {
            ajaxStub.yieldsTo('success', data);

            var newview = setView();

            return newview.$el
                .find('table#recieved, table#neededForUnderwriting, table#obtainAtDelivery');
        };

        it('labels should be "Insured" if customerRole is "Primary Insured"', function () {
            var dataCopy = $.extend(true, {}, policyDataCopy);
            delete dataCopy.customerRoles.Annuitant;

            tables = setupViewAndGetTables(dataCopy);

            // loop through all of the tables in the requirements section
            tables.each(function(e, table) {
                label = $(table).find('thead th:eq(0)').text().trim();
                expect(label).to.equal('Insured');
            });
        });

        it('labels should be "Insured" if customerRole is "Insured"', function () {
            var dataCopy = $.extend(true, {}, policyDataCopy);
            // change "Primary Insured" role to "Insured" and remove "Annuitant"
            dataCopy.customerRoles.Insured =
                dataCopy.customerRoles['Primary Insured'];

            delete dataCopy.customerRoles['Primary Insured'];
            delete dataCopy.customerRoles.Annuitant;

            tables = setupViewAndGetTables(dataCopy);

            // loop through the tables in the requirements section
            tables.each(function(e, table) {
                label = $(table).find('thead th:eq(0)').text().trim();
                expect(label).to.equal('Insured');
            });
        });

        it('labels should be "Annuitant" if customerRole is "Annuitant"', function () {
            // FIXME: This test doesn't do anything. (Same with the corresponding Intern test.)
            var dataCopy = $.extend(true, {}, policyDataCopy);
            delete dataCopy.customerRoles['Primary Insured'];
        });
    });

    describe('Requirements Submission Log accordions section', function () {
        var requirementSubLogPanel;
        before(function () {
            ajaxStub.yieldsTo(
                'success',
                policyDataCopy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });
        
        it('Should display as data exist', function () {
            var requirementsAccordion = view.$el.find('#requirementsAccordion');

            expect(requirementsAccordion.find('.panel-heading:eq(3)').text().trim())
                .to.contain('Requirement Submission Log (3)');
        });

        it('Should match data', function () {
            requirementSubLogPanel = view.$el.find('#requirement-submission-log-collapse');

            expect(requirementSubLogPanel.find('dt:eq(0) strong').text())
                .to.contain('05/13/2017, 12:22 PM EDT');
            expect(requirementSubLogPanel.find('dt:eq(0)').text())
                .to.contain('(1) File uploaded');
            expect(requirementSubLogPanel.find('dt:eq(1)').text())
                .to.contain('(2) Files uploaded');
        });

        it ('file count should displayed (0) Files if there is no fileCount data', function () {
            expect(requirementSubLogPanel.find('dt:eq(2)').text())
                .to.contain('(0) Files uploaded');
        });

        it('display of "comment" data if data exist', function () {
            expect(requirementSubLogPanel.find('dt:eq(0)').text())
                .to.contain('comment uploaded');
            
            expect(requirementSubLogPanel.find('dt:eq(0)').next('dd').text().trim())
                .to.contain('Some comment will be displayed here');
        });

        it('display of "comment" data if data not exist', function () {
            expect(requirementSubLogPanel.find('dt:eq(1)').text())
                .to.contain('no comment');
            
            expect(requirementSubLogPanel.find('dt:eq(1)').next('dd'))
                .to.be.lengthOf(0);
        });

        it('Should not visible if data not exist', function () {
            var dataCopy = $.extend(true, {}, policyDataCopy);
            dataCopy.requirementSubmissionLog = [];

            ajaxStub.yieldsTo(
                'success',
                dataCopy
            );

            view.destroy();
            view = setView();
            var requirementsAccordion = view.$el.find('#requirementsAccordion');

            expect(requirementsAccordion.find('.panel-heading:eq(3)')).to.be.lengthOf(0);
        });

        it('Two column design if log array has 4 or more logs', function () {
            var dataCopy = $.extend(true, {}, policyDataCopy);
            dataCopy.requirementSubmissionLog.push(
                {
                    submitTimestamp : '2017-05-13T12:22:13.000',
                    fileCount : 1
                },
                {
                    submitTimestamp : '2017-06-13T12:22:13.000',
                    fileCount : 2
                }
            );

            ajaxStub.yieldsTo(
                'success',
                dataCopy
            );

            view.destroy();
            view = setView();
            var requirementsAccordion = view.$el.find('#requirementsAccordion');

            expect(requirementsAccordion.find('.collapse:eq(3) .col-md-6')).to.be.lengthOf(2);
        });
    });

});

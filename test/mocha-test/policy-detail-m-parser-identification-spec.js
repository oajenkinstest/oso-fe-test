/* global expect:false, $:false, expect:false */

var parser = 
    require('../dist/pages/policy/models/parsers/policy-identification');
var helpers = require('./helpers/helpers');

describe('Policy Detail Model parser - Identification ' +
    '(pages/policy/models/parsers/policy-identification.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

    describe('_setPolicyIdentificationLabelAndName method', function () {

        it('exists as a function', function () {
            expect(parser._setPolicyIdentificationLabelAndName).to.exist.and.be.a('function');
        });

        it('should append an object with key "policyIdentification" '+
            'to response', function () {

            var response = parser._setPolicyIdentificationLabelAndName(policyDataCopy);

            expect(response.policyIdentification).to.exist;
        });

        it('"policyIdentification" object appended to response should contain ' +
            '"label" and "name"', function () {

            var response = parser._setPolicyIdentificationLabelAndName(policyDataCopy);

            expect(response.policyIdentification.label).to.exist;
            expect(response.policyIdentification.name).to.exist;
        });

        describe('"policyIdentification.label"', function () {

            it('should be "Name" if AWD policy', function () {

                var copyOfAwd = $.extend(true,{},helpers.policyData.pendingPolicyDetailAWDRip);
                var response = parser._setPolicyIdentificationLabelAndName(copyOfAwd);

                expect(response.policyIdentification.label).to.equal('Name');
            });

            it('should be "Annuitant" if role exists in customerRoles', function () {
                var data = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

                var response = parser._setPolicyIdentificationLabelAndName(data);

                expect(response.policyIdentification.label).to.equal('Annuitant');
            });

            it('should be "Insured" if "Primary Insured" or "Insured" role ' +
                'exists in customerRoles AND "Annuitant" role does not exist', function () {
                var data = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
                delete data.customerRoles.Annuitant;

                var response = parser._setPolicyIdentificationLabelAndName(data);

                expect(response.policyIdentification.label).to.equal('Insured');
            });

        }); // policyIdentification.label

        describe('"policyIdentification.name"', function () {

            it('should be 1st element in customers array for an AWD policy', function () {
                var copyOfAwd = $.extend(true,{},helpers.policyData.pendingPolicyDetailAWDRip);
                var customers = {
                    '0' : {
                        fullName  : ', CHRISTOPHER',
                        firstName : 'CHRISTOPHER'
                    }
                };
                var expected  = customers[0].fullName;
                copyOfAwd.customers = customers;
                var response = parser._setPolicyIdentificationLabelAndName(copyOfAwd);

                expect(response.policyIdentification.name).to.equal(expected);
            });

            it('should be fullName of first "Annuitant" if customerRoles.Annuitant ' +
                'exists', function () {
                var data     = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
                var expected = data.customers[data.customerRoles.Annuitant[0].customerId]
                    .fullName;
                var response = parser._setPolicyIdentificationLabelAndName(data);

                expect(response.policyIdentification.name).to.equal(expected);
            });

            it('should be fullName of first "Primary Insured" if ' +
                'customerRoles[\'Primary Insured\'] exists AND customerRoles.Annuitant ' +
                'does NOT exist', function () {
                var data     = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
                var expected = data.customers[data.customerRoles.Annuitant[0].customerId]
                    .fullName;
                delete data.customerRoles.Annuitant;
                var response = parser._setPolicyIdentificationLabelAndName(data);

                expect(response.policyIdentification.name).to.equal(expected);
            });

            it('should be fullName of first "Insured" if "customerRoles.Insured" exists ' +
                'AND both customerRoles.Annuitant and customerRoles["Primary Insured"] ' +
                'do NOT exist', function () {
                var data     = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
                var expected = data.customers[data.customerRoles.Annuitant[0].customerId]
                    .fullName;

                // Copy 'Primary Insured' to 'Insured'
                data.customerRoles.Insured = data.customerRoles['Primary Insured'];

                // delete both 'Annuitant' and 'Primary Insured' roles
                delete data.customerRoles.Annuitant;
                delete data.customerRoles['Primary Insured'];

                var response = parser._setPolicyIdentificationLabelAndName(data);

                expect(response.policyIdentification.name).to.equal(expected);
            });

        }); // policyIdentification.name

    }); // _setPolicyIdentificationLabelAndName method
    
    describe('_setContactMailto Method ' , function () {

        describe ('caseManager', function () {
            it('Does not create a mailto if the caseManager is undefined', function () {
                var response = {
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager).to.be.undefined;
            });

            it('Does not create a mailto if the caseManager.mailto is undefined', function () {
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.be.undefined;
            });

            it('Subject is blank if there is no policy number or customer lastName', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {}
                    }
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Handles response without any customer info', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Handles response with customerRole, but no customers', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123
                            }
                        ]
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Handles response with customerRole, but not the right customer', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123
                            }
                        ]
                    },
                    customers: {
                        '321': {
                            lastName: 'Fonzarelli'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Subject contains policy number if it exists', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {}
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Subject contains customer lastName if it exists', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=HOSSENPFEFFER';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    }
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Subject contains policy number and customer lastName', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890' +
                    '%20HOSSENPFEFFER';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Handles "Primary Insured"', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890' +
                    '%20HOSSENPFEFFER';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        'Primary Insured': [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

            it('Handles "Annuitant"', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231234567890' +
                    '%20HOSSENPFEFFER';
                var response = {
                    caseManager: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Annuitant: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.caseManager.mailto).to.equal(expected);
            });

        });

        describe ('policyServiceContact', function () {
            it('Does not create a mailto if the policyServiceContact is undefined', function () {
                var response = {
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.policyServiceContact).to.be.undefined;
            });

            it('Does not create a mailto if the policyServiceContact.mailto is '+
                'undefined', function () {
                var response = {
                    policyServiceContact: {
                        displayName: 'Fred "Rerun" Berry'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Hossenpfeffer'
                        }
                    },
                    policyNumber: '1234567890'
                };
                response = parser._setContactMailto(response);

                expect(response.policyServiceContact.mailto).to.be.undefined;
            });

            it('Subject is blank if there is no policy number or customer lastName', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=';
                var response = {
                    policyServiceContact: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {}
                    }
                };
                response = parser._setContactMailto(response);

                expect(response.policyServiceContact.mailto).to.equal(expected);
            });

            it('Subject should be correct if policy number or customer '+
                'lastName exist', function () {
                var expected = 'mailto:fred.berry@rerun.com?subject=Policy%20%231212%20JERRY';
                var response = {
                    policyServiceContact: {
                        displayName: 'Fred "Rerun" Berry',
                        emailAddress: 'fred.berry@rerun.com'
                    },
                    customerRoles: {
                        Insured: [
                            {
                                customerId: 123,
                                customerRoleContactId: 321
                            }
                        ]
                    },
                    customers: {
                        '123': {
                            lastName: 'Jerry'
                        }
                    },
                    policyNumber : 1212
                };
                response = parser._setContactMailto(response);

                expect(response.policyServiceContact.mailto).to.equal(expected);
            });
            
        });    
    });

    describe ('_setAbandonedPolicyFlag method', function (){
        it('exists as a function', function () {
            expect(parser._setAbandonedPolicyFlag).to.exist.and.be.a('function');
        });

        it('should set "isAbandoned" flag if policy status contains '+
            'inactive status', function () {
            var response = parser._setAbandonedPolicyFlag({
                policyStatus: {
                    status: 'Declined'
                }
            });
            expect(response.isAbandoned).to.be.true;
        });

        it('should not set "isAbandoned" flag if policy status contain '+
            'active status', function () {
            var response = parser._setAbandonedPolicyFlag({
                policyStatus: {
                    status: 'In Process'
                }
            });
            expect(response.isAbandoned).to.be.undefined;
        });

    }); // _setAbandonedPolicyFlag method

    describe ('_setActivePolicyFlag method', function (){
        it('exists as a function', function () {
            expect(parser._setActivePolicyFlag).to.exist.and.be.a('function');
        });

        it('should set "isActive" flag true if policy status contains '+
            '"acordHoldingStatus" with value "active"' , function () {
            var response = parser._setActivePolicyFlag({
                policyStatus: {
                    acordHoldingStatus: 'Active'
                }
            });
            expect(response.isActive).to.be.true;
        });

        it('should not set "isActive" flag true if acordHoldingStatus '+
            'is not "active"', function () {
            var response = parser._setActivePolicyFlag({
                policyStatus: {
                    acordHoldingStatus: 'Proposed'
                }
            });
            expect(response.isActive).to.be.undefined;
        });

    }); // _setActivePolicyFlag method

    describe ('_setShowflag method', function (){
        var response;
        it('exists as a function', function () {
            expect(parser._setShowflag).to.exist.and.be.a('function');
        });

        it(' "show" property should be true in "policyStatus"' , function () {
            response = parser._setShowflag(policyDataCopy);
            expect(response.policyStatus.show).to.be.true;
        });

        it('"Show property" should be false if '+
                '"acordHoldingStatus" is "Dormant/Unknown"', function () {

            policyDataCopy.policyStatus.acordHoldingStatus = 'Dormant';
            response = parser._setShowflag(policyDataCopy);
            expect(response.policyStatus.show).to.be.false;

            policyDataCopy.policyStatus.acordHoldingStatus = 'Unknown';
            response = parser._setShowflag(policyDataCopy);
            expect(response.policyStatus.show).to.be.false;
        });

    }); // _setShowflag method

    describe('_setShowAgentsFlag method', function () {
        it('exists as a function', function () {
            expect(parser._setShowAgentsFlag).to.exist.and.be.a('function');
        });

        it('should update "show" flags if paid date is '+
                'beyond 30 days and policy in active state', function () {
            var data = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            data.activePolicy = true;
            data.application.paid = '2017-02-05T12:50:00Z';
            var response = parser._setShowAgentsFlag(data, {
                after30Days:true
            });

            expect(response.showServicingProducer).to.be.true;
            expect(response.showWritingProducers).to.be.false;
        });

        it('should update "show" flags if paid date is within 30 days', function () {
            var data = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            data.activePolicy = true;
            data.application.paid = helpers.viewHelpers.getISODate('-', 2);
            var response = parser._setShowAgentsFlag(data, {
                after30Days:false
            });

            expect(response.showServicingProducer).to.be.false;
            expect(response.showWritingProducers).to.be.true;
        });
    }); // _setShowAgentsFlag method

    describe('_sortWritingProducers method', function () {
        it('exists as a function', function () {
            expect(parser._sortWritingProducers).to.exist.and.be.a('function');
        });

        it('should sort writing producer in descending order based on splitPercent', function () {
            var data = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            var expectedOutput = [
                {
                    'fullName': 'Christopher E Ekstrom',
                    'roleCode': 'dg33442',
                    'id': 54555,
                    'splitPercent' : 0.7
                },
                {
                    'fullName': 'John Smith',
                    'roleCode': 'js8828',
                    'id': 12345,
                    'splitPercent' : 0.3
                }
            ];
            expect(parser._sortWritingProducers(data).writingProducers)
                .to.deep.equal(expectedOutput);
        });
        
    }); // _sortWritingProducers method

    describe('_setAlertAttentionMessage method', function () {
        it('exists as a function', function () {
            expect(parser._setAlertAttentionMessage).to.exist.and.be.a('function');
        });

        describe('should return expected alertAttentionMessage object', function () {
            var data = {
                policyStatus : {
                    'acordHoldingStatus'    : 'Active',
                    'statusDetail'          : 'Premium paying',
                    'status'                : 'Issued',
                    'workflowStatus'        : 'Paid',
                    'description'           : 'Active',
                    'date'                  : '2016-02-21T15:50:00Z'
                }
            };
            var alertMessage;
            var expected;

            it('when policy is Active Scope SLRP - has onlySummaryDataAvailable flag '+
                    'and even policy is Active (statusView)', function () {
                data.policyStatus.onlySummaryDataAvailable = true;
                data.policyStatus.statusView = 'Active';
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.activeScopeSLRPPolicy;
                expect(expected).to.deep.equal(alertMessage);

                delete data.policyStatus.onlySummaryDataAvailable;
                delete data.policyStatus.statusView;
            });

            it('when policy in under construction', function () {
                data.policyStatus.statusView = 'Active';
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.underConstruction;
                expect(expected).to.deep.equal(alertMessage);
            });

            it('return under construction message even policy is terminated', function () {
                data.policyStatus.statusView = 'Active';
                data.policyStatus.acordHoldingStatus = 'Inactive';
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.underConstruction;
                expect(expected).to.deep.equal(alertMessage);
            });

            it('when policy is terminated', function () {
                delete data.policyStatus.statusView;
                data.policyStatus.acordHoldingStatus = 'Inactive';
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.terminated;
                expect(expected).to.deep.equal(alertMessage);
            });

            it('when policy is suspended', function () {
                data.policyStatus.acordHoldingStatus = 'Dormant';
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.suspended;
                expect(expected).to.deep.equal(alertMessage);
            });

            it('when policy has inMaintenance and deathClaimPending', function () {
                it('when policy is suspended', function () {
                    data.policyStatus.acordHoldingStatus = 'Active';
                    data.policyStatus.inMaintenance = true;
                    data.policyStatus.deathClaimPending = true;
                    alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                    expected = parser.alertAttentionMessages.maintenanceAndDeathClaimPending;
                    expect(expected).to.deep.equal(alertMessage);
                });
            });

            it('when policy has inMaintenance', function () {
                data.policyStatus.acordHoldingStatus = 'Active';
                data.policyStatus.inMaintenance = true;
                delete data.policyStatus.deathClaimPending;
                alertMessage = parser._setAlertAttentionMessage(data).alertAttentionMessage;
                expected = parser.alertAttentionMessages.maintenance;
                expect(expected).to.deep.equal(alertMessage);
            });
        });

    }); // _setAlertAttentionMessage method

});

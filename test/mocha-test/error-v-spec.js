/* global expect:false, _:false, Backbone:false */

var ErrorPageView = require('../dist/pages/error/views/error-v');

describe('Error Page (pages/error/views/error-v.js)', function() {
    
    it('exists', function() {
        expect(ErrorPageView).to.exist;
    });
    
    describe('Error Page Rendering', function() {

        var rootView;
        var errorPageView;

        var createRootView = function() {
            return new Backbone.Marionette.LayoutView({
                el: 'body',
                template: _.template('<div id=\'content\' ></div>'),
                regions: {
                    contentRegion: '#content'
                }
            });
        };

        beforeEach(function() {
            rootView = createRootView();
            rootView.render();
        });

        afterEach(function() {
            if (errorPageView) {
                errorPageView.destroy();
            }
        });

        it('with a default message', function() {
            errorPageView = new ErrorPageView();
            rootView.showChildView('contentRegion', errorPageView);
            
            var messageText = Backbone.$('.alert p').html();
            
            expect(messageText).to.equal(ErrorPageView.prototype.errors.defaultError);
        });

        it('with a custom message passed in the stateObj', function() {
            var errorMessage = 'You have broken it, you fool!';
            errorPageView = new ErrorPageView({
                stateObj: {
                    message: errorMessage
                }
            });
            rootView.showChildView('contentRegion', errorPageView);

            var messageText = Backbone.$('.alert p').html();

            expect(messageText).to.equal(errorMessage);
        });

        it('with a default message if "message" passed in the stateObj is blank', function() {
            errorPageView = new ErrorPageView({
                stateObj : {
                    message : ''
                }
            });
            rootView.showChildView('contentRegion', errorPageView);
            
            var messageText = Backbone.$('.alert p').html();
            
            expect(messageText).to.equal(ErrorPageView.prototype.errors.defaultError);
        });

        it('with an access error message if "status" passed in the stateObj is 403', function () {
            errorPageView = new ErrorPageView({
                stateObj : {
                    status : '403'
                }
            });
            rootView.showChildView('contentRegion', errorPageView);
            
            var messageText = Backbone.$('.alert p').html();
            
            expect(messageText).to.equal('The page you requested does not exist.');
        });

        it('with a default error message if "status" passed in the stateObj is blank', function () {
            errorPageView = new ErrorPageView({
                stateObj : {
                    status : ''
                }
            });
            rootView.showChildView('contentRegion', errorPageView);
            
            var messageText = Backbone.$('.alert p').html();
            
            expect(messageText).to.equal(ErrorPageView.prototype.errors.defaultError);
        });
    });
});

/* global expect:false, Backbone:false, $:false */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyListDataTableInfoView  
        = require('../dist/pages/policy/views/policy-list-datatable-info-v');
var helpers = require('./helpers/helpers');

var modelResponseBody = $.extend(true,{},helpers.policyListData.policyList);

describe('Policy List DataTable Info View ' +
    '(pages/policy/views/policy-list-datatable-info-v.js)', function () {
    var ajaxStub;
    var rootView;
    var view;
    var listener;
    var setProducerInfoStub;
    var setStatusCellColorSpy;

    before(function() {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {

            /*eslint no-undef:0 */
            this.sinon = sinon.sandbox.create();
        }

        setStatusCellColorSpy = this.sinon.spy(PolicyListDataTableInfoView.prototype,
            '_setStatusCellColor');

        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            modelResponseBody
        );
        rootView = helpers.viewHelpers.createRootView();
        view = new PolicyListDataTableInfoView({
            producerOrgId : '100001'
        });
        setProducerInfoStub = this.sinon.stub();
        listener  = new Backbone.Marionette.Object();
        listener.listenTo(
            view, 'setProducerInfo', setProducerInfoStub
        );
        rootView.render();
        rootView.showChildView('contentRegion', view);
    });

    after(function() {
        ajaxStub.restore();
        listener.stopListening();
        view.destroy();
        setStatusCellColorSpy.restore();
    });

    it('exists', function() {
        expect(PolicyListDataTableInfoView).to.exist;
    });
    
    it('should render view without any error', function () {
        expect(view.isRendered).to.be.true;
    });

    it('should not return error if producerId is missing', function () {
        var fn = function () {
            view = new PolicyListDataTableInfoView();
        };
        expect(fn).to.not.throw();
    });

    // These stopped working after upgrade to jQuery 3. --RKC Jan 29, 2018
    describe.skip('UI element ID selector  ', function () {
        it('If "options" has "hierarchy" item with value "org"', function () {
            view = new PolicyListDataTableInfoView({
                hierarchy : 'org'
            });
            rootView.showChildView('contentRegion', view);

            expect(view.ui.lastRefreshDate.selector)
                .to.equal('#org-last-refresh-date');
            expect(view.ui.policyListDatatable.selector)
                .to.equal('#org-policy-list-datatable');
            expect(view.ui.policyListDTWrapper.selector)
                .to.equal('#org-policy-list-dt-wrapper');
        });

        it('If "options" doesn\'t have "hierarchy" item', function () {
            setStatusCellColorSpy.resetHistory();
            view = new PolicyListDataTableInfoView({
                producerOrgId : '100001'
            });
            rootView.showChildView('contentRegion', view);

            expect(view.ui.lastRefreshDate.selector)
                .to.equal('#last-refresh-date');
            expect(view.ui.policyListDatatable.selector)
                .to.equal('#policy-list-datatable');
            expect(view.ui.policyListDTWrapper.selector)
                .to.equal('#policy-list-dt-wrapper');
        });
    });

    describe('OnRender method - Org Producer List ', function () {
        before(function () {
            view = new PolicyListDataTableInfoView({
                hierarchy : 'org'
            });

            setStatusCellColorSpy.resetHistory();

            rootView.showChildView('contentRegion', view);
        });

        it('table should rendered with 7 column', function () {
            expect(view.$el.find('table')).to.have.lengthOf(1);
            expect(view.$el.find('table tr:eq(0) th')).to.have.lengthOf(7);
        });

        it('Last column heading should match ', function () {
            expect(view.$el.find('table tr:eq(0) th:eq(6)').text())
                .to.be.contain('Servicing Agent');
        });

        it('should called "setProducerInfo" event', function () {
            expect(setProducerInfoStub).to.be.calledWith(modelResponseBody.producer);
        });

        it('should call "_setStatusCellColorSpy" for each row of data', function () {
            var expectedCount = modelResponseBody.data.length;

            expect(setStatusCellColorSpy.callCount).to.equal(expectedCount);
        });

        it('DataTable language info label should update with producer name', function (){
            expect(view.$el.find('.dataTables_info span').text())
                .to.be.contain(modelResponseBody.producer.fullName);
        });

        it('first row - first column (Name) content should match', function () {
            expect(view.$el.find('table tr:eq(1) td:eq(0)').text())
                .to.be.contain(modelResponseBody.data[0].customer.lexicalName);
        });

        it('first row - third column (App Recieved) content should match', function () {
            expect(view.$el.find('table tr:eq(1) td:eq(3)').text())
                .to.be.contain('05/16/2016');
        });

        it('first row - fifth column (Status Date) content should match', function () {
            expect(view.$el.find('table tr:eq(1) td:eq(5)').text())
                .to.be.contain('05/18/2016');
        });

        describe ('5th row - 4th column (status)', function (){
            
            it('Should be empty if status description has "dataAvailability" property and '+
                'it values is "notAvailable"', function (){
                expect(view.$el.find('table tr:eq(5) td:eq(4)').text().trim())
                    .to.be.empty;
            });

            it('Should not be empty if data exist for status description property', function () {
                expect(view.$el.find('table tr:eq(4) td:eq(4)').text().trim())
                    .to.be.equal('Paid');
            });
        });

        describe ('Policy List under paid Tab' , function () {
            
            it ('Column (5) with Status Date should replace with Paid Date', function () {
                view = new PolicyListDataTableInfoView({
                    producerOrgId : '100001',
                    status : 'paid'
                });
                rootView.showChildView('contentRegion', view);

                // Table header label
                expect(view.$el.find('table tr:eq(0) th:eq(5)').text())
                .to.be.contain('Paid Date');

                // first row value
                expect(view.$el.find('table tr:eq(1) td:eq(5)').text())
                .to.be.contain('06/30/2016');
            });
        });

    });

    describe('OnRender method - Producer List ', function () {
        before(function() {
            view = new PolicyListDataTableInfoView({
                producerId : '100001'
            });
            rootView.showChildView('contentRegion', view);
        });
        
        it('table should rendered with 6 column', function () {
            expect(view.$el.find('table')).to.have.lengthOf(1);
            expect(view.$el.find('table tr:eq(0) th')).to.have.lengthOf(6);
        });

        it('Last column heading should match ', function () {
            expect(view.$el.find('table tr:eq(0) th:eq(5)').text())
                .to.be.contain('Status Date');
        });

        it('should called "setProducerInfo" event', function () {
            expect(setProducerInfoStub).to.be.calledWith(modelResponseBody.producer);
        });

        describe ('Policy List under paid Tab' , function () {
            
            it ('Column (5) with Status Date should replace with Paid Date', function () {
                view = new PolicyListDataTableInfoView({
                    producerId : '100001',
                    status : 'paid'
                });
                rootView.showChildView('contentRegion', view);

                // Table header label
                expect(view.$el.find('table tr:eq(0) th:eq(5)').text())
                .to.be.contain('Paid Date');

                // first row value
                expect(view.$el.find('table tr:eq(1) td:eq(5)').text())
                .to.be.contain('06/30/2016');
            });
        });

    });

    describe('Last Refresh Date', function () {

        it('should be displayed if returned from the service', function () {
            var expectedDate = '12/28/2016, 11:50 AM EST';
            expect(view.$el.find('#last-refresh-date').text().trim()).to.equal(expectedDate);
        });

        it('container element should be hidden if lastRefreshDate is not returned ' +
                'from the service', function () {
            var responseNoDate = $.extend(true,{},modelResponseBody);
            delete responseNoDate.lastRefreshDate;

            // set up the ajaxStub and view with the new data
            ajaxStub.restore();
            view.destroy();

            ajaxStub.yieldsTo(
                'success',
                responseNoDate
            );

            rootView = helpers.viewHelpers.createRootView();
            view = new PolicyListDataTableInfoView({
                producerId : '100001'
            });

            rootView.render();
            rootView.showChildView('contentRegion', view);

            expect(view.$el.find('#last-refresh-date').parent().hasClass('hidden')).to.be.true;
        });
    });

    describe('_getTableInstructionTextId method', function () {

        it('should return "policy-list_instruction" if "hierarchy" and "status" are ' +
            'falsey', function () {
            var expected = 'policy-list_instruction';
            var result   = view._getTableInstructionTextId(null, undefined);

            expect(result).to.equal(expected);
        });

        it('should return "org-policy-list_instruction if "hierarchy" is "org" and ' +
            '"status" is falsey', function () {
            var expected = 'org-policy-list_instruction';
            var result   = view._getTableInstructionTextId('org', null);

            expect(result).to.equal(expected);
        });

        it('should return "org-paid-policy-list_instruction" if "hierarchy" is ' +
            '"org" and "status is "paid"', function () {
            var expected = 'org-paid-policy-list_instruction';
            var result   = view._getTableInstructionTextId('org', 'paid');

            expect(result).to.equal(expected);
        });
    }); // _getTableInstructionTextId method

});

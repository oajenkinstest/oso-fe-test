/* global expect:false, $:false, isCanvasLoaded:false, sinon:false */

var helpers = require('./helpers/helpers');
var utils   = require('../dist/utils/utils');

var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);


describe('Performance Center View : Life Lives '+
    '(modules/performance-center/views/performance-center-v.js)', function () {

    var responseBody        = helpers.pcData.responseBody;
    var responseBodyGoals   = $.extend(true,{},helpers.pcData.responseBodyGoals);
    var responseBodyIB      = helpers.pcData.responseBodyIB;

    var rootView;
    var model;
    var view;
    var ajaxStub;
    before(function () {

        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
    
        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

    });

    after(function () {
        rootView.destroy();
    });

    describe('retail', function () {
        var lifeLivesData = responseBody.calendarYearProduction.lifeLives;
        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );
    
            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });
            rootView.contentRegion.show(view);
        });

        after(function () {
            ajaxStub.restore();
            view.destroy();
        });

        it('Life Lives section should displayed', function () {
            expect(view.$el.find('#lifeLives-region')).to.be.lengthOf(1);
        });

        describe('should match', function () {
            
            it('header', function (){
                expect(view.lifeLivesRegion.$el.find('h4').text())
                    .to.contain('Life Lives Bonus');
                expect(view.lifeLivesRegion.$el.find('h4').text())
                    .to.contain(utils.formatDate(lifeLivesData.asOfDate,'YYYY'));
            });

            it('dial value', function () {
                expect(view.lifeLivesRegion.$el.find('.chart-outer').data('percent'))
                    .to.equal(50);
            });

            describe('bullets points', function () {
                it('life lives bonus value', function () {
                    var expectedBonusPoint = helpers.viewHelpers.removeDuplicateWhitespace(
                        view.lifeLivesRegion.$el.find('#life-lives-bonus-values').text().trim()
                    );
                    var forNextBonus = lifeLivesData.forNextBonus;
                    var bonus =utils.formatAsCurrency(lifeLivesData.nextBonusValue,
                        true, true);
                    expect(expectedBonusPoint).to.equal(forNextBonus+' life lives needed to '+
                        'reach goal of 50 life lives bonus and a '+bonus+' bonus!');
                });

                it('life lives percent', function () {
                    var expectedLivesPercent = helpers.viewHelpers.removeDuplicateWhitespace(
                        view.lifeLivesRegion.$el.find('#life-lives-percent-value').text().trim()
                    );
                    var totalSold = lifeLivesData.totalSold;
                    expect(expectedLivesPercent).to.equal(totalSold+' life lives');
                });
            });
        });

        describe('goal achievement', function () {
            before(function () {
                ajaxStub.restore();
                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    responseBodyGoals
                );
                lifeLivesData = responseBodyGoals.calendarYearProduction.lifeLives;
        
                model = new PerformanceCenterModel();
                view = new PerformanceCenterView({
                    model: model
                });
                rootView.contentRegion.show(view);
            });
    
            after(function () {
                ajaxStub.restore();
                view.destroy();
            });
            it('check marks should be displayed', function () {
                expect(view.lifeLivesRegion.$el.find('.fa-check').length).to.equal(1);
            });

            describe('should match bullets points', function () {
                it('goal achieved message displayed as first bullet', function (){
                    expect(view.lifeLivesRegion.$el.find('#life-lives-goal-achieved').text().trim())
                        .to.equal('Goal achieved!');
                });

                it('life lives percent', function () {
                    var expectedLivesPercent = helpers.viewHelpers.removeDuplicateWhitespace(
                        view.lifeLivesRegion.$el.find('#life-lives-percent-value').text().trim()
                    );
                    var totalSold = lifeLivesData.totalSold;
                    expect(expectedLivesPercent).to.equal(totalSold+' life lives');
                });

            });
        });
    });

    describe('ib', function () {
        it('life lives should not be displayed', function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBodyIB
            );
    
            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });
            rootView.contentRegion.show(view);
            
            expect(view.$el.find('#lifeLives-region')).to.be.lengthOf(0);

            ajaxStub.restore();
            view.destroy();
        });
    });
});

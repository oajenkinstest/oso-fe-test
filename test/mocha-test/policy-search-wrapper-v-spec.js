/* global expect:false, Backbone:false, _:false */

var helpers = require('./helpers/helpers');

var PolicySearchWrapper = require('../dist/pages/policy/views/policy-search-wrapper-v');

describe('Search Wrapper View (pages/policy/views/policy-search-wrapper-v)', function () {

    describe('Initialization Tests', function () {

        describe('No state tests', function () {

            var view;

            beforeEach(function () {
                view = new PolicySearchWrapper();
            });

            afterEach(function () {
                if (view) {
                    view.destroy();
                }
            });

            it('Creates a PolicySearchModule and listens to events', function () {
                expect(view.policySearchModule).to.exist;
            });

            it('Creates a PolicySearchFormView and listens to events', function () {
                expect(view.searchFormView).to.exist;
                expect(view.searchFormView.model).to.exist;
            });
        });

        describe('Initialize with state', function () {

            var view;

            beforeEach(function () {
                view = new PolicySearchWrapper({
                    searchTerm: 'Gojira',
                    searchType: 'clientName'
                });
            });

            afterEach(function () {
                if (view) {
                    view.destroy();
                }
            });

            it('Creates a PolicySearchFormView with the proper model', function () {
                expect(view.searchFormView).to.exist;
                expect(view.searchFormView.model).to.exist;
                expect(view.searchFormView.model.get('searchTerm')).to.equal('Gojira');
                expect(view.searchFormView.model.get('searchType')).to.equal('clientName');
            });
        });
    });

    describe('Calls policySearchModule.performClientNameSearch onBeforeShow', function () {

        var rootView;
        var view;
        var clientSearchStub;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper({
                searchTerm : 'Gojira',
                searchType : 'clientName'
            });
            clientSearchStub = this.sinon.stub(view.policySearchModule, 'performClientNameSearch');

            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('Calls performClientNameSearch', function () {
            expect(clientSearchStub).to.have.been.calledWith('Gojira', {});
        });
    });

    describe('Calls policySearchModule.performClientNameSearch with searchState ' +
        'onBeforeShow', function () {

        var searchState = {
            col : 0,
            dir : 'desc'
        };
        var rootView;
        var view;
        var clientSearchStub;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper({
                searchState : searchState,
                searchTerm  : 'Gojira',
                searchType  : 'clientName'
            });
            clientSearchStub = this.sinon.stub(view.policySearchModule, 'performClientNameSearch');

            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('Calls performClientNameSearch', function () {
            expect(clientSearchStub).to.have.been.calledWith('Gojira', searchState);
        });
    });

    describe('Calls policySearchModule.performProducerNameSearch in onBeforeShow', function () {

        var rootView;
        var view;
        var producerSearchStub;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper({
                searchTerm : 'Gojira',
                searchType : 'producerName'
            });

            producerSearchStub = this.sinon.stub(
                view.policySearchModule,
                'performProducerNameSearch'
            );

            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('Calls performClientNameSearch', function () {
            expect(producerSearchStub).to.have.been.calledWith('Gojira', {});
        });
    });

    describe('Calls policySearchModule.performProducerNumberSearch in onBeforeShow', function () {

        var rootView;
        var view;
        var producerSearchStub;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper({
                searchTerm : '23232131',
                searchType : 'producerNumber'
            });

            producerSearchStub = this.sinon.stub(
                view.policySearchModule,
                'performProducerNumberSearch'
            );

            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('Calls performProducerNumberSearch', function () {
            expect(producerSearchStub).to.have.been.calledWith('23232131', {});
        });
    });

    describe('_performClientNameSearch method', function () {

        var clearResultsSpy;
        var listener;
        var searchModuleStub;
        var stateChangeStub;
        var view;

        beforeEach(function () {
            listener        = new Backbone.Marionette.Object();
            stateChangeStub = this.sinon.stub();
            view            = new PolicySearchWrapper();

            searchModuleStub = this.sinon.stub(
                view.policySearchModule,
                'performClientNameSearch'
            );

            listener.listenTo(view, 'stateChange', stateChangeStub);

            clearResultsSpy = this.sinon.spy(view, '_clearCurrentResults');

            view.render();
        });

        afterEach(function () {
            searchModuleStub.restore();
            clearResultsSpy.restore();
            view.destroy();
            listener.destroy();
        });

        it('function exists', function () {
            expect(view._performClientNameSearch).to.exist.and.be.a('function');
        });

        it('clears the current results', function () {
            view._performClientNameSearch('Gojira');
            expect(clearResultsSpy).to.have.been.called;
        });

        it('triggers a "stateChange" event', function () {
            view._performClientNameSearch('Gojira');

            expect(stateChangeStub).to.have.been.calledWith({
                searchTerm : 'Gojira',
                searchType : 'clientName'
            },true);
        });

        it('calls a function on the policySearchModule', function () {
            view._performClientNameSearch('Gojira');
            expect(searchModuleStub).to.have.been.calledWith('Gojira');
        });
    });

    describe('_performPolicyNumberSearch method', function () {
        var clearResultsSpy;
        var searchModuleStub;
        var view;

        beforeEach(function () {
            view            = new PolicySearchWrapper();

            searchModuleStub = this.sinon.stub(
                view.policySearchModule,
                'performPolicyNumberSearch'
            );
            clearResultsSpy = this.sinon.spy(view, '_clearCurrentResults');

            view.render();
        });

        afterEach(function () {
            searchModuleStub.restore();
            clearResultsSpy.restore();
            view.destroy();
        });

        it('should exist', function () {
            expect(view._performPolicyNumberSearch).to.exist.and.be.a('function');
        });

        it('clears the current results', function () {
            view._performPolicyNumberSearch(123456789);
            expect(clearResultsSpy).to.have.been.called;
        });

        it('calls a function on the policySearchModule', function () {
            view._performPolicyNumberSearch(123456789);
            expect(searchModuleStub).to.have.been.calledWith(123456789);
        });
    });

    describe('_performProducerNameSearch method', function () {
        var clearResultsSpy;
        var listener;
        var searchModuleStub;
        var stateChangeStub;
        var view;

        beforeEach(function () {
            listener        = new Backbone.Marionette.Object();
            stateChangeStub = this.sinon.stub();
            view            = new PolicySearchWrapper();

            searchModuleStub = this.sinon.stub(
                view.policySearchModule,
                'performProducerNameSearch'
            );

            listener.listenTo(view, 'stateChange', stateChangeStub);

            clearResultsSpy = this.sinon.spy(view, '_clearCurrentResults');

            view.render();
        });

        afterEach(function () {
            searchModuleStub.restore();
            clearResultsSpy.restore();
            view.destroy();
            listener.destroy();
        });

        it('function exists', function () {
            expect(view._performProducerNameSearch).to.exist.and.be.a('function');
        });

        it('clears the current results', function () {
            view._performProducerNameSearch('Gojira');
            expect(clearResultsSpy).to.have.been.called;
        });

        it('triggers a "stateChange" event', function () {
            view._performProducerNameSearch('Gojira');

            expect(stateChangeStub).to.have.been.calledWith({
                searchTerm : 'Gojira',
                searchType : 'producerName'
            }, true);
        });

        it('calls a function on the policySearchModule', function () {
            view._performProducerNameSearch('Gojira');
            expect(searchModuleStub).to.have.been.calledWith('Gojira');
        });
    });

    describe('_performProducerNumberSearch method', function () {
        var clearResultsSpy;
        var listener;
        var searchModuleStub;
        var stateChangeStub;
        var view;

        beforeEach(function () {
            listener        = new Backbone.Marionette.Object();
            stateChangeStub = this.sinon.stub();
            view            = new PolicySearchWrapper();

            searchModuleStub = this.sinon.stub(
                view.policySearchModule,
                'performProducerNumberSearch'
            );

            listener.listenTo(view, 'stateChange', stateChangeStub);

            clearResultsSpy = this.sinon.spy(view, '_clearCurrentResults');

            view.render();
        });

        afterEach(function () {
            searchModuleStub.restore();
            clearResultsSpy.restore();
            view.destroy();
            listener.destroy();
        });

        it('function exists', function () {
            expect(view._performProducerNumberSearch).to.exist.and.be.a('function');
        });

        it('clears the current results', function () {
            view._performProducerNumberSearch('121212');
            expect(clearResultsSpy).to.have.been.called;
        });

        it('triggers a "stateChange" event', function () {
            view._performProducerNumberSearch('121212');

            expect(stateChangeStub).to.have.been.calledWith({
                searchTerm : '121212',
                searchType : 'producerNumber'
            }, true);
        });

        it('calls a function on the policySearchModule', function () {
            view._performProducerNumberSearch('121212');
            expect(searchModuleStub).to.have.been.calledWith('121212');
        });
    });

    describe('_showResultsView method', function () {

        var view;
        var rootView;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper();
            rootView.showChildView('contentRegion', view);
            view.showChildView = this.sinon.stub();
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('calls the view\'s showChildView function', function () {
            var itemView = new Backbone.Marionette.ItemView();
            view._showResultsView(itemView);
            expect(view.showChildView).to.have.been.calledWith('searchResults', itemView);
        });
    });

    describe('_showServerMessage method', function () {

        var view;
        var rootView;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view= new PolicySearchWrapper();
            rootView.showChildView('contentRegion', view);
            view.searchFormView.showServerMessage = this.sinon.stub();
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('calls the search form\'s showServerMessage function', function () {
            var message = 'This is a message';
            var type    = 'someType';

            view._showServerMessage(message, type);
            expect(view.searchFormView.showServerMessage)
                .to.have.been.calledWith(message, type);
        });
    });

    describe('_showPolicy method', function () {
        var listener;
        var showPolicyStub;
        var view;

        beforeEach(function () {
            showPolicyStub = this.sinon.stub();
            view = new PolicySearchWrapper();
            listener = new Backbone.Marionette.Object();
            listener.listenTo(view, 'showPolicy', showPolicyStub);
        });

        afterEach(function () {
            view.destroy();
        });

        it('triggers a "showPolicy" event', function () {
            var policyId = '918273645';
            view._showPolicy(policyId);
            expect(showPolicyStub).to.have.been.calledWith(policyId);
        });
    });

    describe('_clearCurrentResults method', function () {

        var view;
        var rootView;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper();
            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            view.destroy();
            rootView = null;
        });

        it('Does not throw an error if the region is empty', function () {
            var fn = function () {
                view._clearCurrentResults();
            };

            expect(view.getChildView('searchResults')).to.not.exist;
            expect(fn).to.not.throw(Error);
        });

        it('Destroys the results view if it exists', function () {
            var resultsView = new Backbone.Marionette.ItemView({
                template: _.template('<div id=\'bigDummyView\'></div>')
            });
            view.showChildView('searchResults', resultsView);

            expect(view.getChildView('searchResults')).to.exist;
            view._clearCurrentResults();
            expect(view.getChildView('searchResults')).to.not.exist;
        });
    });

    describe('_notifyOfStateChange method', function () {
        var listener;
        var view;
        var stateChangeStub;
        var rootView;

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new PolicySearchWrapper();
            stateChangeStub = this.sinon.stub();
            listener = new Backbone.Marionette.Object();
            listener.listenTo(view, 'stateChange', stateChangeStub);
            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            listener.destroy();
            view.destroy();
        });

        it('triggers a "stateChange" event', function () {
            var state = {
                prop1 : 'value1',
                prop2 : 'value2'
            };

            view._notifyOfStateChange(state);
            expect(stateChangeStub).to.have.been.calledWith(state);
        });

        it('Includes the searchTerm and searchType values', function () {
            var searchInfo = {
                searchTerm : 'Gojira',
                searchType : 'clientName'
            };
            var state = {
                prop1 : 'value1',
                prop2 : 'value2'
            };
            var expectedState = {
                prop1      : 'value1',
                prop2      : 'value2',
                searchTerm : 'Gojira',
                searchType : 'clientName'
            };

            view.searchFormView.model = new Backbone.Model(searchInfo);
            view._notifyOfStateChange(state);
            expect(stateChangeStub).to.have.been.calledWith(expectedState);
        });
    });
});

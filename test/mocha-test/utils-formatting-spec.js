/* global describe: false, expect:false */

/**
 * Formatting Utility module spec
 */

var utils   = require('../dist/utils/utils-formatting');
var helpers = require('./helpers/helpers');

describe('Utility - Fomatting (utils/utils-formatting) Test Spec', function() {

    it('utilsFormatting should exist', function() {
        expect(utils).to.exist;
    });

    describe('bytesToMegabytesForDisplay method', function () {
        var resultInMb;

        it('should convert values correctly', function () {
            var bytes         = 10800332;
            var expectedValue = '10.3';

            resultInMb = utils.bytesToMegabytesForDisplay(bytes);

            expect(resultInMb).to.equal(expectedValue);
        });

        it('should throw an error if a number is not passed', function () {
            var expectedError = utils.errors.bytesToMegabytes;
            var myString = 'foo';
            var fn = function () {
                utils.bytesToMegabytesForDisplay(myString);
            };

            expect(fn).to.throw(expectedError);
        });

        it('should return value with one decimal point', function () {
            var bytes         = 10951747;    // 10.4444 MB
            var expectedValue = '10.4';

            resultInMb = utils.bytesToMegabytesForDisplay(bytes);

            expect(resultInMb).to.equal(expectedValue);
        });
    });     // bytesToMegabytesForDisplay method

    describe('titleCase method', function(){
        it('exist and be a function', function(){
            expect(utils.titleCase).to.exist.and.be.a('function');
        });

        it('should return title case', function () {
            expect(utils.titleCase('policy search')).to.equal('Policy Search');
        });

        it('should return blank if input is undefined', function () {
            expect(utils.titleCase(undefined)).to.equal('');
        });
    });

    describe('formatAsCurrency method', function () {
        it('formatAsCurrency should exist', function() {
            expect(utils.formatAsCurrency).to.exist;
            expect(utils.formatAsCurrency).to.be.a('function');
        });

        it('formatAsCurrency should return expected format', function() {
            expect(utils.formatAsCurrency('USD 328176.44', true)).to.equal('$328,176');
            expect(utils.formatAsCurrency('USD -328176', true)).to.equal('-$328,176');
        });

        it('formatAsCurrency should not round if round flag is false', function() {
            expect(utils.formatAsCurrency('USD 328176.45')).to.equal('$328,176.45');
        });

        it('formatAsCurrency should return \'--\' if number is 0 '+
            'and when spaceZero flag is true', function() {
            expect(utils.formatAsCurrency('USD 0.0',true, true)).to.equal('--');
        });

        it('formatAsCurrency should not return \'--\' if number is > 0 '+
            'and when spaceZero flag is true', function() {
            expect(utils.formatAsCurrency('USD 4000.0', true, true)).to.equal('$4,000');
        });

    }); // "formatAsCurrency" method

    describe('"isoCurrencyToNumber" method', function () {
        it('isoCurrencyToNumber should exist', function() {
            expect(utils.isoCurrencyToNumber).to.exist;
            expect(utils.isoCurrencyToNumber).to.be.a('function');
        });

        it('isoCurrencyToNumber converts iso-4217 into numeric values', function() {
            var numericVal = utils.isoCurrencyToNumber('USD 123456.78', false, false);
            expect(numericVal).to.equal(123456.78);
        });

        it('isoCurrencyToNumber can round values', function() {
            var numericVal = utils.isoCurrencyToNumber('USD 123456.78', false, true);
            expect(numericVal).to.equal(123457);
        });

        it('isoCurrencyToNumber can return absolute values', function() {
            var numericVal = utils.isoCurrencyToNumber('USD -123456.78', true, false);
            expect(numericVal).to.equal(123456.78);
        });
    }); // "isoCurrencyToNumber" method

    describe('"isoDurationToTimeUnit" function', function () {

        it('should exist', function () {
            expect(utils.isoDurationToTimeUnit).to.be.a('function');
        });

        it('returns a numeric time unit of an ISO-8601 duration', function () {
            var testDuration = 'P3Y6M7W4DT12H30M5S';
            var expectedResult = 3;
            var result = utils.isoDurationToTimeUnit(testDuration, 'year');

            expect(result).to.equal(expectedResult);
        });
    }); // "isoDurationToTimeUnit" function

    describe('"dateDiff" method', function () {
        it('to be a function', function (){
            expect(utils.dateDiff).to.be.a('function');
        });

        it('should return date difference and default format will be '+
                '"days" if nothing is specified ', function () {
            var diff = utils.dateDiff('2017-05-19T00:00:00Z','2017-06-19T00:00:00Z');
            expect(diff).to.equal(31);
        });

        it('should return date difference in "years" ', function () {
            var diff = utils.dateDiff('2016-05-19T00:00:00Z','2017-06-19T00:00:00Z', 'years');
            expect(diff).to.equal(1);
        });

        it('should take second param as current date if param missing', function (){
            var startDate    = helpers.viewHelpers.getISODate('-', 1);
            var diff = utils.dateDiff(startDate);
            expect(diff).to.equal(1);
        });

        it('Should throw error if input parameter is not a string', function () {
            
            var fn = function () {
                utils.dateDiff(new Date());
            };
            expect(fn).to.throw(utils.errors.expectedDateInString);

            fn = function () {
                utils.dateDiff('2016-05-19T00:00:00Z' ,new Date());
            };
            expect(fn).to.throw(utils.errors.expectedDateInString);
        });

        describe( 'errors', function () {
            it('if wrong input for output format', function () {
                var fn = function () {
                    utils.dateDiff('2016-05-19T00:00:00Z','2017-06-19T00:00:00Z', 'some');
                };

                expect(fn).to.throw(utils.errors.wrongDateDiffFormat);
            });

            it('invalid date input', function (){
                var fn = function () {
                    utils.dateDiff('2016-5-19T00:00:00Z');
                };

                expect(fn).to.throw(utils.errors.invalidDate);
            });
        });
    }); // "dateDiff" method

    describe('"signOfNumber" method', function () {
        it('signOfNumber should exist', function() {
            expect(utils.signOfNumber).to.exist;
            expect(utils.signOfNumber).to.be.a('function');
        });

        it('signOfNumber should return -1 for negative value', function() {
            var sign = utils.signOfNumber(-2000);
            expect(sign).to.equal(-1);
        });

        it('signOfNumber should return -1 if input is valid number string', function() {
            var sign = utils.signOfNumber('-2000');
            expect(sign).to.equal(-1);
        });

        it('signOfNumber should return 1 for positive value', function() {
            var sign = utils.signOfNumber(2000);
            expect(sign).to.equal(1);
        });

        it('signOfNumber should return 1 if input is valid number string', function() {
            var sign = utils.signOfNumber('2000');
            expect(sign).to.equal(1);
        });

        it('signOfNumber should return 0 for value 0', function() {
            var sign = utils.signOfNumber(0);
            expect(sign).to.equal(0);
        });

        it('signOfNumber should return 0 for input as string \'0\'', function() {
            var sign = utils.signOfNumber('0');
            expect(sign).to.equal(0);
        });

        it('signOfNumber should return blank for undefined/null input ', function() {
            var sign = utils.signOfNumber();
            expect(sign).to.equal('');
        });

        it('signOfNumber should throw error if input is not numeric', function() {
            var fn = function () {
                utils.signOfNumber(['12','-122']);
            };

            expect(fn).to.throw('Expected a numeric input');
        });
    }); // "signOfNumber" method

    describe ('"formatDate" method', function () {
        it('formatDate should return expected format when converting timezones', function() {
            var date = utils.formatDate('2016-02-10T18:50:00', 'MM/DD/YYYY | hh:mmA z');
            expect(date).to.equal('02/10/2016 | 06:50PM EST');

            date = utils.formatDate('2016-10-20T18:50:00', 'MM/DD/YYYY - hh:mmA z');
            expect(date).to.equal('10/20/2016 - 06:50PM EDT');
        });

        it('formatDate should return expected format when format parameter is empty',
            function() {
                var date = utils.formatDate('2016-02-10T18:50:00');
                expect(date).to.equal('02/10/2016');

                date = utils.formatDate('2016-10-20');
                expect(date).to.equal('10/20/2016');
            });

        it('formatDate should return blank if date provided is undefined', function() {
            var date = utils.formatDate(undefined);
            expect(date).to.equal('');
        });

        it('formatDate should return error if date provided is invalid', function() {
            var fn = function () {
                utils.formatDate('some string');
            };

            expect(fn).to.throw(utils.errors.invalidDate);

        });
    }); // "formatDate" method

    describe ('"formatPhoneNumber" method', function () {
        it('Method formatPhoneNumber should exist', function () {
            expect(utils.formatPhoneNumber).to.exist;
        });

        it('Method formatPhoneNumber should format phone number correctly', function () {
            expect(utils.formatPhoneNumber('2325558989')).to.equal('(232) 555-8989');
        });

        it('Method formatPhoneNumber should return blank if input is undefined',
            function () {
            expect(utils.formatPhoneNumber()).to.equal('');
        });
    }); // "formatPhoneNumber" method
});

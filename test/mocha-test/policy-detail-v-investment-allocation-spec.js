/* global describe, $, _, expect, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

describe('Policy Detail View - Investment Allocation Section ' + 
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var copyOfPolicyDetail;
    var rootView;
    var view;

    var setView = function() {
        var newview = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        ajaxStub = this.sinon.stub($, 'ajax');
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    describe('Happy path tests with typical data', function () {

        before(function () {
            copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('PolicyDetailView exists', function () {
            expect(PolicyDetailView).to.exist;
        });

        it('section should display if data exist', function() {
            expect(view.$el.find('#allocation-info').length).to.equal(1);
        });

        describe('when viewing a variable annuity', function () {

            it('heading should read "Investment Allocation"', function () {
                var allocationHeader = view.$el.find('#allocation-info').prev();

                expect(allocationHeader.html()).to.equal('Investment Allocation');
            });

            it('table should contain two (2) columns', function () {
                var allocationTableHeaders = view.$el.find('#allocation-info table th');

                expect(allocationTableHeaders.length).to.equal(2);
            });

            it('first column header should be titled "Investment Account Name"', function () {
                var allocationTableHeader = view.$el.find('#allocation-info table th:eq(0)');

                expect(allocationTableHeader.html().trim()).to.equal('Investment Account Name');
            });
        }); // variable annuity
    }); // Happy path tests with typical data

    describe('AWD rip tests', function () {

        it('should not display if data not exist', function() {
            ajaxStub.yieldsTo(
                'success',
                helpers.policyData.pendingPolicyDetailAWDRip
            );
            view = setView();

            expect(view.$el.find('#allocation-info').length).to.equal(0);

            view.destroy();
        });
    }); // AWD rip tests

    it('should display "Section unavailable" message in bold text under header when ' +
        '"dataAvailability" attribute is set to "notAvailable"', function () {
        var investmentAllocationSection;
        var allocationNotAvailableData =
            $.extend(true, {}, helpers.policyData.pendingPolicyDetail);
        allocationNotAvailableData.investmentAllocation = { dataAvailability : 'notAvailable' };

        ajaxStub.yieldsTo('success', allocationNotAvailableData);

        view = setView();

        investmentAllocationSection = view.$el.find('#allocation-section-unavailable');
        expect(investmentAllocationSection.length).to.equal(1);
        expect(investmentAllocationSection.text().trim()).to.equal('Section unavailable');
    });

    describe('when viewing an indexed annuity', function () {

        before(function () {
            var indexedPolicy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
            indexedPolicy.product.productTypeCode = 'INXAN';

            _.each(indexedPolicy.investmentAllocation, function(investment) {
                investment.type = 'Fixed';
            });

            ajaxStub.yieldsTo(
                'success',
                indexedPolicy
            );

            view = setView();
        });

        after(function () {
            view.destroy();
        });
        
        it('heading should read "Allocation"', function () {
            var allocationHeader = view.$el.find('#allocation-info').prev();

            expect(allocationHeader.html()).to.equal('Allocation');
        });

        it('table should contain three (3) columns', function () {
            var allocationTableHeaders = view.$el.find('#allocation-info table th');

            expect(allocationTableHeaders.length).to.equal(3);
        });

        it('first column header should be titled "Account Strategy"', function () {
            var allocationTableHeader = view.$el.find('#allocation-info table th:eq(0)');

            expect(allocationTableHeader.html().trim()).to.equal('Account Strategy');
        });

        it('third column header should be titled "Type"', function () {
            var allocationTableHeader = view.$el.find('#allocation-info table th:eq(2)');

            expect(allocationTableHeader.html().trim()).to.equal('Type');
        });
    }); // indexed annuity

});

/* global expect:false, Backbone:false */
/* eslint-disable no-new */


// load behaviors
require('../dist/modules/behaviors/index');

var SiteWideBannerView = require('../dist/modules/siteWideBanner/views/siteWideBanner-v');

describe('Site-Wide Banner View (modules/siteWideBanner/views/siteWideBanner-v.js)', function () {
    var view;

    describe('initialize method', function () {

        describe('Should throw an error', function () {
            var expectedError = SiteWideBannerView.prototype.errors.model;
            var fn;

            it('when an argument is not passed', function () {
                fn = function () {
                    new SiteWideBannerView();
                };

                expect(fn).to.throw(expectedError);

            });

            it('when options passed is not an object', function () {
                fn = function () {
                    new SiteWideBannerView('I am not an object');
                };

                expect(fn).to.throw(expectedError);
            });

            it('when options passed do not contain a "wcmPath" key', function () {
                fn = function () {
                    new SiteWideBannerView({
                        notAWcmPath : 'Plain old string'
                    });
                };

                expect(fn).to.throw(expectedError);
            });

            it('when "wcmPath" value is not a string', function () {
                fn = function () {
                    new SiteWideBannerView({
                        wcmPath : 8675309
                    });
                };

                expect(fn).to.throw(expectedError);
            });

        });     // Should throw an error

    });     // initialize method

    describe('onBeforeShow method', function () {
        var spinnerChannel;
        var showSpy;

        beforeEach(function () {
            view = new SiteWideBannerView({
                wcmPath : 'myPath'
            });
        });

        it('should show the spinner', function () {
            spinnerChannel = Backbone.Radio.channel('spinner');
            showSpy        = this.sinon.spy();
            spinnerChannel.on('show', showSpy);

            view.onBeforeShow();

            expect(showSpy).to.have.been.calledOnce;

            spinnerChannel.stopListening();
        });

        it('should call fetch() on the model', function () {
            var fetchStub = this.sinon.stub(view.model, 'fetch');

            view.onBeforeShow();

            expect(fetchStub).to.have.been.calledOnce;

            fetchStub.restore();
        });

    });     // onBeforeShow method

    describe('_handleDisplay method', function () {
        var spinnerChannel;
        var hideSpy;
        var bannerBody = '<div id="banner-body">Oh say can you see?</div>';

        beforeEach(function () {
            view = new SiteWideBannerView({
                wcmPath : 'path'
            });

            view.model.set('body', bannerBody);
        });

        it('should hide the spinner', function () {
            spinnerChannel = Backbone.Radio.channel('spinner');
            hideSpy        = this.sinon.spy();

            spinnerChannel.on('hide', hideSpy);

            view._handleDisplay();

            expect(hideSpy).to.have.been.calledOnce;

            hideSpy.resetHistory();
            spinnerChannel.stopListening();
        });

        it('should display the content', function () {
            view._handleDisplay();

            expect(view.$el.find('#banner-body').length).to.equal(1);
        });

        it('if no content is returned do nothing', function () {
            var viewAfter;
            var viewBefore = view.$el;

            view.model.unset('body');
            view._handleDisplay();

            viewAfter = view.$el;

            expect(viewAfter).to.equal(viewBefore);
        });

    });     // _handleDisplay method

    describe('_handleWcmError method', function () {
        var analyticsChannel;
        var hideSpy;
        var response;
        var spinnerChannel;
        var trackExceptionSpy;

        beforeEach(function () {
            analyticsChannel = Backbone.Radio.channel('analytics');
            spinnerChannel   = Backbone.Radio.channel('spinner');

            hideSpy = trackExceptionSpy = this.sinon.spy();

            analyticsChannel.on('trackException', trackExceptionSpy);
            spinnerChannel.on('hide', hideSpy);

            response = {
                status : 500
            };

            view = new SiteWideBannerView({ wcmPath : 'path' });
            trackExceptionSpy.resetHistory();

            view._handleWcmError({}, response);
        });

        afterEach(function () {
            analyticsChannel.stopListening();
            spinnerChannel.stopListening();

            analyticsChannel = null;
            spinnerChannel   = null;
            response         = null;
        });

        it('should hide the spinner', function () {
            expect(hideSpy).to.have.been.called;
        });

        describe('Should send error message to analytics', function () {
            var analyticsMessage;
            var expectedPath;

            beforeEach(function () {
                analyticsMessage = trackExceptionSpy.getCalls()[1].args[0].message;
                expectedPath     = view.model.get('wcmPath');
            });

            it('using analytics channel', function () {
                expect(trackExceptionSpy).to.have.been.called;
            });

            it('with "wcmPath" value in message', function () {
                expect(analyticsMessage).to.contain(expectedPath);
            });

            it('with HTTP status code value in message', function () {
                expect(analyticsMessage).to.contain(response.status);
            });
        });

    });     // _handleWcmError method

    describe ('"collapseBanner" method', function () {
        var collapseBannerSpy;
        var bannerBody = '<a data-toggle="collapse" href="#alertcollapse" '+
                'aria-expanded="true" aria-controls="collapseExample" '+
                'class="sans-text-decoration">The <em>New</em> OneSource Online'+
                    '<i class="bigger-130 margin-right-5 ace-icon fa fa-angle-down" '+
                    'data-icon-hide="ace-icon fa fa-angle-down" '+
                    'data-icon-show="ace-icon fa fa-angle-left"></i>'+
                '</a>'+
                '<div class="collapse in" id="alertcollapse" aria-expanded="true"></div>';
                
        beforeEach (function () {
            collapseBannerSpy = this.sinon.spy(SiteWideBannerView.prototype, 'collapseBanner');
            view = new SiteWideBannerView({
                wcmPath         : 'path',
                collapseBanner  : true
            });

            view.model.set('body', bannerBody);
        });

        afterEach (function () {
            view.destroy();
            collapseBannerSpy.restore();
        });

        it('should exist', function () {
            expect(view.collapseBanner).to.exist.and.be.a('function');
        });

        it('should invoke by "_handleDisplay" with "collapseBanner" option', function () {
            
            view._handleDisplay();
            expect(collapseBannerSpy).to.have.been.calledWith(true);

            // collapseBanner option as false
            view = new SiteWideBannerView({
                wcmPath         : 'path',
                collapseBanner  : false
            });

            view.model.set('body', bannerBody);
            view._handleDisplay();
            expect(collapseBannerSpy).to.have.been.calledWith(false);

        });

        it('should collapse banner when flag passed is true', function (){
            view._handleDisplay();
            expect(view.$el.find('#alertcollapse').attr('aria-expanded')).to.equal('false');
        });

        it('should expand banner when flag passed is false', function (){
            view.options.collapseBanner = false;
            view._handleDisplay();
            expect(view.$el.find('#alertcollapse').attr('aria-expanded')).to.equal('true');
        });

    }); // "collapseBanner" method

});     // Site Wide Banner View tests

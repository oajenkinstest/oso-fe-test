/* global describe:false, Backbone:false, expect:false */

var PolicySummaryCollection =  require('../dist/pages/policy/collections/policy-summary-c');
var PolicySummaryModel      =  require('../dist/pages/policy/models/policy-summary-m');
var PolicySearchModule      = require('../dist/modules/policySearch/policySearchModule');

describe('Policy Search Module (modules/policySearch/policySearchModule.js)', function () {

    describe('initialization tests', function() {

        it('exists', function() {
            expect(PolicySearchModule).to.exist;
        });

        it('can be instantiated', function() {
            var policySearchModule = new PolicySearchModule();
            expect(policySearchModule).to.exist;
        });

    });

    describe('Interaction tests', function() {

        var idFoundStub;
        var listener;
        var messageStub;
        var policySearchModule;
        var showResultsStub;

        beforeEach(function() {
            idFoundStub        = this.sinon.stub();
            messageStub        = this.sinon.stub();
            showResultsStub    = this.sinon.stub();
            policySearchModule = new PolicySearchModule();

            listener = new Backbone.Marionette.Object();
            listener.listenTo(policySearchModule, 'showResultsView', showResultsStub);
            listener.listenTo(policySearchModule, 'serverMessage', messageStub);
            listener.listenTo(policySearchModule, 'policyIdFound', idFoundStub);
        });

        afterEach(function() {
            policySearchModule.destroy();
            listener.stopListening();
            listener.destroy();
        });

        describe('_handlePolicyNumberSearchError method', function() {

            it('triggers process error', function () {
                policySearchModule._handlePolicyNumberSearchError(null, {status: 404});
                expect(messageStub).to.have.been.calledWith(
                    PolicySearchModule.prototype.errors.noPolicies, 'info'
                );
            });

            it('triggers system error', function () {
                policySearchModule._handlePolicyNumberSearchError(null, {status: 500});
                expect(messageStub).to.have.been.calledWith(
                    PolicySearchModule.prototype.errors.systemError, 'warning'
                );
            });
        });

        describe('_handlePolicyNumberSearchResponse method', function() {

            var zeroResults = [];

            var singlePolicyId = 1234567890;

            var multiResultsMultiPolicies = new PolicySummaryCollection(
                [
                    new PolicySummaryModel({policyId:singlePolicyId}),
                    new PolicySummaryModel({policyId:9876543210})
                ], { policyNumber: '1234567890' }
            );

            var multiResultsOnePolicy = new PolicySummaryCollection(
                [
                    new PolicySummaryModel({policyId:singlePolicyId}),
                    new PolicySummaryModel({policyId:singlePolicyId})
                ], { policyNumber: '1234567890' }
            );

            var oneResult = new PolicySummaryCollection(
                [new PolicySummaryModel({policyId:singlePolicyId})],
                { policyNumber: '1234567890' }
            );

            it('Handles zero results', function () {
                policySearchModule._handlePolicyNumberSearchResponse(zeroResults, null, null);

                expect(messageStub).to.have.been.calledWith(
                    PolicySearchModule.prototype.errors.noPolicies, 'info'
                );
            });

            it('Handles one result', function () {
                policySearchModule._handlePolicyNumberSearchResponse(oneResult, null, null);
                expect(idFoundStub).to.have.been.calledWith(singlePolicyId);
            });

            it('Handles more than one result when all are for the same policyId', function () {
                policySearchModule._handlePolicyNumberSearchResponse(
                    multiResultsOnePolicy, null, null
                );
                expect(idFoundStub).to.have.been.calledWith(singlePolicyId);
            });

            it('Handles more than one result when all are NOT for the same policyId', function () {
                policySearchModule._handlePolicyNumberSearchResponse(
                    multiResultsMultiPolicies, null, null
                );

                expect(messageStub).to.have.been.calledWith(
                    PolicySearchModule.prototype.errors.noPolicies, 'info'
                );
            });
        });     // _handlePolicyNumberSearchResponse tests

        describe('performClientNameSearch method', function () {

            it('exists as a function', function () {
                expect(policySearchModule.performClientNameSearch).to.exist
                    .and.be.a('function');
            });

            it('creates a new ClientNameSearchView and passes it along with the ' +
                '"noClients" error message to _attachEventsToResultsView()', function () {

                var attachEventsSpy  = this.sinon.spy(policySearchModule,
                    '_attachEventsToResultsView');
                var expectedErrorMsg = PolicySearchModule.prototype.errors.noClients;

                policySearchModule.performClientNameSearch('smith, john');

                expect(attachEventsSpy).to.have.been.called;
                expect(attachEventsSpy.getCalls()[0].args[0]).is.an('Object');
                expect(attachEventsSpy.getCalls()[0].args[1]).equals(expectedErrorMsg);

                attachEventsSpy.restore();
            });
        });     // performClientNameSearch method

        describe('performProducerNameSearch method', function () {

            it('exists as a function', function () {
                expect(policySearchModule.performProducerNameSearch).to.exist
                    .and.be.a('function');
            });

            it('creates a new ProducerNameSearchView and passes it and the "noProducers" ' +
                'error message to _attachEventsToResultsView()', function () {

                var attachEventsSpy  = this.sinon.spy(policySearchModule,
                    '_attachEventsToResultsView');
                var expectedErrorMsg = PolicySearchModule.prototype.errors.noProducersNameSearch;

                policySearchModule.performProducerNameSearch('foo');

                expect(attachEventsSpy).to.have.been.called;
                expect(attachEventsSpy.getCalls()[0].args[0]).is.an('Object');
                expect(attachEventsSpy.getCalls()[0].args[1]).equals(expectedErrorMsg);

                attachEventsSpy.restore();
            });
        });     // performProducerNameSearch method


        describe('performProducerNumberSearch method', function () {

            it('exists as a function', function () {
                expect(policySearchModule.performProducerNumberSearch).to.exist
                    .and.be.a('function');
            });

            it('creates a new ProducerSearchView and passes it and the "noProducers" ' +
                'error message to _attachEventsToResultsView()', function () {

                var attachEventsSpy  = this.sinon.spy(policySearchModule,
                    '_attachEventsToResultsView');
                var expectedErrorMsg = PolicySearchModule.prototype
                .errors.noProducersNumberSearch;

                policySearchModule.performProducerNumberSearch('foo');

                expect(attachEventsSpy).to.have.been.called;
                expect(attachEventsSpy.getCalls()[0].args[0]).is.an('Object');
                expect(attachEventsSpy.getCalls()[0].args[1]).equals(expectedErrorMsg);

                attachEventsSpy.restore();
            });
        });     // performProducerNameSearch method
    });
});
    

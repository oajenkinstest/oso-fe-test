/* global expect:false, Backbone:false, $:false */

var ClientNameSearchResultsView = require(
    '../dist/pages/policy/views/client-name-search-results-v'
);

var ClientNameSearchResultsViewModel = require(
    '../dist/pages/policy/viewModels/client-name-search-results-vm'
);

describe('Client Name Search Results View ' +
    '(pages/policy/views/client-name-search-results-v.js)', function () {

    var data = {
        recordsTotal: 2,
        recordsFiltered: 2,
        dataSrc: 'policies',
        data: [
            {
                'customerLexicalName': 'Ford Prefect',
                'dateOfBirth': '1960-01-01',
                'relationship': 'Owner',
                'productName': 'Bistromatic Annuity',
                'receivedDate': '2016-01-11T00:00:00Z',
                'policyNumber': '720459750',
                'policyId': '8989898901',
                'caseId': '',
                'policyStatus' : {
                    'status'          : 'Pending',
                    'statusDetail'    : 'Paid',
                    'description'     : 'Paid',
                    'statusSortOrder' : 3
                }
            },
            {
                'customerLexicalName': 'Tricia McMillan',
                'dateOfBirth': '1970-05-05',
                'relationship': 'Owner',
                'productName': 'Heart of Gold Life',
                'receivedDate': '2016-02-11T00:00:00Z',
                'policyNumber': '',
                'policyId': '',
                'caseId': '1212121212',
                'policyStatus' : {
                    'status'          : 'Terminated',
                    'statusDetail'    : 'Lapse',
                    'description'     : 'Lapse',
                    'statusSortOrder' : 7
                }
            },
            {
                'customerLexicalName': 'James McMillan',
                'dateOfBirth': '1977-05-05',
                'relationship': 'Owner',
                'productName': 'Heart of Gold Life',
                'receivedDate': '2016-02-11T00:00:00Z',
                'policyNumber': '',
                'policyId': '',
                'caseId': '1212121212',
                'policyStatus': {
                    'acordHoldingStatus': 'Dormant',
                    'status': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'statusDetail': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'workflowStatus': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'statusSortOrder': 11,
                    'description': { 
                        'dataAvailability': 'notAvailable' 
                    }
                }
            }
        ]
    };

    it('exists', function() {
        expect(ClientNameSearchResultsView).to.exist;
    });

    describe('initialize method', function () {

        var userChannel;

        before(function() {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function() {
                return true;
            });
        });

        after(function() {
            userChannel.stopReplying();
            userChannel = null;
        });

        it('If view model provided, will not overwrite props', function () {
            var view = new ClientNameSearchResultsView({
                model: new ClientNameSearchResultsViewModel({
                    fakeProp: 'Monkey'
                })
            });

            expect(view.model.get('fakeProp')).to.equal('Monkey');
        });

        it('sets model value \'hasPolicySearchByProducer\' to false if the user does not ' +
                    'have the Policy_Search_by_Producer capability', function () {

            var userRadioRequestStub = this.sinon.stub(
                userChannel._requests.hasCapability, 'callback');
            userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(false);

            var view = new ClientNameSearchResultsView({
                searchTerm: 'Sam, Yosemite'
            });

            expect(view.model.get('hasPolicySearchByProducer')).to.equal(false);

            userRadioRequestStub.restore();
        });

        it('sets model value \'hasPolicySearchByProducer\' to true if the user does ' +
                    'have the Policy_Search_by_Producer capability', function () {

            var userRadioRequestStub = this.sinon.stub(
                userChannel._requests.hasCapability, 'callback');
            userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(true);

            var view = new ClientNameSearchResultsView({
                searchTerm: 'Sam, Yosemite'
            });

            expect(view.model.get('hasPolicySearchByProducer')).to.equal(true);

            userRadioRequestStub.restore();
        });

    }); // initialize method


    describe('uses DataTables for server calls', function () {

        var ajaxStub;
        var view;

        var userChannel;
        var userRadioRequestStub;

        beforeEach(function() {
            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('hasCapability', function () {
                return true;
            });
        });

        afterEach(function() {
            userChannel.stopReplying();
            userChannel = null;
        });

        describe('Tests with producer user', function () {
            var setStatusCellColorSpy;

            beforeEach(function () {
                userRadioRequestStub = this.sinon.stub(
                    userChannel._requests.hasCapability, 'callback');
                userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(false);

                setStatusCellColorSpy = this.sinon.spy(ClientNameSearchResultsView.prototype,
                        '_setStatusCellColor');

                ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                    'success',
                    data
                );

                view = new ClientNameSearchResultsView({
                    searchTerm: 'Sam, Yosemite'
                });

                view.render();
            });

            afterEach(function () {
                if (ajaxStub) {
                    ajaxStub.restore();
                }

                if (view) {
                    view.destroy();
                }

                if (userRadioRequestStub) {
                    userRadioRequestStub.restore();
                }

                setStatusCellColorSpy.restore();
            });

            it('Creates a datatable when rendered', function () {
                // Should make an ajax call to load data...
                expect(ajaxStub).to.have.been.called;

                var tableWrapper = view.$el.find('.dataTables_wrapper');
                expect(tableWrapper.length).to.equal(1);
            });

            it('should call "_setStatusCellColorSpy" for each row of data', function () {
                var expectedCount = data.data.length;
                expect(setStatusCellColorSpy.callCount).to.equal(expectedCount);
            });

            it('Injects markup into the DataTables structure', function () {
                var table = view.$el.find('.dataTables_wrapper table');

                // check that our code added a wrapper to the table element with proper class
                expect(table.parent().hasClass('table-responsive')).to.equal(true);

                // This would fail if our code did not wrap the table element
                expect(table.parent().hasClass('dataTables_wrapper')).to.equal(false);
            });

            it('Unhides the div containing the table', function() {
                expect(view.ui.clientResultsWrapper.hasClass('hidden')).to.equal(false);
            });

            describe ('status column data', function (){
            
                it('should empty if description has "dataAvailability" property and '+
                    'it values is "notAvailable"', function (){
                    expect(view.$el.find('table tr:eq(3) td:eq(6)').text().trim())
                        .to.be.empty;
                });

                it('should not empty if data exist for description field"', function (){
                    expect(view.$el.find('table tr:eq(2) td:eq(6)').text().trim())
                        .to.be.equal('Lapse');
                });
            });
        });

        describe.skip('Tests with non-200 result', function () {

            var listener;
            var errorStub;
            var server;

            beforeEach(function () {
                listener  = new Backbone.Marionette.Object();
                errorStub = this.sinon.stub();
                userRadioRequestStub = this.sinon.stub(
                    userChannel._requests.hasCapability, 'callback');
                userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(false);

                server = this.setupFakeServer(this.sinon, global, global.window);

                server.respondWith('GET', '', [
                    505,
                    { 'Content-Type' : 'application/json' },
                    JSON.stringify(data)
                ]);

                server.respondImmediately = true;

                view = new ClientNameSearchResultsView({
                    searchTerm: 'Sam, Yosemite'
                });

                // To disable local cache in test
                view.dataTableOptions = view._setDataTableOptons();
                view.dataTableOptions.ajax.localCache=false;

                listener.listenTo(view, 'error', errorStub);
                view.render();
                server.respond();
            });

            afterEach(function () {
                if (server) {
                    server.restore();
                }

                if (view) {
                    view.destroy();
                }

                if (userRadioRequestStub) {
                    userRadioRequestStub.restore();
                }

                listener.stopListening();
                listener.destroy();
            });

            it('does not display the table', function() {
                expect(view.ui.clientResultsWrapper.hasClass('hidden')).to.equal(true);
            });

            // for some reason stub is not calling
            it('triggers an "error" event', function() {
                // TODO - figure out why this is firing twice.
                // It sporadic, sometime once or twice
                expect(errorStub).to.have.been.called;
            });

        });

        describe.skip('Test with invalid response', function() {
            var noData = {
            };

            var listener;
            var errorStub;
            var server;

            beforeEach(function () {
                listener  = new Backbone.Marionette.Object();
                errorStub = this.sinon.stub();
                userRadioRequestStub = this.sinon.stub(
                    userChannel._requests.hasCapability, 'callback');
                userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(false);

                server = this.setupFakeServer(this.sinon, global, global.window);

                server.respondWith('Get', '', [
                    200,
                    { 'Content-Type' : 'application/json' },
                    JSON.stringify(noData)
                ]);

                view = new ClientNameSearchResultsView({
                    searchTerm: 'Sam, Yosemite'
                });

                // To disable local cache in test
                view.dataTableOptions = view._setDataTableOptons();
                view.dataTableOptions.ajax.localCache=false;

                listener.listenTo(view, 'error', errorStub);

                view.render();
                server.respond();
            });

            afterEach(function () {
                if (server) {
                    server.restore();
                }

                if (view) {
                    view.destroy();
                }

                if (userRadioRequestStub) {
                    userRadioRequestStub.restore();
                }

                listener.stopListening();
                listener.destroy();
            });

            it('does not display the table', function() {
                expect(view.ui.clientResultsWrapper.hasClass('hidden')).to.equal(true);
            });

            it('triggers a "noResults" event', function() {
                expect(errorStub).to.have.been.calledOnce;
            });
        });


        describe('Test with no results', function() {

            var noData = {
                recordsTotal: 0,
                recordsFiltered: 0,
                data: []
            };

            var server;

            beforeEach(function () {
                userRadioRequestStub = this.sinon.stub(
                    userChannel._requests.hasCapability, 'callback');
                userRadioRequestStub.withArgs('Policy_Search_by_Producer').returns(false);

                server = this.setupFakeServer(this.sinon, global, global.window);

                server.respondWith('GET', '', [
                    200,
                    { 'Content-Type' : 'application/json' },
                    JSON.stringify(noData)
                ]);

                view = new ClientNameSearchResultsView({
                    searchTerm: 'Sam, Yosemite'
                });

                // To disable local cache in test
                view.dataTableOptions = view._setDataTableOptons();
                view.dataTableOptions.ajax.localCache=false;

                view.render();
                server.respond();
            });

            afterEach(function () {
                if (server) {
                    server.restore();
                }

                if (view) {
                    view.destroy();
                }

                if (userRadioRequestStub) {
                    userRadioRequestStub.restore();
                }
            });

            it('does not display the table', function() {
                expect(view.ui.clientResultsWrapper.hasClass('hidden')).to.equal(true);
            });
        });

    });
});

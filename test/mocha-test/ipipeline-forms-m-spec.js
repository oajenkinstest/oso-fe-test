/* global expect:false */

var FormsModel = require('../dist/pages/home/models/ipipeline-forms-m');

describe('iPipeline Forms Model (pages/home/models/ipipeline-forms-m.js)', function () {

    describe('urlRoot property', function () {

        it('defaults to the correct value', function () {
            var expectedUrlRoot = '/api/oso/secure/rest/sso/ipipeline/formspipe';
            var model = new FormsModel();

            expect(model.urlRoot).to.equal(expectedUrlRoot);
        });
    }); // urlRoot

});

/* global expect:false, Backbone:false, $:false, sinon:false */

var RequirementSubmissionPageView =
    require('../dist/pages/requirementSubmission/views/requirement-submission-page-v');
var helpers = require('./helpers/helpers');

describe('Requirement Submission Page View ' +
    '(pages/requirementSubmission/views/requirement-submission-page-v.js)', function () {

    var checkRequiredBrowserFeaturesStub;
    var copyOfPolicyDetail;
    var rootView;
    var view;
    var ajaxStub;
    var viewShowFormStub;

    var setView = function () {
        var newview = new RequirementSubmissionPageView({
            stateObj : {
                policyId : 1234567890
            }
        });
        viewShowFormStub = this.sinon.stub(newview, '_showForm');
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        ajaxStub = this.sinon.stub($, 'ajax');
    });

    after(function () {
        ajaxStub.restore();
    });

    beforeEach(function () {
        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        copyOfPolicyDetail = $.extend(true, {}, helpers.policyData.pendingPolicyDetail);

        // Stub out the check for the browser requirements. If this isn't done, the
        // view won't render.
        checkRequiredBrowserFeaturesStub = this.sinon.stub(
            RequirementSubmissionPageView.prototype, '_checkRequiredBrowserFeatures');
        checkRequiredBrowserFeaturesStub.returns(true);
    });

    afterEach(function () {
        checkRequiredBrowserFeaturesStub.restore();
        copyOfPolicyDetail = null;
    });

    it('view exists', function () {
        expect(RequirementSubmissionPageView).to.exist;
    });

    it('should render view without any error', function() {
        view = setView();
        expect(view.isRendered).to.be.true;
        view.destroy();
    });

    // This block is actually testing for the presence of elements that are rendered in
    // requirement-submission-form-v.js, not this view. The tests should be moved there. After
    // the upgrade to jQuery 3, deep rendering of nested child views in unit tests (like this
    // test is counting on happening) didn't work anymore. -- RKC Jan 30, 2018
    describe.skip('Header section', function () {

        describe('Successfully displays the correct customer name and label based ' +
            'on the policy', function () {

            it('Label will be "Annuitant" and name will be first "Annuitant" role', function () {

                var expectedLabel = 'Annuitant:';
                var expectedName  = 'Johnny Socko and his Giant Robot';

                copyOfPolicyDetail.product.productTypeCategory = 'Annuity';

                ajaxStub.yieldsTo(
                    'success',
                    copyOfPolicyDetail
                );
                view = setView();

                var label = view.$el.find('#policy-primary-name').parent().text().trim();
                var name  = view.$el.find('#policy-primary-name').text();
                expect(label).to.contain(expectedLabel);
                expect(name).to.equal(expectedName);
                view.destroy();
            });

            it('Label will be "Insured" and name will be first "Primary Insured" role',
                function () {

                var expectedLabel = 'Insured:';
                var expectedName  = 'Johnny Socko and his Giant Robot';

                // delete the 'Annuitant' role
                delete copyOfPolicyDetail.customerRoles.Annuitant;

                ajaxStub.yieldsTo(
                    'success',
                    copyOfPolicyDetail
                );
                view = setView();

                var label = view.$el.find('#policy-primary-name').parent().text().trim();
                var name  = view.$el.find('#policy-primary-name').text();

                expect(label).to.contain(expectedLabel);
                expect(name).to.equal(expectedName);

                view.destroy();
            });

            it('If "Primary Insured" role does not exist for policy, ' + '' +
                '"Insured" role will be used', function () {

                var expectedLabel = 'Insured:';
                var expectedName  = 'Johnny Socko and his Giant Robot';

                // Copy the "Primary Insured" to "Insured" and delete "Primary Insured"
                copyOfPolicyDetail.customerRoles.Insured =
                    copyOfPolicyDetail.customerRoles['Primary Insured'];

                delete copyOfPolicyDetail.customerRoles['Primary Insured'];
                delete copyOfPolicyDetail.customerRoles.Annuitant;

                ajaxStub.yieldsTo(
                    'success',
                    copyOfPolicyDetail
                );
                view = setView();

                var label = view.$el.find('#policy-primary-name').parent().text().trim();
                var name  = view.$el.find('#policy-primary-name').text();

                expect(label).to.contain(expectedLabel);
                expect(name).to.equal(expectedName);

                view.destroy();
            });
        });

        it('should display the policy number', function () {
            var expectedNumber = copyOfPolicyDetail.policyNumber;

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            expect(view.$el.find('#policy-number').text().trim()).to.equal(expectedNumber);

            view.destroy();
        });

        it('should display the product name', function () {
            var expectedProduct = copyOfPolicyDetail.product.productName;

            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            expect(view.$el.find('#product .profile-info-value h5').text().trim())
                .to.equal(expectedProduct);

            view.destroy();
        });

    }); // Header section

    describe('Error handling', function () {

        describe('When retrieving Policy Detail information', function () {

            describe('404 Errors', function () {

                var showAlertMessageSpy;

                beforeEach(function () {
                    showAlertMessageSpy = this.sinon.spy(
                        RequirementSubmissionPageView.prototype, '_showAlertMessage'
                    );

                    view = setView();

                    // Trigger an error event to simulate a 404 error on the model fetch
                    view.model.policyDetailModel.trigger('error', view.model, { status: 404 });
                });

                afterEach(function () {
                    showAlertMessageSpy.restore();
                    view.destroy();
                });

                it('should call _showAlertMessage', function () {
                    expect(showAlertMessageSpy).to.have.been.called;
                });

                it('model should update alertMessage attribute', function () {
                    expect(view.model.has('alertMessage')).to.be.true;
                });

                it('should show correct error message', function () {
                    var expectedMessage =
                        RequirementSubmissionPageView.prototype.errors.policyNotFoundMessage;
                    var errorAlert = view.$el.find('.alert:contains("' + expectedMessage + '")');

                    expect(errorAlert.length).to.equal(1);
                });
            });     // 404 Errors

            describe('500 Errors', function () {

                var showAlertMessageSpy;

                beforeEach(function () {
                    showAlertMessageSpy = this.sinon.spy(
                        RequirementSubmissionPageView.prototype, '_showAlertMessage'
                    );

                    view = setView();

                    // Trigger an error event to simulate a 500 error on the model fetch
                    view.model.policyDetailModel.trigger('error', view.model, { status: 500 });
                });

                afterEach(function () {
                    showAlertMessageSpy.restore();
                    view.destroy();
                });

                it('should call showAlertMessage', function () {
                    expect(showAlertMessageSpy).to.have.been.called;
                });

                it('model should update alertMessage attribute', function () {
                    expect(view.model.has('alertMessage')).to.be.true;
                });

                it('should show correct error message', function () {
                    var expectedMessage =
                        RequirementSubmissionPageView.prototype.errors.serverError;
                    var errorAlert = view.$el
                        .find('.alert:contains("' + expectedMessage + '")');

                    expect(errorAlert.length).to.equal(1);
                });
            });     // 500 Errors

            it('should call showErrorPage if policy number is missing', function () {
                var errorHandlerSpy = this.sinon.spy();
                var errorChannel = Backbone.Radio.channel('error');

                errorChannel.on('showErrorPage', errorHandlerSpy);

                view = new RequirementSubmissionPageView();
                expect(errorHandlerSpy).to.have.been.called;

                errorChannel.off('showErrorPage', errorHandlerSpy);
            });

            it('should call waitIndicator module\'s "show" and "hide" ', function () {
                var spinnerShowSpy = this.sinon.spy();
                var spinnerChannel = Backbone.Radio.channel('spinner');

                spinnerChannel.on('show', spinnerShowSpy);

                view = setView();

                expect(spinnerShowSpy).to.have.been.called;

                spinnerChannel.off('show', spinnerShowSpy);
            });

        });     // When retrieving Policy Detail information

        describe('When the required browser features are not detected', function () {

            it('initialize() should return FALSE', function () {
                var result;

                checkRequiredBrowserFeaturesStub.returns(false);
                result = view.initialize();

                expect(result).to.be.false;
            });

        });     // When the required browser features are not detected

    });     // Error handling

    describe('_checkRequiredBrowserFeatures', function () {
        var browserFeatureDetectionChannel;
        var featureDetectionStub;
        var result;

        beforeEach(function () {
            view = setView();

            // Restore the stub for this method before each step
            checkRequiredBrowserFeaturesStub.restore();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should return true if browser features are detected', function () {
            browserFeatureDetectionChannel = Backbone.Radio.channel('browserFeature');
            featureDetectionStub           = this.sinon.stub().returns(true);

            browserFeatureDetectionChannel.reply('detect', featureDetectionStub);

            result = view._checkRequiredBrowserFeatures(['filereader']);

            expect(result).to.be.true;

            browserFeatureDetectionChannel.reset();
        });

        it('should return false if browser features are NOT detected', function () {

            browserFeatureDetectionChannel = Backbone.Radio.channel('browserFeature');
            featureDetectionStub           = this.sinon.stub().returns(false);

            browserFeatureDetectionChannel.reply('detect', featureDetectionStub);

            result = view._checkRequiredBrowserFeatures(['formdata']);

            expect(result).to.be.false;

            browserFeatureDetectionChannel.reset();
        });
    });

    // Stopped working after the upgrade to jQuery 3.
    describe.skip('_navigateToPolicyDetailPage method', function () {
        var cancelSubmitModal;
        var event;
        var triggerSpy;

        beforeEach(function () {
            ajaxStub.yieldsTo(
                'success',
                copyOfPolicyDetail
            );
            view = setView();

            cancelSubmitModal = view.$el.find('#cancel-submit-modal');

            event = {
                currentTarget : cancelSubmitModal
            };
            triggerSpy = this.sinon.spy(view, 'trigger');

            view._navigateToPolicyDetailPage(event);
        });

        afterEach(function () {
            triggerSpy.restore();
            view.destroy();
        });

        describe('When the data attribute "navigate" is NOT set on the modal', function () {

            it('should not trigger a "nav" event', function () {
                expect(triggerSpy).not.to.have.been.called;
            });

        });

        describe('When the data attribute "navigate" is set on the modal', function () {

            beforeEach(function () {
                cancelSubmitModal.data('navigate', true);

                view._navigateToPolicyDetailPage(event);
            });

            it('should trigger a "nav" event ', function () {
                expect(triggerSpy).to.have.been.calledOnce;
                expect(triggerSpy).to.have.been.calledWith('nav');
            });

            it('should trigger "nav" event with Policy Detail url', function () {
                var expectedUrl = '#pending-policy-detail?policyId=' + view.policyId;

                expect(triggerSpy).to.have.been.calledWith('nav', expectedUrl);
            });

        });     // When the data attribute "navigate" is set on the model

    });     // _navigateToPolicyDetailPage method

    describe('_resetModelsAndReloadForm method', function () {
        var resetModelsStub;
        var emptyRegionStub;

        beforeEach(function () {
            view = setView();

            var contentRegion = view.getRegion('contentRegion');

            resetModelsStub = this.sinon.stub(view, '_resetModels');
            emptyRegionStub = this.sinon.stub(contentRegion, 'empty');
            viewShowFormStub.reset(); // Defined in setView() above

            // Create a dummy event to send as a parameter to the function
            var clickEvt = global.document.createEvent('MouseEvents');
            clickEvt.initEvent('click', true, true);

            // Call the function directly rather than executing it as a side-effect of
            // a simulated mouse click
            view._resetModelsAndReloadForm(clickEvt);
        });

        afterEach(function () {
            if (resetModelsStub) {
                resetModelsStub.restore();
            }

            if (emptyRegionStub) {
                emptyRegionStub.restore();
            }

            view.destroy();
        });

        it('should exist as a function', function () {
            expect(view._resetModelsAndReloadForm).to.exist.and.be.a('function');
        });

        it('should call _resetModels() method', function () {
            expect(resetModelsStub).to.have.been.calledOnce;
        });

        it('should empty the region that contains the confirmation page', function () {
            expect(emptyRegionStub).to.have.been.calledOnce;
        });

        it('should call _showForm()', function () {
            expect(viewShowFormStub).to.have.been.calledOnce;
        });

    });     // _resetModelsAndReloadForm method

    describe('_resetModels method', function () {
        var domainModelClearSpy;
        var domainModelSetSpy;
        var modelResetStub;

        beforeEach(function () {
            view = setView();

            domainModelClearSpy = this.sinon.spy(view.model.domainModel, 'clear');
            domainModelSetSpy   = this.sinon.spy(view.model.domainModel, 'set');
            modelResetStub      = this.sinon.stub(view.model, 'reset');

            view._resetModels();
        });

        afterEach(function () {
            if (domainModelClearSpy) {
                domainModelClearSpy.restore();
            }

            if (domainModelSetSpy) {
                domainModelSetSpy.restore();
            }

            if (modelResetStub) {
                modelResetStub.restore();
            }

            view.destroy();
        });

        it('clear() should be called on the domain model and be passed the silent option',
            function () {
            expect(domainModelClearSpy).to.have.been.calledOnce;
            expect(domainModelClearSpy).to.have.been.calledWith({silent : true});
        });

        it('the domain model should be set with the defaults from the model', function () {
            // clear() calls set() behind the scenes, so we should expect
            // this to have already been called before we explicitly call it
            expect(domainModelSetSpy).to.have.been.calledThrice;
            expect(domainModelSetSpy).to.have.been.calledWith(view.model.defaults);
        });

        it('should call the reset() method on the model', function () {
            expect(modelResetStub).to.have.been.calledOnce;
        });
    });     // _resetModels method

    describe('_showAlertMessage method', function () {
        var hasAlertMessageAfter;
        var hasAlertMessageBefore;

        beforeEach(function () {
            view = setView();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should set "alertMessage" on model if response.status is a 404', function () {
            hasAlertMessageBefore = view.model.has('alertMessage');

            var responseObj = { status : 404 };
            view._showAlertMessage(view.model, responseObj);

            hasAlertMessageAfter = view.model.has('alertMessage');

            expect(hasAlertMessageBefore).to.be.false;
            expect(hasAlertMessageAfter).to.be.true;
        });

        it('should set "alertMessage" on model if response.status is a 500', function () {
            hasAlertMessageBefore = view.model.has('alertMessage');

            var responseObj = { status : 500 };
            view._showAlertMessage(view.model, responseObj);

            hasAlertMessageAfter = view.model.has('alertMessage');

            expect(hasAlertMessageBefore).to.be.false;
            expect(hasAlertMessageAfter).to.be.true;
        });

        it('should NOT set "alertMessage" on model if response.status ' +
            'is NOT 404 or 500', function () {
            hasAlertMessageBefore = view.model.has('alertMessage');

            var responseObj = { status : 400 };
            view._showAlertMessage(view.model, responseObj);

            hasAlertMessageAfter = view.model.has('alertMessage');

            expect(hasAlertMessageBefore).to.be.false;
            expect(hasAlertMessageAfter).to.be.false;
        });

        it('should NOT set "alertMessage" on model if response is not passed in', function () {
            hasAlertMessageBefore = view.model.has('alertMessage');

            view._showAlertMessage(view.model);

            hasAlertMessageAfter = view.model.has('alertMessage');

            expect(hasAlertMessageBefore).to.be.false;
            expect(hasAlertMessageAfter).to.be.false;
        });

    });     // _showAlertMessage method

    describe('_showConfirmation method', function () {
        var contentRegion;
        var emptyRegionStub;
        var scrollToSpy;
        var showRegionStub;

        beforeEach(function () {
            view = setView();

            contentRegion   = view.getRegion('contentRegion');
            emptyRegionStub = this.sinon.stub(contentRegion, 'empty');
            showRegionStub  = this.sinon.stub(contentRegion, 'show');
            scrollToSpy     = this.sinon.spy(view.utils, 'scrollTo');

            view._showConfirmation();
        });

        afterEach(function () {
            emptyRegionStub.restore();
            showRegionStub.restore();
            scrollToSpy.restore();

            view.destroy();
        });

        it('should call empty() on the region displaying the form', function () {
            expect(emptyRegionStub).to.have.been.calledOnce;
        });

        it('should show the region with the new confirmation page view', function () {
            expect(showRegionStub).to.have.been.calledOnce;
        });

        it('should call scrollTo() method to scroll to the top of the page', function () {
            expect(scrollToSpy).to.have.been.calledOnce;
            expect(scrollToSpy).to.have.been.calledWith(Backbone.$('body'));
        });

    });     // _showConfirmation method

    describe('_showForm method', function () {
        beforeEach(function () {
            view = setView();
            viewShowFormStub.restore();
        });

        afterEach(function () {
            view.destroy();
        });

        it('should call showChildView()', function () {
            var showChildViewStub = this.sinon.stub(view, 'showChildView');
            view._showForm();

            expect(showChildViewStub).to.have.been.calledOnce;

            showChildViewStub.restore();
        });

        it('should be called when policy detail data has been retrieved', function () {
            var showFormStub =
                this.sinon.stub(RequirementSubmissionPageView.prototype, '_showForm');
            view._showForm();

            expect(showFormStub).to.have.been.calledOnce;

            showFormStub.restore();
        });

    });     // _showForm method

});     // Requirement Submission Page View tests

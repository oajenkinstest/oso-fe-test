/* global expect:false */

var PolicySearchViewModel = require('../dist/pages/policy/viewModels/policy-search-form-vm');

describe('Policy Search Form ViewModel ' +
    '(pages/policy/viewModels/policy-search-form-vm.js)', function () {

    describe('Basic tests', function () {

        it('policy-search-form-vm exists', function() {
            expect(PolicySearchViewModel).to.exist;
        });

        it('policy-search-form-vm creates default properties', function() {
            var model = new PolicySearchViewModel();

            expect(model.has('searchTerm')).to.be.true;
            expect(model.has('searchTermErrorText')).to.be.true;
            expect(model.has('searchType')).to.be.true;
            expect(model.has('searchTypeErrorText')).to.be.true;

            // This one doesn't exist. Just make sure we're not getting false Trues above.
            expect(model.has('searchTermRequired')).to.be.false;

            model = null;
        });

        it('policy-search-form-vm default has default values', function() {
            var model = new PolicySearchViewModel();

            expect(model.get('searchTerm')).to.equal('');
            expect(model.get('searchTermErrorText')).to.equal('');
            expect(model.get('searchType')).to.equal('');
            expect(model.get('searchTypeErrorText')).to.equal('');

            expect(model.get('searchTermRequired')).to.be.undefined;

            model = null;
        });

        it('policy-search-form-vm trims leading/trailing whitespace in searchTerm on init',
            function() {
            var setValue      = ' MONKEY!   ';
            var expectedValue = 'MONKEY!';

            var model = new PolicySearchViewModel({
                searchTerm: setValue
            });

            expect(model.get('searchTerm')).to.equal(expectedValue);
        });

        it('policy-search-form-vm trims leading/trailing whitespace on change of the ' +
            '"searchTerm" property.', function() {

            var initValue     = 'asdfasdfasd';
            var setValue      = ' MONKEY!   ';
            var expectedValue = 'MONKEY!';

            var model = new PolicySearchViewModel({
                searchTerm: initValue
            });

            model.set('searchTerm', setValue);

            expect(model.get('searchTerm')).to.equal(expectedValue);
        });
    }); // Basic tests

    describe('Validation tests', function () {

        describe('When "Policy Number / Client Name" radio button is selected', function () {

            it('Has a validate function', function () {
                var model = new PolicySearchViewModel();
                expect(model.validate).to.exist;
                expect(model.validate).to.be.a('function');
                model = null;
            });

            it('The searchTerm property must be at least 1 character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '',
                    searchType : 'clientName'
                });
                var errors = model.validate();

                expect(errors.searchTermErrorText).to.equal(model.errors.termRequired);
                expect(errors.searchTypeErrorText).to.be.undefined;
            });

            it('The searchTerm is valid with at least one character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : 'a',
                    searchType : 'clientName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('The searchTerm can be a single special character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '#',
                    searchType : 'clientName'
                });
                var errors = model.validate();
                expect(errors).to.be.undefined;
            });

            it('The searchTerm property can contain whitespace', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : 'Lina Inverse',
                    searchType : 'clientName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('Special characters are totes legit in searchTerm', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '!@#%(){}[]\'"&a',
                    searchType : 'clientName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            describe('Policy Number must be numeric', function () {

                it('Policy Search is invalid with less than 9 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                    expect(errors.searchTypeErrorText).to.be.undefined;
                });

                it('Policy Search is invalid with more than 10 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '12345678901',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                    expect(errors.searchTypeErrorText).to.be.undefined;
                });

                it('Policy Search is valid with 9 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123123123',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                });

                it('Policy Search is valid with 10 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '1234567890',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                });

                it('Policy Search assumes client name search if there ' +
                    'is a non-digit somewhere', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '12345y6789',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                    expect(model.get('searchType')).to.equal('clientName');
                });
            });

            describe('Oddly, Policy Search can also end with a "C" or "c"', function () {
                it('Still invalid with less than 8 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123C',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                    expect(errors.searchTypeErrorText).to.be.undefined;
                });

                it('Is valid with 8 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '12345678C',
                        searchType: 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                });

                it('Is valid with 9 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123456789C',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                });

                it('Now invalid with 10 digits', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '1234567890C',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                    expect(errors.searchTypeErrorText).to.be.undefined;
                });

                it('Assumes client name search with a non-numeric character inside', function() {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123y23123C',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                    expect(model.get('searchType')).to.equal('clientName');
                });

                it('little-c is okay, too', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123456789c',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();

                    expect(errors).to.be.undefined;
                });
            });

            describe('Without type, validate infers the policy search', function () {

                it('For numeric input', function () {
                    var model = new PolicySearchViewModel({searchTerm : '123'});
                    var errors = model.validate();

                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                });

                it('For numeric, ending with "C"', function () {
                    var model = new PolicySearchViewModel({searchTerm : '123C'});
                    var errors = model.validate();
                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                });
            });

            describe('Without type, validate infers a name search if it is not policy search',
                function () {

                it('Plain old invalid name', function () {
                    var model = new PolicySearchViewModel({searchTerm : ',a a'});
                    var errors = model.validate();
                    expect(errors.searchTermErrorText)
                        .to.equal(model.errors.minimumInputLength);
                });

                it('numeric, but ends with little-c', function () {
                    var model = new PolicySearchViewModel({searchTerm : '123456789c'});
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });
            });

            describe('When using default value attribute for searchType', function () {
                var defaultValueAttribute = 'policyNumberClientName';
                var errors;
                var model;

                beforeEach(function () {
                    model = new PolicySearchViewModel({
                        searchTerm : '',
                        searchType : defaultValueAttribute
                    });
                });

                afterEach(function () {
                    model.destroy();
                    errors = null;
                });

                it('infers policy search for numeric input', function () {
                    model.set('searchTerm', '123');
                    errors = model.validate();

                    expect(model.get('searchType')).to.equal('policyNumber');
                    expect(errors.searchTermErrorText).to.equal(model.errors.policyNumberFormat);
                });

                it('infers name search if it is not a policy search', function () {
                    model.set('searchTerm', ',a a');
                    errors = model.validate();

                    expect(model.get('searchType')).to.equal('clientName');
                    expect(errors.searchTermErrorText).to.equal(model.errors.minimumInputLength);
                });
            });

            describe('Check type, if it is required', function () {

                it('No type, not required', function () {
                    // no type, but not required
                    var model = new PolicySearchViewModel({searchTerm : 'abc123'});
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('Required, but no option selected', function () {
                    var model = new PolicySearchViewModel({
                        hasPolicySearchByProducer : true,
                        searchTerm                : 'abc123',
                        searchOption              : ''
                    });
                    var errors = model.validate();
                    expect(errors.searchTypeErrorText).to.equal(model.errors.optionRequired);
                });

                // type set, required
                it('Required and set!', function () {
                    var model = new PolicySearchViewModel({
                        hasPolicySearchByProducer : true,
                        searchType                : 'policyNumber',
                        searchTerm                : '123456789'
                    });

                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('Type is set, but not required', function () {
                    var model = new PolicySearchViewModel({
                        searchType : 'policyNumber',
                        searchTerm : '123456789'
                    });

                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });
            });

            // validate validates both type and term
            describe('validate function validates searchTerm AND searchType', function () {

                it('Both values are invalid', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm                : '',
                        searchType                : '',
                        hasPolicySearchByProducer : true,
                    });
                    var errors = model.validate();
                    expect(errors.searchTermErrorText).to.equal(model.errors.termRequired);
                    expect(errors.searchTypeErrorText).to.equal(model.errors.typeRequired);
                });

                it('Both values are valid', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '1231231234',
                        searchType : 'policyNumber'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });
            });

            describe('"LastName, FirstName" format when the input includes a comma', function () {
                it('"last, first" valid, one space after comma', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'George, Curious',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('"last,first" valid, no spaces around comma', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'George,Curious',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('"last , first" valid, spaces on both sides of comma', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'George   , Curious',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('No first name after comma (forces last name search)', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'George,',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('No first name after comma, but extra spaces around comma', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'George   ,   ',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('Last name must be at least 1 character', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : ', a',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors.searchTermErrorText).to.equal(model.errors.minimumInputLength);
                });

                it('1-character Last name passes', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'b,a',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('Odd little robot names are okay in "last, first" format', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'D2, R2',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('special characters are valid for last name', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '--  ,  ',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('numbers are valid for last name', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : '123456789,',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors).to.be.undefined;
                });

                it('Multiple commas just do NOT make any sense', function () {
                    var model = new PolicySearchViewModel({
                        searchTerm : 'aa, , , ,',
                        searchType : 'clientName'
                    });
                    var errors = model.validate();
                    expect(errors.searchTermErrorText).to.equal(model.errors.nameFormat);
                });
            });

        });     // When "Policy Number / Client Name" radio button is selected

        describe('When "Producer Name" radio button is selected', function () {

            it('The searchTerm property must be at least 1 character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors.searchTermErrorText).to.equal(model.errors.termRequired);
                expect(errors.searchTypeErrorText).to.be.undefined;
            });

            it('The searchTerm is valid with at least one character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : 'a',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('The searchTerm can be a single special character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '#',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('The searchTerm property can contain whitespace', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : 'Lina Inverse',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('Special characters are totes legit in searchTerm', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '!@#%(){}[]\'"&a',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('"last, first" format isn\'t understood for "Producer Name" right now', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : 'Vader, Darth',
                    searchType : 'producerName'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });
        });

        describe('When "Producer Number" radio button is selected', function () {

            it('The searchTerm property must be at least 1 character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '',
                    searchType : 'producerNumber'
                });
                var errors = model.validate();

                expect(errors.searchTermErrorText).to.equal(model.errors.termRequired);
                expect(errors.searchTypeErrorText).to.be.undefined;
            });

            it('The searchTerm is valid with at least 5 alphanumeric character', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '1234a',
                    searchType : 'producerNumber'
                });
                var errors = model.validate();

                expect(errors).to.be.undefined;
            });

            it('minimum length should be 5', function () {
                var model = new PolicySearchViewModel({
                    searchTerm : '123',
                    searchType   : 'producerNumber',
                    searchOption : 'producerNumber'
                });
                var errors = model.validate();

                expect(errors.searchTermErrorText).to.equal(model.errors.producerNumberFormat);
            });


            it('Special characters are not allowed', function () {
                var model = new PolicySearchViewModel({
                    searchTerm   : '11222~!',
                    searchType   : 'producerNumber',
                    searchOption : 'producerNumber'
                });
                var errors = model.validate();

                expect(errors.searchTermErrorText).to.equal(model.errors.producerNumberFormat);
            });
        });

        it('validate function handles empty search term', function () {
            var model = new PolicySearchViewModel();
            var errors = model.validate();

            expect(errors.searchTermErrorText).to.equal(model.errors.termRequired);
        });
    }); // Validation tests
});

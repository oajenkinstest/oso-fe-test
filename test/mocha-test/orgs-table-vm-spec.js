/* global expect:false */

var Model = require('../dist/pages/policy/viewModels/orgs-table-vm');

describe('Organization Table ViewModel (pages/policy/viewModels/orgs-table-vm.js)', function () {

    it('exists', function() {
        expect(Model).to.exist;
    });

    it('has sensible defaults', function() {
        var model = new Model();

        expect(model.get('col')).to.equal(1);
        expect(model.get('dir')).to.equal('asc');
        expect(model.get('length')).to.equal(25);
        expect(model.get('producerId')).to.equal(null);
        expect(model.get('start')).to.equal(0);
    });

    it('Setting the producerId resets the start, col, and dir props', function () {
        var model = new Model({
            col   : 9,
            dir   : 'desc',
            start : 42
        });

        expect(model.get('col')).to.equal(9);
        expect(model.get('dir')).to.equal('desc');
        expect(model.get('start')).to.equal(42);

        model.set('producerId', 99);

        expect(model.get('col')).to.equal(0);
        expect(model.get('dir')).to.equal('asc');
        expect(model.get('producerId')).to.equal(99);
        expect(model.get('start')).to.equal(0);
    });

    it('Ensures that col, length, and start props are Number', function () {
        var stringVal = '25';
        var numVal    = 25;
        var model     = new Model({
            col    : stringVal,
            length : stringVal,
            start  : stringVal
        });

        // make sure this isn't doing type coercion!
        expect(stringVal).to.not.equal(numVal);

        // check the values
        expect(model.get('col')).to.equal(numVal);
        expect(model.get('length')).to.equal(numVal);
        expect(model.get('start')).to.equal(numVal);
    });

    it('When the attrs are already Number, everything is cool', function () {
        var numVal = 25;
        var model  = new Model({
            col    : numVal,
            length : numVal,
            start  : numVal
        });

        // check the values
        expect(model.get('col')).to.equal(numVal);
        expect(model.get('length')).to.equal(numVal);
        expect(model.get('start')).to.equal(numVal);
    });

    it('Uses defaults if the initial col, length, or start props are not Numeric', function () {
        var model = new Model({
            col    : 'monkey',
            length : 'giraffe',
            start  : 'hippopotamus'
        });

        expect(model.get('col')).to.equal(model.defaults.col);
        expect(model.get('length')).to.equal(model.defaults.length);
        expect(model.get('start')).to.equal(model.defaults.start);
    });
});

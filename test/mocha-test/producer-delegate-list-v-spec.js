/* global Backbone:false, expect:false */

var helpers = require('./helpers/helpers');

var ProducerDelegateListView = require(
    '../dist/pages/delegate-access/views/producer-delegate-list-v'
);

describe('Producer Delegate List View ' +
    '(pages/delegate-access/views/producer-delegate-list-v.js)', function() {

    var dataDelegateUser = {
        displayName : 'Delegate Multiple',
        webId : 'D12334',
        capabilities : [
            'OLS:DELEGATE'
        ],
        delegation : [
            {
                producer : {
                    webId : 'oscar',
                    fullName : 'Oscar T. Grouch',
                    lexicalName :'Grouch, Oscar T.',
                    roles : [
                        {
                            roleCode : 'DCC12345',
                            statusCode : 'A'
                        },

                        {
                            roleCode : 'ABC12345',
                            statusCode : 'A'
                        },

                         {
                            roleCode : '0BC12345',
                            statusCode : 'T'
                        }
                    ]
                }
            },
            {
                producer : {
                    webId : 'bookert',
                    fullName : 'Booker T. John',
                    lexicalName :'John, Booker T.',
                    roles : [
                        {
                            roleCode : 'BCSS12332',
                            statusCode : 'A'
                        }
                    ]
                }
            }
        ]
    };

    var view;

    it('exists', function() {
        expect(ProducerDelegateListView).to.exist;
        var producerDelegateListView = new ProducerDelegateListView();
        expect(producerDelegateListView).to.be.instanceof(Backbone.Marionette.View);
    });

    describe('onBeforeShow', function () {
        
        var rootView;
        var spinnerChannel;
        var spinnerShowSpy;
        var userChannel;
        var viewAsProducerSpy;

        beforeEach(function () {

            spinnerChannel = Backbone.Radio.channel('spinner');
            userChannel = Backbone.Radio.channel('user');

            spinnerShowSpy    =  this.sinon.spy();
            viewAsProducerSpy =  this.sinon.spy();

            userChannel.reply('getDelegateTargets', function(capability) {
                return dataDelegateUser.delegation;
            });

            userChannel.on('viewAsProducer', viewAsProducerSpy);
            spinnerChannel.on('show', spinnerShowSpy);

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            view = new ProducerDelegateListView();
            rootView.showChildView('contentRegion', view);
        });

        afterEach(function () {
            if (view) {
                view.destroy();
            }
            userChannel.off('viewAsProducer');
            spinnerChannel.off('show');

            userChannel = null;
        });

        it('displayed within datatables', function () {
            expect(view.$el.find('.dataTables_wrapper').length).to.equal(1);
        });

        it('table has three columns', function () {
            var numCols = view.$el.find('.dataTables_wrapper table tr:eq(0) th').length;

            expect(numCols).to.equal(3);
        });

        it('table displays a "Name" column', function () {
            var firstColumn = view.$el.find('.dataTables_wrapper tr:eq(0) th:eq(0)');

            expect(firstColumn.text()).to.equal('Name');
        });

        it('table displays an "Actions" column', function () {
            var secondColumn = view.$el.find('.dataTables_wrapper tr:eq(0) th:eq(1)');

            expect(secondColumn.text()).to.equal('Actions');
        });

        it('table row should match link text and it properties', function () {
            var actionLink = view.$el.find('.dataTables_wrapper tr:eq(1) td:eq(1) a');
            expect(actionLink.text()).to.be.contain('Start Delegate Access');
            expect(actionLink.data('webid')).to.be.equal('oscar');
            expect(actionLink.data('name')).to.be.equal('Oscar T. Grouch');
        });

        it('Action Link should call userChannel.viewAsProducer)  '+
                'and spinner.show channels', function () {
            
            var actionLink = view.$el.find('.dataTables_wrapper tr:eq(1) td:eq(1) a');
            
            actionLink.click();

            expect(viewAsProducerSpy).to.be.calledWith({
                webId    : actionLink.data('webid'),
                fullName : actionLink.data('name')
            });
            expect(spinnerShowSpy).to.be.called;

        });

        it('Action Link should not call userChannel.viewAsProducer)  '+
                'and spinner.show channels if webId missing', function () {
            
            var actionLink = view.$el.find('.dataTables_wrapper tr:eq(1) td:eq(1) a');

            //set webId blank
            actionLink.data('webid', '');
            
            actionLink.click();

            expect(viewAsProducerSpy).to.not.be.called;
            expect(spinnerShowSpy).to.not.be.called;
        });

        it('table displays "Producer Number" column', function () {
            var thirdColumn = view.$el.find('.dataTables_wrapper tr:eq(0) th:eq(2)');

            expect(thirdColumn.text()).to.equal('Producer Number');
        });

        it('Under "Producer Number" column roleCode should sort '+
                'by alphanumeric ', function () {
            var thirdColumn = view.$el.find('.dataTables_wrapper tr:eq(1) td:eq(2)');

            expect(thirdColumn.text()).to.equal('ABC12345, DCC12345');
        });

    });

    describe ('Error handling', function () {
        var userChannel;
        var rootView;
        var onRenderSpy;

        before(function () {

            userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getDelegateTargets', function(capability) {
                return [];
            });
            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
            onRenderSpy = this.sinon.spy(ProducerDelegateListView.prototype, 'render');
            view = new ProducerDelegateListView();

            rootView.showChildView('contentRegion', view);
        });

        after(function () {
            if (view) {
                view.destroy();
            }

            userChannel = null;
            onRenderSpy.restore();
        });

        it('Should errorHelper. if delegate list is empty', function () {
            expect(onRenderSpy).to.be.called;
        });

        it('Model should set with error alert message data', function () {
            expect(view.model.has('alertMessage')).to.be.true;
            expect(view.model.get('alertMessage').message)
                .to.be.contain(view.errors.noDelegateTargetUsers);
        });

    });

});

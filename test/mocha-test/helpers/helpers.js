/*
* Helper module to serve data or manipulate data for unit test cases
*/

module.exports = {
    pcData              : require('./performance-center-data'),
    policyData          : require('./policy-details-data'),
    policyListData   	: require('./policy-list-data'),
    viewHelpers         : require('./viewHelpers')
};

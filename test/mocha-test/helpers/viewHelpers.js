/* global Backbone:false, _:false */

/**
 * Helper for functions we seem to add to lots of different test specs.
 *
 */
var viewHelpers = {

    /**
     * Start Backbone History
     */
    startBBhistory : function startBBhistory() {
        if (!Backbone.History.started) {
            Backbone.history.start();
        }
    },

    /**
     * Stop Backbone History
     */
    stopBBhistory : function stopBBhistory() {
        if (Backbone.History.started) {
            Backbone.history.stop();
        }
    },

    /**
     * Create a "root" view to test subviews.
     * @returns {*}
     */
    createRootView : function createRootView() {
        // If the <body> is missing, some test must have destroyed it.
        if (Backbone.$('body').length === 0) {
            throw new Error('Some misbehaving test must have destroyed the <body>');
        }

        // Start over with a clean <body> with a #root div
        Backbone.$('body').off(); // remove all event handlers on the body
        Backbone.$('body').empty().append('<div id="root"></div>');

        return new Backbone.Marionette.LayoutView({
            // IMPORTANT: Do NOT set this el to 'body'. Doing so will delete the <body> from the
            // document if a test ever destroys this view (`rootView.destroy();`).
            el: '#root',
            // Some tests expect that there is a #contentView element in the DOM (as there is in
            // the actual app).
            template: _.template('<div id="contentView"></div>'),
            regions: {
                contentRegion: '#contentView'
            }
        });
    },

    /**
     * Removes duplicate (successive) whitespace characters
     * from a string and replaces them with a single
     * space. Useful when performing string
     * comparisons using strings from HTML content.
     *
     * Example:
     *
     *      "A string with     multiple spaces."
     *
     * would return:
     *
     *      "A string with multiple spaces."
     *
     * @param {string} value
     * @returns {string}
     */
    removeDuplicateWhitespace : function(value) {
        if (typeof value !== 'string') {
            return value;
        }

        return value.replace(/\s+/g, ' ');
    },

    /**
    * Generate a date in ISO format using current date.
    * @param  {[string]} daysActions Arithmetic action to days ('+' or '-')
    * @param  {[number]} number      Days to add
    * @return {string}             ISO formated date
    */
    getISODate : function getISODate (daysActions, number) {

        var currentDate;
        var date;
        var month;
        var year;

        if (!number) {
            number = 0;
        }

        currentDate  = new Date();
        date  = currentDate.getDate();

        if (daysActions && daysActions === '+') {
            currentDate.setDate(date + number);
        } else if (daysActions === '-') {
            currentDate.setDate(date - number);
        }

        date  = currentDate.getDate();
        date = date < 10 ? '0' + date : date;

        year  = currentDate.getFullYear();
        month = currentDate.getMonth()+1;
        month = month < 10 ? '0'+ month : month;

        return year+'-'+month+'-'+date+'T00:00:00Z';
    }
};

module.exports = {
    createRootView            : viewHelpers.createRootView,
    startBBhistory            : viewHelpers.startBBhistory,
    stopBBhistory             : viewHelpers.stopBBhistory,
    removeDuplicateWhitespace : viewHelpers.removeDuplicateWhitespace,
    getISODate                : viewHelpers.getISODate
};

//Pending Policy Detail Data
var policyData = {};

policyData.activePolicyDetail = {
    'policyId'         : '123457890',
    'caseId'           : '12341232132',
    'policyNumber'     : '720459750',
    'carrierCode'      : 'AUL',
    'issueAge'         : 'P45Y',
    'lastRefreshDate'  : '2016-02-11T14:30:00Z',
    'residentState'    : 'IN',
    'issueState'       : 'NC',
    'consolidatedPlan' : true,
    'policyStatus'     : {
        'acordHoldingStatus'    : 'Active',
        'statusDetail'          : 'Premium paying',
        'status'                : 'Issued',
        'workflowStatus'        : 'Paid',
        'description'           : 'Active',
        'date'                  : '2016-02-21T15:50:00Z'
    },

    'product' : {
        'productCategory'      : 'Care Solutions',
        'productName'          : 'Annuity Care',
        'productTypeCode'      : 'INXAN',
        'productCode'          : '005T20',
        'underwritingRequired' : true
    },

    'caseManager' : {
        'displayName'  : 'Barry Allen',
        'emailAddress' : 'barry.allen@oa.com'
    },

    'writingProducers' : [
        {
            'fullName' : 'Christopher E Ekstrom',
            'roleCode' : 'dg33442',
            'id'   : 12345
        }
    ],

    'servicingProducer' : {
        'fullName' : 'John M Stephen',
        'roleCode' : '24sds35',
        'id'   : 54321
    },

    'application' : {
        'issueState'      : 'NC',
        'underWriter'     : 'Barb Miskowic',
        'signed'          : '2016-02-10T18:50:00',
        'received'        : '2016-02-02T14:50:00',
        'entered'         : '2016-02-05T12:50:00',
        'uwDecision'      : null,
        'readyToIssue'    : null,
        'policySent'      : null,
        'paid'            : '2016-02-05T12:50:00',
        'applicationUrl'  : null,
        'illustrationUrl' : null
    },

    'billingDetail' : {
        'currentBilling'      : {
            'paymentMethod' : 'direct',
            'paymentMode'   : 'annual',
            'paymentAmount' : 'USD 51953.34'
        },
        'cashWithApplication' : 'USD 51953.34',
        'totalDepositsToDate' : 'USD 84572.22'
    },

    'coverage'                 : {
        'base'           : [
            {
                'sequence'               : 1,
                'customerId'             : '123',
                'name'                   : 'WL Legacy 121',
                'planName'               : 'WL Legacy 121',
                'faceAmount'             : 'USD 1.00',
                'monthlyBenefit'         : 'USD 0.00',
                'annualPremium'          : 'USD 1212.23',
                'deathBenefitOptionName' : 'Face Amount Plus Cash Value',
                'deathBenefitAmount'     : 'USD 55000.00'
            },
            {
                'sequence'       : 2,
                'customerId'     : '456',
                'name'           : 'Subaru Legacy',
                'planName'       : 'WL Legacy 121',
                'faceAmount'     : 'USD 2.00',
                'monthlyBenefit' : 'USD 9.00',
                'annualPremium'  : 'USD 2323.12',
                'deathBenefitOptionName' : 'Face Amount Plus Cash Value',
                'deathBenefitAmount'     : 'USD 55000.00'
            }
        ],
        'coverageDetail' : {
            'ltcCoverageDetail' : {
                'accumulatedValueLtcFund'                 : 'USD 178345.32',
                'accumulatedValueLtcInterestRateCurrent'  : 1.3,
                'benefitPeriod'                           : 12,
                'ltcCurrentBenefitBalance'                : 'USD 165000.00',
                'ltcTotalWithdrawnAmount'                 : 'USD 12000.00',
                'monthlyBenefitAmount'                    : 'USD 3800.00'
            }
        },
        'riders'         : [
            {
                'sequence'       : 3,
                'customerId'     : '123',
                'name'           : 'Accelerator Paid Up Additions Rirder',
                'planName'       : 'Accelerator Paid Up Additions Rirder',
                'faceAmount'     : 'USD 1.00',
                'monthlyBenefit' : 'USD 21212.56',
                'annualPremium'  : 'USD 1212.23',
                'accumulatedValueLtcFund' : 'USD 5000.00',
                'accumulatedValueLtcInterestRateCurrent' : 0.04,
                'monthlyBenefitAmount' : 'USD 700.00',
                'benefitPeriod' : 'P36M',
                'ltcCurrentBenefitBalance' : 'USD 50000.00',
                'ltcTotalWithdrawnAmount' : 'USD 6000.00'
            },
            {
                'sequence'       : 4,
                'customerId'     : '456',
                'name'           : 'Chronic Illness Rider',
                'planName'       : 'Accelerator Paid Up Additions Rirder',
                'faceAmount'     : 'USD 1.00',
                'monthlyBenefit' : 'USD 0.00',
                'annualPremium'  : 'USD 0.00',
                'modalPremium'   : 'USD 20.00'
            }
        ]
    },
    'requirementSubmissionLog' : [
        {
            'submitTimestamp' : '2017-05-13T12:22:13.000',
            'fileCount'       : 1
        },
        {
            'submitTimestamp' : '2017-05-16T09:12:53.000',
            'fileCount'       : 2
        }
    ],
    'requirements'             : {
        'neededForUnderwriting' : [
            {
                'customerId'      : '123',
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Pending',
                'dateCreated'     : '2016-02-05T12:50:00Z',
                'dateReceived'    : null,
                'neededToPay'     : null,
                'comments'        : 'Please provide unsigned illustration prior to '+
                                    'issue due to EBIR.',
                'new'             : true
            }
        ],
        'obtainAtDelivery'      : [
            {
                'customerId'      : '456',
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Pending',
                'dateCreated'     : '2016-03-15T12:50:00Z',
                'dateReceived'    : null,
                'neededToPay'     : true,
                'comments'        : 'Collect on delivery.',
                'new'             : false
            }
        ],
        'received'              : [
            {
                'customerId'      : '456',
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Waived',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-25T12:50:00Z',
                'neededToPay'     : null,
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            },
            {
                'customerId'      : '123',
                'requirementName' : 'Some requirements name 123',
                'status'          : 'In House',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-15T12:50:00Z',
                'neededToPay'     : null,
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            },
            {
                'customerId'      : '456',
                'requirementName' : 'Some requirements name for 456',
                'status'          : 'Complete',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-05T12:50:00Z',
                'neededToPay'     : null,
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            }
        ]
    },

    'relatedPolicies' : [
        {
            'policyNumber'  : '9558845545',
            'policyId'      : 454755522,
            'productName'   : 'Asset-Care IV',
            'modalPremium'  : 'USD 24555.00',
            'billingMode'   : 'Annual',
            'billingMethod' : 'Direct'
        },
        {
            'policyNumber'  : '9558845784',
            'policyId'      : 564555222,
            'productName'   : 'Continuation of Benefit Rider',
            'modalPremium'  : 'USD 24555.00',
            'billingMethod' : 'Direct'
        }
    ],

    'exchangeHistory' : [
        {
            'otherCarrierName'         : 'A USAA',
            'otherCarrierPolicyNumber' : '30900405850',
            'dateReceived'             : '2016-06-14',
            'amountReceived'           : 'USD 20000.00',
            'comments'                 : [
                {
                    'date'    : '2016-06-02T14:22:37Z',
                    'comment' : 'FAXED PACKET TO 8774357099'
                },
                {
                    'date'    : '2016-06-06T17:25:00Z',
                    'comment' : 'Spoke with Melissa. A check was mailed on 06/03/2016' +
                    'in the amount of  $26,742.28.'
                },
                {
                    'date'    : '2016-06-14T23:42:11Z',
                    'comment' : 'Direct transfer. No cost basis required.'
                }
            ]
        },
        {
            'otherCarrierName'         : 'USAA Prudential',
            'otherCarrierPolicyNumber' : '40902595727',
            'dateReceived'             : '2016-06-14',
            'amountReceived'           : 'USD 14355.76',
            'comments'                 : [
                {
                    'date'    : '2016-06-02T15:55:00Z',
                    'comment' : 'FAXED PACKET TO 4544775454'
                },
                {
                    'date'    : '2016-06-06T00:17:22Z',
                    'comment' : 'Spoke with Jasmin. A check was mailed on 06/03/2016 ' +
                    'in the amount of  $60,327.84.'
                },
                {
                    'date'    : '2016-06-14T16:54:00Z',
                    'comment' : 'Direct transfer. Cost basis required.'
                }
            ]
        },
        {
            'otherCarrierName'         : 'AB USAA',
            'otherCarrierPolicyNumber' : '30900405860',
            'dateReceived'             : '2016-06-14',
            'amountReceived'           : 'USD 0.00',
            'comments'                 : [
                {
                    'date'    : '2016-06-02T17:17:17Z',
                    'comment' : 'FAXED PACKET TO 8774357099'
                },
                {
                    'date'    : '2016-06-06T09:33:42Z',
                    'comment' : 'Spoke with Melissa. A check was mailed on 06/03/2016 ' +
                    'in the amount of  $26,742.28.'
                },
                {
                    'date'    : '2016-06-14T06:45:09Z',
                    'comment' : 'Direct transfer. No cost basis required.'
                }
            ]
        }
    ],

    'loan' : {
        'loanBalance'              : 'USD 1000.00',
        'loanInterestRate'         : 0.06,
        'loanInterestTiming'       : 'Arrears',
        'loanedAmountInterestRate' : 0.04,
        'maximumAvailableLoan'     : 'USD 10000.00',
        'netInterestRate'          : 0.02,
        'payoffBalance'            : 'USD 1010.00'
    },

    'policyValue' : {
        'accountValue'                                 : 'USD 167741.38',
        'accumValueInterestRateCurrent'                : 2.4,
        'asOfDate'                                     : '2017-12-06',
        'cashValueAmount'                              : 'USD 12345.00',
        'cashValueOfPaidUpAdditions'                   : 'USD 222.00',
        'cashWithdrawalFromDate'                       : '2017-12-06',
        'cashWithdrawalToDate'                         : '2018-12-06',
        'cashWithdrawals'                              : 'USD 15000.00',
        'costBasis'                                    : 'USD 164000.00',
        'currentInterestRate'                          : 1.2,
        'dividendLastAnniversary'                      : 'USD 12400.00',
        'dividendOnDeposit'                            : 'USD 5468.43',
        'guaranteedAnnualWithdrawalAmount'             : 'USD 10000.00',
        'guaranteedAnnualWithdrawalAmountTONextAnniv'  : 'USD 5000.00',
        'guaranteedCashValueAmount'                    : 'USD 156432.00',
        'guaranteedIncomeRiderBenefitBase'             : 'USD 6543.21',
        'guaranteedIncomeRiderIncomeRate'              : 3.4,
        'guaranteedIncomeRiderIndexMultiplier'         : 1.7,
        'guaranteedIncomeRiderLifetimeAnnualIncome'    : 'USD 56742.21',
        'guaranteedInterestRate'                       : 1.4,
        'guaranteedMinimumWithdrawalBenefitBaseAmount' : 'USD 16665.00',
        'netSurrenderValueAmount'                      : 'USD 7654.32',
        'penaltyFreeCashWithdrawalAmount'              : 'USD 10000.00',
        'premiumDepositFundAmount'                     : 'USD 15000.00',
        'remainingCashFreeOutAmount'                   : 'USD 5000.00',
        'totalCashAmountWithdrawn'                     : 'USD 2000.00'
    },

    'taxQualification' : {
        'description' : 'Individual Retirement Account',
        'type'        : 'Qualified'
    },

    'payout' : {
        'incomeOption'    : null,
        'payoutStartDate' : '2010-03-26'
    },

    'customers'            : {
        '123' : {
            'taxId'         : '2939',
            'taxIdType'     : 'SSN',
            'firstName'     : 'Joe',
            'lastName'      : 'Thomson',
            'fullName'      : 'Joe The Policy holder',
            'issueAge'      : 44,
            'dateOfBirth'   : '2016-02-21T15:50:00Z',
            'gender'        : 'F',
            'maritalStatus' : null
        },
        '456' : {
            'taxId'         : '2939',
            'taxIdType'     : 'SSN',
            'firstName'     : 'Johnny',
            'lastName'      : 'Maxwell',
            'fullName'      : 'Johnny Socko and his Giant Robot',
            'issueAge'      : 44,
            'dateOfBirth'   : '2016-02-21T15:50:00Z',
            'gender'        : 'F',
            'maritalStatus' : null
        }
    },
    'customerRoleContacts' : {
        '1001' : {
            'emailAddress' : null,
            'post'         : {
                'type'  : 'MAIL',
                'line'  : [
                    '8205 test Drive EAST'
                ],
                'city'  : 'Test city',
                'state' : 'WA',
                'zip'   : '12345'
            },
            'phoneNumber'  : {
                'home' : null,
                'work' : null
            }
        },
        '1002' : {
            'emailAddress' : null,
            'post'         : {
                'type'  : 'MAIL',
                'line'  : [
                    '1405 test Drive EAST'
                ],
                'city'  : 'NY city',
                'state' : 'NY',
                'zip'   : '40215'
            },
            'phoneNumber'  : {
                'home' : null,
                'work' : null
            }
        }
    },
    'customerRoles'        : {
        'Owner'           : [{
            'customerId'            : 123,
            'customerRoleContactId' : 1001
        }],
        'Primary Insured' : [{
            'customerId'            : 456,
            'customerRoleContactId' : 1002
        }],
        'Annuitant'       : [{
            'customerId'            : 456,
            'customerRoleContactId' : 1002
        }],
        'Beneficiary'     : [{
            'customerId'            : 123,
            'customerRoleContactId' : 1001
        }],
        'Assignee'        : [{
            'customerId'            : 456,
            'customerRoleContactId' : 1002
        }],
        'Employer'        : [{
            'customerId'            : 123,
            'customerRoleContactId' : 1001
        }],

        'Primary Beneficiary' : [{
            'customerId'            : 456,
            'customerRoleContactId' : 1002
        }]
    },

    'investmentAllocation' : [
        {
            'productFullName'       : 'Point to Point Cap',
            'allocationPercent'     : 89.93000030517578,
            'allocationAmount' : 'USD 30000.00',
            'scheduledAllocationPercent' : 20.00,
            'investmentRate' : 2.75,
            'rateType' : 'Cap Rate',
            'indexPeriodNextStartDate' : '2018-11-15',
            'indexPeriodCurrentEndDate' : '2018-11-14'
        },
        {
            'productFullName'       : 'Point to Point Participation Rate',
            'allocationPercent'     : 0,
            'allocationAmount' : 'USD 30000.00',
            'scheduledAllocationPercent' : 20.00,
            'investmentRate' : 2.75,
            'rateType' : 'Cap Rate',
            'indexPeriodNextStartDate' : '2018-11-15',
            'indexPeriodCurrentEndDate' : '2018-11-14'
        }
    ]
};

policyData.pendingPolicyDetail = {
    '_links': {
        'application-document': { 'href': '../../../web/policies/123457890/applications/latest' },
        'illustration-document': { 'href': '../../../web/policies/123457890/illustrations/latest' }
     },
    'policyId' : '123457890',
    'caseId': '12341232132',
    'policyNumber': '720459750',
    'carrierCode': 'AUL',
    'issueAge': 'P45Y',
    'lastRefreshDate': '2016-02-11T14:30:00Z',
    'residentState': 'IN',
    'issueState': 'NC',
    'consolidatedPlan' : true,
    'policyStatus': {
        'acordHoldingStatus' : 'Proposed',
        'status': 'Pending',
        'workflowStatus': 'In Underwriting',
        'description': 'In Underwriting',
        'date': '2016-02-21T15:50:00Z'
    },

    'product': {
        'productCategory': 'Care Solutions',
        'productName': 'Liberty Select',
        'productTypeCode': 'SPFA',
        'productCode': '005T20',
        'underwritingRequired': true
    },

    'caseManager': {
        'displayName': 'Barry Allen',
        'emailAddress': 'barry.allen@oa.com'
    },

    'writingProducers': [
        {
            'fullName': 'John Smith',
            'roleCode': 'js8828',
            'id': 12345,
            'splitPercent' : 0.3
        },
        {
            'fullName': 'Christopher E Ekstrom',
            'roleCode': 'dg33442',
            'id': 54555,
            'splitPercent' : 0.7
        }
    ],

    'servicingProducer': {
        'fullName': 'John M Stephen',
        'roleCode': '24sds35',
        'id': 54321
    },

    'application': {
        'issueState': 'NC',
        'underWriter': 'Barb Miskowic',
        'signed': '2016-02-10T18:50:00',
        'received': '2016-02-02T14:50:00',
        'entered': '2016-02-05T12:50:00',
        'uwDecision': null,
        'readyToIssue': null,
        'policySent': null,
        'paid': null,
        'applicationUrl': null,
        'illustrationUrl': null
    },

    'billingDetail' : {
        'currentBilling' : {
            'paymentMethod' : 'direct',
            'paymentMode'   : 'annual',
            'paymentAmount' : 'USD 51953.34'
        },
        'bankDraft' : {
            'controlNumber' : '12345',
            'paymentDraftDay' : 23
        },
        'billedToDate' : '2017-10-15',
        'paidToDate' : '2016-10-15',
        'lastPremiumDate' : '2016-10-15',
        'cashWithApplication' : 'USD 51953.34',
        'premiumSchedule'     : 'Payable to Year 10',
        'totalDepositsToDate' : 'USD 1200.00',
        'alternatePremiumModes' : [
            {
                'paymentMethod' : 'Direct',
                'paymentMode' : 'Annual',
                'paymentAmount' : 'USD 200.00'
            }, {
                'paymentMethod' : 'Direct',
                'paymentMode' : 'Monthly',
                'paymentAmount' : 'USD 12.00'
            }, {
                'paymentMethod' : 'Direct',
                'paymentMode' : 'Semiannual',
                'paymentAmount' : 'USD 100.00'
            }
        ]
    },

    'coverage': {
        'base': [
            {
                'sequence': 1,
                'customerId': '123',
                'name': 'WL Legacy 121',
                'planName': 'WL Legacy 121',
                'faceAmount': 'USD 1.00',
                'monthlyBenefit': 'USD 0.00',
                'annualPremium': 'USD 1212.23'
            },
            {
                'sequence': 2,
                'customerId': '456',
                'name': 'Subaru Legacy',
                'planName': 'Subaru Legacy',
                'faceAmount': 'USD 2.00',
                'monthlyBenefit': 'USD 9.00',
                'annualPremium': 'USD 2323.12'
            }
        ],
        'riders': [
            {
                'sequence': 3,
                'customerId': '123',
                'name': 'Accelerator Paid Up Additions Rirder',
                'planName': 'Accelerator Paid Up Additions Rirder',
                'faceAmount': 'USD 1.00',
                'monthlyBenefit': 'USD 21212.56',
                'annualPremium': 'USD 1212.23'
            },
            {
                'sequence': 4,
                'customerId': '456',
                'name': 'Chronic Illness Rider',
                'planName': 'Chronic Illness Rider',
                'faceAmount': 'USD 1.00',
                'monthlyBenefit': 'USD 0.00',
                'annualPremium': 'USD 0.00'
            }
        ]
    },
    'requirementSubmissionLog' : [
        {
            'submitTimestamp' : '2017-05-13T12:22:13.000',
            'fileCount' : 1,
            'comment'   : 'Some comment will be displayed here'
        }, 
        {
            'submitTimestamp' : '2017-05-16T09:12:53.000',
            'fileCount' : 2
        },
        {
            'submitTimestamp' : '2017-05-16T09:12:53.000',
            'comment'   : 'Some comment will be displayed here'
        }
    ],
    'requirements' : {
        'neededForUnderwriting' : [
            {
                'customerId'      : '123', 
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Pending',
                'dateCreated'     : '2016-02-05T12:50:00Z',
                'dateReceived'    : null,
                'neededToPay'     : null,
                'comments'        : 'Please provide unsigned illustration prior to issue due to EBIR.',
                'new'             : true
            }
        ],
        'obtainAtDelivery' : [
            {
                'customerId'      : '456',
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Pending',
                'dateCreated'     : '2016-03-15T12:50:00Z',
                'dateReceived'    : null, 
                'neededToPay'     : true, 
                'comments'        : 'Collect on delivery.',
                'new'             : false
            }
        ],
        'received' : [
            {
                'customerId'      : '456',
                'requirementName' : 'Additional Form Required to Mail Policy',
                'status'          : 'Waived',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-25T12:50:00Z', 
                'neededToPay'     : null, 
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            },
            {
                'customerId'      : '123',
                'requirementName' : 'Some requirements name 123',
                'status'          : 'In House',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-15T12:50:00Z', 
                'neededToPay'     : null, 
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            },
            {
                'customerId'      : '456',
                'requirementName' : 'Some requirements name for 456',
                'status'          : 'Complete',
                'dateCreated'     : '2016-02-10T12:50:00Z',
                'dateReceived'    : '2016-03-05T12:50:00Z', 
                'neededToPay'     : null, 
                'comments'        : 'I-20833 (ICC)',
                'new'             : false
            }
        ]
    },

    'relatedPolicies' : [
        {
            'policyNumber'      : '9558845545',
            'policyId'          : 454755522,
            'productName'       : 'Asset-Care IV',
            'modalPremium'      : 'USD 24555.00',
            'billingMode'       : 'Annual',
            'billingMethod'     : 'Direct'
        },
        {
            'policyNumber'      : '9558845784',
            'policyId'          : 564555222,
            'productName'       : 'Continuation of Benefit Rider',
            'modalPremium'      : 'USD 24555.00',
            'billingMethod'     : 'Direct'
        }
    ],

    'exchangeHistory': [
        {
            'otherCarrierName': 'A USAA',
            'otherCarrierPolicyNumber': '30900405850',
            'dateReceived': '2016-06-14',
            'amountReceived': 'USD 20000.00',
            'comments': [
                {
                    'date': '2016-06-02T14:22:37Z',
                    'comment': 'FAXED PACKET TO 8774357099'
                },
                {
                    'date': '2016-06-06T17:25:00Z',
                    'comment': 'Spoke with Melissa. A check was mailed on 06/03/2016' +
                    'in the amount of  $26,742.28.'
                },
                {
                    'date': '2016-06-14T23:42:11Z',
                    'comment': 'Direct transfer. No cost basis required.'
                }
            ]
        },
        {
            'otherCarrierName': 'USAA Prudential',
            'otherCarrierPolicyNumber': '40902595727',
            'dateReceived': '2016-06-14',
            'amountReceived': 'USD 14355.76',
            'comments': [
                {
                    'date': '2016-06-02T15:55:00Z',
                    'comment': 'FAXED PACKET TO 4544775454'
                },
                {
                    'date': '2016-06-06T00:17:22Z',
                    'comment': 'Spoke with Jasmin. A check was mailed on 06/03/2016 '+
                    'in the amount of  $60,327.84.'
                },
                {
                    'date': '2016-06-14T16:54:00Z',
                    'comment': 'Direct transfer. Cost basis required.'
                }
            ]
        },
        {
            'otherCarrierName': 'AB USAA',
            'otherCarrierPolicyNumber': '30900405860',
            'dateReceived': '2016-06-14',
            'amountReceived': 'USD 0.00',
            'comments': [
                {
                    'date': '2016-06-02T17:17:17Z',
                    'comment': 'FAXED PACKET TO 8774357099'
                },
                {
                    'date': '2016-06-06T09:33:42Z',
                    'comment': 'Spoke with Melissa. A check was mailed on 06/03/2016 '+ 
                    'in the amount of  $26,742.28.'
                },
                {
                    'date': '2016-06-14T06:45:09Z',
                    'comment': 'Direct transfer. No cost basis required.'
                }
            ]
        }
    ],

    'taxQualification': {
        'description': 'Individual Retirement Account',
        'type': 'Qualified'
    },

    'payout' : {
        'incomeOption'    : null,
        'payoutStartDate' : '2010-03-26',
        'selectedPayoutOption' : {
            'payoutMode' : 'Monthly',
            'payoutAmount' : 'USD 100.00'
        },
        'availablePayoutOptions' : [
            {
                'payoutMode' : 'Monthly',
                'payoutAmount' : 'USD 100.00'
            }, {
                'payoutMode' : 'Quarterly',
                'payoutAmount' : 'USD 300.00'
            }, {
                'payoutMode' : 'Semiannual',
                'payoutAmount' : 'USD 600.00'
            }, {
                'payoutMode' : 'Annual',
                'payoutAmount' : 'USD 1200.00'
            }
        ],
        'nextIndexDate' : '2017-12-15'
    },

    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'SSN',
            'firstName': 'Joe',
            'lastName' : 'Thomson',
            'fullName': 'Joe The Policy holder',
            'issueAge': 44,
            'dateOfBirth': '2016-02-21T15:50:00Z',
            'gender': 'F',
            'maritalStatus': null
        },
        '456': {
            'taxId': '2939',
            'taxIdType': 'SSN',
            'firstName': 'Johnny',
            'lastName' : 'Maxwell',
            'fullName': 'Johnny Socko and his Giant Robot',
            'issueAge': 44,
            'dateOfBirth': '2016-02-21T15:50:00Z',
            'gender': 'F',
            'maritalStatus': null
        }
    },
    'customerRoleContacts' : {
        '1001' : {
            'emailAddress': null,
            'post': {
                'type': 'MAIL',
                'line': [
                    '8205 test Drive EAST'
                ],
                'city': 'Test city',
                'state': 'WA',
                'zip': '12345'
            },
            'phoneNumber': {
                'home': null,
                'work': null
            }
        },
        '1002' : {
            'emailAddress': null,
            'post': {
                'type': 'MAIL',
                'line': [
                    '1405 test Drive EAST'
                ],
                'city': 'NY city',
                'state': 'NY',
                'zip': '40215'
            },
            'phoneNumber': {
                'home': null,
                'work': null
            }
        }
    },
    'customerRoles': {
        'Owner': [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],
        'Primary Insured' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }],
        'Annuitant' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }],
        'Beneficiary' : [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],
        'Assignee' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }],
        'Employer' : [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],

        'Primary Beneficiary':[{
            'customerId' : 456,
            'customerRoleContactId':1002
        }]
    },

    'investmentAllocation': [
        {
            'productFullName'   : 'Point to Point Cap',
            'allocationPercent' : 89.93000030517578,
            'allocationAmount' : 'USD 30000.00',
            'scheduledAllocationPercent' : 20.00
        },
        {
            'productFullName'   : 'Point to Point Participation Rate',
            'allocationPercent' : 0,
            'allocationAmount' : 'USD 30000.00',
            'scheduledAllocationPercent' : 20.00
        }
    ]
};

policyData.pendingPolicyDetailWithTaxID = {
    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'SSN',
        },
        '456': {
            'taxId': '2939',
            'taxIdType': 'EIN',
        }
    },
    
    'customerRoles': {
        'Owner': [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],
        'Annuitant' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }]
    }
};

policyData.pendingPolicyDetailWithOnlyTaxID = {
    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'EIN',
        },
        '456': {
            'taxId': '2939',
            'taxIdType': 'EIN',
        }
    },
    
    'customerRoles': {
        'Owner': [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],
        'Annuitant' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }]
    }
};

policyData.pendingPolicyDetailWithTaxIDNone = {
    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'NONE',
        },
         '124': {
            'taxId': '',
            'taxIdType': '',
        },
        '456': {
            'taxId': '2939',
            'taxIdType': 'EIN',
        }
    },
    
    'customerRoles': {
        'Owner': [
            {
                'customerId' : 123,
                'customerRoleContactId':1001
            },
            {
                'customerId' : 124,
                'customerRoleContactId':1003
            }
        ],
        'Annuitant' : [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }]
    }
};

policyData.pendingPolicyDetailAWDRip = {
    'policyId' : '',
    'caseId': '12341232132',
    'policyNumber': '',
    'carrierCode': '',
    'issueAge':null,
    'lastRefreshDate': '2016-02-11T14:30:00Z',
    
    'policyStatus': {
        'status': 'Pending',
        'workflowStatus': 'Application Received',
        'description': 'Application Received',
        'date': '2016-02-21T15:50:00Z'
    },

    'product': {
        'productCategory': '',
        'productName': '',
        'productTypeCode': '',
        'productCode': '',
        'underwritingRequired': false
    },

    'caseManager': {},

    'writingProducers': [
        {
            'fullName': '',
            'roleCode': 'dg33442'  
        }
    ],

    'servicingProducer': {},

    'applicationStatus': {
        'issueState': 'NC',
        'underWriter': null,
        'signed': null,
        'received': null,
        'entered': null,
        'uwDecision': null,
        'readyToIssue': null,
        'policySent': null,
        'paid': null
    },

    'billingDetail': {
    },

    'coverage': {
    },

    'investmentAllocation': [
    ]
};

policyData.pendingPolicyDetailAnnuityCare = {
    'caseId': '12341232132',
    'policyNumber': '720459750',
    'issueAge':45,
    'consolidatedPlan' : true,
    'carrierCode': 'AUL',
    'lastRefreshDate': '2016-02-11T14:30:00Z',

    'policyStatus': {
        'status': 'Pending',
        'workflowStatus': 'In Underwriting',
        'description': 'In Underwriting',
        'date': '2016-02-21T15:50:00Z'
    },

    'product': {
        'productTypeCategory'  : 'Annuity',
        'productCategory'      : 'Care Solutions',
        'productName'          : 'Annuity Care',
        'productTypeCode'      : 'SPFA',
        'productCode'          : '005T20',
        'underwritingRequired' : true
    },
    
    'caseManager': {},

    'writingProducers': [],

    'billingDetail' : {
        'currentBilling' : {
            'paymentMethod' : 'direct',
            'paymentMode'   : 'annual',
            'paymentAmount' : 'USD 51953.34'
        },
        'cashWithApplication' : 'USD 51953.34'
    },

    'coverage': {
        'base': [
            {
                'sequence': 1,
                'customerId': '123',
                'name': 'WL Legacy 121',
                'planName': 'WL Legacy 121',
                'faceAmount': 'USD 1.00',
                'monthlyBenefit': 'USD 0.00',
                'annualPremium': 'USD 1212.23'
            }
        ],
        'riders': [
            {
                'sequence': 2,
                'customerId': '123',
                'name': 'Accelerator Paid Up Additions Rirder',
                'planName': 'Accelerator Paid Up Additions Rirder',
                'faceAmount': 'USD 1.00',
                'monthlyBenefit': 'USD 21212.56',
                'annualPremium': 'USD 1212.23'
            }
        ]
    },

    'payout' : {
        'incomeOption'    : null,
        'payoutStartDate' : '2010-03-26'
    },

    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'SSN',
            'fullName': 'Joe The Policy holder',
            'issueAge': 44,
            'dateOfBirth': '2016-02-21T15:50:00Z',
            'gender': 'F',
            'maritalStatus': null
        }
    },
    'customerRoleContacts' : {
        '1001' : {
            'emailAddress': null,
            'post': {
                'type': 'MAIL',
                'line': [
                    '8205 test Drive EAST'
                ],
                'city': 'Test city',
                'state': 'WA',
                'zip': '12345'
            },
            'phoneNumber': {
                'home': null,
                'work': null
            }
        }
    },
    'customerRoles': {
        'Owner': [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }]
    }
};


policyData.pendingPolicyDetailAssetCare = {
    'caseId': '12341232132',
    'policyNumber': '720459750',
    'carrierCode': 'SL',
    'issueAge':45,
    'consolidatedPlan' : true,
    'lastRefreshDate': '2016-02-11T14:30:00Z',

    'policyStatus': {
        'status': 'Postponed',
        'workflowStatus': 'In Underwriting',
        'description': 'In Underwriting',
        'date': '2016-02-21T15:50:00Z'
    },

    'product': {
        'productCategory': 'Care Solutions',
        'productTypeCategory' : 'Life',
        'productName': 'Life',
        'productTypeCode': 'WL',
        'underwritingRequired': true
    },

    'caseManager': {},

    'writingProducers': [],

    'billingDetail' : {
        'currentBilling' : {
            'paymentMethod' : 'direct',
            'paymentMode'   : 'annual',
            'paymentAmount' : 'USD 51953.34'
        },
        'cashWithApplication' : 'USD 51953.34'
    },

    'application': {
        'issueState': 'NC',
        'underWriter': 'Barb Miskowic',
        'signed': '2016-02-10T18:50:00Z',
        'received': '2016-02-02T14:50:00Z',
        'entered': '2016-02-05T12:50:00Z',
        'uwDecision': '2016-02-05T12:50:00Z',
        'readyToIssue': null,
        'policySent': null,
        'paid': null
    },

    'ratingInformation' : [
        {
            'customerId': 123,
            'underwritingClass': 'Preferred',
            'tobaccoUse': false,
            'coverageRatings': [
                {
                    'coveragePlanName': null,
                    'coverageSequence': 1,
                    'mortalityTable': null,
                    'morbidityTable': null,
                    'ratedAge': 'P99Y'
                }
            ]
        },
        {
            'customerId': 456,
            'underwritingClass': 'Sub-Standard',
            'tobaccoUse': true,
            'coverageRatings': [
                {
                    'coveragePlanName': 'Asset Care 4 - 24 Pay',
                    'coverageSequence': 1,
                    'mortalityTable' : '8/H',
                    'morbidityTable' : '6/F',
                    'ratedAge'   : 'P99Y'
                }
            ]
        },
        {
            'customerId': 456,
            'underwritingClass': 'Sub-Standard',
            'coverageRatings': [
                {
                    'coveragePlanName': 'Asset Care 4 - 24 Pay',
                    'coverageSequence': 1,
                    'mortalityTable' : '8/H',
                    'morbidityTable' : '6/F',
                    'ratedAge'   : 'P99Y'
                }
            ]
        }
    ],
    'coverage': {
        'base': [
            {
                'sequence' : 1,
                'customerId': 123,
                'name': 'WL Legacy 121',
                'planName': 'WL Legacy 121',
                'faceAmount': 'USD 1.38',
                'monthlyBenefit': 'USD 0.00',
                'annualPremium': 'USD 1212.23'
            }
        ],
        'riders': [
            {
                'sequence' : 2,
                'customerId': 123,
                'name': 'Accelerator Paid Up Additions Rider',
                'planName': 'Accelerator Paid Up Additions Rider',
                'faceAmount': 'USD 1.38',
                'monthlyBenefit': 'USD 21212.56',
                'annualPremium': 'USD 1212.23'
            }
        ]
    },

    'payout' : {
        'incomeOption'    : null,
        'payoutStartDate' : '2010-03-26'
    },

    'customers': {
        '123': {
            'taxId': '2939',
            'taxIdType': 'SSN',
            'fullName': 'Joe The Policy holder',
            'issueAge': 44,
            'dateOfBirth': '2016-02-21T15:50:00Z',
            'gender': 'M',
            'maritalStatus': 'Married'
        },
        '456': {
            'taxId': '2940',
            'taxIdType' : 'SSN',
            'fullName': 'Mary The Joint Insured',
            'firstName': 'Mary',
            'lastName': 'Smith',
            'issueAge': 44,
            'dateOfBirth': '2016-02-21T15:50:00Z',
            'gender': 'M',
            'maritalStatus': 'Married'
        }
    },
    'customerRoleContacts' : {
        '1001' : {
            'emailAddress': null,
            'post': {
                'type': 'MAIL',
                'line': [
                    '8205 test Drive EAST'
                ],
                'city': 'Test city',
                'state': 'WA',
                'zip': '12345'
            },
            'phoneNumber': {
                'home': null,
                'work': null
            }
        },
        '1002' : {
            'emailAddress': null,
            'post': {
                'type': 'MAIL',
                'line': [
                    '1405 test Drive EAST'
                ],
                'city': 'NY city',
                'state': 'NY',
                'zip': '40215'
            },
            'phoneNumber': {
                'home': null,
                'work': null
            }
        }
    },

    'customerRoles': {
        'Owner': [{
            'customerId' : 123,
            'customerRoleContactId':1001
        }],
        'Insured' : [{
            'customerId' : 123,
            'customerRoleContactId':1002
        }],
        'Joint Insured': [{
            'customerId' : 456,
            'customerRoleContactId':1002
        }]
    }
};

module.exports = policyData;

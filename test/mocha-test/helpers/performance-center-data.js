// Performance Center Data
var pcData = {};

pcData.responseBody  = {
    'calendarYearProduction': {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 49843.84',
            'qualifyingYearToDate' : 'USD 25434.22'
        },
        'lifeLives' : {
            'totalSold' : -50,
            'forNextBonus' : 50,
            'nextBonusValue' : 'USD 750.00'
        },
        'asOfDate' : '2018-05-08'
    },
    'annualProductionRequirements': {
        'requiredQualifyingFyc' : 'USD 48000.00',
        'actualAdjustedYtdFyc' : 'USD 45678.90',
        'remainingQualifyingFyc' : 'USD 2321.10',
        'qualifyingFycFlag' : false,
        'actualAprStatus' : 'OS'
    },
    'chairmansTrip' : {
        'type'                       : 'retail',
        'conferenceYear'             : '2019',
        'actualQualFyc'              : 'USD 40000.34',
        'actualLives'                : 15,
        'onscheduleQualFyc'          : 'USD 51953.34',
        'onscheduleRemainingQualFyc' : 'USD 31048.97',
        'onscheduleLives'            : 15,
        'onscheduleRemainingLives'   : 15,
        'requiredQualFyc'            : 'USD 51953.34',
        'requiredRemainingQualFyc'   : 'USD 31048.97',
        'requiredLives'              : 15,
        'requiredRemainingLives'     : 15,
        'qualifyingGoalAchievedFlag' : false
    },
    'leadersConference' : {
        'type' : 'retail',
        'conferenceYear' : '2019',
        'actualQualFyc' : 'USD 54588.00',
        'actualTotalFyc' : 'USD 74268.00',
        'actualLives' : 5,
        'onscheduleQualFyc' : 'USD 85333.00',
        'onscheduleRemainingQualFyc' : 'USD 85333.00',
        'onscheduleRemainingTotalFyc' : 'USD 113232.00',
        'onscheduleTotalFyc' : 'USD 171875.00',
        'onscheduleLives' : 7,
        'onscheduleRemainingLives' : 7,
        'requiredQualFyc' : 'USD 128000.00',
        'requiredRemainingQualFyc' : 'USD 128000.00',
        'requiredTotalFyc'           : 'USD 187500.00',
        'requiredRemainingTotalFyc'  : 'USD 113232.00',
        'requiredLives' : 10,
        'requiredRemainingLives' : 10,
        'qualifyingGoalAchievedFlag' : false,
        'totalGoalAchievedFlag' : false,
        'leadersAprStatus' : 'OS'
    }
};

pcData.responseBodyIB  = {
    'calendarYearProduction': {
        'type' : 'ib',
        'commission' : {
            'totalYearToDate' : 'USD 49843.84'
        },
        'fypc' : {
            'totalYearToDate' : 'USD 45097.14',
            'qualifyingYearToDate' : 'USD 25434.22'
        },
        'asOfDate' : '2018-04-30'
    },
    'leadersConference' : {
        'type' : 'ib',
        'conferenceYear' : '2018',
        'actualQualFypc' : 'USD 54588.00',
        'actualLives' : 5,
        'onscheduleQualFypc' : 'USD 85333.00',
        'onscheduleRemainingQualFypc' : 'USD 85333.00',
        'onscheduleLives' : 7,
        'onscheduleRemainingLives' : 7,
        'requiredQualFypc' : 'USD 128000.00',
        'requiredRemainingQualFypc' : 'USD 128000.00',
        'requiredLives' : 10,
        'requiredRemainingLives' : 10,
        'qualifyingGoalAchievedFlag' : false,
    }
};

pcData.responseBodyGoals = {
    'calendarYearProduction': {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 49843.84',
            'qualifyingYearToDate' : 'USD 25434.22'
        },
        'lifeLives' : {
            'totalSold' : 201,
            'forNextBonus' : 0,
            'nextBonusValue' : ''
        },
        'asOfDate' : '2018-05-05'
    },
    'annualProductionRequirements': {
        'requiredQualifyingFyc' : 'USD 48000.00',
        'actualAdjustedYtdFyc' : 'USD 49678.90',
        'remainingQualifyingFyc' : 'USD 0.00',
        'qualifyingFycFlag' : true,
        'actualAprStatus' : 'FQ'

    }
};

pcData.responseBodyFycGoals = {
    'calendarYearProduction': {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 49843.84',
            'qualifyingYearToDate' : 'USD 25434.22'
        },
        'lifeLives' : {
            'totalSold' : 200,
            'forNextBonus' : 0,
            'nextBonusValue' : ''
        },
        'asOfDate' : '2018-04-25'
    },
    'annualProductionRequirements': {
        'requiredQualifyingFyc' : 'USD 48000.00',
        'actualAdjustedYtdFyc' : 'USD 49678.90',
        'remainingQualifyingFyc' : 'USD 0.00',
        'qualifyingFycFlag' : true,
        'actualAprStatus' : 'FQ'
    }
};

//response data for chairman's trip (CT) Qualifying Met goals
pcData.responseBodyCTQFypcGoals = {
    'calendarYearProduction' : {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 12345.84',
            'qualifyingYearToDate' : 'USD 43219.22'
        },
        'lifeLives' : {
            'totalSold' : 75,
            'forNextBonus' : 45,
            'nextBonusValue' : 'USD 750.00'
        },
        'asOfDate' : '2018-05-02'
    },
    'annualProductionRequirements' : {
        'requiredQualifyingFyc' : 'USD 48000.00',
        'actualAdjustedYtdFyc' : 'USD 56789.01',
        'remainingQualifyingFyc' : 'USD 0.00',
        'qualifyingFycFlag' : false,
        'actualAprStatus' : 'OS'
    },
    'chairmansTrip' : {
        'type'                       : 'retail',
        'conferenceYear'             : '2019',
        'actualQualFyc'              : 'USD 60953.34',
        'actualLives'                : 15,
        'onscheduleQualFyc'          : 'USD 51953.34',
        'onscheduleRemainingQualFyc' : 'USD 0.00',
        'onscheduleLives'            : 15,
        'onscheduleRemainingLives'   : 0,
        'requiredQualFyc'            : 'USD 51953.34',
        'requiredRemainingQualFyc'   : 'USD 0.00',
        'requiredLives'              : 15,
        'requiredRemainingLives'     : 0,
        'qualifyingGoalAchievedFlag' : true
    }
};

//response data for chairman's trip (CT) Qualifying Met goals
pcData.responseBodyCTQFypcNegative = {
    'calendarYearProduction' : {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 12345.84',
            'qualifyingYearToDate' : 'USD 43219.22'
        },
        'lifeLives' : {
            'totalSold' : 100,
            'forNextBonus' : 45,
            'nextBonusValue' : 'USD 750.00'
        },
        'asOfDate' : '2018-04-29'
    },
    'annualProductionRequirements' : {
        'requiredQualifyingFyc' : 'USD 48000.00',
        'actualAdjustedYtdFyc' : 'USD -6789.31',
        'remainingQualifyingFyc' : 'USD 54789.31',
        'qualifyingFycFlag' : false,
        'actualAprStatus' : 'OS'
    },
    'chairmansTrip' : {
        'type'                        : 'ib',
        'conferenceYear'              : '2019',
        'actualQualFypc'              : 'USD -11953.34',
        'actualLives'                 : -5,
        'onscheduleQualFypc'          : 'USD 51953.34',
        'onscheduleRemainingQualFypc' : 'USD 31048.97',
        'onscheduleLives'             : 15,
        'onscheduleRemainingLives'    : 15,
        'requiredQualFypc'            : 'USD 51953.34',
        'requiredRemainingQualFypc'   : 'USD 31048.97',
        'requiredLives'               : 15,
        'requiredRemainingLives'      : 15,
        'qualifyingGoalAchievedFlag'  : false
    }
};

//Leader's conference Independent Brokers
pcData.responseBodyLCIB = {
    'calendarYearProduction' : {
        'type' : 'ib',
        'commission' : {
            'totalYearToDate' : 'USD 23898.00'
        },
        'fypc' : {
            'totalYearToDate' : 'USD 24412.00',
            'qualifyingYearToDate' : 'USD 23213.00'
        },
        'lifeLives' : {
            'totalSold' : 58,
            'forNextBonus' : 50,
            'nextBonusValue' : 'USD 750.00'
        },
        'asOfDate' : '2018-05-03'
    },
    'chairmansTrip' : {
        'type' : 'ib',
        'conferenceYear' : '2019',
        'actualQualFypc' : 'USD 8100.00',
        'actualLives' : 2,
        'onscheduleQualFypc' : 'USD 189583.00',
        'onscheduleRemainingQualFypc' : 'USD 189583.00',
        'onscheduleLives' : 9,
        'onscheduleRemainingLives' : 9,
        'requiredQualFypc' : 'USD 8113.00',
        'requiredRemainingQualFypc' : 'USD 21322.00',
        'requiredLives' : 2,
        'requiredRemainingLives' : 1,
        'qualifyingGoalAchievedFlag' : false
    },
    'leadersConference' : {
        'type' : 'ib',
        'conferenceYear' : '2018',
        'actualQualFypc' : 'USD 54588.00',
        'actualLives' : 5,
        'onscheduleQualFypc' : 'USD 85333.00',
        'onscheduleRemainingQualFypc' : 'USD 85333.00',
        'onscheduleLives' : 7,
        'onscheduleRemainingLives' : 7,
        'requiredQualFypc' : 'USD 128000.00',
        'requiredRemainingQualFypc' : 'USD 128000.00',
        'requiredLives' : 10,
        'requiredRemainingLives' : 10,
        'qualifyingGoalAchievedFlag' : false,
        'totalGoalAchievedFlag' : false,
        'leadersAprStatus' : 'OS'
    }
};

//Leader's conference Career Agent
pcData.responseBodyLCCA = {
    'annualProductionRequirements' : {
        'requiredQualifyingFyc' : 'USD 54500.00',
        'actualAdjustedYtdFyc' : 'USD 25444.54',
        'remainingQualifyingFyc' : 'USD 29055.46',
        'qualifyingFycFlag' : false,
        'actualAprStatus' : 'OS'
    },
    'calendarYearProduction' : {
        'type' : 'retail',
        'commission' : {
            'totalYearToDate' : 'USD 55792.26',
            'qualifyingYearToDate' : 'USD 25444.54'
        },
        'lifeLives' : {
            'totalSold' : 69,
            'forNextBonus' : 52,
            'nextBonusValue' : 'USD 750.00'
        },
        'asOfDate' : '2018-04-18'
    },
    'chairmansTrip' : {
        'type'                       : 'retail',
        'conferenceYear'             : '2018',
        'actualQualFyc'              : 'USD 45102.00',
        'actualLives'                : 7,
        'onscheduleQualFyc'          : 'USD 189583.00',
        'onscheduleRemainingQualFyc' : 'USD 144481.00',
        'onscheduleLives'            : 9,
        'onscheduleRemainingLives'   : 2,
        'requiredQualFyc'            : 'USD 8113.00',
        'requiredRemainingQualFyc'   : 'USD 0.00',
        'requiredLives'              : 2,
        'requiredRemainingLives'     : 1,
        'qualifyingGoalAchievedFlag' : false
    },

    'leadersConference' : {
        'type'                        : 'retail',
        'conferenceYear'              : '2019',
        'actualQualFyc'               : 'USD 265104.00',
        'actualTotalFyc'              : 'USD 74268.00',
        'actualLives'                 : 24,
        'onscheduleQualFyc'           : 'USD 94444.00',
        'onscheduleRemainingQualFyc'  : 'USD 0.00',
        'onscheduleRemainingTotalFyc' : 'USD 113232.00',
        'onscheduleLives'             : 19,
        'onscheduleRemainingLives'    : 0,
        'onscheduleTotalFyc'          : 'USD 81932.00',
        'requiredQualFyc'             : 'USD 100000.00',
        'requiredRemainingQualFyc'    : 'USD 1692.00',
        'requiredTotalFyc'            : 'USD 187500.00',
        'requiredRemainingTotalFyc'   : 'USD 113232.00',
        'requiredLives'               : 20,
        'requiredRemainingLives'      : 0,
        'qualifyingGoalAchievedFlag'  : true,
        'totalGoalAchievedFlag'       : true,
        'leadersAprStatus'            : 'FQ'
    }
};

module.exports = {
    responseBody                : pcData.responseBody,
    responseBodyIB              : pcData.responseBodyIB,
    responseBodyGoals           : pcData.responseBodyGoals,
    responseBodyFycGoals        : pcData.responseBodyFycGoals,
    responseBodyCTQFypcGoals    : pcData.responseBodyCTQFypcGoals,
    responseBodyCTQFypcNegative : pcData.responseBodyCTQFypcNegative,
    responseBodyLCIB            : pcData.responseBodyLCIB,
    responseBodyLCCA            : pcData.responseBodyLCCA
};

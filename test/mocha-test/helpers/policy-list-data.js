var policyList = {
    draw : null,
    recordsTotal : 5,
    recordsFiltered : 5,
    producer : {
        id       : 1234567890,
        fullName : 'JOE PRODUCER'
    },
    data : [
        {
            policyId          : 8096728,
            customer          : {
                lexicalName : 'BUCHANAN, PEGGY',
                customerId  : 32430538
            },
            dateOfBirth       : '1950-02-27',
            relationship      : 'Owner',
            caseId            : '2016-05-16-10.03.20.967780',
            policyNumber      : '5200735720',
            productName       : 'Continuation Of Benefit',
            application       : {
                received    : '2016-05-16',
                paid            : '2016-06-30'
            },
            servicingProducer : {
                lexicalName : 'TISDALE, BRADLEY'
            },
            policyStatus      : {
                status         : 'Pending',
                statusDetail   : 'In Underwriting',
                workflowStatus : 'In Process',
                date           : '2016-05-18',
                description    : 'Paid'
            }
        },
        {
            policyId          : 8096728,
            customer          : {
                lexicalName : 'BUCHANAN, PEGGY',
                customerId  : 35148959
            },
            dateOfBirth       : '1950-02-27',
            relationship      : 'Owner',
            caseId            : '2016-05-16-10.03.20.967780',
            policyNumber      : '5200735720',
            productName       : 'Continuation Of Benefit',
            application       : {
                received    : '2016-05-16',
                paid            : '2016-06-30'
            },
            servicingProducer : {
                lexicalName : 'TISDALE, BRADLEY'
            },
            policyStatus      : {
                status         : 'Pending',
                statusDetail   : 'In Underwriting',
                workflowStatus : 'In Process',
                date           : '2016-05-18',
                description    : 'Terminated'
            }
        },
        {
            policyId          : 488488,
            customer          : {
                lexicalName : 'HARDLEY, PEGGY',
                customerId  : 35148959
            },
            dateOfBirth       : '1950-02-27',
            relationship      : 'Owner',
            caseId            : '2016-05-16-10.03.20.967780',
            policyNumber      : '5200735720',
            productName       : 'Continuation Of Benefit',
            application       : {
                received    : '2016-05-16',
                paid            : '2015-06-30'
            },
            servicingProducer : {
                lexicalName : 'TISDALE, BRADLEY'
            },
            policyStatus      : {
                status         : 'Pending',
                statusDetail   : 'In Underwriting',
                workflowStatus : 'In Process',
                date           : '2016-05-18',
                description    : 'In Process'
            }
        },
        {
            policyId          : 488488,
            customer          : {
                lexicalName : 'HARDLEY, PEGGY',
                customerId  : 35148959
            },
            dateOfBirth       : '1950-02-27',
            relationship      : 'Owner',
            caseId            : '2016-05-16-10.03.20.967780',
            policyNumber      : '5200735720',
            productName       : 'Continuation Of Benefit',
            application       : {
                received    : '2016-05-16',
                paid            : '2015-06-30'
            },
            servicingProducer : {
                lexicalName : 'TISDALE, BRADLEY'
            },
            policyStatus      : {
                status         : 'Terminated',
                statusDetail   : 'In Underwriting',
                workflowStatus : 'In Process',
                date           : '2016-05-18',
                description    : 'Paid'
            }
        },
        {
            policyId: 8989898915,
            caseId: '2017-09-12-13.12.39.672280',
            policyNumber: '0620857375',
            productName: 'American Protector Plus',
            application: {
                received : '2017-09-12',
                paid : '2017-11-29'
            },
            pendingRequirementCount: 1,
            customer: {
                customerId: 55798578,
                relationship: 'Insured',
                lexicalName: 'F ROMERO, MARGARITA',
                dateOfBirth: '1989-06-27'
            },
            servicingProducer: {
                lexicalName: 'VRGPGB, HHCHATJ  SWXZZL'
            },
            policyStatus: {
                acordHoldingStatus: 'Dormant',
                status: { 
                    dataAvailability: 'notAvailable' 
                },
                statusDetail: { 
                    dataAvailability: 'notAvailable' 
                },
                workflowStatus: { 
                    dataAvailability: 'notAvailable' 
                },
                date: '2018-05-15',
                statusSortOrder: 11,
                incompleteFinancials: false,
                description: { 
                    dataAvailability: 'notAvailable' 
                }
            }
        },
    ],
    error : null,
    column : null,
    dir : null,
    lastRefreshDate : '2016-12-28T11:50:00.000',
    dataSrc : 'data'
};

module.exports = {
    policyList  : policyList
};

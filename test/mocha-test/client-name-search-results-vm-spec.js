/* global expect:false */

var ClientNameSearchResultsViewModel = require(
    '../dist/pages/policy/viewModels/client-name-search-results-vm'
);

describe('Client Name Search Results View Model ' +
    '(pages/policy/viewModels/client-name-search-results-vm.js)', function() {

    it('hasPolicySearchByProducer defaults to false', function () {
        var model = new ClientNameSearchResultsViewModel();
        expect(model.has('hasPolicySearchByProducer')).to.equal(true);
        expect(model.get('hasPolicySearchByProducer')).to.equal(false);
    });

});

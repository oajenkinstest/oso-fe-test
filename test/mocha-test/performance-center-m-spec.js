/* global expect:false, $:false */

var helpers = require('../mocha-test/helpers/helpers');
var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);

describe('Performance Center Model ' +
    '(modules/performance-center/models/performance-center-m.js)', function () {

    var responseBody   = helpers.pcData.responseBody;
    var responseBodyIB = helpers.pcData.responseBodyIB;

    var performanceCenterModel;
    var ajaxStub;

    beforeEach(function () {
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            responseBody
        );
        performanceCenterModel = new PerformanceCenterModel();
        performanceCenterModel.fetch();
    });

    afterEach( function () {
        ajaxStub.restore();
    });

    after(function (){
        performanceCenterModel = null;
    });

    it('performanceCenterCollection object should exist', function () {
        expect(performanceCenterModel).to.exist;
    });

    it('should match all sections based on collections response parse (retail)', function () {
        expect(performanceCenterModel.get('fyc')).to.exist;
        expect(performanceCenterModel.get('qualifyingFyc')).to.exist;
        expect(performanceCenterModel.get('lifeLives')).to.exist;
        expect(performanceCenterModel.get('chairmansTrip')).to.exist;
        expect(performanceCenterModel.get('leadersConference')).to.exist;
        expect(performanceCenterModel.get('annualProductionRequirements')).to.exist;
    });

    it('should match all sections based on collections response parse (ib)', function () {
        ajaxStub.restore();
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            responseBodyIB
        );
        performanceCenterModel.clear();
        performanceCenterModel.fetch();
        expect(performanceCenterModel.get('fyc')).to.exist;
        expect(performanceCenterModel.get('fypc')).to.exist;
        expect(performanceCenterModel.get('qualifyingFypc')).to.exist;
    });

    it('should match "FYC" and "qualifyingFyc" text based on type "retail"', function () {
        expect(performanceCenterModel.get('fyc').textTwo)
            .to.equal('First Year Commission (FYC)');

        expect(performanceCenterModel.get('qualifyingFyc').textTwo)
            .to.equal('Qualifying FYC');
    });

    it('should match "FYC", "FYPC" and "Qualified FYPC" text based on type "IB"', function () {
        ajaxStub.restore();
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            responseBodyIB
        );
        performanceCenterModel.clear();
        performanceCenterModel.fetch();
        expect(performanceCenterModel.get('fyc').textTwo)
            .to.equal('First Year Commission (FYC)');

        expect(performanceCenterModel.get('fypc').textTwo)
            .to.equal('First Year Premium Credit (FYPC)');

        expect(performanceCenterModel.get('qualifyingFypc').textTwo)
            .to.equal('Qualifying FYPC');
    });

    describe('`chairmansTrip` data', function () {
        var ctObject;

        beforeEach(function () {
            ctObject = performanceCenterModel.get('chairmansTrip');
        });

        describe('When `type` is "retail"', function () {

            it('"actualQualFypc" should exist in the model and have a value ' +
                'equal to "actualQualFyc"', function () {
                var expectedValue = ctObject.actualQualFyc;

                expect(ctObject.actualQualFypc).equal(expectedValue);
            });

            it('"onscheduleQualFypc" should exist in the model and have a value ' +
                'equal to "onscheduleQualFyc"', function () {
                var expectedValue = ctObject.onscheduleQualFyc;

                expect(ctObject.onscheduleQualFypc).equal(expectedValue);
            });

            it('"onscheduleRemainingQualFypc" should exist in the model and have a value ' +
                'equal to "onscheduleRemainingQualFyc"', function () {
                var expectedValue = ctObject.onscheduleRemainingQualFyc;

                expect(ctObject.onscheduleRemainingQualFypc).equal(expectedValue);
            });

            it('"requiredQualFypc" should exist in the model and have a value ' +
                'equal to "requiredQualFyc"', function () {
                var expectedValue = ctObject.onscheduleRemainingQualFyc;

                expect(ctObject.onscheduleRemainingQualFypc).to.equal(expectedValue);
            });

            it('"requiredRemainingQualFypc" should exist in the model and have a value ' +
                'equal to "requiredRemainingQualFyc"', function () {
                var expectedValue = ctObject.requiredRemainingQualFyc;

                expect(ctObject.requiredRemainingQualFypc).to.equal(expectedValue);
            });

            it('"fyLabel" should be "FYC"', function () {
                expect(ctObject.fyLabel).to.equal('FYC');
            });

        });
    });
});

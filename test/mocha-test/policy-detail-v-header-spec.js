/* global describe, $, expect, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

describe('Policy Detail Page - Header (Top of Page) ' +
    '(pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var pendingPolicyData;
    var activePolicyData;
    var rootView;
    var view;
    var pendingRequirements = {
        obtainAtDelivery : [{
            customerId      : 25074156,
            requirementName : 'Proxy-Delivery Receipt',
            status          : 'Complete',
            dateCreated     : '2016-03-15T12:50:00Z',
            dateReceived    : null,
            neededToPay     : true,
            comments        : 'Collect on delivery.',
            new             : false
        }, {
            customerId      : 25074155,
            requirementName : 'Amendment',
            status          : 'Complete',
            dateCreated     : '2016-03-16T12:50:00Z',
            dateReceived    : null,
            neededToPay     : false,
            comments        : 'Collect on delivery.',
            new             : false
        }],
        received         : [{
            customerId      : 25498372,
            requirementName : 'Temporary Insurance Agreement',
            status          : 'Pending',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-05T12:50:00Z',
            neededToPay     : null,
            comments        : 'New form required to be completed and signed',
            new             : false
        }, {
            customerId      : 25498373,
            requirementName : 'Balance of Premium',
            status          : 'Complete',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-05T12:50:00Z',
            neededToPay     : null,
            comments        : 'I-20833 (ICC)',
            new             : false
        }, {
            customerId      : 25074155,
            requirementName : 'Bank Draft Authorization Form',
            status          : 'In House',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-25T12:50:00Z',
            neededToPay     : null,
            comments        : '$31.16 initial monthly premium due.',
            new             : false
        }]
    };
    var noPendingRequirements = {
        obtainAtDelivery : [{
            customerId      : 25074156,
            requirementName : 'Proxy-Delivery Receipt',
            status          : 'Complete',
            dateCreated     : '2016-03-15T12:50:00Z',
            dateReceived    : null,
            neededToPay     : true,
            comments        : 'Collect on delivery.',
            new             : false
        }, {
            customerId      : 25074155,
            requirementName : 'Amendment',
            status          : 'Complete',
            dateCreated     : '2016-03-16T12:50:00Z',
            dateReceived    : null,
            neededToPay     : false,
            comments        : 'Collect on delivery.',
            new             : false
        }],
        received         : [{
            customerId      : 25498372,
            requirementName : 'Temporary Insurance Agreement',
            status          : 'Waived',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-05T12:50:00Z',
            neededToPay     : null,
            comments        : 'New form required to be completed and signed',
            new             : false
        }, {
            customerId      : 25498373,
            requirementName : 'Balance of Premium',
            status          : 'Complete',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-05T12:50:00Z',
            neededToPay     : null,
            comments        : 'I-20833 (ICC)',
            new             : false
        }, {
            customerId      : 25074155,
            requirementName : 'Bank Draft Authorization Form',
            status          : 'In House',
            dateCreated     : '2016-02-10T12:50:00Z',
            dateReceived    : '2016-03-25T12:50:00Z',
            neededToPay     : null,
            comments        : '$31.16 initial monthly premium due.',
            new             : false
        }]
    };

    var setView = function() {
        var newview = new PolicyDetailView({
            stateObj : {
                policyId : 1234567890
            }
        });
        rootView.showChildView('contentRegion', newview);
        
        return newview;
    };

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
        ajaxStub = this.sinon.stub($, 'ajax');

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();

        pendingPolicyData = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        activePolicyData = $.extend(true,{},helpers.policyData.activePolicyDetail);
    });

    beforeEach(function () {
        pendingPolicyData = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
        activePolicyData = $.extend(true,{},helpers.policyData.activePolicyDetail);
    });

    after(function () {
        ajaxStub.restore();
        rootView.destroy();
    });

    describe('Sub Heading', function () {

        before(function () {
            ajaxStub.yieldsTo('success', pendingPolicyData);
            view = setView();
        });

        after(function () {
            view.destroy();
        });

        it('First part - policy status', function (){
            expect(view.$el.find('.page-header h1 span').text().trim())
                .to.be.contains(pendingPolicyData.policyStatus.status);
        });

        it('Second part - policy description', function (){
            expect(view.$el.find('.page-header h1 span').text().trim())
                .to.be.contains(pendingPolicyData.policyStatus.description);
        });

        it('last refresh date', function () {
            expect(view.$el.find('#last-refresh-date').text().trim())
                .to.equal('Last Refresh Date: 02/11/2016, 09:30 AM EST');
        });

        it('Second part of Sub Heading should hide for inactive policies', function (){
            var modifiedData = $.extend(true, {}, pendingPolicyData);
            modifiedData.policyStatus.status = 'Declined';

            ajaxStub.yieldsTo('success', modifiedData);
            view.destroy();
            view = setView();

            expect(view.$el.find('.page-header h1 span').text().trim())
                .to.be.not.contains(pendingPolicyData.policyStatus.description);
        });

        it('Second part of sub-heading should use `statusDetail` for ' +
            '"Terminated" policies', function () {
            var modifiedData = $.extend(true, {}, pendingPolicyData);
            var statusDetail = 'My Status Detail';
            modifiedData.policyStatus.statusDetail = statusDetail;
            modifiedData.policyStatus.status       = 'Terminated';

            ajaxStub.yieldsTo('success', modifiedData);
            view.destroy();
            view = setView();

            expect(view.$el.find('.page-header h1 span').text().trim()).to.contain(statusDetail);
        });

        describe('policy with active status', function () {
            before(function () {
                ajaxStub.yieldsTo('success', activePolicyData);
                view = setView();
            });

            after(function () {
                view.destroy();
            });
            
            it('First part - policy status', function (){
                expect(view.$el.find('.page-header h1 span').text().trim())
                    .to.be.contains(activePolicyData.policyStatus.acordHoldingStatus);
            });

            it('Second part - policy description', function (){
                expect(view.$el.find('.page-header h1 span').text().trim())
                    .to.be.contains(activePolicyData.policyStatus.statusDetail);
            });
        });

        describe('if acordHoldingStatus is Dormant/Unknown', function () {
            it('There will be no sub heading, only display "Policy Detail"', function () {
                pendingPolicyData.policyStatus = {
                    'acordHoldingStatus': 'Dormant',
                    'status': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'statusDetail': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'workflowStatus': { 
                        'dataAvailability': 'notAvailable' 
                    },
                    'date': '2018-05-15',
                    'statusSortOrder': 11,
                    'incompleteFinancials': false,
                    'description': { 
                        'dataAvailability': 'notAvailable' 
                    }
                };
                ajaxStub.yieldsTo('success', pendingPolicyData);
                view = setView();
                expect(view.$el.find('.page-header h1').text().trim())
                    .to.be.equal('Policy Detail');
            });
        });
    }); // Sub Heading

    describe('Jump links section', function () {

        describe('Pending policy', function () {
            describe('Happy path tests with typical data', function () {

                before(function () {
                    ajaxStub.yieldsTo('success', pendingPolicyData);
                    view = setView();
                });

                after(function () {
                    view.destroy();
                });

                describe('when exchangeHistory is present in the response', function () {

                    it('displays a link to the "1035 Exchange / Transfer" section', function () {
                        expect(view.$el.find('#jump-links a[href="#transfers"]').length)
                            .to.be.greaterThan(0);
                    });

                    describe('when the status of an exchangeHistory is "Pending"', function () {
                        var $badge;
                        var $link;

                        before(function () {
                            $badge = view.$el.find('#jump-links a[href="#transfers"]:eq(1) .badge');
                            $link  = view.$el.find('#jump-links a[href="#transfers"]:eq(0)');
                        });

                        it('display a badge next to the textual link which contains the number of ' +
                            'pending histories', function () {
                            expect($badge.html().trim()).to.equal('1');
                        });

                        it('title of the badge will be in the form of "See 1035/Transfers: (##) ' +
                            'Pending | (##) Received', function () {
                            var expectedTitle = 'See 1035/Transfers: (1) Pending | (2) Received';
                            var actualTitle   =
                                helpers.viewHelpers.removeDuplicateWhitespace($badge.attr('title'));

                            expect(actualTitle).to.equal(expectedTitle);
                        });

                        it('link text should be "1035 / Transfers Pending"', function () {
                            expect($link.text().trim()).to.equal('1035 / Transfers Pending');
                        });

                        it('link title will be ' +
                            '"See 1035/Transfers: (##) Pending | (##) Received"', function () {
                            var expectedTitle = 'See 1035/Transfers: (1) Pending | (2) Received';
                            var actualTitle   =
                                helpers.viewHelpers.removeDuplicateWhitespace($link.attr('title'));

                            expect(actualTitle).to.equal(expectedTitle);
                        });
                    }); // exchangeHistory Pending
                }); // exchangeHistory present

                describe('when requirements are present in the response', function () {

                    it('displays a link to the requirements section', function () {
                        expect(view.$el.find('#jump-links a[href="#requirements"]').length)
                            .to.be.greaterThan(0);
                    });

                    describe('when the status of requirements are "Pending"', function () {
                        var $badge;
                        var $link;

                        before(function () {
                            $badge = view.$el.find('#jump-links a[href="#requirements"]:eq(1) .badge');
                            $link  = view.$el.find('#jump-links a[href="#requirements"]:eq(0)');
                        });

                        it('display a badge next to the textual link which contains the number of ' +
                            'pending requirements', function () {
                            expect($badge.html().trim()).to.equal('2');
                        });

                        it('title of the badge will be in the form of "See Requirements: (##) Pending' +
                            ' | (##) Received"', function () {
                            var expectedTitle = 'See Requirements: (2) Pending | (3) Received';

                            expect($badge.attr('title')).to.equal(expectedTitle);
                        });

                        it('link text should be "Requirements Outstanding"', function () {
                            expect($link.text().trim()).to.equal('Requirements Outstanding');
                        });

                        it('link title will be ' +
                            '"See Requirements: (##) Pending | (##) Received"', function () {
                            var expectedTitle = 'See Requirements: (2) Pending | (3) Received';

                            // make white-space characters a single space for ease of comparison
                            var titleText = helpers.viewHelpers
                                .removeDuplicateWhitespace($link.attr('title'));

                            expect(titleText).to.equal(expectedTitle);
                        });

                        describe('Badge class changes depending on number of pending ' +
                            'requirements', function () {

                            it('badge class will be "badge-info" when there are ' +
                                'nine or less', function () {

                                expect($badge.hasClass('badge-info')).to.be.true;
                            });

                            it('badge class will be "badge-important" when there are ' +
                                '10 or more', function () {
                                var i;
                                var moreRequirementsData = $.extend(true, {}, pendingPolicyData);

                                // Duplicate the pending requirements so that there are more than 9
                                for (i = 0; i<5; i++) {
                                    moreRequirementsData.requirements.neededForUnderwriting.push(
                                        moreRequirementsData.requirements.neededForUnderwriting[0]
                                    );

                                    moreRequirementsData.requirements.obtainAtDelivery.push(
                                        moreRequirementsData.requirements.obtainAtDelivery[0]
                                    );
                                }

                                ajaxStub.yieldsTo('success', moreRequirementsData);
                                view.destroy();
                                view = setView();

                                $badge = view.$el
                                    .find('#jump-links a[href="#requirements"]:eq(1) .badge');

                                expect($badge.hasClass('badge-important')).to.be.true;
                            });
                        });
                    }); // requirements pending

                    describe('when the status of requirements are NOT "Pending"', function () {
                        var $badge;
                        var $link;

                        before(function () {
                            var modifiedData = $.extend(true, {}, pendingPolicyData);
                            modifiedData.requirements = noPendingRequirements;

                            ajaxStub.yieldsTo('success', modifiedData);
                            view = setView();

                            $link  = view.$el.find('#jump-links a[href="#requirements"]:eq(0)');
                            $badge = view.$el.
                                find('#jump-links a[href="#requirements"]:eq(1) .badge');
                        });

                        after(function () {
                            view.destroy();
                        });

                        it('a badge will NOT be displayed next to the textual link', function () {
                            expect($badge.length).to.equal(0);
                        });

                        it('link text should be "Requirements"', function () {
                            expect($link.text().trim()).to.equal('Requirements');
                        });

                        it('link title will be "See Requirements: (##) Received"', function () {
                            var expectedTitle = 'See Requirements: (5) Received';

                            // make white-space characters a single space for ease of comparison
                            var titleText = helpers.viewHelpers
                                .removeDuplicateWhitespace($link.attr('title'));

                            expect(titleText).to.equal(expectedTitle);
                        });
                    }); // requirements NOT Pending
                });
            }); // Happy path tests with typical data

            describe('when exchangeHistory is NOT present in the response', function () {

                it('"1035 / Transfer" link does not show in view', function () {
                    var modifiedData = $.extend(true, {}, pendingPolicyData);
                    delete modifiedData.exchangeHistory;

                    ajaxStub.yieldsTo('success', modifiedData);
                    view = setView();

                    expect(view.$el.find('#jump-links a[href="#transfers"]').length).to.equal(0);

                    view.destroy();
                });
            }); // exchangeHistory NOT present

            describe('when the status of exchangeHistories are NOT "Pending"', function () {
                var $badge;
                var $link;

                before(function () {
                    var noPendingExchangeHistory = [
                        {
                            otherCarrierName         : 'A USAA',
                            otherCarrierPolicyNumber : '30900405850',
                            dateReceived             : '2016-06-14',
                            amountReceived           : 'USD 20000.00',
                            comments                 : [
                                {
                                    date    : '2016-06-02',
                                    comment : 'FAXED PACKET TO 8774357099'
                                }, {
                                    date    : '2016-06-06',
                                    comment : 'Spoke with Melissa. A check was mailed on 06/03/2016.'
                                }, {
                                    date    : '2016-06-14',
                                    comment : 'Direct transfer. No cost basis required.'
                                }
                            ]
                        }, {
                            otherCarrierName         : 'USAA Prudential',
                            otherCarrierPolicyNumber : '40902595727',
                            dateReceived             : '2016-06-14',
                            amountReceived           : 'USD 14355.76',
                            comments                 : [
                                {
                                    date    : '2016-06-02',
                                    comment : 'FAXED PACKET TO 4544775454'
                                }, {
                                    date    : '2016-06-06',
                                    comment : 'Spoke with Jasmin. A check was mailed on 06/03/2016.'
                                }, {
                                    date    : '2016-06-14',
                                    comment : 'Direct transfer. Cost basis required.'
                                }
                            ]
                        }, {
                            otherCarrierName         : 'AB USAA',
                            otherCarrierPolicyNumber : '30900405860',
                            dateReceived             : '2016-06-14',
                            amountReceived           : 'USD 20000.00',
                            comments                 : [
                                {
                                    date    : '2016-06-02',
                                    comment : 'FAXED PACKET TO 8774357099'
                                }, {
                                    date    : '2016-06-06',
                                    comment : 'Spoke with Melissa. A check was mailed on 06/03/2016.'
                                }, {
                                    date    : '2016-06-14',
                                    comment : 'Direct transfer. No cost basis required.'
                                }
                            ]
                        }
                    ];
                    var modifiedData = $.extend(true, {}, pendingPolicyData);
                    modifiedData.exchangeHistory = noPendingExchangeHistory;

                    ajaxStub.yieldsTo('success', modifiedData);
                    view = setView();

                    $link  = view.$el.find('#jump-links a[href="#transfers"]:eq(0)');
                    $badge = view.$el.find('#jump-links a[href="#transfers"]:eq(1) .badge');
                });

                after(function () {
                    view.destroy();
                });

                it('a badge will NOT be displayed next to the textual link', function () {
                    expect($badge.length).to.equal(0);
                });

                it('link text should be "1035 / Transfers"', function () {
                    expect($link.text().trim()).to.equal('1035 / Transfers');
                });

                it('link title will be "See 1035/Transfers: (##) Received"', function () {
                    var expectedTitle = 'See 1035/Transfers: (3) Received';

                    // make white-space characters a single space for ease of comparison
                    var titleText = helpers.viewHelpers.removeDuplicateWhitespace($link.attr('title'));

                    expect(titleText).to.equal(expectedTitle);
                });
            }); // exchangeHistory NOT Pending

            describe('when requirements are NOT present in the response', function () {

                it('"Requirements" link does not show in view', function () {
                    var modifiedData = $.extend(true, {}, pendingPolicyData);
                    delete modifiedData.requirements;

                    ajaxStub.yieldsTo('success', modifiedData);
                    view = setView();

                    expect(view.$el.find('#jump-links a[href="#requirements"]').length).to.equal(0);

                    view.destroy();
                });

            }); // requirements NOT present
        }); // Pending policy

        describe('Active policy', function () {

            it('shows "Requirements" link if there are pending requirements', function () {
                var modifiedData = $.extend(true, {}, activePolicyData);
                modifiedData.requirements = pendingRequirements;

                ajaxStub.yieldsTo('success', modifiedData);
                view = setView();

                expect(view.$el.find('#jump-links a[href="#requirements"]').length).to.be.greaterThan(0);

                view.destroy();
            });

            it('does not show "Requirements" link if there are no requirements', function () {
                var modifiedData = $.extend(true, {}, activePolicyData);
                delete modifiedData.requirements;

                ajaxStub.yieldsTo('success', modifiedData);
                view = setView();

                expect(view.$el.find('#jump-links a[href="#requirements"]').length).to.equal(0);

                view.destroy();
            });

            it('does not show "Requirements" link if there are requirements, ' +
                'but none of them are pending', function () {
                var modifiedData = $.extend(true, {}, activePolicyData);
                modifiedData.requirements = noPendingRequirements;

                ajaxStub.yieldsTo('success', modifiedData);
                view = setView();

                expect(view.$el.find('#jump-links a[href="#requirements"]').length).to.equal(0);

                view.destroy();
            });
            
        }); // Active policy
}); // Jump Links section

});

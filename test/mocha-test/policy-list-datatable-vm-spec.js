/* global expect:false */

var Model = require('../dist/pages/policy/viewModels/policy-list-datatable-vm');

describe('Policy List DataTable View Model ' + 
    '(pages/policy/viewModels/policy-list-datatable-vm.js)', function () {

    it('should exist', function () {
        expect(Model).to.exist;
    });

    it('defaults values should match', function () {
        var model = new Model();

        expect(model.get('col')).to.equal(0);
        expect(model.get('dir')).to.equal('asc');
        expect(model.get('length')).to.equal(25);
        expect(model.get('producerId')).to.equal(null);
        expect(model.get('start')).to.equal(0);
        expect(model.get('status')).to.equal('pending');
    });

    it('Ensures that col, length, and start props are Number', function () {
        var stringVal = '25';
        var numVal    = 25;
        var model     = new Model({
            col    : stringVal,
            length : stringVal,
            start  : stringVal
        });

        // make sure this isn't doing type coercion!
        expect(stringVal).to.not.equal(numVal);

        // check the values
        expect(model.get('col')).to.equal(numVal);
        expect(model.get('length')).to.equal(numVal);
        expect(model.get('start')).to.equal(numVal);
    });

    it('When the attrs are already Number, everything is cool', function () {
        var numVal = 25;
        var model  = new Model({
            col    : numVal,
            length : numVal,
            start  : numVal
        });

        // check the values
        expect(model.get('col')).to.equal(numVal);
        expect(model.get('length')).to.equal(numVal);
        expect(model.get('start')).to.equal(numVal);
    });

    it('Uses defaults if the initial col, length, or start props are not Numeric', function () {
        var model = new Model({
            col    : 'monkey',
            length : 'giraffe',
            start  : 'hippopotamus'
        });

        expect(model.get('col')).to.equal(model.defaults.col);
        expect(model.get('length')).to.equal(model.defaults.length);
        expect(model.get('start')).to.equal(model.defaults.start);
    });

    it('get model URL based on data - if model has "producerId"', function () {
        var model  = new Model({
            status      : 'pending',
            producerId  : '10010010'
        });
        var urlExpected = '/api/oso/secure/rest/policies/summaries'+
            '?hierarchy=producer&statusView=pending&reportingGroup=pending&producerId=10010010';
        expect(model.url()).to.equal(urlExpected);
    });

    it('get model URL based on data - if model has "producerOrgId"', function () {
        var model  = new Model({
            status      : 'pending',
            producerOrgId  : '10010010'
        });
        var urlExpected = '/api/oso/secure/rest/policies/summaries'+
            '?hierarchy=org&statusView=pending&reportingGroup=pending&producerId=10010010';
        expect(model.url()).to.equal(urlExpected);
    });

    it('get model URL based on data - if model has "hierarchy" and with value "org"', 
            function () {
        var model  = new Model({
            status      : 'pending',
            hierarchy   : 'org'
        });
        var urlExpected = '/api/oso/secure/rest/policies/summaries'+
            '?hierarchy=org&statusView=pending&reportingGroup=pending';
        expect(model.url()).to.equal(urlExpected);
    });

    it('reportingGroup query in model URL should empty', function () {
        var model  = new Model({
            status      : '',
            producerOrgId  : '10010010'
        });
        var urlExpected = '/api/oso/secure/rest/policies/summaries'+
            '?hierarchy=org&statusView=pending&reportingGroup=&producerId=10010010';
        expect(model.url()).to.equal(urlExpected);
    });
});

/* global expect:false */

var SalesConnectionLinksModel = require('../dist/pages/home/models/sales-connection-links-m');

describe('Sales Connection Links Model ' +
    '(pages/home/models/sales-connection-links-m.js)', function () {

    var model = new SalesConnectionLinksModel();

    describe('Model data Fetch URL', function () {

        it('Should match iGO url', function () {
            var expectedEndPoint = '/api/oso/secure/rest/sso/ipipeline/igo';
            expect(model.url()).to.equal(expectedEndPoint);
        });

        it('Should match practice URL, if set ID', function () {
            var expectedEndPoint = '/api/oso/secure/rest/sso/ipipeline/igo/practice';
            model.set('id', 'practice');
            expect(model.url()).to.equal(expectedEndPoint);
        });
    });

});

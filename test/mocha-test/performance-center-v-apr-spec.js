/* global expect:false, $:false, isCanvasLoaded:false, sinon:false */
var helpers = require('./helpers/helpers');
var utils   = require('../dist/utils/utils');


var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);

describe('Performance Center View : Annual Production Requirement(APR)'+
    '(modules/performance-center/views/performance-center-v.js)', function () {

    var view;
    var ajaxStub;

    var getViewWithData = function (data) {
        var pcView;
        var root = helpers.viewHelpers.createRootView();
        if (ajaxStub) {
            ajaxStub.restore();
        }
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo('success', data);

        pcView = new PerformanceCenterView({
            model : new PerformanceCenterModel()
        });

        root.render();
        root.contentRegion.show(pcView);

        return pcView;
    };

    before(function () {

        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
    });

    after(function () { 
        if (ajaxStub) {
            ajaxStub.restore();
        }
    });

    describe ('retail', function () {
        var aprQualifyingPanel;
        var copyOfResponseBody = $.extend(true,{},helpers.pcData.responseBody);
        var negativeValues     = $.extend(true,{},helpers.pcData.responseBodyCTQFypcNegative);
        var responseBodyGoals  = $.extend(true,{},helpers.pcData.responseBodyGoals);

        before(function () {
            view  = getViewWithData(copyOfResponseBody);
            aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
        });

        after(function (){
            view.destroy();
        });

        it('header value should be correct', function (){
            var headerValue = helpers.viewHelpers.removeDuplicateWhitespace(
                aprQualifyingPanel.find('h4:eq(0)').text().trim()
            );
            expect(headerValue).to.equal('Annual Production Requirement');
        });

        describe('"Qualifying FYC" dial and bullets', function () {

            describe('When goal has not yet been achieved', function () {

                it('dial displays "actualAdjustedYtdFyc"', function () {
                    expect(aprQualifyingPanel.find('.chart-outer').data('percent'))
                    .to.equal(45679);
                });

                // Unable to add a test to check label value printed on center of dial
                // for some reason its not getting accurately
                // spend more time, but not worth.

                it('Comparison bullet should display in "$<remainingQualifyingFypc> Qualifying ' +
                    'FYC needed to reach goal of $<requiredQualifyingFyc> format"', function () {
                    var aprData        = copyOfResponseBody.annualProductionRequirements;
                    var remainingValue = utils.formatAsCurrency(aprData.remainingQualifyingFyc,
                        true, false);
                    var requiredValue  = utils.formatAsCurrency(aprData.requiredQualifyingFyc,
                        true, false);
                    var expected       = remainingValue + ' Qualifying FYC needed to reach ' +
                        'goal of ' + requiredValue;

                    var bulletText     = helpers.viewHelpers.removeDuplicateWhitespace(
                        aprQualifyingPanel.find('.qualifying-fyc-comparison').text().trim());

                    expect(bulletText).to.equal(expected);
                });

                it('Qualifying FYC should display in "$<actualAdjustedYtdFyc>'+ 
                        'actual Qualifying FYC" format', function () {
                    var actualValue = utils.formatAsCurrency(
                        copyOfResponseBody.annualProductionRequirements.actualAdjustedYtdFyc,
                        true, false);
                    var expected    = actualValue + ' actual Qualifying FYC';
                    var bulletText  = helpers.viewHelpers.removeDuplicateWhitespace(
                        aprQualifyingPanel.find('.actual-qualifying-fyc').text().trim());

                    expect(bulletText).to.equal(expected);
                });

                it('Qualifying FYC amount should be in red color', function () {
                    var valueSpan = aprQualifyingPanel.find('#apr-qualifying-fyc');

                    expect(valueSpan.hasClass('oa-lake')).to.be.true;
                });
            });     // When goal has not yet been achieved

            describe ('Apr Status bullet', function () {

                it('APR status NMMR', function () {
                    var bulletTxt;
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = 'NMMR';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    bulletTxt = aprQualifyingPanel.find('li.apr-status-text').text().trim();
                    expect(bulletTxt).to.be.equal('APR not met');
                });

                it('APR status New-Agent', function () {
                    var bulletTxt;
                    copyOfResponseBody
                            .annualProductionRequirements.actualAprStatus = 'New-Agent';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    bulletTxt = aprQualifyingPanel.find('li.apr-status-text').text().trim();
                    expect(bulletTxt).to.be.equal('APR not required');
                });

                it('APR status 1-in-3', function () {
                    var bulletTxt;
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = '1-in-3';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    bulletTxt = aprQualifyingPanel.find('li.apr-status-text').text().trim();
                    expect(bulletTxt).to.be.equal('One-in-Three Goal Achieved!');
                });

                it('APR status OS', function () {
                    var bulletTxt;
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = 'OS';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    bulletTxt = aprQualifyingPanel.find('li.apr-status-text').text().trim();
                    expect(bulletTxt).to.be.equal('APR on schedule');
                });
            });

            describe('When "actualAdjustedYtdFyc" value is negative', function () {

                before(function () {
                    view               = getViewWithData(negativeValues);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                });

                it('value in chart displays a negative number in red text', function () {
                    var value = aprQualifyingPanel.find('.chart-text-one');

                    expect(value.text()).to.contain('-');
                    expect(value.hasClass('oa-banner-red')).to.be.true;
                });

                it('Actual Qualifying FYC amount displays in red text', function () {
                    var value = aprQualifyingPanel.find('#apr-qualifying-fyc');

                    expect(value.hasClass('oa-banner-red')).to.be.true;
                });
            });

            describe('Goal achievement', function () {

                before(function () {
                    view               = getViewWithData(responseBodyGoals);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                });

                it('check displayed in graph', function () {
                    var checkMark = aprQualifyingPanel.find('.fa-check');
                    expect(checkMark.length).to.equal(1);
                });

                it('APR status text displays "Goal Achieved!" ', function () {
                    var bullet   = aprQualifyingPanel.find('li.apr-status-text');
                    var expected = 'Goal Achieved!';

                    expect(bullet.text().trim()).to.equal(expected);
                });

            });     // Goal achievement

            describe('comparison bullet (2nd) should not be displayed', function () {
               
                it('when APR status is FQ', function () {
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = 'FQ';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;

                    expect(aprQualifyingPanel.find('li')).to.be.lengthOf(3);
                    expect(aprQualifyingPanel.find('#apr-remaining-fyc')).to.be.lengthOf(0);
                });

                it('when APR status is New-Agent', function () {
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = 'New-Agent';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    expect(aprQualifyingPanel.find('li')).to.be.lengthOf(3);
                    expect(aprQualifyingPanel.find('#apr-remaining-fyc')).to.be.lengthOf(0);
                });

                it('For other APR status ', function () {
                    copyOfResponseBody.annualProductionRequirements.actualAprStatus = 'OS';
                    view               = getViewWithData(copyOfResponseBody);
                    aprQualifyingPanel = view.annualProductionRequirementsRegion.$el;
                    expect(aprQualifyingPanel.find('li')).to.be.lengthOf(4);
                    expect(aprQualifyingPanel.find('#apr-remaining-fyc')).to.be.lengthOf(1);
                });

            });

        });     // "Qualifying FYPC" dial and bullets
    });

    describe('independent broker (ib)', function () {
        var responseBodyIB = helpers.pcData.responseBodyIB;
        before(function () {
            view  = getViewWithData(responseBodyIB);
        });
        it ('APR region should not be exist', function (){
            expect(view.annualProductionRequirementsRegion).to.be.undefined;
        });
    });
});


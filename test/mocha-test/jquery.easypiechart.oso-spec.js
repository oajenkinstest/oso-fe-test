/* global expect:false, isCanvasLoaded:false, Marionette:false, _:false */

describe('EasyPieChart plugin (utils/jquery.easypiechart.oso.js)', function() {

    var chartLabel;
    var clock;
    var dialElement;
    var onStepSpy;
    var root;
    var View;

    var viewTemplate = '<div id="dial" data-percent="150" >' +
        '<span class="chart-label"></span>' +
        '</div>';

    var dialOptions = {
        animate:{
            duration:2500,
            enabled: true
        },
        maxValue:200,
        size:190,
        rotate: 225,
        centerLabel: {
            animate: true,
            elementSelector: '.chart-label',
            round:true,
            sign:1,
            formatFunction: function (val) {
                return val;
            }
        }
    };


    beforeEach(function () {
        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.skip();
        }

        clock = this.sinon.useFakeTimers();

        root = new Marionette.LayoutView({
            template: _.template('<div id="dial-layout"></div>'),
            regions: {
                mainRegion: '#dial-layout'
            }
        });

        View = Marionette.ItemView.extend({
            template: _.template(viewTemplate),
            onRender: function () {
                this.$el.find('#dial').easyPieChart(dialOptions);
            }
        });

        root.render();
        root.mainRegion.show(new View());
        dialElement = root.getChildView('mainRegion').$el.find('#dial');
        chartLabel = dialElement.find('.chart-label');
        onStepSpy = this.sinon.spy(dialElement.data('easyPieChart').renderer, 'onStep');
    });

    afterEach(function () {
        root.destroy();
        clock.restore();
        onStepSpy.restore();
    });

    it('should exist', function () {
        expect(dialElement.easyPieChart).to.be.defined;
    });

    it('dial element should bind with easyPieChart data', function () {
        expect(dialElement.data('easyPieChart')).to.be.an.object;
    });

    it('onStep method should exist', function () {
        expect(dialElement.data('easyPieChart').renderer.onStep).to.be.exist;
        expect(dialElement.data('easyPieChart').renderer.onStep).to.be.a('function');
    });

    it('updateCenterLabel method should exist', function () {
        expect(dialElement.data('easyPieChart').renderer.updateCenterLabel).to.be.exist;
        expect(dialElement.data('easyPieChart').renderer.updateCenterLabel).to.be.a('function');
    });

    it('if dialOptions.centerLabel.animate=true (the default), onStep method is called ' +
            'and label is animated', function () {
        // Convert the chart label text into a Number so that we can use closeTo() to verify
        // that at a certain tick in the animation, the label is within an acceptable offset of
        // an expected number (the exact value at each tick isn't really important).
        var acceptableOffset = 2;

        clock.tick(500);
        expect(parseInt(chartLabel.text(), 10)).to.be.closeTo(11, acceptableOffset);

        clock.tick(1000);
        expect(parseInt(chartLabel.text(), 10)).to.be.closeTo(100, acceptableOffset);

        clock.tick(2500);
        expect(parseInt(chartLabel.text(), 10)).to.be.closeTo(150, acceptableOffset);

        expect(onStepSpy).to.have.been.called;
    });

    it('if dialOptions.centerLabel.animate=false, onStep method is not called but label ' +
            'is updated once', function () {

        View = Marionette.ItemView.extend({
            template: _.template(viewTemplate),
            onRender: function () {
                dialOptions.centerLabel.animate = false;
                this.$el.find('#dial').easyPieChart(dialOptions);
            }
        });

        root.mainRegion.show(new View());
        dialElement = root.getChildView('mainRegion').$el.find('#dial');
        chartLabel = dialElement.find('.chart-label');

        //reset onStepSpy call records
        onStepSpy.resetHistory();

        //dial animation time, not animating label, value always will be same
        clock.tick(500);
        expect(chartLabel.text()).to.equal('150');

        clock.tick(1000);
        expect(chartLabel.text()).to.equal('150');

        clock.tick(2500);
        expect(chartLabel.text()).to.equal('150');

        expect(onStepSpy).to.have.not.been.called;
    });

});

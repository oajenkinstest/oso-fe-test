/* global expect:false */
var config = require('../dist/config/config');


describe('Config Module (config/config.js)', function() {

    it('config object should exist', function () {
        expect(config).to.exist;
    });

    it('config should have getAjaxConfig method', function () {
        expect(config.getAjaxConfig).to.exist;
    });

});

describe('getAjaxConfig should set correct config based on hostname', function() {

    it('hostname: www.sbols.oneamerica.com', function () {

        config = config.getAjaxConfig('www.sbols.oneamerica.com');

        expect(config.apiUrlRoot).to.equal('/api/oso/secure/rest/');
        expect(config.apiPublicUrlRoot).to.equal('/api/oso/public/web/');
    });

    it('hostname: stonesourceonline.oneamerica.com', function () {

        config = config.getAjaxConfig('stonesourceonline.oneamerica.com');

        expect(config.defaultLoginPage).to.equal('https://www.st.oneamerica.com/login');
        expect(config.pinpointURL)
            .to.equal('https://stapi.oneamerica.com/oso/secure/web/sso/pinpoint');
    });

    it('hostname: ftonesourceonline.oneamerica.com', function () {

        config = config.getAjaxConfig('ftonesourceonline.oneamerica.com');

        expect(config.defaultLoginPage).to.equal('https://www.ft.oneamerica.com/login');
        expect(config.pinpointURL)
            .to.equal('https://ftapi.oneamerica.com/oso/secure/web/sso/pinpoint');
    });

    it('hostname: onesourceonline.oneamerica.com', function () {

        config = config.getAjaxConfig('onesourceonline.oneamerica.com');

        expect(config.defaultLoginPage).to.equal('https://www.oneamerica.com/login');
        expect(config.pinpointURL).to.equal('https://api.oneamerica.com/oso/secure/web/sso/pinpoint');
    });

});

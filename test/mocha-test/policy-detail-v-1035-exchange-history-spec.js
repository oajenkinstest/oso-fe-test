/* global expect, $, sinon */

// load behaviors
require('../dist/modules/behaviors/index');

var PolicyDetailView = require('../dist/pages/policy/views/policy-detail-v');
var helpers = require('./helpers/helpers');

describe('Policy Detail - 1035 Exchange History Section ' +
    ' (pages/policy/views/policy-detail-v.js)', function () {

    var ajaxStub;
    var policyDataCopy;
    var rootView;
    var view;
    var xChangeHistoryElement;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            policyDataCopy
        );

        view = new PolicyDetailView({
            stateObj: {
                policyId: 1234567890
            }
        });

        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
        rootView.showChildView('contentRegion', view);

        xChangeHistoryElement = view.$el.find('#policy-1035-exchange-history-comments');
    });

    after(function() {
        ajaxStub.restore();
        view.destroy();
    });

    it('section should exist', function () {
        expect(xChangeHistoryElement.length).to.equal(1);
    });

    it('table Header columns value should match', function (){
        var thElements = xChangeHistoryElement.find('th');
        expect($(thElements[0]).text()).to.equal('Last Activity');
        expect($(thElements[1]).text()).to.equal('Company Name');
        expect($(thElements[2]).text()).to.equal('Company Policy Number');
        expect($(thElements[3]).text()).to.equal('Date Received');
        expect($(thElements[4]).text()).to.equal('Amount Received');
    });

    it('View Comments link should exist to each Transfer row\'s first column', function () {
        var transferRows = xChangeHistoryElement.find('tbody tr:even');
        $(transferRows).each(function (index ,row) {
            expect($(row).find('td:eq(0) a').text().trim()).to.contain('View Comments');
        });
    });

    it.skip('comments TR should be expanded while clicking on View Comments links', function (){
    // Skipping this test because the following .click() call doesn't seem to have any effect
    // on the elements. Maybe because of jsdom? --RKC 06/20/2017
    //
    // Bootstrap's "Collapse" plugin is responsible for toggling the aria-expanded attribute on the
    // link and on the <tr> containing the comments. Bootstrap's click listener is attached to the
    // document root, so it isn't seeing the click that is happening in the rootView because the
    // rootView isn't being inserted into the document.
    // 
    // I've tried changing the helpers.viewHelpers.createRootView function to go ahead and put the
    // rootView into the document, but it causes lots of other tests to fail. Attempting to fix
    // the other failures turned into an enormous task, so I'm giving up on this test.
    // --RKC Dec 11, 2017
        xChangeHistoryElement.find('a[href="#exchange-comments-1"]').click();

        expect(xChangeHistoryElement.find('a[href="#exchange-comments-1"]')
            .attr('aria-expanded')).to.equal('true');

        expect(xChangeHistoryElement.find('tr[id="exchange-comments-1"]')
            .attr('aria-expanded')).to.equal('true');
    });

    it('comments should be prefixed by the date and timestamp', function () {
        var expected = '06/14/2016, 07:42 PM';
        var result   = xChangeHistoryElement.find('#exchange-comments-0 .profile-info-row ' +
            '.profile-info-name:eq(0)').text().trim();

        expect(result).to.equal(expected);
    });

    it('should not render when data not exist', function () {

        // First, show that the exchange history element is present
        expect(xChangeHistoryElement.length).above(0);

        // Then remove the exchangeHistory data and re-render the view
        policyDataCopy.exchangeHistory = [];

        ajaxStub.yieldsTo(
            'success',
            policyDataCopy
        );

        view.destroy();
        view = new PolicyDetailView({
            stateObj: {
                policyId: 1234567890
            }
        });

        // Now, show that the exchange history element is NOT present
        xChangeHistoryElement = view.$el.find('#policy-1035-exchange-history-comments');

        expect(xChangeHistoryElement.length).to.equal(0);

        view.destroy();
    });

});

/* global expect:false, $:false */

var Model = require('../dist/pages/policy/models/policy-list-counts-m');

describe('Policy List Counts Model '+
    '(pages/policy/models/policy-list-counts-m.js)', function() {

    it('should exist', function() {
        expect(Model).to.exist;
    });

    it('has defaults', function() {
        var model = new Model();

        expect(model.get('ALL')).to.equal(0);
        expect(model.get('PENDING')).to.equal(0);
        expect(model.get('PAID')).to.equal(0);
        expect(model.get('INACTIVE')).to.equal(0);
    });

    it('url should build correctly', function () {
        var model = new Model({
            producerId: 10001
        });

        var urlExpected = '/api/oso/secure/rest/policies/summaries/counts'+
            '?hierarchy=producer&statusView=pending&producerId=10001';

        expect(model.url()).to.equal(urlExpected);
    });

    it('url should build correctly - if model has producerOrgId', function () {
        var model = new Model({
            producerOrgId: 10001
        });

        var urlExpected = '/api/oso/secure/rest/policies/summaries/counts'+
            '?hierarchy=org&statusView=pending&producerId=10001';

        expect(model.url()).to.equal(urlExpected);
    });

    it('url should build correctly - if model has item "hierarchy" with value "org" ', 
            function () {
        var model = new Model({
            hierarchy: 'org'
        });

        var urlExpected = '/api/oso/secure/rest/policies/summaries/counts'+
            '?hierarchy=org&statusView=pending';

        expect(model.url()).to.equal(urlExpected);
    });

    it('url should build correctly if producerId is not exist', function () {
        var model = new Model();

        var urlExpected = '/api/oso/secure/rest/policies/summaries/counts'+
            '?hierarchy=producer&statusView=pending';

        expect(model.url()).to.equal(urlExpected);
    });

    describe('model fetch', function () {
        var ajaxStub;
        var model;
        var responseBody = {
            reportingGroups : [ {
                group : 'PENDING',
                count : 29
            }, {
                group : 'PAID',
                count : 39
            }, {
                group : 'INACTIVE',
                count : 33
            }, {
                group : 'ALL',
                count : 114
          } ]
        };
        
        beforeEach (function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );

            model = new Model({
                producerId: 10001
            });
    
            model.fetch();
        });

        afterEach(function () {
            ajaxStub.restore();
        });

        it ('ALL should match', function () {
            expect (model.get('ALL')).to.equal(114);
        });

        it ('PENDING should match', function () {
            expect (model.get('PENDING')).to.equal(29);
        });

        it ('PAID should match', function () {
            expect (model.get('PAID')).to.equal(39);
        });

        it ('INACTIVE should match', function () {
            expect (model.get('INACTIVE')).to.equal(33);
        });

    });
});

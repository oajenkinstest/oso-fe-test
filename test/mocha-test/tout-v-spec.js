/* global expect:false, describe: false, Marionette, _*/

/**
 * Unit tests covering the tout view class
 */

var ToutView  = require('../dist/modules/tout/views/tout-v');

describe('Tout View (modules/tout/views/tout-v.js)', function () {
    var options;
    var rootView;
    var view;

    beforeEach(function () {
        rootView = new Marionette.LayoutView({
            template: _.template('<div id=\'pending-policies-tout\' ></div>'),
            regions: {
                mainRegion: '#pending-policies-tout'
            }
        });

        options = {
            iconCss : 'ace-icon fa fa-pencil brown',
            title   : 'My Tout',
            text    : 'Hello',
            href    : '#hiya'
        };

        rootView.render();
    });

    afterEach(function () {
        if (rootView) {
            rootView.destroy();
        }

        if (view) {
            view.destroy();
        }
    });

    it('exists', function () {
       expect(ToutView).to.exist;
    });

    it('should be rendered correctly', function () {
        view = new ToutView(options);
        rootView.mainRegion.show(view);
        expect(view.isRendered).to.equal(true);
    });

    it('should throw an error if an object is not passed to the constructor', function () {
        var fn = function () {
            view = new ToutView();
        };

        expect(fn).to.throw(ToutView.prototype.errors.objectRequired);
    });

});

/* global expect:false, describe: false, Backbone: false*/

/**
 * Unit tests covering the tout view class
 */

var RootLayoutView  = require('../dist/views/root-layout-v');

describe('Root Layout View (views/root-layout-v.js)', function () {
    var rootView;
    beforeEach(function () {
        rootView = new RootLayoutView();

        rootView.render();
    });

    afterEach(function () {
        if (rootView) {
            rootView.destroy();
        }
    });

    it('exists', function () {
       expect(rootView).to.exist;
    });

    it('regions should exist', function () {
        expect(rootView.regionManager._regions.topBarRegion).to.exist;
        expect(rootView.regionManager._regions.contentRegion).to.exist;
        expect(rootView.regionManager._regions.footerRegion).to.exist;
    });

    describe('logAction method', function () {
        it('to be a function', function () {
            expect(rootView.logAction).to.exist.and.be.a('function');
        });

        it('should call "trackAction" analytics method', function () {
            var analyticsChannel = Backbone.Radio.channel('analytics');
            var trackActionStub = this.sinon.stub();
            analyticsChannel.on('trackAction', trackActionStub);

            rootView.logAction({currentTarget: {
                hash: '#/policy-details',
                tagName: 'A',
                textContent: 'Policy Details'
            }});

            expect(trackActionStub).to.have.been.called;

            analyticsChannel.off('trackAction', trackActionStub);
            trackActionStub = null;
        });
    });

});

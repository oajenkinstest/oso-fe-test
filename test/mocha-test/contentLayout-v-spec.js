/* global expect:false, Backbone:false, Marionette:false, _:false, $:false */

// load behaviors
require('../dist/modules/behaviors/index');

var ContentLayout = require('../dist/modules/content/contentLayout-v');
var helpers       = require('./helpers/helpers');

var littleTestView = Marionette.ItemView.extend({
    template: _.template('<p id="littleTestView">Test</p>' +
        '<a class="oa-js-nav" href="#foo">Internal link</a>'+
        '<a class="not-nav" href="#bar">Another Internal Link</a>')
});
var toolsTestView = Marionette.ItemView.extend({
    template: _.template('<p id="toolsTestView">Tools</p>')
});

var validOptions = {
    pageLookup : {
        home         : {
            icon        : 'home',
            displayText : 'Home',
            view        : littleTestView,
            capability  : 'all'

        },
        'producer-delegate-list' : {
            icon        : 'producer-delegate-list',
            displayText : 'Producer Delegate List',
            view        : littleTestView,
            capability  : 'all'

        },
        'contact-us' : {
            icon        : 'desktop',
            displayText : 'Contact Us',
            view        : littleTestView,
            capability  : 'all'
        },
        'billing-payments' : {
            displayText : 'Billing and Payments',
            view        : littleTestView,
            capability  : 'billing'
        },
        tools : {
            icon        : 'tachometer',
            displayText : 'Tools',
            capability  : 'user.tools',
            view        : toolsTestView
        }
    }
};

describe('ContentLayout Module (modules/content/contentLayout-v.js)', function () {

    it('exists', function () {
        expect(ContentLayout).to.exist;
    });

    it('can be instantiated', function () {
        var cl = new ContentLayout(validOptions);
        expect(cl).not.to.be.undefined
            .and.not.to.throw;

        cl = null;
    });

    it('has a childPageView property', function () {
        var cl = new ContentLayout(validOptions);
        expect(cl.childPageView).to.be.defined;

        cl = null;
    });

    it('contructor requires a pageLookup object', function () {
        var cl; // eslint-disable-line no-unused-vars
        var fn = function () {
            var options = {
                el : '#contentContainer'
            };
            cl = new ContentLayout(options);
        };
        expect(fn).to.throw(ContentLayout.prototype.errors.pageLookup);

        cl = null;
    });

    it('template exists', function () {
        var cl = new ContentLayout(validOptions);
        expect(cl.template).not.equal(false);

        cl = null;
    });

    it('creates a "content" region', function () {
        var cl = new ContentLayout(validOptions);

        expect(cl.getRegion('content')).to.be.defined;
    });

    it('adds #contentView div to DOM', function () {
        var cl;
        cl = new ContentLayout(validOptions);
        var testRegion = new Marionette.Region({
            el: document.createElement('div')
        });
        testRegion.show(cl);

        expect(cl.$('#contentView')).not.to.be.null;

        testRegion.reset();

        testRegion = null;
        cl = null;
    });


    describe('showBanner method', function () {
        var cl;

        beforeEach(function () {
            $('body').append('<div id="contentLayout"></div>');
            cl = new ContentLayout(validOptions);
            cl.render();
            helpers.viewHelpers.startBBhistory();
        });

        afterEach(function () {
            $('#contentLayout').remove();
            cl.destroy();
            cl = null;
            helpers.viewHelpers.stopBBhistory();
        });

        it('sets "bannerModule" property', function () {
            var bannerModuleBeforeCall = cl.bannerModule;
            var bannerModuleAfterCall;

            cl.showBanner();
            bannerModuleAfterCall = cl.bannerModule;

            expect(bannerModuleBeforeCall).to.be.undefined;
            expect(bannerModuleAfterCall).to.be.defined;
        });

        describe ('sets "collapseBanner" option in bannerModule property', function () {
            it(' Should be "false" if fragment is "#home"', function () {
                Backbone.history.navigate('home', {trigger: true});

                cl.showBanner();
                expect(cl.bannerModule.options.collapseBanner).to.be.false;
            });

            it('Should be "false" if fragment is "#producer-delegate-list"', function () {
                Backbone.history.navigate('producer-delegate-list', {trigger: true});

                cl.showBanner();
                expect(cl.bannerModule.options.collapseBanner).to.be.false;
            });

            it('Should be "true" fragment is "#search" or other', function () {
                Backbone.history.navigate('search', {trigger: true});

                cl.showBanner();
                expect(cl.bannerModule.options.collapseBanner).to.be.true;
            }); 
        }); //sets "collapseBanner" option in bannerModule property
    }); // showBanner method


    describe('showPage method', function () {
        var cl;

        beforeEach(function () {
            $('body').append('<div id="contentLayout"></div>');
            cl = new ContentLayout(validOptions);
            cl.render();
        });

        afterEach(function () {
            $('#contentLayout').remove();
            cl.destroy();
            cl = null;
        });

        it('exists as a function', function () {
            expect(cl.showPage).to.be.a('function');
        });

        it('requires a pageId parameter', function () {
            var fn = function () {
                cl.showPage();
            };

            expect(fn)
                .to.throw('ContentLayout.showPage requires a pageId (string) parameter');
        });

        it('returns a reference to the child page view', function () {
            var currPageView = cl.showPage('tools');
            expect(currPageView).to.be.an.instanceof(Marionette.View);
        });

        it('renders the page into the DOM', function () {
            cl.showPage('tools');

            expect(cl.$el.find('#toolsTestView').text()).to.equal('Tools');
        });

        it('saves a reference to the child view', function () {
            expect(cl.childPageView).to.be.null;
            cl.showPage('tools');
            expect(cl.childPageView).not.to.be.null;
        });

        it('repeated calls render the proper pages into the DOM', function () {
            cl.showPage('tools');
            cl.showPage('contact-us');

            expect(cl.$el.find('#littleTestView').text()).to.equal('Test');
        });

        it('showing a new page properly destroys the current page', function () {
            // show a view
            var firstView = cl.showPage('tools');
            var destroyHandler = this.sinon.stub();
            firstView.on('destroy', destroyHandler);

            // show a second view
            cl.showPage('contact-us');

            expect(destroyHandler).to.have.been.calledOnce;
        });

        describe ('trigger alert banner module\'s "collapseBanner" method', function () {
            var listener;
            var collapseBannerStub;
            beforeEach (function () {
                listener = new Marionette.Object();
                collapseBannerStub = this.sinon.stub();
                cl.bannerModule = new Marionette.Object();
                listener.listenTo(cl.bannerModule, 'collapseBanner', collapseBannerStub);
            });

            afterEach (function () {
                listener.stopListening();
                listener.destroy();
            });
            it('pageId: #home - Parameter should be False', function () {
                cl.showPage('#home');
                expect(collapseBannerStub).to.have.been.calledWith(false);
            });

            it('pageId: #producer-delegate-list - Parameter should be False', function () {
                cl.showPage('#producer-delegate-list');
                expect(collapseBannerStub).to.have.been.calledWith(false);
            });

            it('for other pageIds parameter should be True', function () {
                cl.showPage('#contact-us');
                expect(collapseBannerStub).to.have.been.calledWith(true);

                cl.showPage('#billing-payments');
                expect(collapseBannerStub).to.have.been.calledWith(true);
            });
        }); // trigger alert banner modules "collapseBanner" method

    }); // showPage method


    describe('_instantiateChildViewForId method', function () {
        var cl;

        beforeEach(function () {
            cl = new ContentLayout(validOptions);
        });

        afterEach(function () {
            cl = null;
        });

        it('exists as a function', function () {
            expect(cl._instantiateChildViewForId).to.be.a('function');
        });

        it('throws an error if pageId is not present in appStructure', function () {
            var fn = function () {
                cl._instantiateChildViewForId('bogus pageId');
            };

            expect(fn)
                .to.throw('ContentLayout._instantiateChildViewForId ' +
                    'received an unknown pageId "bogus pageId"');
        });

        it('returns a reference to the child page view', function () {
            var currPageView = cl._instantiateChildViewForId('tools');
            expect(currPageView).to.be.an.instanceof(Marionette.View);
        });

        it('ignores a leading # character on the pageId', function () {
            expect(cl._instantiateChildViewForId('#contact-us').pageId)
                .to.equal('contact-us');
        });

    }); // _instantiateChildViewForId method


    describe('_handleNavAnchorClicks method', function () {
        var cl;
        var testRegion;

        beforeEach(function () {
            testRegion = new Marionette.Region({
                el: document.createElement('div')
            });
            cl = new ContentLayout(validOptions);
            testRegion.show(cl);
        });

        afterEach(function () {
            cl.destroy();
            cl = null;
            testRegion.reset();
            testRegion = null;
        });

        it('exists as a function', function () {
            expect(cl._handleNavAnchorClicks).to.be.a('function');
        });

        it('executes on <a class="oa-js-nav"> clicks within child views', function () {
            var spy = this.sinon.spy(ContentLayout.prototype, '_handleNavAnchorClicks');
            var myCl = new ContentLayout(validOptions);

            testRegion.show(myCl);
            myCl.showPage('contact-us');
            myCl.$el.find('a.oa-js-nav').click();

            expect(spy).to.have.been.calledOnce;

            ContentLayout.prototype._handleNavAnchorClicks.restore();
        });

        it('stops navigation link clicks from changing the browser\'s URL', function () {
            var startingURL = document.location.toString();

            cl.showPage('contact-us');
            cl.$el.find('a.oa-js-nav').click();

            expect(document.location.toString()).to.equal(startingURL);
        });

        it('does not execute on <a> tags without the "oa-js-nav" class', function() {
            var spy = this.sinon.spy(ContentLayout.prototype, '_handleNavAnchorClicks');
            var myCl = new ContentLayout(validOptions);

            testRegion.show(myCl);
            myCl.showPage('contact-us');
            myCl.$el.find('a.not-nav').click();

            expect(spy).to.have.not.been.called;

            ContentLayout.prototype._handleNavAnchorClicks.restore();
        });

    }); // _handleNavAnchorClicks method


    describe('_trimHash method', function () {
        var cl;

        beforeEach(function () {
            $('body').append('<div id="contentLayout"></div>');
            cl = new ContentLayout(validOptions);
            cl.render();
        });

        afterEach(function () {
            cl.destroy();
            cl = null;
            $('#contentLayout').remove();
        });

        it('exists as a function', function () {
            expect(cl._trimHash).to.be.a('function');
        });

        it('returns pageId without leading hash character', function () {
            var pageId = cl._trimHash('#home');
            expect(pageId).to.equal('home');
        });

    }); // _trimHash method

    describe('ribbon methods', function() {

        var cl;

        beforeEach(function () {
            $('body').append('<div id="contentLayout"></div>');
            cl = new ContentLayout(validOptions);
            cl.render();
        });

        afterEach(function () {
            cl.destroy();
            cl = null;
            $('#contentLayout').remove();
        });

        it('showRibbon exists as a function', function() {
            expect(cl.showRibbon).to.exist.and.be.a('function');
        });

        it('removeRibbon exists as a function', function() {
            expect(cl.removeRibbon).to.exist.and.be.a('function');
        });

        it('showRibbon throws an error if now view is passed in', function() {
            var fn = function() {
                cl.showRibbon();
            };
            expect(fn).to.throw(cl.errors.viewRequired);
        });

        it('showRibbon shows the specified view in the ribbonView region', function() {
            var simpleView = new Marionette.ItemView({
                template : _.template('<div id="simpleViewDiv">DIV</div>')
            });

            var div = cl.$el.find('#ribbonView #simpleViewDiv');
            expect(div).to.have.length(0);

            cl.showRibbon(simpleView);
            div = cl.$el.find('#ribbonView #simpleViewDiv');
            expect(div).to.have.length(1);
        });

        it('removeRibbon does not throw an error if the region is empty', function() {
            var fn = function() {
                cl.removeRibbon();
            };
            expect(fn).to.not.throw(Error);
        });

        it('removeRibbon removes the view in the ribbonView region', function() {
            var simpleView = new Marionette.ItemView({
                template : _.template('<div id="simpleViewDiv">DIV</div>')
            });

            cl.showRibbon(simpleView);
            var div = cl.$el.find('#ribbonView #simpleViewDiv');
            expect(div).to.have.length(1);

            cl.removeRibbon();
            div = cl.$el.find('#ribbonView #simpleViewDiv');
            expect(div).to.have.length(0);
        });
    }); // ribbon methods

}); // describe ContentLayout

/* global expect:false, $:false, isCanvasLoaded:false, sinon:false */
var helpers = require('./helpers/helpers');


var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);

describe('Performance Center View : Leaders Conference(LC) '+
    '(modules/performance-center/views/performance-center-v.js)', function () {

    var ajaxStub;
    var copyOfResponseBody;
    var copyOfResponseBodyLCCA;
    var root;
    var view;

    var buildView = function(pcData) {
        var model;
        var pcView;
        root = helpers.viewHelpers.createRootView();
        if (ajaxStub) {
            ajaxStub.restore();
        }
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            pcData
        );

        model = new PerformanceCenterModel();
        pcView  = new PerformanceCenterView({
            model : model
        });

        root.render();
        root.contentRegion.show(pcView);
        return pcView;
    };

    before(function (){
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
    });

    after(function (){
        ajaxStub.restore();
        root.destroy();
    });

    beforeEach(function () {
        copyOfResponseBody      = $.extend(true,{},helpers.pcData.responseBody);
        copyOfResponseBodyLCCA  = $.extend(true,{},helpers.pcData.responseBodyLCCA);
    });

    describe('header', function () {
        it('CA producer should see "Leaders\' Conference 2019"', function () {
            view = buildView(copyOfResponseBody);
            var headerText = helpers.viewHelpers.removeDuplicateWhitespace(
                view.leadersConferenceRegion.$el.find('h4:eq(0)').text().trim()
            );

            expect(headerText).to.equal('Leaders\' Conference 2019');
        });

        it('IB producer should see "Leaders\' Conference 2018"', function () {

            view = buildView(helpers.pcData.responseBodyIB);
            var headerText = helpers.viewHelpers.removeDuplicateWhitespace(
                view.leadersConferenceRegion.$el.find('h4:eq(0)').text().trim()
            );

            expect(headerText).to.equal('Leaders\' Conference 2018');
        });
    });

    describe('Modal window header', function () {
        it('CA producer should see "Leaders\' Conference 2019"', function () {
            view = buildView(copyOfResponseBody);
            var headerText = helpers.viewHelpers.removeDuplicateWhitespace(
                view.leadersConferenceRegion.$el.find('h4:eq(1)').text().trim()
            );

            expect(headerText).to.equal('Leaders\' Conference 2019');
        });

        it('IB producer should see "Leaders\' Conference 2018"', function () {
            view = buildView(helpers.pcData.responseBodyIB);
            var headerText = helpers.viewHelpers.removeDuplicateWhitespace(
                view.leadersConferenceRegion.$el.find('h4:eq(1)').text().trim()
            );

            expect(headerText).to.equal('Leaders\' Conference 2018');
        });
    });

    describe('Links in Header', function () {

        it('CA should have link to display "Qualifying FYC"', function () {
            view = buildView(copyOfResponseBody);
            var qualifyingLink = view.leadersConferenceRegion.$el
                .find('a[href="#lc-qualifying"]');

            expect(qualifyingLink.length).to.equal(1);
            expect(qualifyingLink.text().trim()).to.equal('Qualifying FYC');
        });

        it('CA should have link to display "Total FYC"', function () {
            view = buildView(copyOfResponseBody);
            var totalLink = view.leadersConferenceRegion.$el.find('a[href="#lc-total"]');

            expect(totalLink.length).to.equal(1);
            expect(totalLink.text().trim()).to.equal('Total FYC');
        });

        it('IB should NOT have link to display "Qualifying FYC"', function () {
            copyOfResponseBody.leadersConference.type = 'ib';
            view = buildView(copyOfResponseBody);

            var qualifyingLink = view.leadersConferenceRegion.$el
                .find('a[href="#lc-qualifying"]');

            expect(qualifyingLink.length).to.equal(0);
        });

        it('IB should NOT have link to display "Total FYC"', function () {
            copyOfResponseBody.leadersConference.type = 'ib';
            view = buildView(copyOfResponseBody);

            var qualifyingLink = view.leadersConferenceRegion.$el.find('a[href="#lc-total"]');

            expect(qualifyingLink.length).to.equal(0);
        });
    });

    describe('"Qualifying FYC" charts', function () {

        it('all values should be formatted correctly', function () {
            view = buildView(copyOfResponseBody);

            expect(view.leadersConferenceRegion.$el.find('.chart-outer').data('percent'))
                .to.equal(54588);
            expect(view.leadersConferenceRegion.$el.find('.chart-inner').data('percent'))
                .to.equal(5);

            expect(view.leadersConferenceRegion.$el.find('.chart-outer div:eq(0)').text()
                .trim()).to.equal('$54,588');

            expect(view.leadersConferenceRegion.$el.find('.chart-outer div:eq(1)')
                .text().trim()).to.equal('5 lives');

            expect(view.leadersConferenceRegion.$el
                .find('#leadersConference-required-remaining-fypc').text().trim())
                .to.equal('$128,000');
            expect(view.leadersConferenceRegion.$el.find('#leadersConference-required-fypc')
                .text().trim()).to.equal('$128,000');
            expect(view.leadersConferenceRegion.$el
                .find('#leadersConference-required-remaining-lives').text().trim())
                .to.equal('10');
            expect(view.leadersConferenceRegion.$el.find('#leadersConference-required-lives')
                .text().trim()).to.equal('10');
        });

        describe('dail', function () {
            var lcRegion;
            it ('checkmark should appeared if qualifyingGoalAchievedFlag is true', function () {
                view = buildView(copyOfResponseBodyLCCA);
                lcRegion = view.leadersConferenceRegion;
                expect(lcRegion.$el.find('#leadersConference-qualified .fa-check'))
                    .to.be.lengthOf(1);
            });
    
            it ('checkmark should not appeared even qualifyingGoalAchievedFlag is true '+
                    'while APR status is not "FQ" or "NR"', function () {
                copyOfResponseBodyLCCA.leadersConference.leadersAprStatus = 'OS';
                view = buildView(copyOfResponseBodyLCCA);
                lcRegion = view.leadersConferenceRegion;
                expect(lcRegion.$el.find('#leadersConference-qualified .fa-check'))
                    .to.be.lengthOf(0);
            });

            it('dial color should be green when remaining value(qualifying '+
                    'fypc or lives) is zero',function(){
                copyOfResponseBodyLCCA.leadersConference.leadersAprStatus = 'OS';
                view = buildView(copyOfResponseBodyLCCA);
                var lcView = view.leadersConferenceRegion.currentView;
                expect(lcView.dialChartOptionsOne.innerChartOptions.barColor)
                    .to.be.equal(lcView.dialColorComplete);
            });
        });       
    });

    describe ('Retail (CA) - Qualifying FYC bullets', function () {

        describe ('Qualified', function (){
            describe ('Leaders APR status bullet', function () {
                
                it ('APR status message - FQ ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'FQ';
                    view = buildView(copyOfResponseBody);
                    var aprStatusTextEl = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-qualified .leaders-apr-status-text');
                    expect(aprStatusTextEl.find('span').hasClass('oa-forest'))
                        .to.be.true;

                    expect(aprStatusTextEl.text()).to.contain('APR Goal Achieved!');
                });
                
                it ('APR status message - OS ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'OS';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-qualified .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR on schedule');
                });

                it ('APR status message - NMMR ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'NMMR';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-qualified .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR not met');
                });

                it ('APR status message - NR ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'NR';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-qualified .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR not required');
                });

                it('Should not displayed of IB users', function () {
                    copyOfResponseBody.leadersConference.type = 'ib';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-qualified .leaders-apr-status-text');

                    expect(aprStatusText).to.have.lengthOf(0);
                });
            });
        });

        describe ('qualifying FYC - On Schedule', function (){

            describe ('chart', function () {
                it ('checkmark should appeared if qualifyingGoalAchievedFlag is true', function () {
                    view = buildView(copyOfResponseBodyLCCA);
                    expect(view.leadersConferenceRegion.$el
                        .find('#leadersConference-on-schedule .fa-check')).to.be.lengthOf(1);
                });
            });

            describe ('leaders APR status bullet', function () {
                it ('APR status message - FQ ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'FQ';
                    view = buildView(copyOfResponseBody);
                    var aprStatusTextEl = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-on-schedule .leaders-apr-status-text');
                    
                    expect(aprStatusTextEl.find('span').hasClass('oa-forest'))
                        .to.be.true;

                    expect(aprStatusTextEl.text()).to.contain('APR Goal Achieved!');
                });
                
                it ('APR status message - OS ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'OS';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region #lc-qualifying '+
                        '#leadersConference-on-schedule .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR On Schedule!');
                });

                it ('APR status message - NMMR ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'NMMR';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-on-schedule .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR not met');
                });

                it ('APR status message - NR ', function (){
                    copyOfResponseBody.leadersConference.leadersAprStatus = 'NR';
                    view = buildView(copyOfResponseBody);
                    var aprStatusText = view.$el.find('#leadersConference-region '+
                        '#lc-qualifying #leadersConference-on-schedule .leaders-apr-status-text')
                        .text();

                    expect(aprStatusText).to.contain('APR not required');
                });
            });
        });
    });

    describe ('Retail (CA) Total FYC bullets', function () {
        describe ('Qualified', function (){
            it ('APR status message - FQ ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'FQ';
                view = buildView(copyOfResponseBody);
                var aprStatusTextEl = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-qualified .leaders-apr-status-text');

                expect(aprStatusTextEl.find('span').hasClass('oa-forest'))
                    .to.be.true;
                expect(aprStatusTextEl.text()).to.contain('APR Goal Achieved!');
            });
            
            it ('APR status message - OS ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'OS';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-qualified .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR on schedule');
            });

            it ('APR status message - NMMR ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'NMMR';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-qualified .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR not met');
            });

            it ('APR status message - NR ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'NR';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-qualified .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR not required');
            });
        });
        
        describe ('On Schedule', function (){
            it ('APR status message - FQ ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'FQ';
                view = buildView(copyOfResponseBody);
                var aprStatusTextEl = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-on-schedule .leaders-apr-status-text');
                
                expect(aprStatusTextEl.find('span').hasClass('oa-forest'))
                    .to.be.true;

                expect(aprStatusTextEl.text()).to.contain('APR Goal Achieved!');
            });
            
            it ('APR status message - OS ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'OS';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-on-schedule .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR On Schedule!');
            });

            it ('APR status message - NMMR ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'NMMR';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-on-schedule .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR not met');
            });

            it ('APR status message - NR ', function (){
                copyOfResponseBody.leadersConference.leadersAprStatus = 'NR';
                view = buildView(copyOfResponseBody);
                var aprStatusText = view.$el.find('#leadersConference-region '+
                    '#lc-total #total-on-schedule .leaders-apr-status-text')
                    .text();

                expect(aprStatusText).to.contain('APR not required');
            });
        });
    });

    describe('"Total FYC" charts', function () {

        it('toggle buttons should exist in panel', function () {
            view = buildView(copyOfResponseBody);

            var buttons = view.leadersConferenceRegion.$el.find('#lc-total ul.nav-tabs');

            expect(buttons.find('a:eq(0)').text().trim()).to.equal('Am I Qualified?');
            expect(buttons.find('a:eq(1)').text().trim()).to.equal('Am I On Schedule?');
        });

        describe('Total Qualified', function () {

            it('all values should be formatted correctly', function () {
                var firstLi;
                var firstLiText;
                var secondLi;
                var secondLiText;
                var totalQual;
                view = buildView(copyOfResponseBody);

                totalQual = view.leadersConferenceRegion.$el.find('#total-qualified');
                firstLi = totalQual.find('#total-qualified-fypc-goal-text');
                secondLi = firstLi.next('li');
                firstLiText = helpers.viewHelpers.removeDuplicateWhitespace(
                    firstLi.text().trim()
                );
                secondLiText = helpers.viewHelpers.removeDuplicateWhitespace(
                    secondLi.text().trim()
                );

                expect(totalQual.find('.chart-outer').data('percent')).to.equal(74268);

                expect(firstLiText)
                    .to.equal('$113,232 Total FYC needed to reach goal of $187,500');
                expect(secondLiText)
                    .to.equal('$74,268 actual Total FYC');
            });

            it('Comparison bullet (1st) should read "Qualifying FYC Goal Achieved!" ' +
                'if "requiredRemainingTotalFyc" is 0', function () {

                var firstBulletText;

                copyOfResponseBody.leadersConference.totalGoalAchievedFlag = true;
                copyOfResponseBody.leadersConference.requiredRemainingTotalFyc = 'USD 0.00';
                view = buildView(copyOfResponseBody);

                firstBulletText = helpers.viewHelpers.removeDuplicateWhitespace(
                    view.leadersConferenceRegion.$el.find('#total-qualified-fypc-goal-text')
                        .text().trim()
                );

                expect(firstBulletText).to.equal('Qualifying FYC Goal Achieved!');
            });

            it('Actual Qualifying FYC bullet should display amount in '+
                    'red if "actualTotalFyc" is less than 0', function () {

                copyOfResponseBody.leadersConference.actualTotalFyc = 'USD -17000.00';
                view = buildView(copyOfResponseBody);

                var secondLiSpan = view.leadersConferenceRegion.$el
                    .find('#total-qualified-actual-fypc');

                expect(secondLiSpan.hasClass('oa-banner-red')).to.be.true;
            });

            it ('checkmark should appeared if totalGoalAchievedFlag is true', function () {
                view = buildView(copyOfResponseBodyLCCA);
                expect(view.leadersConferenceRegion.$el
                    .find('#total-qualified .fa-check')).to.be.lengthOf(1);
            });
        }); // Total Qualified

        describe('Total On-Schedule', function () {
            var totalQual;

            it('all values should be formatted correctly', function () {
                var firstLi;
                var firstLiText;
                var secondLi;
                var secondLiText;

                view = buildView(copyOfResponseBody);

                totalQual = view.leadersConferenceRegion.$el.find('#total-on-schedule');
                firstLi = totalQual.find('#total-on-schedule-fypc-goal-text');
                secondLi = firstLi.next('li');
                firstLiText = helpers.viewHelpers.removeDuplicateWhitespace(
                    firstLi.text().trim()
                );
                secondLiText = helpers.viewHelpers.removeDuplicateWhitespace(
                    secondLi.text().trim()
                );

                expect(totalQual.find('.chart-outer').data('percent')).to.equal(74268);

                expect(firstLiText)
                    .to.equal('$113,232 Total FYC needed to reach goal of $171,875');
                expect(secondLiText)
                    .to.equal('$74,268 actual Total FYC');
            });

            describe('When "onscheduleRemainingTotalFyc" is 0', function () {

                it('Comparison bullet (1st) should read '+
                        '"Qualifying FYC On Schedule!"', function () {
                    var firstBulletText;

                    copyOfResponseBody.leadersConference
                        .onscheduleRemainingTotalFyc = 'USD 0.00';
                    view = buildView(copyOfResponseBody);

                    firstBulletText = helpers.viewHelpers.removeDuplicateWhitespace(
                        view.leadersConferenceRegion.$el
                            .find('#total-on-schedule-fypc-goal-text').text().trim()
                    );

                    expect(firstBulletText).to.equal('Qualifying FYC On Schedule!');
                });

                it('first bullet should display as green text', function () {
                    var firstBulletSpan;

                    copyOfResponseBody.leadersConference
                        .onscheduleRemainingTotalFyc = 'USD 0.00';
                    view = buildView(copyOfResponseBody);

                    firstBulletSpan = view.leadersConferenceRegion.$el
                        .find('#total-on-schedule-fypc-goal-text span');

                    expect(firstBulletSpan.hasClass('oa-forest')).to.be.true;
                });

                it('inner text should display with a green color', function () {
                    var chartTextOne;

                    copyOfResponseBody.leadersConference
                        .onscheduleRemainingTotalFyc = 'USD 0.00';
                    view = buildView(copyOfResponseBody);

                    chartTextOne = view.leadersConferenceRegion.$el
                        .find('#total-on-schedule .chart-text-one');

                    expect(chartTextOne.hasClass('oa-forest')).to.be.true;
                });

                it('inner label should display with a green color', function () {
                    var chartTextTwo;

                    copyOfResponseBody.leadersConference
                        .onscheduleRemainingTotalFyc = 'USD 0.00';
                    view = buildView(copyOfResponseBody);

                    chartTextTwo = view.leadersConferenceRegion.$el
                        .find('#total-on-schedule .chart-text-two');

                    expect(chartTextTwo.hasClass('oa-forest')).to.be.true;
                });

                it ('checkmark should appeared if totalGoalAchievedFlag is true', function () {
                    view = buildView(copyOfResponseBodyLCCA);
                    expect(view.leadersConferenceRegion.$el
                        .find('#total-on-schedule .fa-check')).to.be.lengthOf(1);
                });

            });
        }); // Total On-Schedule
    });  // Total FYPC Charts

});


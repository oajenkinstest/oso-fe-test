/* global describe:false, it:false, expect:false, $:false, Backbone:false, sinon:false */

var config             = require('../dist/config/config');
var ProducerRolesModel = require('../dist/models/producerRoles-m');

describe('Producer Roles Model (src/js/models/producerRoles-m.js)', function () {

    var rolesResponse = {
        'id'    : 144586,
        'roles' : [
            {
                'id'                  : 655455,
                'roleCode'            : '000000008728',
                'reporting'           : false,
                'carrierDivision'     : 'ILDIV',
                'carrierCode'         : 'PML',
                'statusCode'          : 'T',
                'distributionChannel' : 'Retail General Agency',
                'contact'             : {
                    'email'       : 'HOME.SIMPSON@SPRINGFIELDNUCLEAR.COM',
                    'post'        : {
                        'city'          : 'SPRINGFIELD',
                        'stateProvince' : 'IL',
                        'postalCode'    : '62629',
                        'country'       : 'US',
                        'lines'         : [
                            'SPRINGFIELD NUCLEAR POWER PLANT',
                            '100 INDUSTRIAL WAY'
                        ]
                    },
                    'phoneNumber' : {
                        'work' : '555-555-7600',
                        'fax'  : '555-555-7601'
                    }
                }
            },
            {
                'id'                  : 655456,
                'roleCode'            : '01027',
                'reporting'           : true,
                'carrierDivision'     : 'ILDIV',
                'carrierCode'         : 'AUL',
                'statusCode'          : 'A',
                'distributionChannel' : 'Retail General Agency',
                'contact'             : {
                    'email'       : 'HOMER.SIMPSON@SPRINGFIELDNUCLEAR.COM',
                    'post'        : {
                        'city'          : 'SPRINGFIELD',
                        'stateProvince' : 'IL',
                        'postalCode'    : '62629',
                        'country'       : 'US',
                        'lines'         : [
                            'SPRINGFIELD NUCLEAR POWER PLANT',
                            '100 INDUSTRIAL WAY'
                        ]
                    },
                    'phoneNumber' : {
                        'work' : '555-555-7600',
                        'fax'  : '555-555-7601'
                    }
                }
            }
        ]
    };

    describe('initialization method', function () {

        it('will throw an error if options are not passed', function () {
            var fn = function () {
                new ProducerRolesModel();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(ProducerRolesModel.prototype.errors.missingProducerId);
        });

        it('will throw an error if options are not passed as an object', function () {
            var fn = function () {
                new ProducerRolesModel(3);    // eslint-disable-line no-new
            };

            expect(fn).to.throw(ProducerRolesModel.prototype.errors.missingProducerId);
        });

        it('will throw an error if options does not contain a "producerId" key', function () {
            var options = {notProducerId : true};
            var fn      = function () {
                new ProducerRolesModel(options);    // eslint-disable-line no-new
            };

            expect(fn).to.throw(ProducerRolesModel.prototype.errors.missingProducerId);
        });

        it('will throw an error if options contains a non-numeric "producerId" key', function () {
            var options = {producerId : 'I am not numeric. :('};
            var fn      = function () {
                new ProducerRolesModel(options);    // eslint-disable-line no-new
            };

            expect(fn).to.throw(ProducerRolesModel.prototype.errors.missingProducerId);
        });

        it('will not throw an error if options.producerId passed is numeric', function () {
            var options = {producerId : 98736};
            var fn      = function () {
                new ProducerRolesModel(options);    // eslint-disable-line no-new
            };

            expect(fn).not.to.throw(Error);
        });

    }); // initialization method

    describe('parse method', function () {

        it('will return undefined if the response parameter does not exist', function () {
            var model  = new ProducerRolesModel({ producerId : 8726 });
            var result = model.parse();

            expect(result).to.be.undefined;
        });

        it('will find the role with the "reporting" key set to TRUE and ' +
            'set that as the "activeRole" key', function () {
            var model  = new ProducerRolesModel({ producerId : 8726 });
            var result = model.parse(rolesResponse);

            expect(result.activeRole).to.exist;
        });

        it('will return the original response unmodified if none of the roles have a ' +
            '"reporting" value which is TRUE', function () {
            var result;
            var model             = new ProducerRolesModel({ producerId : 87265 });
            var rolesResponseCopy = $.extend(true, {}, rolesResponse);

            rolesResponseCopy.roles.forEach(function(role) {
                role.reporting = false;
            });

            result = model.parse(rolesResponseCopy);

            expect(result.activeRole).to.be.undefined;
            expect(result).to.deep.equal(rolesResponseCopy);
        });

        it('will return the original response unmodified if it has a "roles" ' +
            'attribute which is not an array', function () {
            var model             = new ProducerRolesModel({ producerId : 8726 });
            var rolesResponseCopy = $.extend(true, {}, rolesResponse);
            var result;

            rolesResponseCopy.roles = 'Four score and seven years ago...';

            result = model.parse(rolesResponseCopy);

            expect(result.activeRole).to.be.undefined;
            expect(result).to.deep.equal(rolesResponseCopy);
        });
    }); // parse method

    describe('urlRoot method', function () {

        it('should construct the correct URI for the endpoint if ' +
            '"producerId" attribute exists', function () {
            var options  = { producerId : 8736 };
            var expected = config.apiUrlRoot + 'producers/' + options.producerId + '/roles';
            var model    = new ProducerRolesModel(options);

            expect(model.urlRoot()).to.equal(expected);
        });

        it('should throw an error if "producerId" is not set for some reason', function () {
            var fn;
            var options = { producerId : 876 };
            var model   = new ProducerRolesModel(options);

            model.producerId = null;
            fn = function () {
                model.urlRoot();
            };

            expect(fn).to.throw(model.errors.missingProducerId);
        });

    }); // urlRoot method

    describe('_handleFetchError method', function () {
        var analyticsChannel;
        var trackExceptionSpy;
        var message;
        var model;
        var producerId   = 872651;
        var fakeResponse = { status : 500 };

        before(function () {
            if (!this.sinon) {
                this.sinon = sinon;
            }
            analyticsChannel  = Backbone.Radio.channel('analytics');
            trackExceptionSpy = this.sinon.spy();

            analyticsChannel.on('trackException', trackExceptionSpy);

            model = new ProducerRolesModel({ producerId : producerId });

            model._handleFetchError(model, fakeResponse);

            message = trackExceptionSpy.getCalls()[0].args[0].message;
        });

        after(function () {
            analyticsChannel.stopListening();
            trackExceptionSpy = analyticsChannel = model = null;
        });

        it('should call "trackException" on analyticsChannel', function () {
            expect(trackExceptionSpy).to.have.been.called;
        });

        it('should pass the producerId value in the message sent to analytics', function () {
            expect(message).to.contain(producerId);
        });

        it('should pass the HTTP status in the message sent to analytics', function () {
            expect(message).to.contain(fakeResponse.status);
        });

    }); // _handleFetchError method

});

/* global expect:false, $:false, isCanvasLoaded:false, sinon:false */

var helpers = require('./helpers/helpers');

var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);

describe('Performance Center View : Information Box Matrix '+
    '(modules/performance-center/views/performance-center-v.js)', function () {

    var responseBody   = helpers.pcData.responseBody;
    var responseBodyIB = helpers.pcData.responseBodyIB;

    var rootView;
    var model;
    var view;
    var ajaxStub;
    before(function () {

        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }

        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
    
        rootView = helpers.viewHelpers.createRootView();
        rootView.render();
    });

    after(function () { 
        rootView.destroy();
    });

    describe ('retail', function () {
        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBody
            );
    
            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });
            rootView.contentRegion.show(view);
        });

        after(function () {
            ajaxStub.restore();
            view.destroy();
        });
        it('performance center view should rendered correctly', function () {
            expect(view.isRendered).to.equal(true);
        });

        it('should only contain 2 column', function (){
            expect(view.$el.find('.row:first .col-sm-4')).to.be.lengthOf(2);
        });

        describe ('should match',function () {
            it('First Year Commission (FYC) region labels', function () {
                expect(view.fycRegion.$el.find('h4').text()).to.contain('Year To Date');
                expect(view.fycRegion.$el.find('h4').text())
                    .to.contain('First Year Commission (FYC)');
            });
    
            it('First Year Commission (FYC) region data value', function () {
                expect(view.fycRegion.$el.find('.year-to-date').text()).to.contain('$49,844');
            });
    
            it('Qualifying FYC region labels', function () {
                expect(view.qualifyingFycRegion.$el.find('h4').text()).to.contain('Year To Date');
                expect(view.qualifyingFycRegion.$el.find('h4').text())
                    .to.contain('Qualifying FYC');
            });
    
            it('Qualifying FYC region data value', function () {
                expect(view.qualifyingFycRegion.$el.find('.year-to-date')
                    .text()).to.contain('$25,434');
            });
        });
    });

    describe('independent broker (ib)', function () {
        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                responseBodyIB
            );

            model = new PerformanceCenterModel();
            view = new PerformanceCenterView({
                model: model
            });

            rootView.contentRegion.show(view);
        });

        after(function () {
            ajaxStub.restore();
            view.destroy();
        });

        it('should contain 3 column', function (){
            expect(view.$el.find('.row:first .col-sm-4')).to.be.lengthOf(3);
        });

        describe('should match', function (){
            it('First Year Commission (FYC) labels', function () {
                expect(view.fycRegion.$el.find('h4').text())
                    .to.contain('Year To Date');
                expect(view.fycRegion.$el.find('h4').text())
                    .to.contain('First Year Commission');   
            });
            it('First Year Commission (FYC) value', function () {
                expect(view.fycRegion.$el.find('.year-to-date').text().trim())
                    .to.equal('$49,844');
            });
    
            it('FYPC labels', function () {
                expect(view.fypcRegion.$el.find('h4').text()).to.contain('Year To Date');
                expect(view.fypcRegion.$el.find('h4').text())
                    .to.contain('First Year Premium Credit (FYPC)');
            }); 

            it('FYPC value', function () {
                expect(view.fypcRegion.$el.find('.year-to-date')
                    .text().trim()).to.equal('$45,097');
            });

            it('Qualifying FYPC labels', function () {
                expect(view.qualifyingFypcRegion.$el.find('h4').text()).to.contain('Year To Date');
                expect(view.qualifyingFypcRegion.$el.find('h4')
                    .text()).to.contain('Qualifying FYPC');
            });
    
            it('Qualifying FYPC value', function () {
                expect(view.qualifyingFypcRegion.$el.find('.year-to-date')
                    .text().trim()).to.equal('$25,434');
            });
        });

    });

});


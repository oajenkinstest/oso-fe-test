/* global expect:false */

var PolicySummaryModel = require('../dist/pages/policy/models/policy-summary-m');

describe('Policy Summary Model ' +
    '(pages/policy/models/policy-summary-m.js)', function () {

    it('Has a default value for policyId', function () {
        var model = new PolicySummaryModel();
        expect(model.get('policyId')).to.equal('');
    });
});

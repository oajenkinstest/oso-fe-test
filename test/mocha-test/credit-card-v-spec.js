/* global expect:false, _:false */

var CreditCardView = require('../dist/pages/requirementSubmission/views/credit-card-v');

var RequirementSubmissionFormModel = require(
    '../dist/pages/requirementSubmission/models/requirement-submission-form-m'
);

var RequirementSubmissionFormViewModel = require(
    '../dist/pages/requirementSubmission/viewModels/requirement-submission-form-vm'
);

describe('Requirement Submission Page - Credit Card View' +
    '(pages/requirementSubmission/views/credit-card-v.js)', function () {

    var checkbox;
    var domainModel;
    var view;

    beforeEach(function () {
        domainModel = new RequirementSubmissionFormModel();

        view = new CreditCardView({
            model : new RequirementSubmissionFormViewModel({
                domainModel : domainModel,
                policyId    : 198273645
            })
        });

        view.render();

        checkbox = view.$el.find('[name="credit-card"]');
    });

    afterEach(function () {
        view.destroy();
    });

    it('exists', function () {
        expect(CreditCardView).to.exist;
    });

    describe('initialize method', function () {

        it('exists as a function', function () {
            expect(view.initialize).to.exist.and.be.a('function');
        });

        it('throws an error if not passed a model or policyId', function () {
            var expectedErrorMsg = CreditCardView.prototype.errors.invalidOptions;
            var fn = function () {
                new CreditCardView();   // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedErrorMsg);
        });

        it('creates model if passed policyId', function () {
            view.destroy();
            view = new CreditCardView({
                policyId : 987656789
            });

            expect(view.model).to.exist;
        });

    });     // initialize method

    it('renders checkbox input and accompanying markup', function () {
        expect(view.$el.find('[name="credit-card"]')).to.exist;
    });

    it('renders checkbox checked if "creditCard" is TRUE in model', function () {

        view.model.set('creditCard', true);
        view.render();

        expect(view.$el.find('[name="credit-card"]').is(':checked')).to.be.true;
    });

    it('updates the "creditCard" value in the model if the checkbox value is changed',
        function () {
        var setStub = this.sinon.stub(view.model, 'set');

        checkbox.trigger('change');

        expect(setStub).to.have.been.calledOnce;

        setStub.restore();
    });

    describe('Error Message associated with Credit Card', function () {

        it('should not be rendered when "creditCardErrorText" is undefined', function () {
            var errorMsg     = view.model.get('creditCardErrorText');

            expect(_.isUndefined(errorMsg)).to.be.true;
            expect(view.$el.find('div.form-group').hasClass('has-error')).to.be.false;
            expect(view.$el.find('div.well').hasClass('well-form-error')).to.be.false;
        });

        it('should be rendered when "creditCardErrorText" is defined in model', function () {
            var errorMsg = 'Intruder alert!';

            // update the error message
            view.model.set('creditCardErrorText', errorMsg);

            expect(view.$el.find('div.form-group').hasClass('has-error')).to.be.true;
            expect(view.$el.find('div.well').hasClass('well-form-error')).to.be.true;
            expect(view.$el.find('#credit-card-error-region').text().trim()).to.equal(errorMsg);
        });
    });
});

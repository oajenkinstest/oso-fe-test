/* global describe:false, Backbone:false, expect:false, $:false */

var UserModule = require('../dist/modules/user/userModule');
var UserModel  = require('../dist/models/user-m');

describe('User module (modules/user/userModule.js)', function () {

    var userResponseBody  = {
        'webId'        : '123abc',
        'producerId'   : 987654,
        'displayName'  : 'Joe',
        'capabilities' : [
            'user.get',
            'inforcepolicies.list',
            'inforcepolicies.update'
        ]
    };

    var server;
    var userModule;

    beforeEach(function () {
        userModule = new UserModule();
        server = this.setupFakeServer(this.sinon, global, global.window);
    });

    afterEach(function () {
        server.restore();
        userModule.destroy();
    });

    it('userModule object should exist', function () {
        expect(userModule).to.exist;
    });

    it('sends an "error" Radio message if userModel fails validation', function () {
        var errorHandlerSpy = this.sinon.spy();
        var errorChannel = Backbone.Radio.channel('error');
        userModule.listenTo(errorChannel,'showErrorPage', errorHandlerSpy);

        userModule.userModel.set(
            'capabilities',
            'This is not an array.',
            {validate: true}
        );

        expect(errorHandlerSpy).to.have.been.calledOnce;
    });

    describe('getCapabilities method', function () {

        it('exists', function () {
            expect(userModule.getCapabilities).to.exist
                .and.be.a('function');
        });

        it('returns an array', function () {
            userModule.userModel.set(userResponseBody);
            var capabilities = userModule.getCapabilities();

            expect(Array.isArray(capabilities)).to.be.true;
        });
    }); // getCapabilities method

    describe('getProducerId method', function () {
        it('should exist and be a function', function () {
            expect(userModule.getProducerId).to.be.a('function');
        });

        it('should return a number if "producerId" is returned from the service', function () {
            var result;
            userModule.userModel.set(userResponseBody);
            result = userModule.getProducerId();

            expect(result).to.be.a('number');
        });

        it('should return NULL if "producerId" is not returned from the service', function () {
            var copyOfUserResponse = $.extend(true,{},userResponseBody);
            var result;

            // remove the "producerId" from the response
            delete copyOfUserResponse.producerId;

            userModule.userModel.set(copyOfUserResponse);
            result = userModule.getProducerId();

            expect(result).to.be.null;
        });
    }); // getProducerId method


    describe('fetchUserData method', function () {

        it('exists', function () {
            expect(userModule.fetchUserData).to.exist
                .and.be.a('function');
        });

        it('triggers a "state:ready" event after user model is fetched', function (done) {
            var listener = new Backbone.Marionette.Object();
            var readyHandlerSpy = this.sinon.spy();
            server.respondWith('GET', '',
                [   200,
                    { 'Content-Type': 'application/json' },
                    JSON.stringify(userResponseBody)
                ]);
            userModule.on('state:ready', readyHandlerSpy);

            listener.listenTo(userModule, 'state:ready', function () {
                expect(readyHandlerSpy).to.have.been.calledOnce;
                listener.stopListening();
                done();
            });

            userModule.fetchUserData();
            server.respond();
        });

        it('sets up a model change event listener after user model is fetched', function (done) {
            var userModuleChangeSpy = this.sinon.spy();
            userModule.userModel.on('change', userModuleChangeSpy);

            // First, get the user module to trigger 'state:ready'
            server.respondWith('GET', '',
                [   200,
                    { 'Content-Type': 'application/json' },
                    JSON.stringify(userResponseBody)
                ]);
            userModule.fetchUserData(); // triggers first 'change' event
            server.respond();

            // Then, change something in the user model to cause the model
            // to trigger a second 'change' event.
            userModule.userModel.set('superPower', 'unbreakable fingernails');

            setTimeout(function () {
                expect(userModuleChangeSpy).to.have.been.calledTwice;
                userModule.userModel.off('change', userModuleChangeSpy);
                done();
            }, 0);
        });
    }); // fetchUserData method


    describe('logout method', function () {

        it('exists', function () {
            expect(userModule.logout).to.exist
                .and.be.a('function');
        });

        it('clears the userModel objects and redirects the ' +
            'browser to the logout URL', function () {

            // Setup: Make sure that the userModule has a user model
            userModule.userModel = new UserModel(userResponseBody);
            userModule.impersonatedUserModel = new UserModel(userResponseBody);
            expect(userModule.getWebId()).not.to.be.undefined;

            userModule.logout(false,'#/auth/logout');

            // After the logout call, the user model should be undefined
            expect(userModule.getWebId()).to.be.undefined;
            expect(userModule.userModel).to.equal(null);
            expect(userModule.impersonatedUserModel).to.equal(null);
            expect(window.location.hash).to.equal('#/auth/logout');
        });

        it('"deepLinking" parameter to add target and smagentname', function () {
            var encodedTargetURL = encodeURIComponent(Backbone.history.location.href);
            var expectedURL = '#/auth/logout?smagentname=api-agent'+
                '&target='+ encodedTargetURL;

            userModule.logout(true, '#/auth/logout');
            expect(window.location.hash).to.equal(expectedURL);
        });

        it('works when no url parameter is supplied', function () {

            // Setup: Make sure that the userModule has a user model
            userModule.userModel = new UserModel(userResponseBody);
            expect(userModule.getWebId()).not.to.be.undefined;

            // userModule.logout();
            userModule.logout(false,'#/auth/logout');

            // After the logout call, the user model should be undefined
            expect(userModule.getWebId()).to.be.undefined;
        });
    }); // logout method

    describe ('"timeout" method', function () {
        it('build redirect URL as expected', function () {
            window.location.hash='';
            var logoutSpy =  this.sinon.spy(userModule, 'logout');
            var encodedTargetURL = encodeURIComponent(Backbone.history.location.href);
            var expectedURL = '#/auth/logout?timeout=true&smagentname=api-agent'+
                '&target='+ encodedTargetURL;

            userModule.timeout('#/auth/logout?timeout=true');
            expect(logoutSpy).to.have.been.called;
            expect(window.location.hash).to.equal(expectedURL);
        });
    });


    describe('beginImpersonation method', function () {

        var userData = {
            webId        : 'evel',
            displayName  : 'Evel Knievel',
            capabilities : [
                'daredevil', 'death-defiance'
            ]
        };

        var delegationUserData = [
            {
                producer : {
                    webId : 'abc123',
                    roles : [{roleCode : '123', statusCode : 'A' }]
                }
            }, {
                producer : {
                    webId : 'xyz789',
                    roles : [{roleCode : '456', statusCode : 'A'}]
                }
            }
        ];

        beforeEach(function () {
            server.respondWith('GET', '/api/oso/secure/rest/user',
                [   200,
                    { 'Content-Type': 'application/json' },
                    JSON.stringify(userData)
                ]);
        });

        it('exists', function () {
            expect(userModule.beginImpersonation).to.exist.and.be.a('function');
        });

        it('requires a producerInfo parameter', function() {
            var impersonationFail = function () {
                userModule.beginImpersonation();
            };

            expect(impersonationFail).to.throw(userModule.errors.beginImpersonationError);
        });

        it('requires a webId in the producerInfo parameter', function () {
            var impersonationFail = function () {
                userModule.beginImpersonation({
                    producerName : 'Evel Knievel'
                });
            };

            expect(impersonationFail).to.throw(userModule.errors.beginImpersonationError);
        });

        it('sets the impersonatedWebId', function () {
            userModule.beginImpersonation({
                webId    : 'evel001',
                fullName : 'Evel Knievel'
            });

            var targetUser = userModule.impersonatedWebId;
            expect(targetUser).to.equal('evel001');
        });

        it('triggers a "viewAsProducer" event', function (done) {
            var listener = new Backbone.Marionette.Object();

            listener.listenTo(
                userModule,
                'viewAsProducer',
                function (isDelegate, hasMultiple, isProducer) {
                    //flags :- isDelegate, hasMultiple delegates, isProducer
                    // expect(viewAsProducerSpy).to.have.been.calledWith(false, false, false);
                    expect(isDelegate).to.equal(false);
                    expect(hasMultiple).to.equal(false);
                    expect(isProducer).to.equal(false);

                    listener.stopListening();
                    done();
                }
            );

            userModule.beginImpersonation({
                webId    : 'evel',
                fullName : 'Evel Knievel'
            });
            server.respond();
        });

        it('triggers a "viewAsProducer" event for delegate access', function (done) {
            var listener = new Backbone.Marionette.Object();

            listener.listenTo(
                userModule,
                'viewAsProducer',
                function (isDelegate, hasMultiple, isProducer) {
                    //flags :- isDelegate, hasMultiple delegates, isProducer
                    // expect(viewAsProducerSpy).to.have.been.calledWith(true, true, false);
                    expect(isDelegate).to.equal(true);
                    expect(hasMultiple).to.equal(true);
                    expect(isProducer).to.equal(false);

                    listener.stopListening();
                    done();
                }
            );

            userModule.userModel.set('capabilities', 'OLS:DELEGATE');
            userModule.userModel.set('delegation', delegationUserData);
            userModule.beginImpersonation({
                webId    : 'evel',
                fullName : 'Evel Knievel'
            });
            server.respond();
        });

        it('triggers a "viewAsProducer" event for Producer delegate access', function (done) {
            var listener = new Backbone.Marionette.Object();

            listener.listenTo(
                userModule,
                'viewAsProducer',
                function (isDelegate, hasMultiple, isProducer) {
                    //flags :- isDelegate, hasMultiple delegates, isProducer
                    // expect(viewAsProducerSpy).to.have.been.calledWith(true, true, true);
                    expect(isDelegate).to.equal(true);
                    expect(hasMultiple).to.equal(true);
                    expect(isProducer).to.equal(true);

                    listener.stopListening();
                    done();
                }
            );

            userModule.userModel.set('capabilities', ['OLS:DELEGATE', 'OLS:PRODUCER']);
            userModule.userModel.set('delegation', delegationUserData);
            userModule.beginImpersonation({
                webId    : 'evel',
                fullName : 'Evel Knievel'
            });
            server.respond();
        });

        it('Should include redirectHash parameter if that exist in '+
            'producerInfo object', function (done) {
            var redirectHash      ='#/pendingpolicy-manager/org';
            var listener = new Backbone.Marionette.Object();

            listener.listenTo(
                userModule,
                'viewAsProducer',
                function (isDelegate, hasMultiple, isProducer, hash) {
                    // expect(viewAsProducerSpy).to.have.been.calledWith(false, false, false, redirectHash);
                    expect(isDelegate).to.equal(false);
                    expect(hasMultiple).to.equal(false);
                    expect(isProducer).to.equal(false);
                    expect(hash).to.equal(redirectHash);

                    listener.stopListening();
                    done();
                }
            );

            userModule.beginImpersonation({
                webId        : 'evel',
                fullName     : 'Evel Knievel',
                redirectHash : redirectHash
            });
            server.respond();
        });

        it('trigger "setViewingAs" analytics method with impersonatedWebId', function (done) {
            var analyticsChannel = Backbone.Radio.channel('analytics');
            var listener = new Backbone.Marionette.Object();

            listener.listenTo(
                analyticsChannel,
                'setViewingAs',
                function (webId) {
                    expect(webId).to.equal('evel002');

                    listener.stopListening();
                    done();
                }
            );

            userModule.beginImpersonation({
                webId    : 'evel002',
                fullName : 'Evel Knievel'
            });
            server.respond();
        });

        it('Should set "displayName" property of impersonatedUserModel with '+
            'impersonated user\'s displayName in absence of fullName '+
            'of producer ', function (done) {

            userModule.beginImpersonation({
                webId    : 'evel'
            });
            server.respond();

            setTimeout(function () {
                expect(userModule.impersonatedUserModel.get('displayName'))
                    .to.equal(userData.displayName);
                done();
            }, 0);
        });

    }); // beginImpersonation method


    describe('getImpersonatedWebId method', function () {

        it('exists', function () {
            expect(userModule.getImpersonatedWebId).to.exist
                .and.be.a('function');
        });

        it('initially returns undefined', function () {
            var targetUser = userModule.getImpersonatedWebId();
            expect(targetUser).to.be.undefined;
        });

        it('during impersonation, returns the webId string', function () {
            userModule.beginImpersonation({
                webId    : 'SimonTheDigger',
                fullName : 'Simon the Digger'
            });
            var targetUser = userModule.getImpersonatedWebId();

            expect(targetUser).to.equal('SimonTheDigger');
        });
    }); // getImpersonatedWebId method


    describe('endImpersonation method', function () {

        it('exists', function () {
            expect(userModule.endImpersonation).to.exist
                .and.be.a('function');
        });

        it('endImpersonation() sets the impersonatedWebId to null', function () {
            userModule.endImpersonation();
            var targetUser = userModule.getImpersonatedWebId();
            expect(targetUser).to.be.null;
        });

        it('endImpersonation() sets all other delegate related '+
            'variable (isDelegate, isProducer, hasMultiple) to null', function () {
            userModule.endImpersonation();
            expect(userModule.isDelegate).to.be.null;
            expect(userModule.isProducer).to.be.null;
            expect(userModule.hasMultiple).to.be.null;
        });

        it('endImpersonation() triggers an "endViewAsProducer" event', function () {
            var endViewAsProducerSpy = this.sinon.spy();
            userModule.on('endViewAsProducer', endViewAsProducerSpy);
            userModule.endImpersonation();
            expect(endViewAsProducerSpy).to.have.been.calledOnce;
            userModule.off('endViewAsProducer');
        });

        it('endImpersonation() triggers an "endViewAsProducer" event with '+
            'redirectFlag if that exist', function () {
            var endViewAsProducerSpy = this.sinon.spy();
            var redirectFlag = true;
            userModule.on('endViewAsProducer', endViewAsProducerSpy);
            userModule.endImpersonation(redirectFlag);
            expect(endViewAsProducerSpy).to.have
                .been.calledWith(undefined, undefined, undefined, redirectFlag);
            userModule.off('endViewAsProducer');
        });


        it('trigger "setViewingAs" analytics method with null', function () {
            var analyticsHandlerSpy = this.sinon.spy();
            var analyticsChannel = Backbone.Radio.channel('analytics');
            userModule.listenTo(analyticsChannel,'setViewingAs', analyticsHandlerSpy);

            userModule.endImpersonation();

            expect(analyticsHandlerSpy).to.have.been.calledWith(null);

        });
    }); // endImpersonation method

    it('_onProducerRolesRetrieved method should trigger "state:ready:roles" event', function () {
        var readyRolesSpy = this.sinon.spy();
        userModule.on('state:ready:roles', readyRolesSpy);
        userModule._onProducerRolesRetrieved();

        expect(readyRolesSpy).to.have.been.calledOnce;
    });

    describe('getDistributionChannelName method', function () {

        it('should return NULL if an active role does not exist', function () {
            var activeRole;
            var result;

            userModule.producerRolesModel = new Backbone.Model();

            activeRole = userModule.producerRolesModel.get('activeRole');
            result     = userModule.getDistributionChannelName();

            expect(activeRole).to.be.undefined;
            expect(result).to.be.null;
        });

        it('should return distributionChannel value for "activeRole" attribute when set', function () {
            var result;
            var expected = 'SYFY';

            userModule.producerRolesModel = new Backbone.Model();
            userModule.producerRolesModel.set('activeRole', {
                id                  : 8675309,
                roleCode            : '4290',
                reporting           : true,
                distributionChannel : expected
            });

            result = userModule.getDistributionChannelName();

            expect(result).to.equal(expected);
        });

    }); // getDistributionChannelName method

    describe('getReportingPostalCode method', function () {

        it('should return NULL if an active role does not exist', function () {
            var activeRole;
            var result;

            userModule.producerRolesModel = new Backbone.Model();

            activeRole = userModule.producerRolesModel.get('activeRole');
            result     = userModule.getReportingPostalCode();

            expect(activeRole).to.be.undefined;
            expect(result).to.be.null;
        });

        it('should return NULL if an active contact does not exist', function () {
            var activeContact;
            var result;

            userModule.producerRolesModel = new Backbone.Model({
                activeRole : {
                    noContact : {}
                }
            });

            activeContact = userModule.producerRolesModel.get('activeRole').contact;
            result        = userModule.getReportingPostalCode();

            expect(activeContact).to.be.undefined;
            expect(result).to.be.null;
        });

        it('should return NULL if an active contact does not have an associated post object',
            function () {
                var activePost;
                var result;

                userModule.producerRolesModel = new Backbone.Model({
                    activeRole : {
                        contact : {
                            noPost : 'fooey'
                        }
                    }
                });

                activePost = userModule.producerRolesModel.get('activeRole').contact.post;
                result     = userModule.getReportingPostalCode();

                expect(activePost).to.be.undefined;
                expect(result).to.be.null;
            });

        it('should return NULL if an active contact does not have an associated postalCode value',
            function () {
                var activePostalCode;
                var result;

                userModule.producerRolesModel = new Backbone.Model({
                    activeRole : {
                        contact : {
                            post : {
                                noPostalCode : 'nope'
                            }
                        }
                    }
                });

                activePostalCode = userModule.producerRolesModel.get('activeRole')
                    .contact.post.postalCode;

                result = userModule.getReportingPostalCode();

                expect(activePostalCode).to.be.undefined;
                expect(result).to.be.null;
            });

        it('should return postalCode for an active role contact', function () {
            var expected = '12345';
            var result;

            userModule.producerRolesModel = new Backbone.Model({
                activeRole : {
                    contact : {
                        post : {
                            postalCode : expected
                        }
                    }
                }
            });

            result = userModule.getReportingPostalCode();

            expect(result).to.equal(expected);
        });

    }); // getReportingPostalCode method

    describe('getUserOLSRole method', function () {

        it('should return NULL if none of the user capabilities start with OLS:', function () {
           
            var result;

             // Setup: Make sure that the userModule has a user model
            userModule.userModel = new UserModel(userResponseBody);

            result     = userModule.getUserOLSRole();

            expect(result).to.be.null;
        });

        it('should return role value if user has OLS capabilities', function () {
            var result;
            var copyUserResponseBody = $.extend(true,{},userResponseBody);
            copyUserResponseBody.capabilities.push(
                'OLS:HO:OVERRIDE:IGO.EAPP',
                'OLS:HO'
            );

            // Setup: Make sure that the userModule has a user model
            userModule.userModel = new UserModel(copyUserResponseBody);
            result = userModule.getUserOLSRole();
            expect(result).to.equal('HO');
        });

        it('should return PRODUCER-DELEGATE if user has two OLS capability', function (){
            var result;
            var copyUserResponseBody = $.extend(true,{},userResponseBody);
            copyUserResponseBody.capabilities.push(
                'OLS:DELEGATE',
                'OLS:PRODUCER'
            );

            // Setup: Make sure that the userModule has a user model
            userModule.userModel = new UserModel(copyUserResponseBody);
            result = userModule.getUserOLSRole();
            expect(result).to.equal('PRODUCER-DELEGATE');
        });

    }); // getUserOLSRole method

    describe('getReportingStateProvince method', function () {

        it('should return NULL if an active role does not exist', function () {
            var activeRole;
            var result;

            userModule.producerRolesModel = new Backbone.Model();

            activeRole = userModule.producerRolesModel.get('activeRole');
            result     = userModule.getReportingStateProvince();

            expect(activeRole).to.be.undefined;
            expect(result).to.be.null;
        });

        it('should return NULL if an active contact does not exist', function () {
            var activeContact;
            var result;

            userModule.producerRolesModel = new Backbone.Model({
                activeRole : {
                    noContact : {}
                }
            });

            activeContact = userModule.producerRolesModel.get('activeRole').contact;
            result        = userModule.getReportingStateProvince();

            expect(activeContact).to.be.undefined;
            expect(result).to.be.null;
        });

        it('should return NULL if an active contact does not have an associated post object',
            function () {
                var activePost;
                var result;

                userModule.producerRolesModel = new Backbone.Model({
                    activeRole : {
                        contact : {
                            noPost : 'fooey'
                        }
                    }
                });

                activePost = userModule.producerRolesModel.get('activeRole').contact.post;
                result     = userModule.getReportingStateProvince();

                expect(activePost).to.be.undefined;
                expect(result).to.be.null;
            });

        it('should return NULL if an active contact does not have an associated stateProvince value',
            function () {
                var activePostalCode;
                var result;

                userModule.producerRolesModel = new Backbone.Model({
                    activeRole : {
                        contact : {
                            post : {
                                noStateProvince : 'nope'
                            }
                        }
                    }
                });

                activePostalCode = userModule.producerRolesModel.get('activeRole')
                    .contact.post.stateProvince;

                result = userModule.getReportingStateProvince();

                expect(activePostalCode).to.be.undefined;
                expect(result).to.be.null;
            });

        it('should return stateProvince for an active role contact', function () {
            var expected = 'IN';
            var result;

            userModule.producerRolesModel = new Backbone.Model({
                activeRole : {
                    contact : {
                        post : {
                            stateProvince : expected
                        }
                    }
                }
            });

            result = userModule.getReportingStateProvince();

            expect(result).to.equal(expected);
        });

    }); // getReportingStateProvince method

    describe('getDelegationTargets function', function() {

        it('Returns an empty array when no "delegation" array present', function() {
            expect(userModule.getDelegationTargets()).to.deep.equal([]);
        });

        it('Returns only producers with at least one active role code', function() {
            var expectedTargets = [
                {
                    producer : {
                        webId : 'abc123',
                        roles : [
                            {
                                roleCode   : '123',
                                statusCode : 'A'
                            }, {
                                roleCode   : '456',
                                statusCode : 'I'
                            }
                        ]
                    }
                }
            ];
            userModule.userModel.set('delegation', [
                {
                    producer : {
                        webId : 'abc123',
                        roles : [
                            {
                                roleCode   : '123',
                                statusCode : 'A'
                            }, {
                                roleCode   : '456',
                                statusCode : 'I'
                            }
                        ]
                    }
                }, {
                    producer : {
                        webId : 'xyz789',
                        roles : [
                            {
                                roleCode   : '456',
                                statusCode : 'I'
                            }
                        ]
                    }
                }
            ]);
            expect(userModule.getDelegationTargets()).to.deep.equal(expectedTargets);
        });

    });

    describe('"user" Backbone.Radio channel', function () {

        it('userChannel should exist and name should be user \'user\'', function() {
            expect(userModule.userChannel).to.be.an.instanceOf(Backbone.Radio.Channel);
            expect(userModule.userChannel).to.have.property('channelName', 'user');
        });

        it('userChannel should have \'hasCapability\' request object', function() {
            expect(userModule.userChannel._requests).to.have.property('hasCapability');
        });

        it('userChannel should have \'getDelegateTargets\' request object', function () {
            expect(userModule.userChannel._requests).to.have.property('getDelegateTargets');
        });

        it('userChannel should have \'getImpersonatedWebId\' request object', function () {
            expect(userModule.userChannel._requests).to.have.property('getImpersonatedWebId');
        });

        it('userChannel should stop replying before the userModule is destroyed',
            function () {
                var stopReplyingSpy = this.sinon.spy(userModule.userChannel, 'stopReplying');

                userModule.destroy();

                expect(stopReplyingSpy).to.have.been.called;

                userModule.userChannel.stopReplying.restore();
            });

    });


    describe('Capabilities functions', function () {

        beforeEach(function() {
            userModule.userModel = new UserModel(userResponseBody);
        });

        afterEach(function() {
            userModule.userModel             = null;
            userModule.impersonatedUserModel = null;
        });

        describe('getCapabilities tests', function() {
            it('Has a getCapabilities function', function() {
                expect(userModule.getCapabilities).to.exist.and.be.a.function;
            });

            it('Retrieves the capabilities from the userModel', function() {
                var expectedCapabilities = [
                    'user.get',
                    'inforcepolicies.list',
                    'inforcepolicies.update'
                ];

                var capabilities = userModule.getCapabilities();

                expect(capabilities).to.deep.equal(expectedCapabilities);
            });

            it('Retrieves the capabilities from the impersonatedUserModel (if it exists)',
                function() {
                    var impersonatedCapabilities = [ 'one', 'two', 'three' ];

                    expect(userModule.impersonatedUserModel).to.be.undefined;
                    userModule.impersonatedUserModel = new UserModel({
                        webId        : 'Yoko',
                        displayName  : 'Yoko Littner',
                        capabilities : impersonatedCapabilities
                    });
                    expect(userModule.impersonatedUserModel).to.not.be.undefined;

                    var capabilities = userModule.getCapabilities();

                    expect(capabilities).to.deep.equal(impersonatedCapabilities);
                }
            );

            it('Retrieves an empty array if no userModel or impersonatedUserModel is set',
                function() {
                    userModule.userModel = null;
                    expect(userModule.getCapabilities()).to.deep.equal([]);
                }
            );
        });

        describe('hasCapability tests', function() {
            it('Has a hasCapability function', function() {
                expect(userModule.hasCapability).to.exist.and.be.a.function;
            });

            it('Checks the impersonatedUser object if it is set', function() {
                var impersonatedCapabilities = [ 'one', 'two', 'three' ];

                expect(userModule.impersonatedUserModel).to.be.undefined;
                userModule.impersonatedUserModel = new UserModel({
                    webId        : 'Yoko',
                    displayName  : 'Yoko Littner',
                    capabilities : impersonatedCapabilities
                });
                expect(userModule.impersonatedUserModel).to.not.be.undefined;

                // A capability on the impersonatedUserModel
                expect(userModule.hasCapability('one')).to.equal(true);

                // A capability on the userModel should be false
                expect(userModule.hasCapability('inforcepolicies.list')).to.equal(false);

                // A capability that isn't on either model should be false
                expect(userModule.hasCapability('delete.stuff')).to.equal(false);
            });

            it('Add additional params to explicitly query userModel', function () {
                var impersonatedCapabilities = [ 'one', 'two', 'three' ];
                userModule.impersonatedUserModel = new UserModel({
                    webId        : 'Yoko',
                    displayName  : 'Yoko Littner',
                    capabilities : impersonatedCapabilities
                });

                expect(userModule.hasCapability('inforcepolicies.list', true)).to.equal(true);
            });

            it('Uses the userModel object if it exists', function() {
                // A capability on the userModel should be true
                expect(userModule.hasCapability('inforcepolicies.list')).to.equal(true);

                // A capability that isn't on either model should be false
                expect(userModule.hasCapability('delete.stuff')).to.equal(false);
            });

            it('Returns false if both models are null', function() {
                userModule.userModel = null;

                expect(userModule.hasCapability('inforcepolicies.list')).to.equal(false);
            });
        });
    });

    describe('Method getDelegationTarget', function () {
        var delegationData = [
            {
                producer : {
                    webId : 'abc123',
                    roles : [
                        {
                            roleCode   : '123',
                            statusCode : 'A'
                        }, {
                            roleCode   : '456',
                            statusCode : 'I'
                        }
                    ]
                }
            }, {
                producer : {
                    webId : 'xyz789',
                    roles : [
                        {
                            roleCode   : '456',
                            statusCode : 'I'
                        }
                    ]
                }
            }
        ];

        it('should exist', function () {
            expect(userModule.getDelegationTarget).to.exist;
        });

        it('should return producer info if webId match with any of '+
            'delegation object', function () {
            userModule.userModel.set('delegation', delegationData);
            var producerInfo = userModule.getDelegationTarget('abc123');

            expect(producerInfo).to.deep.equal(delegationData[0].producer);

        });

        it('should return null if webId not match with any of '+
            'delegation object / any of them having statusCode "I" ', function () {
            userModule.userModel.set('delegation', delegationData);
            var producerInfo = userModule.getDelegationTarget('xyz789');

            expect(producerInfo).to.be.null;
        });

        it('should return null if webId is missing as param', function () {
            userModule.userModel.set('delegation', delegationData);
            var producerInfo = userModule.getDelegationTarget();

            expect(producerInfo).to.be.null;
        });
    });

    describe('Simple getter functions', function() {
        beforeEach(function() {
            userModule.userModel = new UserModel(userResponseBody);
            userModule.impersonatedUserModel = new UserModel({
                webId       : '12345',
                displayName : 'Curious George'
            });
        });

        afterEach(function() {
            userModule.userModel             = null;
            userModule.impersonatedUserModel = null;
        });

        describe('getWebId function', function() {
            it('exists', function() {
                expect(userModule.getWebId).to.exist.and.be.a('function');
            });
            it('returns the property on the userModel', function() {
                expect(userModule.getWebId()).to.equal('123abc');
            });
            it('returns undefined if the userModel is not set', function() {
                userModule.userModel = null;
                expect(userModule.getWebId()).to.be.undefined;
            });
        });

        describe('getDisplayName function', function() {
            it('exists', function() {
                expect(userModule.getDisplayName).to.exist.and.be.a('function');
            });
            it('returns the property on the userModel', function() {
                expect(userModule.getDisplayName()).to.equal('Joe');
            });
            it('returns undefined if the userModel is not set', function() {
                userModule.userModel = null;
                expect(userModule.getDisplayName()).to.be.undefined;
            });
        });

        describe('getImpersonatedUserName function', function() {
            it('exists', function() {
                expect(userModule.getImpersonatedUserName).to.exist.and.be.a('function');
            });
            it('returns the property on the userModel', function() {
                expect(userModule.getImpersonatedUserName()).to.equal('Curious George');
            });
            it('returns undefined if the userModel is not set', function() {
                userModule.impersonatedUserModel = null;
                expect(userModule.getImpersonatedUserName()).to.be.undefined;
            });
        });
    });
});
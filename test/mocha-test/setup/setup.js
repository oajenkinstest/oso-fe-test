/* global $:false */

console.log('Processing setup/setup.js'); // eslint-disable-line no-console

var srcPath         = '../../../src';
var chai            = require('chai');
var fs              = require('fs');
var sinon           = require('sinon');
var sinonChai       = require('sinon-chai');
var vendorFile      = fs.readFileSync(__dirname + '/../../dist/vendor.js', 'utf-8');
var webtrends       = fs.readFileSync(__dirname + '/' + srcPath + '/vendor/webtrends.js', 'utf-8');

var Canvas;
var jsdom;
var propagateToGlobal;
var virtualConsole;

// See if the system has canvas installed.
// If it does, load it. If not, set the isCanvasLoaded
// to false so that tests which depend upon Canvas can
// be skipped dynamically.
try {
    Canvas = require('canvas');
    global.isCanvasLoaded = true;
    console.log('Canvas loaded!'); // eslint-disable-line no-console
} catch(e) {
    global.isCanvasLoaded = false;

    console.log('Canvas NOT loaded. '+ // eslint-disable-line no-console
        'Tests requiring use of canvas will be skipped.');
} finally {
    global.Canvas = Canvas;
}

chai.use(sinonChai);

// Attach modules to Mocha's `global` object so that they are accessible in the tests
global.expect = chai.expect;
global.sinon = sinon;

/**
 * Propagates properties in the window object to Mocha's `global` object.
 * For example, a test which adds a spy on the console.log method
 * would need to prefix the console with the 'window' object. This
 * allows for accessing objects such as this during testing.
 *
 * This function is originally from mocha-jsdom:
 * https://github.com/rstacruz/mocha-jsdom/blob/master/index.js#L92
 *
 *
 * @param window
 */
propagateToGlobal = function(window) {
    for (var key in window) {

        if (!window.hasOwnProperty(key)) {
            continue;
        }

        if (key in global) {
            continue;
        }

        global[key] = window[key];
    }
};

// Create the jsdom object
if (!global.document || !global.window) {
    jsdom = require('jsdom');

    virtualConsole = jsdom.createVirtualConsole();

    /* Uncomment the following lines in order to
       log any errors associated with
       the loading of scripts by jsdom. This is
       useful for debugging tests.
    */
    virtualConsole.on('jsdomError', function (error) {
        console.log(error.stack, error.detail); // eslint-disable-line no-console
        // throw error; // To make console errors visible to unit tests
    });

    // I think the way we're using JSDOM causes all of the specs to run within the same DOM rather
    // than giving each spec a new DOM, so it is very easy for code in one spec to interfere with
    // tests in a unrelated spec. I don't know exactly how to fix this problem (I suspect it would
    // mean modifying every spec), and since we're probably going to migrate this codebase to
    // Angular anyway, I'm giving up for now. -- RKC 11 Dev 2017
    global.document = jsdom.jsdom('<html><head>' +
            '<script>' + webtrends + '</script>' +
            '<script>' + vendorFile + '</script></head><body><div class="main-container"></div>' +
            '</body></html>', {
        FetchExternalResources   : ['script'],
        ProcessExternalResources : ['script'],
        MutationEvents           : '2.0',
        QuerySelector            : false,
        virtualConsole           : virtualConsole
    });

    global.window    = document.defaultView;
    global.navigator = global.window.navigator;
    global.Image     = window.Image;
    

    // Add the DOM method .contains() if it doesn't exist.
    // While this doesn't appear to be used in our code
    // Marionette does use this method, so we'll add it
    // to the global object.
    if (global.window.Node && global.window.Node.prototype
        && !global.window.Node.prototype.contains) {

        global.window.Node.prototype.contains = function(node) {
            return this.compareDocumentPosition(node) && 16; // 16 = 'Is contained by'
        };
    }

    // set jsdom's location to localhost:3000
    jsdom.changeURL(window, 'http://localhost:3000');

    propagateToGlobal(window);
}

// stub out the dir function for the console
global.window.console.dir = function () {};

// stub out window.open(), as it is not implemented in JSDOM
global.window.open = function () {};

// stub out form.submit(), as it is not implemented in JSDOM
global.window.HTMLFormElement.prototype.submit = function ( ) {};


// add jsdom's console to the global and
// use it for attaching spies in tests
global.jsdomConsole = global.window.console;

// add jquery and underscore to global
global.$      = require('jquery');
global._      = require('underscore');
global.jQuery = global.$;
window.jQuery = global.jQuery; // Ace sidebar needs window.jQuery
require('jquery-migrate');

global.Backbone   = require('backbone');
global.Backbone.$ = global.$;
global.Marionette = global.Backbone.Marionette = require('backbone.marionette');
require('backbone.radio');
require('bootstrap');
require('backbone.base-router');
require(srcPath + '/vendor/jquery-deparam');
require(srcPath + '/vendor/position-change-listener');

// Stubbing out Option for JSDOM's DHTML calls
// This is called by datatables internally
global.Option = function (){};
require('datatables.net-bs')(window, $);
// DataTables should swallow errors instead of using an 'alert'
global.Backbone.$.fn.dataTable.ext.errMode = 'none';

// OSO version of easyPieChart, for dials in performance center
require(srcPath + '/js/utils/jquery.easypiechart.oso');

global.Spinner    = require(srcPath + '/vendor/spin');

// OSO version of ace.js, for the sidebar in osoApp-spec.js and sidebar-app-spec.js
require(srcPath + '/vendor/ace-customized-for-oso');

// disable URL object in URL instead load URI.js
global.window.URL = null;
global.URI = require('urijs');

console.log('Done processing setup/setup.js'); // eslint-disable-line no-console

/* global sinon, $:false, _:false */

console.log('Processing setup/helpers.js'); // eslint-disable-line no-console

var $body = $(document.body);

var setFixtures = function () { // eslint-disable-line no-unused-vars
    _.each(arguments, function(content) {
        $body.append(content);
    });
};

var clearFixtures = function () { // eslint-disable-line no-unused-vars
    $body.empty();
};

// Sinon.fakeServer won't work for node, so we have to
// add the XMLHttpRequest to both global and window scopes
// from https://gist.github.com/sgtdck/29a996a0e8436dff4233
var setupFakeServer = function(sinon, globalScope, windowScope) {
    var fakeServer = typeof sinon.useFakeServer === 'function' ? 
        sinon.useFakeServer() : sinon.fakeServer.create();

    // set the fake XMLHttpRequest on both the 'global' and 'window' objects.
    globalScope.XMLHttpRequest = windowScope.XMLHttpRequest = fakeServer.xhr;

    // get a reference to the original onCreate method
    var onCreate = windowScope.XMLHttpRequest.onCreate;

    windowScope.XMLHttpRequest.onCreate = function(xhr) {
        // stub the missing methods on the xhr object
        xhr.setRequestHeader = sinon.stub();
        xhr.getAllResponseHeaders = sinon.stub();
        onCreate(xhr);
    };

    return fakeServer;
};

// global.viewHelpers = {
//     startBBHistory : function () {
//         if (!Backbone.History.started) {
//             Backbone.history.start();
//         }
//     },

//     stopBBHistory : function () {
//         if (Backbone.History.started) {
//             Backbone.history.stop();
//         }
//     },

    /**
     * Removes duplicate (successive) whitespace characters
     * from a string and replaces them with a single
     * space. Useful when performing string
     * comparisons using strings from HTML content.
     *
     * Example:
     *
     *      "A string with     multiple spaces."
     *
     * would return:
     *
     *      "A string with multiple spaces."
     *
     * @param {string} value
     * @returns {string}
     */
//     removeDuplicateWhitespace : function(value) {
//         if (typeof value !== 'string') {
//             return value;
//         }

//         return value.replace(/\s+/g, ' ');
//     }
// };

before(function () {
    this.setupFakeServer = setupFakeServer;
});

beforeEach(function () {
    this.sinon           = sinon.sandbox.create();
    // this.setupFakeServer = setupFakeServer;
    // this.setFixtures     = setFixtures;
    // this.clearFixtures   = clearFixtures;
});

afterEach(function () {
    // restore the sandbox
    this.sinon.restore();
    
    // window.location.hash = '';

    // // stop Backbone.history if it's been started
    // global.viewHelpers.stopBBHistory();

    // Backbone.history.handlers.length = 0;
    // clearFixtures();
});

console.log('Done processing setup/helpers.js'); // eslint-disable-line no-console
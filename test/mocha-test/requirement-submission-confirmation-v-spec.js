/* global expect:false, Backbone:false, $:false */


var ConfirmationView =
    require('../dist/pages/requirementSubmission/views/requirement-submission-confirmation-v');
var helpers = require('./helpers/helpers');


describe('Requirement Submission Confirmation View' +
    '(pages/requirementSubmission/views/requirement-submission-confirmation-v.js)', function () {

    it('should exist', function () {
        expect(ConfirmationView).to.exist;
    });

    describe('initialization', function () {

        it('should throw an error if a model is not passed on initialize', function () {
            var expectedError = ConfirmationView.prototype.errors.noModel;
            var fn = function () {
                new ConfirmationView(); // eslint-disable-line no-new
            };

            expect(fn).to.throw(expectedError);
        });

        it('should call render() method upon successful initialization', function () {
            var view = new ConfirmationView({
                model : new Backbone.Model()
            });

            expect(view.isRendered).to.be.true;
        });

    });     // initialization

    describe('Markup tests', function () {
        var copyOfPolicyDetail;
        var setFormData;
        var view;

        beforeEach(function () {
            copyOfPolicyDetail = $.extend(true,{},helpers.policyData.pendingPolicyDetail);

            setFormData = function(formData) {
                var policyAndFormData = $.extend(true, formData, copyOfPolicyDetail);

                // Create the view and model with policy values
                view = new ConfirmationView({
                    model : new Backbone.Model(policyAndFormData)
                });
            };

            // Create the view and model with policy values
            view = new ConfirmationView({
                model : new Backbone.Model(copyOfPolicyDetail)
            });
        });

        afterEach(function () {
            view.destroy();
        });

        describe('should indicate successful submission', function () {
            var checkmark;

            beforeEach(function () {
                checkmark = view.$el.find('.oa-grass i.fa-check');
            });


            it('green checkmark should appear', function () {
                expect(checkmark.length).to.equal(1);
            });

            it('text next to checkmark should read "Submitted"', function () {
                expect(checkmark.parent().text().trim()).to.equal('Submitted');
            });

        });     // should indicate successful submission

        describe('"Submit another requirement for policy ..." link', function () {
            var link;

            beforeEach(function () {
                link = view.$el.find('#submit-another-form-link');
            });

            it('should be displayed', function () {
                expect(link.length).to.equal(1);
            });

            it('expect text to be "Submit another requirement for policy ' +
                '#<policyNumber>"', function () {
                var policyNumber = copyOfPolicyDetail.policyNumber;
                var expectedText = 'Submit another requirement for policy #' + policyNumber;

                expect(link.text().trim()).to.equal(expectedText);
            });

        });     // "Submit another requirement for policy ..." link

        describe('"Return to policy #<policyNumber> detail" link', function () {
            var link;

            beforeEach(function () {
                link = view.$el.find('p a:not("#submit-another-form-link")');
            });

            it('should be displayed', function () {
                expect(link.length).to.equal(1);
            });

            it('expect text to be "Return to policy #<policyNumber> detail"', function () {
                var policyNumber = copyOfPolicyDetail.policyNumber;
                var expectedText = 'Return to policy #' + policyNumber + ' detail';

                expect(link.text().trim()).to.equal(expectedText);
            });

        });     // "Return to Policy #<policyNumber> detail" link

        describe('Requirement Type section', function () {
            var requirementTypeText;

            beforeEach(function () {
                view.destroy();
            });

            afterEach(function () {
                view.destroy();
            });

            it('should contain the appropriate text when "Credit Card" has been ' +
                'checked in the form', function () {
                var expected = 'Submission includes credit card or eCheck authorization.';

                // creditCard value is true
                setFormData({ creditCard : true });
                requirementTypeText = view.$el.find('#requirement-type-section').text().trim();

                expect(requirementTypeText).to.equal(expected);
            });

            it('should contain the appropriate text when "Credit Card" has NOT been' +
                'checked in the form', function () {
                var expected = 'Submission does not include credit card or eCheck ' +
                    'authorization.';

                // set credit card value to false
                setFormData({ creditCard : false });
                requirementTypeText = view.$el.find('#requirement-type-section').text().trim();

                expect(requirementTypeText).to.equal(expected);
            });

        });     // Requirement Type section

        describe('Attachments section', function () {
            var attachmentsWells;
            var files = [];

            describe('When no files were attached', function () {

                it('message should indicate "No files were attached."', function () {
                    var expectedText = 'No files were attached.';

                    setFormData({ files : files });
                    attachmentsWells = view.$el.find('#attachments-section .well');

                    expect(attachmentsWells.length).to.equal(1);
                    expect(attachmentsWells.text().trim()).to.equal(expectedText);
                });

            });     // When no files were attached

            describe('wells should display each file added to the form', function () {

                beforeEach(function () {
                    files = [
                        {
                            name : 'File_1.tif',
                            size : 1258291          // 1.2mb
                        }, {
                            name : 'File_2.pdf',
                            size : 2306867          // 2.2mb
                        }
                    ];

                    setFormData({ files : files });
                });

                it('should display one well per file submitted', function () {
                    attachmentsWells = view.$el.find('#attachments-section .well');

                    expect(attachmentsWells.length).to.equal(files.length);
                });

                it('should display the file name in each of the wells', function() {
                    attachmentsWells = view.$el.find('#attachments-section .well');

                    attachmentsWells.each(function(index) {
                        var expected = files[index].name;
                        var fileName = $(this).find('p').text().trim();
                        expect(fileName).to.contain(expected);
                    });
                });

                it('should display the file size in megabytes in each ' +
                    'of the wells', function () {
                    attachmentsWells = view.$el.find('#attachments-section .well');

                    attachmentsWells.each(function(index) {
                        var expected = (files[index].size / (1024*1024)).toFixed(1) + ' MB';
                        var fileSize = $(this).find('p span').text().trim();
                        expect(fileSize).to.equal(expected);
                    });
                });

            });     // wells should display each file added to the form

        });     // Attachments section

        describe('Comments section', function () {
            var commentsText;

            it('should indicate when no comment was entered', function () {
                setFormData({ comments : '' });

                commentsText = view.$el.find('#comments-section').text().trim();

                expect(commentsText).to.equal('No comment was entered.');
            });

            it('should display the text that the user entered into the form', function () {
                var comments = 'In a hole in the ground there lived a Hobbit';
                setFormData({ comments : comments });

                commentsText = view.$el.find('#comments-section').text().trim();

                expect(commentsText).to.equal(comments);
            });

        });     // Comments section

    });     // Markup tests

});     // Requirement Submission Confirmation View tests

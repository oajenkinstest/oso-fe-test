/* global expect:false, Backbone:false, $:false */

var FormsView = require('../dist/pages/home/views/ipipeline-forms-v');

describe('iPipeline Forms View (pages/home/views/ipipeline-forms-v.js)', function () {
    var view;

    beforeEach(function () {
        view = new FormsView();
        view.render();
    });

    afterEach(function () {
        view.destroy();
    });

    describe('initialize method', function () {

        it('creates the model used by the view', function () {
            expect(view.model).to.exist;
        });

    });     // initialize method

    describe('_handleFormSsoError method', function () {
        var alertMessage;
        var expectedMessage;
        var response = {};

        describe('When response status is 404', function () {

            beforeEach(function () {
                expectedMessage = view.errors.missingProducerRecord;
                response.status = 404;

                view._handleFormsSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "formsPipeErrorMessage" in model', function () {
                expect(view.model.get('formsPipeErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });     // when response status is 404

        describe('When response status is 403', function () {

            beforeEach(function () {
                expectedMessage = view.errors.missingProducerRecord;
                response.status = 403;

                view._handleFormsSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "formsPipeErrorMessage" in model', function () {
                expect(view.model.get('formsPipeErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });     // when response status is 403

        describe('When response status is 500', function () {

            beforeEach(function () {
                expectedMessage = view.errors.xmlGenerationError;
                response.status = 500;

                view._handleFormsSsoError(view.model, response);

                alertMessage = view.$el.find('.alert-warning');
            });

            it('should set "formsPipeErrorMessage" in model', function () {
                expect(view.model.get('formsPipeErrorMessage')).to.equal(expectedMessage);
            });

            it('alert error message should be displayed', function () {
                expect(alertMessage.hasClass('hidden')).to.be.false;
            });

            it('alert error message displays correct error text', function () {
                expect(alertMessage.find('p').html().trim()).to.equal(expectedMessage);
            });

        });

        describe('Errors are sent on to analytics', function () {
            var analyticsChannel;
            var message;
            var trackExceptionStub;

            beforeEach(function () {
                response.status = 404;
                response.responseText = 'Cannot compute!';
                analyticsChannel = Backbone.Radio.channel('analytics');
                trackExceptionStub = this.sinon.stub();

                analyticsChannel.on('trackException', trackExceptionStub);

                view._handleFormsSsoError(view.model, response);

                message = trackExceptionStub.getCalls()[0].args[0].message;
            });

            afterEach(function () {
                trackExceptionStub = null;
            });

            it('"trackExcpetion" should be called on analyticsChannel', function () {
                expect(trackExceptionStub).to.have.been.calledOnce;
            });

            it('message sent should include response.status', function () {
                expect(message).to.contain(response.status);
            });

            it('message sent should include response.responseText', function () {
                expect(message).to.contain(response.responseText);
            });

        });

        describe('When response is not defined or response.status is not set',
            function () {

            it('"formsPipeErrorMessage" is not set', function () {
                view._handleFormsSsoError(view.model, null);
                expect(view.model.has('formsPipeErrorMessage')).to.be.false;
            });

        });

    });     // _handleFormsSsoError method

    describe('_fetchToken method', function () {
        var event;

        var fetchStub;

        beforeEach(function () {
            event     = $.Event('click');
            fetchStub = this.sinon.stub(view.model, 'fetch');
        });

        afterEach(function () {
            fetchStub.restore();
        });

        describe('Should fetch token from service', function () {

            it('when "ApplicationDataXML" is not set in the model', function () {
                view.model.unset('ApplicationDataXML');

                view._fetchToken(event);

                expect(fetchStub).to.have.been.calledOnce;
            });

            it('when "formsPipeErrorMessage" is set in the model', function () {
                view.model.set('formsPipeErrorMessage', 'Inconceivable!');
                view._fetchToken(event);

                expect(fetchStub).to.have.been.calledOnce;
            });

        });

        describe('When "ApplicationDataXML is set in the model and ' +
            '"formsPipeErrorMessage" is NOT', function () {
            var setupFormAndSubmitStub;

            beforeEach(function () {
                view.model.set('ApplicationDataXML', 'I am data');
                view.model.unset('formsPipeErrorMessage');
                setupFormAndSubmitStub = this.sinon.stub(view, '_setupFormAndSubmit');
                view._fetchToken(event);
            });

            afterEach(function () {
                setupFormAndSubmitStub.restore();
            });

            it('should not call fetch() on model', function () {
                expect(fetchStub).not.to.have.been.called;
            });

            it('should call _setupFormAndSubmit', function () {
                expect(setupFormAndSubmitStub).to.have.been.calledOnce;
            });

        });

        describe('When parent div.center is disabled', function () {
            var preventDefaultStub;
            var setupFormAndSubmitStub;

            beforeEach(function () {
                view.$el.find('.center').addClass('disabled');
                setupFormAndSubmitStub = this.sinon.stub(view, '_setupFormAndSubmit');
                preventDefaultStub     = this.sinon.stub(event, 'preventDefault');

                view._fetchToken(event);
            });

            afterEach(function () {
                setupFormAndSubmitStub.restore();
                preventDefaultStub.restore();
            });

            it('link should be prevented from doing anything', function () {
                expect(preventDefaultStub).to.have.been.calledOnce;
            });

            it('_setupFormAndSubmit should not be called', function () {
                expect(setupFormAndSubmitStub).not.to.have.been.called;
            });

        });

    });     // _fetchToken method

    describe('_setupFormAndSubmit method', function () {
        var actionAttribute;
        var inputValue;
        var submitStub;

        beforeEach(function () {
            submitStub = this.sinon.stub(view.ui.form, 'submit');
        });

        afterEach(function () {
            submitStub.restore();
        });

        describe('When "targetURL" or "ApplicationDataXML" are not set in the model',
            function () {

            beforeEach(function () {
                view.model.unset('targetUrl');
                view.model.unset('ApplicationDataXML');

                view._setupFormAndSubmit();

                actionAttribute = view.ui.form.attr('action');
                inputValue      = view.ui.dataInput.val();
            });

            it('form "action" attribute is empty', function () {
                expect(actionAttribute).to.be.empty;
            });

            it('"ApplicationDataXML" input field is empty', function () {
                expect(inputValue).to.be.empty;
            });

            it('the form is not submitted', function () {
                expect(submitStub).not.to.have.been.called;
            });

        });     // When "targetUrl" or "ApplicationDataXML" are not set in the model

        describe('When both "targetURL" and "ApplicationDataXML" are set in the model',
            function () {

            beforeEach(function () {
                view.model.set('targetURL', 'http://someurl.com/SSO');
                view.model.set('ApplicationDataXML', '<xml>');

                view._setupFormAndSubmit();

                actionAttribute = view.ui.form.attr('action');
                inputValue      = view.ui.dataInput.val();
            });

            it('form "action" attribute is set with "targetURL" from model', function () {
                expect(actionAttribute).to.equal(view.model.get('targetURL'));
            });

            it('form input is set with "ApplicationDataXML" from model', function () {
                expect(inputValue).to.equal(view.model.get('ApplicationDataXML'));
            });

            it('form should be submitted', function () {
                expect(submitStub).to.be.calledOnce;
            });

            it('POST should be sent in a new "formspipe" window', function () {
                expect(view.ui.form.attr('target')).to.equal('formspipe');
            });

        });

    });     // _setupFormAndSubmit method

});

/* global describe:false, Backbone:false, $:false, expect:false */

var cp = require('../dist/modules/checkpoint/checkpointModule');
var helpers = require('./helpers/helpers');

describe('Checkpoint Module (modules/checkpoint/checkpointModule.js)', function () {

    beforeEach(function () {
        helpers.viewHelpers.startBBhistory();
    });
    afterEach(function () {
        helpers.viewHelpers.stopBBhistory();
    });

    describe('writeCheckpoint', function () {

        it('is a function', function () {
            expect(cp.writeCheckpoint).to.exist.and.be.a('function');
        });

        it('Throws an error if Backbone.History is not started', function () {
            Backbone.history.stop();
            var fn = function () {
                cp.writeCheckpoint();
            };

            expect(fn).to.throw(cp.errors.bbHistoryNotStarted);
        });

        describe('stateObj parameter', function () {

            it('must be a simple object (not a primitive or array, etc)', function () {
                var param;
                var fn = function () {
                    cp.writeCheckpoint(param);
                };

                param = 123;
                expect(fn).to.throw(cp.errors.stateObjType);

                param = 'this is a string primitive';
                expect(fn).to.throw(cp.errors.stateObjType);

                param = ['this', 'is', 'an', 'array'];
                expect(fn).to.throw(cp.errors.stateObjType);

                param = {};
                expect(fn).not.to.throw();
            });

            it('is optional', function () {
                var fn = function () {
                    cp.writeCheckpoint();
                };

                expect(fn).not.to.throw();
            });
        }); // stateObj parameter

        it('encodes the stateObj and puts it into the URL hash', function () {

            var stateObj = { foo: 123, baz: 'quux' };
            var encodedState = 'foo=123&baz=quux';
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.contain(encodedState);
        });

        it('doesn\'t modify the pageId', function () {

            var initialHash = '#tetris?score=0';
            var expectedHash = '#tetris?score=42 jillion&nextShape=L';
            var stateObj = { score: '42 jillion', nextShape: 'L' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.equal(expectedHash);
        });

        it('Keeps the subpage if it exists', function() {

            var initialHash = '#tetris/game/?score=0';
            var expectedHash = '#tetris/game/?score=42 jillion&nextShape=L';
            var stateObj = { score: '42 jillion', nextShape: 'L' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.equal(expectedHash);
        });
        
        it('Overrides the subpage if there is a subpage in the state object', function() {

            var initialHash = '#tetris/game/?score=0';
            var expectedHash = '#tetris/gameOver/?score=42 jillion&nextShape=L';
            var stateObj = {
                score: '42 jillion',
                nextShape: 'L',
                subpages: ['gameOver'] };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.equal(expectedHash);
        });

        it('clears the state if stateObj is undefined', function () {

            var initialHash = '#tetris?score=42%20jillion';
            var expectedHash = '#tetris';

            document.location.hash = initialHash;
            cp.writeCheckpoint();

            expect(document.location.hash).to.equal(expectedHash);
        });
        
        it('clears the state if stateObj is an empty object', function () {
            var navigateSpy = this.sinon.spy(Backbone.history, 'navigate');

            var initialHash = '#tetris?score=42%20jillion';
            var expectedHash = '#tetris';

            document.location.hash = initialHash;
            cp.writeCheckpoint({});

            expect(navigateSpy).to.have.been.calledWith(expectedHash, { replace: false });

            navigateSpy.restore();
        });

        it('overwrites the existing data in the URL', function () {

            var initialHash = '#pageId/?search=Bond%2C%20James&sortCol=1&page=2';
            var expectedHash = '#pageId/?item1=12345&item2=Foo';
            var stateObj = { item1: 12345, item2: 'Foo' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.equal(expectedHash);
        });

        it('URL encodes the property values', function () {

            var initialHash = '#pageId';
            var expectedHash = '#pageId?search=Bond%2C James';
            var stateObj = { search: 'Bond, James' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj);

            expect(document.location.hash).to.equal(expectedHash);
        });

        // Unfortunately, using {replace: true} doesn't update the hash in the context of
        // these tests. Possibly because it replaces the entire href in the history. The test
        // framework probably prevents that.
        // So, using a spy to check that the call to Backbone.history.navigate has the param
        it('Can handle a "replace" param that is true', function () {
            var navigateSpy = this.sinon.spy(Backbone.history, 'navigate');

            var initialHash = '#pageId/?search=Bond%2C%20James&sortCol=1&page=2';
            var expectedHash = '#pageId/?item1=12345&item2=Foo';
            var stateObj = { item1: 12345, item2: 'Foo' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj, true);

            expect(navigateSpy).to.have.been.calledWith(expectedHash, { replace: true });
            
            navigateSpy.restore();
        });

        it('Can handle a "replace" param that is false', function () {
            var navigateSpy = this.sinon.spy(Backbone.history, 'navigate');

            var initialHash = '#pageId/?search=Bond%2C%20James&sortCol=1&page=2';
            var expectedHash = '#pageId/?item1=12345&item2=Foo';
            var stateObj = { item1: 12345, item2: 'Foo' };

            document.location.hash = initialHash;
            cp.writeCheckpoint(stateObj, false);

            expect(navigateSpy).to.have.been.calledWith(expectedHash, { replace: false });

            navigateSpy.restore();
        });

    }); // writeCheckpoint


    describe('readCheckpoint', function () {

        it('is a function', function () {
            expect(cp.readCheckpoint).to.exist
                .and.be.a('function');
        });

        it('throws an error if $.deparam plugin is not available', function () {
            var saveDeparam = $.deparam;
            delete $.deparam;
            var fn = function () {
                cp.readCheckpoint();
            };

            expect(fn).to.throw(cp.errors.jqueryDeparamMissing);

            $.deparam = saveDeparam;
        });
        
        it('returns undefined if there is no checkpoint data', function () {
            document.location.hash = '#foo';

            expect(cp.readCheckpoint()).to.be.undefined;
        });

        it('returns a correctly parsed object', function () {
            document.location.hash = '#tetris?score=4096&highscore=42+jillion';
            var expectedObj = { score: 4096, highscore: '42 jillion' };

            expect(cp.readCheckpoint()).to.deep.equal(expectedObj);
        });

        it('does not remove the data from the URL', function () {
            var initialHash = '#tetris?score=4096&highscore=42+jillion';
            document.location.hash = initialHash;
            cp.readCheckpoint();

            expect(document.location.hash).to.equal(initialHash);
        });

    }); // readCheckpoint

    describe('getPageIdFromHash', function () {

        it('is a function', function () {
            expect(cp.getPageIdFromHash).to.be.a('function');
        });

        describe('when a subpage is not included in the hash', function () {

            it('returns a pageId', function () {
                document.location.hash = '#tetris';
                var expectedPageId = '#tetris';
                var result = cp.getPageIdFromHash(document.location.hash);

                expect(result).to.equal(expectedPageId);
            });

            it('returns a pageId separate from query string', function () {
                document.location.hash = '#tetris?score=4096&highscore=42+jillion';
                var expectedPageId = '#tetris';
                var result = cp.getPageIdFromHash(document.location.hash);

                expect(result).to.equal(expectedPageId);
            });

            it('returns a pageId with a trailing slash when a trailing slash is used',
                function () {
                document.location.hash = '#tetris/?score=4096&highscore=42+jillion';
                var expectedPageId = '#tetris/';
                var result = cp.getPageIdFromHash(document.location.hash);

                expect(result).to.equal(expectedPageId);
            });
        });

        describe('when a subpage is included in the hash', function () {
            it('returns a pageId', function () {
                document.location.hash = '#tetris/fast/';
                var expectedPageId = '#tetris';
                var result = cp.getPageIdFromHash(document.location.hash);

                expect(result).to.equal(expectedPageId);
            });
        });

    });

    describe('_getSubpageFromHash', function() {
        it('is a function', function () {
            expect(cp._getSubpageFromHash).to.be.a('function');
        });
        
        describe('when a subpage exists in the hash', function() {

            it('without a querystring', function() {
                var hash     = '#pending/search/';
                var expected = '/search/';
                var actual   = cp._getSubpageFromHash(hash);

                expect(actual).to.equal(expected);
            });

            it('with a querystring', function() {
                var hash     = '#pending/search/?searchTerm=Giraffe';
                var expected = '/search/';
                var actual   = cp._getSubpageFromHash(hash);

                expect(actual).to.equal(expected);
            });
        });

        describe('when a subpage does not exist in the hash', function() {
            
            it('without a querystring', function() {
                var hash     = '#pending';
                var expected = '';
                var actual   = cp._getSubpageFromHash(hash);

                expect(actual).to.equal(expected);
            });
            
            it('with a querystring', function() {
                var hash = '#pending?searchTerm=Giraffe';
                var expected = '';
                var actual   = cp._getSubpageFromHash(hash);

                expect(actual).to.equal(expected);
            });
        });
    });
});

/* global describe: false, expect:false, Backbone: false, _:false, $:false, sinon:false */

var utils   = require('../dist/utils/utils-ui');
var helpers = require('./helpers/helpers');

describe('Utility - UI (utils/utils-ui.js)', function () {

    it('utilsUI should exist', function () {
        expect(utils).to.exist;
    });

    describe('"unwrapView" Tests', function () {

        var rootView;
        var childView;

        var createChildView = function () {
            return new Backbone.Marionette.ItemView({
                template: _.template('<div id=\'child\'></div>')
            });
        };

        beforeEach(function () {
            rootView = helpers.viewHelpers.createRootView();
            childView = createChildView();
            rootView.render();
        });

        afterEach(function () {
            childView.destroy();
            rootView.destroy();
        });

        it('The function exists', function () {
            expect(utils.unwrapView).to.exist.and.be.a('function');
        });

        // This one is here as proof that the next one does what it says.
        it('Without calling "unwrapView", resulting markup has an '+
            'additional wrapping div', function () {
            rootView.showChildView('contentRegion', childView);
            var childDiv = rootView.$el.find('#contentView > div > #child');
            expect(childDiv.length).to.equal(1);
        });

        it('"unwrapView" removes the additional div that Backbone adds', function () {
            childView.onRender = function () {
                utils.unwrapView(childView);
            };
            rootView.showChildView('contentRegion', childView);

            var childDiv = rootView.$el.find('#contentView > #child');
            var childWithWrapperDiv= rootView.$el.find('#contentView > div #child');

            expect(childDiv.length).to.equal(1);
            expect(childWithWrapperDiv.length).to.equal(0);
        });
    }); // "unwrapView" Tests

    describe('"scrollTo" tests', function () {
        it('"scrollTo" exists.', function () {
            expect(utils.scrollTo).to.exist.and.be.a('function');
        });

        it('"scrollTo" requires a jQuery element', function () {
            var fn = function () {
                utils.scrollTo('Giraffe');
            };

            expect(fn).to.throw(utils.errors.scrollToJQuery);
        });

        it('"getOffsetTop" exists.', function () {
            expect(utils.getOffsetTop).to.exist.and.be.a('function');
        });
    }); // "scrollTo" tests

    describe('"isValidURL" tests', function () {
        it('Should exist', function () {
            expect(utils.isValidURL).to.exist.and.be.a('function');
        });

        it('Should return true if valid input', function () {
            var url = 'https://api.oneamerica.com/secure/oso/auth';
            expect(utils.isValidURL(url)).to.be.true;

            url = 'https://www.oneamerica.com?targetuser=james';
            expect(utils.isValidURL(url)).to.be.true;

            url = 'https://www.oneamerica.com';
            expect(utils.isValidURL(url)).to.be.true;
        });

        it('Should return false if invalid input', function () {
            var url = 'oneamerica.com';
            expect(utils.isValidURL(url)).to.be.false;

            url = 'https://www.oneamer  ica.com';
            expect(utils.isValidURL(url)).to.be.false;
        });
    }); // "isValidURL" tests

    describe('"parseSearchParams" tests', function () {

        it('Includes all of the params we are interested in', function () {
            var input = {
                col        : 0,
                dir        : 'asc',
                length     : 25,
                searchTerm : 'Gojira',
                searchType : 'Godzilla',
                start      : 25,
                noCache    : true
            };
            var expectedOutput = {
                searchState : {
                    col     : 0,
                    dir     : 'asc',
                    length  : 25,
                    start   : 25,
                    noCache : true
                },
                searchTerm : 'Gojira',
                searchType : 'Godzilla'
            };
            var output = utils.parseSearchParams(input);
            expect(output).to.deep.equal(expectedOutput);
        });

        it('Does not include the unrelated params', function () {
            var input = {
                name         : 'Gojira',
                rank         : 'King',
                serialNumber : '7',
                searchTerm   : 'Kaiju'
            };
            var expectedOutput = {
                searchTerm : 'Kaiju'
            };
            var output = utils.parseSearchParams(input);
            expect(output).to.deep.equal(expectedOutput);
        });
    }); // "parseSearchParams" tests

    describe('"generateSpinnerElement" method', function () {
        var spinnerElement;

        beforeEach(function () {
            spinnerElement =  utils.generateSpinnerElement();
        });

        afterEach(function () {
            spinnerElement = null;
        });

        it('should exist', function () {
            expect(utils.generateSpinnerElement).to.exist;
        });

        it('should return spinner element', function () {
            expect(Backbone.$(spinnerElement).hasClass('spinner')).to.be.true;
        });

        it('should spinner element positoned absolute by default', function () {
            expect(Backbone.$(spinnerElement).attr('style')).to.be.contain('position: absolute');
        });

        it('should spinner element positoned static if param "static" ', function () {
            spinnerElement =  utils.generateSpinnerElement('', 'static');
            expect(Backbone.$(spinnerElement).attr('style')).to.be.contain('position: static');
        });

    }); // "generateSpinnerElement" method

    describe('"sendFormErrorsToAnalytics" Method', function () {
        var analyticsChannel;
        var errors;
        var fieldMap;
        var trackExceptionStub;

        describe('Should throw an error', function () {
            var testString = 'foo';
            var emptyObj   = {};

            errors  = {
                fileErrorText : 'There was an error with an attachment'
            };

            fieldMap = {
                fileErrorText : 'attachments'
            };

            var fn;

            it('if errors parameter is not an object', function () {
                fn = function () {
                    utils.sendFormErrorsToAnalytics(testString);
                };

                expect(fn).to.throw(utils.errors.sendFormErrorsToAnalytics.emptyErrors);
            });

            it('if errors parameter is empty object', function () {
                fn = function () {
                    utils.sendFormErrorsToAnalytics(emptyObj);
                };

                expect(fn).to.throw(utils.errors.sendFormErrorsToAnalytics.emptyErrors);
            });

            it('if fieldMap parameter is not an object', function () {
                fn = function () {
                    utils.sendFormErrorsToAnalytics(errors, testString, testString);
                };

                expect(fn).to.throw(utils.errors.sendFormErrorsToAnalytics.emptyFieldMap);
            });

            it('if fieldMap parameter is empty object', function () {
                fn = function () {
                    utils.sendFormErrorsToAnalytics(errors, emptyObj, testString);
                };

                expect(fn).to.throw(utils.errors.sendFormErrorsToAnalytics.emptyFieldMap);
            });

            it('if formName parameter is not defined', function () {
                fn = function () {
                    utils.sendFormErrorsToAnalytics(errors, fieldMap);
                };

                expect(fn).to.throw(utils.errors.sendFormErrorsToAnalytics.formName);
            });

        });     // Should throw an error


        it('should call trackException() on analyticsChannel', function () {
            analyticsChannel   = Backbone.Radio.channel('analytics');
            trackExceptionStub = this.sinon.stub();

            analyticsChannel.on('trackException', trackExceptionStub);

            errors = {
                fieldErrorText : 'This is an error'
            };

            fieldMap = {
                fieldErrorText : 'field'
            };

            utils.sendFormErrorsToAnalytics(errors, fieldMap, 'foo');

            expect(trackExceptionStub).to.have.been.calledOnce;

            analyticsChannel.stopListening();
        });

        it('should map error keys to field names in fieldMap parameter', function () {
            var expectedMessage;
            var formName;
            var messageString;

            analyticsChannel   = Backbone.Radio.channel('analytics');
            trackExceptionStub = this.sinon.stub();

            analyticsChannel.on('trackException', trackExceptionStub);

            errors = {
                blueLightSaberErrorText : ' Adegan crystal is not seated correctly',
                redLightSaberErrorText  : 'Operator is breathing too noisily'
            };

            fieldMap = {
                blueLightSaberErrorText : 'Luke Skywalker',
                redLightSaberErrorText  : 'Darth Vader'
            };

            formName = 'Star Wars';

            expectedMessage = JSON.stringify({
                form   : formName,
                errors : {
                    'Luke Skywalker' : errors.blueLightSaberErrorText,
                    'Darth Vader'    : errors.redLightSaberErrorText
                }
            });

            utils.sendFormErrorsToAnalytics(errors, fieldMap, 'Star Wars');

            messageString = trackExceptionStub.getCalls()[0].args[0].message;

            expect(messageString).to.equal(expectedMessage);

            analyticsChannel.stopListening();
        });

    }); // "sendFormErrorsToAnalytics" Method

    describe('"addTargetUserQueryParamToURL" method', function () {
        it('should exist', function () {
            expect(utils.addTargetUserQueryParamToURL).to.exist.and.to.be.a.function;
        });

        it('should throw error if url parameter is null', function () {
            var fn = function () {
                utils.addTargetUserQueryParamToURL();
            };
            expect(fn).to.throw(utils.errors.urlIsMissing);
        });

        it('should throw error if url parameter is not valid hash/http URL', function () {
            var fn = function () {
                utils.addTargetUserQueryParamToURL(
                    'api.oneamerica .com/oso/secure/web/pinpoint'
                );
            };
            expect(fn).to.throw(utils.errors.invalidURL);
        });

        it('should add "targetuser" query to hash url if user is in impersonate state', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return 'john';
            });

            var hashUpdated = utils.addTargetUserQueryParamToURL('#pending-policy-manager');
            var expected = '#pending-policy-manager?targetuser=john';

            expect(hashUpdated).to.be.equal(expected);
            userChannel.stopReplying();
        });

        it('should add "targetuser" query to http url with hash, '+
                'if user is in impersonate state', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return 'john';
            });

            var hashUpdated = utils.addTargetUserQueryParamToURL(
                'https://onesourceonline.oneamerica.com#pending-policy-manager'
            );
            var expected = 'https://onesourceonline.oneamerica.com#pending-policy-manager' +
                '?targetuser=john';

            expect(hashUpdated).to.be.equal(expected);
            userChannel.stopReplying();
        });

        it('should add "targetuser" query to http url if user is in impersonate state', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return 'john';
            });

            var hashUpdated = utils.addTargetUserQueryParamToURL(
                'https://api.oneamerica.com/oso/secure/web/pinpoint'
            );
            var expected = 'https://api.oneamerica.com/oso/secure/web/pinpoint?targetuser=john';

            expect(hashUpdated).to.be.equal(expected);
            userChannel.stopReplying();
        });

        it('should not update "targetuser" query to hash if hash already have', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return 'john';
            });

            var hashUpdated = utils.addTargetUserQueryParamToURL('#pending-policy-manager' +
                '?targetuser=smith');
            var hasExpected = '#pending-policy-manager?targetuser=smith';

            expect(hashUpdated).to.be.equal(hasExpected);
            userChannel.stopReplying();
        });

        it('should return original has if there is no impersonated WebId', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return null;
            });

            var hashUpdated = utils.addTargetUserQueryParamToURL('#pending-policy-manager');
            var hasExpected = '#pending-policy-manager';

            expect(hashUpdated).to.be.equal(hasExpected);
            userChannel.stopReplying();
        });
    }); // "addTargetUserQueryParamToURL" method

    describe ('"buildHrefURL" method', function () {
        it('Should exist', function () {
            expect(utils.buildHrefURL).to.exist.and.be.a('function');
        });

        it('Should throw error if pageLink parameter is not string', function () {
            var fn = function () {
                utils.buildHrefURL({});
            };
            expect(fn).to.throw(utils.errors.pageLinkExpectedAsString);
        });

        it('Should return expected result', function () {
            var result =  utils.buildHrefURL('#pending-policy-manager', {
                producerId : 1000023
            });
            var expectedResult = '#pending-policy-manager?producerId=1000023';
            expect(result).to.equal(expectedResult);
        });

        it('Should return expected result when there is no parameters', function () {
            var result =  utils.buildHrefURL('#pending-policies/all');
            var expectedResult = '#pending-policies/all';
            expect(result).to.equal(expectedResult);
        });

        it('Should prefix # if pageLink is missing it', function () {
            var result =  utils.buildHrefURL('pending-policy-manager', {
                producerId : 1000023
            });
            var expectedResult = '#pending-policy-manager?producerId=1000023';
            expect(result).to.equal(expectedResult);
        });

        it('Should add "&" as seperator if pageLink has "?" already', function () {
            var result =  utils.buildHrefURL('#pending-policy-manager?producerId=100', {
                start : 0
            });
            var expectedResult = '#pending-policy-manager?producerId=100&start=0';
            expect(result).to.equal(expectedResult);
        });

        it('Should add "targetuser" query param if user is in impersonated state', function () {
            var userChannel = Backbone.Radio.channel('user');
            userChannel.reply('getImpersonatedWebId', function () {
                return 'john';
            });

            var result =  utils.buildHrefURL('#pending-policy-manager?producerId=100', {
                start : 0
            });
            var expectedResult = '#pending-policy-manager?producerId=100&start=0&targetuser=john';
            expect(result).to.equal(expectedResult);

            userChannel.stopReplying();
            userChannel = null;
        });
    }); // "buildHrefURL" method

    describe('"convertWcmLinks" method', function () {

        describe('Replace URL prefix of all "a" elements with "#c/" ' +
            'if path matches', function () {

            var addTargetUserQueryParamToURLSpy;
            var view;
            var aElements;

            beforeEach(function () {
                var testBody =
                    '<h2>ELEVATE</h2>' +
                    '<img src="/wps/wcm/connect/764e7e804c8062758d71ed910725b403/'+
                    'ELV_216w.png?MOD=AJPERES"/>' +
                    '0<a href="/wps/wcm/connect/indcontent/OSO/underwriting/programs-retail">'+
                    'Some Content</a>'+
                    '1<a href="/wps/wcm/myconnect/indcontent/OSO/myTestContent">Test</a>'+
                    '2<a href="#some-hash"></a>'+
                    '3<a href="/wps/wcm/connect/indcontent/oso/'+
                    'documents/service-only-sample-pages" class="icon-pdf"></a>'+
                    '4<a href="#c/some-products/link"></a>'+
                    '5<a href="/wps/wcm/connect/indcontent/oso/external" '+
                    'class="icon-external"></a>'+
                    '6<a href="mailto:test@oa.com" class="icon-email"></a>'+
                    '7<a href="/wps/wcm/connect/indcontent/oso/video" class ="icon-video"></a>'+
                    '8<a href="/wps/wcm/connect/indcontent/oso/products/oso-case-insensitive"></a>'+
                    '9<a href="/wps/wcm/myconnect/indcontent/oso/pdf-content"></a><i class="' +
                    'fa gray-80 fa-file-pdf-o"></i>' +
                    '10<a href="/wps/wcm/connect/indcontent/oso/zip-content"></a><i class="' +
                    'fa gray-80 fa-file-archive-o"></i>'+
                    '11<a href="/wps/wcm/connect/indcontent/oso/modal" data-toggle="modal"></a>';

                view = new Backbone.Marionette.ItemView({
                    template : _.template('<%= body%>'),
                    model    : new Backbone.Model({
                        body : testBody
                    })
                });

                view.render();

                addTargetUserQueryParamToURLSpy = sinon.spy(utils, 'addTargetUserQueryParamToURL');

                utils.convertWcmLinks(view);
            });

            afterEach(function () {
                addTargetUserQueryParamToURLSpy.restore();
                view.destroy();
            });

            it('URLs should match', function () {
                aElements = view.$el.find('a');

                expect($(aElements[0]).attr('href'))
                    .to.equal('#c/underwriting/programs-retail');

                expect($(aElements[1]).attr('href'))
                    .to.equal('#c/myTestContent');
            });

            it('URL pattern search is case-insensitive for conversion', function () {
                aElements = view.$el.find('a');

                expect($(aElements[8]).attr('href'))
                    .to.equal('#c/products/oso-case-insensitive');
            });

            it('URL should not updated if pattern not match', function () {
                aElements = view.$el.find('a');
                expect($(aElements[2]).attr('href'))
                    .to.equal('#some-hash');
                expect($(aElements[4]).attr('href')).to.equal('#c/some-products/link');
            });

            it('URL should not updated if elements has CSS class name '+
                'which need to ignore from conversion', function () {
                aElements = view.$el.find('a');

                expect($(aElements[3]).attr('href'))
                    .to.equal(
                    '/wps/wcm/connect/indcontent/oso/documents/service-only-sample-pages'
                );

                expect($(aElements[5]).attr('href'))
                    .to.equal('/wps/wcm/connect/indcontent/oso/external');
                expect($(aElements[6]).attr('href')).to.equal('mailto:test@oa.com');
                expect($(aElements[7]).attr('href'))
                    .to.equal('/wps/wcm/connect/indcontent/oso/video');
            });

            it('URL should not be updated if next element is a PDF icon', function () {
                aElements = view.$el.find('a')[9];

                expect($(aElements).attr('href'))
                    .to.equal('/wps/wcm/myconnect/indcontent/oso/pdf-content');
            });

            it('URL should not be updated if next element is a PDF icon', function () {
                aElements = view.$el.find('a')[10];

                expect($(aElements).attr('href'))
                    .to.equal('/wps/wcm/connect/indcontent/oso/zip-content');
            });

            it('URL should not be updated if "data-toggle" attribute '+
                'is "modal"', function () {
                aElements = view.$el.find('a')[11];

                expect($(aElements).attr('href')).to
                    .equal('/wps/wcm/connect/indcontent/oso/modal');
            });

            describe('`utils.addTargetUserQueryParamToURL`', function () {

                it('should be called for each matching "a" tag', function () {
                    expect(addTargetUserQueryParamToURLSpy.callCount).to.equal(4);
                });
            });

        });
    }); // "convertWcmLinks" method

    describe('`utils.setVisibilityOnSmartScrollMessage`', function () {
        var fakeTableContainer;
        var View = Backbone.Marionette.ItemView.extend({
            template : _.template(
                '<html><body><div class="main-container container">' +
                '<div id="table-container">' +
                '<table class="table-responsive">' +
                '<thead><tr><th>Foo</th><th>Bar</th></tr></thead>' +
                '<tbody><tr><td>Something</td><td>Something else</td></tr></tbody></table>' +
                '</div><div class="table-responsive-instruction">Scrolly scrolly</div>' +
                '</div></body></html>'
            ),
            ui : {
                tableContainer : '#table-container',
                instructionDiv : '.table-responsive-instruction'
            }
        });
        var view;

        before(function () {
            view = new View();
            view.render();
            // Since the scrollWidth and offsetWidth are read-only properties on the DOM and
            // I wasn't able to adjust the width to cause scrollbars in the tests, a fake
            // "DOM element" needs to be created. It's used to hold values which, on a normal
            // DOM element, would cause the "table-responsive-instruction" to be shown. This
            // fake element is then added to a fake table container which is an instance of a
            // jQuery object.
            var fakeDomElement = {
                scrollWidth : 200,
                offsetWidth  : 200
            };
            fakeTableContainer = view.$el.find('#foo');

            fakeTableContainer.push(fakeDomElement);
        });

        after(function () {
            view.destroy();
        });

        it('should hide "div.table-responsive-instruction" if the table container ' +
            'has no scroll bars present', function () {
            utils.setVisibilityOnSmartScrollMessage(fakeTableContainer, view.ui.instructionDiv);

            expect(view.ui.instructionDiv.hasClass('hidden')).to.be.true;
        });

        it('should show "div.table-responsive-instruction" if the table container ' +
            'has scroll bars present', function () {
            // Make the scrollWidth larger than offsetWidth to simulate horizontal scroll bars
            fakeTableContainer[0].scrollWidth = 500;

            utils.setVisibilityOnSmartScrollMessage(fakeTableContainer, view.ui.instructionDiv);

            expect(view.ui.instructionDiv.hasClass('hidden')).to.be.false;
        });

        describe('should not change CSS classes on '+
            '"div.table-responsive-instruction"', function () {
            var expected;

            beforeEach(function () {
                expected = view.ui.instructionDiv.attr('class');
            });

            it('when tableContainer is null', function () {

                utils.setVisibilityOnSmartScrollMessage(null, view.ui.instructionDiv);

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });

            it('when tableContainer length is 0', function () {
                utils.setVisibilityOnSmartScrollMessage(view.$el.find('#foo'),
                    view.ui.instructionDiv);

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });

            it('when tableContainer is not a jQuery object', function () {
                utils.setVisibilityOnSmartScrollMessage([0, 1, 2], view.ui.instructionDiv);

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });

            it('when instructionDiv is null', function () {

                utils.setVisibilityOnSmartScrollMessage(view.ui.tableContainer, null);

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });

            it('when instructionDiv length is 0', function () {
                utils.setVisibilityOnSmartScrollMessage(view.ui.tableContainer,
                    view.$el.find('#foo'));

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });

            it('when instructionDiv is not a jQuery object', function () {

                utils.setVisibilityOnSmartScrollMessage(view.ui.tableContainer, [0, 1, 2]);

                expect(view.ui.instructionDiv.attr('class')).to.equal(expected);
            });
        });

    });

    describe('mergeRelativeToAbsoluteURL method', function () {
        it('Should exist', function () {
            expect(utils.mergeRelativeToAbsoluteURL).to.exist.and.be.a('function');
        });

        it('should return error if baseURL is invalid', function (){
            var fn = function () {
                utils.mergeRelativeToAbsoluteURL('/base/secure/rest','../some/path');
            };

            expect(fn).to.throw(utils.errors.invalidBaseURL);
        });

        it('should return error if relative path is missing', function (){
            var fn = function () {
                utils.mergeRelativeToAbsoluteURL('https://stoso.oa.com/oso/');
            };

            expect(fn).to.throw(utils.errors.invalidRelativePath);
        });

        it('should return correct output', function () {
            var url =  utils.mergeRelativeToAbsoluteURL(
                'https://stonesourceonline.oneamerica.com/one/two/three/',
                '../../../four?bar=456'
            );
            var expected  = 'https://stonesourceonline.oneamerica.com/four?bar=456';
            expect(url).to.equal(expected);
        });
    });
});

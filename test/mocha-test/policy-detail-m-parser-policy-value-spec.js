/* global expect:false, describe:false, it:false, $:false */

var PolicyValue = require('../dist/pages/policy/models/parsers/policy-value');
var helpers     = require('./helpers/helpers');

describe('Policy Value Model parser (pages/policy/models/parsers/policy-value.js)', function () {
    var activePolicyCopy;
    var policyValue;

    before(function () {
        policyValue = new PolicyValue();
    });

    beforeEach(function () {
        activePolicyCopy = $.extend(true,{},helpers.policyData.activePolicyDetail);
    });

    after(function () {
        policyValue.destroy();
    });

    describe('setCashValueAmountLabel method', function () {

        it('exists as a function', function () {
            expect(policyValue.setCashValueAmountLabel).to.exist.and.be.a('function');
        });

        describe('When "cashValueAmount" exists', function () {

            it('should set "cashValueAmountLabel" to "Accumulated Value" when ' +
                '"product.productName" begins with "Annuity Care"', function () {
                var response;
                activePolicyCopy.product.productName = 'Annuity Care Something or Other';

                response = policyValue.setCashValueAmountLabel(activePolicyCopy);

                expect(activePolicyCopy.policyValue).not.to.have.property('cashValueAmountLabel');
                expect(response.policyValue.cashValueAmountLabel).to.equal('Accumulated Value');
            });

            it('should set "cashValueAmountLabel" to "Cash Value" when ' +
                '"product.productTypeCode" equals "VA"', function () {
                var response;
                activePolicyCopy.product.productName     = 'Not Annuity Care';
                activePolicyCopy.product.productTypeCode = 'VA';

                response = policyValue.setCashValueAmountLabel(activePolicyCopy);

                expect(response.policyValue.cashValueAmountLabel).to.equal('Cash Value');
            });

            it('should not set "cashValueAmountLabel" if "productName" does ' +
                'not begin with "Annuity Care" and "productTypeCode" is not "VA"', function () {
                var response;
                activePolicyCopy.product.productName     = 'Not Annuity Care';
                activePolicyCopy.product.productTypeCode = 'Not VA';

                response = policyValue.setCashValueAmountLabel(activePolicyCopy);

                expect(activePolicyCopy.policyValue).not.to.have.property('cashValueAmountLabel');
                expect(response.policyValue).not.to.have.property('cashValueAmountLabel');
            });
        });

        it('should not set "cashValueAmountLabel" if "cashValueAmount" does not exist', function () {
            var response;
            delete activePolicyCopy.policyValue.cashValueAmount;

            response = policyValue.setCashValueAmountLabel(activePolicyCopy);
            expect(activePolicyCopy.policyValue).not.to.have.property('cashValueAmountLabel');
            expect(response.policyValue).not.to.have.property('cashValueAmountLabel');
        });
    });

    describe('setNetSurrenderValueAmountLabel method', function () {

        it('exists as a function', function () {
            expect(policyValue.setNetSurrenderValueAmountLabel).to.exist.and.be.a('function');
        });

        describe('When "netSurrenderValueAmount" exists', function () {

            it('should set "netSurrenderValueAmountLabel', function () {
                var response;

                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue).to.have
                    .property('netSurrenderValueAmountLabel');
            });

            describe('should set "netSurrenderValueAmountLabel" value to "Cash Surrender Value"',
                function () {
                    var expectedValue = 'Cash Surrender Value';

                    it('when "product.productTypeCode" is "INXAN"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'INXAN';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productTypeCode" is "FDIA"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'FDIA';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productName" starts with "Annuity Care"', function () {
                        var response;
                        // Change productTypeCode so that we will trigger off of productName
                        activePolicyCopy.product.productTypeCode = 'WL';
                        activePolicyCopy.product.productName     = 'Annuity Care ABCDEFG';
                        response= policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productName" starts with "Asset-Care" and ' +
                        '"product.productTypeCode" equals "Life"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'Life';
                        activePolicyCopy.product.productName     = 'Asset-Care II';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productName" starts with "Legacy Care" and ' +
                        '"product.productTypeCode" equals "Annuity"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'Annuity';
                        activePolicyCopy.product.productName     = 'Legacy Care II';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                }); // should set "netSurrenderValueAmountLabel" value to "Cash Surrender Value"

            describe('should set "netSurrenderValueAmountLabel" value to "Net Cash Value"',
                function () {
                    var expectedValue = 'Net Cash Value';

                    it('when "product.productTypeCode" is "FA"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'FA';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productTypeCode" is "ISL"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'ISL';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productTypeCode" is "UL"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'UL';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                    it('when "product.productTypeCode" is "VUL"', function () {
                        var response;
                        activePolicyCopy.product.productTypeCode = 'VUL';
                        response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                        expect(response.policyValue.netSurrenderValueAmountLabel).to
                            .equal(expectedValue);
                    });

                }); // should set "netSurrenderValueAmountLabel" value to "Net Cash Value"

            it('should not set "netSurrenderValueAmountLabel" if ' +
                '"netSurrenderValueAmount" does not exist', function () {
                var response;
                delete activePolicyCopy.policyValue.netSurrenderValueAmount;
                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue).not.to.have.property('netSurrenderValueAmountLabel');
            });
        });

        describe('Should not set "netSurrenderValueAmountLabel"', function () {

            it('when "policyValue" is falsey', function () {
                var response;
                activePolicyCopy.policyValue = {};

                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue.netSurrenderValueAmountLabel).not.to.exist;
            });

            it('when "policyValue.netSurrenderValueAmount" is falsey', function () {
                var response;
                delete activePolicyCopy.policyValue.netSurrenderValueAmount;

                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue.netSurrenderValueAmountLabel).not.to.exist;
            });

            it('when "product.productTypeCode" is falsey', function () {
                var response;
                delete activePolicyCopy.product.productTypeCode;

                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue.netSurrenderValueAmountLabel).not.to.exist;
            });

            it('when "product.productName" is falsey', function () {
                var response;
                delete activePolicyCopy.product.productName;

                response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

                expect(response.policyValue.netSurrenderValueAmountLabel).not.to.exist;
            });

        }); // Should not set "netSurrenderValueAmountLabel"

        it('should not modify response object passed in when ' +
            '"policyValue" parameter exists', function () {
            var copyOfActivePolicyCopy = $.extend(true, {}, activePolicyCopy);

            policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

            expect(copyOfActivePolicyCopy).to.deep.equal(activePolicyCopy);
        });

        it('should return exact copy of response object passed in when ' +
            '"policyValue" does not exist', function () {
            var response;
            delete activePolicyCopy.policyValue;

            response = policyValue.setNetSurrenderValueAmountLabel(activePolicyCopy);

            expect(response).to.deep.equal(activePolicyCopy);
        });

    }); // setNetSurrenderValueAmountLabel method

});
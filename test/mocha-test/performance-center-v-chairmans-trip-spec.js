/* global expect:false, $:false, isCanvasLoaded:false */

var helpers = require('./helpers/helpers');

var PerformanceCenterModel = require(
    '../dist/modules/performance-center/models/performance-center-m'
);
var PerformanceCenterView = require(
    '../dist/modules/performance-center/views/performance-center-v'
);


describe('Performance Center View : Chairmans Trip (CT) '+
    '(modules/performance-center/views/performance-center-v.js)', function () {

    var responseBody   = helpers.pcData.responseBody;
    var ajaxStub;
    var root;
    var view;

    var buildView = function(pcData) {
        var model;
        var pcView;
        root = helpers.viewHelpers.createRootView();
        if (ajaxStub) {
            ajaxStub.restore();
        }
        ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
            'success',
            pcData
        );

        model = new PerformanceCenterModel();
        pcView  = new PerformanceCenterView({
            model : model
        });

        root.render();
        root.contentRegion.show(pcView);

        return pcView;
    };

    before(function () {
        // if canvas does not exist in the global object, skip this suite
        if (!isCanvasLoaded) {
            this.test.parent.pending = true;
            this.skip();
        }
        view = buildView(responseBody);
    });

    after(function (){
        ajaxStub.restore();
        root.destroy();
        view.destroy();
    });

    it('dial chart values', function () {
        expect(view.chairmansTripRegion.$el.find('.chart-outer').data('percent'))
            .to.equal(40000);
        expect(view.chairmansTripRegion.$el.find('.chart-inner').data('percent'))
            .to.equal(15);
    });

    it ('dial chart center labels', function () {
        expect(view.chairmansTripRegion.$el.find('.chart-outer div:first').text().trim())
            .to.equal('$40,000');
        expect(view.chairmansTripRegion.$el.find('.chart-outer div:last').text().trim())
            .to.equal('15 lives');
    });

    it(' bullets values should be formatted correctly', function () {
        expect(view.chairmansTripRegion.$el.find('#chairmansTrip-required-remaining-fypc')
            .text().trim()).to.equal('$31,049');
        expect(view.chairmansTripRegion.$el.find('#chairmansTrip-required-fypc').text()
            .trim()).to.equal('$51,953');
        expect(view.chairmansTripRegion.$el.find('#chairmansTrip-required-remaining-lives')
            .text().trim()).to.equal('15');
        expect(view.chairmansTripRegion.$el.find('#chairmansTrip-required-lives')
            .text().trim()).to.equal('15');
    });

    it('Rules PDF link should point to Retail version when "type" is "retail"', function () {
        var retailPdf = view.chairmansTripRegion.currentView.pdfLink.retail;
        var rulesLink = view.chairmansTripRegion.$el.find('#ctPdfLink').attr('href');

        expect(rulesLink).to.equal(retailPdf);
    });

    describe('Chairman\'s Trip "conferenceYear" value', function () {

        it('should be displayed in the title', function () {
            var expected = 'Chairman\'s Trip ' +
                view.model.get('chairmansTrip').conferenceYear;
            var headerText = helpers.viewHelpers.removeDuplicateWhitespace(
                view.chairmansTripRegion.$el.find('h4:eq(0)').text().trim()
            );

            expect(headerText).contains(expected);
        });

        it('should be displayed in the modal window', function () {
            var ctModalHeader = view.$el.find('#chairmansTripHelpTextModalLabel').text().trim();
            var expected      = 'Chairman\'s Trip ' +
                view.model.get('chairmansTrip').conferenceYear;

            expect(ctModalHeader).to.equal(expected);
        });

        it('should be displayed in title attribute of span.fa-stack', function () {
            var titleText = view.chairmansTripRegion.$el.find('h4 .fa-stack').attr('title');
            var expected  = 'Chairman\'s Trip ' +
                view.model.get('chairmansTrip').conferenceYear + ' Help';

            expect(titleText).to.equal(expected);
        });
    });

    describe('show check mark while met goal', function () {

        var responseBodyCTQFypcGoals = helpers.pcData.responseBodyCTQFypcGoals;

        before(function () {
            view = buildView(responseBodyCTQFypcGoals);
        });

        after(function () {
            view.destroy();
        });

        it('Chairman\'s Trip Qualified - Green Check mark should be displayed if ' +
            '"qualifyingGoalAchievedFlag" is TRUE', function () {
            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-qualified-fypc-goal-text')
                .text().trim()).to.equal('Qualifying FYC Goal Achieved!');

            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-qualified-lives-goal-text')
                .text().trim()).to.equal('Lives Goal Achieved!');

            //green check should displayed
            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-qualified .fa-check').length)
                .to.equal(1);
        });

        it('Chairman\'s Trip On Schedule - Green Check mark should be displayed if ' +
            '"qualifyingGoalAchievedFlag" is TRUE', function () {

            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-on-schedule-fypc-goal-text')
                .text().trim()).to.equal('Qualifying FYC Goal Achieved!');

            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-on-schedule-lives-goal-text')
                .text().trim()).to.equal('Lives Goal Achieved!');

            //green check should displayed
            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-on-schedule .fa-check').length)
                .to.equal(1);
        });
    });

    describe('Chairman\'s Trip Ring Color when "qualifyingGoalAchievedFlag" is FALSE', function () {

        var modifiedCTQFypcGoals = $.extend(true, {}, helpers.pcData.responseBodyCTQFypcGoals);

        // Set qualifyingGoalAchievedFlag to FALSE
        modifiedCTQFypcGoals.chairmansTrip.qualifyingGoalAchievedFlag = false;

        var ctView;
        before(function () {
            view = buildView(modifiedCTQFypcGoals);
            ctView = view.chairmansTripRegion.currentView;
        });

        after(function () {
            view.destroy();
        });

        describe('Qualified Dial', function () {

            it('outer dial should be green when ' +
                '"requiredRemainingQualFypcNumber" is 0', function () {
                var expected   = ctView.dialColorComplete;
                var outerColor = ctView.dialChartOptionsOne.outerChartOptions.barColor;

                expect(ctView.model.get('qualifyingGoalAchievedFlag')).to.be.false;
                expect(ctView.model.get('requiredRemainingQualFypcNumber')).to.equal(0);
                expect(outerColor).to.equal(expected);
            });

            it('inner dial should be green when "requiredRemainingLives" is 0', function () {
                var expected   = ctView.dialColorComplete;
                var innerColor = ctView.dialChartOptionsOne.innerChartOptions.barColor;

                expect(ctView.model.get('qualifyingGoalAchievedFlag')).to.be.false;
                expect(ctView.model.get('requiredRemainingLives')).to.equal(0);
                expect(innerColor).to.equal(expected);
            });

        }); // Qualified Dial

        describe('On Schedule Dial', function () {

            it('outer dial should be green when ' +
                '"onscheduleRemainingQualFypcNumber" is 0', function () {
                var expected   = ctView.dialColorComplete;
                var outerColor = ctView.dialChartOptionsTwo.outerChartOptions.barColor;

                expect(ctView.model.get('qualifyingGoalAchievedFlag')).to.be.false;
                expect(ctView.model.get('onscheduleRemainingQualFypcNumber')).to.equal(0);
                expect(outerColor).to.equal(expected);
            });

            it('inner dial should be green when "onscheduleRemainingLives" is 0', function () {
                var expected   = ctView.dialColorComplete;
                var innerColor = ctView.dialChartOptionsTwo.innerChartOptions.barColor;

                expect(ctView.model.get('qualifyingGoalAchievedFlag')).to.be.false;
                expect(ctView.model.get('onscheduleRemainingLives')).to.equal(0);
                expect(innerColor).to.equal(expected);
            });

        }); // On Schedule Dial

    }); // Chairman's Trip Ring Color when "qualifyingGoalAchievedFlag" is FALSE

    describe('Set red color to FYPC and Lives graph and label', function () {

        var responseBodyCTQFypcNegative = helpers.pcData.responseBodyCTQFypcNegative;

        before(function () {
            view = buildView(responseBodyCTQFypcNegative);
        });

        after(function () {
            view.destroy();
        });

        it('percent value of outer graph should be an absolute value', function () {
            expect(view.chairmansTripRegion.$el.find('#chairmansTrip-qualified .chart-outer')
                .data('percent')).to.equal(11953);
        });

        it('Color of FYPC and Lives labels should be in red ' +
            'while values are in negative', function () {
            expect(view.chairmansTripRegion.$el.find('.actual-fypc-number')
                .hasClass('oa-banner-red')).to.equal(true);

            expect(view.chairmansTripRegion.$el.find('.actual-lives')
                .hasClass('oa-banner-red')).to.equal(true);
        });

        it('Rules PDF link should point to Retail version when "type" is "ib"', function () {
            var ibPdf     = view.chairmansTripRegion.currentView.pdfLink.ib;
            var rulesLink = view.chairmansTripRegion.$el.find('#ctPdfLink').attr('href');

            expect(rulesLink).to.equal(ibPdf);
        });
    });
});
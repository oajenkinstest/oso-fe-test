/* global describe:false, $:false, expect:false, sinon:false, Backbone:false */

// load behaviors
require('../dist/modules/behaviors/index');

var config  = require('../dist/config/config');
var WcmView = require('../dist/modules/wcm-content/views/wcm-content-v');
var helpers = require('./helpers/helpers');

describe('WCM Content View (modules/wcm-content/views/wcm-content-v.js)', function () {
    var validationError = WcmView.prototype.errors.wcmPathMissing;

    before(function () {
        // Since this.sinon is set within a `beforeEach` in setup/helpers.js, it might not be
        // set yet if this is the first spec file to be run, so we have to set it here to be able
        // to use it in this `before` block. (`before` blocks run before `beforeEach` blocks)
        if (!this.sinon) {
            this.sinon = sinon.sandbox.create();
        }
    });

    /*
     Basic tests on instantiation of a wcm view
     */
    it('WcmView exists', function() {
        expect(WcmView).to.exist;
    });

    it('WcmView instantiation should throw error if wcmPath is missing', function () {
        document.location.hash = '';
        var fn = function() {
            var view = new WcmView(); // eslint-disable-line no-unused-vars
        };

        expect(fn).to.throw(validationError);
    });

    it('WcmView instantiation successful if url contain "#c/" pattern ', function () {

        //replace hash with wcm path
        document.location.hash = '#c/Marketing/ELEVATE';
        var fn = function() {
             var view = new WcmView(); // eslint-disable-line no-unused-vars
        };

        expect(fn).to.not.throw(Error);
    });

    it('WcmView instantiation successful with strings for wcmPath', function () {
        var fn = function () {
            var view = new WcmView({ // eslint-disable-line no-unused-vars
                wcmPath: 'OnlineServices/Marketing/ELEVATE'
            });
        };

        expect(fn).to.not.throw(Error);
    });

    describe('Rendering tests', function () {
        var ajaxStub;
        var rootView;

        var testBody =
                '<div class="wcmContent"><h2>ELEVATE</h2>' +
                '<img src="/wps/wcm/connect/764e7e804c8062758d71ed910725b403/ELV_216w.png?MOD=AJPERES"/>' +
                '<p>ELEVATE Content</p><a href="/wps/wcm/connect/myTestContent">Test</a></div>';

        before(function () {
            ajaxStub = this.sinon.stub($, 'ajax').yieldsTo(
                'success',
                testBody
            );

            rootView = helpers.viewHelpers.createRootView();
            rootView.render();
        });

        after(function () {
            if (ajaxStub) {
                ajaxStub.restore();
            }
        });

        it('Renders content normally', function () {
            var view = new WcmView({
                wcmPath: 'OnlineServices/Marketing/ELEVATE'
            });

            rootView.showChildView('contentRegion', view);
            var title = view.$el.find('h2');

            // Has a title
            expect(title.text()).to.equal('ELEVATE');

            // has body content, ie siblings of the h1 element - h2, img, p
            // Not too concerned beyond existence, because model tests handle content.
            expect(title.siblings().length).to.equal(3);
        });

        describe('When a WCM link is followed by the user, the view displays WCM content ' +
            'without leaving the OSO application', function () {

            it('behaviors are used in the view to handle links clicked within WCM content',
                function () {

                var wcmContentLinksBehavior = 'wcmContentLinks';
                var view = new WcmView({
                    wcmPath: 'OnlineServices/Marketing/ELEVATE'
                });

                expect(view.behaviors).to.include.keys(wcmContentLinksBehavior);
            });

            it('adds the clicked link\'s href attribute as the "wcmLink" parameter', function () {
                var expectedNameValue = 'wcmLink=/wps/wcm/connect/myTestContent';
                var navEventSpy = this.sinon.spy();
                var view = new WcmView({
                    wcmPath: 'OnlineServices/Marketing/ELEVATE'
                });

                rootView.showChildView('contentRegion', view);

                view.once('nav', navEventSpy);
                view.$el.find('.wcmContent a').click();

                expect(navEventSpy).to.have.been.calledWithMatch(expectedNameValue);
            });

            it('does not display the content within OSO if it contains an invalid file ' +
                'type extension (e.g. ".exe", ".pdf", ".zip"', function () {

                var navEventSpy = this.sinon.spy();
                var testBodyWithInvalidLink = '<div class=".wcmContent">' +
                    '<a href="/wps/wcm/connect/myTestContent.pdf">Test</a></div>';

                ajaxStub.yieldsTo(
                    'success',
                    testBodyWithInvalidLink
                );

                var view = new WcmView({
                    wcmPath: 'OnlineServices/Marketing/ELEVATE'
                });

                view.once('nav', navEventSpy);
                view.$el.find('.wcmContent a').click();

                expect(navEventSpy).to.not.have.been.called;
            });

            it('wcmLink parameter in stateObj is used to render the WCM content', function () {
                var wcmLink = '/wps/wcm/connect/myTestContent';
                var view = new WcmView({
                    wcmPath  : 'OnlineServices/Marketing/ELEVATE',
                    stateObj : {
                        wcmLink : wcmLink
                    }
                });

                expect(view.model.get('wcmPath')).to.equal(wcmLink);
            });

        });

        describe('Method _affixJumpLinksContainer and bindScrollToWithJumpLinks', function () {

            var view;
            var _affixJumpLinksContainerSpy;
            var affixSpy;
            var scrollToSpy;
            var bindScrollToWithJumpLinksSpy;

            beforeEach(function () {
                testBody =
                '<h2>ELEVATE</h2>' +
                '<img src="/wps/wcm/connect/764e7e804c8062758d71ed910725b403/'+
                'ELV_216w.png?MOD=AJPERES"/>' +
                '<p>ELEVATE Content</p><a href="/wps/wcm/connect/myTestContent">Test</a>'+
                '<div class="jumplinks-traveling" id="jumplinks-traveling">'+
                '<button data-toggle="dropdown" class="btn btn-link dropdown-toggle">'+
                'On this page<span class="ace-icon fa fa-caret-down icon-on-right"></span>'+
                '</button><ul class="dropdown-menu dropdown-info dropdown-menu-left">'+
                '<li><a href="#some-hash"></a><li></ul></div></div>';

                ajaxStub.yieldsTo(
                    'success',
                    testBody
                );

                _affixJumpLinksContainerSpy  = this.sinon.spy(WcmView.prototype,
                        '_affixJumpLinksContainer');
                affixSpy                     = this.sinon.spy($.fn,'affix');

                bindScrollToWithJumpLinksSpy = this.sinon.spy(WcmView.prototype,
                        'bindScrollToWithJumpLinks');
                view = new WcmView({
                    wcmPath  : 'OnlineServices/Marketing/ELEVATE'
                });

                rootView.showChildView('contentRegion', view);

                scrollToSpy  = this.sinon.spy(view.utils,'scrollTo');
            });

            afterEach(function () {
                _affixJumpLinksContainerSpy.resetHistory();
                _affixJumpLinksContainerSpy.restore();
                affixSpy.restore();
                scrollToSpy.restore();
                bindScrollToWithJumpLinksSpy.restore();
            });

            it('should exist "_affixJumpLinksContainer"', function () {
                expect(view._affixJumpLinksContainer).to.be.exist;
            });

            it.skip('should called while rendering', function () {
                // This test is skipped since our current test configuration doesn't insert
                // the rootView into the DOM, so the onDomRefresh event is never triggered.
                expect(_affixJumpLinksContainerSpy).to.be.called;
                expect(affixSpy).to.be.calledWithMatch({
                    offset: {}
                });
            });

            it('should exist "bindScrollToWithJumpLinks"', function () {
                expect(view.bindScrollToWithJumpLinks).to.be.exist;
            });

            it('should call "bindScrollToWithJumpLinks"', function () {
                view.ui.jumpLinks.click();
                expect(bindScrollToWithJumpLinksSpy).to.be.called;
            });

            it('should call "scrollTo"', function () {
                view.ui.jumpLinks.click();
                expect(scrollToSpy).to.be.calledWith($('#some-hash'), 40);
            });
        }); // Method _affixJumpLinksContainer and bindScrollToWithJumpLinks

        describe('_handleWcmError method', function () {
            var analyticsChannel;
            var hideSpinnerSpy;
            var response;
            var spinnerChannel;
            var trackExceptionSpy;
            var view;

            before(function () {
                analyticsChannel = Backbone.Radio.channel('analytics');
                spinnerChannel   = Backbone.Radio.channel('spinner');

                hideSpinnerSpy = trackExceptionSpy = this.sinon.spy();

                analyticsChannel.on('trackException', trackExceptionSpy);
                spinnerChannel.on('hide', hideSpinnerSpy);

                view = new WcmView({ // eslint-disable-line no-unused-vars
                    wcmPath: 'OnlineServices/Marketing/ELEVATE'
                });

                // Mimic a 500 HTTP response
                response = {
                    status : 500
                };

                view._handleWcmError(view.model, response);
            });

            after(function () {
                analyticsChannel.stopListening();
                spinnerChannel.stopListening();

                analyticsChannel = spinnerChannel = null;
                view.destroy();
            });

            it('should hide the spinner', function () {
                expect(hideSpinnerSpy).to.have.been.called;
            });

            describe('Should send error message to analytics', function () {
                var analyticsMessage;
                var expectedPath;

                beforeEach(function () {
                    analyticsMessage = trackExceptionSpy.getCalls()[1].args[0].message;
                    expectedPath     = view.model.get('wcmPath');
                });

                it('using analytics channel', function () {
                    expect(trackExceptionSpy).to.have.been.called;
                });

                it('with "wcmPath" value in message', function () {
                    expect(analyticsMessage).to.contain(expectedPath);
                });

                it('with HTTP status code value in message', function () {
                    expect(analyticsMessage).to.contain(response.status);
                });
            });

            describe('Should display an error message to the user', function () {

                it('from the global error handling utilities', function () {
                    var getAjaxMessagesSpy = this.sinon.spy(view.ajaxUtils, 'getAjaxMessages');

                    view._handleWcmError(view.model, response);

                    expect(getAjaxMessagesSpy).to.have.been.calledOnce;
                });

                it('should display the userMessage returned from ajaxUtils for ' +
                    'supported status codes', function () {
                    var expectedMessage;
                    var message;

                    response.status = 503;
                    expectedMessage = view.ajaxUtils.getAjaxMessages(response, null).userMessage;
                    view._handleWcmError(view.model, response);

                    message = view.$el.find('.alert').text();

                    expect(message).to.contain(expectedMessage);
                });

                it('should display a generic error message if the globalAjaxUtils ' +
                    'function does not return a "userMessage" property', function () {
                    var expected = view.errors.genericErrorMessage;
                    var message;

                    // pass in a status code not checked by ajaxUtils
                    response.status = 589;
                    view._handleWcmError(view.model, response);

                    message = view.$el.find('.alert').text();

                    expect(message).to.contain(expected);
                });

                it('Display the global 404 error message when a 404 ' +
                    'response is received', function () {
                    var expected = config.errorMessages.noPageFound;
                    var message;
                    response.status = 404;

                    view._handleWcmError(view.model, response);

                    message = view.$el.find('.alert').text();

                    expect(message).to.contain(expected);
                });
            });

        }); // _handleWcmError method

    });
});

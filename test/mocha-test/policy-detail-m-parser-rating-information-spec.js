/* global expect:false, $:false, _:false, expect:false */

var PolicyRatingInformationParser = 
        require('../dist/pages/policy/models/parsers/policy-rating-information');
var helpers = require('./helpers/helpers');

describe('Pending Policy Detail Model parser - Rating Information ' +
    '(pages/policy/models/pending-policy-detail-m.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
    var parser = new PolicyRatingInformationParser();

    describe('_setRatingInformationData method', function () {

        beforeEach(function () {
            policyDataCopy = $.extend(true,{},
                helpers.policyData.pendingPolicyDetailAssetCare);
        });

        afterEach(function () {
            policyDataCopy = null;
        });

        it('exists as a function', function () {
            expect(parser._setRatingInformationData).to.exist.and.be.a('function');
        });

        it('should order insureds field first', function () {
            var expectedInsuredFullName = policyDataCopy
                .customers[policyDataCopy.customerRoles.Insured[0].customerId].fullName;
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(data[0].fullName).to.equal(expectedInsuredFullName);
        });

        it('should order jointInsureds after insured', function () {
            var expectedJointInsuredFullName = policyDataCopy
                .customers[policyDataCopy.customerRoles['Joint Insured'][0].customerId]
                .fullName;
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(data[1].fullName).to.equal(expectedJointInsuredFullName);
        });

        it('should include the \'underwritingClass\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(_.has(data[0], 'underwritingClass')).to.be.true;
        });

        it('should include the \'tobaccoUse\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(_.has(data[0], 'tobaccoUse')).to.be.true;
        });

        it('should include the \'coveragePlanName\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);
            expect(_.has(data[0].coverageRatings[0], 'coveragePlanName')).to.be.true;
        });

        it('should include the \'mortalityTable\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);
            expect(_.has(data[0].coverageRatings[0], 'mortalityTable')).to.be.true;
        });

        it('should include the \'morbidityTable\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);
            expect(_.has(data[0].coverageRatings[0], 'morbidityTable')).to.be.true;
        });

        it('should include the \'ratedAge\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(_.has(data[0].coverageRatings[0], 'ratedAge')).to.be.true;
        });

        it('should include the \'fullName\' key', function () {
            var data = parser._setRatingInformationData(policyDataCopy);

            expect(_.has(data[0], 'fullName')).to.be.true;
        });

    }); // _setRatingInformationData method

    describe('_setRatingInformationFlagsAndData method', function () {

        beforeEach(function () {
            policyDataCopy = $.extend(true,{},
                helpers.policyData.pendingPolicyDetailAssetCare);
        });

        afterEach(function () {
            policyDataCopy = null;
        });

        it('exists as a function', function () {
            expect(parser._setRatingInformationFlagsAndData).to.exist.and.be.a('function');
        });

        describe('\'showRatingInfo\' flag', function () {

            it('should be set to TRUE when underwritingRequired is ' +
                'true, and there is ratingInformationData present', function () {

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.showRatingInfo).to.exist;
                expect(policyDataCopy.showRatingInfo).to.be.true;
            });

            it('should be set to FALSE when underwritingRequired is false', function () {
                policyDataCopy.product.underwritingRequired = false;

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.showRatingInfo).to.exist;
                expect(policyDataCopy.showRatingInfo).to.be.false;
            });

            it('should be set to FALSE when uwDecision is null in response', function () {
                
                policyDataCopy.application.uwDecision = null;

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.showRatingInfo).to.exist;
                expect(policyDataCopy.showRatingInfo).to.be.false;
            });

        });

        describe('\'isProductAssetCare\' flag', function () {

            it('should be set to TRUE when productTypeCode is "WL" and ' +
                'carrierCode is "SL"', function () {

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.exist;
                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.be.true;
            });

            it('should be set to FALSE when productTypeCode is "WL" and ' +
                'carrierCode is NOT "SL"', function () {

                policyDataCopy.carrierCode = 'AUL';

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.exist;
                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.be.false;
            });

            it('should be set to FALSE when productTypeCode is NOT "WL"', function () {

                policyDataCopy.product.productTypeCode = 'FFPL';

                parser._setRatingInformationFlagsAndData(policyDataCopy);

                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.exist;
                expect(policyDataCopy.ratingInformationData.isProductAssetCare).to.be.false;
            });

        });

        describe('hasAssetCareTableRatingData flag', function () {

            var policyDetailCopy = $.extend(true,{},
                helpers.policyData.pendingPolicyDetailAssetCare);

            it('should return true if Table Risk Data exist', function () {

                parser._setRatingInformationFlagsAndData(policyDetailCopy);
                expect(policyDetailCopy.ratingInformationData.hasAssetCareTableRatingData)
                    .to.be.true;
            });
        });

        describe('hasNonAssetCareTableRatingData', function () {

            var ratingInformation = [
                {
                    customerId  : 123,
                    underwritingClass: 'Sub-Standard',
                    tobaccoUse: false,
                    ratedAge  : 99,
                    coverageRatings : [
                        {
                            coveragePlanName   : '20 Year Level Term',
                            coverageSequence   : 1,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'

                        },{
                            coveragePlanName   : '20 Year Level Term',
                            coverageSequence   : 2,
                            ratingType         : 'Flat Extra',
                            flatExtraAmount    : 'USD 2.50',
                            duration           : 'P3Y',
                            tableName          : null
                        },{
                            coveragePlanName   : 'Rider 1',
                            coverageSequence   : 3,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'
                        },{
                            coveragePlanName   : 'Rider 2',
                            coverageSequence   : 4,
                            ratingType         : 'Table',
                            flatExtraAmount    : null,
                            duration           : 'P20Y',
                            tableName          : 'B/2'
                        }
                    ]
                },
                {
                    customerId  : 456,
                    underwritingClass : 'Sub-Standard',
                    tobaccoUse: true,
                    ratedAge  : 99,
                    coverageRatings : []
                }
            ];

            var policyDetailCopy = $.extend(true,{},
                helpers.policyData.pendingPolicyDetailAssetCare);

            policyDetailCopy.carrierCode = 'AUL';
            policyDetailCopy.ratingInformation = ratingInformation;

            it('should return true if it has coverages (Rating Data)', function () {
                parser._setRatingInformationFlagsAndData(policyDetailCopy);
                expect(policyDetailCopy.ratingInformationData.hasNonAssetCareTableRatingData)
                    .to.be.true;
            });

        });

    }); // _setRatingInformationFlagsAndData method

});

/* global expect:false, $:false, _:false, expect:false */

var PolicyRelationshipsParser = 
        require('../dist/pages/policy/models/parsers/policy-relationships');
var helpers = require('./helpers/helpers');

describe('Pending Policy Detail Model parser - Policy Relationships ' +
    '(pages/policy/models/parsers/policy-relationships.js)', function () {

    var policyDataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetail);
    var parser = new PolicyRelationshipsParser();

    describe('_setCustomerPolicyRelationshipData method', function () {

        var expectedOutput = {
            primaries: {
                ssnColumnLabel: 'SSN',

                //sorted in specific order defined in model
                data: [
                    {
                        relationship: 'Annuitant',
                        customerRoleContactId:1002,
                        customerId: 456,
                        showDoBnGender: true,
                        showDoBnGenderColumns: true
                    },
                    {
                        relationship: 'Primary Insured',
                        customerRoleContactId:1002,
                        customerId: 456,
                        showDoBnGender: true,
                        showDoBnGenderColumns: true
                    }
                ]
            },
            beneficiaries: {
                ssnColumnLabel: 'SSN',

                //sorted ascending order
                data: [
                    {
                        relationship: 'Beneficiary',
                        customerRoleContactId:1001,
                        customerId: 123,
                        showDoBnGender: false,
                        showDoBnGenderColumns: false
                    },
                    {
                        relationship: 'Primary Beneficiary',
                        customerRoleContactId:1002,
                        customerId: 456,
                        showDoBnGender: false,
                        showDoBnGenderColumns: false
                    }
                ]
            },
            others: {
                ssnColumnLabel: 'SSN',

                //sorted ascending order
                data: [
                    {
                        relationship: 'Assignee',
                        customerRoleContactId:1002,
                        customerId: 456,
                        showDoBnGender: false,
                        showDoBnGenderColumns: false
                    },
                    {
                        relationship: 'Employer',
                        customerRoleContactId:1001,
                        customerId: 123,
                        showDoBnGender: false,
                        showDoBnGenderColumns: false
                    },
                    {
                        relationship: 'Owner',
                        customerRoleContactId:1001,
                        customerId: 123,
                        showDoBnGender: false,
                        showDoBnGenderColumns: false
                    }
                ]
            }
        };

        it('exists as a function', function () {
            expect(parser._setCustomerPolicyRelationshipData).to.exist.and.be.a('function');
        });

        it('should return expected output (which should sorted correctly) ', function () {
            var cPRCategories = parser._setCustomerPolicyRelationshipData(
                helpers.policyData.pendingPolicyDetail
            ).policyRelationshipCategories;

            expect(cPRCategories).to.deep.equal(expectedOutput);
        });

        it('should return other roles sorted by relationship and fullName', function () {

            policyDataCopy.customers = {
                '123' : {
                    lastName  : 'Zulu',
                    firstName : 'Frank E.',
                    fullName  : 'Frank E. Zulu'
                },
                '456' : {
                    lastName  : 'Zulu',
                    firstName : 'Betty L.',
                    fullName  : 'Betty L. Zulu'
                },
                '789' : {
                    lastName  : 'Alpha',
                    firstName : 'Sam E.',
                    fullName  : 'Sam E. Alpha'
                }
            };

            policyDataCopy.customerRoles = {
                Conservator : [{
                    customerId : 123,
                    customerRoleContactId: 1001
                },
                {
                    customerId : 456,
                    customerRoleContactId: 1002
                }],
                Assignee : [{
                    customerId : 789,
                    customerRoleContactId: 1001
                },
                {
                    customerId : 456,
                    customerRoleContactId: 1002
                }]
            };

            var expectedOrder = [456, 789, 456, 123];

            var cPRCategories = parser._setCustomerPolicyRelationshipData(policyDataCopy)
                .policyRelationshipCategories;

            _.each(cPRCategories.others.data, function(other, i) {
                expect(other.customerId).to.be.equal(expectedOrder[i]);
            });
        });

        it('should return beneficiaries sorted by fullName', function () {

            policyDataCopy.customers = {
                '123' : {
                    lastName  : 'Zulu',
                    firstName : 'Frank E.',
                    fullName  : 'Frank E. Zulu'
                },
                '456' : {
                    lastName  : 'Zulu',
                    firstName : 'Betty L.',
                    fullName  : 'Betty L. Zulu'
                },
                '789' : {
                    lastName  : 'Alpha',
                    firstName : 'Sam E.',
                    fullName  : 'Sam E. Alpha'
                }
            };

            policyDataCopy.customerRoles = {
                Beneficiary : [{
                    customerId : 123,
                    customerRoleContactId: 1001
                },
                {
                    customerId : 789,
                    customerRoleContactId: 1002
                },
                {
                    customerId : 456,
                    customerRoleContactId: 1002
                }]
            };

            var expectedOrder = [456, 123, 789];

            var cPRCategories = parser._setCustomerPolicyRelationshipData(policyDataCopy)
                .policyRelationshipCategories;

            _.each(cPRCategories.beneficiaries.data, function(beneficiary, i) {
                expect(beneficiary.customerId).to.be.equal(expectedOrder[i]);
            });
        });

        it('should set "showDoBnGender" with proper flag', function () {

            policyDataCopy.customers = {
                '123' : {
                    lastName  : 'Zulu',
                    firstName : 'Frank E.',
                    fullName  : 'Frank E. Zulu'
                },
                '456' : {
                    lastName  : 'Zulu',
                    firstName : 'Betty L.',
                    fullName  : 'Betty L. Zulu'
                },
                '789' : {
                    lastName  : 'Alpha',
                    firstName : 'Sam E.',
                    fullName  : 'Sam E. Alpha'
                }
            };

            policyDataCopy.customerRoles = {
                Owner : [{
                    customerId : 123,
                    customerRoleContactId: 1001
                }],
                'Eligible Person' :[{
                    customerId : 456,
                    customerRoleContactId: 1002
                }],
                'Insured' :[{
                    customerId : 456,
                    customerRoleContactId: 1002
                }]
            };

            var cPRCategories = parser._setCustomerPolicyRelationshipData(policyDataCopy)
                .policyRelationshipCategories;

            //insured
            expect(cPRCategories.primaries.data[0].showDoBnGender).to.be.equal(true);

            //eligible person
            expect(cPRCategories.primaries.data[1].showDoBnGender).to.be.equal(false);
        });

        it('SSN column label should be "SSN / Tax ID" if Tax ID exist',
            function () {

            var dataCopy = $.extend(true,{},helpers.policyData.pendingPolicyDetailWithTaxID);

            dataCopy.customerRoles.Insured = dataCopy.customerRoles.Owner;
            delete dataCopy.customerRoles.Owner;

            var cPRCategories = parser._setCustomerPolicyRelationshipData(dataCopy)
                .policyRelationshipCategories;

            expect(cPRCategories.primaries.ssnColumnLabel).to.equal('SSN / Tax ID');
        });

        it('SSN column label should be "Tax ID" if all'+
            ' customers have a Tax ID', function () {

            var cPRCategories = parser._setCustomerPolicyRelationshipData(
                helpers.policyData.pendingPolicyDetailWithOnlyTaxID
            ).policyRelationshipCategories;

            expect(cPRCategories.primaries.ssnColumnLabel).to.equal('Tax ID');
        });

        it('SSN column label should be "Tax ID" if "taxIdType"' +
            ' is blank or "NONE" for any of the customers' , function () {

            var cPRCategories = parser._setCustomerPolicyRelationshipData(
                helpers.policyData.pendingPolicyDetailWithTaxIDNone
            ).policyRelationshipCategories;

            expect(cPRCategories.primaries.ssnColumnLabel).to.equal('Tax ID');
        });

        it('calls _setSSNColumnLabelName', function () {
            var spySetSSNColumnLabelName = this.sinon.spy(parser,'_setSSNColumnLabelName');
            parser._setCustomerPolicyRelationshipData(
                helpers.policyData.pendingPolicyDetailWithTaxID
            );

            expect(spySetSSNColumnLabelName).to.have.been.called;
            spySetSSNColumnLabelName.restore();
        });

        it('should NOT throw an error due to unexpected customerRoles', function () {
            var response = {};
            _.extend(response, helpers.policyData.pendingPolicyDetailWithOnlyTaxID);
            response.customerRoles.unexpectedRole = [
                {
                    'customerId'            : 321,
                    'customerRoleContactId' : 9999
                }
            ];

            expect(parser._setCustomerPolicyRelationshipData.bind(parser, response))
                .to.not.throw(Error);
        });
    }); // _setCustomerPolicyRelationshipData method

});

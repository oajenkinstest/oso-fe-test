/* global expect:false, Backbone: false */

describe('Browser Feature Detection Module ' +
    '(modules/browserFeatureDetection/browserFeatureDetectionModule.js)', function () {

    var bfdModule = require('../dist/modules/browserFeatureDetection/browserFeatureDetectionModule');

    it('Module instanceOf Marionette Object', function () {
        expect(bfdModule).to.be.an.instanceOf(Backbone.Marionette.Object);
    });

    it('setChannelAndDetectFeatures should exist', function () {
        expect(bfdModule.setChannelAndDetectFeatures).to.exist;
    });

    describe('check features through Radio call', function () {

        it('"browserFeature" channel should exist', function () {
            expect(bfdModule.browserFeatureDetectionChannel.channelName)
                .to.equal('browserFeature');
        });
        
        describe ('File API', function () {
            it('should return API Object if feature is valid', function () {
                var radioBrowserFeatureDetect = Backbone.Radio.channel('browserFeature');
                expect(radioBrowserFeatureDetect.request('detect', 'filereader'))
                    .to.be.not.undefined;
                expect(radioBrowserFeatureDetect.request('detect', 'formdata'))
                    .to.be.not.undefined;
            });
        });

        describe ('webP', function () {
            it('Should return true if webP feature is supported', function () {
                var radioBrowserFeatureDetect = Backbone.Radio.channel('browserFeature');
                Backbone.$.when(
                    bfdModule._checkWebpFeature('lossless'),
                    bfdModule._checkWebpFeature('alpha')
                ).then( function (lossless, alpha) {
                    
                    expect(lossless).to.have.been.true;
                    expect(alpha).to.have.been.true;

                    bfdModule._setWebpSupportFlag(lossless, alpha);
                    expect(radioBrowserFeatureDetect.request('detect', 'webp')).to.be.true;
                });
            });
        });
    });  

    describe('if features are not supported', function () {
        
        it('filereader and formdata should be undefined ', function () {
            window.File         = undefined;
            window.FileList     = undefined;
            window.FileReader   = undefined;
            window.FormData     = undefined;

            //override with new settings
            bfdModule.setChannelAndDetectFeatures();

            var radioBrowserFeatureDetect = Backbone.Radio.channel('browserFeature');
            expect(radioBrowserFeatureDetect.request('detect', 'filereader'))
                .to.be.undefined;
            expect(radioBrowserFeatureDetect.request('detect', 'formdata'))
                .to.be.undefined;
        });

    });

});

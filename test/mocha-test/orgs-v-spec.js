/* global expect:false, Marionette:false */

var OrgsView = require('../dist/pages/policy/views/orgs-v');
var helpers = require('./helpers/helpers');

describe('Organization View (pages/policy/views/orgs-v.js)', function() {

    var rootView;
    var view;

    beforeEach(function () {
        rootView = helpers.viewHelpers.createRootView();
        view = new OrgsView();
        rootView.render();
        rootView.showChildView('contentRegion', view);
    });

    afterEach(function () {
        view.destroy();
        view = null;
    });

    it('exists', function () {
        expect(OrgsView).to.exist;
    });

    it('contains "generalInfo" and "orgsTable" regions', function () {
        expect(view.getRegion('generalInfo')).not.to.be.undefined;
        expect(view.getRegion('orgsTable')).not.to.be.undefined;
    });

    describe('_showGeneralInfo method', function () {
        it('exists as a function', function () {
            expect(view._showGeneralInfo).to.exist.and.be.a('function');
        });

        it('renders a view in "generalInfo" region ', function () {
            view._showGeneralInfo({
                fullName : 'JEFFREY R SEMLER',
                id        : 12345
            });

            expect(view.getChildView('generalInfo').isRendered).to.be.true;
            expect(view.getChildView('generalInfo').$el.find('h3 span').text())
                .to.be.contain('JEFFREY R SEMLER');
        });
    }); // _showGeneralInfo method

    describe('_clearGeneralInfo method', function () {
        it('exists as a function', function () {
            expect(view._clearGeneralInfo).to.exist.and.be.a('function');
        });

        it('destroys the view in the "generalInfo" region', function () {
            // first, render a view so we can prove it is there...
            view._showGeneralInfo({
                fullName : 'JEFFREY R SEMLER',
                id       : 12345
            });
            expect(view.getChildView('generalInfo').isRendered).to.be.true;
            expect(view.getChildView('generalInfo')).to.not.be.undefined;

            // now clear it and prove it really goes away.
            view._clearGeneralInfo();
            expect(view.getChildView('generalInfo')).to.be.undefined;
        });

        it('does not throw an error if the child view does not exist', function () {
            var fn = function () {
                view._clearGeneralInfo();
            };

            // first we have to clear it...
            view._clearGeneralInfo();

            // and then check that clearing it when it doesn't exist is OK.
            expect(view.getChildView('generalInfo')).to.be.undefined;
            expect(fn).to.not.throw(Error);
        });
    }); // _clearGeneralInfo method

    describe('trigger change tests', function () {

        var clearSpy;
        var hierarchyHandler;
        var listener;
        var showProducerListHandler;
        var stateHandler;

        beforeEach(function() {
            hierarchyHandler        = this.sinon.stub();
            listener                = new Marionette.Object();
            showProducerListHandler = this.sinon.stub();
            stateHandler            = this.sinon.stub();

            listener.listenTo(view, 'stateChange', stateHandler);
            listener.listenTo(view, 'hierarchyChange', hierarchyHandler);
            listener.listenTo(view, 'showProducerPolicyList', showProducerListHandler);

            clearSpy = this.sinon.spy(view, '_clearGeneralInfo');
        });

        afterEach(function () {
            listener.stopListening();
            listener.destroy();
            hierarchyHandler = null;
            showProducerListHandler = null;
            stateHandler = null;
            clearSpy.restore();
        });

        describe('_stateChange method', function () {

            it('exists as a function', function () {
                expect(view._stateChange).to.exist.and.be.a('function');
            });

            it('triggers a stateChange message with the input', function () {
                var state = { someProp : 'someValue' };
                view._stateChange(state);
                expect(stateHandler).to.have.been.calledWith(state);
            });

            it('adds producerId and producerOrgId (if they exist) to message', function () {
                var producerId    = 123;
                var producerOrgId = 456;
                var state         = { someProp : 'someValue' };
                var expectedState = {
                    someProp      : 'someValue',
                    producerId    : producerId,
                    producerOrgId : producerOrgId
                };

                view.producerId    = producerId;
                view.producerOrgId = producerOrgId;

                view._stateChange(state);
                expect(stateHandler).to.have.been.calledWith(expectedState);
            });

            it('creates a new object to pass if its input is not an object', function() {
                var expectedState = {};
                view._stateChange('Monkey');

                expect(stateHandler).to.have.been.calledWith(expectedState);
            });
        }); // _stateChange method

        describe('_hierarchyChange method', function () {
            
            it('exists as a function', function () {
                expect(view._hierarchyChange).to.exist.and.be.a('function');
            });

            it('triggers a hierarchyChange message', function () {
                var state = { producerOrgId : 987 };
                view._hierarchyChange(state);

                expect(hierarchyHandler).to.have.been.calledWith(state);
            });

            it('sets the producerOrgId on the view', function () {
                var state = { producerOrgId : 987 };
                expect(view.producerOrgId).to.be.undefined;
                view._hierarchyChange(state);

                expect(view.producerOrgId).to.equal(state.producerOrgId);
            });

            it('calls _clearGeneralInfo', function () {
                var state = { producerOrgId : 987 };
                view._hierarchyChange(state);

                expect(clearSpy).to.have.been.called.once;
            });
        }); // _hiearchyChange method

        describe('_showProducerPolicyList method', function () {

            it('exists as a function', function () {
                expect(view._showProducerPolicyList).to.be.a('function');
            });

            it('triggers a showProducerPolicyList message', function () {
                var state = { producerId : 12345 };
                view._showProducerPolicyList(state);

                expect(showProducerListHandler).to.have.been.called.once;
            });
        }); // _showProducerPolicyList method
    });

    describe('_showServerMessage method', function () {

        it('exists as a function', function () {
            expect(view._showServerMessage).to.exist.and.be.a('function');
        });

        it('renders correctly ', function () {
            view._showServerMessage();

            expect(view.getChildView('orgsTable').$el.text())
                .to.be.contain(view.errors.systemError);
            expect(view.getChildView('orgsTable').$el.find('.fa-bullhorn'))
                .to.have.lengthOf(1);
        });

        it('renders correctly "Info" message', function () {
            view._showServerMessage('some message', 'info');

            expect(view.getChildView('orgsTable').$el.text())
                .to.be.contain('some message');
            expect(view.getChildView('orgsTable').$el.find('.fa-exclamation'))
                .to.have.lengthOf(1);
        });
    }); // _showServerMessage method
});

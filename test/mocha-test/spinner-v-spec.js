/* global expect:false */

var SpinnerView = require('../dist/modules/waitIndicator/views/spinner-v');
var utils       = require('../dist/utils/utils');

describe('Spinner View (modules/waitIndicator/views/spinner-v.js)', function() {
    var spinnerElement;
    var view;

    beforeEach(function () {
        spinnerElement = utils.generateSpinnerElement();
        view = new SpinnerView({
            spinner : spinnerElement
        });
        view.render();
    });

    afterEach(function () {
        view.destroy();
    });
    
    it('spinner-v should exist', function () {
        expect(SpinnerView).to.exist;
    });

    it('Should render with Spinner element', function () {
        expect(view.$el.find('.spinner')).to.have.lengthOf(1);
    });

    it('Should not contain spinner and call console error if "spinner" '+
        'element missing in "options" ', function () {
        // Due to the way we're mocking the window object in JSDOM, the console object isn't
        // directly available to attach spies to, so use window.console.
        var errorLogSpy   = this.sinon.spy(window.console, 'error');
        view              = new SpinnerView();
        view.render();

        expect(errorLogSpy).to.have.been.calledWith([view.errors.spinnerObjectMissing]);
        errorLogSpy.restore();
    });

    it('Should add "aling-static" to view element if '+
        'options "position" is passed as "static" ', function () {
        view = new SpinnerView({
            spinner : spinnerElement,
            position : 'static'
        });
        view.render();
        expect(view.$el.hasClass('align-static')).to.be.true;
    });

    it('Should add "aling-relative" to view element if '+
        'options "position" is passed other than "fixed" or "static" ', function () {
        expect(view.$el.hasClass('align-relative')).to.be.true;
    });

    it('Should not add "aling-relative" or "aling-static" to view  if '+
        'options "position" passed is "fixed" ', function () {
        view = new SpinnerView({
            spinner : spinnerElement,
            position : 'fixed'
        });
        view.render();
        expect(view.$el.hasClass('align-relative')).to.be.false;
        expect(view.$el.hasClass('align-static')).to.be.false;
    });
});

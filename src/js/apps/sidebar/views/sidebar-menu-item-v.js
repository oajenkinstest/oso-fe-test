/* global Marionette */

var menuItemViewTemplate = require('../templates/sidebar-menu-item.hbs');
var utils                = require('../../../utils/utils');
require('../../../utils/hb-helpers');

var SidebarMenuItemView = Marionette.CompositeView.extend({

    template    : menuItemViewTemplate,

    initialize: function () {
        this.collection = this.model.childItems;
    },

    attachHtml: function (collectionView, childView, index) {
        collectionView.$('ul:first').append(childView.el);
    },

    onBeforeRender: function () {
        this.model.set('activeAndOpen', this.model.containsAnActiveItem());
    },

    onRender: function () {
        // remove the wrapping div to match design markup
        utils.unwrapView(this);
    }

});

module.exports = SidebarMenuItemView;

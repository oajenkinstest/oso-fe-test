/* global $, Marionette*/

var menuViewTemplate = require('../templates/sidebar-menu-t.hbs');
var MenuItemView = require('./sidebar-menu-item-v');

var SidebarMenuView = Marionette.CompositeView.extend({

    template            : menuViewTemplate,

    childView           : MenuItemView,

    childViewContainer  : '#__OA_SIDEBAR_MENU__',
    
    events: {
        'click a': 'selectMenuItem'
    },

    selectMenuItem: function (e) {
        var $clickTarget;
        var href;

        if (e && e.currentTarget) {
            $clickTarget = $(e.currentTarget);
        }

        // If we have an external link, just return. Don't handle anything.
        if ($clickTarget.attr('target') === '_blank') {
            return;
        }

        if (e && e.preventDefault) {
            e.preventDefault();
        }

        href = $clickTarget.attr('href');

        if (!$clickTarget.hasClass('dropdown-toggle')) {

            if (href) {
                this.trigger('nav', href);            
            }
        }
    },

    onChildviewNav: function (childView, href) {
        this.trigger('nav', href);
    }
    
});

module.exports = SidebarMenuView;

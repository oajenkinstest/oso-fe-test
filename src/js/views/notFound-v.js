/* global Marionette */
/**
 *
 * Created by Jason.Bell on 3/5/2015.
 */

var template = require('../templates/notFound-t.hbs');
var NotFoundView = Marionette.ItemView.extend({
    template: template
});
module.exports = NotFoundView;

/* global Marionette */

var topBarTemplate = require('../templates/topbar-t.hbs');

var TopBarView = Marionette.ItemView.extend({
    template: topBarTemplate
});

module.exports = TopBarView;

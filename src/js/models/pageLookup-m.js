/* global Backbone, _ */

/**
 * PageLookup
 *
 * The pageLookup model is related to the appStructure model. It uses the
 * pageId as a key to store the associated capability and view. This is
 * used to verify that a pageId exists and what capability a user must
 * have in order to view a particular page.
 *
 * @param {Object} options
 * @param {Object[]} appStructure - (array of objects) Metadata for the pages that make up the site.
 *                  See src/js/models/appStructure-m.js.
 *
 */

var PageLookupModel = Backbone.Model.extend({

    errors : {
        appStructure : 'pageLookup constructor requires an appStructure array'
    },

    initialize : function(options) {
        if (!options || !options.appStructure || !_.isArray(options.appStructure)) {
            throw new Error(this.errors.appStructure);
        } else {
            this.set('structure', this._buildPageLookup(options.appStructure));
        }
    },

    /**
     * isValidPage() checks to see if pageId is defined in the pageLookup object
     * @param pageId
     * @returns {boolean}
     */
    isValidPage : function(pageId) {
        return this.get('structure').hasOwnProperty(this._removeHashPrefix(pageId));
    },

    /**
     * userCanAccessPage() validates whether or not the pageId
     * exists in the filteredAppStructure.
     * @param {string} pageId
     * @param {array} capabilities
     * @returns {boolean}
     */
    userCanAccessPage : function(pageId, capabilities) {
        var _this = this;
        var pageIdNoHash = this._removeHashPrefix(pageId);
        var page = this.get('structure')[pageIdNoHash];
        var canAccess = false;
        var hasActiveItemInSidebar = false;

        if (page.capability) {
            var capabilitiesForSidebar      = page.capability.forSidebar;
            var capabilitiesForPageAccess   = page.capability.forPageAccess;

            //if page has menuAccess property then all should match with
            // existing user capabilities
            if (capabilitiesForSidebar) {
                canAccess = this._hasCapabilitiesForSidebar(page, capabilities);
            } 
            
            // if pageId doesn't have 'forSidebar' property or not matching with 
            // any capabilities defined in 'forSidebar' property, but it has pageAccess 
            // property with user capabilities, then it should be pointed to
            // any one of the menu item using 'activeFor' property to set a active menu item and
            // user should have the capabilities which added to the capability.forSidebar property
            // of 'active menu item'
            if (!canAccess) {
                
                // get all page which has activeFor property with current pageId
                var pageItemsToSetActive = _.filter(this.get('structure'), 
                    function (pageItem) {
                        if(pageItem.activeFor) {
                            return _.contains(pageItem.activeFor, pageIdNoHash);
                        }
                    });

                // Check whether containing pageItems has enough capabilities to set a flag
                if (pageItemsToSetActive && _.isArray(pageItemsToSetActive)) {
                    pageItemsToSetActive.forEach( function (pageItem) {
                        if (!hasActiveItemInSidebar) {
                            hasActiveItemInSidebar =  _this._hasCapabilitiesForSidebar(
                                pageItem, 
                                capabilities
                            );
                        }
                    });
                }
            }

            //if capabilities are not matching with menuAccess property then
            //look for pageAccess property to identify page access. Also there 
            //should be a mapping with activeItem for this page ID
            if (!canAccess && capabilitiesForPageAccess && hasActiveItemInSidebar) {
                canAccess = this._hasCapabilitiesForPageAccess(page, capabilities);
            }

            return canAccess;
        }
        return false;
    },

    /**
     * Removes the hash if a string is prefixed with one.
     * If not prefixed with a hash, the entire string is returned.
     * (e.g. '#mypageId' would be returned as 'mypageId').
     * @param {string} pageId
     * @returns {string}
     * @private
     */
    _removeHashPrefix : function(pageId) {
        return pageId.charAt(0) === '#' ? pageId.substring(1) : pageId;
    },

    /**
    * Convert the appStructure to a lookup with pageId (link) as the key.
    *
    * @param {Object[]} appStructure - Metadata for the pages that make up the site.
    * @returns {Object} Page data lookup object
    *
    * Only items representing actual pages are included in the lookup object.
    * Items used only for subitem headings are omitted.
    *
    * As an example this app structure...
    *     [
    *         {
    *             icon        : 'desktop',
    *             displayText : 'Contact Us',
    *             link        : 'contact-us',
    *             view        : ContactView,
    *             capability  : {
    *                   forSidebar : [{
    *                       all : true
    *                   }]
    *             }
    *         },
    *         {
    *             icon        : 'info-circle',
    *             displayText : 'FAQs & Resources',
    *             subItems: [
    *                 {
    *                     displayText : 'Billing and Payments',
    *                     link        : 'billing-payments',
    *                     view        : BillingView,
    *                     capability  : {
    *                           forPageAccess : {
    *                               'WCM_All_Role_View' : true
    *                           }
    *                     }
    *                 },
    *                 {
    *                     icon        : 'tachometer',
    *                     displayText : 'Tools',
    *                     link        : 'tools',
    *                     view        : ToolsView,
    *                     capability  : {
    *                           forPageAccess : {
    *                               'WCM_Foo_Role_View' : true
    *                           }
    *                     }
    *                 }
    *             ]
    *         }
    *     ]
    *
    * Will become this lookup object...
    *      {
    *         'contact-us': {
    *             icon        : 'desktop',
    *             displayText : 'Contact Us',
    *             view        : ContactView,
    *             capability  : {
    *                   forSidebar : [{
    *                       all : true
    *                   }]
    *             }
    *         },
    *         'billing-payments': {
    *             displayText : 'Billing and Payments',
    *             view        : BillingView
    *             capability  : {
    *                   forPageAccess : {
    *                       'WCM_All_Role_View' : true
    *                   }
    *             }
    *         },
    *         'tools': {
    *             icon        : 'tachometer',
    *             displayText : 'Tools',
    *             view        : ToolsView,
    *             capability  : {
    *                   forPageAccess : {
    *                       'WCM_Foo_Role_View' : true
    *                   }
    *             }
    *         }
    *     }
     */
    _buildPageLookup: function _buildPageLookup(appStructure) {
        var lookupObj = {};

        // This recursive function adds items to lookupObj as a side effect. It's
        // not pretty, but trying to pass the lookupObj as a parameter and merging
        // the return values into it at each level of recursion is even uglier. -RKC
        var iterate = function (nodeObj) {
            var i;

            // If nodeObj is an array, loop through the elements
            if (_.isArray(nodeObj)) {
                for (i=0; i<nodeObj.length; i++) {
                    iterate(nodeObj[i]);
                }

            } else {

                // If there is a 'link' property, then add the node to the lookupObj
                if (nodeObj.hasOwnProperty('link')) {

                    // All nodes will include displayText and view
                    lookupObj[nodeObj.link] = {
                        displayText : nodeObj.displayText,
                        view        : nodeObj.view,
                        capability  : nodeObj.capability
                    };

                    // Add activeFor property if that exist
                    if (nodeObj.activeFor) {
                        lookupObj[nodeObj.link].activeFor  = nodeObj.activeFor;
                    }

                    // icon might be undefined
                    if (nodeObj.icon) {
                        lookupObj[nodeObj.link].icon = nodeObj.icon;
                    }

                }

                // If there is a 'subItems' array, iterate into it
                if (nodeObj.hasOwnProperty('subItems')) {
                    iterate(nodeObj.subItems);
                }
            }
        };

        iterate(appStructure);

        return lookupObj;
    },

    /**
     * Determine if the pageObj passed in has capabilities applied which match one of
     * the capabilities passed to the function. The `forPageAccess` capability is
     * evaluated and must match at least one of the capabilities applied to the pageObj.
     *
     * @param {object} pageObj - The page which is part of the "structure" model attribute
     * @param {Array} capabilities - A list of capability strings
     * @returns {boolean}
     * @private
     */
    _hasCapabilitiesForPageAccess : function _hasCapabilitiesForPageAccess(pageObj, capabilities) {
        var hasAccess = false;

        if (pageObj && pageObj.capability && pageObj.capability.forPageAccess) {
            if (_.has(pageObj.capability.forPageAccess, 'all')) {
                hasAccess = true;
            } else {
                _.each(pageObj.capability.forPageAccess, function (flag, capability) {
                    if (!hasAccess && (flag === _.contains(capabilities, capability))) {
                        hasAccess = true;
                    }
                });
            }
        }

        return hasAccess;
    },

    /**
     * Determine if the pageObj passed in has capabilities which match those passed in
     * to the function. The `forSidebar` (menu access) capability is evaluated with this
     * function and must match ALL capabilities applied to the pageObj.
     *
     * @param {object} pageObj - The page which is part of the "structure" model attribute
     * @param {Array} capabilities - A list of capability strings
     * @returns {boolean}
     * @private
     */
    _hasCapabilitiesForSidebar : function _hasCapabilitiesForSidebar(pageObj, capabilities) {
        var capabilitiesForSidebar;
        var hasAccess = false;
        var i;

        if (pageObj && pageObj.capability && pageObj.capability.forSidebar) {
            capabilitiesForSidebar = pageObj.capability.forSidebar;
        }

        if (capabilitiesForSidebar && _.isArray(capabilitiesForSidebar)) {

            for (i = 0; i < capabilitiesForSidebar.length; i++) {
                var sidebarCapability = capabilitiesForSidebar[i];

                if (_.has(sidebarCapability, 'all')) {
                    hasAccess = true;
                } else {
                    /* eslint-disable no-loop-func */
                    hasAccess = _.all(sidebarCapability, function (flag, capability) {
                        return (flag === _.contains(capabilities, capability));
                    });
                    /* eslint-enable no-loop-func */
                }

                // Break out of the loop if we found a match
                if (hasAccess) {
                    break;
                }
            }
        }

        return hasAccess;
    }
});

module.exports = PageLookupModel;

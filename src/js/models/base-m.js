/* global Backbone */

var BaseModel = Backbone.Model.extend({

    parse: function(data) {
        if (data && data.items && data.items[0]) {
            return data.items[0];
        }
        return null;
    }

});

module.exports = BaseModel;

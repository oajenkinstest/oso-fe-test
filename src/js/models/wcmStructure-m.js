/* global Backbone:false */
/* eslint-disable max-len */
/**
 *
 * The WcmStructureModel contains a property named 'structure' which is an array of elements to be
 * incorporated into the side navigation.
 *
 * {
 *     displayText : String,
 *                   Required,
 *                   The label for the item in the menu
 *
 *     icon        : String,
 *                   Optional,
 *                   the name of a class or Font Awesome icon
 *                   (http://fortawesome.github.io/Font-Awesome/icons/)
 *
 *     subItems    : Array,
 *                   Required for branch-level items,
 *                   an array containing more nodes in the menu structure.
 *
 *     isExternal  : Boolean,
 *                   Optional,
 *                   When true, indicates that the menu item should open in a new window. An
 *                   icon which indicates the link as external will be placed after the link text.
 *
 *     link        : String,
 *                   Required for leaf-level items,
 *                   The uriEncoded name used in the URL. This is preferred over the ID because it
 *                   is more easily readable, making bookmarks and web history more useful for the
 *                   end user. This will also serve as the identifier for routing within the app.
 *
 *     capability  : Object,
 *                   Required for leaf-level items,
 *                   the capability needed for this item to show in the side nav
 * }
 *
 *
 * Hypothetical app flow when the user clicks a WCM link in the sidebar (the URL hashes will be in
 * the form "#content/readable-link-name"):
 *
 * 1) Router invokes handler for "content" (which will be added to the link hash by the utility that
 *    integrates this structure into the sidebar menu).
 *
 * 2) "content" handler parses the remainder of the URL hash to get the link name
 *
 * 3) handler looks up the WCM ID based on the link name. For this, we will need to build a lookup
 *    for the ID based on the link. This will be a similar lookup to that used by the router for
 *    dynamic portions of the app. See OOSO-2309 for information.
 *
 * 4) handler creates a wcm-content-v to display the content for the ID found above in the content
 *    area.
 *
 */

var PathwayMenuStructureModel = require('./pathwayMenuStructure-m');

var WcmStructureModel = Backbone.Model.extend({
    defaults: {
        structure: [
            {
                icon        : 'fa fa-list',
                displayText : 'Products',
                subItems    : [
                    {
                        icon        : '',
                        displayText : 'Care Solutions',
                        subItems    : [
                            {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Annuity Care',
                                capability  : {
                                    forSidebar : [{
                                        'WCM_All_But_Bank_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/annuity-care'
                            }, {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Asset-Care',
                                capability  : {
                                    forSidebar : [{
                                        'WCM_All_But_Bank_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/asset-care'
                            }
                        ]
                    },
                    {
                        icon        : '',
                        displayText : 'Term Life',
                        subItems    : [
                            {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'American Protector Plus',
                                capability  : {
                                    forSidebar : [{
                                        'Interest_Rates_All_Products_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/life/american-protector-plus'
                            }, {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Term Conversions',
                                activeFor   : [
                                    'c/product-pages/life/term-conversions/life-events',
                                    'c/product-pages/life/term-conversions/paperwork',
                                    'c/product-pages/life/term-conversions/credit',
                                    'c/product-pages/life/term-conversions/eligibility',
                                    'c/product-pages/life/term-conversions/underwriting',
                                    'c/product-pages/life/term-conversions/asset-care'
                                ],
                                capability  : {
                                    forSidebar : [{
                                        'Interest_Rates_All_Products_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/life/term-conversions'
                            }, {
                                displayText : 'Life Events',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/life-events'
                            }, {
                                displayText : 'Paperwork',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/paperwork'
                            }, {
                                displayText : 'Term Conversion Credit',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/credit'
                            }, {
                                displayText : 'Term Conversion Eligibility',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/eligibility'
                            }, {
                                displayText : 'Term Conversion Underwriting',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/underwriting'
                            }, {
                                displayText : 'Term Conversion to Asset-Care',
                                capability  : {
                                    forPageAccess : {
                                        'Interest_Rates_All_Products_View' : true
                                    }
                                },
                                link        : 'c/product-pages/life/term-conversions/asset-care'
                            }

                        ]
                    }, {
                        icon        : '',
                        displayText : 'Whole Life',
                        capability  : {
                            forSidebar : [{
                                'Interest_Rates_All_Products_View' : true
                            }]
                        },
                        link        : 'c/product-pages/life/whole-life'
                    }, {
                        icon        : '',
                        displayText : 'Fixed Annuities',
                        subItems    : [
                            {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Freedom Builder',
                                capability  : {
                                    forSidebar : [{
                                        'Interest_Rates_All_Products_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/fixed-annuities/freedom-builder'
                            }, {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Secure Income Stream',
                                capability  : {
                                    forSidebar : [{
                                        'Interest_Rates_All_Products_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/fixed-annuities/secure-income-stream'
                            }, {
                                icon        : 'fa fa-arrow-right',
                                displayText : 'Single Premium Immediate Annuity',
                                capability  : {
                                    forSidebar : [{
                                        'Interest_Rates_All_Products_View' : true
                                    }]
                                },
                                link        : 'c/product-pages/fixed-annuities/single-premium-immediate-annuity'
                            }
                        ]
                    }, {
                        icon        : '',
                        displayText : 'Service Only',
                        activeFor   : [
                            'c/product-pages/service-only/atp3-term',
                            'c/product-pages/service-only/term-2002',
                            'c/product-pages/service-only/century',
                            'c/product-pages/service-only/lifestyle-freedom-ul',
                            'c/product-pages/service-only/optimum-plus',
                            'c/product-pages/service-only/optimum-accumulator',
                            'c/product-pages/service-only/american-accumulator',
                            'c/product-pages/service-only/asset-care-single-premium-vul',
                            'c/product-pages/service-only/asset-care-last-survivor-spvul',
                            'c/product-pages/service-only/flexible-premium-vul',
                            'c/product-pages/service-only/last-survivor-flexible-premium-vul',
                            'c/product-pages/service-only/last-survivor-single-premium-vul',
                            'c/product-pages/service-only/single-premium-vul',
                            'c/product-pages/service-only/legacy-2004',
                            'c/product-pages/service-only/liberty',
                            'c/product-pages/service-only/accumannuity-ii',
                            'c/product-pages/service-only/secure5',
                            'c/product-pages/service-only/securechoice',
                            'c/product-pages/service-only/directpoint',
                            'c/product-pages/service-only/starpoint',
                            'c/product-pages/service-only/voyage-protector',
                            'c/product-pages/service-only/portfolio-descriptions'
                        ],
                        capability  : {
                            forSidebar : [{
                                'Interest_Rates_All_Products_View' : true
                            }]
                        },
                        link        : 'c/product-pages/service-only'
                    }, {
                        displayText : 'ATP3',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/atp3-term'
                    }, {
                        displayText : 'Term 2002',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/term-2002'
                    }, {
                        displayText : 'Century',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/century'
                    }, {
                        displayText : 'LifeStyle Freedom UL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/lifestyle-freedom-ul'
                    }, {
                        displayText : 'Optimum Plus',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/optimum-plus'
                    }, {
                        displayText : 'American Accumulator VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/american-accumulator'
                    }, {
                        displayText : 'Optimum Accumulator',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/optimum-accumulator'
                    }, {
                        displayText : 'Asset-Care Single Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/asset-care-single-premium-vul'
                    }, {
                        displayText : 'Asset-Care Last Survivor Single Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/asset-care-last-survivor-spvul'
                    }, {
                        displayText : 'Flexible Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/flexible-premium-vul'
                    }, {
                        displayText : 'Last Survivor Flexible Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/last-survivor-flexible-premium-vul'
                    }, {
                        displayText : 'Last Survivor Single Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/last-survivor-single-premium-vul'
                    }, {
                        displayText : 'Single Premium VUL',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/single-premium-vul'
                    }, {
                        displayText : 'Legacy 2004',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/legacy-2004'
                    }, {
                        displayText : 'Liberty',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/liberty'
                    }, {
                        displayText : 'AccumAnnuity II',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/accumannuity-ii'
                    }, {
                        displayText : 'Secure5',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/secure5'
                    }, {
                        displayText : 'SecureChoice',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/securechoice'
                    }, {
                        displayText : 'DirectPoint',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/directpoint'
                    }, {
                        displayText : 'StarPoint',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/starpoint'
                    }, {
                        displayText : 'Voyage Protector',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/voyage-protector'
                    }, {
                        displayText : 'Portfolio Descriptions',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/product-pages/service-only/portfolio-descriptions'
                    },{
                        icon        : '',
                        displayText : 'Interest Rates',
                        capability  : {
                            forSidebar : [{
                                'Interest_Rates_CS_Products_View' : true
                            }]
                        },
                        link        : 'c/interest-rates/Content-Care-Solutions'
                    },
                    {
                        icon        : '',
                        displayText : 'Interest Rates',
                        capability  : {
                            forSidebar : [{
                                'Interest_Rates_All_Products_View' : true
                            }]
                        },
                        link        : 'c/interest-rates/Content-Retail'
                    }
                ]
            },
            {
                icon        : 'fa fa-calendar-check-o',
                displayText : 'Underwriting',
                subItems    : [
                    {
                        icon        : 'fa fa-play-circle',
                        displayText : 'Underwriting',
                        activeFor   : [
                            'c/underwriting/consider-the-source',
                            'c/underwriting/more-standard-policies',
                            'c/underwriting/pre-underwriting-inquiry',
                            'c/underwriting/pre-underwriting-inquiry-cs',
                            'c/underwriting/military-guidelines',
                            'c/underwriting/programs/preferred-point-system',
                            'c/underwriting/cover-letter',
                            'c/underwriting/asset-care-impairment-guidelines/1',
                            'c/underwriting/asset-care-impairment-guidelines/2',
                            'c/underwriting/asset-care-impairment-guidelines/3',
                            'c/underwriting/asset-care-height-weight'
                        ],
                        capability  : {
                            forSidebar : [{
                                'Contact_List_HO_View' : true
                            }, {
                                'Contact_List_HO_View'     : false,
                                'Contact_List_Retail_View' : true
                            }, {
                                'Contact_List_IB_View'     : true,
                                'Contact_List_HO_View'     : false,
                                'Contact_List_Retail_View' : false
                            }, {
                                'Contact_List_CS_View'      : true,
                                'Contact_List_HO_View'      : false,
                                'Contact_List_Retail_View'  : false,
                                'Contact_List_IB_View'      : false
                            }]
                        },
                        link        : 'c/underwriting'
                    }, {
                        icon        : '',
                        displayText : 'Programs',
                        capability  : {
                            forSidebar : [{
                                'Contact_List_HO_View' : true
                            }, {
                                'Contact_List_IB_View'     : true,
                                'Contact_List_HO_View'     : false,
                                'Contact_List_Retail_View' : false
                            }, {
                                'Contact_List_HO_View'     : false,
                                'Contact_List_Retail_View' : true
                            }, {
                                'Contact_List_CS_View'     : true,
                                'Contact_List_IB_View'     : false,
                                'Contact_List_HO_View'     : false,
                                'Contact_List_Retail_View' : false
                            }]
                        },
                        link        : 'c/underwriting/programs'
                    }, {
                        icon        : '',
                        displayText : 'Vendors',
                        capability  : {
                            forSidebar : [{
                                'WCM_All_Role_View' : true
                            }]
                        },
                        link        : 'c/underwriting/vendors'
                    }, {
                        icon        : '',
                        displayText : 'Foreign Travel',
                        capability  : {
                            forSidebar : [{
                                'WCM_All_Role_View' : true
                            }]
                        },
                        link        : 'c/underwriting/foreign-travel'
                    }, {
                        displayText : 'Consider the Source',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/underwriting/consider-the-source'
                    }, {
                        displayText : 'More Standard Policies',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/underwriting/more-standard-policies'
                    }, {
                        displayText : 'Pre-Underwriting Inquiry',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/underwriting/pre-underwriting-inquiry'
                    }, {
                        displayText : 'Pre-Underwriting Inquiry',
                        capability  : {
                            forPageAccess : {
                                'WCM_CS_HO_View' : true
                            }
                        },
                        link        : 'c/underwriting/pre-underwriting-inquiry-cs'
                    }, {
                        displayText : 'Military Guidelines',
                        capability  : {
                            forPageAccess : {
                                'WCM_Retail_HO_View' : true,
                                'WCM_IB_HO_View'     : true
                            }
                        },
                        link        : 'c/underwriting/military-guidelines'
                    }, {
                        displayText : 'The Underwriting Cover Letter',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/underwriting/cover-letter'
                    }, {
                        displayText : 'Preferred Point System',
                        capability  : {
                            forPageAccess : {
                                'Interest_Rates_All_Products_View' : true
                            }
                        },
                        link        : 'c/underwriting/programs/preferred-point-system'
                    }, {
                        displayText : 'Activities of Daily Living Deficits through Dialysis',
                        capability  : {
                            forPageAccess : {
                                'WCM_All_Role_View' : true
                            }
                        },
                        link        : 'c/underwriting/asset-care-impairment-guidelines/1'
                    }, {
                        displayText : 'Down Syndrome through Non-ocular Myasthenia Gravis',
                        capability  : {
                            forPageAccess : {
                                'WCM_All_Role_View' : true
                            }
                        },
                        link        : 'c/underwriting/asset-care-impairment-guidelines/2'
                    }, {
                        displayText : 'Organic Brain Syndrome through Ventricular Tachycardia',
                        capability  : {
                            forPageAccess : {
                                'WCM_All_Role_View' : true
                            }
                        },
                        link        : 'c/underwriting/asset-care-impairment-guidelines/3'
                    }, {
                        displayText : 'Asset-Care Height and Weight Guidelines',
                        capability  : {
                            forPageAccess : {
                                'WCM_All_Role_View' : true
                            }
                        },
                        link        : 'c/underwriting/asset-care-height-weight'
                    }
                ]
            },
            // Add "The Pathway" menu item
            new PathwayMenuStructureModel().get('structure')[0],
            {
                icon        : 'fa fa-users',
                displayText : 'Contact List',
                link        : 'c/contact-lists/contact-list-home-office',
                capability  : {
                    forSidebar : [{
                        'Contact_List_HO_View' : true
                    }]
                }
            },
            {
                icon        : 'fa fa-users',
                displayText : 'Contact List',
                link        : 'c/contact-lists/contact-list-retail',
                capability  : {
                    forSidebar : [{
                        'Contact_List_HO_View'     : false,
                        'Contact_List_Retail_View' : true
                    }]
                }
            },
            {
                icon        : 'fa fa-users',
                displayText : 'Contact List',
                link        : 'c/contact-lists/contact-list-ib',
                capability  : {
                    forSidebar : [{
                        'Contact_List_IB_View'     : true,
                        'Contact_List_HO_View'     : false,
                        'Contact_List_Retail_View' : false
                    }]
                }
            },
            {
                icon        : 'fa fa-users',
                displayText : 'Contact List',
                link        : 'c/contact-lists/contact-list-care-solutions',
                capability  : {
                    forSidebar : [{
                        'Contact_List_CS_View'      : true,
                        'Contact_List_HO_View'      : false,
                        'Contact_List_Retail_View'  : false,
                        'Contact_List_IB_View'      : false
                    }]
                }
            }, {
                icon        : 'fa fa-question-circle',
                displayText : 'FAQ',
                activeFor   : [
                    'c/faq/oso',
                    'c/faq/oso-how-to-training',
                    'c/faq/oso-how-to-training/registration',
                    'c/faq/oso-how-to-training/delegates/add-delete-edit',
                    'c/faq/oso-how-to-training/delegates/register',
                    'c/faq/oso-how-to-training/delegates/access',
                    'c/faq/oso-how-to-training/find-info/business',
                    'c/faq/oso-how-to-training/tech-tips/pop-up-blocker',
                    'c/faq/oso-how-to-training/performance-center-my-profile',
                    'c/faq/oso-vintage-retirement',
                    'c/faq/underwriting',
                    'c/faq/commissions',
                    'c/faq/commissions/view',
                    'c/faq/oams'
                ],
                capability  : {
                    forSidebar : [{
                        'WCM_All_Role_View' : true
                    }]
                },
                link        : 'c/faq'
            }, {
                displayText : 'OSO FAQ',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso'
            }, {
                displayText : 'Training FAQ',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training'
            }, {
                displayText : 'OSO Registration',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/registration'
            }, {
                displayText : 'Add, Delete or Edit Delegates',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/delegates/add-delete-edit'
            }, {
                displayText : 'Register as a Delegate',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/delegates/register'
            }, {
                displayText : 'Access OneSource Online as a Delegate',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/delegates/access'
            }, {
                displayText : 'View Pending and Inforce Business',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/find-info/business'
            }, {
                displayText : 'Disable Pop-up Blockers',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/tech-tips/pop-up-blocker'
            }, {
                displayText : 'Performance Center and My Profile',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-how-to-training/performance-center-my-profile'
            }, {
                displayText : 'Vintage Retirement FAQ',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/oso-vintage-retirement'
            }, {
                displayText : 'Underwriting FAQ',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/underwriting'
            }, {
                displayText : 'Commissions FAQ',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/commissions'
            }, {
                displayText : 'View Commissions',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/faq/commissions/view'
            }, {
                displayText : 'OA Marketing Store FAQs',
                capability  : {
                    forPageAccess : {
                        'Interest_Rates_All_Products_View' : true
                    }
                },
                link        : 'c/faq/oams'
            }, {
                displayText : 'Emergency',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/ep/oso-1'
            },{
                displayText : 'Emergency',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/ep/oso-2'
            },{
                displayText : 'Emergency',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/ep/oso-3'
            },{
                displayText : 'Emergency',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/ep/oso-4'
            },{
                displayText : 'Emergency',
                capability  : {
                    forPageAccess : {
                        'WCM_All_Role_View' : true
                    }
                },
                link        : 'c/ep/oso-5'
            }, {
                displayText : 'Fiduciary News',
                capability  : {
                    forPageAccess : {
                        'WCM_Retail_HO_View' : true
                    }
                },
                link        : 'c/fiduciary/retail-fiduciary-news'
            }, {
                displayText : 'Fiduciary News',
                capability  : {
                    forPageAccess : {
                        'WCM_IB_HO_View' : true
                    }
                },
                link        : 'c/fiduciary/ib-fiduciary-news'
            }, {
                displayText : 'Fiduciary News',
                capability  : {
                    forPageAccess : {
                        'WCM_CS_Not_Bank_HO_View' : true
                    }
                },
                link        : 'c/fiduciary/cs-fiduciary-news'
            }
        ]
    }
});

module.exports = WcmStructureModel;

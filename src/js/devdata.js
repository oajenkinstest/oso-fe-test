var _global = _global || (function() { return this; })();

var $ = require('jquery');

// Set $ as a global variable
_global.$ = _global.$ || (_global.jQuery = $);

require('jquery.cookie');

// Set _ as a global variable
var _ = require('underscore');
_global._ = _global._ || _;

var Backbone = require('backbone');
_global.Backbone = _global.Backbone || Backbone;

// Load the shim to allow Marionette to use Radio instead of Wreqr
require('backbone.radio');
require('radio.shim');

// Set debug as global variable
var debug = require('./modules/debug/debugModule');
_global.debug = _global.debug || (_global.debug = debug);

module.exports = _global;

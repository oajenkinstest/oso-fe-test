/* global Backbone: false, Marionette:false */

/**
 * LayoutView for the home page. This page loads the following modules:
 *
 * - Performance Center (if the user has the 'Producer_Performance_View' capability)
 *
 */

var errorHelper               = require('../../../utils/error-helper');
var homeTemplate              = require('../templates/home-t.hbs');
var checkpoint                = require('../../../modules/checkpoint/checkpointModule');
var FormsView                 = require('./ipipeline-forms-v');
var SalesConnectionLinksView  = require('./sales-connection-links-v');
var WcmToutView               = require('./wcm-tout-v');

//Performance Center class
var PerformanceCenterView  = 
    require('../../../modules/performance-center/views/performance-center-v');

var ToutView = require('../../../modules/tout/views/tout-v');

// add global partials
require('../../../partials');

var userChannel = Backbone.Radio.channel('user');


// LayoutView to contain the components on the page
var HomeView = Marionette.LayoutView.extend({

    template : homeTemplate,
    
    ui : {
        myBusinessHeader             : '#my-business-header',
        myBusinessWell               : '#my-business-well',
        educationDevelopmentHeader   : '#education-development-header',
        educationDevelopmentWell     : '#education-development-well'
    },

    regions : {
        coeLinkRegion              : '#coe-link-region',
        fiduciaryRegion            : '#fiduciary-link-region',
        formsRegion                : '#forms-region',
        oamsRegion                 : '#oams-link-region',
        performanceCenterRegion    : '#performance-center-region',
        policiesToutRegion         : '#policies-tout-region',
        salesConnectionLinksRegion : '#sales-connection-links-region'
    },

    errors : {
        noDelegateTargetUsers : 'There are no active producers for your delegate access'
    },

    initialize: function () {
        this.appStructureChannel = Backbone.Radio.channel('appStructure');

        //remove all invalid sub page hashes 
        if (this.options && this.options.stateObj && this.options.stateObj.subpages) {
            this.options.stateObj.subpages = [];
            checkpoint.writeCheckpoint(this.options.stateObj, true);
        }

        this.model =  new Backbone.Model();

        // Determine HO capability
        this.model.set('hasHOCapability', userChannel.request('hasCapability', 'Home_Office'));

        // IPipeline_iGO capability check
        var canViewSalesConnectionLinks = userChannel.request('hasCapability', 'IPipeline_iGO');
        var canViewDisabledLinks        = userChannel.request('hasCapability',
            'IPipeline_iGO(disabled)'
        );

        if (canViewSalesConnectionLinks) {
            this.model.set('showSalesConnectionLinksRegion', true);
        }
        this.model.set('showSalesConnectionLinksRegionDisabled', canViewDisabledLinks); 

        // IPipeline_FormsPipe capability check
        var canViewForms    = userChannel.request('hasCapability', 'IPipeline_FormsPipe');
        var canViewDisabled = userChannel.request('hasCapability', 
            'IPipeline_FormsPipe(disabled)'
        );

        this.model.set('showFormsRegion', canViewForms);
        this.model.set('showFormsRegionDisabled', canViewDisabled);

        //Pinpoint_LMS capability check
        var canViewCOELink          = userChannel.request('hasCapability', 'Pinpoint_LMS');
        var canViewCOELinkDisabled  = userChannel.request('hasCapability', 
            'Pinpoint_LMS(disabled)'
        );

        this.model.set('showCOELinkRegion', canViewCOELink);
        this.model.set('showCOELinkRegionDisabled', canViewCOELinkDisabled);

        this._setFiduciaryCapabilitiesInModel();

        this._setOAMSCapabilitiesInModel();

        this._setEducationDisplayAttributesInModel();
    },

    onBeforeShow: function () {

        var canViewProducerPerformance = userChannel.request('hasCapability', 
                'Producer_Performance_View');

        // whether user is delegate user
        var isDelegate = userChannel.request('hasCapability', 'OLS:DELEGATE');
        var isProducer = userChannel.request('hasCapability', 'OLS:PRODUCER');

        // Get the producers this user can be a delegate for (will be an empty array if none)
        var delegateTargetProducers = userChannel.request('getDelegateTargets');

        if (isDelegate && !isProducer && !delegateTargetProducers.length) {
            this.model.set('alertMessage', 
                errorHelper.createAlert(
                    this.errors.noDelegateTargetUsers, 'info'
                )
            );

            this.render();

            return false;
        }

        // Do not attempt to load the PC if the user shouldn't see it
        if (canViewProducerPerformance) {

            //option showInfoLinks is not mandatory, by default it is false
            this.showChildView('performanceCenterRegion', new PerformanceCenterView({
                showInfoLinks: false
            }));
        }

        var pageData = this.appStructureChannel.request(
            'getPageDataByKeyValue', 'defaultMyBusinessPage', true
        );
        
        if (pageData) {

            var policiestext='';
            var title = '';

            title = pageData.displayText;

            //define text to display under title
            if (this.model.get('hasHOCapability')) {
                policiestext = 'Find policies and <br class="visible-lg">View as Producer';
            } else {
                policiestext = 'Find policies and pending lists';
                title = 'View ' + title;
            }

            // create and display the policies tout
            this.showChildView('policiesToutRegion', new ToutView({
                iconCss : 'ace-icon fa fa-pencil brown fa-3x',
                title   : title,
                text    : policiestext,
                href    : '#'+pageData.link
            }));
            
            // show the myBusinessHeader and myBusinessWell
            this.ui.myBusinessHeader.removeClass('hidden');
            this.ui.myBusinessWell.removeClass('hidden');
        }

        // Show Sales Connection links
        this._displaySalesConnectionLinks();

        // Show IPipeline Forms links
        this._displayForms();

        //Show COE link
        this._displayCOELink();

        // Show Fiduciary tout if allowed
        if (this.model.get('showFiduciaryToutRegion')) {
            this._displayFiduciaryLink();
        }

        // Show the OAMS tout if allowed
        this._displayOAMSLink();

    },

    /**
     * Show Sales Connection links such as eApp/Illustration, Sales Connection Practice
     * and care solutions products links based up on user capability
     * 
     * @private
     */
    _displaySalesConnectionLinks : function _displaySalesConnectionLinks () {
        var showCareSolutionsProductsLink;
        var isAdvancedHomeOfficeUser;

        if (this.model.get('showSalesConnectionLinksRegion')) {

            // Advanced home office user check
            isAdvancedHomeOfficeUser = ( 
                userChannel.request('hasCapability', 'OLS:HO:OVERRIDE:IGO.EAPP')
                || userChannel.request('hasCapability', 'OLS:HO:OVERRIDE:IGO.ILLUSTRATIONS')
                || userChannel.request('hasCapability', 'OLS:HO:OVERRIDE:IGO.ADMIN')
            );

            // Care Solution Products Link will be displayed only
            // for advanced home user
            if (this.model.get('hasHOCapability') && isAdvancedHomeOfficeUser) {
                showCareSolutionsProductsLink = true;
            }

            this.showChildView('salesConnectionLinksRegion', new SalesConnectionLinksView({
                showCareSolutionsProductsLink : showCareSolutionsProductsLink
            }));
        } else if (this.model.get('showSalesConnectionLinksRegionDisabled')) {
            this.showChildView('salesConnectionLinksRegion', new SalesConnectionLinksView({
                showDisabled : true 
            }));
        }
    },

    /**
     * Show the Fiduciary tout from WCM based on the user's capability.
     * @private
     */
    _displayFiduciaryLink : function _displayFiduciaryLink () {
        var wcmPath;

        if (this.model.get('hasHOCapability')) {
            wcmPath = 'touts/homepage/education-and-practice-development/fiduciary-ho';
        } else if (this.model.get('showFiduciaryRetail')) {
            wcmPath = 'touts/homepage/education-and-practice-development/fiduciary-retail';
        } else if (this.model.get('showFiduciaryIB')) {
            wcmPath = 'touts/homepage/education-and-practice-development/fiduciary-ib';
        } else {
            wcmPath = 'touts/homepage/education-and-practice-development/fiduciary-cs';
        }

        this.showChildView('fiduciaryRegion', new WcmToutView({
            wcmPath : wcmPath
        }));
    },

    /**
     * Determine which of the three views for the Forms tout the user is allowed to see.
     * This is determined by capability:
     *
     * @see {@link https://oneamerica.atlassian.net/browse/OOSO-3226|OOSO-3226}
     * @private
     */
    _displayForms : function _displayForms () {

        if (this.model.get('showFormsRegion')) {
            this.showChildView('formsRegion', new FormsView());

        } else if (this.model.get('showFormsRegionDisabled')) {
            this.showChildView('formsRegion', new FormsView({ showDisabled : true }));
        }
    },

    /**
     * Show Center of Excellence tout from WCM. Manipulate the tout based on
     * whether or not the tout should be displayed in a disabled state.
     * 
     * @private
     */
    _displayCOELink : function _displayCOELink () {
        var showCOELinkRegion         = this.model.get('showCOELinkRegion');
        var showCOELinkRegionDisabled = this.model.get('showCOELinkRegionDisabled');
      
        if (showCOELinkRegion || showCOELinkRegionDisabled) {
            this.showChildView('coeLinkRegion', new WcmToutView({
                wcmPath            : 'touts/homepage/education-and-practice-development/coe',
                showDisabledIcon   : showCOELinkRegionDisabled,
                buildHrefUrl       : true
            }));
        }
    },

    /**
     * Display the OneAmerica Marketing Store tout from WCM.
     * @private
     */
    _displayOAMSLink : function _displayOAMSLink() {
        var showOAMSToutRegion         = this.model.get('showOAMSToutRegion');
        var showOAMSToutRegionDisabled = this.model.get('showOAMSToutRegionDisabled');
        var impersonatedWebId          = userChannel.request('getImpersonatedWebId');
        var wcmPath                    = 'touts/homepage/education-and-practice-development/oams';

        if (impersonatedWebId) {
            wcmPath = wcmPath+'-delegate';
        }

        if (showOAMSToutRegion || showOAMSToutRegionDisabled) {
            this.showChildView('oamsRegion', new WcmToutView({
                wcmPath : wcmPath,
                showDisabledIcon : showOAMSToutRegionDisabled
            }));
        }
    },

    /**
     * Query the capabilities related to the Fiduciary links and set them in the model.
     * @private
     */
    _setFiduciaryCapabilitiesInModel : function _setFiduciaryCapabilitiesInModel () {
        var hasCSNoBank = userChannel.request('hasCapability', 'WCM_CS_Not_Bank_HO_View');
        var hasIB       = userChannel.request('hasCapability', 'WCM_IB_HO_View');
        var hasRetail   = userChannel.request('hasCapability', 'WCM_Retail_HO_View');
        var showTout    = hasCSNoBank || hasIB || hasRetail || this.model.get('hasHOCapability');

        this.model.set('showFiduciaryRetail', hasRetail);
        this.model.set('showFiduciaryIB', hasIB);
        this.model.set('showFiduciaryCS', hasCSNoBank);
        this.model.set('showFiduciaryToutRegion', showTout);
    },

    /**
     * Query the capabilities related to the OneAmerica Marketing Store and set them in the model.
     * @private
     */
    _setOAMSCapabilitiesInModel : function _setOAMSCapabilitiesInModel () {
        this.model.set('showOAMSToutRegion', userChannel.request('hasCapability', 'OAMS_View'));
        this.model.set('showOAMSToutRegionDisabled', 
            userChannel.request('hasCapability', 'OAMS_View(disabled)'));
    },

    /**
     * Determine the number of touts which will be displayed in the
     * "Education & Practice Development" section.
     * @return {number}
     * @private
     */
    _getEducationToutCount : function _getEducationToutCount () {
        var touts = 0;

        if (this.model.get('showCOELinkRegion') || this.model.get('showCOELinkRegionDisabled')) {
            touts++;
        }

        if (this.model.get('showFiduciaryToutRegion')) {
            touts++;
        }

        if (this.model.get('showOAMSToutRegion') || this.model.get('showOAMSToutRegionDisabled')) {
            touts++;
        }

        return touts;
    },

    /**
     * Set attributes in the model which will be used to display the
     * "Education & Practice Development" section and related column widths.
     * @private
     */
    _setEducationDisplayAttributesInModel : function _setEducationDisplayAttributesInModel () {
        var colClass            = 'col-sm-4';

        // Count of tout available to display. It can be used to define column(colClass) layout
        var totalEducationTouts = this._getEducationToutCount();

        // Display the education section if we have at least one tout
        this.model.set('showEducationSection', (totalEducationTouts > 0));

        this.model.set('educationColumnWidthClass', colClass);
    }
    
});

module.exports = HomeView;

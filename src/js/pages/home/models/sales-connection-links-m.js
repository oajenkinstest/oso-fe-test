/* global Backbone:false */
/**
 * Simple model used to retrieve data used for single sign-on to iPipeline forms.
 */
var config = require('../../../config/config');

var SalesConnectionLinksModel = Backbone.Model.extend({

    urlRoot : config.apiUrlRoot + 'sso/ipipeline/igo'

});

module.exports = SalesConnectionLinksModel;
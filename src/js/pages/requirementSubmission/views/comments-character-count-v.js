/* global Backbone:false */
/**
 * View used to display the character count for the comments section of the
 * Requirement Submission form.
 */

var template                           = require('../templates/comments-character-count-t.hbs');
var RequirementSubmissionFormViewModel = require('../viewModels/requirement-submission-form-vm');

var CommentsCharacterCountView = Backbone.Marionette.ItemView.extend({

    errors   : {
        invalidOptions : 'Options passed to CommentsCharacterCountView ' +
                         'must contain either a policyId or a model'
    },

    template : template,

    initialize : function initialize(options) {

        if (!options.model && !options.policyId) {
            throw new Error(this.errors.invalidOptions);

        } else if (options.model) {
            this.model = options.model;

        } else {
            this.model = new RequirementSubmissionFormViewModel({
                policyId : options.policyId
            });
        }

        this.listenTo(this.model, 'change:commentsLength', this.render);
    }

});

module.exports = CommentsCharacterCountView;
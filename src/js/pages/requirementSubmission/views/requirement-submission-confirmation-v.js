/* global Backbone:false */
/**
 * Displays the confirmation message and navigation links after a successful form
 * submission.
 */

// Load partials
require('../partials');

// add handlebar helpers
require('../../../utils/hb-helpers');

var template = require('../templates/requirement-submission-confirmation-t.hbs');

var RequirementSubmissionConfirmationPageView = Backbone.Marionette.ItemView.extend({

    errors   : {
        noModel : 'An existing RequirementSubmissionFormViewModel must be passed to the page!'
    },

    template : template,

    initialize : function initialize(options) {

        if (!options.model) {
            throw new Error(this.errors.noModel);
        } else {
            this.model = options.model;
        }

        this.render();
    }

});

module.exports = RequirementSubmissionConfirmationPageView;
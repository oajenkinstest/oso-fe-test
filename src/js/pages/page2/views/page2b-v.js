/* global Marionette */
/**
 *
 * Created by Jason.Bell on 3/9/2015.
 */
var page2bTemplate = require('../templates/page2b-t.hbs');


var Page2bView = Marionette.ItemView.extend({
    template: page2bTemplate
});

module.exports = Page2bView;

/* global Marionette */
/**
 * The simple, default view of the Page1 module.
 * Created by Jason.Bell on 3/10/2015.
 */

var page2aTemplate = require('../templates/page2a-t.hbs');

var Page2aView = Marionette.ItemView.extend({
    template: page2aTemplate
});
module.exports = Page2aView;

/* global Marionette:false */
/**
 * UI Test View to render the WCM content
 * It use wcm-content-v which require 'wcmPath' and 'el' as options.
 *
 * wcmPath - a path to load the html content from connectServlet
 * el - element from template provided to place the HTML contwent
 *
 * Created by jbell on 1/25/16.
 */

var wcmTestTemplate  = require('../templates/wcm-test-t.hbs');
var WcmView = require('../../../modules/wcm-content/views/wcm-content-v');

var WcmTestView = Marionette.LayoutView.extend({
    template : wcmTestTemplate,

    onBeforeShow: function () {

        var wcmView = new WcmView({
            wcmPath: 'Sales+Support/Selling+Concepts+to+Grow+Your+Practice/'+
                        'Selling+Concepts+to+Grow+Your+Practice'
        });

        wcmView.render();
    }
});

module.exports = WcmTestView;

/* global clients:false*/
/* eslint-disable no-console */
'use strict';

var payload;
self.addEventListener('push', function(event) {
    console.log('[Service Worker] Push Received.');
    console.log('[Service Worker] Push had this data: '+event.data.text());
    
    payload = JSON.parse(event.data.text());

    var title = payload.title;

    /** Options can have below properties:
     * var options = {
     *     body: 'This notification was generated from a push!',
     *     icon: 'images/example.png',
     *     badge: '',
     *     vibrate: [100, 50, 100],
     *     data: {
     *         dateOfArrival: Date.now(),
     *         primaryKey: '2'
     *     },
     *     actions: [
     *        {action: 'explore', title: 'Explore this new world',
     *                 icon: 'images/checkmark.png'},
     *        {action: 'close', title: 'Close',
     *                 icon: 'images/xmark.png'},
     *      ]
     * };
     */
    var options = {
        body: payload.message,
        icon: '/images/touch-icon-128x128.png'
    };
  
    event.waitUntil(self.registration.showNotification(title, options));
});
  
self.addEventListener('notificationclick', function(event) {
    console.log('[Service Worker] Notification click Received.');
    
    event.notification.close();
  
    if (payload.clickTarget) {
        event.waitUntil(
            clients.openWindow(payload.clickTarget)
        );
    }
});
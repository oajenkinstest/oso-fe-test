var Handlebars = require('hbsfy/runtime');

Handlebars.registerPartial(
    'policy-identification',
    require('./policy-identification.hbs')
);

Handlebars.registerPartial(
    'application-status',
    require('./application-status.hbs')
);

Handlebars.registerPartial(
    'billing-payment-info',
    require('./billing-payment-info.hbs')
);

Handlebars.registerPartial(
    'jump-links',
    require('./jump-links.hbs')
);

Handlebars.registerPartial(
    'policy-highlights',
    require('./policy-highlights.hbs')
);

Handlebars.registerPartial(
    'policy-relationships',
    require('./policy-relationships.hbs')
);
Handlebars.registerPartial(
    'policy-relationships-table-row',
    require('./policy-relationships-table-row.hbs')
);
Handlebars.registerPartial(
    'coverages',
    require('./coverages.hbs')
);

Handlebars.registerPartial(
    'rating-information',
    require('./rating-information.hbs')
);

Handlebars.registerPartial(
    'rating-information-table-row',
    require('./rating-information-table-row.hbs')
);

Handlebars.registerPartial(
    'rating-information-col-insured-rateclass',
    require('./rating-information-col-insured-rateclass.hbs')
);

Handlebars.registerPartial(
    'rating-information-rating-data-asset-care',
    require('./rating-information-rating-data-asset-care.hbs')
);

Handlebars.registerPartial(
    'rating-information-rating-data-non-asset-care',
    require('./rating-information-rating-data-non-asset-care.hbs')
);

Handlebars.registerPartial(
    'related-policies',
    require('./related-policies.hbs')
);

Handlebars.registerPartial(
    'policy-requirements',
    require('./policy-requirements.hbs')
);

Handlebars.registerPartial(
    'policy-requirement-submission-log-items',
    require('./policy-requirement-submission-log-items.hbs')
);

Handlebars.registerPartial(
    'investment-allocation',
    require('./investment-allocation.hbs')
);

Handlebars.registerPartial(
    'policy-workflow-status-help-text-modal',
    require('./policy-workflow-status-help-text-modal.hbs')
);

Handlebars.registerPartial(
    'policy-1035-exchange-history-comments',
    require('./policy-1035-exchange-history-comments.hbs')
);

Handlebars.registerPartial(
    'table-responsive-instruction',
    require('../../../partials/table-responsive-instruction.hbs')
);

module.exports = Handlebars;

/* global Backbone:false, _:false*/

/**
* policy details model
* 
* To build url to fetch data:
* if Policy Number exist: 
*   new PolicyDetailModel({
*       id : 1234567890   
*    });
*
* if policy number is not ready yet but there is caseId:
*   this.model = new PolicyDetailModel({
*       caseId:212121
*   });
**/

var applicationStatusTrackerParser;
var config                          = require('../../../config/config');

var policyDataAvailability          = require('./parsers/policy-data-availability');

var Policy1035ExchangeHistory       = require('./parsers/policy-1035-exchange-history');
var policy1035ExchangeParser;

var PolicyApplicationStatusTracker  = require('./parsers/policy-application-status-tracker');

var PolicyBillingPayment            = require('./parsers/policy-billing-payment');
var policyBillingPayment;

var policyCoverage                  = require('./parsers/policy-coverage');

var policyIdentification            = require('./parsers/policy-identification');

var PolicyInvestmentAllocation      = require('./parsers/policy-investment-allocation');
var policyInvestmentAllocation;

var PolicyRatingInformation         = require('./parsers/policy-rating-information');
var policyRatingInformation;

var PolicyRelationships             = require('./parsers/policy-relationships');
var policyRelationships;

var policyRequirements              = require('./parsers/policy-requirements');

var PolicyValue                     = require('./parsers/policy-value');
var policyValue;

var utils                           = require('../../../utils/utils');

var userChannel      = Backbone.Radio.channel('user');

var PolicyDetailModel = Backbone.Model.extend({

    errors : {
        missingURLParam     : 'URL is missing to add targetuser parameter',
        missingRelParameter : 'relation partmeter is missing to find link'
    },

    /**
     * Return the URL root based on the values in the model.
     * If 'id' is not set, it uses the URL with a caseId querystring parameter.
     * @returns {string}
     */
    urlRoot: function () {
        if (this.id) {
            return config.apiUrlRoot + 'policies';  
        } else if (this.get('caseId')) {
            return config.apiUrlRoot + 'policies?caseId='+ this.get('caseId');
        } else {
            throw new Error('PolicyDetailModel.urlRoot: missing \'id\' or \'caseId\'');
        }
    },


    /**
     * Override the default parse in order to set some flags based on other values within the
     * returned data.
     * 
     * @param response the JSON response from the service
     * @returns {*} the response with flags set based on returned values
     */
    parse: function (response) {

        response = policyDataAvailability.deleteNotImplemented(response);
        response = policyDataAvailability.nullifyNotAvailableChildProperties(response);

        //Flag to notify whether policy detail is ready
        response.hasPolicyDetails = true;

        //set an object to show disclaimer note to a specific users
        response = policyCoverage._setDisclaimerNoteState(response);

        //set whether user is annuitant
        response = policyCoverage._setAnnuitantState(response);

        //set whether to use issueAge or jointEqualAge
        response = policyCoverage._setCoverageAge(response);

        // determine the Coverage section header
        response = policyCoverage._setCoverageHeaderLeadingText(response);

        //set labels for related policy detail in the "Coverage" section
        response = policyCoverage._setRelatedPoliciesLabels(response);

        // set flag for header in coverages.
        response = policyCoverage._setIsUlVulFlag(response);

        response = policyCoverage._setHasPUAAmountFlag(response);

        policyInvestmentAllocation = new PolicyInvestmentAllocation(); 

        // set whether policy is indexed annuity
        response = policyInvestmentAllocation._setIndexedAnnuityState(response);

        if (utils.isImplemented(response.customerRoles)) {
            policyRelationships = new PolicyRelationships();
            //set  data or Policy Relationship section
            response = policyRelationships._setCustomerPolicyRelationshipData(response);
        }

        // set policy identification information
        response = policyIdentification._setPolicyIdentificationLabelAndName(response);

        // Generate contact mail link (caseManager / Policy Service Contact)
        response = policyIdentification._setContactMailto(response);

        //set active status flag
        response = policyIdentification._setActivePolicyFlag(response);

        response = policyIdentification._setAbandonedPolicyFlag(response);
        response = policyIdentification._setShowflag(response);

        // To determing display of Writing / Servicing agents
        response = policyIdentification._setShowAgentsFlag(
            response,
            this._getActivePolicyPaidStatus(response)
        );

        // sort writing producers
        response = policyIdentification._sortWritingProducers(response);

        // set alert messages
        response = policyIdentification._setAlertAttentionMessage(response);

        response = policyRequirements.setCustomerFirstNamePolicyRequirements(response);

        // set counts on requirements
        response = policyRequirements.setRequirementsCounts(response);
        
        //sort received policy requirement list
        response = policyRequirements.sortPolicyRequirements(response);

        response.showRequirements = policyRequirements.shouldShowRequirements(response);
        
        if (!policyRequirements.shouldShowReceivedRequirements(response)) {
            response = policyRequirements.deleteReceivedRequirements(response);
        }

        if (utils.isImplemented(response.ratingInformation)) {
            policyRatingInformation = new PolicyRatingInformation();
            // set flags and data to display rating information in the coverage section
            response = policyRatingInformation._setRatingInformationFlagsAndData(response);
        }

        //sort exchange history
        var exchangeHistory = response.exchangeHistory;
        if (exchangeHistory && utils.isImplemented(exchangeHistory)) {
            policy1035ExchangeParser              = new Policy1035ExchangeHistory(exchangeHistory);
            response.exchangeHistory              = policy1035ExchangeParser.sort();
            response.exchangeHistory.statusCounts = policy1035ExchangeParser.getStatusCounts();
        }

        // parse policyStatus to get icons for the "Application Status" tracker
        if (response.policyStatus && response.application) {

            // instantiate the parser
            applicationStatusTrackerParser
                = new PolicyApplicationStatusTracker(response.policyStatus, response.application,
                    response.policyId);

            // append the icon properties object to the application object
            response.application.statusIcons
                = applicationStatusTrackerParser.getIconPropertiesObject();

            // set application display flags
            applicationStatusTrackerParser._setApplicationStatusDisplayFlags(
                    this._getActivePolicyPaidStatus(response)
                );

            // set the acceleratedUnderwritingMessage if acceleratedUnderwriting exists
            if (response.application.acceleratedUnderwriting) {
                response.application.acceleratedUnderwritingMessage =
                    applicationStatusTrackerParser._setAcceleratedUnderwritingMessage(
                        response.application.acceleratedUnderwriting
                    );
            }
        }

        policyBillingPayment = new PolicyBillingPayment();

        response = policyBillingPayment._setBillingDetailDisplayFlags(response);

        if (response.isActive) {
            policyValue = new PolicyValue();

            response = policyValue.setNetSurrenderValueAmountLabel(response);
            response = policyValue.setCashValueAmountLabel(response);
        }

        //return final response as model object
        return response;
    },

    /**
     * Get active policies paid status whether its within / after 30days
     * @param  {object} response Service response
     * @return {object}          paidStatus object with flags for within/after days
     */
    _getActivePolicyPaidStatus : function _getActivePolicyPaidStatus (response) {

        var paidStatus = {
            afterDisplayPeriod   : false,
            withinDisplayPeriod  : false,

            // There is no logic change to display Servicing agent under
            // Policy Identification section
            after30Days         : false
        };

        // Hide icons is holding status is not Active, Unknown, Dormant or Inactive
        // if paid date is within 92 days, 
        // if its more than 92 days hide Application Status section itself
        if ((response.policyStatus && response.policyStatus.acordHoldingStatus !== 'Proposed')
                && (response.application && response.application.paid)) {
            var dateDifference = utils.dateDiff(response.application.paid);

            if (dateDifference > 92) {
                paidStatus.afterDisplayPeriod = true;
            } else {
                paidStatus.withinDisplayPeriod = true;
            }

            if (dateDifference > 30) {
                paidStatus.after30Days = true;
            }
        }

        return paidStatus;
    },

    /**
     * Return HAL links by parsing _links property
     *
     * @return {object}   model object created with _links property
     * @private
     */
    _getLinks : function _getLinks () {
        if(this.has('_links')
            && _.isObject(this.get('_links'))
            && !_.isEmpty(this.get('_links'))) {
            return  new Backbone.Model(this.get('_links'));
        }

        return undefined;
    },

    /**
     * Return HAL link based on relation type
     *
     * @param {string} rel relation type specified in _link property
     * @return {object} model object created with relation type provided.
     * @private
     */
    _getLinkForRel : function _getLinkForRel (rel) {
        if (!rel) {
            throw new Error (this.errors.missingRelParameter);
        }
        var links = this._getLinks();
        if (links && links instanceof Backbone.Model
            && links.get(rel)
            && _.isObject(links.get(rel))
            && !_.isEmpty(links.get(rel))) {
            return new Backbone.Model(links.get(rel));
        }
        return undefined;
    },

    /**
     * Add 'hasUnderwriterAndFiles' flag and documents URL(application and illustration) in
     * application object based on availability of data.
     */
    setUnderwriterAndFilesAvailabilityFlag : function setUnderwriterAndFilesAvailabilityFlag () {
        var application         = this.get('application');
        var applicationDoc      = this._getLinkForRel('application-document');
        var illustrationDoc     = this._getLinkForRel('illustration-document');
        var baseURL;
        
        // IE 10 support 
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + '//' + window.location.hostname + 
                (window.location.port ? ':' + window.location.port : '');
        }

        baseURL = location.origin + this.url();

        if ( (application && application.underWriter)
            ||(applicationDoc && applicationDoc.get('href'))
            || (illustrationDoc && illustrationDoc.get('href'))) {

            application.hasUnderwriterAndFiles = true;

            if (applicationDoc && applicationDoc.get('href')) {

                application.applicationDocHref = utils.mergeRelativeToAbsoluteURL(
                    baseURL, 
                    this._addTargetuser(applicationDoc.get('href'))
                );
            }

            if (illustrationDoc && illustrationDoc.get('href')) {

                application.illustrationDocHref = utils.mergeRelativeToAbsoluteURL(
                    baseURL, 
                    this._addTargetuser(illustrationDoc.get('href'))
                );
            }
        }
    },

    /**
     * Add target users to Application and Illustration url
     * while user in impersonated state.
     *
     * @param {string} url
     * @return {string} url Updated with adding targetuser param
     * @private
     */
    _addTargetuser : function _addTargetuser (url) {
        if (!url) {
            throw new Error(this.errors.missingURLParam);
        }
        var impersonatedWebId   = userChannel.request('getImpersonatedWebId');
        var separator;
        if (impersonatedWebId) {
            separator = url.indexOf('?') !== -1 ? '&' : '?';
            url = url + separator + 'targetuser='+ impersonatedWebId;
        }

        return url;
    }

});

module.exports = PolicyDetailModel;
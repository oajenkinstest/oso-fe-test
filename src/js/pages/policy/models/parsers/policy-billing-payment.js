/* global Marionette, _:false*/
/**
 A model parser class which parses the data for Policy billing data

 */
var config               = require('../../../../config/config');
var policyBillingPayment = Marionette.Object.extend({

    /**
     * Logic to determine if 2 large sections of the billingDetail / payout section will display.
     * If the response contains the billingDetail section, this function will set the following
     * fields within that section:
     * - showBillingModeAndMethod (true if any of paymentMode, paymentMethod exist in 
     *      billingDetail)
     * - showCashIncomePlan (true if any of cashWithApplication, paymentAmount, 
     *      billedToDate, totalDepositsToDate, controlNumber and 
     *      lastPremiumDate exist in billingDetail and any of payout.incomeOption, 
     *      payout.payoutStartDate, payout.nextIndexDate or policy.consolidatedPlan exist)
     *
     * @param response the response from the server as a JSON object
     * @returns {*} the response, with the added flags, if the billingDetail section exists
     * @private
     */
    _setBillingDetailDisplayFlags: function _setBillingDetailDisplayFlags(response) {
        var controlNumber;
        var paymentAmount;
        var paymentDraftDay;
        var paymentMethod;
        var paymentMode;
        
        var incomeOption;
        var nextIndexDate;
        var payoutStartDate;

        if (response.billingDetail && !_.isEmpty(response.billingDetail)) {
            
            /****
             *  This feature flag logic will be removed as Part of OOSO-3900 
             */
            // Add feature flag to billing detail to hide new active fields
            if (config.fflag && config.fflag.showActiveBillingFields) {
                response.billingDetail.showActiveBillingFields = true;
            }

            if (response.billingDetail.bankDraft) {
                paymentDraftDay = response.billingDetail.bankDraft.paymentDraftDay;
                controlNumber   = response.billingDetail.bankDraft.controlNumber;
            }

            if (response.billingDetail.currentBilling) {
                paymentMode     = response.billingDetail.currentBilling.paymentMode;
                paymentMethod   = response.billingDetail.currentBilling.paymentMethod;
                paymentAmount   = response.billingDetail.currentBilling.paymentAmount;
            }

            
            response.billingDetail.showBillingModeAndMethod = (
                paymentMode 
                    || paymentMethod
                    || (response.billingDetail.showActiveBillingFields &&
                        (
                            response.billingDetail.paidToDate
                            || paymentDraftDay
                        )
                    )
            );

            if (response.payout) {
                if (response.payout.incomeOption) {
                    incomeOption = response.payout.incomeOption;
                }

                if (response.payout.payoutStartDate) {
                    payoutStartDate = response.payout.payoutStartDate;
                }
                nextIndexDate = response.payout.nextIndexDate;
            }

            response.billingDetail.showCashIncomePlan = (
                incomeOption 
                    || response.billingDetail.cashWithApplication
                    || payoutStartDate 
                    || response.consolidatedPlan
                    || response.billingDetail.premiumSchedule
                    || (response.billingDetail.showActiveBillingFields &&
                        (
                            nextIndexDate
                            || paymentAmount
                            || response.billingDetail.billedToDate
                            || response.billingDetail.totalDepositsToDate
                            || controlNumber
                            || response.billingDetail.lastPremiumDate
                        )
                    )
            );
        }

        return response;
    }
    
});

module.exports = policyBillingPayment;
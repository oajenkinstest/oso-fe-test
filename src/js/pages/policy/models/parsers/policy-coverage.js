/* global _:false */
/**
 A model parser class which parses the data for policyCoverage

 */


var policyCoverage = {


    /**
    * set visibility state for 'disclaimer note' based on requirement
    * This is determined by combinations of productTypeCategory,
    * workflowStatus, status, and underwritingRequired.
    *
    * The following possible combinations would result in the flag being TRUE:
    *
    * =================================================================================
    * | productTypeCategory | underwritingRequired |  workflowStatus      |   status   |
    * ====================== ====================== ====================== =============
    * |        Life         |         true         | Application Received |    *any*   |
    * ----------------------------------------------------------------------------------
    * |        Life         |         true         |    In Underwriting   |    *any*   |
    * ----------------------------------------------------------------------------------
    * |        Life         |         true         |         *any*        |  Declined  |
    * ----------------------------------------------------------------------------------
    * |        Life         |         true         |         *any*        |  Postponed |
    * ----------------------------------------------------------------------------------
    * |        Life         |         true         |         *any*        |  Withdrawn |
    * ----------------------------------------------------------------------------------
    * |        Life         |         true         |         *any*        | Incomplete |
    * ----------------------------------------------------------------------------------
    *
    * @param {object} response
    */
    _setDisclaimerNoteState: function (response) {
        var workflowStatus      = ['Application Received', 'In Underwriting'];
        var status              = ['Withdrawn', 'Postponed', 'Declined', 'Incomplete'];
        var productTypeCategory = 'Life';

        var currentProductTypeCategory  = '';
        var currentWorkflowStatus       = '';
        var currentStatus               = '';
        var underwritingRequired        = '';

        if (response.product) {
            currentProductTypeCategory  = response.product.productTypeCategory;
            underwritingRequired        = response.product.underwritingRequired;
        }

        if (response.policyStatus) {
            currentWorkflowStatus = response.policyStatus.workflowStatus;
            currentStatus         = response.policyStatus.status;
        }

        if (currentProductTypeCategory === productTypeCategory
                && underwritingRequired
                && (workflowStatus.indexOf(currentWorkflowStatus) !== -1
                || status.indexOf(currentStatus) !== -1)) {

            response.showDisclaimerNote = true;
        }

        return response;
    },

    /**
    * Set policy owner as Annuitant based current product
    * An annuity is determined by using a combination of the
    * productTypeCategory and the productTypeCode.
    *
    * Table of combinations:
    *
    * =================================================================
    * | productTypeCategory Value | productTypeCode Value | isAnnuity |
    * ============================ ======================= ============
    * |     Annuity               |    *any* !== 'WL'     |  true     |
    * -----------------------------------------------------------------
    * |       Life                |          SPFA         |  true     |
    * -----------------------------------------------------------------
    * |     Annuity               |           WL          |  false    |
    * -----------------------------------------------------------------
    *
    *
    * @param {object} response
    */
    _setAnnuitantState: function (response) {
        var currentProductTypeCategory;
        var currentProductTypeCode;

        if (response.product 
                && response.product.productTypeCategory 
                && response.product.productTypeCode) {

            currentProductTypeCategory = response.product.productTypeCategory.trim().toUpperCase();
            currentProductTypeCode     = response.product.productTypeCode.trim().toUpperCase();

            if ( (currentProductTypeCategory === 'ANNUITY' && currentProductTypeCode !== 'WL') 
                || (currentProductTypeCategory === 'LIFE' && currentProductTypeCode === 'SPFA') ) {
                
                response.isAnnuitant = true;
            }
        }

        return response;
    },

    /**
     * Sets the label text and the value of either issueAge or jointEqualAge.
     * Setting this allows for less conditional checks in the handlebars templates.
     *
     * The service will either return issueAge or jointEqualAge, never both.
     *
     * @param {object} response
     * @returns {object} response with 'coverageAge' object appended to it.
     *
     * Example:
     *
     * response.jointEqualAge = 'P107Y'
     *
     * The age object would be returned as:
     *
     *      response.coverageAge : {
     *          label : 'Joint Equal Age',
     *          value : 'P107Y'
     *      }
     */
    _setCoverageAge : function(response) {

        if (response.issueAge && typeof response.issueAge === 'string') {
            response.coverageAge = {
                label : 'Issue Age',
                value : response.issueAge
            };
        }

        if (response.jointEqualAge && typeof response.jointEqualAge === 'string') {
            response.coverageAge = {
                label : 'Joint Equal Age',
                value : response.jointEqualAge
            };
        }

        return response;
    },

    /**
     * The logic for OSO2.7.67. Determine whether or not
     * to replace "Modal" with either "Single" or <billingMode>.
     *
     * @param response
     * @returns {*}
     */
    _setCoverageHeaderLeadingText : function(response) {
        var headerText = 'Modal';

        if (response.billingDetail && response.billingDetail.currentBilling) {

            // if paymentMethod is "Single Premium",
            // replace "Modal" with "Single"
            if (response.billingDetail.currentBilling.paymentMethod === 'Single Premium') {
                headerText = 'Single';

            } else if (response.billingDetail.currentBilling.paymentMode) {
                // if paymentMethod is not "Single Premium",
                // replace "Modal" with paymentMode
                headerText = response.billingDetail.currentBilling.paymentMode;
            }

        }

        response.coverageHeaderLeadingText = headerText;

        return response;
    },

    /**
     * Set the labels for related policies in the  "Coverage" section
     *
     * @param {object} response response from service
     *
     * @return {object} response
     */
    _setRelatedPoliciesLabels : function (response) {

        var relatedPolicies = response.relatedPolicies;

        if (relatedPolicies) {

            /**
             * OSO2.7.140
             * 1) Replace "Modal" with "Single" if Billing Method = Single Premium.
             * 2) Replace "Modal" with <Billing Mode>, if Billing Method <> Single Premium
             *
             * Clarified that "Modal" should only appear if billingMode is undefined.
             */

            _.each(relatedPolicies, function(policy, index) {
                var policyLabel = 'Modal Premium';

                if (typeof policy.billingMethod === 'string') {

                    if (policy.billingMethod.trim().toLowerCase() === 'single premium') {
                        policyLabel = 'Single Premium';

                    } else {
                        if (typeof policy.billingMode === 'string') {
                            // Per Stacy, display billingMode without changing case
                            policyLabel = policy.billingMode + ' Premium';
                        }
                    }

                }

                // attach the policyLabel to the policy
                relatedPolicies[index].label = policyLabel;
            });
        }

        return response;
    },

    /**
     * set an 'isUlVul' flag at the root of the response. If the product.productTypeCode is UL or
     * VUL, set the flag to true, otherwise to false.
     * 
     * @param response the service response
     * @returns {*} the response, with the added isUlVul flag
     * @private
     */
    _setIsUlVulFlag: function _setIsUlVulFlag(response) {
        var ulVul   = ['UL', 'VUL'];
        var isUlVul = false;

        if (response.product && response.product.productTypeCode) {
            isUlVul = ulVul.indexOf(response.product.productTypeCode) > -1;
        }

        response.isUlVul = isUlVul;

        return response;
    },

    /**
     * Check whether coverage data (Base or Rider) has PUA amount 
     * @param {object} response the service response 
     * @returns {*} the response, with the added hasPUAAmount flag
     * @private
     */
    _setHasPUAAmountFlag: function _setHasPUAAmountFlag (response) {
        var hasPUAAmount = false;
        if(response.coverage) {
            
            if (response.coverage.base) {
                _.each(response.coverage.base, function (base) {
                    if(base.divPUA) {
                        hasPUAAmount = true;
                    }
                });
            }

            if (!hasPUAAmount && response.coverage.riders) {
                _.each(response.coverage.riders, function (rider) {
                    if(rider.divPUA) {
                        hasPUAAmount = true;
                    }
                });
            }

            response.coverage.hasPUAAmount = hasPUAAmount;    
        }
        return response;
    }
    
};

module.exports = policyCoverage;
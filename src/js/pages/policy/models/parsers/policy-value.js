/* global Backbone:false, Marionette:false, module, _:false */
/**
    A model parser for the policy.policyValue object
 */

var policyValue = Marionette.Object.extend({

    setCashValueAmountLabel : function setCashValueAmountLabel(response) {
        var responseCopy = Backbone.$.extend(true, {}, response);

        if (response.policyValue
            && response.policyValue.cashValueAmount
            && response.product
            && response.product.productTypeCode
            && response.product.productName) {

            if (response.product.productName.indexOf('Annuity Care') === 0) {
                responseCopy.policyValue.cashValueAmountLabel = 'Accumulated Value';
            } else if (response.product.productTypeCode === 'VA') {
                responseCopy.policyValue.cashValueAmountLabel = 'Cash Value';
            }

        }

        return responseCopy;
    },

    /**
     * For the policyValue object, the `netSurrenderValueAmount` is displayed for different
     * items depending on the product as shown below:
     *
     * ========================================================================
     * |  Label Name            |                Used for                     |
     * ========================================================================
     * | "Cash Surrender Value" | Annuity Care, Asset Care Life, INXAN, FDIA  |
     * |                        | Legacy Care, and Asset Care Annuities       |
     * ------------------------------------------------------------------------
     * | "Net Cash Value"       | VUL, FA, UL, ISL                            |
     * ------------------------------------------------------------------------
     *
     * If the `policyValue.netSurrenderValueAmount` exists, a new property
     * named "netSurrenderValueAmountLabel" will be appended to the `policyValue` object.
     *
     * @param {*} response
     * @return {*}
     * @private
     */
    setNetSurrenderValueAmountLabel : function setNetSurrenderValueAmountLabel(response) {
        var cashSurrenderProductTypes = ['FDIA', 'INXAN'];
        var netCashProductTypes       = ['FA', 'ISL', 'UL', 'VUL'];
        var responseCopy              = Backbone.$.extend(true, {}, response);

        var label;
        var productName;
        var productTypeCode;

        if (response.policyValue
            && response.policyValue.netSurrenderValueAmount
            && response.product
            && response.product.productTypeCode
            && response.product.productName) {

            productName     = response.product.productName;
            productTypeCode = response.product.productTypeCode;

            if (_.contains(netCashProductTypes, productTypeCode)) {
                label = 'Net Cash Value';

            } else if (_.contains(cashSurrenderProductTypes, productTypeCode)
                || productName.indexOf('Annuity Care') === 0
                || (productName.indexOf('Asset-Care') === 0 && productTypeCode === 'Life')
                || (productName.indexOf('Legacy Care') === 0 && productTypeCode === 'Annuity')) {

                label = 'Cash Surrender Value';
            }

            // Add "netSurrenderValueAmountLabel" to "policyValue" object in copy of response
            responseCopy.policyValue.netSurrenderValueAmountLabel = label;

        }

        return responseCopy;
    }
});

module.exports = policyValue;
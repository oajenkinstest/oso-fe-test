/* global Backbone :false, _:false*/
/**
 Policy model data parser functions for Policy identification section
 */

var policyIdentificationSection = {

    /**
     * This function sets the name and label used in the Policy Identification section of the
     * policy detail page. The label and role displayed in this section are determined by
     * the customerRoles/customers returned by the service.
     *
     *
     * ==============================================================================
     * |  Type of Policy / Role |  Label Value  |    Name for Display               |
     * ========================= =============== ====================================
     * | AWD                    | Name:         | Use customers['0']                |
     * ------------------------------------------------------------------------------
     * | (if "Annuitant" role)  | Annuitant:    | Only one 'Annuitant'              |
     * ------------------------------------------------------------------------------
     * | (if "Primary Insured"  | Insured:      | Only one of either                |
     * |  or "Insured role AND  |               | 'Primary Insured' (if it exists)  |
     * |  "Annuitant" role does |               | or 'Insured'.                     |
     * |  NOT exist)            |               |                                   |
     * ------------------------------------------------------------------------------
     *
     * Currently, there is no business rule to use in order to choose which name to display for
     * a given role, so we're just returning the first customerId for that role returned by the
     * service.
     *
     * This method appends an object to the root of the response object. This object looks as
     * follows:
     *
     *          policyIdentification = {
     *               label : 'Insured',
     *               name  : 'Wile E. Coyote'
     *           }
     *
     * @param {*} response
     * @returns {*}
     * @private
     */
    _setPolicyIdentificationLabelAndName : function _setPolicyIdentificationLabelAndName(response) {

        var id = {
            label : 'Name',
            name  : ''
        };

        var customers = response.customers;
        var roles     = response.customerRoles;

        if (response.policyId) {

            if (_.has(roles, 'Annuitant')) {
                id.label = 'Annuitant';
                id.name  = customers[roles.Annuitant[0].customerId].fullName;

            } else if (_.has(roles, 'Primary Insured')) {
                id.label = 'Insured';
                id.name  = customers[roles['Primary Insured'][0].customerId].fullName;

            } else if (_.has(roles, 'Insured')) {
                id.label = 'Insured';
                id.name  = customers[roles.Insured[0].customerId].fullName;
            }

        } else {
            id.label = 'Name';

            if (customers && _.isObject(customers['0'])) {
                id.name = customers['0'].fullName;
            }
        }

        response.policyIdentification = id;

        return response;
    },

    /**
     * Build the mailto for the caseManager and Policy service Contact
     *
     * @param response the response from the REST call
     * @returns {object} - the response, with an additional 'mailto' property 
     * under caseManager/policyServiceContact if the email exists for corresponding object. 
     * Additionally, it will add the policy number and customer's
     * last name in the subject of the mailto if those properties are available.
     * @private
     */
    _setContactMailto: function _setContactMailto(response) {
        var address      = '';
        var custId       = '';
        var lastName     = '';
        var policyNumber = '';
        var subject      = '';

        if (response && (response.caseManager && response.caseManager.emailAddress) 
            || (response.policyServiceContact && response.policyServiceContact.emailAddress)) {

            if (response.policyNumber) {
                policyNumber = response.policyNumber;
            }

            if (response.customerRoles) {

                // figure out which role we can use
                if (response.customerRoles.Insured && response.customerRoles.Insured.length >= 1) {
                    custId = response.customerRoles.Insured[0].customerId;

                } else if (response.customerRoles['Primary Insured'] &&
                    response.customerRoles['Primary Insured'].length >= 1) {
                    custId = response.customerRoles['Primary Insured'][0].customerId;

                } else if (response.customerRoles.Annuitant &&
                    response.customerRoles.Annuitant.length >= 1) {
                    custId = response.customerRoles.Annuitant[0].customerId;
                }

                // Now find the proper customer record and get the lastName
                if (response.customers && response.customers[custId]) {
                    lastName = response.customers[custId].lastName;
                }
            }

            if (policyNumber || lastName) {
                if (policyNumber && lastName) {
                    subject = 'Policy #' + policyNumber + ' ' + lastName.toUpperCase();
                } else if (policyNumber) {
                    subject = 'Policy #' + policyNumber;
                } else if (lastName) {
                    subject = lastName.toUpperCase();
                }
                subject = encodeURIComponent(subject);
            }
        
            if (response.caseManager) {
                address = response.caseManager.emailAddress;
                response.caseManager.mailto = 'mailto:' + address + '?subject=' + subject;
            }
            else if (response.policyServiceContact) {
                address = response.policyServiceContact.emailAddress;
                response.policyServiceContact.mailto = 'mailto:' + address + '?subject=' + subject;
            }
        }

        return response;
    },

    /**
     * Set whether current policy is in Inactive status.
     *
     * A new flags will be added based on policy Status
     *
     *      isAbandoned
     * 
     * @param {object} response response received from service
     *
     * @return {object} updated response object with new flags 
     */
    _setAbandonedPolicyFlag: function _setAbandonedPolicyFlag(response) {

        var inactivePolicyStatus = [
            'Declined', 
            'Postponed', 
            'Withdrawn', 
            'Incomplete'
        ];

        if (response.policyStatus) {
            
            if (_.contains(inactivePolicyStatus, response.policyStatus.status)) {
                response.isAbandoned = true;
            }
        }

        return response;
    },

    /**
     * Set whether current policy is in active status.
     *
     * A new flags will be added based on policy Status
     *
     *      isActive
     * 
     * @param {object} response response received from service
     *
     * @return {object} updated response object with new flags 
     */
    _setActivePolicyFlag: function _setActivePolicyFlag(response) {
        if (response.policyStatus && response.policyStatus.acordHoldingStatus === 'Active') {
            response.isActive = true;
        }
        return response;
    },

    /**
     * Set policyStatus.show properties if acordHoldingStatus is 'Dormant' or'Unknown'
     * 
     * @param {object} response response received from service
     * @return {object} updated response object with new and updated property.
     */
    _setShowflag : function _setShowflag(response) {
        if (_.isObject(response.policyStatus)) {
            response.policyStatus.show = true;
            if (_.contains(['Dormant','Unknown'], response.policyStatus.acordHoldingStatus)) {
                response.policyStatus.show = false;
            }
        }
        return response;
    },

    /**
     * Add "show" flags to servicingProducer and writingProducers objects
     * 
     * @param {Object} response service response
     * @param {Object} activePolicyPaidStatus Paid status of active policy
     * 
     * if policy is in active state and paid date is after/within 30 days
     */
    _setShowAgentsFlag : function _setShowAgentsFlag (response, activePolicyPaidStatus) {
        if (response.writingProducers) {
            response.showWritingProducers = true;
        }
        if (response.servicingProducer) {
            response.showServicingProducer = false;
        }

        if (activePolicyPaidStatus && activePolicyPaidStatus.after30Days) {
            if (response.servicingProducer) {
                response.showServicingProducer = true;
            }

            if (response.writingProducers) {
                response.showWritingProducers = false;
            }
        }

        return response;
    },

    /**
     * Sort Writing Producers in descending order based on splitPercent field
     * 
     * @param {object} response response received from service
     *
     * @return {object} updated response object with sorted list of writingProducers 
     */
    _sortWritingProducers : function _sortWritingProducers (response) {
        if (response.writingProducers && response.writingProducers.length > 1) {
            response.writingProducers =  _.sortBy(
                response.writingProducers, 'splitPercent'
            ).reverse();
        }

        return response;
    },

    /**
     * Set alert message to display on policy detail message
     * @param {object} response response received from service
     *
     * @return {object} updated response object with selected alert message 
     */
    _setAlertAttentionMessage : function _setAlertAttentionMessage (response) {
        var responseCopy  = Backbone.$.extend(true, {}, response);
        this.alertAttentionMessages = {
            activeScopeSLRPPolicy : {
                id      : 'under-construction-alert-message',
                title   : 'No additional information is available online for this policy.',
                message : 'SPIAs are issued on a system that currently does not interface '+
                            'with OSO. More policy details will be provided at a later time.'
            },
            underConstruction : {
                id      : 'under-construction-alert-message',
                title   : 'NOTICE:',
                message : 'This policy detail page is currently being built by section. '+
                            'More policy detail sections will be released each month.'
            },
            terminated : {
                id      : 'terminated-alert-message',
                title   : 'This policy is terminated.',
                message : 'Information displayed is as of the termination date. '+
                            'Some sections of policy detail are unavailable.'
            },
            suspended : {
                id      : 'suspended-alert-message',
                title   : 'This policy is being updated.',
                message : 'Limited information is available at this time. '+
                            'Some sections of policy detail are unavailable.'
            },
            maintenance  : {
                id      : 'maintenance-alert-message',
                title   : 'This policy is being updated.',
                message : 'Limited information is available at this time. '+
                            'Some sections of policy detail are unavailable.'
            },
            maintenanceAndDeathClaimPending  : {
                id      : 'maintenance-death-claim-pending-alert-message',
                title   : 'This policy is being updated.',
                message : 'Limited information is available at this time. '+
                            'Some sections of policy detail are unavailable. '+
                            'A claim is also pending for this policy. Beneficiary status '+
                            'is subject to confirmation by the insurance company.'
            }
        };

        if (responseCopy.policyStatus) {
            if (responseCopy.policyStatus.onlySummaryDataAvailable) {
                responseCopy.alertAttentionMessage = 
                    this.alertAttentionMessages.activeScopeSLRPPolicy;
            } else if (responseCopy.policyStatus.statusView === 'Active') {
                responseCopy.alertAttentionMessage = this.alertAttentionMessages.underConstruction;
            } else if (responseCopy.policyStatus.acordHoldingStatus ==='Inactive') {
                responseCopy.alertAttentionMessage = this.alertAttentionMessages.terminated;
            } else if (responseCopy.policyStatus.acordHoldingStatus ==='Dormant') {
                responseCopy.alertAttentionMessage = this.alertAttentionMessages.suspended;
            } else if(responseCopy.policyStatus.inMaintenance 
                    && responseCopy.policyStatus.deathClaimPending) {
                responseCopy.alertAttentionMessage 
                        = this.alertAttentionMessages.maintenanceAndDeathClaimPending;
            } else if (responseCopy.policyStatus.inMaintenance) {
                responseCopy.alertAttentionMessage = this.alertAttentionMessages.maintenance;
            }
        }

        return responseCopy;
    }
    
};

module.exports = policyIdentificationSection;
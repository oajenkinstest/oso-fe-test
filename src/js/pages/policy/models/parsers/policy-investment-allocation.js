/* global Marionette */
/**
 A model parser class which parses the data for Policy Ivestment allocation

 */


var policyInvestmentAllocation = Marionette.Object.extend({

    /**
     * Set whether or not the policy is an indexed annuity 
     * (identified by product.productTypeCode === 'INXAN').
     * @param response
     * @private
     */
    _setIndexedAnnuityState : function(response) {
        if (response.product 
                && response.product.productTypeCode 
                && response.product.productTypeCode.trim().toUpperCase() === 'INXAN') {
            
            response.isIndexedAnnuity = true;
        }
        
        return response;
    }
});

module.exports = policyInvestmentAllocation;
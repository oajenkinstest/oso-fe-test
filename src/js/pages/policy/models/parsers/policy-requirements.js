/* global _:false */
/**
 Policy model data parser functions for Requirements section
*/

var utils = require('../../../../utils/utils');

var policyRequirements = {

    /**
     * Boolean function to see if there are pending requirements
     * @param {object} requirements
     * @returns {boolean}
     * @private
     */
    _hasPendingRequirements : function _hasPendingRequirements (requirements) {
        if (requirements) {
            
            for (var key in requirements) {
                if (requirements.hasOwnProperty(key)) {
                    for (var i=0; i<requirements[key].length; i++) {
                        if (requirements[key][i].status 
                            && typeof requirements[key][i].status === 'string'
                            && requirements[key][i].status.trim().toUpperCase()
                                === 'PENDING') {
                            return true;
                        }
                    }
                }
            }

        }

        return false;
    },

    /**
     * Determine whether or not the Requirements section should be displayed
     * 
     * @param {object} response
     * @returns boolean
     */
    shouldShowRequirements : function shouldShowRequirements (response) {
        // Always show the section for policies that have never been 'Active'
        if (response.requirements && response.policyStatus 
                && response.policyStatus.acordHoldingStatus === 'Proposed') {
            return true;
        }

        // For non-pending policies, show if there are pending requirements
        return this._hasPendingRequirements(response.requirements);
    },

    /**
     * Determine whether or not the Received Requirements subsection should be displayed
     * 
     * @param {object} response
     * @returns boolean
     */
    shouldShowReceivedRequirements : function shouldShowReceivedRequirements (response) {
        // Always show received requirements for policies that have never been 'Active'
        if (response.requirements && response.policyStatus 
            && response.policyStatus.acordHoldingStatus === 'Proposed') {
            return true;
        }

        if (response.application && response.application.paid) {
            var daysSincePaid = utils.dateDiff(response.application.paid);
            
            // Don't show if the paid date is more than 92 days ago or there are no
            // pending requirements
            if (daysSincePaid > 92 || !this._hasPendingRequirements(response.requirements)) {
                return false;
            }
        }

        return true;
    },

    /**
     * Deletes the received requirements from the policy data
     * 
     * @param {object} response
     * @returns {object} response with the received requirements removed
     */
    deleteReceivedRequirements : function deleteReceivedRequirements (response) {
        if (response.requirements) {
            delete response.requirements.received;
        }
        return response;
    },

    /**
     * Set customer first name (insuredName) against customer Id
     * for requirements list
     * 
     * @param  {Object} response response received from service
     * @return {object} response
     */
    setCustomerFirstNamePolicyRequirements :
        function setCustomerFirstNamePolicyRequirements (response) {
            if (response.requirements && utils.isImplemented(response.requirements)) {
            
                //iterate each requirements
                _.each(response.requirements, function (requirements) {
                    if (requirements) {
                        _.each(requirements, function (requirement) {                      
                            
                            // Datatable require a field to render data
                            // otherwise dt throw error
                            requirement.insuredName = '';
                            if (requirement.customerId 
                                    && response.customers
                                    && response.customers[requirement.customerId]) {

                                requirement.insuredName = 
                                    response.customers[requirement.customerId].fullName;
                            } 
                        });
                    }
                });
            }

            return response;
        },

    /**
     * Get a count of all pending and non-pending 
     * requirements for a policy and append this
     * structure to the response. 
     * 
     * @param {object} response
     * @returns {object} response
     */
    setRequirementsCounts : function setRequirementsCounts (response) {
        var pendingCount      = 0;
        var nonPendingCount   = 0;

        if (response.requirements && utils.isImplemented(response.requirements)) {
            
            _.each(_.keys(response.requirements), function(reqType) {
                
                _.each(response.requirements[reqType], function(req) {
                    
                    if (req.status 
                            && typeof req.status === 'string'
                            && req.status.trim().toUpperCase() === 'PENDING') {
                        pendingCount++;
                    } else {
                        nonPendingCount++;
                    }
                    
                });
            });
            
            response.requirements.pendingCount    = pendingCount;
            response.requirements.nonPendingCount = nonPendingCount;
        }
        
        return response;
    },

    /**
     * A Method to sort 'received' list property 
     * which is coming under 'requirements' object of response.
     * Finally it will sort requirements list based on 'key'. 
     * Both are uses custom sort order
     * 
     * @param  {Object} response response received from service
     * @return {object} response
     */
    sortPolicyRequirements : function sortPolicyRequirements (response) {

        //order to sort Requirement based on 'key'
        var requirementKeyOrder = [ 
            'neededForUnderwriting',
            'obtainAtDelivery', 
            'received'
        ];

        //Received list sorting
        //default sort order for 'Status' column of 'received' property
        var statusSortOrder = [
            'In House',
            'Complete',
            'Waived'
        ];

        // Bail out if response.requirements is not implemented
        if (response.requirements && !utils.isImplemented(response.requirements)) {
            return response;
        }

        var receivedList  = response.requirements && response.requirements.received;

        if (receivedList) {

            receivedList = _.chain(receivedList)

                .sortBy('requirementName')
                .reverse()

                //sort by date received
                .sortBy(function(list){ 
                    var dateReceived = new Date(list.dateReceived);
                    if (dateReceived) {
                        return dateReceived.getTime();
                    } 
                    return 0;
                })

                //descending
                .reverse()

                // sort by status order 
                .sortBy(function(list){ 
                    return _.indexOf(statusSortOrder, list.status); 
                })

                .value();

            response.requirements.received = receivedList;

        }

        //Requirement list sorting
        response.requirements = _.object(
            _.sortBy(
                _.pairs(response.requirements),
                function(value){
                    return _.indexOf(requirementKeyOrder,value[0]);
                }
            )
        );

        return response;
    }
};

module.exports = policyRequirements;
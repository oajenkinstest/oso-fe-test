/* global Backbone:false, _:false */
/**
 * Policy Search View.
 *
 * Created by jbell on 2/4/16.
 */
var debugModule             = require('../../../modules/debug/debugModule').init();
var errorHelper             = require('../../../utils/error-helper');
var PolicySearchViewModel   = require('../viewModels/policy-search-form-vm');
var template                = require('../templates/policy-search-form-t.hbs');

// Need to include this, otherwise unit tests blow up.
require('../../../utils/hb-helpers');

// load the global partials.
require('../../../partials');

var analyticsChannel    = Backbone.Radio.channel('analytics');
var userChannel         = Backbone.Radio.channel('user');

var PolicySearchFormView = Backbone.Marionette.ItemView.extend({

    template: template,

    ui: {
        tooltipElements : '[data-toggle="tooltip"]',
        searchButton    : '#pending-search-button',
        searchForm      : '#pending-search-form',
        searchTerm      : '#pending-search-term-input',
        searchOption    : '[name="search-option"]'
    },

    events: {
        'submit @ui.searchForm' : '_search'
    },

    /**
     * Initialize the view. This function will create a model for the view if one was not
     * included in the options object.
     * 
     * @param options
     */
    initialize: function(options) {

        var canSearchPolicyByProducer = userChannel.request('hasCapability', 
                'Policy_Search_by_Producer');
        var canViewAsProducer         = userChannel.request('hasCapability', 
                'HO_ViewAsProducer');

        // Create and set a new model if there isn't one passed in
        if (!options.model) {
            this.model = new PolicySearchViewModel();
        }

        // searchType isn't set, and can search by Producer, so default to 'policyNumber'
        if (! this.model.get('searchType') && canSearchPolicyByProducer) {
            this.model.set({
                searchType : 'policyNumber'
            });
        }

        this.model.set({
            hasPolicySearchByProducer : canSearchPolicyByProducer,
            hasHOViewAsProducer       : canViewAsProducer
        });
    },

    /**
     * When rendering, bind the popover handler to the popover ui elements.
     */
    onRender: function() {

        // Binding tooltip event to all tooltip elements
        // Trigger even 'hover' will handle displaying it based on device
        // In Desktop browser it will display on 'mouse over' event and on mobile browser
        // Tooltip will be displayed with 'click' event 
        this.ui.tooltipElements.tooltip({
            trigger:  'hover',
            placement:'auto', 
            container: this.$el 
        });
    },


    /**
     * Show a server message
     * @param message The text of the message
     * @param messageType The type of the message - 'process' or 'server'
     */
    showServerMessage: function(message, messageType) {
        var messageTemplate = _.template(message);
        var messageText     = messageTemplate({ searchTerm: this.model.get('searchTerm') });

        this.model.set('alertMessage', errorHelper.createAlert(messageText, messageType));
        this.render();
        this.ui.searchButton.focus();
    },
    
    /* 'Private' functions */

    /**
     * Validate the entire form and then perform the search if there are no errors.
     *
     * @param event The event that initiated the call to this function.
     * @private
     */
    _search: function(event) {

        // Don't want to just go submitting all willy-nilly.
        if (event) {
            event.preventDefault();
        }

        // Update the model and validate
        // error messages are available via this.model.validation
        this._updateModel();
        var isValid = this.model.isValid();

        if (isValid) {

            // Track form submission through Enter key
            if (event && event.currentTarget 
                    && event.currentTarget.tagName 
                    && event.currentTarget.tagName.toLowerCase() === 'form') {
                analyticsChannel.trigger('trackAction', {
                    event : event
                });
            }

            // Input OK, trigger appropriate search
            switch(this.model.get('searchType')) {

                case 'clientName'   :
                    this.trigger('clientNameSearch', this.model.get('searchTerm'));
                    break;

                case 'policyNumber' :
                    this.trigger('policyNumberSearch', this.model.get('searchTerm'));
                    break;

                case 'producerName' :
                    this.trigger('producerNameSearch', this.model.get('searchTerm'));
                    break;
                case 'producerNumber' :
                    this.trigger('producerNumberSearch', this.model.get('searchTerm'));
                    break;

                default :
                    // somehow passed validation, but didn't have a searchType. Error.
                    debugModule.error('Unknown searchType "' + this.model.get('searchType') + '"');
            }
        }

        // render to show/hide errors
        this.render();

        // set focus to search button after render (helps user with tab ordering)
        this.ui.searchButton.focus();
    },


    /**
     * Update the model object, based on the UI
     *
     * @private
     */
    _updateModel: function() {
        var searchOption = this.ui.searchOption.filter(':checked').val();
        var searchType   = null;
        var searchTerm   = this.ui.searchTerm.val();

        // if type is required, get the selection 
        // (only for the user has 'Policy_Search_by_Producer' capability)
        if (this.model.get('hasPolicySearchByProducer') && searchOption === 'producerName') {
            searchType = 'producerName';
        } else if (searchOption === 'producerNumber') {
            searchType = searchOption;
        }

        // update the model with the proper form values, and clear any alert message
        this.model.set({
            alertMessage : null,
            searchOption : searchOption,
            searchTerm   : searchTerm,
            searchType   : searchType
        });
    }

});

module.exports = PolicySearchFormView;

/* global Backbone: false, Marionette:false, _:false */

/**
 * Display result list for a Producer Name and Number Search.
 *
 * This view relies almost entirely on jQuery DataTables, which uses ajax calls for the initial
 * population of the data, as well as for sorting and paging.
 *
 * Created by rkonecky on 9/30/16.
 */

var config               = require('../../../config/config');
var debugModule          = require('../../../modules/debug/debugModule').init();
var template             = require('../templates/producer-search-results-t.hbs');
var userChannel          = Backbone.Radio.channel('user');
var utils                = require('../../../utils/utils');
var spinnerChannel       = Backbone.Radio.channel('spinner');
var _this;

// Need to include this, otherwise unit tests blow up.
require('../../../utils/hb-helpers');

// load the global partials.
require('../../../partials');

var ProducerSearchResultsView = Marionette.ItemView.extend({

    pageOptions: ['searchTerm', 'searchType'],

    template: template,

    ui: {
        producerResultsWrapper     : '#producerSearchResults',
        dataTable                  : '#producerSearchResultsTable',
        responsiveTableInstruction : '.table-responsive-instruction',
        linkViewAsProducer         : '.link-view-as-producer',
        tableLegend                : '#producer-results-table-legend'
    },

    events: {
        'click @ui.linkViewAsProducer' : '_startImpersonation'
    },

    errors: {
        searchTypeMissing : 'Search type is missing to perform producer search'
    },

    initialize: function (options) {

        this.mergeOptions(options, this.pageOptions);

        // SearchType is mandatory option to decide type of search
        if (!this.searchType && !(typeof this.searchType === 'string')) {
            throw new Error(this.errors.searchTypeMissing);
        }

    },

    onRender: function () {

        var startIndex = 0;
        var pageLength = 25;
        var dataTable;
        var dataTableOptions;

        //shouldn't perform search if search term undefined
        if (!this.searchTerm) {
            return false;
        }

        _this = this;

        spinnerChannel.trigger('show', {
            viewScope   : this,
            position    : 'fixed'
        });

        // Use the available options for datatables setup
        if (this.options) {
            if (this.options.start) {
                startIndex = Number(this.options.start);
            }
            if (this.options.length) {
                pageLength = Number(this.options.length);
            }
        }
        
        var columnDefs = [
            { data: 'lexicalName', name: 'lexicalName' },
            { data: 'id', name: 'id', render: this.renderProducerActions },
            { data: 'roles', name: 'roles', render: utils.getActiveProducerRoleCodesAsString },
            { data: 'ico[, ]', name: 'ico' },
            { data: 'email[, ]', name: 'email', className : 'wordbreak', width: '25%' }
        ];

        // In order to prevent "null" from being shown in IE/Edge, an
        // empty string will be set as the defaultContent for each column.
        utils.setDatatablesDefaultContent(columnDefs, '');

        this.url = '';
        var dataSource ='data';
        if (this.searchType === 'name') {
            this.url = config.apiUrlRoot + 'producers?producerName=' +
                encodeURIComponent(this.searchTerm);
        } else if (this.searchType === 'number') {
            this.url = config.apiUrlRoot + 'producers?roleCode=' +
                encodeURIComponent(this.searchTerm);
            dataSource = '';
        }

        dataTableOptions =  utils.getDatatableOptions(this, this.url);

        Backbone.$.extend(true, dataTableOptions, {

            ajax : {
                dataSrc  : dataSource
            },

            autoWidth: false,

            // TODO: Remove this line and add sorting when OOSO-2932 is complete
            bSort : false,

            // column definitions. Length MUST match number of columns in template
            columns : columnDefs,

            // show table only for now
            dom : 't',

            paging : false,
            info   : false,
        });

        // Enabling pagination for ProducerName search
        // 
        // Based on SVC story # OOSO-2932 there will be sorting alone for producer name search.
        // 'order' property can be added once OOSO-2932 is ready.
        if (this.searchType === 'name') {
            _.extend(dataTableOptions, {
                paging : true,
                info : true,

                // show info, length control, pagination, table, info, length, pagination
                dom : 'ilptilp',
                language : {
                    info : 'Showing _START_ to _END_ of _TOTAL_ entries for <strong>"' +
                    this.searchTerm + '"</strong>',

                    lengthMenu : 'Display _MENU_ records'
                },

                // 0-based start index
                displayStart : startIndex,

                // initial page length
                pageLength : pageLength
            });
        }

        dataTable = this.ui.dataTable.DataTable(dataTableOptions)
        .on('xhr', function($event, dataTableSettings, jsonResponse, xhr) {

            spinnerChannel.trigger('hide', _this);

            // Make sure we're getting the info back that we should expect
            if (!jsonResponse || !xhr) {
                _this.trigger('error');
                return;
            }

            // Handle any non-200 HTTP responses
            // after the readyState has been set to 4 (DONE)
            if (xhr.readyState === 4 && xhr.status !== 200) {
                _this.trigger('error');
                return;
            }

            // Name search have sorting and pagination
            if (_this.searchType === 'name') {
            
                // Checking whether response have property 'recordsTotal'
                if (_.isUndefined(jsonResponse.recordsTotal)) {
                    _this.trigger('error');
                    return;
                }

                // Show the table legend
                _this.ui.tableLegend.removeClass('hidden');

                // Handle no results being returned
                if (jsonResponse.data && jsonResponse.data.length === 0) {
                    _this.trigger('noResults');
                    return;
                }
            } else if (_this.searchType === 'number') {

                if (jsonResponse.length === 0) {
                    _this.trigger('noResults');
                    return;
                }

                // Hide the legend for this search
                _this.ui.tableLegend.addClass('hidden');
            }
                
            // display the results
            _this.ui.producerResultsWrapper.removeClass('hidden');

            // scroll to results section
            utils.scrollTo(_this.ui.producerResultsWrapper);
            
        });

        utils.bindDataTableEvents({
            dtObject            : dataTable, 
            viewScope           : this, 
            viewName            : 'producer-search-results-v: ', 
            spinnerChannel      : spinnerChannel,
            debugModule         : debugModule
        });

        utils.formatDataTable(this.$el.find('.dataTables_wrapper'));
    },

    renderProducerActions : function renderProducerActions(producerId, type, row, meta) {
        var actionsTemplate;
        var pendingViewCount;
        var link;
        if (_this.searchType === 'name') {

            // If the pendingViewCount is 0, don't display the link
            if (row.pendingViewCount === 0) {
                return;
            }

             // Its to add query param 'targetuser' if impersonated webID exist
            link = utils.addTargetUserQueryParamToURL('#policies?producerId=<%= id %>');

            pendingViewCount = row.pendingViewCount;

            actionsTemplate = _.template(
                '<a href="'+link+'">View&nbsp;Pending&nbsp;List</a>&nbsp;' +
                '<strong class="oa-lake">(<%=pendingViewCount %>)</strong>'
            );
        } else if (_this.searchType === 'number') {
            actionsTemplate = _.template(
                '<a class="link-view-as-producer" '+'href="#" '+
                'data-webid="<%=webId %>" '+
                'data-name="<%=fullName %>">'+
                'View as Producer</a>'
            );
        }
        return actionsTemplate( { 
            id                : producerId,
            webId             : row.webId,
            fullName          : row.fullName,
            pendingViewCount  : pendingViewCount
        });
    },

    /**
     * Start impersonate session
     * @return {object} event event object
     */
    _startImpersonation : function _startImpersonation (event) {
        event.preventDefault();
        
        var anchor = Backbone.$(event.currentTarget);
        var webId  = anchor.data('webid');

        if (webId) {

            spinnerChannel.trigger('show', {
                viewScope : this,
                position  : 'fixed'
            });

            userChannel.trigger(
                'viewAsProducer',
                {
                    webId    : webId,
                    fullName : anchor.data('name')
                }
            );
        } else {
            debugModule.warn('producer-search-results-v: webId missing for a producer');
        }
    }
});

module.exports = ProducerSearchResultsView;

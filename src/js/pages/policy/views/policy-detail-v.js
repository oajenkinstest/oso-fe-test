/* global require:false, Backbone:false, $:false, _:false */
/**
 * Pending Policy Detail View.
 */

 //load all partials and helpers
require('../partials');

// load the global partials.
require('../../../partials');
require('../../../utils/hb-helpers');

var errorHelper              = require('../../../utils/error-helper');
var PolicyDetailModel        = require('../models/policy-detail-m');
var template                 = require('../templates/policy-detail-t.hbs');
var utils                    = require('../../../utils/utils');

var analyticsChannel         = Backbone.Radio.channel('analytics');
var errorChannel             = Backbone.Radio.channel('error');
var spinnerChannel           = Backbone.Radio.channel('spinner');

// Need to include this, otherwise unit tests blow up.
require('../../../utils/hb-helpers');

var PolicyDetailView = Backbone.Marionette.ItemView.extend({

    template: template,
    initialize: function initialize (options) {

        var caseId;
        var policyId;

        if (options && options.stateObj && options.stateObj.policyId) {
            policyId = options.stateObj.policyId;
        }

        if (options && options.stateObj && options.stateObj.caseId) {
            caseId = options.stateObj.caseId;
        }

        // Add gtmTrackViewEvent so that Google Tag Manager
        // will hold off on tracking the new view until this event.
        this.gtmTrackViewEvent = 'dataFetched';

        // This code is for a future optimization that makes this view more easily
        // testable by allowing an existing model to be passed in as an option
        // instead of the view initializer always fetching data and creating a new
        // model object. --RKC 09/11/2017
        // if (options.model) {
        //     // the model was provided, but needs the id property
        //     this.model.set('id', policyId);
        //     return true;
        // }

        if (!options.model && (policyId || caseId)) {
            
            // if both policyId and caseId are present, model will fetch with policyId
            this.model = new PolicyDetailModel({
                id: policyId,
                caseId: caseId
            });
        } else {
            
            //show error message as policy number or case Id is missing
            errorChannel.trigger('showErrorPage', this.errors.policyNumberMissing);

            //destroying view to prevent calling render method
            //an show the error page which triggered above
            this.destroy();

            return false;
        }
    },

    behaviors : {
        jumpLinks           : {},
        smartScrollTables   : {}
    },

    ui : {
        policyStatusHelpTextIcon               : '#policy-status-help-text-icon',
        policyStatusHelpTextModal              : '#policy-status-help-text-modal',

        //policy requirement tables
        neededForUnderwritingRequirementsTable : 'table#neededForUnderwriting',
        obtainAtDeliveryRequirementsTable      : 'table#obtainAtDelivery',
        receivedRequirementsTable              : 'table#received'
    },

    events : {
        'click @ui.policyStatusHelpTextIcon' : 'showHelpTextModal'
    },

    errors : {
        policyNotFoundMessage : 'The policy you requested was not found.',
        policyNumberMissing   : 'No Policy Number Specified',
        serverError           : 'An error occurred while processing this request.'
    },

    onBeforeShow : function onBeforeShow () {
        var _this = this;
        this.listenTo(this.model, 'sync', this.render);

        this.listenTo(this.model, 'error', this._showAlertMessage);

        //show wait indicator
        spinnerChannel.trigger('show', {
            viewScope : this,
            position  : 'fixed'
        });

        this.model.fetch({

            //to hide wait indicator
            success : function () {
                spinnerChannel.trigger('hide', _this);

                _this._prepPolicyHighlightsConfig(_this.model);

                // set flag to display underwriter and files under applcation status section
                // This cannot be moved to application parsers 
                // because model object will be created only after parse
                _this.model.setUnderwriterAndFilesAvailabilityFlag();
            }
        });
    },

    _showAlertMessage : function _showAlertMessage (model, response) {
        var errorMessage;
        spinnerChannel.trigger('hide', this);

        if (response) {

            if (response.status === 404) {
                this.model.set(
                    'alertMessage',
                    errorHelper.createAlert(this.errors.policyNotFoundMessage, 'info')
                );
                this.render();
                
            } else if (response.status === 500 || response.status === 400) {
                this.model.set(
                    'alertMessage',
                    errorHelper.createAlert(this.errors.serverError, 'warning')
                );
                this.render();
            }

            errorMessage = JSON.stringify(_.pick(response, [
                'status', 
                'statusText', 
                'url',
                'responseText'
            ]));

            // track server exception error
            analyticsChannel.trigger('trackException', {
                message : errorMessage,
                fatal : false
            });
        }
    },

    onRender : function onRender () {
        //bind DataTable to each Tables under Policy Requirements section
        this._bindRequirementsTablesWithDataTable();

        // If the subpage is set, scroll to the section
        if (this.options && this.options.stateObj && this.options.stateObj.subpages) {
            utils.scrollTo( $('#' + this.options.stateObj.subpages[0]) );
        }

        // add policy-related data for analytics
        this._setAnalyticsData();
    },

    onDestroy : function onDestroy () {
        if (this.dataLayerValues && _.isObject(this.dataLayerValues)) {
            analyticsChannel.trigger('unsetDataLayerValue', this.dataLayerValues);
        }
    },

    /**
     * Show Help text modal
     * to display help information from WCM
     * @param {Object} e jQuery event object
     */
    showHelpTextModal : function showHelpTextModal (e) {
        e.preventDefault();

        var wcmHelpPath = '/wps/wcm/connect/indcontent/OSO/misc/policy-status-help';

        // Load the wcm content in the modal-content area
        this.ui.policyStatusHelpTextModal.modal('show').find('.modal-content').load(wcmHelpPath);
    },

    /**
     * Bind DataTable to each the each table of Policy Requirements 
     * Section
     * 
     */
    _bindRequirementsTablesWithDataTable : function _bindRequirementsTablesWithDataTable() {

        //Policy requirements table binding with DataTable
        var _this = this;

        var dataTablesOptionsDefault = {

            //t stand for only contain table. no wrapper elements
            //https://datatables.net/reference/option/dom
            dom: 't', 
            searching: false,
            paging: false,
            info : false,
            columns : [
                {
                    title: 'Insured', 
                    data: 'insuredName', 
                    className: 'insured-head', 
                    orderable: false
                },
                {
                    title: 'Requirement', 
                    data: 'requirementName', 
                    className: 'requirement-head',
                    orderable: false
                },
                {
                    title: 'Status', 
                    data: 'status' , 
                    className: 'status-head',
                    orderable: false
                },
                {
                    title: 'Date Created', 
                    data: 'dateCreated', 
                    className: 'ordered-head',
                    render: utils.dataTableFormatDate
                },
                {
                    title: 'Comments', 
                    data: 'comments',
                    orderable: false
                }
            ],
            order:[

                //default sorting Date Created
                [3, 'desc'] 
            ]
        };

        //Needed to Pay Comp column to be added for Obtain At Delivery table
        var needToPayColumn  = {
            title: 'Needed to Pay Comp', 
            data: 'neededToPay', 
            className: 'need-head',
            orderable: false
        };

        //Date Received column to be added for Received table
        var dateReceivedColumn  = {
            title: 'Date Received', 
            data: 'dateReceived', 
            className: 'received-head',
            render: utils.dataTableFormatDate
        };

        var dataTablesOptions;
        var policyIdentificationLabel = 'Insured';
        var requirementsData;
       
        if (this.model.get('requirements')) {

            requirementsData  = this.model.get('requirements');

            // set the "insuredName" column label to match Policy Identification header
            if (this.model.get('policyIdentification')) {
                policyIdentificationLabel = this.model.get('policyIdentification').label;
            }

            // Table : Need for underwriting 
            if (requirementsData.neededForUnderwriting) {
                dataTablesOptions = Backbone.$.extend(true,{},dataTablesOptionsDefault);
                dataTablesOptions.columns[0].title = policyIdentificationLabel;
                this._bindEventsAndLoadData(
                    this.ui.neededForUnderwritingRequirementsTable.DataTable(dataTablesOptions),
                    requirementsData.neededForUnderwriting);
            }
            
            //Table : May Obtain at Delivery 
            if (requirementsData.obtainAtDelivery) {
                dataTablesOptions = Backbone.$.extend(true,{},dataTablesOptionsDefault);

                //add column 'Needed to Pay Comp' at index 4
                dataTablesOptions.columns.splice(4, 0, needToPayColumn);

                //to highlight with green color 'Needed to Pay Comp' 
                //cell if value is 'Yes'
                dataTablesOptions.columnDefs = [{
                    targets: 4,
                    createdCell: _this._setNeedToPayCellContent
                }];

                dataTablesOptions.columns[0].title = policyIdentificationLabel;
                this._bindEventsAndLoadData(
                    this.ui.obtainAtDeliveryRequirementsTable.DataTable(dataTablesOptions),
                    requirementsData.obtainAtDelivery);
            }


            // Table : Received Requirements 
            if (requirementsData.received) {
                dataTablesOptions = Backbone.$.extend(true,{},dataTablesOptionsDefault);

                //add column 'Date Received column' at index 4
                dataTablesOptions.columns.splice(4, 0, dateReceivedColumn);
                dataTablesOptions.order = [];
                dataTablesOptions.columns[0].title = policyIdentificationLabel;
                this._bindEventsAndLoadData(
                    this.ui.receivedRequirementsTable.DataTable(dataTablesOptions),
                    requirementsData.received);
            }

            dataTablesOptions = null;
        }
       
        utils.formatDataTable(this.$el.find('.dataTables_wrapper'));
    },

    /**
     * Bind "column-sizing" and "draw" events on the dataTable and then
     * load the data in a way that will trigger a "draw" event. Both
     * events are needed in order to use the "smart" scrolling message.
     * @param {object} dataTable
     * @param {Array} data
     * @private
     */
    _bindEventsAndLoadData : function _bindEventsAndLoadData (dataTable, data) {
        dataTable.on('column-sizing draw', _.bind(this._onDrawAndColumnResize, this)
        ).rows.add(data).draw();
    },

    /**
     * Get the DOM elements needed to call "smartScrollTable" in "utils.js"
     * using the DataTables settings for context.
     * @param {object} e jQuery event
     * @param {object} settings DataTables settings
     * @private
     */
    _onDrawAndColumnResize : function _onDrawAndColumnResize (e, settings) {
        var tableContainer = $(settings.nTable).closest('.table-responsive');
        var instructionDiv = this.$el.find($(settings.nTable).data('instruction-text'));

        if (tableContainer.length === 1 && instructionDiv.length === 1) {
            utils.setVisibilityOnSmartScrollMessage(tableContainer, instructionDiv);
        }
    },

    /**
     * Send policy data to analytics if it exists. The render method
     * on this view appears to be triggered twice for some reason.
     * Because of this, data is not sent to analytics unless there is
     * data to send.
     * @private
     */
    _setAnalyticsData : function _setAnalyticsData () {
        this.dataLayerValues = {};
        var product      = this.model.get('product');
        var policyStatus = this.model.get('policyStatus');

        if (product && product.productName) {
            this.dataLayerValues['Policy Product Type'] = product.productName;
        }

        if (policyStatus) {
            if (policyStatus.description) {
                this.dataLayerValues['Policy Status'] = policyStatus.description;
            }

            if (policyStatus.statusView) {
                this.dataLayerValues['Status View'] = policyStatus.statusView;
            }
        } 

        if (this.model.has('policyId')) {
            this.dataLayerValues['Policy ID'] = this.model.get('policyId');
        } else if (this.model.has('caseId')) {
            this.dataLayerValues['Case ID'] = this.model.get('caseId');
        }

        // Currently googleTagMangerProvider.trackView method is
        // listening to this event to track page view along with `dataLayerValues`.
        this.trigger(this.gtmTrackViewEvent);
    },

    /**
     * Set text color to brown in 'Needed to Pay Comp'
     * cell of Policy Requirements Table based on value 'Yes'.
     * Uses the columns.render function from DataTables.
     * 
     * @param {DOM} td       TD element
     * @param {string} cellData value of cell
     * @param {[type]} rowData  Data source object / array for the whole row
     * @param {number} row      DataTables' internal index for the row
     * @param {number} col      DataTables' internal index for the column
     * @private
     */
    _setNeedToPayCellContent : function _setNeedToPayCellContent (td, cellData, rowData, row, col) {
        if ( cellData === true ) {
            Backbone.$(td)
                .html('<strong class="pending-brown">Yes</strong>');
        } else {
            Backbone.$(td)
                .text('No');
        }
    },

    /**
    * Add a `policyHighlightsConfig` property to the model that contains data that controls
    * the appearance of the Policy Highlights section.
    *
    * Note that it takes a reference to the model as a parameter instead of modifying `this.model`
    * as a side effect. This makes the function much easier to test.
    *
    * @param {object} policyModel - a PolicyDetailModel object
    */
    _prepPolicyHighlightsConfig : function _prepPolicyHighlightsConfig (policyModel) {
        var policyData = {
            coverage : policyModel.get('coverage'),
            policyValue : policyModel.get('policyValue')
        };
        var columnClasses = ['invalid', 'one-col', 'two-col', 'three-col', 'four-col'];
        var visibleColumns = 4;

        var config = {
            columnClass: 'four-col',
            show: {
                policyValues    : true,  // Change these defaults to false when each column
                ltcValues       : false, // is implemented. (And when all four are implemented,
                cashValues      : false, // delete this comment.)
                rates           : false
            }
        };

        // Should we show the Long Term Care Values column?
        if (policyData.coverage && policyData.coverage.coverageDetail &&
            policyData.coverage.coverageDetail.ltcCoverageDetail &&
            (policyData.coverage.coverageDetail.ltcCoverageDetail.ltcCurrentBenefitBalance ||
            policyData.coverage.coverageDetail.ltcCoverageDetail.ltcTotalWithdrawnAmount ||
            policyData.coverage.coverageDetail.ltcCoverageDetail.monthlyBenefitAmount )) {
            config.show.ltcValues = true;
        }

        // Should we show the Cash Withdrawal Values column?
        if (policyData.policyValue && (policyData.policyValue.penaltyFreeCashWithdrawalAmount ||
            policyData.policyValue.totalCashAmountWithdrawn ||
            policyData.policyValue.remainingCashFreeOutAmount)) {
            config.show.cashValues = true;
        }

        // Should we show the Rates column?
        if (policyData.policyValue && (policyData.policyValue.accumValueInterestRateCurrent ||
            policyData.policyValue.currentInterestRate ||
            policyData.policyValue.guaranteedInterestRate ) ||
            (policyData.coverage && policyData.coverage.coverageDetail &&
            policyData.coverage.coverageDetail.ltcCoverageDetail&&
            policyData.coverage.coverageDetail.ltcCoverageDetail
                .accumulatedValueLtcInterestRateCurrent)) {
            config.show.rates = true;
        }

        // Set the column class correctly based on how many columns we're going to show
        visibleColumns = _.filter(config.show, function(isShown) { return isShown;}).length;
        config.columnClass = columnClasses[visibleColumns];

        policyModel.set('policyHighlightsConfig', config);
    }
});

module.exports = PolicyDetailView;

/* global _:false */

//helper methods for Handlerbars templates

var Handlebars = require('hbsfy/runtime');
var utils = require('../utils/utils');

/**
* Format currency with comma 
* 
* @param {number} number
* @param {boolean} [round = false] - Round number to nearest whole dollar
* @param {boolean} [spaceZero = false] - Return '--' if number is 0
* 
* @return {string} formatted currency
*/
//TODO: Add sample usage to the above documentation
var currencyFormat = function (number, round, spaceZero) {
    //if second / third param is missing, hbs will pass 'options' as second param.
    if (typeof round !== 'boolean') {
        round = false;
    }

    if (typeof spaceZero !== 'boolean') {
        spaceZero=false;
    }
    return utils.formatAsCurrency(number, round, spaceZero);
};

Handlebars.registerHelper('currencyFormat', currencyFormat);


//Equality check between two parameters
/* 
* Sample usasge
*   {{#compare commission.type 'ib'}}
*       <strong>Earnings</strong>
*   {{/compare}}

*    {{#compare some_number 10 operator='>' }}
*       <strong>Earnings</strong>
*   {{/compare}}
*
*/
Handlebars.registerHelper('compare', function(controlValue, actualValue, options) {
    if (arguments.length < 3) {
        throw new Error('Handlebars Helper \'compare\' needs 2 parameters');
    }

    var operator = options.hash.operator || '==';

    var operators = {
        '==':       function(l,r) { return l === r; },
        '===':      function(l,r) { return l === r; },
        '!=':       function(l,r) { return l !== r; },   //JS Lint is picky
        '!==':      function(l,r) { return l !== r; },
        '<':        function(l,r) { return l < r; },
        '>':        function(l,r) { return l > r; },
        '<=':       function(l,r) { return l <= r; },
        '>=':       function(l,r) { return l >= r; },
        '&&':       function(l,r) { return l && r; },
        '||':       function(l,r) { return l || r; },
        'strcis':   function(l,r) { return (l+'').toLowerCase() === (r+'').toLowerCase(); },
        '!strcis':  function(l,r) { return (l+'').toLowerCase() !== (r+'').toLowerCase(); },
        'typeof':   function(l,r) { return typeof l === r; }
    };

    if (!operators[operator]) {
        throw new Error('Handlebars Helper \'compare\' doesn\'t know the operator ' + operator);
    }

    var result = operators[operator](controlValue,actualValue);

    if( result ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
        
});

/**
 * Math helper for arithmetic operator
 * @param  {number} lvalue   left value
 * @param  {number} operator arithmetic operator
 * @param  {number} rvalue   right value
 */
//TODO: Add sample usage to the above documentation
Handlebars.registerHelper('math', function(lvalue, operator, rvalue) {
    
    if (isNaN(lvalue) || isNaN(rvalue) || ['+', '-', '*' ,'/', '%'].indexOf(operator) === -1 ) {
        return 0;
    }

    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
        
    return {
        '+': lvalue + rvalue,
        '-': lvalue - rvalue,
        '*': lvalue * rvalue,
        '/': lvalue / rvalue,
        '%': lvalue % rvalue
    }[operator];
});

/*
 * Convert a string from ISO-4217 currency (USD only) to a number
 *
 * Sample usage:
 *   {{#isoCurrencyToNumber 'USD -1234.56' true true}}
 * Outputs:
 *   1235
 */
Handlebars.registerHelper('isoCurrencyToNumber', function(value, absolute, round) {
    return utils.isoCurrencyToNumber(value, absolute, round);
});

/**
 * Get the time unit of an ISO duration. This is simply using
 * moment's get function {@link http://momentjs.com/docs/#/durations/get/}.
 * 
 * @param {string} value - ISO duration
 * @param {string} unit  - Unit of time to get
 * 
 * Sample usage:
 *      {{isoDurationToTimeUnit 'P107Y4M' 'year'}}
 * Outputs:
 *      107
 */
Handlebars.registerHelper('isoDurationToTimeUnit', function(value, unit) {
    if (!value) {
        return '';
    }

    if (!unit) {
        throw new Error('Handlebars Helper \'isoDurationToTimeUnit\' ' +
            'requires a time unit to be passed');
    }

    return utils.isoDurationToTimeUnit(value, unit);
});


/**
 * Format phone number to US standard format
 *
 * Sample usage:
 *   {{#formatPhoneNumber '3215458555'}}
 * Outputs:
 *   321-545-8555
 */
Handlebars.registerHelper('formatPhoneNumber', function(phoneNumber) {
    return utils.formatPhoneNumber(phoneNumber);
});

/*
* Show / hide graph labels based on Met flags value 
* (if any of the object has match with 'value' passed as param will return true) 
* @param value - string/number 
* @param matchAll - boolean
* @param as hash value
*
* Sample usage:
*    {{#hasValue 'value' true key=value key=value key=value .... }}

*    {{/hasValue}}
*/

Handlebars.registerHelper('hasValue', function(value, matchAll, options) {
    var hasValue = false;
    var notMatch = false;    
    for (var key in options.hash) {
        if (options.hash[key] === value) {
            hasValue = true;
            if (!matchAll) {
                break;
            }
        }
        else {
            notMatch= true;
        }
        //checking whether it matched earlier
        if (notMatch) {
            hasValue = false;
        }
    }

    if (hasValue) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    } 
});

/*
 * Get absolute value of the number
 *
 * Sample usage:
 *   {{absolute -1}}
 * Outputs:
 *   1
 */
Handlebars.registerHelper('absolute', function (value) {
    if(value === undefined || value === null) {
        return '';
    } 

    //convert to number
    value = Number(value);

    if (isNaN(value)) {
        throw new Error ('Expected a numeric inputs');
    }

    return Math.abs(value);
});


/*
 * Get round value of the number
 *
 * Sample usage:
 *   {{round 29.56}}
 * Outputs:
 *   30
 */
Handlebars.registerHelper('round', function (value) {
    if(value === undefined || value === null) {
        return '';
    } 

    //convert to number
    value = Number(value);

    if (isNaN(value)) {
        throw new Error ('Expected a numeric inputs');
    }

    return Math.round(value);
});

/**
 * uppercase converts strings to upper-case values.
 * A null or undefined value will return an empty
 * string.
 * @param {string} value
 *
 * @return {string}
 */
Handlebars.registerHelper('uppercase', function(value) {
    return (value || '').toUpperCase();
});

/**
 * Converts bytes to megabytes within a decimal place.
 */
//TODO: Add sample usage to the above documentation
Handlebars.registerHelper('bytesToMegabytes', function(value) {
    if (isNaN(value)) {
        value = 0;
    }

    return utils.bytesToMegabytesForDisplay(value);
});

/**
 * format date
 * Sample usages :
 * 
 *      {{dateTimeFormat '2018-05-10T15:50:00Z' 'MM/DD/YYYY, hh:mm A z'}}
 *      output: 05/10/2018, 11:50 AM EDT
 * 
 *      {{dateTimeFormat '2018-05-10T15:50:00Z'}} 
 *      output: 05/10/2018
 *      
 *      {{dateTimeFormat '2018-05-10'}} 
 *      output: 05/10/2018
 */
Handlebars.registerHelper('dateTimeFormat', function(date, format) {

    if (typeof format !== 'string') {
        format='';
    }

    return utils.formatDate(date, format);
});

Handlebars.registerHelper('dateFormat', function(date, format) {

    // Ensure that the date format does not contain any 
    // time-related characters like 'h', 'm', 's', or 'z'.
    var invalidFormatCharacters = ['h', 'm', 's', 'z'];
    
    if (typeof format !== 'string') {
        format = 'MM/DD/YYYY';
    }

    // If any invalid characters were passed in the format, throw an error.
    _.each(invalidFormatCharacters, function(value) {
        if (format.indexOf(value) > -1) {
            throw new Error('Handlerbars Helper \'dateFormat\' only allows date-specific ' +
                'formatting. The format passed (' + format + ') contains time-specific ' +
                'formatting. Try using the helper \'dateTimeFormat\' instead.');
        }
    });
    
    return utils.formatDate(date, format, null, false);
});

/**
* Conditional Helper for checking whether object is empty
* @param {object} obj
* @output {boolean}
* @example
*   {{#isNotEmpty person}}
*       Name: {{person.name}}<br>
*       Age: {{person.age}}<br>
*   {{/isNotEmpty}}
*/
Handlebars.registerHelper('isNotEmpty', function(obj, options) {

    if (_.isEmpty(obj)) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    } 
});

/**
* Conditional Helper for checking whether object has dataAvailability property
  with 'noAvailable' value
* @param {object} obj
* 
* @output {boolean} 
* @example 
* 
*   {{#ifAvailable policy.billingDetail}}
*       (markup to display the billing details goes here)
*   {{/ifAvailable}}
*/
Handlebars.registerHelper('ifAvailable', function(obj, options) {
    var isAvailable = true;
    if (obj && obj.dataAvailability === 'notAvailable') {
        isAvailable = false;
    } 
    if (isAvailable) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

/**
* Helper method specifically for pending policy details page
* Use case: if there is name exist wrap roleCode with parenthesis
* 
* @param {string} name  
* @param {string} roleCode
*
* @output {string} "(232dss2)" or 232dss2
*/
//TODO: Add sample usage to the above documentation
Handlebars.registerHelper('wrapRoleCodeWithParenthesis', function(name, roleCode) {
    if (name && roleCode) {
        return '('+roleCode+')';
    }
    return roleCode;
});

/**
 * Block helper to wrap underscore's `has` function.
 * If the given object contains the specified key, the contained block will execute.
 *
 * Sample usage:
 *
 * {{#has insured "tobaccoUse"}}
 *     {{#if insured.tobaccoUse}}
 *         Tobacco
 *     {{else}}
 *         Non-Tobacco
 *     {{/if}}
 * {{else}}
 *     Unknown
 * {{/has}}
 *
 */
Handlebars.registerHelper('has', function(object, key, options) {

    var has = _.has(object, key);

    if (has) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

/**
 * Create Hash URL for HREF attributes
 *
 * Example usage:
 * 
 * {{buildHrefURL '#org-policies/pending/' producerOrgId=id }}
 * This will generate : #org-policies/pending/?producerOrgId=11000
 * 
 * @param  {string} pageLink        Page link can be hash URL or http URL 
 * @param  {object} options         Inbuilt Handlebars helper callback parameter
 *                                  which as data, hash and name objects
 * @return {string} pageLink        Updated page link
 */
Handlebars.registerHelper('buildHrefURL', function(pageLink, options) {
    
    if (!pageLink || !_.isString(pageLink)) {
        throw new Error ('buildHrefURL : Expected pageLink as a string format');
    }
    
    // options.hash object will contain query parameters
    pageLink = utils.buildHrefURL(pageLink, options.hash);

    return pageLink;
});

/**
 * Concat strings together
 *
 * Example usage:
 *
 * {{concat '#pending-policy-list/' 'allPolicies/' '?producerId=10002'}}
 * This will generate : #pending-policy-list/allPolicies/?producerId=10002
 * 
 * @param  {object} JS default arguments
 * @return {string} Concantinated string
 */
Handlebars.registerHelper('concat', function() {
    var outString = '';
    for(var arg in arguments){
        if(typeof arguments[arg] !== 'object' && arguments[arg]){
            outString += arguments[arg];
        }
    }
    return outString;
});


module.exports = Handlebars;

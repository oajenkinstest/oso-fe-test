/* global Backbone:false, _:false, Spinner:false, URI:false */

/**
 * A utility sub module with UI utility methods.
 *
 */

var analyticsChannel = Backbone.Radio.channel('analytics');
var userChannel      = Backbone.Radio.channel('user');

var utilsUI = {

    errors: {
        pageLinkExpectedAsString   : 'buildHrefURL : Expected pageLink as a string format',
        scrollToJQuery             : '$element must be a jQuery object',
        sendFormErrorsToAnalytics : {
            emptyErrors   : 'errors parameter passed to sendFormErrorsToAnalytics must be ' +
                            'an object',
            emptyFieldMap : 'fieldMap parameter passed to sendFormErrorsToAnalytics must be ' +
                            'an object',
            formName      : 'formName passed to sendFormErrorsToAnalytics must not be null'
        },
        invalidURL      : 'addTargetUserQueryParamToURL: URL parameter should start with ' +
                          'hash/http/https',
        urlIsMissing    : 'addTargetUserQueryParamToURL: URL parameter is missing',
        invalidBaseURL  : 'base URL provided is not valid URL, it should start with http/https',
        invalidRelativePath : 'relative path should be in string format'
    },

    /**
     * Remove the wrapping div element that Backbone adds to the view by default.
     * @param marionetteView the Marionette view to be unwrapped.
     */
    unwrapView: function unwrapView(marionetteView) {
        marionetteView.$el = marionetteView.$el.children();
        marionetteView.$el.unwrap();
        marionetteView.setElement(marionetteView.$el);
    },


    /**
     * Scroll to the top of the specified jQuery element
     * @param {jquery object} $element the element to scroll to
     * @param {number} minusOffset subtraction some offset with actual one
     */
    scrollTo: function scrollTo($element, minusOffset) {
        if (! $element || ! ($element instanceof Backbone.$)) {
            throw new Error(utilsUI.errors.scrollToJQuery);
        }
        var _this = this;

        //get scroll position 
        var scrollPosition = this.getOffsetTop($element, minusOffset);
        Backbone.$('html, body').animate({
            scrollTop: scrollPosition
        }, {
            durarion : 400,

            // to check variance of scroll position during scrolling
            // and stop current one, start with new value
            step: function(now, fx) {
                var newScrollPosition = _this.getOffsetTop($element, minusOffset);
                if (scrollPosition !== newScrollPosition  ) {
                    Backbone.$(this).stop().animate({scrollTop: newScrollPosition}, 400);
                }
            }
        });
    },

    /**
     * Get offset top
     * @param  {jquery object} $element    
     * @param  {number} minusOffset For some cases if we want to minus actual offset 
     *                               value for better positionng 
     * @return {number}             scroll position
     */
    getOffsetTop : function ($element, minusOffset) {
        
        //get element offset from document(parent)
        var offset = $element.offset();
        var scrollPosition = offset ? offset.top : 0;

        if (minusOffset && typeof minusOffset === 'number') {
            scrollPosition = scrollPosition - minusOffset;
        }

        return scrollPosition;
    },

    /**
     * Verify URL validity
     * Currently only support
     *     * Should start with https:// or http://
     *     * White space not allowed
     * 
     * @param  {string}  url 
     * @return {Boolean}  
     */
    isValidURL : function isValidURL (url) {
        var urlRegex = new RegExp(
            /^(https?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        );

        if (url && urlRegex.test(url)) {
            return true;
        }

        return false;
    },

    /**
     * Parse the stateObj and create an object for the search wrapper.
     * @param stateObj
     * @returns {{}}
     */
    parseSearchParams : function parseSearchParams(stateObj) {
        var options     = {};
        var searchState = {};

        if (stateObj.searchTerm) {
            options.searchTerm = stateObj.searchTerm;
        }

        if (stateObj.searchType) {
            options.searchType = stateObj.searchType;
        }

        if (stateObj.col || stateObj.col === 0) {
            searchState.col = stateObj.col;
        }

        if (stateObj.dir) {
            searchState.dir = stateObj.dir;
        }

        if (stateObj.length) {
            searchState.length = stateObj.length;
        }

        if (stateObj.start || stateObj.start === 0) {
            searchState.start = stateObj.start;
        }

        if (stateObj.noCache) {
            searchState.noCache = stateObj.noCache;
        }

        if (!_.isEmpty(searchState)) {
            options.searchState = searchState;
        }

        return options;
    },

    /**
     * Generate spinner element to show wait indicator
     * @param {string} size Optional parameter to define the size small|medium
     * @param {string} position Optional parameter It can be static|relative|fixed|absolute
     * @return {String}      Spinner element
     */
    generateSpinnerElement : function  generateSpinnerElement (size, position) {
        
        //standard
        var options = {
            lines: 12, 
            corners:0,
            width: 5,
            length: 8,
            radius:10,
            color: '#4899ce'
        };
        var spinner;

        if (position) {
            options.position = position; 
        }

        if (size === 'small') {
            _.extend(options, {
                width:2,
                length:3,
                radius:3
            });
        } else if (size === 'medium') {
            _.extend(options, {
                width:2,
                length:5,
                radius:5
            });
        }

        spinner = new Spinner(options).spin();
        return spinner.el;
    },

    /**
     * Handles the mapping of error messages to form fields and sends the errors and
     * related fields to "trackException" in the analytics module.
     *
     * The fieldMap object maps keys in the errors object to fields in the form. For
     * example, an errors object which looks like this:
     *
     * {
     *    serviceFileErrorText : "This file is too big",
     *    creditCardErrorText  : "This selection requires an attachment"
     * }
     *
     * ... may have a fieldMap which looks like this:
     *
     * {
     *     serviceFileErrorText : "attachments",
     *     fileErrorText        : "attachments",
     *     creditCardErrorText  : "creditCard"
     * }
     *
     * @param {Object} errors
     * @param {Object} fieldMap Used to associate fields on the form with errors keys
     * @param {string} formName The name of the form
     */
    sendFormErrorsToAnalytics : function sendFormErrorsToAnalytics(errors, fieldMap, formName) {
        var messageObject = {
            form   : null,
            errors : {}
        };

        if (!errors || !_.isObject(errors) || _.isEmpty(errors)) {
            throw new Error(this.errors.sendFormErrorsToAnalytics.emptyErrors);
        }

        if (!fieldMap || !_.isObject(fieldMap) || _.isEmpty(fieldMap)) {
            throw new Error(this.errors.sendFormErrorsToAnalytics.emptyFieldMap);
        }

        if (!formName) {
            throw new Error(this.errors.sendFormErrorsToAnalytics.formName);
        }

        messageObject.form = formName;

        // map the errors to field names
        _.each(_.keys(errors), function (key) {
            var fieldName    = fieldMap[key];
            var errorMessage = errors[key];

            if (!_.isUndefined(fieldName)) {
                messageObject.errors[fieldName] = errorMessage;
            }

        });

        analyticsChannel.trigger('trackException', {
            fatal   : false,
            message : JSON.stringify(messageObject)
        });

    },

    /**
     * Determine whether to show/hide the "smart scroll" message for a table. If vertical
     * scroll bar is present on the tableContainer, the message will be shown.
     *
     * @param {Object} tableContainer
     * @param {Object} messageContainer
     */
    setVisibilityOnSmartScrollMessage : function
        setVisibilityOnSmartScrollMessage(tableContainer, messageContainer) {

        var nativeDomContainer;
        if (messageContainer && messageContainer instanceof Backbone.$ &&
            tableContainer && tableContainer instanceof Backbone.$ && tableContainer.length) {

            // The only way to reliably detect a scroll bar
            // is by using scrollWidth and offsetWidth on the native DOM
            // element. jQuery's innerWidth() and outerWidth() were tested
            // but didn't reliably detect the overflow.
            nativeDomContainer = tableContainer[0];

            if (nativeDomContainer.scrollWidth > nativeDomContainer.offsetWidth) {
                messageContainer.removeClass('hidden');
            } else {
                messageContainer.addClass('hidden');
            }
        }
    },

    /**
     * Add 'targetuser' query parameter to URL if impersonate Web Id exist in user Module
     * 
     * @param {string} url It can be  hash URL / HTTP URL
     * @return {string} url updated with 'targetuser' query parameter
     */
    addTargetUserQueryParamToURL : function addTargetUserQueryParamToURL(url) {

        if (!url) {
            throw new Error(this.errors.urlIsMissing);
        } else if (url.indexOf('#')!==0 && !this.isValidURL(url)) {
            throw new Error(this.errors.invalidURL);
        }

        var impersonatedWebId = userChannel.request('getImpersonatedWebId');

        if (!impersonatedWebId) {
            return url;
        }

        // Regex to check whether query param has 'targetuser'
        var regexHasAsInURL = new RegExp('([?&])targetuser=.*?(&|$)', 'i');

        // separator to add 'targetuser' query based on existence of ?
        var separator = url.indexOf('?') !== -1 ? '&' : '?';

        //add only if 'targetuser' query is not exist in queryParams
        if (!url.match(regexHasAsInURL)) {

            url =  url + separator + 'targetuser=' + impersonatedWebId;
        }/* else {
            // There will be another scenario that we may need to overwrite value of 
            // url query 'targetuser' with current value got from url
            // not sure whether this scenario exist or not.
            // So keeping in comment
            url =  url.replace(regexHasAsInURL, '$1targetuser=' + queryParamsObj.targetuser + '$2');
        }*/

        return url;
    },

    /**
     * Create final Hash URL / HTTP URL for href attribute.
     * @param  {string} pageLink  It can hash URL / HTTP URL.
     *                            Will be prefix hash 
     *                            If pageLink is missing hash / http
     *                            
     * @param  {object} parameters  An object to build URL query params
     * @return {string} pageLink Updated page Link;
     */
    buildHrefURL : function buildHrefURL (pageLink, parameters) {
        
        var queryParams;

        if (!pageLink || !_.isString(pageLink)) {
            throw new Error (this.errors.pageLinkExpectedAsString);
        }

        // Separator to add 'targetuser' query based on existence of '?'
        var separator = pageLink.indexOf('?') !== -1 ? '&' : '?';

        // If pageLink is missing #
        if (pageLink.charAt(0) !== '#' && !pageLink.match(/^(http|https):\/\//gi) ) {
            pageLink = '#'+pageLink;
        }

        if (parameters && !_.isEmpty(parameters)) {
            queryParams = Backbone.$.param(parameters);
            pageLink = pageLink+separator+queryParams;
        }

        //add 'targetuser' query param if user in impersonation/delegation mode
        pageLink = this.addTargetUserQueryParamToURL(pageLink);

        return pageLink;
    },

    /**
     * Find all 'a' elements from the view passed in as a parameter to convert to determine
     * whether they should be converted to "#c" links. Adds the "targetuser" query parameter when
     * needed.
     *
     * @param {object} view
     */
    convertWcmLinks : function convertWcmLinks(view) {
        var self = this;
        // Find all 'a' elements from WCM contents:
        //   1. Elements with href attribute with the URL pattern matched.
        //   2. Ignore the elements those are having CSS classes or data attributes listed below.
        var aElements = view.$el.find(
            'a[href^="/wps/wcm/connect/indcontent/OSO/"], '+
            'a[href^="/wps/wcm/connect/indcontent/oso/"], '+
            'a[href^="/wps/wcm/myconnect/indcontent/OSO/"], '+
            'a[href^="/wps/wcm/myconnect/indcontent/oso/"], '+
            'a[href^="#c/"], ' +
            'a[href^="/#c/"]'
        ).not(
            '.icon-pdf, '+
            '.icon-external,'+
            '.icon-email,'+
            '.icon-video,'+
            '[data-toggle="modal"]'
        );

        aElements.each( function (idx, aElement) {
            var hasPdfOrZipIcon;
            var href = Backbone.$(aElement).attr('href');

            // TODO: When existing WCM content has replaced the <i> tags
            // following the links with the ".icon-*" classes, the `hasPdfOrZipIcon`
            // as well as the `iconIndicatesViewableContent` function in `modules/behaviors.js`
            // can be removed.
            hasPdfOrZipIcon = Backbone.$(aElement).next().is('.fa-file-pdf-o,.fa-file-archive-o');

            // If the link is not followed by an <i> tag which indicates a link
            // to either a PDF or a ZIP archive, we can safely assume that the
            // content is HTML and convert the href to a "#c" link and/or add 'targetuser' param.
            if (!hasPdfOrZipIcon) {
                //replace with #c if URL prefix pattern is matched
                href = href.replace(
                    /\/wps\/wcm\/(?:my)?connect\/indcontent\/OSO/i,
                    '#c'
                );

                // Add 'targetuser' query param if user is in impersonate state
                href = self.addTargetUserQueryParamToURL(href);

                Backbone.$(aElement).attr('href', href);
            }
        });
    },
    
    /**
     * Merge relative path with base url and build an absolute url
     * 
     * @param {string} baseURL 
     * @param {string} relativePath
     * 
     * @return {string} absolute url
     */
    mergeRelativeToAbsoluteURL : function mergeRelativeToAbsoluteURL(baseURL, relativePath) {

        if (!baseURL || !baseURL.match(/^(http|https):\/\//i)) {
            throw new Error(this.errors.invalidBaseURL);
        } else if(!relativePath || !_.isString(relativePath)) {
            throw new Error(this.errors.invalidRelativePath);
        }

        // Add trailing slash if that missing
        var splitBaseURL = baseURL.split('?');
        if (!splitBaseURL[0].match(/\/$/)) {
            splitBaseURL[0] = splitBaseURL[0]+ '/';
        }
        if (splitBaseURL[1]) {
            baseURL = splitBaseURL[0]+'?'+splitBaseURL[1];
        } else {
            baseURL = splitBaseURL[0];
        }

        var url;
        if (typeof window.URL === 'function') {
            url = new URL(relativePath, baseURL).toString();
        } else {
            url = new URI(relativePath).absoluteTo(baseURL).toString();
        }

        return url;
    }
};

module.exports = utilsUI;

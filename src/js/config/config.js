/* global _ */

var config = {
    apiBase           : '/api/oso/secure/rest/',
    apiBasePublic     : '/api/oso/public/web/',
    salesForceBaseUrl : 'https://na60.salesforce.com/',
    wcmBaseUrl        : '/wps/wcm/connect/indcontent/OSO/',
    wcmRolePrefix     : '/inddesign/TAX-Roles/',
    defaultTimezone   : 'America/New_York'
};

config.errorMessages = {
    noPageFound : 'The page you requested does not exist.'
};

//@ifdef WIDGETPCPROD
// api base for performance center vintage
// below statement will be added only for 'widget-pc-prodcheck' build
config.apiBase = '/oso/secure/rest/';
//@endif

var envConfig = {

    // IMPORTANT: These 4 (or 3)-letter keys are matched to the first 4 (or 3)
    // letters in the URL hostname. Don't change them!

    // local development environments : http://localhost:3000
    loca : {
        debugEnabled     : true,
        defaultLoginPage : 'http://localhost:3000/devdata.html',
        pinpointURL      : 'http://localhost.com/devdata.html?COEPinpointglobal',
        webTrends        : {
            dcsid        : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms   : 'ftonesourceonline.oneamerica.com',
            fpcdom       : '.ftonesourceonline.oneamerica.com'
        },
        gtmContainerId   : 'GTM-PK6RP5Q',
        cache : {
            ttl     : 0.17,
            wcmTtl  : 0.5
        },

        // feature flag to enable / disable application feature 
        fflag : {
            showActiveBillingFields : true
        }
    },

    // sandbox : https://sbonesourceonline.oneamerica.com
    sbon : {
        debugEnabled     : true,
        defaultLoginPage : 'https://www.sb.oneamerica.com/login',
        pinpointURL      : 'https://sbapi.oneamerica.com/oso/secure/web/sso/pinpoint',
        webTrends        : {
            dcsid        : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms   : 'ftonesourceonline.oneamerica.com',
            fpcdom       : '.ftonesourceonline.oneamerica.com'
        },
        gtmContainerId   : 'GTM-PK6RP5Q',

        // cache expiry time
        cache : {

            // 10 minutes 2 sec
            ttl     : 0.17,

            // 30 minutes
            wcmTtl  : 0.5
        }
    },

    // sandbox : https://www.sbols.oneamerica.com    
    sbol : {
        debugEnabled     : true,
        webTrends        : {
            dcsid        : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms   : 'ftonesourceonline.oneamerica.com',
            fpcdom       : '.ftonesourceonline.oneamerica.com'
        }
    },

    // system test : https://stonesourceonline.oneamerica.com
    ston : {
        debugEnabled      : true,
        defaultLoginPage  : 'https://www.st.oneamerica.com/login',
        pinpointURL       : 'https://stapi.oneamerica.com/oso/secure/web/sso/pinpoint',
        webTrends         : {
            dcsid         : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms    : 'ftonesourceonline.oneamerica.com',
            fpcdom        : '.ftonesourceonline.oneamerica.com'
        },
        gtmContainerId    : 'GTM-PK6RP5Q',
        cache : {
            ttl     : 0.17,
            wcmTtl  : 0.5
        },
        
        // feature flag to enable / disable application feature 
        fflag : {
            // no feature flags are required
        }
    },

    // system test : https://www.stols.oneamerica.com
    stol : {
        debugEnabled    : true,
        webTrends       : {
            dcsid       : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms  : 'ftonesourceonline.oneamerica.com',
            fpcdom      : '.ftonesourceonline.oneamerica.com'
        }
    },

    // function test : https://ftonesourceonline.oneamerica.com
    fton : {
        debugEnabled     : false,
        defaultLoginPage : 'https://www.ft.oneamerica.com/login',
        pinpointURL      : 'https://ftapi.oneamerica.com/oso/secure/web/sso/pinpoint',
        webTrends        : {
            dcsid        : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms   : 'ftonesourceonline.oneamerica.com',
            fpcdom       : '.ftonesourceonline.oneamerica.com'
        },
        gtmContainerId   : 'GTM-PK6RP5Q',
        cache : {
            ttl     : 0.17,
            wcmTtl  : 0.5
        },

        // feature flag to enable / disable application feature 
        fflag : {
            // no feature flags are required
        }
    },

    // function test : https://www.ftols.oneamerica.com
    ftol : {
        debugEnabled    : false,
        webTrends       : {
            dcsid       : 'dcs222ry8pkjqwz2mhuqmdtim_6c9j',
            onsitedoms  : 'ftonesourceonline.oneamerica.com',
            fpcdom      : '.ftonesourceonline.oneamerica.com'
        }
    },

    // production (new stack): https://onesourceonline.oneamerica.com
    ones : {
        debugEnabled     : false,
        defaultLoginPage : 'https://www.oneamerica.com/login',
        pinpointURL      : 'https://api.oneamerica.com/oso/secure/web/sso/pinpoint',
        webTrends        : {
            active       : true,
            dcsid        : 'dcs222dn4iur9k64ed0auexqc_3q4w',
            onsitedoms   : 'onesourceonline.oneamerica.com',
            fpcdom       : '.onesourceonline.oneamerica.com'
        },
        gtmContainerId   : 'GTM-NWBWNKT',
        cache : {
            ttl     : 0.17,
            wcmTtl  : 0.5
        },

        // feature flag to enable / disable application feature 
        fflag : {
            // no feature flags are required
        }
    },

    // production (old stack) : https://www.ols.oneamerica.com
    ols : {
        debugEnabled    : false,
        webTrends       : {
            dcsid       : 'dcs222dn4iur9k64ed0auexqc_3q4w',
            onsitedoms  : 'onesourceonline.oneamerica.com',
            fpcdom      : '.onesourceonline.oneamerica.com'
        }
    }

};

/*
* Method to get config based on hostname
*
* @param hostname - string
*/
config.getAjaxConfig = function (hostname) {

    if (!hostname || !_.isString(hostname)) {
        throw new Error('Config.getAjaxConfig : parameter hostname should be a string');
    } 

    var envPrefix = hostname.slice(0,2);

    // If this code is running within the old stack (e.g. Performance Center),
    // all of the urls will start with "www." and that needs to be ignored.
    if (envPrefix === 'ww') {
        // skip the 'www.'
        envPrefix = hostname.slice(4,8);

        //replace ols. with ols
        envPrefix = envPrefix.replace(/\./,'');

    //to run with new api URL which don't have www. prefix
    } else {
        envPrefix = hostname.slice(0,4);
    }

    // Use the config settings that correspond to the first 4 (or 3) letters
    // of the hostname.
    if (envConfig[envPrefix]) {
        config = _.extend(config, envConfig[envPrefix]);
    }

    config.apiUrlRoot       = config.apiBase;
    config.apiPublicUrlRoot = config.apiBasePublic;

    return config;
};

//load configs based on hostname
config = config.getAjaxConfig(window.location.hostname);

module.exports = config;

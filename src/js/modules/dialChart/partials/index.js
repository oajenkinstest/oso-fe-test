var Handlebars = require('hbsfy/runtime');

Handlebars.registerPartial(
    'chartLabels',
    require('./chart-labels.hbs')
);

module.exports = Handlebars;

/* global Marionette */
/**
 * Simple footer View.
 * Created by jbell on 12/4/15.
 */

var footerTemplate = require('../templates/footer-t.hbs');

var FooterView = Marionette.ItemView.extend({
    template: footerTemplate
});

module.exports = FooterView;

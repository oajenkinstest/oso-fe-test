/* global Backbone, Marionette */
/**
 * Footer Module. Creates the footer view with the region by the options.region property.
 * Created by jbell on 12/4/15.
 */

var FooterView = require('./views/footer-v');

var Footer = Marionette.Object.extend({
    
    errors : {
        parentRegion : 'Footer requires an options object with a region property!'
    },

    initialize: function(options) {

        if (! options || ! options.region) {
            throw new Error(this.errors.parentRegion);
        }

        var model = new Backbone.Model({
            year: (new Date()).getFullYear()
        });

        this.footerView = new FooterView({
            model : model
        });

        options.region.show(this.footerView);
    }
});

module.exports = Footer;

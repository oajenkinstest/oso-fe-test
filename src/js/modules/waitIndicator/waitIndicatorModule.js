/* global Backbone, Marionette, Spinner */
/**
 * A module to draw wait indicator using spin plugin
 *
 * Usage: 
 * var WaitIndicatorModule  = require('waitIndicatorModule');
 *
 * //initialize at OSOApp.js or from start up of the App
 * var waitIndicator = new WaitIndicatorModule();
 *
 * //later on use channel call from any module or page
 * var waitIndicatorChannel = Backbone.Radio.channel('wait');
 * 
 * //to show
 * waitIndicatorChannel.trigger('show');
 *
 * //to hide
 * waitIndicatorChannel.trigger('hide');
 * 
 */

var WaitIndicatorModule  = Marionette.Object.extend({

    initialize: function(options) {

        // Let's make the interface with Backbone.Radio
        var channelName = options && options.channelName || 'wait';
        this.waitIndicatorChannel = Backbone.Radio.channel(channelName);
        
        /*
         * Register wait indicator channel callback for showSpinner and hideSpinner
         */
        this.listenTo(this.waitIndicatorChannel, 'show', this.showSpinner);
        this.listenTo(this.waitIndicatorChannel, 'hide', this.hideSpinner);
        this.listenTo(this.waitIndicatorChannel, 'reset', this.resetActiveSpinnersCount);

        // get ready
        this.spinner = new Spinner({position: 'fixed', color: '#4899ce'});

        this.activeSpinners = 0;
    },

    /**
     * Method to show spinner
     */
    showSpinner : function () {

        this.spinner.spin();
        Backbone.$('body').append(this.spinner.el);

        this.activeSpinners++;
    },

    /**
     * Method to hide spinner
     */
    hideSpinner : function () {
        this.activeSpinners--;

        if (this.activeSpinners <= 0) {
            this.spinner.stop();
            this.activeSpinners = 0;
        }
    },

    /**
    * Reset count while navigating page.
    */
    resetActiveSpinnersCount : function () {
        this.activeSpinners = 0;
    }

});

module.exports = WaitIndicatorModule;

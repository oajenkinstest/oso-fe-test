/**
 * Instantiates the Behaviors object when the module is used.
 */
var Behaviors = require('./behaviors');
var behaviors = new Behaviors();

module.exports = behaviors;
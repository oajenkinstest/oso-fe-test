/* global Marionette, $ */
var navbarViewTemplate = require('../templates/navbar.hbs');

// will autoload the partials in that directory
require('../partials');

// Load handlebars helper method to avoid unit test failure
require('../../../utils/hb-helpers');

var NavbarView = Marionette.ItemView.extend({

    template: navbarViewTemplate,

    ui: {
        homeLink                    : '#__OA_LINK__',
        logoutLink                  : '#logout_link',
        producerDelegateAccessLink  : '#delegate_access'
    },

    events: {
        'click @ui.homeLink'                    : 'triggerNavEvent',
        'click @ui.logoutLink'                  : 'triggerLogoutEvent',
        'click @ui.producerDelegateAccessLink'  : 'triggerProducerDelegateAccessEvent',
    },

    modelEvents: {
        'change:userName'                   : 'userNameChanged',
        'change:hasProducerDelegateAccess'  : 'showProducerDelegateMenuOptions'
    },

    userNameChanged: function() {
        this.render();
    },

    showProducerDelegateMenuOptions : function showProducerDelegateMenuOptions () {
        this.render();
    },

    updateUser: function (model) {
        this.model = model;
    },

    triggerNavEvent: function (e) {
        e.preventDefault();

        var $clickTarget = $(e.currentTarget);
        var href = $clickTarget.attr('href');

        this.trigger('nav', href);
    },

    triggerLogoutEvent: function (e) {
        e.preventDefault();

        this.trigger('logout');
    },

    triggerProducerDelegateAccessEvent: function triggerProducerDelegateAccessEvent (e) {
        e.preventDefault();

        this.trigger('startProducerDelegateAccess');
    }
});

module.exports = NavbarView;

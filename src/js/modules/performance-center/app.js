/* global require, Backbone, Marionette */
/*
* To take build alone for performance center widgte use below command
*    gulp widget-pc-dev 
* this will generate app which have only widget running. 
* dist folder contain only the files for the widget
* for modifying target element modify root-layout.js
*/

var RootLayout             = require('./views/root-layout-v');
var PerformanceCenterView  = require('./views/performance-center-v');
var ajaxUtils              = require('../../utils/ajax-utils');

var app = new Marionette.Application();

var appOptions = {};

/*
 * This probably belongs in setup or something...
 *
 * pascalpp - override Backbone.History.prototype.route to allow for handlers to have a
 * router associated with them this allows us to remove routes dynamically e.g. in
 * router.destroy.
 *
 * Also, add a call to analytics in here?
 */
Backbone.History.prototype.route = function(route, callback, router) {
    this.handlers.unshift({router: router, route: route, callback: callback});
};

app.on('before:start', function (options) {
    // Set up some options before the app is actually started
    //options.initialContentView = new Page1View();
});

// A simple object in place of the usual userModule
var userMod = {
    impersonatedWebId: null,
    getImpersonatedWebId: function () {
        return userMod.impersonatedWebId;
    }
};

// Setting up ajax options
// the prefilter allows for CORS
Backbone.$.ajaxPrefilter(ajaxUtils.buildPrefilter(userMod));

// beforeSend adds the URL to the jqXHR object so that it is available in error handlers
Backbone.$.ajaxSetup({
    beforeSend:ajaxUtils.beforeSend
});

Backbone.$(document).ajaxError(ajaxUtils.buildErrorHandler(function() {}));

app.on('start', function(options) {
    var rootView = new RootLayout();
    this.rootView = rootView; // to make Marionette Inspector happy
    rootView.render();

    var performanceCenterView = new PerformanceCenterView({
        showInfoLinks: true
    });
    rootView.performanceCenterRegion.show(performanceCenterView);

    Backbone.history.start();
});

Backbone.$(document).ready(function () {
    var $targetElement = Backbone.$('#performancecenter');
    var targetUser     = null;

    // Add target element if that not exists.
    if (!$targetElement || !$targetElement.length) {
        Backbone.$('body').append('<div id="performancecenter"></div>');
        $targetElement = Backbone.$('#performancecenter');
    }

    //get the targetuser data element. 
    //This indicates that the user is impersonating the targetuser
    targetUser = $targetElement.data('targetuser');
    if (targetUser) {
        userMod.impersonatedWebId = targetUser;
    }

    app.start(appOptions);
});

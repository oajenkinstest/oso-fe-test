/* global Backbone */

var config = require('../../../config/config');

var PerformanceCenterModel = Backbone.Model.extend({
    url: config.apiUrlRoot + 'performanceCenter',

    //Adding new objects to peform view rendering for each section
    //This one will not make any changes to response objects 
    //unless adding new/formatted objects.
    parse: function (response) {
        var performanceCenterData={};
        var cyp = response.calendarYearProduction;
        var ct  = response.chairmansTrip;
        var lc  = response.leadersConference;
        var apr = response.annualProductionRequirements;

        if (cyp) {
            
            if (cyp.commission) {
                
                // section FYC (retail and ib)
                cyp.commission.section  = 'fyc';
                cyp.commission.textOne  = 'Year To Date';
                cyp.commission.viewType = 'text';
                cyp.commission.textTwo = 'First Year Commission (FYC)';
                performanceCenterData[cyp.commission.section] = cyp.commission;

                // section qualifyingFyc - only for retail
                if (cyp.type === 'retail') {
                    var qualifyingFyc = {
                        section              : 'qualifyingFyc',
                        textOne              : 'Year To Date',
                        textTwo              : 'Qualifying FYC',
                        viewType             : 'text',
                        qualifyingYearToDate : cyp.commission.qualifyingYearToDate
                    };
                    performanceCenterData[qualifyingFyc.section] = qualifyingFyc;
                }
            }

            // As its required to render two seperate views splitting up
            // Only for IB
            if (cyp.fypc && cyp.type === 'ib') {

                var fypc = {
                    section  : 'fypc',
                    textOne  : 'Year To Date',
                    textTwo  : 'First Year Premium Credit (FYPC)',
                    viewType : 'text',
                    totalYearToDate : cyp.fypc.totalYearToDate
                };
                performanceCenterData[fypc.section] = fypc;

                var qualifyingFypc = {
                    section  : 'qualifyingFypc',
                    textOne  : 'Year To Date',
                    textTwo  : 'Qualifying FYPC',
                    viewType : 'text',
                    qualifyingYearToDate : cyp.fypc.qualifyingYearToDate
                };
                performanceCenterData[qualifyingFypc.section] = qualifyingFypc;
                
            }

            if (cyp.lifeLives) {
                cyp.lifeLives.section = 'lifeLives';
                cyp.lifeLives.asOfDate = cyp.asOfDate;
                performanceCenterData[cyp.lifeLives.section] = cyp.lifeLives;
            }

            // performanceCenter refresh date
            performanceCenterData.refreshDate = cyp.asOfDate;
        }

        if (ct) {
            if (ct.type === 'retail') {
                // The data contract for the retail CT uses "Fyc" names, while the IB CT still
                // uses "Fypc" names. Create "Fypc" versions of the properties so that both
                // types of CT can use the same views.
                ct.actualQualFypc              = ct.actualQualFyc;
                ct.onscheduleQualFypc          = ct.onscheduleQualFyc;
                ct.onscheduleRemainingQualFypc = ct.onscheduleRemainingQualFyc;
                ct.requiredQualFypc            = ct.requiredQualFyc;
                ct.requiredRemainingQualFypc   = ct.requiredRemainingQualFyc;

                ct.fyLabel = 'FYC';
            } else {
                ct.fyLabel = 'FYPC';
            }
            ct.section = 'chairmansTrip';
            performanceCenterData[ct.section] = ct;
        }

        if (lc) {
            if (lc.type === 'retail') {
                lc.toggleChart = true;

                // The data contract for the retail LC uses "Fyc" names, while the IB LC still
                // uses "Fypc" names. Create "Fypc" versions of the properties so that both
                // types of LC can use the same views.
                lc.actualQualFypc = lc.actualQualFyc;
                lc.actualTotalFypc = lc.actualTotalFyc;
                lc.onscheduleQualFypc = lc.onscheduleQualFyc;
                lc.onscheduleRemainingQualFypc = lc.onscheduleRemainingQualFyc;
                lc.onscheduleRemainingTotalFypc = lc.onscheduleRemainingTotalFyc;
                lc.onscheduleTotalFypc = lc.onscheduleTotalFyc;
                lc.requiredQualFypc = lc.requiredQualFyc;
                lc.requiredRemainingQualFypc = lc.requiredRemainingQualFyc;
                lc.requiredTotalFypc = lc.requiredTotalFyc;
                lc.requiredRemainingTotalFypc = lc.requiredRemainingTotalFyc;
                lc.onscheduleQualFypcFlag = lc.onscheduleQualFycFlag;
                lc.onscheduleTotalFypcFlag = lc.onscheduleTotalFycFlag;
                lc.requiredQualFypcFlag = lc.requiredQualFycFlag;
                lc.requiredTotalFypcFlag = lc.requiredTotalFycFlag;

                lc.fyLabel = 'FYC';
            } else {
                lc.fyLabel = 'FYPC';
            }
            lc.section = 'leadersConference';
            performanceCenterData[lc.section] = lc;
        }

        if (apr) {
            apr.section = 'annualProductionRequirements';
            performanceCenterData[apr.section] = apr;
        }

        return performanceCenterData;
    }
});

module.exports = PerformanceCenterModel;

/* global require */

/**
* Extending Performance center dial chart Base view for Leaders' Conference (LC)
*/

var utils  = require('../../../utils/utils');
var PerformanceCenterDialBaseView = require('./performance-center-dial-base-v');
var performanceCenterDialLCTemplate 
    = require('../templates/performance-center-dial-lc-t.hbs');

var PerformanceCenterDialLCView = PerformanceCenterDialBaseView.extend({
    template: performanceCenterDialLCTemplate,
    buildDialChartOptions: function () {

        //*** Qualifying FYPC Qualified ***/
        var actualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('actualQualFypc'), false, true
        );
        this.model.set('actualFypcNumber', actualFypcNumber);

        var requiredFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('requiredQualFypc'), true, true
        );

        // Check mark flag
        // Same flag will be used for Qualifying FYPC onSchedule
        if (this.model.get('qualifyingGoalAchievedFlag') === true) {
            this.checkMark = true;
        }
        if (this.model.get('leadersAprStatus')) {
            if (this.checkMark && (this.model.get('leadersAprStatus') !== 'FQ'
                    && this.model.get('leadersAprStatus') !== 'NR' )) {
                this.checkMark = false;
            }
        }

        this.dialChartOptionsOne = {
            chartType: 'double',
            outerValue:actualFypcNumber,
            innerValue:this.model.get('actualLives'),
            classLabelOne: 'chart-double-text-one',
            classLabelTwo: 'chart-double-text-two',
            textLabelOne: utils.formatAsCurrency(actualFypcNumber),
            textLabelTwo: this.model.get('actualLives')+ ' lives',
            checkMark: this.checkMark,
            outerChartOptions: {
                maxValue   : requiredFypcNumber,
                scaleColor : '#ffffff'
            },
            innerChartOptions: {
                maxValue   : this.model.get('requiredLives'),
                scaleColor : '#ffffff'
            }
        };

        var requiredRemainingQualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('requiredRemainingQualFypc'), true, true
        );
        this.model.set('requiredRemainingQualFypcNumber', requiredRemainingQualFypcNumber);

        //show green color for outer chart if flag Y
        if (requiredRemainingQualFypcNumber === 0 && !this.checkMark) {
            this.dialChartOptionsOne.outerChartOptions.barColor = this.dialColorComplete;
        } else if (this.model.get('requiredRemainingLives') === 0 && !this.checkMark) {
            this.dialChartOptionsOne.innerChartOptions.barColor = this.dialColorComplete;
        }

        //*** Qualifying FYPC onSchedule ***/
        var onScheduleFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('onscheduleQualFypc'), true, true
        );

        var onscheduleRemainingQualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('onscheduleRemainingQualFypc'), true, true
        );
        this.model.set('onscheduleRemainingQualFypcNumber', onscheduleRemainingQualFypcNumber);

        this.dialChartOptionsTwo = {
            chartType: 'double',
            outerValue:actualFypcNumber,
            innerValue:this.model.get('actualLives'),
            classLabelOne: 'chart-double-text-one',
            classLabelTwo: 'chart-double-text-two',
            textLabelOne: utils.formatAsCurrency(actualFypcNumber),
            textLabelTwo: this.model.get('actualLives')+ ' lives',
            checkMark: this.checkMark,
            outerChartOptions: {
                maxValue:onScheduleFypcNumber,
                scaleColor: '#ffffff'
            },
            innerChartOptions: {
                maxValue:this.model.get('onscheduleLives'),
                scaleColor: '#ffffff'
            }
        };

        //show green color for chart bars depending on flags
        if (onscheduleRemainingQualFypcNumber === 0 && !this.checkMark) {
            this.dialChartOptionsTwo.outerChartOptions.barColor = this.dialColorComplete;
        }
        if (this.model.get('onscheduleRemainingLives') === 0 && !this.checkMark) {
            this.dialChartOptionsTwo.innerChartOptions.barColor = this.dialColorComplete;
        }

        // Only worry about the "Total FYC" if this is the "leadersConference" section AND
        // this is a CA (type = "retail").

        if (this.model.get('section') === 'leadersConference'
            && this.model.get('type') === 'retail') {

            //** Total FYC Qualified **/
            var actualTotalFypcNumber = utils.isoCurrencyToNumber(
                this.model.get('actualTotalFypc'), false, true
            );
            this.model.set('actualTotalFypcNumber', actualTotalFypcNumber);

            var requiredTotalFypcNumber = utils.isoCurrencyToNumber(
                this.model.get('requiredTotalFypc'), false, true
            );

            var requiredRemainingTotalFypcNumber = utils.isoCurrencyToNumber(
                this.model.get('requiredRemainingTotalFypc'), true, true
            );
            this.model.set('requiredRemainingTotalFypcNumber', requiredRemainingTotalFypcNumber);

            var actualTotalFypcSign = utils.signOfNumber(actualTotalFypcNumber);

            this.checkMark = false;
            if (this.model.get('totalGoalAchievedFlag') === true
                && (this.model.get('leadersAprStatus') === 'FQ'
                    || this.model.get('leadersAprStatus') === 'NR')) {
                this.checkMark = true;
            }

            this.dialChartOptionsThree = {
                chartType     : 'single',
                outerValue    : actualTotalFypcNumber,
                classLabelOne : 'chart-text-one',
                classLabelTwo : 'chart-text-two lighter',
                textLabelOne  : actualTotalFypcNumber,
                textLabelTwo  : 'Total FYC',
                checkMark     : this.checkMark,
                chartOptions  : {
                    maxValue    : requiredTotalFypcNumber,
                    scaleColor  : '#ffffff',
                    centerLabel : {
                        animate         : true,
                        elementSelector : '.chart-text-one',
                        sign            : actualTotalFypcSign,
                        formatFunction  : utils.formatAsCurrency,
                        round           : true
                    }
                }
            };

            if (requiredRemainingTotalFypcNumber === 0) {
                this.dialChartOptionsThree.chartOptions.barColor = this.dialColorComplete;
                this.dialChartOptionsThree.classLabelTwo = 
                    this.dialChartOptionsThree.classLabelTwo +' oa-forest';
            }

            //*** Total FYC onSchedule ***/

            var onscheduleTotalFypcNumber = utils.isoCurrencyToNumber(
                this.model.get('onscheduleTotalFypc'), false, true
            );

            this.dialChartOptionsFour = {
                chartType     : 'single',
                outerValue    : actualTotalFypcNumber,
                classLabelOne : 'chart-text-one',
                classLabelTwo : 'chart-text-two lighter',
                textLabelOne  : actualTotalFypcNumber,
                textLabelTwo  : 'Total FYC',
                checkMark     : this.checkMark,
                chartOptions  : {
                    maxValue    : onscheduleTotalFypcNumber,
                    scaleColor  : '#ffffff',
                    centerLabel : {
                        animate         : true,
                        elementSelector : '.chart-text-one',
                        sign            : actualTotalFypcSign,
                        formatFunction  : utils.formatAsCurrency,
                        round           : true
                    }
                }
            };

            // if onscheduleRemainingTotalFypc is 0, the dial and the inner text should be green
            var onscheduleRemainingTotalFypcNumber = utils.isoCurrencyToNumber(
                this.model.get('onscheduleRemainingTotalFypc'), true, true
            );
            this.model.set(
                'onscheduleRemainingTotalFypcNumber', 
                onscheduleRemainingTotalFypcNumber
            );

            if (onscheduleRemainingTotalFypcNumber === 0 
                    || requiredRemainingTotalFypcNumber === 0) {
                this.dialChartOptionsFour.chartOptions.barColor = this.dialColorComplete;
                this.dialChartOptionsFour.classLabelTwo = this.dialChartOptionsFour.classLabelTwo +
                    ' oa-forest';
            }

        }

    }

});

module.exports = PerformanceCenterDialLCView;

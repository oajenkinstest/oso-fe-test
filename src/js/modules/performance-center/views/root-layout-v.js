/* global require, Marionette */

var rootTemplate = require('./../templates/root-t.hbs');

var RootLayout = Marionette.LayoutView.extend({
    el: '#performancecenter',
    template: rootTemplate,

    regions: {
        performanceCenterRegion : '#performance-center'
    }

});

module.exports = RootLayout;

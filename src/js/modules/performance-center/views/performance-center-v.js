/* global require, Marionette, Backbone, _ */

//load all partials and helpers
require('../partials');
require('../../../utils/hb-helpers');

var performanceCenterTemplate    = require('../templates/performance-center-t.hbs');
var PerformanceCenterModel       = require('../models/performance-center-m');

var PerformanceCenterTextView    = require('./performance-center-text-v');

var PerformanceCenterDialAPRView = require('./performance-center-dial-apr-v');
var PerformanceCenterDialLifeLivesView  = require('./performance-center-dial-lifelives-v');
var PerformanceCenterDialCTView  = require('./performance-center-dial-ct-v');
var PerformanceCenterDialLCView  = require('./performance-center-dial-lc-v');


var PerformanceCenterLayout = Marionette.LayoutView.extend({
    template: performanceCenterTemplate,
    ui : {
        performanceCenterCollapseWrapper : '#collapse-performance-center',
        performanceCenterHeading         : '#performance-center-heading',
        performanceCenterCollapseBtn     : '#performance-center-collapse-btn'
    },
    events : {
        'hide.bs.collapse @ui.performanceCenterCollapseWrapper'   : '_handlePCCollapseHide',
        'hidden.bs.collapse @ui.performanceCenterCollapseWrapper' : '_handlePCCollapseHidden',
        'show.bs.collapse @ui.performanceCenterCollapseWrapper'   : '_handlePCCollapseShow',
        'shown.bs.collapse @ui.performanceCenterCollapseWrapper'  : '_handlePCCollapseShown'
    },
    initialize: function () {

        //to set rules to view the more info / rule links
        this.showInfoLinks = false;
        if (this.options.showInfoLinks) {
            this.showInfoLinks = true;
        }

        if (this.options.model && this.options.model instanceof Backbone.Model) {
            this.model = this.options.model;
        } else {
            this.model = new PerformanceCenterModel();
        }

        this.model.on('sync', this.render, this);
        this.model.on('error', function (model, response, options) {
            throw new Error('PerformanceCenterLayout.initialize: data fetch failed.' +
                JSON.stringify(response) + JSON.stringify(options));
        });

        this.model.fetch();
    },

    onRender: function () {
        this._addPerformanceCenterRegions();
        this._addSeparator();
        this._handlePerformanceCenterCollapse();
    },

    /**
     * Method to configure Peformance Center collapse feature
     * @private
     */
    _handlePerformanceCenterCollapse : function _handlePerformanceCenterCollapse () {

        //Handling Peformance Center  collapse feature.
        // set default height (collapased) for Peformance Center Panel based on screen width
        this.pcCollapseHeight = ((window.innerWidth < 768) ? 45 : 55)+'px';
        this.ui.performanceCenterCollapseWrapper.css({
            display: 'block',
            overflow: 'hidden',
            height: this.pcCollapseHeight
        });
    },

    /**
     * Event callback method for Performance Center collapse feature
     * It will be fired while finishing collapse
     * 
     * Animate back to default height while collapsing
     * @private
    */
    _handlePCCollapseHidden : function _handlePCCollapseHidden () {
        this.ui.performanceCenterCollapseWrapper.animate({
            height: this.pcCollapseHeight
        });
    },

    _handlePCCollapseHide : function _handlePCCollapseHide () { 
        this.ui.performanceCenterCollapseWrapper.css({
            overflow: 'hidden'
        });
        this.ui.performanceCenterCollapseBtn.find('span').text('Expand');
    },

    /**
     * Event callback method for Performance Center collapse feature
     * It will be fired while finishing expand
     * 
     * Scroll position is correct for initial expand.
     * But for frequent expand, scroll position is moved to bottom 
     * So scroll to PC panel if body scroll position is greater than panel offset
     * @private
    */
    _handlePCCollapseShown : function _handlePCCollapseShown () {
        var panelOffsetTop          = this.ui.performanceCenterCollapseWrapper.offset().top;
        var currentScrollPosition   = Backbone.$(window).scrollTop();

        // Change overflow property to visible state, otherwise floating contents
        // such as tooltip will trimmed off.
        this.ui.performanceCenterCollapseWrapper.css({
            overflow: 'inherit'
        });

        // Adding 15px to header height, as height function ignore margin value to
        // calculate height.
        var pcHeadingHeight         = this.ui.performanceCenterHeading.height()+15;

        // reduce height of Performance Center header from panelOffsetTop, 
        // Otherwise heading will be cut of while scrolling up
        if (currentScrollPosition > panelOffsetTop) {
            Backbone.$('html, body').animate({
                scrollTop: panelOffsetTop - pcHeadingHeight
            }, 500);
        }
    },

    _handlePCCollapseShow : function _handlePCCollapseShow () { 
        this.ui.performanceCenterCollapseBtn.find('span').text('Collapse');
    },

    /**
     * Add a horizontal rule to separate items in first two rows (OOSO-3376)
     * @private
     */
    _addSeparator : function _addSeparator () {
        var firstRow = this.$el.find('.row:first');
        var rowCount = this.$el.find('.row').length;
        var hr       = '<hr class="margin-top-30 margin-btm-40 hidden-xs">';

        if (rowCount > 1) {
            Backbone.$(hr).insertAfter(firstRow);
        }
    },

    /**
    * Add regions dynamically based on availability of data
    */
    _addPerformanceCenterRegions: function () {
        var _this =  this;

        //iterate model object and get section objects to pas to view
        _.each(this.model.toJSON(),function(sectionObjects, key){

            // accept only objects to draw regions
            if (!_.isObject(sectionObjects)) {
                return;
            }

            //set showInfoLinks flag to all childViews
            sectionObjects.showInfoLinks = _this.showInfoLinks;

            //create div elements to add region for layout
            _this._addColumn(key, sectionObjects.viewType);

            //add region to layout
            _this.addRegion(key+'Region', '#'+key+'-region');

            //choose view for each type section
            if (key ==='fyc' 
                    || key === 'fypc' 
                    || key === 'qualifyingFyc' 
                    || key === 'qualifyingFypc') {
                _this.showChildView(key+'Region',
                    new PerformanceCenterTextView({
                        model: new Backbone.Model(sectionObjects)
                    })
                );

            } else if (key === 'lifeLives'){
                _this.showChildView(key+'Region',
                    new PerformanceCenterDialLifeLivesView({
                        model: new Backbone.Model(sectionObjects)
                    })
                );
            } else if (key ==='chairmansTrip') {
                _this.showChildView(key+'Region',
                    new PerformanceCenterDialCTView({
                        model: new Backbone.Model(sectionObjects)
                    })
                );
            } else if (key === 'leadersConference') {
                _this.showChildView(key+'Region',
                    new PerformanceCenterDialLCView({
                        model: new Backbone.Model(sectionObjects)
                    })
                );
            } else if (key === 'annualProductionRequirements') {
                _this.showChildView(key+'Region',
                    new PerformanceCenterDialAPRView({
                        model: new Backbone.Model(sectionObjects)
                    })
                );
            }
        });
    },

    /**
    * Create column to map with layout region for each section
    * @param {string} columnId
    */
    _addColumn: function (columnId, viewType) {

        if (!columnId) {
            return false;
        }

        var column = 3;
        var rowHTML = '<div class="row" ></div>';
        var columnHTML = '<div class="col-sm-4" id="'+columnId+'-region" ></div>';
        var lastRowElement = this.$el.find('.row:last');

        // start new row if column count reach at maximum (3)
        // or current viewType is different from previous one (text vs dial)
        if(!lastRowElement.length 
                || lastRowElement.children('.col-sm-4').length >= column 
                || viewType !== this.previousBoxViewType) {
            this.ui.performanceCenterCollapseWrapper.append(rowHTML);
        }

        //update row element
        lastRowElement = this.$el.find('.row:last');

        lastRowElement.append(columnHTML);
        this.previousBoxViewType = viewType;
    }

});

module.exports = PerformanceCenterLayout;

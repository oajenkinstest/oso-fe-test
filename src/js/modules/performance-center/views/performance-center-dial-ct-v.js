/* global require */

/**
* Extending Performance center dial chart Base view for Chairmans Trip (CT) 
*/
var config = require('../../../config/config');
var utils  = require('../../../utils/utils');
var PerformanceCenterDialBaseView = require('./performance-center-dial-base-v');
var performanceCenterDialCTTemplate 
    = require('../templates/performance-center-dial-ct-t.hbs');

var PerformanceCenterDialCTView = PerformanceCenterDialBaseView.extend({
    pdfLink : {
        ib     : config.salesForceBaseUrl +
        'sfc/p/300000006oEE/a/0c000000Ee9s/6dtOv1xmgko.7FpRNIEzWNLH5br6N2x6ICAtrF2IasI',
        retail : config.salesForceBaseUrl +
            'sfc/p/#300000006oEE/a/0c000000EKVF/qBUak8DSkXfUx.nJOehdG2s58t1DhOAGucGFCUkJcNA'
    },
    template: performanceCenterDialCTTemplate,
    buildDialChartOptions: function () {
        // Set the PDF link
        this._setRulesHref(this.model.get('type'));

        //*** Qualifying FY(P)C Qualified ***/
        var actualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('actualQualFypc'), false, true
        );
        this.model.set('actualFypcNumber', actualFypcNumber);

        var onscheduleRemainingQualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('onscheduleRemainingQualFypc'), true, true
        );
        this.model.set('onscheduleRemainingQualFypcNumber', onscheduleRemainingQualFypcNumber);

        var requiredFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('requiredQualFypc'), true, true
        );

        var requiredRemainingQualFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('requiredRemainingQualFypc'), true, true
        );
        this.model.set('requiredRemainingQualFypcNumber', requiredRemainingQualFypcNumber);

        //check mark
        this.checkMark = this.model.get('qualifyingGoalAchievedFlag');

        this.dialChartOptionsOne = {
            chartType: 'double',
            outerValue:actualFypcNumber,
            innerValue:this.model.get('actualLives'),
            classLabelOne: 'chart-double-text-one',
            classLabelTwo: 'chart-double-text-two',
            textLabelOne: utils.formatAsCurrency(actualFypcNumber),
            textLabelTwo: this.model.get('actualLives')+ ' lives',
            checkMark: this.checkMark,
            outerChartOptions: {
                maxValue   : requiredFypcNumber,
                scaleColor : '#ffffff'
            },
            innerChartOptions: {
                maxValue   : this.model.get('requiredLives'),
                scaleColor : '#ffffff'
            }
        };

        // When checkMark is false, check to see if inner/outer ring should be green
        if (!this.checkMark) {
            if (requiredRemainingQualFypcNumber === 0) {
                this.dialChartOptionsOne.outerChartOptions.barColor = this.dialColorComplete;
            }

            if (this.model.get('requiredRemainingLives') === 0) {
                this.dialChartOptionsOne.innerChartOptions.barColor = this.dialColorComplete;
            }
        }

        //*** Qualifying FY(P)C onSchedule ***/
        var onScheduleFypcNumber = utils.isoCurrencyToNumber(
            this.model.get('onscheduleQualFypc'), true, true
        );

        this.dialChartOptionsTwo = {
            chartType: 'double',
            outerValue:actualFypcNumber,
            innerValue:this.model.get('actualLives'),
            classLabelOne: 'chart-double-text-one',
            classLabelTwo: 'chart-double-text-two',
            textLabelOne: utils.formatAsCurrency(actualFypcNumber),
            textLabelTwo: this.model.get('actualLives')+ ' lives',
            checkMark: this.checkMark,
            outerChartOptions: {
                maxValue:onScheduleFypcNumber,
                scaleColor: '#ffffff'
            },
            innerChartOptions: {
                maxValue:this.model.get('onscheduleLives'),
                scaleColor: '#ffffff'
            }
        };

        // When checkMark is false, check to see if inner/outer ring should be green
        if (!this.checkMark) {

            if (this.model.get('onscheduleRemainingQualFypcNumber') === 0) {
                this.dialChartOptionsTwo.outerChartOptions.barColor = this.dialColorComplete;
            }

            if (this.model.get('onscheduleRemainingLives') === 0) {
                this.dialChartOptionsTwo.innerChartOptions.barColor = this.dialColorComplete;
            }
        }
    },

    /**
     * Set the link to the "Rules" PDF based on `type`.
     * @private
     */
    _setRulesHref : function _setRulesHref(type) {
        var link = this.pdfLink[type];
        this.model.set('pdfLink', link);
    }

});

module.exports = PerformanceCenterDialCTView;

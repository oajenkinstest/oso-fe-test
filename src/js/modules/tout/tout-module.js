
var ToutModel = require('./models/tout-m');
var ToutView = require('./views/tout-v');

var toutModule = {

    createTout: function(toutObj) {
        var toutModel = new ToutModel(toutObj);
        return new ToutView({model: toutModel});
    },

    repeat: function (input) {
        var strInput = input.toString();
        return strInput + strInput;
    }
    
};

module.exports = toutModule;

/* global Marionette, _ */

var ToutTemplate = require('../templates/tout-t.hbs');
var ToutModel    = require('../models/tout-m');

// Load handlebars helper method to avoid unit test failure
require('../../../utils/hb-helpers');

var ToutView = Marionette.ItemView.extend({

    template: ToutTemplate,

    errors : {
        objectRequired : 'ToutView requires options passed to constructor to be an object'
    },

    initialize : function(options) {
        
        if (_.isEmpty(options) || !_.isObject(options)) {
            throw new Error(this.errors.objectRequired);
        } 
        
        this.model = new ToutModel(options);
    }

});

module.exports = ToutView;


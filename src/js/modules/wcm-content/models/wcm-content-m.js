/* global Backbone: false, _:false */
/**
 * Implementation of wcm content model.
 *
 * WCM contents are loaded from connectServlet in html format. 
 * There might chance for some changes to templates in future
 */

var config               = require('../../../config/config');
var localStorageMemory   = require('../../../modules/localStorageMemory/localStorageMemory');

var userChannel = Backbone.Radio.channel('user');

var WCMModel = Backbone.Model.extend({

    cache : {
        cacheStore : localStorageMemory,
        ttl        : (config && config.cache && config.cache.wcmTtl) ? config.cache.wcmTtl : 0.0
    },

    errors : {
        addParamsNoString : 'Parameter passed to _addParams is required and must be a string'
    },

    defaults: function() {
        return {
            body: null,

            // connectServlet URL to fetch the HTML content.
            // eg: OnlineServices/Marketing/ELEVATE
            wcmPath: null
        };
    },

    // setting default option for 'dataType' as 'html'
    fetch: function(options){
        options = _.extend(options || {}, {
            dataType: 'html',
            cache      : true,
            cacheTTL   : this.cache.ttl,
            localCache : this.cache.cacheStore,
        });
        this.constructor.__super__.fetch.call(this, options);
    },


    /**
     * Parse the HTML response to set body content .
     *
     * @param {string} data - The HTML data returned from the wcm connectServlet
     * @returns {body: string, wcmPath: string}
     */
    parse: function(data) {
        var body    = '';
        var wcmPath = this.get('wcmPath');

        // set data returned as body content
        if (data) {
            body = data;
        }

        return {
            body    : body,
            wcmPath : wcmPath
        };
    },

    /**
     * Build the URL to fetch the content. Appends the
     * "wcmRole" query parameter which gets sent to WCM.
     * @returns {string}
     */
    url: function() {
        var wcmPathWithParams = this._addParams(this.get('wcmPath'));

        return [
            config.wcmBaseUrl,
            wcmPathWithParams
        ].join('');
    },

    /**
     * Adds the wcmRole parameter while keeping existing query
     * strings.
     * @param uriString
     * @returns {string} URI with query params if they exist. If query params do not exist, only the
     * URI passed to the function is returned.
     * @private
     */
    _addParams : function _addParams(uriString) {
        var queryString;
        var params = {};
        var wcmRoleValue;
        var urlBase;

        if (!_.isString(uriString)) {
            throw new Error(this.errors.addParamsNoString);
        }

        urlBase = uriString.indexOf('?') === -1 ? uriString : uriString.split('?')[0];
        wcmRoleValue = this._getWcmRole();

        queryString = uriString.split('?')[1];
        params      = Backbone.$.deparam(queryString ? queryString : '');

        // Add wcmRole to params if it exists.
        if (wcmRoleValue) {
            params.wcmRole = wcmRoleValue;
        }

        if (_.isEmpty(params)) {
            return urlBase;
        } else {
            return urlBase + '?' + Backbone.$.param(params);
        }
    },

    /**
     * Returns a role based on capabilities which
     * is ultimately passed to WCM as a query parameter (e.g. "wcmRole")
     * in order to determine the content to display to the user.
     *
     * Note that because of some constraints in WCM, the
     * actual role must be prefixed with some additional text.
     *
     * {@link https://oneamerica.atlassian.net/browse/OOSO-3662}
     * @returns {string|null} Role (e.g. "/inddesign/TAX-Roles/Home_Office",
     * "/inddesign/TAX-Roles/Retail_Producer", "/inddesign/TAX-Roles/IB_Manager", etc.)
     */
    _getWcmRole : function _getWcmRole () {
        if (userChannel.request('hasCapability', 'Home_Office')) {
            return config.wcmRolePrefix + 'Home_Office';
        } else if (userChannel.request('hasCapability', 'Pathway_IB_Manager_View')) {
            return config.wcmRolePrefix + 'IB_Manager';
        } else if (userChannel.request('hasCapability', 'Pathway_IB_Producer_View')) {
            return config.wcmRolePrefix + 'IB_Producer';
        } else if (userChannel.request('hasCapability', 'Pathway_Retail_Manager_View')) {
            return config.wcmRolePrefix + 'Retail_Manager';
        } else if (userChannel.request('hasCapability', 'Pathway_Retail_Producer_View')) {
            return config.wcmRolePrefix + 'Retail_Producer';
        } else if (userChannel.request('hasCapability', 'Contact_List_CS_View')) {
            // Care Solutions/IB Producer roles - OOSO-3818

            if (userChannel.request('hasCapability', 'Producer_Role_View')) {

                if (userChannel.request('hasCapability', 'WCM_All_But_Bank_View')) {
                    return config.wcmRolePrefix + 'CS_Producer';
                } else {
                    return config.wcmRolePrefix + 'CS_Bank_Producer';
                }

            }

            if (userChannel.request('hasCapability', 'Producer_Manager')) {

                if (userChannel.request('hasCapability', 'WCM_All_But_Bank_View')) {
                    return config.wcmRolePrefix + 'CS_Manager';
                } else {
                    return config.wcmRolePrefix + 'CS_Bank_Manager';
                }
            }

            return null;

        } else if (userChannel.request('hasCapability', 'Contact_List_IB_View') &&
            userChannel.request('hasCapability', 'Producer_Role_View')) {
            return config.wcmRolePrefix + 'IB_Producer';
        } else {
            return null;
        }
    }
});

module.exports = WCMModel;

/* global Backbone:false, Marionette:false, _:false */
/**
 * View to handle the WCM content. 
 * To render this view below options are mandatory
 *
 * @param {string} options.el - jquery element selector to place the HTML content
 * @param {string} options.wcmPath - path to load the HTML content
 *
 * wcmPath will be passed to wcmModel object to fetch html content.
 * Once html content got fetched, view will start rendering.
 * 
 * Created by jbell on 1/20/16.
 */
var config          = require('../../../config/config');
var errorHelper     = require('../../../utils/error-helper');
var wcmTemplate     = require('../templates/wcm-content-t.hbs');
var WcmModel        = require('../models/wcm-content-m');
var utils           = require('../../../utils/utils');

var analyticsChannel = Backbone.Radio.channel('analytics');
var spinnerChannel   = Backbone.Radio.channel('spinner');

// load the global partials.
require('../../../partials');

var WcmView = Marionette.ItemView.extend({

    template : wcmTemplate,

    errors : {
        wcmPathMissing      : 'WcmView.initialize() requires wcmPath to show the content',
        genericErrorMessage : 'An error occurred while processing this request.'
    },

    ui : {
        'jumpLinksContainer'              : '.jumplinks-traveling',
        'jumpLinksDropdownMenu'           : '.jumplinks-traveling .btn-group',
        'jumpLinks'                       : '.jumplinks-traveling ul a',
        'responsiveInstructionContainers' : '.table-responsive-instruction'
    },

    events : {
        'click @ui.jumpLinks' : 'bindScrollToWithJumpLinks'
    },

    behaviors : {
        floatTableHeaders : {},
        jumpLinks         : {},
        smartScrollTables : {},
        wcmContentLinks   : {},
        wcmContentGrid    : {},
        wcmModalWindow    : {}
    },

    initialize: function(options) {

        var wcmPath;
        var matchWcmPath;

        // Setting utils and ajaxUtils function to local scope for unit testing
        this.utils     = require('../../../utils/utils');
        this.ajaxUtils = require('../../../utils/ajax-utils');
       
        if (options && typeof options === 'object') {
            // check to see if wcmLink is set first
            if (options.stateObj && typeof options.stateObj.wcmLink === 'string') {
                wcmPath = options.stateObj.wcmLink;
            } else if (typeof options.wcmPath === 'string') {
                wcmPath = options.wcmPath;
            }
        }

        // if wcmPath has not been set, check in the location.hash
        if (_.isUndefined(wcmPath) && document.location.hash) {

            //get WCM path from location.hash if it exactly match (start #c/)
            matchWcmPath = document.location.hash.match(/^#c\/(.*)$/);
            if (matchWcmPath && matchWcmPath.length) {
                wcmPath = matchWcmPath[1];
            }
        }

        if (!wcmPath) {
            throw new Error(this.errors.wcmPathMissing);
        }

        this.model = new WcmModel({
            wcmPath: wcmPath
        });

        this.listenTo(this.model, 'error', this._handleWcmError);
        this.listenTo(this.model,'change', this.render);
    },

    onBeforeShow : function () {
        
        //show wait indicator
        spinnerChannel.trigger('show', {
            viewScope : this,
            position  : 'fixed'
        });

        this.model.fetch();
    },

    onRender : function () {
        var _this = this;
        spinnerChannel.trigger('hide', this);
                
        utils.convertWcmLinks(this);

        // Hide all .table-responsive-instruction in WCM content by default. The WCM authors
        // have added these for tables without providing the associated table with a
        // "table-responsive" wrapper. Without this wrapper, scroll bars won't appear
        // for the table. If a table does, in fact, have scroll bars, the
        // smartScrollTables behavior will show the message when/if needed.
        this.ui.responsiveInstructionContainers.addClass('hidden');

        if (this.ui.jumpLinksContainer instanceof Backbone.$ 
                && this.ui.jumpLinksContainer.length) {

            // Adding anchor element just before of jumplink menu container and set an UI element
            this.ui.jumpLinksContainer.before('<a id="anchor-jumplink-menu" ></a>');
            this.ui.anchorJumpLinksMenu = Backbone.$('#anchor-jumplink-menu');

            // Duplicating jumplinksContainer to keep visibility of jump links
            // while existing jumplink menu transformed into affix menu, which avoid content jumps
            var jumpLinksContainerCopy = this.ui.jumpLinksContainer.clone();
            jumpLinksContainerCopy.removeClass('jumplinks-traveling')
                .addClass('affix-top jumplinks-traveling-copy')
                .prop('id', 'jumplinka-traveling-copy')
                .hide();
            this.ui.anchorJumpLinksMenu.after(jumpLinksContainerCopy);
            this.ui.jumpLinksContainerCopy = Backbone.$('.jumplinks-traveling-copy');
            
            this.ui.jumpLinksDropdownMenu.on('shown.bs.dropdown', function () {
                _this.ui.jumpLinksContainer.addClass('open');
            });

            this.ui.jumpLinksDropdownMenu.on('hidden.bs.dropdown', function () {
                _this.ui.jumpLinksContainer.removeClass('open');
            });
        }
    },

    onDestroy: function() {

        //unbind resize.app.jumpLinks event while destroying this view
        Backbone.$(window).off('resize.app.jumpLinks');
    },

    /**
     * Bind scrollTo method with each jump links  
     * @param {object} event - jquery event object
     */
    bindScrollToWithJumpLinks : function bindScrollToWithJumpLinks (event) {
        event.preventDefault();

        // For better positioning we need to send additional params to 
        // scrollTo method to subtract height (40) of the Affix button from actual offset.top
        // Otherwise button will overlap with Header.
        this.utils.scrollTo(Backbone.$(event.currentTarget.hash), 40);
    },

    /**
    * Bind affix plugin to JumpLink container based on window size
    */
    _affixJumpLinksContainer : function _affixJumpLinksContainer() {

        if (this.ui.jumpLinksContainer instanceof Backbone.$
                && this.ui.jumpLinksContainer.length) {
            var _this = this;
            var jumpLinksContainerHeight = this.ui.jumpLinksContainer.height();
            
            // Remove affix data if that exist, so re-bind work correctly 
            // while resizing window
            Backbone.$(window).off('.affix');
            this.ui.jumpLinksContainer.removeData('bs.affix')
                .removeClass('affix affix-top');

            _this.ui.jumpLinksContainer.affix({
                offset: {

                    // set top offset to start showing jumplink menu
                    // currently it will be displayed when scroll reach near by top offset of
                    // anchor element added before jumpmenu
                    top: _this.ui.anchorJumpLinksMenu.offset().top + jumpLinksContainerHeight 
                }
            });

            _this.ui.jumpLinksContainer.off('affixed.bs.affix')
                .on('affixed.bs.affix', function () {
                    _this.ui.jumpLinksContainerCopy.show();
                });
            
            _this.ui.jumpLinksContainer.off('affixed-top.bs.affix')
                .on('affixed-top.bs.affix', function () {
                    _this.ui.jumpLinksContainerCopy.hide();
                });
        }
    },

    onDomRefresh : function onDomRefresh () {

        if (this.ui.jumpLinksContainer instanceof Backbone.$ 
                && this.ui.jumpLinksContainer.length) {

            // Set 'resize' event listener with callback method to re-initialize affix plugin   
            // 
            // Its suppose to do at initialization of view but getting failed in unit testing
            // as it is not passing current reference to `this` context 
            Backbone.$(window).on('resize.app.jumpLinks',
                _.bind(this._affixJumpLinksContainer, this));

            // Initial call to bind affix plugin with jump links container
            Backbone.$(window).trigger('resize.app.jumpLinks');

            // track 'top' offset change of jumplink anchor element and re-position affix element
            this.elPositionChangeListener = this.ui.anchorJumpLinksMenu
                .onPositionChanged(_.bind(this._affixJumpLinksContainer, this), 100);

            this.currentPageOffsetTop = Backbone.$(window).scrollTop();
            
            // Adding scroll and touchmove(only for mobile) to re-initialize JumpLink affix menu
            // Its to re-initialize affix plugin with new top offset property 
            // only if there any change to position of jumplink anchor element for first time.
            Backbone.$(window).on(
                'scroll.wcmContent, touchmove.wcmContent', 
                _.debounce(_.bind(this._unbindPageScrollListener, this),100)
            );
        }
    },

    /**
     * Unbind page scroll/touchmove event bound with window
     */
    _unbindPageScrollListener : function _unbindPageScrollListener () {

        // re-initialize JumpLink menu while scrolling for once
        Backbone.$(window).trigger('resize.app.jumpLinks');

        // Ignore initial scroll which will be auto triggered by browser
        // while refresh
        if (this.currentPageOffsetTop !== Backbone.$(window).scrollTop()) {
            
            //unbind scroll.wcmContent event too
            Backbone.$(window).off('scroll.wcmContent, touchmove.wcmContent');
        } 
    },

    /**
     * Logs an error message to analytics providers, hides
     * the spinner, and displays a message to the user.
     * @param {object} model
     * @param {object} response
     * @private
     */
    _handleWcmError : function _handleWcmError(model, response) {
        var logMessage;
        var userMessage;

        if (response && response.status) {

            // Keep the error messages consistent with the global error handling
            if (response.status === 404) {
                userMessage = config.errorMessages.noPageFound;
            } else {
                userMessage = this.ajaxUtils.getAjaxMessages(response, null).userMessage ||
                    this.errors.genericErrorMessage;
            }

            logMessage = 'Error retrieving WCM content from ' + this.model.get('wcmPath')
                + '. Received a ' + response.status + ' status code.';

            // Stop the spinner
            spinnerChannel.trigger('hide', this);

            analyticsChannel.trigger('trackException', {
                fatal   : false,
                message : logMessage
            });

            // Display an error message to the user with an info icon for 403
            // and 404 errors and a warning icon for everything else
            if (response.status === 403 || response.status === 404) {
                this.model.set(
                    'alertMessage',
                    errorHelper.createAlert(userMessage, 'info')
                );
                this.render();

            } else {
                this.model.set(
                    'alertMessage',
                    errorHelper.createAlert(userMessage, 'warning')
                );
                this.render();
            }
        }
    }
});

module.exports = WcmView;
# Frontend Architecture for Access Control

## Summary

OSO's web service provides a list of "capabilities" -- strings that identify specific actions that a specific user may perform. The OSO client uses the capabilities list to control which navigation items and user interface components are displayed to the user.


## The scope of this document

This document describes the frontend architecture in sufficient detail to implement features that need to know about user access control, even though the final details will change as the OSO project progresses.
The details should be encapsulated as much as possible to simplify the inevitible rework tasks.

## Retrieving a user's capabilities

The client obtains details about the user from the RESTful web service. The service uses the Siteminder cookie to determine the user's identity.

GET https://api.oneamerica.com/oso/secure/rest/user

The response object will look something like this:

    {
        "webId"        : "123abc",
        "displayName"  : "Joe T Smith",
        "capabilities" : [
            "user.get",
            "user.getCapabilities",
            "user.password.update",
            "user.securityinfo.update",
            "user.statusemailpreferences.get",
            "user.statusemailpreferences.update",
            "user.surrogates.list",
            "user.surrogates.get",
            "user.surrogates.update",
            "user.surrogates.add",
            "user.surrogates.delete",
            "pendingpolicies.get",
            "pendingpolicies.insert",
            "pendingpolicies.update",
            "pendingpolicies.list",
            "inforcepolicies.get",
            "inforcepolicies.list",
            "inforcepolicies.update",
            "etc",
            "etc",
            "etc"
        ]
    }


This response object is used to construct an instance of UserModel (`models/user-m.js`), which has these properties:

* webId (string) - The ID used to
* displayName (string)
* capabilities (array of strings)

The specific capabilities are still being determined, as is the format of the text string to be used. So far, capabilities include:

* Ability to view the performance center
* Ability to search for policies
* Ability to search for producers

### Unimplemented Exception - Unauthenticated user
If the user is not yet authenticated, the service should return a status code 401 and the client should redirect the user to the OneAmerica.com login page. This should be implemented in [OOSO-1289](https://oneamerica.atlassian.net/browse/OOSO-1289).


## Conditional code: `user.hasCapability()` method

UserModel has a `.hasCapability()` function to check the presence of a capability:

    var myUser = new UserModel(configObj);
    ...
    if (myUser.hasCapability('pendingpolicies.list')) {
        // display the list of pending policies
    }

## Conditionals in Handlebars templates

Instead of using a custom handlebars helper to check capabilities, OSO uses view models. The initialization (and potentially) change listeners within the view model check the user's capablities, and set flags in the view model to be checked by the standard `{{#if}}` and `{{#unless}}` helpers.

The `pending-search-form-vm.js` view model and the `pending-search-form-t.hbs` template provide a good example. The view model has a `_setCapabilities` function to set the `hasPendingSearchProducerSearch` flag in the view model. The template checks the flag to determine whether or not to show instruction messages and controls.

## Navigation

Most users will not have access to every possible page in the OSO client. The user's capability list is used to filter out navigation items that the user is not authorized to see.

Each navigation item is associated with a capability field in a configuration object that defines the navigation structure. Here is a simplified example:

    var navConfig = [
        {
            "displayText" : "User Tasks",
            "subItems"    : [
                {
                    "capability"  :{
                        forSidebar : [
                            {
                                "home.user : true,
                                "user.producer : true
                            }, {
                                "Pathway_View_User"   : true,
                                "Pathway_Retail_User" : false
                            }
                        ],
                        forPageAccess : {
                            "user.password.update" : true
                        }
                    },
                    "displayText" : "Change Password",
                    "link"        : "something"
                },{
                    "capability"  : {
                        forSidebar : [{
                            "user.securityinfo.update" : true
                        }]
                    },
                    "displayText" : "Change Security Questions",
                    "link"        : "something"
                },{
                    "capability"  : {
                            forSidebar : [{
                                "user.statusemailpreferences.update" : true
                            }]
                        },
                    "displayText" : "Status Email Preferences",
                    "href"        : "something"
                },{
                    "capability"  : {
                        forSidebar :  [{
                            "user.surrogates.get" : true
                        }]
                    },
                    "displayText" : "Surrogates",
                    "link"        : "something"
                }
            ]
        },
        {
            "capability"  : "applications.get",
            "displayText" : "Policy Applications",
            "subItems"    : [
                {    
                    "capability"  : {
                        forSidebar : [{
                            "applications.add" : true
                        }]
                    },
                    "displayText" : "New Application",
                    "link"        : "something"
                },{
                    "capability"  : {
                        forSidebar : [{
                            "applications.list" : true
                        }]
                    },
                    "displayText" : "Pending Applications",
                    "link"        : "something"
                }
            ]
        },
        {
            "capability"   : {
                    forSidebar: [{
                        "inforcepolicies.get" : true
                    }]
            },
            "displayText"  : "In-force Policies",
            "subItems"     : [
                {    
                    "capability"  : {
                        forSidebar : [{
                            "inforcepolicies.search": true
                        }]
                    },
                    "displayText" : "Search",
                    "link"        : "policy-search"
                },{
                    "capability"  : {
                        forSidebar : [{
                            "inforcepolicies.detail": true
                        }]
                    },
                    "displayText" : "Policy detail",
                    "link"        : "policy-detail",
                    "activeLink"  : "policy-search"
                },{
                    "capability"  : {
                        forSidebar : [{
                            "inforcepolicies.list" : true
                        }]
                    },
                    "displayText" : "Browse All",
                    "link"        : "something"
                }
            ]
        }
    ];


To filter out navigation items, call the `.filterForSidebar()` function on the appStructure module with the user's capability list. The code might look something like this:

    var fullAppStructure = new AppstructureModel();
    this.appStructure = fullAppStructure.filterForSidebar(userCapabilities);

The navigation item with `hidden` attribute will be removed while fitering with `filterForSidebar` method.

The navigation item with attribute `activeFor` (An array object) contain list `pageID`s. While user viewing a page with `pageId` which existing in that list, `setActiveItem`(A method to highlight active menu item under sideBar which is defined at `sideBarApp`) will be used current navigation item to highlight.

Capability attribute - An object which should have at least one or two of below properties.
        
        1. forSidebar (optional) 
                   
            An array of properties considered to display item on sidebar. If it does
            not exist, it will be ignored.
                
            Each element of the array will contain one or more capabilities, which need to match
            with user capabilities to display on sidebar and to get access to 
            that particular view.
            
            Capability 'all' mean, every user has access to that page.
            
        2. forPageAccess (optional) 
            If any user has any additional capabilities can be included here.
            Which will be considered while access check is failed against forSidebar
            property.


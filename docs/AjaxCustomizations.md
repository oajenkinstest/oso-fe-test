## Ajax Customizations

The `ajax-utils.js` file contains functions to set up the various customizations for OSO. The initialization of these customizations lives in the osoApp.js module.

### Impersonation
The OSO app allows impersonation - a logged in user can view the site as another user. The services rely on the `targetuser` query string parameter to indicate the impersonated user.  This property is the user's Elad ID (also commonly known as web ID and global ID). It is a numeric ID.
In order to make sure this parameter gets added for all calls, there are functions within `userModule.js` to assist:

`beginImpersonation(producerInfo)` sets a `impersonatedWebId` property in the `userModule` fetches a new `userModel` and triggers an event to `osoApp`.

`endImpersonation()` sets the `impersonatedWebId` property in the `userModule` to null.

`getImpersonatedWebId()` returns the `impersonatedWebId` property. If it is not falsy, then the user is impersonating the user specified.

The `buildPrefilter` function in `ajax-utils.js` returns a function to be used as jQuery's prefilter. This function will call `getImpersonatedWebId()` to check if the user is impersonating another. If so, it will add a `targetuser` param to the query string of the request.


### Cross-Origin Resource Sharing
The `buildPrefilter` function in `ajax-utils.js` returns a function to be used as jQuery's prefilter. It adds properties to ajax requests to allow for CORS.

### Error Handling
There are a few functions involved in error handling.

First, the `beforeSend` function adds the request url to the jqXHR object. This is necessary in order to log the url in the error handler as it will not have the URL in any closure context.
Next, the `buildErrorHandler` function creates a function to be used as the global ajax error handler. It takes a function to be used as a logout callback as a parameter.
Finally, the `getAjaxMessages` function inspects the jqXHR object and creates messages to be displayed to the user and logged via the debugModule. The function created by `buildErrorHandler` uses this function.

#### Testing Error Handling
You can test the basic error handling of RESTful calls using the localdata-dev project. To do so, add a `fail` property to the endpoint you would like to test. The value of the property should equal the error code you would like to return. If there is a file within the endpoint's folder that matches the error code (ie, 503.json for fail = 503) the localdata-dev server will return that file as the response. If not, it will look for an error file in the `osoPrj/defaultErrors` folder. If there is no error file there, the server will return a generic error.

The mappings.js section should look like this:
```
helloWorld: {
    route: config.osoBaseUrl + '/greeting',
    folder: 'hell0',
    fail: 503
}
```

The 503.json file within the `/osoPrj/hello` or `/osoPrj/defaultErrors` folder:
```
{
    "message": "Fail property set to 503, forcing Service Unavailable error."
}
```
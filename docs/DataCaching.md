# Data Caching in OneSource Online

## Implementation

Data caching in OSO is managed using three modules:

 * `src/vendor/jquery-ajax-localstorage-cache.js` - (JALC) Third-party utility used to cache jQuery ajax responses (see https://github.com/SaneMethod/jquery-ajax-localstorage-cache). The OSO front-end team has slightly modified the `genCacheKey()` method. The key used to cache data is the HTTP verb used in the request (e.g. `GET`) followed by a colon (:) and then the URI (including query string). The following is an example of a key used in the cache for client search:

 ```
 GET:/api/oso/secure/rest/policies?statusView=pending&customerName=a&columns%5B0%5D.data=customerLexicalName&columns%5B0%5D.name=customerName&columns%5B0%5D.searchable=true&columns%5B0%5D.orderable=true&columns%5B0%5D.search.value=&columns%5B0%5D.search.regex=false&columns%5B1%5D.data=dateOfBirth&columns%5B1%5D.name=dateOfBirth&columns%5B1%5D.searchable=true&columns%5B1%5D.orderable=true&columns%5B1%5D.search.value=&columns%5B1%5D.search.regex=false&columns%5B2%5D.data=relationship&columns%5B2%5D.name=relationship&columns%5B2%5D.searchable=true&columns%5B2%5D.orderable=true&columns%5B2%5D.search.value=&columns%5B2%5D.search.regex=false&columns%5B3%5D.data=policyNumber&columns%5B3%5D.name=policyNumber&columns%5B3%5D.searchable=true&columns%5B3%5D.orderable=true&columns%5B3%5D.search.value=&columns%5B3%5D.search.regex=false&columns%5B4%5D.data=productName&columns%5B4%5D.name=productName&columns%5B4%5D.searchable=true&columns%5B4%5D.orderable=true&columns%5B4%5D.search.value=&columns%5B4%5D.search.regex=false&columns%5B5%5D.data=receivedDate&columns%5B5%5D.name=receivedDate&columns%5B5%5D.searchable=true&columns%5B5%5D.orderable=true&columns%5B5%5D.search.value=&columns%5B5%5D.search.regex=false&columns%5B6%5D.data=policyStatus.description&columns%5B6%5D.name=policyStatus&columns%5B6%5D.searchable=true&columns%5B6%5D.orderable=true&columns%5B6%5D.search.value=&columns%5B6%5D.search.regex=false&order%5B0%5D.column=0&order%5B0%5D.dir=asc&start=0&length=25&search.value=&search.regex=false
 ```
 * `src/js/modules/localStorageMemory/localStorageMemory.js` - A singleton which implements the [storage interface](https://developer.mozilla.org/en-US/docs/Web/API/Storage)
 * `src/js/config/config.js` - The configuration has a `cache` object defined for each environment which controls the TTL for both data retrieved from the server (`cache.ttl`) and content retrieved from WCM (`cache.wcmTtl`).

Currently, the OneAmerica Security team has forbidden the use of traditional client caching (e.g. Web Storage) for OSO. In order to allow data from the service and WCM to be cached while avoiding the use of modern caching mechanisms for the browser, the `localStorageMemory.js` module uses an in-memory store. This makes the cache very volatile -- simply refreshing the page will clear the cache.

### Implementing with DataTables

Implementing caching with DataTables is relatively straight-forward. An example of this can be seen in `src/js/pages/pending/views/client-name-search-results-v.js`.

When initializing the table, use the [JALC parameters](https://github.com/SaneMethod/jquery-ajax-localstorage-cache#parameters) in the jQuery `ajax` object. *Note*: The `localCache` parameter should be set to the `localStorageMemory` module (rather than a boolean value) and the cacheTTL should be set to use the value stored in the `cache.ttl` object of `config.js`.

In order for DataTables to use this data, a [`dataSrc` method](https://datatables.net/reference/option/ajax.dataSrc) should be configured. The `dataSrc` method is used to manipulate data retrieved from an Ajax call before it is displayed in the DataTables table.

There are some potential issues when using this cached data with DataTables. When the cached data is used and displayed, none of the [DataTables events](https://datatables.net/reference/event/) are fired -- including the "column-sizing" event which typically fires when the table is simply re-sized.

### Implementing with Web Content Manager (WCM)

Caching for WCM content has been implemented in `sr/js/modules/wcm-content/models/wcm-content-m.js`. Any WCM content which is displayed within OSO will be cached as a result. As new WCM content items are added to OSO, the markup sent from the server will be cached.

### View as Producer/Delegate

When a user is viewing OSO as a producer/delegate, caching is NOT enabled. The reason for this is that the `targetuser` parameter is added to the `options.data` in `src/js/utils/ajax-utils.js` using the `buildPrefilter()` method after the cache key is created.

## Deleting/Avoiding Cached Data

There are a few ways in which the cached data will be removed from the cache:

 1. The cache has expired. The time that the data was added to the cache has exceeded the TTL set in the `config.js`
 2. The browser displaying OSO has been refreshed

For calls to the service, a `noCache` parameter can be added to the query string and used to prevent the use of a cached value for that specific request while maintaining all other cached values.

For example, the following link could be created to allow the user to avoid cached responses when searching for a client:

```
<a href="#pending-policies/search/?searchTerm=smith&searchType=clientName&noCache=1" class="oa-js-nav">Search for Smith</a>
```

*Note*: The actual value assigned to `noCache` does not matter.

## Memory Usage

Since the cache is stored in memory, OSO should be profiled periodically to ensure that memory usage does not exceed reasonable limits. The team should be aware of how the caching performs in lower-memory devices before implementing caching in new features.

### Evaluating Memory Usage

The following site outlines a couple of methods used to evaluate memory usage of a web application in Chrome:

https://developers.google.com/web/tools/chrome-devtools/memory-problems/



# Code Review Checklist

A list of considerations for reviewing code.

* Does the code complete the requirements of the task?
* Does the code fit with existing [architecture](./Architecture.md) and conventions
* Does the code match our [style guide](./JavaScriptStyleGuide.md)?
* Are any of the files related to the review getting too large to manage? Can they be broken up into more focused modules?
* Does the code have proper comments?
* Have the needed documentation files been created and/or updated?
* Is the code readable and maintainable?
* [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), [KISS](https://en.wikipedia.org/wiki/KISS_principle), and [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it) considerations.
* Did we write something that an already-included library can handle?
* Is the unit test coverage sufficient?
  * Is the quality of the tests high enough?
  * Are we wasting time testing a library, or are we truly testing our code?
  * Positive tests - show that it works when it should.
  * Negative tests - show it doesn't do something when it shouldn't
  * Tests for boundary conditions
* Technical debt
  * Does it leave any?
  * If so, is there a plan in place to pay it (perhaps a story for the next feature)?
# Simulating OSO REST Service for Local Development

While working on frontend code in local development environments, OSO
developers can use the `localdata-dev` server to simulate REST APIs. The
`localdata-dev` server uses NodeJS and Express to serve data from a set
of static JSON files.


Getting Started With `localdata-dev`
=====================================================================

### Setting Up

1. Clone the repository.
    https://bitbucket.org/ebus/localdata-dev
2. Open a git bash window and switch to the localdata-dev directory.
3. Run `npm install` to set up the necessary dependencies.

### Starting and Stopping the Server

In the git bash window, run `npm start` to start the server.

To stop the server, press control-C.

While the server is running, a message will appear in the bash window for every
REST request that the server receives.


Testing the OSO Frontend Using the Dev Data Page 
=====================================================================

The `localdata-dev` server relies on configuration settings provided in
cookies that are sent with every ajax request. Use the `devdata.html` page
in the OSO frontend to set up the cookies.

1. Run the default `gulp` command to start the OSO frontend server.
2. Open the Dev Data page in a browser:
    http://localhost:3000/devdata.html

### Options
By default, all requests to the simulated REST service are delayed by a
random amount of time, ranging from zero seconds to 2.75 seconds. To
avoid the delay, check *Development Mode*.

Successful requests return HTTP status code 200. To cause all REST requests
to return an error response, check *Error Mode*. Change the "Status Code"
value if you want the errors to return a status other than the default of 500.

### Selecting a User
To run the OSO application locally as a specific user, click on the user's
link in the *Test Users* box. The Dev Data page will add the appropriate
cookie, and navigate to the default OSO page: http://localhost:3000/index.html

### Adding a New User to the Dev Data Page
To add a new test user, edit the `devdata.html` file and append a new user
link to the list. The `href` must always point to `index.html`, and the `data-target` property is the user id. The text in the anchor tag should include the
user's name and a description of the user's important features.

    <li>
        <a href="index.html" data-target="123abc">
            Joe Smith (123abc): General Agent with surrogate access to Jan Brown
        </a>
    </li>
    <li>
        <a href="index.html" data-target="456def">
            Jan Brown (456def): General Agent who has earned Chairman's Trip
        </a>
    </li>

After you add a new user to `devdata.html`, the `localdata-dev` server will
not recognize the new user id, so it will only serve the default data files
until you add user-specific data that corresponds with the new user id.


`localdata-dev` Directory Structure
=====================================================================
The `localdata-dev` tool is used by several frontend products in eBusiness,
and each product has a separate directory in the repository. OSO files are
located in the `/osoPrj` directory.

Each REST API endpoint has a separate subdirectory within `/osoPrj`. For
example, data for OSO's _User_ API is located in `/osoPrj/user`.

Within the endpoint directories are a number of `.json` files. For GET requests, the filenames correspond to the id of the user selected in the Dev
Data page. For example, when testing the OSO frontend as Joe Smith (userid
123abc), for a GET request to the _/user_ REST API, `localdata-dev` returns
the contents of `/osoPrj/user/123abc.json` as the response object.

If there is no .json file that corresponds to the requested user id, then
the API returns the default values located in `/osoPrj/user/all.json` as the
GET response object.

POST requests use the file `post-all.json` as the response object for every
POST, regardless of the userid. _(It's possible that user-specific POST responses might work--e.g. `post-123abc.json`--but I'm not 100% sure --Randy)_


Modifying `localdata-dev`
=====================================================================
Changes to the `localdata-dev` repository should follow the same development
process as any other ebus git repository:

1. Create a new "feature/my-feature-name" branch.
2. Make changes in the feature branch, committing as normal.
3. When the changes are complete, tested, and fully committed, merge the `master` branch into your feature branch to resolve any conflicts.
4. Push the feature branch to the remote repository on BitBucket.
5. Issue a pull request to have your changes reviewed before they are merged into `master`. Look at other commits to the project to find several developers to add as reviewers for your pull request.
6. Wait for approvals before merging your pull request.


Adding New User-Specific Data to `localdata-dev`
=====================================================================
To add user-specific response data, simply add a new .json file in the
appropriate endpoint directory, using the userid as the filename. No further
code changes are necessary.

Example: To add a /user response for Jan Brown (456def), create this file:
`osoPrj/user/456def.json`.

*Note: Check with other OSO frontend developers before modifying existing
data that you didn't create.*


Adding A New REST API Endpoint to `localdata-dev`
=====================================================================
1. Add the new endpoint to `osoPrj/mappings.js`. GET and POST endpoints have separate sections.
2. Create a new folder under `osoPrj` that matches the folder name you added in `mappings.js`.
3. In the new folder, create an `all.json` file for GET endpoints or a `post-all.json` file for POST endpoints, along with any user-specific .json files you need.


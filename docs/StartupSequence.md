# OSO Frontend Startup Sequence

A step-by-step look at what is happening in osoApp.js when the app is starting up:

1. OSOapp.initialize() executes when OSOapp is instantiated in app.js
    * Creates a user module object. The user module does NOT fetch from the user service yet.
    * Calls ajaxSetup() to configure ajax prefilter, error handler, etc.
2. OSOapp.onStart() executes when app is started in app.js
    * Adds 'showErrorPage' listener on Radio channel "errorChannel" which would call OSOapp._showErrorPage().
    * Calls OSOapp.requestUserData() to set up a one-time "state:ready" event listener on user module and to instruct the user module to fetch its data from the server.
    * Calls OSOapp.addUIComponents() to add the app's root layout, navbar, and footer. These components don't rely on user data.
3. When the user module triggers the "state:ready" event after the user data is loaded, OSOapp.userReadySetup() executes.
    * The capabilities array in the user data is used to filter the appStructure so that it includes only the pages that the user can see.
    * Use the filtered appStructure to show the sidebar navigation.
    * Add the contentLayout view.
    * Add listeners for the "nav" event from either the sidebar or the contentLayout view. They are handled by OSOapp._handleNavEvent().
    * idleWatcher is started. It will log the user out if there are no click or scroll events for 30 minutes.
    * The analytics module is started.
    * Call OSOapp._configureRouter()...
4. OSOapp._configureRouter()
    * Instantiates the OSORouter, which is an instance of Backbone.BaseRouter.
    * Binds the BaseRouter's onNavigate() function to OSOapp._routerOnNavigate().
    * If it isn't already started, start Backbone.History. This causes an immediate hashchange event which the BaseRouter handles by calling OSOapp._routerOnNavigate()...
5. OSOapp._routerOnNavigate()
    * BaseRouter sends this function a datastructure called routeData that contains lots of info about the route that was matched.
    * Get the pageId from the routeData, or use "home" if no pageId is present.
    * Get the query object from the routeData. It is a parsed copy of the key/value pairs that appear in the querystring portion of the URL. OSO uses it to represent the current state of the page, so we call it the stateObj.
    * Send the pageId and stateObj to OSOapp._showPage()...
6. OSOapp._showPage()
    * Send the pageId and stateObj to contentLayout.showPage() where the pageId will be validated against the list of pages that the current user is allowed to see. The contentLayout then creates the appropriate page view and sends it the stateObj. Each page is responsible for using the stateObj to configure itself appropriately.
    * Send the pageId to the sidebarApp so that it can highlight the correct menu item.

**At this point, the OSO application is fully loaded and is displaying the correct page.**

- - -
### Navigation within the OSO app

If the user clicks a navigation link (a link with the class "oa-js-nav") in the page content, sidebar navigation, or top navbar, that child component generates a "nav" event with a new pageId. The "nav" event is handled by OSOapp._handleNavEvent()...

  * Uses Backbone.history.navigate(pageId) to update the browser's URL hash with the new pageId. **Note: None of the child components is permitted to modify the URL hash. Therefore, any child component that displays navigation links must call preventDefault() on those link click events. Only OSOapp._handleNavEvent() is allowed to modify the URL hash.**
  * Sends the pageId to OSOapp._showPage().


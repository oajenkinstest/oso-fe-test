# Ace Template

OSO uses the Ace Template, which is a theme for Bootstrap 3. The source of the Ace template resides in the [ace-template-vendor repo on BitBucket](https://bitbucket.org/ebus/ace-template-vendor).

## Custom Build

In order to streamline Ace to keep it small and prevent it from doing things that we do not want it to do, we create custom builds of the `ace.js` and `ace-elements.js` files.

### Ace Elements

The `ace-elements.js` file provides a number of optional features that we don't use in OSO, so it doesn't appear in the codebase. Here are the features that it can provide, included here as a reference for potential future use in OSO:

| Option                       |
| ---------------------------- |
| Ace custom scroller          |
| Custom color picker element  |
| Ace file input element       |
| Bootstrap 2 typeahead plugin |
| Wysiwyg                      |
| Spinner                      |
| Treeview                     |
| Wizard                       |
| Content Slider               |
| Onpage Help                  |

### Ace Functions

The current `ace.js` file has been built with the following options:

| Checked | Option                                                  |
| ------- | ------------------------------------------------------- |
| No      | Load content via Ajax                                   |
| Yes     | Custom drag event for touch devices                     |
| Yes     | Sidebar functions                                       |
| No      | Scrollbars for sidebar                                  |
| Yes     | Scrollbars for sidebar (second style)                   |
| No      | Submenu hover adjustment                                |
| Yes     | Widget boxes                                            |
| No      | Settings box                                            |
| No      | RTL                                                     |
| No      | Select a different skin                                 |
| No      | The widget box reload button/event handler              |
| No      | The autocomplete dropdown when typing inside search box |
| No      | Auto content padding on fixed navbar & breadcrumbs      |
| No      | Auto Container                                          |

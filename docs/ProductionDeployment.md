# Production Deployment and Release

## Steps to Build and Deploy to FT or Production
In order to deploy code to the FT environment, the following steps should be completed. **Note**: The person making these updates must be logged in to Jenkins (http://swpbuilds11:8080/login) using an account with the appropriate access required to complete these steps.

The steps are the same for deploying to both FT and Production. The only difference is the Jenkins job that is selected:

 | Deployment Environment | Jenkins Job Name                       |
 |------------------------|----------------------------------------|
 | FT                     | EBUS_oso-frontend_VERSIONED_build      |
 | Production             | EBUS_oso-frontend_VERSIONED_build_PROD |

1. Click on the **EBUS_oso-frontend_VERSIONED_build*** job in Jenkins
2. Select *Build with Parameters* from the left-hand menu
3. Use the following settings:

    | Name        | Value                                        | Example Value
    |-------------|----------------------------------------------|--------------
    | BRANCH_NAME | The name of the branch being deployed.       | `release/v5.0.x`
    | APPROVER    | The name of the person approving the release | `Shanthi Santhakumar`

4. Click the **Build** button

If the build job is successful, the deploy job will run automatically to copy the files to the webserver in a version folder given by the `version` property in the project's `package.json` file. At that point, the new version will be *available* in the target environment. The deployment can be tested by going to the target environment and appending the version number to the URL.

For example, if the `package.json` version used in the release is "5.2.3", the deployment can be verified in the production environment by using the following URL:

```https://onesourceonline.oneamerica.com/v5.2.3```

The updates will not be seen without appending the version number until the release (next section) has been completed.

## Steps to Release to FT or Production
In order to perform a release, the following steps should be completed. **Note**: The person making these updates should be logged in to the Jenkins The person making these updates should be logged in to Jenkins (http://swpbuilds11:8080/login) in order to have the access required to complete these steps.

The steps are the same for releasing to both FT and Production. The only difference is the Jenkins job that is selected:

 | Deployment Environment | Jenkins Job Name                         |
 |------------------------|------------------------------------------|
 | FT                     | EBUS_oso-frontend_VERSIONED_release_FT   |
 | Production             | EBUS_oso-frontend_VERSIONED_release_PROD |

1. Click on the **EBUS_oso-frontend_VERSIONED_release_FT** job in Jenkins
2. Select *Build with Parameters* from the left-hand menu
3. Use the following settings:

    | Name      | Value                                        | Example Value
    |-----------|----------------------------------------------|--------------
    | VERSION   | The SEMVER version used in `package.json`    | `v5.0.4`
    | APPROVER  | The name of the person approving the release | `Shanthi Santhakumar`

4. Click the **Build** button.

This will deploy a new `index.html` file to the target server which will point to the versioned directory (e.g. "v5.0.4" using the example value above).

## Rollback

It may be necessary to roll back to an earlier version of OSO. Right now, that's simply running the
release job and specifying an earlier version number.
# WCM - Web Content Management

WCM is a static content management and retrieval system that allows the business to modify website content without needing changes to OSO and without new deployments and/or downtime.

## IBM WCM info

http://www-03.ibm.com/software/products/en/ibmwebcontmana

## Accessing WCM in OSO

Since WCM resides on another server, OSO needs to access the content in a way that does not violate the browser's security policy. Since the version of WCM we are using does not implemnt content-as-a-service and does not return Access-Control-Allow-Origin headers, we use the Apache server's Proxy Pass module to allow the content to come through the web server that the OSO frontend resides on.

Documentation of the Apache Module mod_proxy:
https://httpd.apache.org/docs/2.2/mod/mod_proxy.html


The configuration for the webservers is in `/opt/IBM/HTTPServer85/conf`. Within that folder, each web application has its own config location. The OSO configurations are:

| Environment | Servers           | path                |
|-------------|-------------------|---------------------|
| ST          | sldweb11/sldweb12 | /stonesourceonline/ |
| FT          | sltweb11/sldweb12 | /onesourceonline/   |
| Production  | slpweb11/slpweb12 | /onesourceonline/   |

ProxyPass configuration for WCM in `httpd.conf` file in the above locations:

```
...

Listen 161.199.122.140:443
<VirtualHost 161.199.122.140:443>

  ...

   ProxyPass /wcm/ http://www.stwcm.oneamerica.com/
   ProxyPassReverse /wcm/ http://www.stwcm.oneamerica.com/

   ProxyPass /wps/wcm/myconnect/ http://www.stwcm.oneamerica.com/wps/wcm/myconnect/
   ProxyPassReverse /wps/wcm/myconnect/ http://www.stwcm.oneamerica.com/wps/wcm/myconnect/

   ProxyPass /wps/wcm/connect/ http://www.stwcm.oneamerica.com/wps/wcm/connect/
   ProxyPassReverse /wps/wcm/connect/ http://www.stwcm.oneamerica.com/wps/wcm/connect/

   ProxyPass /wps/contenthandler/ http://www.stwcm.oneamerica.com/wps/contenthandler/
   ProxyPassReverse /wps/contenthandler/ http://www.stwcm.oneamerica.com/wps/contenthandler/
  ...

</VirtualHost>

...
```

These ProxyPass settings allow the OSO app to reference the `/wcm/`, `/wps/wcm/myconnect/`, `/wps/wcm/connect/`, and `/wps/contenthandler/` urls as if they were on the local server.

The above configuration example is for ST. The base URLs for WCM in each environment are below:

| WCM Servers   | URL                              |
|---------------|----------------------------------|
| System Test   | http://www.stwcm.oneamerica.com/ |
| Function Test | http://www.ftwcm.oneamerica.com/ |
| Production    | http://www.wcm.oneamerica.com/   |

IDs of content match across ST, FT, and Prod.

## Finding WCM IDs

1. Find the path to a piece of content.
1. Add `?presentationtemplate=tools/pt-id` to the URL
1. launch that URL in a browser

Example, for the content at `/wps/wcm/connect/ind/Products/WholeLife/Liberty_Select`, the following URL will result in the ID being displayed in the browser:

http://www.stwcm.oneamerica.com/wps/wcm/connect/ind/Products/WholeLife/Liberty_Select?presentationtemplate=tools/pt-id

Which results in `d183590048135e4c9263b77656ded129`. Since the IDs are the same across all environments (ST, FT, Prod), you can look up the ID in any environment and use it in all others.

**Do NOT use this utility in application code!**

## `wcmRole` Parameter
In the WCM Content model (`/src/modules/wcm-content/models/wcm-content-m.js`), a parameter named `wcmRole` is passed to the WCM server in the query string. Some WCM site areas (like those used by "The Pathway") use the `wcmRole` value to determine what content should be presented to the user.

For example, The Pathway Menu Structure model (`/src/models/pathwayMenuStructure-m.js`), has `link` properties which only point to a WCM site area. WCM uses the `wcmRole` to return content for that particular role. The content returned resides within the site area in WCM, but OSO does not need to know the exact path of the content item itself.

The parameter is passed on all requests to WCM. If the site area for the requested content does not use the `wcmRole` parameter, WCM will ignore the parameter. In the event that `wcmRole` is not passed and the site area is expecting it, WCM will send back content which contains the same markup as a 404 error on the front-end.

## WCM Module in OSO

The WCM module in OSO is fairly simple. It consists of a model, view, and template. In general, a developer should just instantiate a view and render it, as in the following example:

```
var WcmView = require('/path/to/modules/wcm-content/views/wcm-content-v');

// Create the view
var wcmView = new WcmView({

    // The ID of the WCM content
    id: '526e5e15-65da-4e72-a8b6-fec3c1347acf',

    // The element in which to render the content
    el: '#contentView'
});

// Render it!
wcmView.render();
```

The view handles the instantiation of the model and calling the model.fetch() function. The model contains logic to parse out the relavent bits from the WCM JSON response.

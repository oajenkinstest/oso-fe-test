# Performance Center Module

* A stand alone module, implemented as a Marionette App, Which will be deployed into OSO Legacy Website.
* Performance Center widget includes the following information:
    - Type of Commission (retail or ib)
    - YTD Commission (totalYearToDate)
    - Annual R&R Qualifying YTD FYPC 
    - YTD Total FYPC (total Year To Date)
    - YTD Life Lives Sold
    - Life Lives Needed for Next Level Bonus
    - Life Lives Next Bonus Value
    - Required Qualifying FYPC
    - Actual Year to Date FYPC
    - Remaining Qualifying FYPC (difference between Required and Actual)
    - Flag indicating the producer met the APR even when the Actual is less than the Required amount
    - Chairman's Trip data
        - Actual FYPC
        - Actual Life FYPC
        - Actual Lives
        - Qualified and On Schedule (FYPC, Life FYPC, Lives)
        - Remaining (FYPC, Life FYPC and Lives) for Qualified and On Schedule
        - Flags to indicate whether producer met the goal (FYPC, Life FYPC, Lives)
    - Leader's Conference
        - Actual FYPC
        - Actual Life FYPC
        - Actual Lives
        - Qualified and On Schedule (FYPC, Life FYPC, Lives)
        - Remaining (FYPC, Life FYPC and Lives) for Qualified and On Schedule
        - Flags to indicate whether producer met the goal (FYPC, Life FYPC, Lives)
        - Total FYPC (for CA Producers only)

  Label for commission will be different based on the `calendarYearProduction.commission.type` object received from the service. If `type` is 'retail' label will be as 'Net Earned Commission (NEC)' and label for 'ib' will be changed to 'First Year Commission'.

  For Leader's conference, Life FYPC (Actual, Remaining, Qualified and On Schedule, and Flags) data will be trimmed for Career Agent (CA) users. So Lives will be replaced with Life FYPC dial.

* The data structure for the info (above) from the service is:
 
          {
            "calendarYearProduction" : {
              "commission" : {
                "type" : "retail",
                "totalYearToDate" : "USD 36889.88"
              },
              "fypc" : {
                "totalYearToDate" : "USD 4338.78",
                "qualifyingYearToDate" : "USD 4338.78"
              },
              "lifeLives" : {
                "totalSold" : 1,
                "forNextBonus" : 49,
                "nextBonusValue" : "USD 750.00"
              },
              "annualProductionRequirements" : {
                "requiredQualifyingFypc" : "USD 48000.00",
                "actualYtdFypc" : "USD 56789.01",
                "remainingQualifyingFypc" : "USD 0.00",
                "requiredTotalFypc" : "USD 120000.00",
                "actualTotalYtdFypc" : "USD 8047.78",
                "remainingTotalFypc" : "USD 111952.22",
                "totalFypcFlag" : "N"
              },
              "chairmansTrip" : {
                "type" : "retail",
                "actualQualFypc" : "USD 40000.34",
                "actualLifeFypc" : "USD 35048.97",
                "actualLives" : 15,
                "onscheduleQualFypc" : "USD 51953.34",
                "onscheduleRemainingQualFypc" : "USD 31048.97",
                "onscheduleLifeFypc" : "USD 51953.34",
                "onscheduleRemainingLifeFypc" : "USD 31048.97",
                "onscheduleLives" : 15,
                "onscheduleRemainingLives" : 15,
                "requiredQualFypc" : "USD 51953.34",
                "requiredRemainingQualFypc" : "USD 31048.97",
                "requiredLifeFypc" : "USD 51953.34",
                "requiredRemainingLifeFypc" : "USD 31048.97",
                "requiredLives" : 15,
                "requiredRemainingLives" : 15,
                "onscheduleQualFypcFlag" : "N",
                "requiredQualFypcFlag" : "N",
                "onscheduleLifeFypcFlag" : "N",
                "requiredLifeFypcFlag": "N",
                "onscheduleLivesFlag" : "N",
                "requiredLivesFlag" : "N"
              },
              "leadersConference" : {
                "type" : "retail",
                "actualQualFypc" : "USD 54588.00",
                "actualLifeFypc" : "USD 44455.00",
                "actualLives" : 5,
                "onscheduleQualFypc" : "USD 85333.00",
                "onscheduleRemainingQualFypc" : "USD 85333.00",
                "onscheduleLifeFypc" : "USD 222556.00",
                "onscheduleRemainingLifeFypc" : "USD 54552.00",
                "onscheduleLives" : 7,
                "onscheduleRemainingLives" : 7,
                "requiredQualFypc" : "USD 128000.00",
                "requiredRemainingQualFypc" : "USD 128000.00",
                "requiredLifeFypc" : "USD 10025.00",
                "requiredRemainingLifeFypc" : "USD 15522.00",
                "requiredLives" : 10,
                "requiredRemainingLives" : 10,
                "onscheduleQualFypcFlag" : "N",
                "requiredQualFypcFlag" : "N",
                "onscheduleLifeFypcFlag" : "N",
                "requiredLifeFypcFlag" : "N",
                "onscheduleLivesFlag" : "N",
                "requiredLivesFlag" : "N",
                
                /*** The fields below are only sent for CA Producers ("type" === "retail") ***/
                "actualTotalFypc" : "USD 71803.00",
                "onscheduleTotalFypc" : "USD 171875.00",
                "onscheduleRemainingTotalFypc" : "USD 100072.00",
                "requiredTotalFypc" : "USD 187500.00",
                "requiredRemainingTotalFypc" : "USD 115697.00",
                "onscheduleTotalFypcFlag" : "N",
                "requiredTotalFypcFlag" : "N"
              }
            }
          }

##### Gulp task
* `widget-pc-dev` used to take build for dev version of Peformance Center widget
* `widget-pc-prodcheck` used to take build for production version Peformance Center widget

The gulp tasks will create a directory named `release` which will include the artifacts from the build. The following files from the `release` directory should be 
used to include in the old OnlineServices application:

* `css/*.*`
* `fonts/*.*`
* `images/*.*`
* `js/`
    * `app.min.js`
    * `app.min.js.map`
    * `vendor.min.js`
    * `vendor.min.js.map`

We have modified package.json by adding `browserify-shim` section, in order to run the Performance Center widget on Old Stack Website. Which will check the dependency on jQuery version 1.11.3, before initializing Bootstrap component.

#####  Views used in Performance Center Modules 
* RootLayout (views/root-layout-v.js)
    A Marionette LayoutView view which render the Performance Center Widget 
    - `el` object in the view is pointed to a HTML element (`#performancecenter`) to place the  Performance Center Widget
    - defined a region `#performance-center` (contained in the template provided) to render the Performance center widget

* PerformanceCenterView (views/performance-center-v)
    - A Marionette LayoutView to handle Performance Center UI which will be placed to the region defined in `RootLayout`
    
    - Option `showInfoLinks` is an optional param to show all more info / rule links

    ######Sample usage 
    Assuming region `performanceCenterRegion` defined on LayoutView

        this.showChildView('performanceCenterRegion',new PerformanceCenterView({
            showInfoLinks : true
        })); 
   

* PerformanceCenterModel (models/performance-center-m)
    - A Backbone Model to handle Performance Center content fetched from `/oso/secure/rest/performanceCenter/`
    - The response data fetched will be parsed to add new attributes and converted to an object to render the view. 

* `compare` 
A conditional handlebarsjs helper method to compare the equality of two parameters. This method require two mandatory parameters and third parameter is optional to handle multiple comparision operators, by default it will be '=='.

Example usage:

##### Template
    //in the template file
    {{#compare commission.type "ib"}}
      <strong>Earnings</strong>
    {{/compare}}
    
    //in the template file with third parameter
    {{#compare commission.type "ib" operator="=="}}
      <strong>Earnings</strong>
    {{/compare}}

* `isoCurrencyToNumber`
A handlebarsjs helper to convert ISO-4217 USD values into numbers. Optional boolean parameters are to return an absolute value and to round to the nearest whole number.

Example usage:

##### Template
    // in the template file
    {{isoCurrencyToNumber annualProductionRequirements.actualYtdFypc true true}}

* `hasValue`
A conditional handlebarsjs helper to show the graph values for Chairman's Trip (Outer and Inner) Graph of if any of the Qualifying FYPC flag has value 'N'. There will be three parameters for this method. One will be `value` to compare, second one will be a flag to match all object with `value` and hash values in form of key=value.

Example usage:

##### Template
    // in the template file
    {{#hasValue value false key=value key=value key=value .... }}
    <HTML>
    {{/hasValue}}

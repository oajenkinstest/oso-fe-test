# Error Handling

In any application, there are expected and unexpected errors. This document explains how OSO handles
these errors.

## Backbone.Radio

The `osoApp` creates the 'error' channel and listens for the 'showErrorPage' trigger on the channel.

## Basic Application Errors

### Page does not exist

In a classic application, a server would return a 404 for a page that does not exist. Since the
"pages" are all a part of the client-side application, `osoApp` checks whether or not a "page"
(in this case, the string immediately after the hash) actually exists in `appStructure-m`.

### User does not have capability to view the page

Again, a classic application would return a 403 for a page that the user does not have access to.
In the case of OSO, the client-side application must check `appStructure-m` to see if the user
has the proper capability to view the page.

## Handling Errors for specific AJAX calls

When saving/retrieving data, the calling code should handle expected errors gracefully. To do so,
have the object (likely a view) listen for errors on the Model or Collection as below:

```
var policies = new PendingPolicySummaryCollection(null, {
    policyNumber: policyNumber
});

this.listenTo(policies, 'error', this._handlePolicyNumberSearchError);
```

The `_handlePolicyNumberSearchError` can trap expected errors and gracefully display a message on
the page, without routing to the error page. Next, the specific error codes need to be added in the
`utils/error-helper` module. The `handledEndpoints` data structure has a key that represents a
portion of the api URL, matched with a regular expression, and a value that is a list of handled
status codes.

```
var handledEndpoints = {
    '/policies'  : [404, 500],
    '/producers' : [500],
    '/ui/log'    : []
};
```

URL/status codes that are not listed in this structure will fall through to be handled by the global
error handler.

## Unhandled Errors

As noted in [Ajax Customizations](docs/AjaxCustomizations.md), the `ajax-utils` module adds a global
error handler for AJAX calls. This handler will inspect the status code and message of any failed
AJAX call. If the endpoint/status does not exist in the `error-helper.handledEndpoints` structure,
the global error handler will display the error page.


# Continuous Integration

To help ensure the OSO frontend project is continually producing the best quality code, we are using a Continuous Integration (CI) process using Jenkins and Gradle, as well as our usual tools (Git and Gulp).


## Git (Bitbucket)

OneAmerica's ebusiness projects use [Git](https://git-scm.com) for source code version control, with repos hosted on bitbucket.org. The repo for the OSO frontend is:
https://bitbucket.org/ebus/oso-frontend


## Gulp

[Gulp](http://gulpjs.com/) is a task runner that actually builds the OSO frontend web application. Developers use it locally, and the Continuous Integration system (via Gradle) uses it as well. See the [GulpTasks Documentation](GulpTasks.md) for the list of available tasks.


## NPM

[NPM](https://www.npmjs.com/) handles package management. The build process calls `npm install` in order to update all of the node modules used in the OSO frontend application.

OneAmerica has an internal Nexus server that is used as a local NPM repository. If a package is requested that is not yet cached locally, the Nexus server will fetch the package from the public NPM repository and store it for future use.


## Jenkins

[Jenkins](https://jenkins-ci.org/) is the main component involved in our CI process. It monitors our [Bitbucket](https://bitbucket.org/ebus/oso-frontend) repository for changes and starts builds. Our Jenkins server resides at: [http://swpbuilds11:8080/](http://swpbuilds11:8080/).

https://wiki.jenkins-ci.org/display/JENKINS/Building+a+software+project


### User accounts on the Jenkins server

Generally, developers can monitor the status and console output of Jenkins jobs without having an account. However, to manually run or modify builds, a developer must have an account with the ability to manage Jenkins.

Accounts are created in two different ways:

- Jenkins parses commit logs to create users on its own. If you've checked in code, you probably have an account. To use this, you'll need someone with access to set a password and (possibly) update the account's email address.
- Register yourself! On the bottom of the Jenkins login form is a "Create an account" link.

Once you have an account, a Jenkins administrator (Ryan McBride, for example) will need to grant you permission to run specific jobs.


## Gradle

[Gradle](https://gradle.org/) is the final major piece in the CI strategy. Gradle uses the Nebula plugins and calls Gulp to do the builds.

### Files in oso-frontend project

- /build.gradle

  This file contains the plugin configuration and set of Gradle tasks.

- /gradle.properties

  This file contains two properties, **group** and **mavenRepoBase**. The URL in the **mavenRepoBase** is also present in the `build.gradle` file, so I'm not sure if this file is necessary.

### Nebula Plugins Reference

- [nebula.nebula-publishing](https://plugins.gradle.org/plugin/nebula.nebula-publishing)
- [nebula.nebula-release](https://plugins.gradle.org/plugin/nebula.nebula-release)
- [nebula.nebula-source-jar](https://plugins.gradle.org/plugin/nebula.nebula-source-jar)
- [nebula.info](https://plugins.gradle.org/plugin/nebula.info)
- [nebula.provided-base](https://plugins.gradle.org/plugin/nebula.provided-base)


-----


## Front-end OSO Jenkins Jobs

The Jenkins jobs for building the OSO frontend can be found at the following location:
[http://swpbuilds11:8080/job/EBUS/job/EBUS_oso](http://swpbuilds11:8080/job/EBUS/job/EBUS_oso)

There are three types of jobs in OSO: _build_, _deploy_, and _release_.

**Build** jobs check out a branch from the git repo, use npm to fetch all of the dependencies, run the automated tests, and use `gulp` tasks to create a _build artifact_ containing all of the files needed to serve the OSO web site.

**Deploy** jobs copy the build artifact onto a set of web servers. The files are put into a subdirectory that is named for the git branch or the version number so that it can be accessed at a special URL (see the job descriptions below for the specific URLs).

The directory structure on the web server looks like this:

```
index.html
v2.1.0/
    css/
    js/
    images/
    index.html
    index-root.html
v2.0.1/
v2.0.0/
```

**Release** jobs set a specified subdirectory as the one that will be served from the root of that web server.

The doc root of the webserver contains the `index.html` file along with folders for each deployed version of the site. Each HTML, JS, and CSS file reference in `index.html` includes a path into whichever subdirectory contains the current "released" version of OSO.

This job simply copies the `webroot/vX.Y.X/index-root.html` to `webroot/index.html` making that
version of the application accessible from the root url, e.g. https://onesourceonline.oneamerica.com/.

#### How Jobs Are Run

Most build jobs are triggered by webhooks from BitBucket when commits are pushed to certain branches in the Git repository. A successful build job will then trigger one or more _downstream_ jobs to deploy the files. Release jobs can only be run with human intervention; they are never run as an automatic downstream job.

Any job can be run manually, even if it is normally run downstream of another job. To run a job manually, first click on the job name from the [EBUS_oso list in Jenkins](http://swpbuilds11:8080/job/EBUS/job/EBUS_oso). On the left of the page is a list of command links, including **Build Now** (or **Build with Parameters** for parameterized jobs). Click the build link to launch the job. For parameterized jobs, it will display a page for you to enter/select the parameters before running.

See the [detailed document on Jenkins jobs](JenkinsJobConfiguration.md) for more information on configuring Jenkins jobs.


### Build Jobs

All build jobs abort if they take longer than 20 minutes.

- **EBUS_oso-frontend_feature_build**

  This build job is normally launched by a webhook in the oso-frontend repository in Bitbucket when there are new commits in branches with _/feature/_ in the name. This is useful to check developers' pushes to the repo for errors.

  If the build succeeds, these downstream jobs are triggered:
  - EBUS_oso-frontend_feature_deploy_ST
  - EBUS_oso-frontend_feature_deploy_SB

- **EBUS_oso-frontend_FT_branch_build**

  A manually run, parameterized job to build a specified branch. The only parameter for the job is the name of the branch to build. If the build is stable, the **EBUS_oso-frontend_feature_deploy_FT** job handles the deployment to FT. This job allows us to build and deploy individual branches to FT for additional testing without affecting any release statuses.

  If the build succeeds, this downstream job is triggered:
  - EBUS_oso-frontend_feature_deploy_FT

- **EBUS_oso-frontend_master_build**

  This build job is normally launched by a webhook in the oso-frontend repository in Bitbucket when there are new commits in the `master` branch.

  If the build succeeds, these downstream jobs are triggered:
  - EBUS_oso-frontend_master_deploy_SB
  - EBUS_oso-frontend_master_deploy_ST

- **EBUS_oso-frontend_master_pc_build**

  A **manually run** job to build the Performance Center widget from the master branch.

  If the build succeeds, this downstream job is triggered:
  - EBUS_oso-frontend_master_pc_deploy

- **EBUS_oso-frontend_VERSIONED_build**

  A parameterized job to build a specified branch. The parameters are the branch to build, and the name of the approver. It uses the version property of the `package.json` file to create an `index-root.html` file that places the version in the paths of css, js, and image files. The job also creates two new files that are added to the build artifact: `build_version.txt` which lists the version, the person who ran the job, the approver, and a timestamp, and `version.properties` which stores the version name that the downstream deploy jobs will use.

  If the build succeeds, these downstream jobs are triggered:
  - EBUS_oso-frontend_VERSIONED_deploy
  - EBUS_oso-frontend_VERSIONED_deploy_ST

- **EBUS_oso-frontend_VERSIONED_build_PROD**

  This job is identical to EBUS_oso-frontend_VERSIONED_build. The only difference is that when this job succeeds, it triggers a job that deploys the files to the _production_ web servers instead of FT and ST.

  If the build succeeds, this downstream job is triggered:
  - EBUS_oso-frontend_VERSIONED_deploy_PROD


### Deployment Jobs

- **EBUS_oso-frontend_feature_deploy_FT**

  Copies the oso-frontend website files to the FT web servers (SLTWEB11 and SLTWEB12). This job monitors the **EBUS_oso-frontend_FT_branch_build** job and runs if that build is stable. The build is deployed into a directory named for the branch. For example, a branch named `feature/ooso-12345-awesomeness` will be available at [https://ftonesourceonline.oneamerica.com/ooso-12345-awesomeness](https://ftonesourceonline.oneamerica.com/ooso-12345-awesomeness) and a branch named `bugfix/fix-input-validation` will be available at [https://ftonesourceonline.oneamerica.com/fix-input-validation](https://ftonesourceonline.oneamerica.com/fix-input-validation).

  Upstream jobs:
  - EBUS_oso-frontend_FT_branch_build

- **EBUS_oso-frontend_feature_deploy_SB**

  Copies the oso-frontend website files to the SB (sandbox) web servers (SLDWEB11 and SLDWEB12). This job monitors the **EBUS_oso-frontend_feature_build** job and runs if that build is stable. The build is deployed into a directory named for the branch. For example, a branch named `feature/ooso-9876-mario-kart` will be available at [https://sbonesourceonline.oneamerica.com/ooso-9876-mario-kart](https://sbonesourceonline.oneamerica.com/ooso-9876-mario-kart).

  Upstream jobs:
  - EBUS_oso-frontend_feature_build

- **EBUS_oso-frontend_feature_deploy_ST**

  Copies the oso-frontend website files to the ST web servers (SLDWEB11 and SLDWEB12). This job monitors the **EBUS_oso-frontend_feature_build** job and runs if that build is stable. The build is deployed into a directory named for the branch. For example, a branch named `feature/ooso-9876-mario-kart` will be available at [https://stonesourceonline.oneamerica.com/ooso-9876-mario-kart](https://stonesourceonline.oneamerica.com/ooso-9876-mario-kart).

  Upstream jobs:
  - EBUS_oso-frontend_feature_build

- **EBUS_oso-frontend_master_deploy_SB**

  A job that deploys the oso-frontend to the SB (sandbox) environment. This job monitors the **EBUS_oso-frontend_master_build** job and runs if that build is stable. This job deploys the master build to the root folder on SB, so the master build is always available at [https://sbonesourceonline.oneamerica.com/](https://sbonesourceonline.oneamerica.com/).

  Upstream jobs:
  - EBUS_oso-frontend_master_build

- **EBUS_oso-frontend_master_deploy_ST**

  A job that deploys the oso-frontend to the ST environment. This job monitors the **EBUS_oso-frontend_master_build** job and runs if that build is stable. This job deploys the master build to the root folder on ST, so the master build is always available at [https://stonesourceonline.oneamerica.com/](https://stonesourceonline.oneamerica.com/).

  Upstream jobs:
  - EBUS_oso-frontend_master_build

- **EBUS_oso-frontend_master_pc_deploy**

  A job that deploys the performance center to the ST environment. This job monitors the **EBUS_oso-frontend_master_pc_build** job and runs if that build is stable. This job deploys the build to the /pc folder on ST, so the widget is available at [https://stonesourceonline.oneamerica.com/pc/](https://stonesourceonline.oneamerica.com/pc/).

  Upstream jobs:
  - EBUS_oso-frontend_master_pc_build

- **EBUS_oso-frontend_VERSIONED_deploy**

  Deploys a build to the FT web servers (SLTWEB11 and SLTWEB12). This job runs when the EBUS_oso-frontend_VERSIONED_build job completes successfully. It copies the OSO frontend files to an appropriate version-named folder on the webserver root. For example, if the package.json version is "3.1.2" it will place the files in `/v3.1.2`. This version will be available at `https://ftonesourceonline.oneamerica.com/v3.1.2/index.html`.

  Upstream job:
  - EBUS_oso-frontend_VERSIONED_build

- **EBUS_oso-frontend_VERSIONED_deploy_ST**

  Deploys a build to the ST web servers (SLDWEB11 and SLDWEB12). This job runs when the EBUS_oso-frontend_VERSIONED_build_ST job completes successfully. It copies the OSO frontend files to an appropriate version-named folder on the webserver root. For example, if the package.json version is "3.1.2" it will place the files in `/v3.1.2`. This version will be available at `https://stonesourceonline.oneamerica.com/v3.1.2/index.html`.

  EBUS_oso-frontend_VERSIONED_deploy_ST upstream jobs:
  - EBUS_oso-frontend_VERSIONED_build_ST

- **EBUS_oso-frontend_VERSIONED_deploy_PROD**

  Deploys a build to the production web servers (SLPWEB11, SLPWEB12, and SLPWEB13). This job runs when the EBUS_oso-frontend_VERSIONED_build_PROD job completes successfully. It copies the OSO frontend files to an appropriate version-named folder on the webserver root. For example, if the package.json version is "3.1.2" it will place the files in `/v3.1.2`. This version will be available at `https://onesourceonline.oneamerica.com/v3.1.2/index.html`.

  EBUS_oso-frontend_VERSIONED_deploy_PROD upstream jobs:
  - EBUS_oso-frontend_VERSIONED_build_PROD


### Release Jobs

  Release jobs are only run manually. They copy a deployed version's `index-root.html` file to the webserver root's `index.html` file. The parameters are the version to release and the name of the approver. Like the build job, it creates a release.txt file listing version, the person who ran the job, the approver, and a timestamp.

  The only difference between the release jobs are which web servers they operate against.

- **EBUS_oso-frontend_VERSIONED_release_ST**

  Releases to the ST web servers SLDWEB11 and SLDWEB12.
  
- **EBUS_oso-frontend_VERSIONED_release_FT**

  Releases to the FT web servers SLTWEB11 and SLTWEB12.

- **EBUS_oso-frontend_VERSIONED_release_PROD**

  Releases to the production web servers SLPWEB11, SLPWEB12, and SLPWEB13.






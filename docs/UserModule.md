# Frontend User Module

The UserModule is a CommonJS module, implemented as a Marionette Object. On creation, it creates a
default UserModel. Few modules, other than the main `osoApp` will access the UserModule. Most will
use the UserModel instead.

## UserModule Methods:

* `.fetchUserData()`
    - Retrieves the user data, populating the UserModule's UserModel, and triggers a 'state:ready' method when complete. The osoApp listens for the 'state:ready' method before initializing user-dependent portions of the application.
* `.getUser()`
    - Returns the UserModel object.
* `.getCapabilities()`
    - A function that returns the user's `capabilities` array.

* `.getUserOLSRole()`
    - A function that returns the user's OLS role filtered from `capabilities` array. 
    If user has multiple OLS capabilities, those capabilities will be sorted in descending order 
    and  joined together with -(hyphen).

* `.logout()`
    - A function that redirects the user to the logout URL.

## Impersonation Functions

The UserModule contains functions for impersonation. These may be removed in the future, in favor of using Siteminder for impersonation.

* `.beginImpersonation( producerInfo )`
    - sets the `impersonatedWebId` property
    - future state, this will:
        + do a new fetch on the userModel
        + trigger an event to `osoApp` to re-initialize the sidebar, redirect to the 'home' page, and add a "Viewing As" ribbon..
* `.endImpersonation()`
    - sets the `impersonatedWebId` property to null
    - future state, this will:
        + do a new fetch on the userModel
        + trigger an event to `osoApp` to re-initialize the sidebar, navigate to the home page, and remove the "Viewing As" ribbon.
* `.getImpersonatedWebId()`
    - returns the value of the `impersonatedWebId` property. This can be used to determine if the current user is impersonating another (truthy value) or not (falsy value). This is used within the `jQuery.prefilter` to add a parameter to the query string of all requests.


## `UserModel`

* A Backbone Model
* Includes these properties and methods:
    - webId {string} (though this is technically a numeric value)
    - displayName {string}
    - capabilities
        + an Array of strings
        + See [Frontend Architecture for Access Control](FrontendAccessControl.md) for details
    - hasCapability({string})
        + method that returns a boolean if the user has the specified capability
        + See [Frontend Architecture for Access Control](FrontendAccessControl.md) for details

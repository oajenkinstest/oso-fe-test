# Jenkins Job Configuration

Jenkins jobs contain many different sections to configure the jobs. To view / edit the job configuration, select the **Configure** link on the job's page in Jenkins.

## Basic Build Settings (No Heading in Config)
This section defines the basic information and settings for the build, such as Project name, Description, variables, etc.

In general, we have "Discard Old Builds" checked, with a "Log Rotation" strategy for when to remove old build. For each of the following properties, we are using the value *20*

- Days to keep builds
- Max # of builds to keep
- Days to keep artifacts
- Max # of builds to keep with artifacts

### Prepare an environment for the run
We check the "Prepare an environment for the run", which opens up more settings. In this section we have the following options checked:

- Keep Jenkins Environment Variables
- Keep Jenkins Build Variables

#### Properties Content
Within the "Prepare an env...", we set environment variables used later in the job. In general, our build jobs have the following properties:

- GIT_PROJECT_NAME (oso-frontend)
- GIT_GROUP_NAME (ebus)
- SCRIPT_FIX_BRANCH (/e/DEV/git/jenkins-utils-dev/scripts/checkout-local-tracking-branch.sh - a script to check out from git)


## Advanced Project Options
No options currently in use.

## Source Code Management
This section defines the repository used for the Jenkins project. Since OSO uses Git, we select the "Git Repositories" option and use the following settings:

- Repository URL (ex: git@bitbucket.org:ebus/oso-frontend.git)
- Credentials (Jenkins access to BitBucket)
- Branches to build / Branch Specifier (ex: \*/feature/\* - anything with /feature/ in the name)
- Git executable (Local Git)
- Additional Behaviours
  - Check out to specific local branch / Branch name (${GIT_BRANCH} var populated from above?)
  - Prune stale remote-tracking branches

## Build Triggers
This section defines the events that trigger the Jenkins job. For build jobs, it is often "Build when a change is pushed to BitBucket". For deployment jobs, it is generally "Build after other projects are built", with the "Projects to watch" item specifying a build job and "Trigger only if build is stable" selected. Jenkins provides fairly detailed information on configuring these sections if you click on the help icon next to the item.

### Build on BitBucket Push
The "Build when a change is pushed to BitBucket" trigger uses the [BitBucket plugin](https://wiki.jenkins-ci.org/display/JENKINS/BitBucket+Plugin) in Jenkins. This uses a Webhook configured in the [repo's settings](https://bitbucket.org/ebus/oso-frontend/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin). Currently the plugin only supports `repo:push`, but the Webhook can send a message to Jenkins for a variety of events.

The webhook is configured to send a request to Jenkins at the url http://githooks.oneamerica.com/bitbucket-hook/, and shows a log of requests sent in the "View requests" page available via the [Webhooks settings](https://bitbucket.org/ebus/oso-frontend/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin).

Sometimes, the "push" trigger does not start correctly. In this case, the "BitBucket Hook Log" (available via the Jenkins job's navigation) will show a message such as "polling has not yet run", but the webhook will show requests sent to Jenkins. In this case, we have found that enabling the "Poll SCM" option to run every minute in order to catch the first push will enable the push trigger. Once you see the push events logged in the Hook Log, you can disable the "Poll SCM" option.

Occasionally, BitBucket's service is disrupted. If the webhook does not appear to be sending requests, check [BitBucket's status page](http://status.bitbucket.org/).

## Build Environment
This section defines the environment in which the build will actually run. In general, check the following options:

- Abort the build if it's stuck
  - Absolute, 10 minutes longer than a typical duration for that job
- Inject passwords to the build as environment variables
  - Global passwords
- Set jenkins user build variables

## Build
The items in the "Build" section are actual commands to run. They come from a list of possible types of commands available via the "Add build step" button. The commands differ by the job type.

### Build Jobs
In general, the jobs that build the OSO frontend script use the following commands:

- Execute Shell
  Executes shell commands. In our case, this often builds the properties file used in the "Inject environment variables" command. It also handles installing dependencies via running "npm install".

- Inject environment variables
  Specify a properties file or set of properties, as key=value pairs.

- Invoke Gradle Script
  - Gradle Version = 2.1
  - Tasks = 'devSnapshot' + the task from the `./build.gradle` file you want to invoke for the job

#### Handling Node Modules
There are two `Execute Shell` commands in each build script that help prevent the build from removing and reinstalling all node modules used by the project, unless it is necessary.

- Clean the workspace

  This command will clean the workspace, but leave the `checksum.txt` file and the `node_modules` and `bin` folders.

  ```git clean -xfd --exclude=checksum.txt --exclude=node_modules --exclude=bin```

- Run `checksum.sh`

  This script will delete the node_modules folder and run `npm install` if any of the following are true:

  - `node_modules` folder does not exist
  - `checksum.txt` file does not exist
  - the result of an md5 checksum on the `package.json` file does not match the value in `checksum.txt`

  After running `npm install`, it will save the new md5 value in `checksum.txt`.

  ```$SHELL -ex ./scripts/checksum.sh```

Using these scripts means that the builds will _not_ automatically pick up newly available versions of node modules that `npm install` would bring in. To do this, we would need to delete the `checksum.txt` file from each build project's workspace.

### Deploy Jobs
Deployment jobs commonly use the following commands:

- Inject environment variables
  Specify a properties file or set of properties, as key=value pairs

- Copy artifacts from another project
  This command is used to copy the artifacts created in the build job to be deployed

- Send files or execute commands over SSH
  This command is used to clean the destination folder(s) and to copy the new artifacts to the web servers


## Post-build Actions
Like the "Build" section, the items are commands that can be added and deleted.

- Archive the artifacts
- Editable Email Notification
- Publish Cobertura Coverage Report

  Istanbul has a cobertura reporter that write the code coverage report in the root folder with the name `cobertura-coverage.xml`. Jenkins references that file in the "Cobertura xml report pattern" field.

- Publish JUnit test result report

  Mocha uses the "mocha-jenkins-reporter" to generate a JUnit-compatible unit test report. It writes the report to the root folder with the name `report.xml`, which is used for the "Test report XMLs" field.


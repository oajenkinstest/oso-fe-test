# Application
OneSource Online is a Backbone / Marionette application.

## App Initialization
OSO uses an `app.js` file to set up browser-specific workarounds and load the actual Marionette application.
The actual Marionette application is `osoApp.js`, which acts as the top-level component in the application.
The `osoApp` module initializes the necessary utility modules as well as the app's UI components.
Please see [StartupSequence](./StartupSequence.md) for more details.

## Component Architecture

* A component is a grouping of Views, Templates, and Models that provide a limited function
  * Many of the folders in `apps`, `modules`, and `pages` are components of this type
* Components should be decoupled from other components and reusable throughout the application
* A component's view might contain other components, making a parent/child relationship between them.
  * Child components...
    * have no knowledge of their parent or sibling components
    * communicate up to their parent only by triggering events (`this.trigger(..)`)
    * expose functions that the parent component can call
    * uses Backbone.Radio for non-parent communication (should be rare)
  * Parent components...
    * may call functions within their child components
    * should avoid directly manipulating properties of a child component
    * listens for events from child components (`this.listenTo(..)`)

## OSO App Components
There are three main tiers of components within OSO.

### Top-level components
The top-level components are the `osoApp` itself, and the UI and utility components that it initializes.

The User Interface is made up of the following components initialized by `osoApp`:

* apps/sidebar - An Ace Template side navigation
* modules/content - A module responsible for displaying 'pages' in the application
* modules/footer - A simple module for the footer
* modules/navbar - A module for the header / top navigation bar.

Additionally, `osoApp` initializes the following utility modules:

* [modules/analytics](./Analytics.md) - A single interface for web analytics packages.
* modules/idle - A module for maintaining a user's Siteminder session.
* modules/siteWideBanner - A module used for displaying a WCM banner on all views
* modules/user - A module encapsulating the user functions including impersonation (view-as-producer and delegation).

### Pages
A page is a UI component displayed within the content module. A page can also contain sub-components.

* Use a Layout View as the main interface for the component
* Place components and coordinate their actions
* Request app to set checkpoints for navigation
* Use the checkpoint data to rebuild the state of the page and the child components

### Other UI Components
OSO contains other components that are displayed within a page.

* modules/behaviors - An implementation of [Marionette Behavior](https://marionettejs.com/docs/v2.4.7/marionette.behavior.html)
* modules/browserFeatureDetection - A module used to detect the features available within the client's browser
* modules/debug - A module used in lieu of `console.log` statements for debugging the app. This module logs error messages to `waslogs/api01/ui.log` on the WAS application servers at the following locations:

| Environment | Servers                          |
|-------------|----------------------------------|
| ST          | swdwasap11/swdwasap12            |
| FT          | swtwasap11/swtwasap12            |
| Production  | swpwasap11/swpwasap12/swpwasap13 |

* modules/dialChart - An abstraction around easyPieCharts
* modules/footer - The content used in the footer of the app
* modules/performance-center
* modules/tout - A module used for displaying touts like "Policy Search" on the Home view
* modules/viewAsProducer - A module used to display the "View as Producer" ribbon on each page when in VaP mode
* modules/waitIndicator - A module used to display a spinner icon to indicate that the view is waiting on data in order to render completely
* modules/wcm-content

### iPipeline Forms/iGo Single Sign-on
The Home view for OSO can contain icons (depending on the user's capability) which allow the user to view FormsPipe forms and/or eApp/Illustrator in a new window.
The form uses a `target` attribute set to a named browser window so that the token is submitted and the iPipeline content is displayed in this new, named window. In order to try to avoid popup-blockers being triggered during this process, the named window is opened immediately during the `click` event and data is posted to this named window after the token has been retrieved from the service. Since security tokens need to be retrieved from the service in this process, the ajax `success` callback loses the context of the `click` event which can trigger the popup blocker in browsers like Chrome.

## Backbone.Radio
To allow components to communicate beyone the parent/child relationship, OSO uses [Backbone.Radio](https://github.com/marionettejs/backbone.radio "Radio Documentation").
Since Marionette does not yet include Radio (it uses Wreqr), we needed to use a [shim](https://gist.github.com/thejameskyle/48afb443b8c8c6ee4f46 "Shim to use Radio in Marionette").

Below are the Radio Channels used in the application:

* 'analytics'
  - Defined in `modules/analytics/analyticsModule.js`
  - to track `view` or `actions` in the application
* 'appStructure'
  - Defined in `models/appStructure-m.js`
* 'browserFeature'
  - Defined in `modules/browserFeatureDetection/browserFeatureDetectionModule.js`
  - to access a list of browser features that may or may not be present
* 'error'
  - Defined in `osoApp.js`
  - for app level error logging
* 'spinner'
  - Defined in `osoApp.js`
* 'user'
  - Defined in `modules/user/userModule.js`
  - to access user's capability across application
* 'wait'
  - Defined in `modules/waitIndicator/waitIndicatorModule.js`


## Ajax Considerations
OSO uses an ajax prefilter to allow user impersonation and enable CORS communication with the service. It also has custom ajax error handling. See [AjaxCustomizations](./AjaxCustomizations.md) for details.

## Impersonation re-initialization for View as Producer and Delegation
When App is running of impersonate state and a user wishes to refresh browser, open a link in new tab, or navigate back to a previous page of authenticated user (actual user) from impersonated page. Currently app is storing all impersonation data in memory and haven't leveraged any browser storage to re-create impersonation because of some limitation such as setting *life* to storage items.

We resolved this issue with help of adding `targetuser` `Query Param` in url, once user start impersonate session (VaP (View as Producer) / Delegation).

https://onesourceonline.oneamerica.com/#pageId?stateProp1=Some&targetuser=oscar

There are multiple navigation scenarios to add `targetuser` query param to the URL.

1. Navigating through `_handleNavEvent` method from `osoApp.js` which is bind with `nav` event for multiple components defined in `osoApp.js`.
2. Navigate with `HREF` attributes of `anchor` element.

The `addTargetUserQueryParamToURL` method defined in `utils.js` will check whether `userModule` has impersonated data to add `targetuser` query param with hash URL. This method has used in both scenarios mentioned above.

example: `addTargetUserQueryParamToURL(#pageId/)` will return `#pageId/?targetuser=oscar`

Currently we have included the logic to add `targetuser` query param in following places:

1. Method `_handleNavEvent` from `osoApp.js` 
Here we used `addTargetUserQueryParamToURL` method to build the URL.

2. 'buildHrefURL' Handlebarjs helper method which will invoke a utility method with same name 'buildHrefURL'
This one used in all templates across app.

3. Updating `stateObj` used in the view to build the checkpoint. All `view` has `stateObj` in `options`. Example usage in `pending-policy-manager-page-v.js`

          // If there is 'targetuser' in the stateObj please add it back
          // Delegate and VaP access 
          if (this.options.stateObj && this.options.stateObj.targetuser) {
              newState.targetuser = this.options.stateObj.targetuser;
          }


Finally while refreshing / opening link in new tab, application will capture `targetuser` query param from the URL and it will be used to get producer info. The method `getImpersonatedDataFromQueryString` defined in `osoApp.js` used to handle this logic. If it is delegation user producer info get it from `delegation` object and in case of VaP it will just use value of `targetuser` query param. This method used below scenarios:

1. App load with `targetuser` query param
Used in `userReadySetup` method of `osoApp.js`
2. App navigate with `targetuser` query param
Used in `_routerOnNavigate` method of `osoApp.js`

This  method will help application to decide to start / end of impersonation.

## Building Href URL
*Its very important* to use use `buildHrefURL`, a Handlerbarjs helper method to build href URL
to render links in all templates across application.It will help to build hash URL through a single interface which will add 'targetuser' query parameter to URL while user is in impersonate state. The first input parameter (pageLink) should be a URL which need to start with hash (#) or http or https.
Sample usage:

          // pageLink along with query parameters
          {{buildHrefURL '#pending-policy-manager/org/' producerOrgId=producer.id}}

          // Only pageLink
          {{buildHrefURL '#pending-policy-manager/org'}}

          // Only pageLink as URL start with https
          {{buildHrefURL 'https://api.oneamerica.com/'}}

          // Concat multiple inputs to build page link
          {{buildHrefURL (concat hashPrefix '/allPolicies' tabLinksId)}}


This helper method (`buildHrefURL`) will be invoking  a utility method named as same as `buildHrefURL` and this utility method can be used in any views / modules to build URL.

## Caching of Data

See the [Data Caching](docs/DataCaching.md) documentation.

## "Smart" Scroll Messages
The UI design allows for both vertical and horizontal scrolling on smaller screens if needed. Since horizontal scrollbars on mobile devices are not easily seen, a message indicating that the user can scroll horizontally is added to most tables (both HTML and DataTables tables). There is a behavior (see [Other UI Components](#other-ui-components) above) which looks for tables in the view, checks for horizontal scrollbars, and then displays the message to the user.

The CSS class for the scroll message is `.table-responsive-instruction`. The element is visible by default. The behavior (`smartScrollTables`) is added to `/src/js/modules/contentLayout-v.js` so that even tables in WCM content can be checked for horizontal scrolling and the message -- if present under the table -- will be shown/hidden.

DataTables tables have the associated `.table-responsive-instruction` located in different places relative to the DataTables table. As a result, a data attribute (`data-instruction-container`)should be applied to all DataTables to indicate the ID of the `.table-responsive-instruction` container for that specific table. As an example, the DataTables table in the Client Name search results (`src/js/pages/pending/templates/client-name-search-results-t.hbs`) has a `div.table-responsive-instruction` with an ID of "policy-search-instruction". The associated table tag looks like the following:

```
<table id="policySearchResults" class="table table-striped table-bordered table-hover table-sortable" width="100%" data-instruction-container="#policy-search-instruction">
```

*Note:* The `data-instruction-container` has the ID with the leading hash ("#") so that a jQuery selector can be used with this value.
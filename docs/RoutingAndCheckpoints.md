# OSO Application Routing


## Summary ##

OSO uses a router to initialize itself during application startup. We use Backbone.BaseRouter (an extension of Backbone's built-in Router) because it allows us to define one general-purpose route handler rather than having to add a new route handler for every page in the app.

Certain page states need to be "bookmarkable" to allow deep-linking. We call these states *checkpoints*.


## URL Format ##

We store two bits of information in the fragment identifier (hash) of the URL: a **pageId** that identifies which content page to display, and a **stateObj** with key/value pairs that a page can use to configure itself. The format is as follows:

```
https://onesourceonline.oneamerica.com/#pageId?stateProp1=Some+string+data&stateProp2=1234
```

**Important: It is no accident that what looks like a querystring appears _after_ the hash.** In a "standard" URL, the querystring comes first, and the hash is at the end. Our "backwards" arrangement actually makes the *stateObj* (the question mark and everything after it) become part of the hash.

We want the _stateObj_ to be part of the hash so that we can change the _stateObj_ to set a checkpoint. If we use a real querystring for _stateObj_, then setting a checkpoint will cause the page to reload.

The reason we still use the querystring format is that even though it is technically part of the hash, Backbone.BaseRouter still extracts the _stateObj_ data as though it were a normal querystring. (Read more about BaseRouter below.)


## Rewriting Deprecated URLs ##

When OSO was first released, it only supported pending policies. The original set of pageIds reflected this limitation: _#pending-policies_ was the policy list page, _#pending-policy-detail_ was the policy detail page, etc. When support for active policies was added in September 2018, those pages were used for both pending and active policies, so the pageIds no longer made sense. Knowing that some users would have saved bookmarks, there needed to be a way to change the pageId for an existing page while still allowing the old pageIds to work.

Before the router is started, OSOapp runs the `rewrite()` function from src/utils/rewriteDeprecatedURL.js to check the current location.hash for an old pageId and replace it with a new pageId if necessary. This does not trigger a page reload or navigation event. Since it happens before the router is started, the router only sees the new pageId.


## The Role of the Router ##

The router's only job is to show the right page with the right configuration when the app is starting up or when the user clicks the browser's forward or back buttons. Clicks on navigation links within the app do NOT trigger the router. Instead, the OSOapp (the topmost object) listens for "nav" events from child components and handles them independently of the router.


## Why not use _pushState_ instead of a hash? ##

We can, but there are several historical reasons why we implemented our routing and checkpoints using a hash:

* Until recently, we had to support IE9, which can't do pushState.
* We would have to change the configuration of our Apache web servers to serve the same index.html page no matter the URL. It's a minor change, but getting even simple things changed on OneAmerica production servers requires going through our IT provider (T-Systems and/or Cognizant), which is a time-consuming and annoying process.
* Backbone.router.navigate can use pushState, but it hardcodes the *state* parameter to an empty object. Therefore, the only thing you can use in a Backbone router to identify the current application state is the url in the history. To be fair, that’s all you’re going to have available in a deep link anyway.

None of these things are insurmountable. We could switch from hash to pushState URLs in the future by modifying the router's navigation handler and the checkpoint code.


## Backbone.BaseRouter ##

Backbone's built-in Router has some limitations that make it inflexible and annoying to configure. For OSO we have chosen to use an extension called **Backbone.BaseRouter**. Here is a snippet from the documentation (https://github.com/jmeas/backbone.base-router):

> Instead of requiring that you associate a callback with a route, this 
> library lets you associate whatever you'd like with a route. It can be a 
> callback if you want, but it can also be an object, or even a string.
>
> Whenever a Route is matched, a single method on the Router is called. This 
> method is passed a single argument,routeData, that contains as much about 
> the matched route as possible. Included this object are parsed query 
> parameters, named fragment params, and the object you associated with the 
> route, among other things. This single point-of-entry, combined with all of 
> this data, makes it remarkably easy to add new abstractions to the Router.

Backbone.BaseRouter has only one routing function that is always called for any matched route. We have defined the routing function as `OSOapp._routerOnNavigate(routeData)`. The `routeData` parameter is one of the key advantages of using this library because it provides easy access to the _pageId_ and the _stateObj_ portions of the URL when we use the URL hash format described above and only two routes:

### Routes

```
var OSORouter = Backbone.BaseRouter.extend({
    routes: {
        ''                      : 'home',
        'c(/)*pageId'           : 'wcm',
        ':pageId(/)(*subpages)' : 'page'
    }
});
```

**These three routes are all we need unless we decide to make a major change to our URL format. Specifically, there should be no need to add more routes as we add additional pages to OSO. If you find yourself wanting to add a new route to support a new page, stop and figure out why your new page can't behave like the other pages in the app.**

The "home" route pattern is an empty string. Without the empty route, the routing function would not be run when the app is loaded with nothing but a bare domain name and no hash.

The "wcm" route pattern tells the BaseRouter to expect a URL hash that contains a partial path to an item in WCM. Example:

```
#c/interest-rates/Content-Care-Solutions
```

The "page" route pattern tells BaseRouter to expect a URL hash that breaks down as follows:

| piece of route pattern | description                                   |
| ---------------------- | --------------------------------------------- |
| :pageId                | unique name for the page in the OSO app       |
| (/)                    | optional slash                                |
| (*subpages)            | optional characters to complete the hash, but NOT including the querystring portion |

To illustrate, consider this URL hash which follows the format of a typical OSO page:

```
#planet/?name=Alderaan&temperament=peaceful&weaponCount=0&isDestroyed=true
```
(or without the optional slash...)

```
#planet?name=Alderaan&temperament=peaceful&weaponCount=0&isDestroyed=true
```

The `routeData` object that BaseRouter sends to `OSOapp._routerOnNavigate()` contains these handy properties (among others):

| property              | value                                               |
| --------------------- | --------------------------------------------------- |
| routeData.params      | Object {pageId: "planet", subpages: null}            |
| routeData.linked      | "page"                                              |
| routeData.queryString | "name=Alderaan&temperament=peaceful&weaponCount=0&isDestroyed=true" |
| routeData.query       | Object {name: "Alderaan", temperament: "peaceful", weaponCount: "0", isDestroyed: "true"} |

**The `routeData.query` object is the awesome part.** Even though the thing that looks like a URL querystring is actually part of the hash (meaning it can be modified without causing a page refresh), BaseRouter treats it as if it really is a querystring. BaseRouter goes so far as to parse it into a JS object for us and include it as the `query` object property on `routeData`. Then it is a simple matter for `OSOapp._routerOnNavigate()` to use `routeData.query` as the `stateObj` that gets eventually gets sent to the page views.

#### What is (*subpages) for?

Technically, the `(*subpages)` portion of the route pattern is be useful for pages that require a hash with a second (or more) value like this:

```
#planet/Alderaan/?temperament=peaceful&weaponCount=0&isDestroyed=true
```
```
#planet/Alderaan/subpage2/?temperament=peaceful&weaponCount=0&isDestroyed=true
```
or more realistically:

```
#policy-manager/polices/pending
```

For this example hash, BaseRouter would put the "polices/pending" in the `routeData.params.subpages` property:

| property         | value                                            |
| ---------------- | ------------------------------------------------ |
| routeData.params | Object {pageId: "policy", subpages: "polices/pending"} |

While `_routerOnNavigate` called by `BaseRouter` on app will split `subpages` property at the `/` and share it with content layout view to handle the checkpoint.


```
stateObj.subpages['policies', 'pending']
```

The advantage of using `subpages` as an array format is 'a page view' can decide the subpage level to render it on screen. That means that the app will eliminate all invalid levels of subpages silently and will help to retain valid subpage levels.

For example : A `view` in the app is expecting `#policy-manager/polices/pending` url to render the subpage, but if there are any invalid subpage levels (`#policy-manager/polices/pending/invalidsub1/invalidsub2`), the application will silently reset to `#policy-manager/polices/pending/`. **Every page view is responsible to check and write the checkpoint manually.**

## Checkpoints ##

In some videogames, when the player has progressed partway through a level, they reach a "checkpoint." It's a place in the game that they can easily return to later instead of having to play through the entire level again. The OSO app has pages instead of levels, and each page has a workflow. OSO's checkpoint module allows OSO users to easily return to a specific point in their workflow rather than having to start the page from the beginning every time. To think of it in a different way, OSO pages can use checkpoints to record the current state of the application. When a user returns to that checkpoint, the page uses the stored state to configure itself properly.

### Implementation details

The checkpoint module (src/js/modules/checkpoint/checkpointModule.js) implements two functions: `writeCheckpoint()` and `readCheckpoint()`.

`writeCheckpoint()` takes a plain JS object and encodes it into a string using jQuery's `$.param()` function. It combines the encoded string with the current pageId, then uses `Backbone.history.navigate()` to update the URL hash and add an entry in the browser's history.

*Writing a checkpoint does not trigger a nav event.* The only thing that happens when writing a checkpoint is that the URL hash changes. Nothing in the OSO app should be listening for hash changes, so writing a checkpoint should not have any side-effects.

`readCheckpoint()` uses the `$.deparam()` jQuery plugin to decode the state data from the hash and return it as a JS object. Reading a checkpoint does not clear the state data from the URL hash. For now, OSO does not use `readCheckpoint()` except for in unit tests.

### Only *pages* should write checkpoints

A checkpoint represents the state of a *page* at a significant moment in time. It doesn't include any non-page-related information, such as the current user id, sidebar configuration, etc. **Since the checkpoint data can only come from a *page*, then only *pages* may write checkpoints.**

There is nothing in the code that actually prevents non-pages from writing a checkpoint, so we must rely on our developers to use checkpoints correctly in their own code and to enforce proper use through careful code reviews.

One possible way to add protection against non-page checkpoints is to create a new custom view type called PageView that all pages use. Move the checkpoint functions into the PageView so that only pages have a reference to them. *(Caveat: Some OSO pages use Marionette.ItemView and some use Marionette.LayoutView, so it's probably best to wait until Marionette v3 to implement PageView since ItemView and LayoutView are being combined together into Marionette.View in v3.)*

### How to write a checkpoint

1. Obtain a reference to the `checkpointModule`.
2. Build a `stateObj` that represents the state of the page. It must be a plain JavaScript object, not an array, string, date, or something else. However, it can *contain* arrays, strings, etc, as object properties.
3. Call `checkpointModule.writeCheckpoint(stateObj);`

```
// starting hash: #foo

var cp  = require('../../../modules/checkpoint/checkpointModule');

// later in the page code...

var myState = { bar: 123, baz: 'quux', subpages : ['xyzzy']};
cp.writeCheckpoint(myState);

// ending hash: #foo/xyzzy/?bar=123&baz='quux'
```

### How to clear a checkpoint

To clear an existing checkpoint from the URL hash, call `writeCheckpoint()` with no parameters:

```
// hash: #foo?bar=123&baz='quux'

cp.writeCheckpoint(); // now: #foo
```

Sending an empty stateObj will clear the checkpoint, but it will leave behind a trailing question mark after the pageId in the URL hash:

```
// hash: #foo?bar=123&baz='quux'

cp.writeCheckpoint({}); // now: #foo?
```

### How checkpoint data is used

The checkpoint data in the URL is only important for moving through the browser's history or for when the URL is saved as a "deep link" into an OSO page.

When the OSO app is first loaded or when the user clicks the "back" or "forward" buttons in the browser, BaseRouter reads the checkpoint data from the URL. The data eventually gets passed to the page view `initialize` function in the `options.stateObj` object, where the page can use the data to configure itself.

Here is a contrived example of a page view using the `stateObj` to configure its view model:

```
var searchView = Marionette.ItemView.extend({

    template : searchPageTemplate,
    model    : new SearchViewModel(),

    initialize: function (options) {

        if (options && options.stateObj) {
            if (typeof options.stateObj.searchStr === 'string') {
                this.model.set('searchString', options.stateObj.searchStr);
            }

            if (typeof options.stateObj.rowsPerPage === 'number') {
                this.model.set('rowsPerPage', options.stateObj.rowsPerPage);
            }
        }
    },

    // more page view methods here
});
```


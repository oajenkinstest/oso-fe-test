**[root]** - Project-wide setting, properties, and build files.
* **bin** - Binaries for development tools that are not managed by npm.
* **dist** - A generated folder with built artifacts used during local development.
* **docs** - Documentation files.
* **gulp** - The core build tool.
  * **config** - Configuration for Gulp.
  * **tasks** - Individual Gulp tasks.
  * **util** - Utilities for Gulp tasks.
* **node_modules** - A generated folder with all of the downloaded node modules.
* **release** - A generated folder with built artifacts that have been minified in preparation for deploying to FT or production.
* **scripts** - Various bash scripts to aid in development. Run these via your terminal or Git Bash.
* **src** - The root of the application source code.
  * **css** - CSS files for the application.
    * **less** - Less files (which compile to CSS).
  * **images** - Images used in the application.
  * **js** - Folder holding the JavaScript for the application.
    * **apps** - Marionette.Application modules for major user interface components like the top navbar and the left sidebar. These have been adapted from the Employee Benefits product.
    * **collections** - Backbone.Collection items used either at the global app level or common across modules.
    * **config** - Overall app configuration.
    * **models** - Backbone.Model items used either at the global app level or common across modules.
    * **modules** - Folder holding modules that are intended to be reused by any page.
      * **someModule** - Root folder for 'someModule'.
    * **pages** - Modules for individual OSO content pages
      * **somePage** - Root folder for 'somepage'. Subfolders will mirror the top level, with subfolders for collections, models, templates, views, etc.
    * **templates** - Handlebars templates used either at the global app level or common across modules.
    * **validations** - Validation used at the app level or common across modules.
    * **viewModels** - Models backing an entire view (rather than being a discreet entity) used at the app level or common across modules.
    * **views** - Marionette Views used either at the global app level or common across modules.
  * **vendor** - JavaScript from 3rd parties.
    * **fonts** - Special fonts used in the application.
* **test** - Testing files.
  * **config** - Configuration for tests.
  * **functional** - Functional tests for a complete application.
    * **helpers** - Helper files to assist in the testing.
    * **specs** - Actual tests.
  * **unit** - Tests for code units that do _not_ require a browser environment.
    * **specs** - The actual unit tests
  * **unit-browser** - Tests for code units that _require_ a browser context.
    * **specs** - Actual unit browser tests

# Release Checklist

## Overview
The purpose of this document is to provide a list of generally sequential steps which are typically completed for each release of OneSource Online. The specific timing of each task should be coordinated with SA’s and service developers on the OSO team.

## Checklists

### Near End of Sprint

 |    |            Task            |        Reasoning           |
 |----|----------------------------|----------------------------|
 |    | Ensure that no commits are added to the **master** branch that should not be included in the current release. | When the **release** branch is cut, it’s risky to be reverting commits.|
 |    | Coordinate with the service to determine if any breaking changes occur in this release. If so, the MAJOR version should be updated and match the MAJOR version of the service. If no breaking changes occurred, the MINOR version should be updated in the `package.json` file. | The MAJOR and MINOR versions of both the front-end and service should be kept in sync (e.g. v12.2.3 for the front-end can be deployed with v12.2.9 of the service) |
 |    | Submit a pull-request for **master** for the updated version. | This updates the **master** branch with the new version number.|
 |    | Once the version is updated in master, create a new release branch. The naming convention for the branch should be in the format **release/v{MAJOR}.{MINOR}.x** (e.g. "release/v12.2.x") | This branch is what will be deployed to the FT and Production environments for the release. |
 |    | If fixes or updates need to get added to the release, continue submitting pull requests for master and cherry-pick the commits (or equivalent task) that need to into the release. If fixes are added, increment the PATCH version number as needed in the pull request for each fix. | This allows both the **master** branch and **release** branch to be updated without creating multiple pull requests and reviewing/approving the exact same code |
 |    | Coordinate with service team to determine which service team member and which FE team member will be handling the release. Ensure that the method of communication (usually Skype), time, etc. are worked out. | |

### End of Sprint
 |    |            Task            |       Reasoning            |
 |----|----------------------------|----------------------------|
 |    | Coordinate with the service developers to deploy the release to FT. | This allows for UAT testing prior to the release in the FT environment. |

### Prior to Release
 |    |            Task            |       Reasoning            |
 |----|----------------------------|----------------------------|
 |    | Deploy (but *do not* release) the front-end to production. Once the deployment is done, the new version can be checked prior to the actual release by appending the version number in the URL. For example, if the front-end version is “12.2.3”, the production URL can be checked by using https://onesourceonline.oneamerica.com/v12.2.3. | The deployment can take up to 20 minutes to perform. If this is done prior to the morning of the release, the actual release will only take a few seconds. The front-end can also be tested in production using the URL with the appended version number once the service has been deployed. |

### Day of Release
 |    |            Task            |
 |----|----------------------------|
 |    | Once the service team member has given the word, the front-end should be released to production. | |
 |    | Smoke-test the changes in the front-end to ensure that the deployment appears to be successful. | |

## Resources

 * Release dates and names can be found by viewing the directory structure in **K:\IT WORK\PROJ00011703 - OneSource Online Strategic Initiative\\! Releases**
 * https://semver.org for versioning guidance
 * [JenkinsJobConfiguration.md](./JenkinsJobConfiguration.md) for instructions on deploying and releasing using Jenkins jobs
 * [ProductionDeployment.md](./ProductionDeployment.md) for overview of release process

# Unit Testing

## Coverage Goal
- \>= 70% code coverage
- \>= 90% coverage for core architectural components


## Tools Used
OSO uses a variety of libraries for unit testing. The list, and links to their sites (and documentation) is below:

- Mocha (framework) <https://mochajs.org>
- Chai (assertion library) <http://chaijs.com/>
- Sinon (mocks, stubs, spies, etc) <http://sinonjs.org/>
- Istanbul (code coverage reporting) <https://www.npmjs.com/package/istanbul>


## Mocha Test Configuration
The mocha tests reside in `/test/mocha-test`. 

There are two files which are used to set up the environment along with a root-level [hooks](http://mochajs.org/#hooks):

* The `setup.js` file is used in the `require` option for mocha. This creates the JSDOM and loads any packages which could be used during testing to the `window` and `global` namespace.
* The `helpers.js` file contains the root-level hooks as well as some helper functions used in different suites. 

## Writing Tests for Mocha
The basic format of the tests is as follows
```
/* global describe:false */


// Inject our module(s) that we need for the test.
// Generally, this will be the object of our test. We may
// need to include another module (such as a model if we
// need to build those for the test(s)).
var MyView = require('dist/modules/someModule/views/myView');

// Describe the test spec, using the bdd variable defined above
// You can have multiple 'describe()' functions in the spec.
describe('A description of this test spec', function() {

    // define variables and/or helper functions
    var mySpy;

    // set up to do before the tests run, optional
    // However, it is highly recommended to use before(Each) and after(Each) to
    // set up and  restore sinon's mocks, spies, and stubs if the object is going
    // to be used in multiple tests. Otherwise, a test failure that involves the
    // use of one of these can cause failures in future tests within the spec.
    beforeEach(function () {
        // It is recommended that any spies or stubs which are created
        // using sinon use the 'this' context. 'this.sinon' is a sandbox
        // which is created in the root-level hook in the helpers.js file. 
        // Spies or stubs created with this are automatically cleaned up 
        // using the afterEach hook.
        mySpy = this.sinon.spy(MyObject, 'functionName');
    });

    // clean up after tests complete, optional
    afterEach(function () {
        mySpy.restore();
    });

    // Make your tests specific, and split them up into small chunks
    it('description of this test', function () {
        // test and expect
    });

    it('description of another test', function () {
        // test and expect
    });
});
```

## Running Mocha Tests
There are two ways to run the mocha tests:
1. Execute the `mocha-test` gulp task: `gulp mocha-test`
2. Run the test manually from the command line. In order to run mocha tests manually, you will need to include both setup files in the command. For example, to test `foo-spec.js` by running mocha manually, you would use the following command:
`./node_modules/.bin/mocha --require ./test/mocha-test/setup/setup.js  ./test/mocha-test/setup/helpers.js ./test/mocha-test/foo-spec.js`

### Debugging Mocha Tests In Chrome DevTools
*Requires Node v6 or higher*
To debug the tests within Chrome DevTools, you have to run Mocha from the command line instead of through gulp. From the oso-frontend directory, run this command:
`$ ./node_modules/.bin/mocha --inspect --inspect-brk test/mocha-test/setup/helpers.js test/mocha-test/foo-spec.js`

Then, in Chrome, open a new tab to this page: about:inspect There should be a link to inspect the Mocha tests as a “Remote Target.” Click the link and the tests will start. You can set breakpoints in devtools or you can put debugger; statements in the spec code itself.

Be warned that the tests seem to run much, much slower while monitoring them in DevTools. So don’t run all of the specs (test/mocha-test/*spec.js), but instead just run the one spec that you want to debug.

## Tips and Tricks

### Spying on handlers in Views

When adding spies/stubs to methods in a view, you will likely need to place the spy on the prototype of the view class and then instantiate the view for the test.

See the following example:
```
it('executes on <a class="oa-js-nav"> clicks within child views', function () {
    // Create the spy on the prototype
    var spy = this.sinon.spy(ContentLayout.prototype, '_handleNavAnchorClicks');

    var myCl = new ContentLayout(validOptions);
    myCl.showPage('contact-us');
    myCl.$el.find('a.oa-js-nav').trigger('click');

    expect(spy).to.have.been.calledOnce;

    // clean up the spy (a bit different than usual)
    ContentLayout.prototype._handleNavAnchorClicks.restore();
});
```

### Testing `Model.fetch()`

For the most part, we should not need to write tests that call model.fetch() (we already trust Backbone). Instead, we should be writing tests for functions that would be called as a result of a fetch. However, if we _do_ need to test in a way that includes fetch(), this is a simple example of how to stub the call:

```
var ajaxStub = sinon.stub($, 'ajax').yieldsTo(
    'success',
    { 'id': 123, 'content': 'Hello, World!'}
);
model.fetch();
expect(model.get('id')).to.equal(123);
ajaxStub.restore();
```


### Using `sinon.fakeServer` with Mocha Tests

There is an [issue using fakeServer with node.js](https://github.com/sinonjs/sinon/issues/319). In order to work around this, a function was created in `setup/helpers.js`. 

If a test is required to use a fakeServer, use the following code within your test spec:

```
var fakeServer = this.setupFakeServer(this.sinon, global, window);
```

### Using `sinon.useFakeTimers` to Handle Timeout Functionality

We could fake timer functionalities used in the application, which can be a animation timer / ajax timeout etc..

See the following example

```
var clock;
describe('fake timers', function () {
    beforeEach(function () {
        // Overwrite the global timer functions (setTimeout, setInterval) with Sinon fakes
        clock = sinon.useFakeTimers();
    });

  afterEach(function () {
    // Restore the global timer functions to their native implementations
    clock.restore();
  });

  it('should synchronously advance the JavaScript clock', function () {
    // Create a spy to record all calls
    var spy = sinon.spy();
    // Call the spy in 1000ms
    setTimeout(spy, 1000);
    // Tick the clock forward 999ms
    clock.tick(999);
    // The spy should not have been called yet
    expect(spy).to.not.be.called;
    // Tick the clock forward 1ms
    clock.tick(1000);
    // Now the spy should have been called
    expect(spy).to.be.called;
  });

  it('should also work with setInterval', function () {
    // Faking the JavaScript clock also works with setInterval:
    var spy = sinon.spy();
    setInterval(spy, 200);
    clock.tick(400);
    expect(spy).to.be.calledTwice;
    clock.tick(200);
    expect(spy).to.be.calledThrice;
  });
});

```

### Stubbing Backbone.Radio calls


For a Radio Channel setup like the following:
```
var myChannel = Backbone.Radio.channel('channelName');
myChannel.reply('methodName', handlerFunction);
```

In which a module calls the method in this manner:
```
var myChannel = Backbone.Radio.channel('channelName');
var value = myChannel.request('arg1', 'arg2');
```

Stubbing requires the following:
```
var myChannel = Backbone.Radio.channel('channelName');
var radioRequestStub = sinon.stub(myChannel._requests.methodName, 'callback');
radioRequestStub.withArgs('arg1', 'arg2').returns('someValue');
```

The stub uses the property added to the channel's `_requests` property as the object and the name of the function to be stubbed is literally 'callback'.
Using the stub.withArgs() call allows for tests that may call the channel's function at different times, with different arguments. For example, to stub
a function to stub two different input/output values
```
radioRequestStub.withArgs('arg1').returns(true);
radioRequestStub.withArgs('arg2').returns(false);
```


To check that the stub was called with the proper arguments:
```
expect(radioRequestStub).to.have.been.calledWith('arg1', 'arg2');
```

And finally, to clean up the stub:
```
radioRequestStub.restore();
```

##Running Coverage Report From NPM
While the gulp task `mocha-test` will provide a coverage report, this can also be done via an npm script: `npm run coverage`

This script uses the Istanbul's CLI. There is an `.istanbul.yml` file at the project root which provides options for Istanbul. *Note*: This file is _not_ read by `gulp-istanbul` which is used in the `mocha-test` gulp task.

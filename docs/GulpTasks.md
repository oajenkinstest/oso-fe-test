# OSO Gulp Tasks

## browserify.js

Browserify generates JavaScript bundles by combining files according to the CommonJS module dependencies that the files require. It generates a source map that includes the source files encoded into the source map itself. The source map is appended *inline* as a comment at the end of the file, more than doubling the size of the bundle. Because of the large size, these bundles must always be minified before they are deployed to production. See the *uglify.js* section for details about how the OSO frontend code is minified.



* __browserify-for-testing__
    - Creates a bundle for each JS file in the codebase. Each bundle includes all dependencies for that one JS file.
    - Bundles are placed into the /test/dist directory, maintaining the same directory structure and filenames as in /src.
* __browserify-devdata__
    - Creates a bundle for the Dev Data page, /src/js/devdata.js.
    - This bundle includes *all* dependencies, even third-party JS code.
    - JS is preprocessed using the "PROD" target.
    - Outputs to /dist/js/devdata.built.js
* __browserify-vendor__
    - Creates a bundle of OSO's third-party JS dependencies, as defined in /src/js/vendor.js.
    - JS is preprocessed using the "PROD" target.
    - Outputs to /dist/js/vendor.built.js
* __browserify-dev__
    - Creates a bundle for OSO's main JavaScript file, /src/js/app.js.
    - This bundle excludes most third-party JS code, which is bundled separately.
    - JS is preprocessed using the "DEV" target.
    - Outputs to /dist/js/app.built.js
* __browserify-prod__
    - Creates a bundle for OSO's main JavaScript file, /src/js/app.js.
    - This bundle excludes most third-party JS code, which is bundled separately.
    - JS is preprocessed using the "PROD" target.
    - Outputs to /dist/js/app.built.js
* __browserify-release__
    - Identical behavior to "browserify-prod".
* __browserify-widgetpc-dev__
    - Creates a bundle for Performance Center widget's main Javascript file, /src/js/modules/performance-center/app.js which includes only the files contained in /src/js/modules/performance-center/. 
    - This bundle excludes most third-party JS code, which is bundled separately.
    - JS is preproccessed using the "DEV" target.
    - Outputs to /dist/js/app.built.js
* __browserify-widgetpc-release__
    - Same behavior as "browserify-widgetpc-dev", except the JS is preprocessed using the "PROD" target.

## browserSync.js

* __browserSync__
    - ??
* __bs-reload__
    - ??

## build.js

* __build-dist__
    - Builds the site into the /dist directory.
    - JavaScript is bundled, but not minified.
    - HTML files are preprocessed using the "DEV" target.
* __build-dist-webpack__
        - Utilizes much of the same tasks as "build-dist" above, however, the JavaScript bundling, preprocessing, and creation of separate source-maps are handled by webpack.
* __build-release__
    - Runs unit browser tests through the `mocha-test` task. If there are test failures, the build will fail.
    - Builds a release-ready version of the site into the /release directory.
    - JavaScript is bundled and minified.
    - HTML files are preprocessed using the "PROD" target.
* __build-release-archive__
    - Creates a ZIP of all files in /release, then runs "clean-release-keep-artifact" to delete everything but the ZIP from the /release directory.
    - The filename of the ZIP file is the name property from package.json, with .zip appended (e.g. oso-frontend.zip).
* __build-release-webpack__
    - Utilizes much of the same tasks as "build-release" above, however, the JavaScript bundling, preprocessing, minification, and creation of separate source-maps are handled by webpack.
* __build-widgetpc-dist__
    - Builds the Performance Center widget into the /dist directory.
    - JavaScript is bundled, but not minified.
    - HTML files are preprocessed using the "DEV" target.

## bump.js

* __bump-release__
    - Usage: `gulp bump-release --tag vMAJOR.MINOR.PATCH`
    - Changes the version in package.json to the version string provided in the required `--tag` parameter.

## clean.js

* __clean-all__
    - Cleans /dist, /release, and /test/dist directories.
* __clean-dist__
    - Deletes all files from /dist directory.
* __clean-release__
    - Deletes all files from /release directory.
* __clean-release-keep-artifact__
    - Deletes all non-ZIP files from /release directory.
* __clean-test__
    - Deletes all files from /test/dist directory.

## copy.js

* __copy-dist__
    - Runs "copy-fonts-dist", "copy-xdomain-dist", and "copy-ace-ie-css-dist".
* __copy-release__
    - Runs "copy-fonts-release", "copy-xdomain-release", and "copy-ace-ie-css-release".
* __copy-fonts-dist__
    - Copies all fonts into the /dist/fonts directory.
* __copy-fonts-release__
    - Copies all fonts into the /release/fonts directory.
* __copy-xdomain-dist__
    - Copies src/vendor/xdomain.min.js into the /dist/js directory.
* __copy-xdomain-release__
    - Copies src/vendor/xdomain.min.js into the /release/js directory.
* __copy-ace-ie-css-dist__
    - Copies src/css/ace-ie.css into the /dist/css directory.
* __copy-ace-ie-css-release__
    - Copies src/css/ace-ie.css into the /release/css directory.

## css.js

* __css-minify-css-prod__
    - _unused_
* __css-minify-css-release__
    - Minifies /dist/css/all.css and writes the result to /release/css/all.min.css.
    - NOTE: This task does nothing if /dist/css/all.css is not found, and no error is reported.

## default.js

* __default__
    - Runs the "dev" task.
* __dev__
    - Runs other tasks to build the site into /dist (`build-dist`), start the local server(`devserver-dist`), run unit tests (`mocha-test`) and watch for file changes (`watch-dev`).
* __dev-fast__
    - Runs the same tasks as `dev` above with the exception of running tests (`mocha-test`)*[]:
* __dev-fast-webpack__
    - Based on `dev-fast` above, except the JavaScript bundling, preprocessing, and source-map creation are handled by webpack.
* __dev-webpack__
    - Runs other tasks to build the site into /dist (`build-webpack-dist`), start the local server (`devserver-dist`), run unit tests (`mocha-test`) and watch for file changes (`watch-dev-webpack`).
* __prodcheck__
    - Runs other tasks to build the site into /release (`build-release`), start the local server(`devserver-release`), an watch for file changes (`watch-prod`).
    - Used for developer testing of the site using minified files as if the site were in production.
* __widget-pc-dev__
    - Runs other tasks to build the Performance Center widget into /dist (`build-widgetpc-dist`), start the local server (`devserver-dist`), and watch for file changes(`watch-widgetpc-dev`).
* __widget-pc-prodcheck__
    - Runs other tasks to build the Performance Center widget into /release (`build-widgetpc-release`), start the local server (`devserver-release`), and watch for file changes(`watch-prod`).

## devserver.js

* __devserver-dist__
    - Start a local web server at http://localhost:3000.
    - Serves bundled but unminified files from /dist.
* __devserver-release__
    - Start a local web server at http://localhost:3000.
    - Serves bundled and minified files from /release.
* __devserver-unit-test-develop__
    - ??

## images.js

* __images__
    - Copies image assets to /dist/images.
* __images-release__
    - Copies image assets to /release/images.
* __images-widget-pc__
    - Copies images required for Performance Center widget to /dist/images folder.

## less.js

* __less__
    - Preprocess .less files to generate CSS stylesheet for OSO.
    - Outputs to /dist/css/all.css.
* __less-pc__
    - Preprocess .less files to generate CSS stylesheet for Performance Center widget.
    - Outputs to /dist/css/all.css.

## lint.js

* __lint__
    - Runs ESLint on all .js files in the src/js directory.
    - Uses settings in .eslintrc.
* __lint-prod__
    - Runs ESLint on all .js files in the src/js directory.
    - Uses settings in .eslintrc-prod.
* __lint-release__
    - Identical behavior to "lint-prod"
* __lint-gulp__
    - Runs JSHESLintint on all .js files in the src/js directory.
    - Uses settings in .eslintrc-node.
* __lint-test__
    - Runs ESLint on all .js files in the various "specs" directories within /test.
    - Uses settings in .eslintrc-test.

## preprocess.js

* __preprocess-dev__
    - ??
* __preprocess-prod__
    - ??
* __preprocess-release__
    - ??

## setWatch.js

(no code)

## test.js

* __test__
    - Shortcut to run the "mocha-test" task.
* __test-unit-test-setup__
    - Through the "browserify-for-testing" task, it puts fresh bundles of the source code used by the test specs into the test/dist directory.
* __mocha-test__
    - First it rebuilds the test/dist bundles, then it runs the Mocha tests utilizing [JSDOM](https://github.com/tmpvar/jsdom). If any tests fail, then this task will cause the entire task to fail.
* __mocha-test-fast__
    - It runs the Mocha test specs without first creating fresh bundles in test/dist. This is useful while tweaking tests if no changes have been made to the application source.

## uglify.js

The OSO frontend JavsScript bundle is minified using UglifyJS 2 by way of the *gulp-uglify* npm module. These tasks also use *gulp-sourcemaps* to save the source map that uglify generates to a separate file.

* __uglify-scripts-prod__
    - Runs the "uglify-app-script-prod," "uglify-vendor-script-prod," and "uglify-devdata-script-prod" tasks.
* __uglify-app-script-prod__
    - Minifies OSO's main JavaScript bundle, /dist/js/app.built.js.
    - Outputs to /dist/js/app.min.js.
* __uglify-devdata-script-prod__
    - Minifies the bundle for the Dev Data page, /dist/js/devdata.built.js.
    - Outputs to /dist/js/devdata.min.js.
* __uglify-vendor-script-prod__
    - Minifies the vendor bundle, /dist/js/vendor.built.js.
    - Outputs to /dist/js/vendor.min.js.
* __uglify-scripts-release__
    - Runs the "uglify-app-script-release," "uglify-vendor-script-release," and "uglify-devdata-script-release" tasks.
* __uglify-app-script-release__
    - Minifies OSO's main JavaScript bundle, /dist/js/app.built.js.
    - Outputs to /release/js/app.min.js.
* __uglify-devdata-script-release__
    - Minifies the bundle for the Dev Data page, /dist/js/devdata.built.js.
    - Outputs to /release/js/devdata.min.js.
* __uglify-vendor-script-release__
    - Minifies the vendor bundle, /dist/js/vendor.built.js.
    - Outputs to /release/js/vendor.min.js.
    
## watch.js

* __watch-dev__
    - Watch all file changes in source code and run other tasks such as `lint`, `less`, `images`, `preprocess-dev`, `browserify-dev` and `mocha-test`
* __watch-dev-webpack__
    - Watch all file changes in source code and run other tasks such as `lint`, `less`, `images`, `preprocess-dev`, `webpack-dev` and `mocha-test`
* __watch-prod__
    - Watch all file changes in source code and run other tasks such as `lint-prod`, `less`, `images`, `preprocess-prod`, `css-minify-css-prod` , `browserify-release` and `uglify-scripts-prod`
* __watch-widgetpc-dev__
    - Watch all files changes especially for Performance Center widget module source and run other tasks such as `lint`, `less-pc`, `images-widget-pc`, `preprocess-dev` and `browserify-widgetpc-dev`

## webpack.js
**Note**: `webpack-stream` and `webpack` are used to integrate with `gulp`. Since the version of `webpack` used requires Node v6.11.0 or higher, these tasks will not be initialized if this requirement is not met.

* __webpack-bundle-dev__
    - Uses the configuration options in `webpack.common.js` and `webpack.dev.js` to bundle JavaScript files.
    - `webpack-bundle-analyzer` will create `htmlReport/webpackBundleReport.html` which will give insight into the makeup of each bundle
* __webpack-bundle-release__
    - Used the configuration options in `webpack.common.js` and `webpack.prod.js` to bundle JavaScript files

## zip.js

* __zip-release__
    - ??

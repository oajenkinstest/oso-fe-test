# Analytics / Site Metrics

## Summary
In order to track the use of the OSO web application, we are using 3rd party analytics tools.
We are using WebTrends and Google Tag Manager.

## Modules

### analyticsModule.js

The analytics code is encapsulated in the `analyticsModule.js` module. It exposes methods via
Backbone.Radio for loose coupling. The application can use the module by calling the methods defined in
the `analytics` channel.

###### Parameters

* `providers`
An Array object. Expected at least one provider to configure analytics
* `webId`
	A parameter to set user identifier for analytics.


example code

	// initialize analytics.
    analytics.init({
        providers: [
            new WebTrendsProvider(),
            new GoogleTagManagerProvider()
        ],
        webId: userModel.get('webId')
    });

#### baseAnalyticsProvider.js
A class which can be extendable for multiple providers. The class defines the following public functions:

* `setUser` - Set a new user ID for the provider to use
* `trackAction` - Track that an action has occurred (a link click, form submission, etc).
* `trackView` - Track that a page has been viewed.

If a provider does not implement a method, the `analyticsModule` will silently skip it.

### Radio call for Analytics to trigger the corresponding methods

##### Track that a user viewed a page
        
        analyticsChannel.trigger('trackView', {
            uri   : '#home',
            title : 'OSO Home'
        });

##### Track that a user completed some action

        analyticsChannel.trigger('trackAction', {
            event : eventObject
        });

##### Set data layer for Google Tag Manager

        analyticsChannel.trigger('setDataLayer', {
            policyId : 015456897
        });

### config.js
The `config` module contains environment-specific properties for the analytics packages.

## Providers

### Google Tag Manager
[Google Tag Manager](https://developers.google.com/tag-manager/quickstart) (GTM) was added to replace Google Analytics([OOSO-3660](https://oneamerica.atlassian.net/browse/OOSO-3660)).
Events like button clicks, link clicks, etc. are tracked by Google Tag Manager automatically (see [auto-event tracking](https://support.google.com/tagmanager/answer/6106716)), so this provider does not implement `trackAction` or `trackView`.

Google Tag Manager works by pushing data on to a global array named `gtmDataLayer`. This is handled by the module using the following public methods:
  * `setDataLayerValue(options)` - Used to push new data to the `gtmDataLayer` array which will eventually get sent to Google Analytics as well as any other tool associated with GTM.
  * `trackException(options)` - Used to send errors and exceptions to GTM
  * `trackView(options)` - Used to send `newViewEvent` event to `gtmDataLayer`. A view can have a `gtmTrackViewEvent` string set to the name of an event which is called when data is retrieved. The `trackView()` method will wait until this event has been fired so that data retrieved from the service and set in the view's `dataLayerValues` object can be sent to the data layer along with the `newViewEvent`. See [Policy Detail view](../src/js/pages/policy/views/policy-detail-v.js) for an example.

The [Google Tag Manager Implementation Guide](./GoogleTagManagerImplementationGuide.docx) is the initial guide provided by Fusion to OneAmerica.

**Note**: There are some security concerns with Google Tag Manager since it can be integrated with many third-party tools. In addition to a [Content Security Policy](./ContentSecurityPolicy.md) being applied to the site, GTM can be prevented from using these third-party scripts programmatically via [whitelisting/blacklisting](https://developers.google.com/tag-manager/devguide#restricting-tag-deployment).

### WebTrends
Since OSO is a single-page app, we must make explicit calls to WebTrends. We use the `multitrack`
function. More information is available here: http://help.webtrends.com/en/jstag/tracking_multitrack.html.
In an effort to keep licensing costs down, OneAmerica only uses WebTrends in production environments ([OOSO-3481](https://oneamerica.atlassian.net/browse/OOSO-3481)).



## Adding a Provider

* Place provider and environment-specific configuration, like URLs and IDs in the `src/js/config/config` module.
* Create a module in `/src/js/modules/analytics/providers` that extends the `baseAnalyticsProvider` module. The public functions of the module should be:
  * `initialize(options)` - set up the provider and allow config to be overridden by options properties
  * `trackView(options)` - send data to the provider indicating that a user viewed a page. Properties of the options object are 'title' and 'uri'.
  * `trackAction(options)` - send data to the provider indicating that a user performed an action. The `options` object contains the following properties:
    * actionName - name of the action. This may need to be translated before calling the provider.
    * eventType - the type of link clicked, based on the JavaScript event's `currentTarget.href` property.
    * referenceURI - the URI of the page the action took place on
    * title - text describing the action, ie 'Download: [file path]'
    * uri - the href of the link being clicked (may not be present)
  * `trackException` - A method to track all exceptions occured in the application. Currently its only implemented for Google Analytics, as WebTrends doesn't have interface for exception reporting. The `options` contain below propreties:
    * message - Error message to keep record.
    * fatal - To classify fatal error.



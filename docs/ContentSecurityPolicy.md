# Content Security Policy

## Overview
A Content Security Policy (CSP) is in place for OneSource Online in order to mitigate the risk and impact of [cross-site scripting](https://en.wikipedia.org/wiki/Cross-site_scripting) (XSS) attacks.

This document is meant to be *the* source for the CSP used by the OSO front-end. It should be used to provide reasoning and history for the decisions made regarding the CSP which is currently in place.

The following links provide information on CSP as well as the reasons for putting one in place:

 * https://www.owasp.org/index.php/Content_Security_Policy
 * https://developers.google.com/web/fundamentals/security/csp/
 * https://www.w3.org/TR/CSP2/#violation-reports
 * https://www.html5rocks.com/en/tutorials/security/content-security-policy/
 * https://en.wikipedia.org/wiki/Cross-site_scripting

## Implementation Risks
There are risks involved when implementing and maintaining a CSP for a site. The greatest risk is to create a policy which is too restrictive and render some functionality useless.

In order to prevent this from happening, the `Content-Security-Policy-Report-Only` header will be used which will prevent browsers from actively enforcing the policy. Instead, violations to the policy will be reported to a public endpoint provided by the service and sent as the [`report-uri`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/report-uri) directive value -- in the case of OSO this is `/api/oso/public/rest/csp-log`.

This log should be checked periodically in order to ensure that the current policy is adequate and would not prevent certain features from being used. When no violations have been reported, we can be reasonably certain that the policy is adequate and set the `reportOnly` property to false so that browsers will begin to actually enforce the policy.

In addition to the initial roll-out detailed above, the development server used by front-end developers will enforce the policy. As new features are rolled out, the developers will be able to see
any violations during development in the following areas:

 * Browser consoles like Chrome's "Developer Tools"
 * The console used to run the `gulp` tasks
 * The `content-security-policy.log` file which is created at the root of the project directory when the `gulp` development task is started and the first violation is logged

## Policy for OneSource Online

The policy definition used by the front-end developers is defined in [`/gulp/tasks/devserver.js`](../gulp/tasks/devserver.js). This policy should be kept in-sync with the policy sent by the web servers in ST, FT, and production environments.

### Directives

####`default-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/default-src

 | Value      |  Reasoning  |
 |------------|-------------|
 | `'self'`   | This should be set as per a [W3C recommendation](https://www.w3.org/TR/CSP2/#directives) for protecting against XSS attacks. Allow any non-specified directives to allow content from onesourceonline.oneamerica.com|


####`base-uri` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/base-uri

 | Value                      |  Reasoning                                      |
 |----------------------------|-------------------------------------------------|
 | `'none'`                   | According to [W3C note](https://www.w3.org/TR/CSP2/#directive-base-uri), this directive does not inherit from `default-src`. Since no `<base>` tag is defined in the `index.html` document, setting this value explicitly to `'none'`.|

####`connect-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/connect-src

 | Value                      | Reasoning                                       |
 |----------------------------|-------------------------------------------------|
 | `'self`                    | Allow proxy to the service through onesourceonline.oneamerica.com/api/* |
 | `www.google-analytics.com` | Used to send analytics data to Google Analytics |

####`font-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/font-src

 | Value                       | Reasoning                                              |
 |-----------------------------|--------------------------------------------------------|
 | data:                       | Some browsers appear to use a data URL to access fonts |

####`frame-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/frame-src

 | Value                      |  Reasoning                                                             |
 |----------------------------|------------------------------------------------------------------------|
 | `'self'`                   | Allow `<iframe>` with context set to onesourceonline.oneamerica.com    |
 | `www.googletagmanager.com` | Allow the `<iframe>` context in case the user has JavaScript disabled. |

####`img-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/img-src

 | Value                      |  Reasoning                                      |
 |----------------------------|-------------------------------------------------|
 | `'self'`                   | Allow images from onesourceonline.oneamerica.com|
 | `statse.webtrendslive.com` | Images are used from this domain for analytics  |
 | `www.google-analytics.com` | Images are used from this domain for analytics  |
 | `www.googletagmanager.com` | Images are used from this domain for analytics  |
 | `https://ssl.gstatic.com`  | Images are used from this domain for analytics  |
 | `*.oneamerica.com`         | Image requests from SiteMinder, service, maintenance.oneamerica.com, etc.  |
 | `data:`                    | A [data URL](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) is used in order to check browser support for the WEBP image format.  |


####`script-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src

 | Value                      |  Reasoning                                       |
 |----------------------------|--------------------------------------------------|
 | `'self'`                   | Allow scripts from onesourceonline.oneamerica.com|
 | `statse.webtrendslive.com` | Scripts are used from this domain for analytics  |
 | `www.google-analytics.com` | Scripts are used from this domain for analytics  |
 | `www.googletagmanager.com` | Scripts are used from this domain for analytics  |

**NOTE:** Since the templates used by OSO are not pre-compiled locally, the front-end developers may frequently see violations such as the following:

 ```
 Refused to evaluate a string as JavaScript because 'unsafe-eval' is not an allowed source of script in the following Content Security Policy directive: ...
 ```

This is occurring because of the templates used by OSO. The deployed code uses pre-compiled templates which will not use `evaluate`.

####`style-src` Values

Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src

 | Value                      |  Reasoning                                           |
 |----------------------------|------------------------------------------------------|
 | `'self'`                   | Allow stylesheets from onesourceonline.oneamerica.com|
 | `'unsafe-inline`           | jQuery and Bootstrap use inline styles to animate/show/hide elements. While not recommended, this is necessary in order to keep these elements working. |

## Making Changes to the CSP
Since the CSP is managed by the headers sent from the web servers, any changes which need to be made need to be communicated to the Cognizant Middleware IHS team via a service request using ServiceNow. When the changes are required in ST and FT environments, a service request is all that is required. If a change is being promoted to production, a service request is still needed, however, the request should also include a request for the IHS Middleware Team to create a Change Request.

The service request ticket should request that the headers be modified in the `httpd.conf` file on each server in the environment being changed (e.g. `httpd.conf` on SLTWEB11 and SLTWEB12 for ST). The Cognizant team may also need to be reminded that the IHS instance needs to be restarted after the change is made.

An example of such a service request can be found at https://oa.service-now.com/nav_to.do?uri=sc_req_item.do?sys_id=f97060bcdba25b083cbff5441d9619bf%26sysparm_view=ess.

## CSP Log File Analysis
The CSP log files which contain reported violations can be found at the following locations. These should be evaluated periodically to ensure that the policy continues to allow required functionality.

 | Environment | Path                                          |
 |-------------|-----------------------------------------------|
 | Production  |  \\\\swpwasap11\waslogs\api01\csp-report.log  |
 | Production  |  \\\\swpwasap12\waslogs\api01\csp-report.log  |
 | Production  |  \\\\swpwasap13\waslogs\api01\csp-report.log  |
 | FT          |  \\\\swtwasap11\waslogs\api01\csp-report.log  |
 | FT          |  \\\\swtwasap12\waslogs\api01\csp-report.log  |
 | ST/SB       |  \\\\swdwasap11\waslogs\api01\csp-report.log  |
 | ST/SB       |  \\\\swdwasap12\waslogs\api01\csp-report.log  |

When analyzing the reports, the following URL can be a helpful resource for determining whether or not the violation is preventing intended functionality on the site or not:

https://github.com/nico3333fr/CSP-useful

The log files should be analyzed to determine what violations are being reported. There will *always* be violations appearing in the log files. The goal is not to get to state where violations no longer occur. The goal is to get to a state where required functionality is permitted on the site while *all other activity* is prevented.

If a particular violation is appearing in the log file and, after thorough research, it has been determined that the violation should be allowed, a service request should be opened to modify the violated directive. This document should also be updated to describe the reasoning for the updated/changed directive.

### Going From "Report-only" to Fully Implemented CSP
When it has been determined that all of the legitimate requests for resources are being allowed, the time has come to change the header from `Content-Security-Policy-Report-Only` to `Content-Security-Policy`. Doing so will restrict any resources that are not explicitly allowed under the policy. This should be done slowly and environment-by-environment (e.g. in SB/ST first, then FT, then Production).

When CSP has been fully implemented, it is still important that the CSP logs regularly be analyzed to ensure that needed functionality is not being restricted.
/* global module:false, require:false */
/**
 * This file is merged (using webpack-merge) with the common
 * config file to create a development bundle using webpack.
 */
var commonWebpackConfig   = require('./webpack.common');
var merge                 = require('webpack-merge');
var WebpackBundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = merge(commonWebpackConfig, {
    mode: 'development',
    output: {
        filename: '[name].built.js'
    },
    devtool: 'cheap-module-source-map',
    module: {
        rules: [
            {
                // Preprocess the JS files for DEV
                test: /\.js$/,
                use: ['preprocess-loader?dev=true']
            }
        ]
    },
    plugins: [
        new WebpackBundleAnalyzer({
            analyzerMode: 'static',
            openAnalyzer: false,
            reportFilename: 'htmlReport/webpackBundleReport.html'
        })
    ]
});

